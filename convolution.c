#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core/convolution.h>
#include <sortsmill/core/copy_gmp_with_strides.h>
#include <sortsmill/core/copy_with_strides.h>

static void
mpq_init_vector (size_t n, mpq_t *a)
{
  for (size_t i = 0; i < n; i++)
    mpq_init (a[i]);
}

static void
mpq_clear_vector (size_t n, mpq_t *a)
{
  for (size_t i = 0; i < n; i++)
    mpq_clear (a[i]);
}

VISIBLE void
mpq_convolve (size_t na, ssize_t stride1, mpq_t *a,
              size_t nb, ssize_t stride2, mpq_t *b, ssize_t c_stride, mpq_t *c)
{
  // Convolution of two sequences.
  //
  // This is just the ‘naïve’ algorithm (no Karatsuba, FFT, etc.).

  if (0 < na + nb)
    {
      const size_t n = na + nb - 1;

      mpq_t tmp;
      mpq_init (tmp);

      mpq_t *buffer = xmalloc (n * sizeof (mpq_t));
      mpq_init_vector (n, buffer);

      for (size_t i = 0; i < n; i++)
        mpq_set_ui (buffer[i], 0, 1);   // buffer[i] ← 0
      for (size_t j = 0; j < nb; j++)
        for (size_t i = 0; i < na; i++)
          {
            mpq_mul (tmp, b[stride2 * (ssize_t) j], a[stride1 * (ssize_t) i]);
            mpq_add (buffer[j + i], buffer[j + i], tmp);
          }

      copy_mpq_with_strides (c_stride, c, 1, buffer, n);

      mpq_clear_vector (n, buffer);
      free (buffer);
      mpq_clear (tmp);
    }
}

VISIBLE void
f64convolve (size_t na, ssize_t stride1, const double *poly1,
             size_t nb, ssize_t stride2, const double *poly2,
             ssize_t result_stride, double *result)
{
  // This is just the ‘naïve’ algorithm (no Karatsuba, FFT, etc.).

  const size_t n = na + nb - 1;
  double buffer[n];
  for (size_t i = 0; i < n; i++)
    buffer[i] = 0;
  for (size_t j = 0; j < nb; j++)
    for (size_t i = 0; i < na; i++)
      buffer[j + i] +=
        poly2[stride2 * (ssize_t) j] * poly1[stride1 * (ssize_t) i];
  copy_f64_with_strides (result_stride, result, 1, buffer, n);
}
