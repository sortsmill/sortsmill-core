#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>
#include <math.h>
#include <stdio.h>

#if SMCORE_N_ENABLE_THREADS

#include <pthread.h>

pthread_mutex_t pi__Mutex = PTHREAD_MUTEX_INITIALIZER;

STM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, double, pi,
                         pthread_mutex_lock (&pi__Mutex),
                         pthread_mutex_unlock (&pi__Mutex),
                         (pi__Value = 4 * atan (1)));

#else

STM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, double, pi,
                         /* no locking */ ,
                         /* no unlocking */ ,
                         (pi__Value = 4 * atan (1)));

#endif

int
main (int argc, char **argv)
{
  // Cheat by looking at and modifying the ‘internal’ variables.
  printf ("%u ", (unsigned int) __pi__is_initialized);
  printf ("%.6lf ", pi ());
  printf ("%u ", (unsigned int) __pi__is_initialized);
  printf ("%.6lf ", pi__Value);
  printf ("%u ", (unsigned int) (pi () == pi__Value));
  pi__Value = -5;
  printf ("%.6lf", pi ());
  return 0;
}
