#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// gauss_elim_solve__() may not be visible in the library, so include
// the source directly.
#include "../gauss_elim_solve__.c"

int
main (int argc, char **argv)
{
  double tolerance = atof (argv[1]);
  int m = atoi (argv[2]);
  int n = atoi (argv[3]);

  double data[m][n];
  double *A[m];
  for (int i = 0; i < m; i++)
    A[i] = data[i];
  for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++)
      A[i][j] = atof (argv[4 + n * i + j]);

  int signum;
  int info;
  gauss_elim_solve__ (tolerance, m, n, A, &signum, &info);

  printf ("%d", info);
  if (info == 0)
    {
      printf (" ");
      for (int i = 0; i < m; i++)
        for (int j = m; j < n; j++)
          printf ("%.6lf ", A[i][j]);
      printf ("%d", signum);
    }

  return 0;
}
