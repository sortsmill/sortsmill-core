// Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
#include "sortsmill/core/HATS/staload.hats"

staload UN = "prelude/SATS/unsafe.sats"
staload STDLIB = "libats/libc/SATS/stdlib.sats"

val node_size = 32

typedef t = Int
typedef vector (n : int, p : addr) = immutable_vector (t, n, p)
typedef vector (n : int) = [p : addr] vector (n, p)
typedef vector = [n : int] vector n
typedef entry_ref (q : addr) = immutable_vector_entry_ref (t, q)
typedef entry_ref = [q : addr] entry_ref q

val nil = immutable_vector_nil
val is_nil = immutable_vector_is_nil<>
val length = immutable_vector_length<>
val get = immutable_vector_get<t>
val get_exn = immutable_vector_get_exn<t>
val gets = immutable_vector_gets_as_arrayref<t>
val gets_exn = immutable_vector_gets_as_arrayref_exn<t>
val set = immutable_vector_set<t>
val set_exn = immutable_vector_set_exn<t>
val sets = immutable_vector_sets_from_arrayref<t>
val sets_exn = immutable_vector_sets_from_arrayref_exn<t>
val push = immutable_vector_push<t>
val pushes = immutable_vector_pushes_from_arrayref<t>
val pop = immutable_vector_pop<t>
val pop_exn = immutable_vector_pop_exn<t>
val pops = immutable_vector_pops<t>
val pops_exn = immutable_vector_pops_exn<t>
val slice_append = immutable_vector_slice_append<t>
val slice_append_exn = immutable_vector_slice_append_exn<t>
val slice = immutable_vector_slice<t>
val slice_exn = immutable_vector_slice_exn<t>
val append = immutable_vector_append<t>
val slice_reverse_append = immutable_vector_slice_reverse_append<t>
val slice_reverse_append_exn = immutable_vector_slice_reverse_append_exn<t>
val slice_reverse = immutable_vector_slice_reverse<t>
val slice_reverse_exn = immutable_vector_slice_reverse_exn<t>
val reverse_append = immutable_vector_reverse_append<t>
val reverse = immutable_vector_reverse<t>

typedef i2i = (Int -<cloref> Int)
val negate = (lam i => ~i) : i2i
val times10 = (lam i => i * 10) : i2i
val double = (lam i => i + i) : i2i

fn {t : t@ype}
shuffle_array {n : nat} (n : size_t n, a : arrayref (t, n)) : void =
  // See https://en.wikipedia.org/w/index.php?title=Fisher%E2%80%93Yates_shuffle&oldid=683105835
  let
    fun loop {i : nat | i < n} (i : size_t i) :<cloref1> void =
      if i2sz 0 < i then
        let
          val () = assertloc (i < n)
          val j =
            $UN.cast {Size_t} ($STDLIB.random () mod ($UN.cast {lint} (succ i)))
          val () = assertloc (j < n)
          val x = a[i]
          val () = a[i] := a[j]
          val () = a[j] := x
        in
          loop (pred i)
        end
  in
    if i2sz 0 < n then
      loop (pred n)
  end

fun
check_many {n, i, j : nat | i <= j; j <= n} .<j - i>.
           (v : vector n, i : int i, j : int j, f : i2i) : void =
  if i < j then
    let
      val x = f i
    in
      assertloc (get (v, i2sz i) = x);
      assertloc (get_exn (v, i2sz i) = x);
      check_many (v, succ i, j, f)
    end

fn
check_next_many {n : pos} {p : addr}
                (v : vector (n, p)) : void =
  let
    fun loop {i : nat | i <= n} {q : addr} .<n - i>.
             (pf : IMMUTABLE_VECTOR_ENTRY_REF (p, i, q) |
              i : size_t i, entry_ref : entry_ref q) :<cloref1> void =
      let
        val n = length v
      in
        if i < n then
          let
            val x1 = immutable_vector_entry_ref_get (entry_ref) : t
            val x2 = get (v, i) : t
            val () = assertloc (x1 = x2)
            val (pf | entry_ref) =
              immutable_vector_entry_ref_next (pf | v, i, entry_ref)
          in
            loop (pf | succ i, entry_ref)
          end
        else
          assertloc (immutable_vector_entry_ref_is_nil entry_ref)
      end

    val (pf | entry_ref) = immutable_vector_entry_ref (v, i2sz 0)
  in
    loop (pf | i2sz 0, entry_ref)
  end

fn
check_prev_many {n : pos} {p : addr}
                (v : vector (n, p)) : void =
  let
    fun loop {i : nat | i < n} {q : addr} .<i>.
             (pf : IMMUTABLE_VECTOR_ENTRY_REF (p, i, q) |
              i : size_t i, entry_ref : entry_ref q) :<cloref1> void =
      let
        val x1 = immutable_vector_entry_ref_get (entry_ref) : t
        val x2 = get (v, i) : t
        val () = assertloc (x1 = x2)
        val (pf | entry_ref) =
          immutable_vector_entry_ref_prev (pf | v, i, entry_ref)
      in
        if i2sz 0 < i then
          loop (pf | pred i, entry_ref)
        else
          assertloc (immutable_vector_entry_ref_is_nil entry_ref)
      end

    val n = immutable_vector_length v
    val (pf | entry_ref) = immutable_vector_entry_ref (v, pred n)
  in
    loop (pf | pred n, entry_ref)
  end

fun
push_many {n, i, j : nat | i <= j} .<j - i>.
          (v : vector n, i : int i, j : int j, f : i2i) :
    vector (n + j - i) =
  if i < j then
    push_many (push (v, f i), succ i, j, f)
  else
    v

fn
pushes_many {n, j : nat | 0 <= j}
            (v : vector n, j : int j, f : i2i) :
    vector (n + j) =
  let
    val a = arrayref_tabulate_cloref (i2sz j, lam i => f (sz2i i))
  in
    pushes (v, i2sz j, a)
  end

fn
set_many {n, i, j : nat | i <= j}
         (v : vector n, i : int i, j : int j, f : i2i) :
    vector n =
  let
    // Set the entries in a random order.
    val a = arrayref_tabulate_cloref<Int> (i2sz (j - i), lam k => sz2i k)
    val () = shuffle_array (i2sz (j - i), a)

    fun loop {k : nat | k <= (j - i)} .<(j - i) - k>.
             (w : vector n, k : int k) :<cloref1> vector n =
      if k < (j - i) then
        let
          val m = i + a[k]
          val () = assertloc (0 <= m)
          val () = assertloc (i2sz m < immutable_vector_length v)
        in
          loop (set (w, i2sz m, f m), succ k)
        end
      else
        w
  in
    loop (v, 0)
  end

fn
set_exn_many {n, i, j : nat | i <= j}
             (v : vector n, i : int i, j : int j, f : i2i) :
    vector n =
  let
    // Set the entries in a random order.
    val a = arrayref_tabulate_cloref<Int> (i2sz (j - i), lam k => sz2i k)
    val () = shuffle_array (i2sz (j - i), a)

    fun loop {k : nat | k <= (j - i)} .<(j - i) - k>.
             (w : vector n, k : int k) :<cloref1> vector n =
      if k < (j - i) then
        let
          val m = i + a[k]
          val () = assertloc (0 <= m)
          val () = assertloc (i2sz m < immutable_vector_length v)
        in
          loop (set_exn (w, i2sz m, f m), succ k)
        end
      else
        w
  in
    loop (v, 0)
  end

fn
check_sets_and_gets {n : nat} (n : int n, f : i2i, g : i2i) : void =
  let
    val v = push_many (nil, 0, n, f)

    fun loop {i : nat} (i : size_t i) :<cloref1> void =
      if sz2i i <= n then
        let
          fun inner_loop {m : nat | m <= n - i + 1} (m : size_t m) :<cloref1>
              void =
            if sz2i (i + m) <= n then
              let
                val a = arrayref_tabulate_cloref (m, lam k => g (sz2i (i + k)))
                val w = sets (v, i, m, a)
                val h =
                  (lam k =>
                     if k < sz2i i then
                       f k
                     else if k < sz2i (i + m) then
                       g k
                     else
                       f k) : i2i
                val () = check_many (w, 0, n, h)
                val a1 = gets (w, i, m)
                val a2 = gets_exn (w, i, m)
                fun compare {j : nat | j <= m} (j : size_t j) :<cloref1> void =
                  if j < m then
                    begin
                      assertloc (a1[j] = a[j]);
                      assertloc (a2[j] = a[j]);
                    end
                val () = compare (i2sz 0)
              in
                inner_loop (succ m)
              end
          val () = inner_loop (i2sz 0)
        in
          if i <= i2sz (3 * node_size) then
            loop (succ i)
          else if sz2i i = n then
            loop (i2sz (succ n))
          else
            loop (i2sz n)
        end
  in
    loop (i2sz 0)
  end

fun
pop_many {n : nat} .<n>.
         (v : vector n, n : int n, f : i2i) : void =
  if i2sz 0 < n then
    let
      val v = pop v
      val () = check_many (v, 0, pred n, f)
    in
      pop_many (v, pred n, f)
    end

fun
pop_exn_many (v : vector, f : i2i) : void =
  let
    val n = immutable_vector_length v
  in
    if i2sz 0 < n then
      let
        val v = pop_exn v
        val n = pred n
        val () = assertloc (immutable_vector_length v = n)
        val () = check_many (v, 0, sz2i n, f)
      in
        pop_exn_many (v, f)
      end
  end

fun
pops_many {n : nat} {m : nat | m <= n} .<m>.
          (v : vector n, m : int m, f : i2i) : void =
  let
    val n = $UN.cast {int n} (length v)
    val v1 = pops (v, i2sz m)
    val () = check_many (v1, 0, n - m, f)
    val v2 = pops (v1, i2sz (n - m))
    val () = assertloc (is_nil v2)
  in
    if i2sz 0 < m then
      pops_many (v, pred m, f)
  end

fun
pops_exn_many {n : nat} {m : nat | m <= n} .<m>.
              (v : vector n, m : int m, f : i2i) : void =
  let
    val n = $UN.cast {int n} (length v)
    val v1 = pops_exn (v, i2sz m)
    val () = check_many (v1, 0, n - m, f)
    val v2 = pops_exn (v1, i2sz (n - m))
    val () = assertloc (is_nil v2)
  in
    if i2sz 0 < m then
      pops_exn_many (v, pred m, f)
  end

fun
compare_subvectors {i, j, n : nat} .<n>.
                   (u : immutable_vector t, i : size_t i,
                    v : immutable_vector t, j : size_t j,
                    n : size_t n) : void =
  if i2sz 0 < n then
    let
      val () = assertloc (get_exn (u, i) = get_exn (v, j))
    in
      compare_subvectors (u, succ i, v, succ j, pred n)
    end

fn
compare_reverse {n : nat}
                (u : immutable_vector (t, n),
                 v : immutable_vector (t, n)) : void =
  let
    val () = assertloc (length u = length v)
    fun loop {i : nat | i <= n} .<n - i>. (i : size_t i) :<cloref1> void =
      if i < length u then
        let
          val () = assertloc (get_exn (u, i) = get_exn (v, pred (length v) - i))
        in
          loop (succ i)
        end
  in
    loop (i2sz 0)
  end

implement
main0 () =
  let
    val () = assertloc (ptr_is_null ($UN.cast {ptr} nil))
    val () = assertloc (is_nil ($UN.cast {immutable_vector t} the_null_ptr))

    val v = nil
    val () = assertloc (is_nil v)
    val () = assertloc (length v = i2sz 0)

    val n = node_size * node_size * node_size + 5 * node_size + 1
    val v = push_many (nil, 0, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val v = set_many (v, 0, n, times10)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, times10)
    val v = set_exn_many (v, 0, n, double)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, double)
    val () = check_next_many v
    val () = check_prev_many v

    val n = 32
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 33
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 64
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 65
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 1024
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 1025
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 2080
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 2081
    val v = pushes_many (nil, n, negate)
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val v = pushes_many (v, n, lam i => g1ofg0 ~((g0ofg1 i) mod n))
    val () = assertloc (not (is_nil v))
    val () = assertloc (length v = i2sz n + i2sz n)
    val () = check_many (v, 0, n + n, lam i => g1ofg0 ~((g0ofg1 i) mod n))

    val n = node_size * node_size + 5 * node_size + 1
    val v = push_many (nil, 0, n, negate)
    val () = check_many (v, 0, n, negate)
    val () = pop_many (v, n, negate)
    val () = pop_exn_many (v, negate)
    val () = pops_many (v, n, negate)
    val () = pops_exn_many (v, n, negate)

    val n = node_size * node_size + 5 * node_size + 1
    val () = check_sets_and_gets (n, negate, double)

    val () =
      {
        val n = node_size * node_size + 5 * node_size + 1
        val u = push_many (nil, 0, n, negate)
        val v = push_many (nil, 0, n, double)
        val w = append (nil, u)
        val () = assertloc (length w = length u)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, length u)
        val w = append (u, nil)
        val () = assertloc (length w = length u)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, length u)
        val w = append (u, v)
        val () = assertloc (length w = length u + length v)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, length u)
        val () = compare_subvectors (v, i2sz 0, w, length u, length v)
      }

    val () =
      {
        val n = 17 * node_size + 1
        val [n : int] n = g1ofg0 n
        val () = assertloc (0 < n)
        val u = push_many (nil, 0, n, negate)
        val v = push_many (nil, 0, n, double)
        val n = i2sz n
        fun loop {i, j : nat | i <= n; i <= j; j <= n + 1}
                 .<n - i, n + 1 - j>.
                 (i : size_t i, j : size_t j) :<cloref1> void =
          if i = n then
            ()
          else if j = succ n then
            loop (succ i, succ i)
          else
            let
              val w = slice (v, i, j)
              val () = assertloc (length w = j - i)
              val () = compare_subvectors (v, i, w, i2sz 0, j - i)
              val w = slice_exn (v, i, j)
              val () = assertloc (length w = j - i)
              val () = compare_subvectors (v, i, w, i2sz 0, j - i)
              val w = slice_append (u, v, i, j)
              val () = assertloc (length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_subvectors (v, i, w, n, j - i)
              val w = slice_append_exn (u, v, i, j)
              val () = assertloc (length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_subvectors (v, i, w, n, j - i)
            in
              loop (i, succ j)
            end
        val () = loop (i2sz 0, i2sz 0)
      }

    val () =
      {
        val n = node_size * node_size + 5 * node_size + 1
        val u = pushes_many (nil, n, negate)
        fun loop {i : nat} (i : size_t i) :<cloref1> void =
          if i <= length u then
            let
              val v = slice (u, i2sz 0, i)
              val w = reverse v
              val () = compare_reverse (v, w)
            in
              loop (succ i)
            end
        val () = loop (i2sz 0)
      }

    val () =
      {
        val n = node_size * node_size + 5 * node_size + 1
        val u = push_many (nil, 0, n, negate)
        val v = push_many (nil, 0, n, double)
        val w = reverse_append (nil, u)
        val () = assertloc (length w = length u)
        val () = compare_reverse (u, w)
        val w = reverse_append (u, nil)
        val () = assertloc (length w = length u)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, length u)
        val w = reverse_append (u, v)
        val () = assertloc (length w = length u + length v)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, length u)
        val w1 = slice (w, length u, length w)
        val () = compare_reverse (v, w1)
      }

    val () =
      {
        val n = 17 * node_size + 1
        val [n : int] n = g1ofg0 n
        val () = assertloc (0 < n)
        val u = push_many (nil, 0, n, negate)
        val v = push_many (nil, 0, n, double)
        val n = i2sz n
        fun loop {i, j : nat | i <= n; i <= j; j <= n + 1}
                 .<n - i, n + 1 - j>.
                 (i : size_t i, j : size_t j) :<cloref1> void =
          if i = n then
            ()
          else if j = succ n then
            loop (succ i, succ i)
          else
            let
              val w = slice_reverse (v, i, j)
              val () = assertloc (length w = j - i)
              val () = compare_reverse (slice (v, i, j), w)
              val w = slice_reverse_exn (v, i, j)
              val () = assertloc (length w = j - i)
              val () = compare_reverse (slice (v, i, j), w)
              val w = slice_reverse_append (u, v, i, j)
              val () = assertloc (length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_reverse (slice (v, i, j),
                                        slice (w, n, n + (j - i)))
              val w = slice_reverse_append_exn (u, v, i, j)
              val () = assertloc (length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_reverse (slice (v, i, j),
                                        slice (w, n, n + (j - i)))
            in
              loop (i, succ j)
            end
        val () = loop (i2sz 0, i2sz 0)
      }
  in
  end
