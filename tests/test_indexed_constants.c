#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <sortsmill/core.h>

#define MY_ONE_PLUS_INITIALIZER(i) (1 + (i))

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, int, one_plus,
                       MY_ONE_PLUS_INITIALIZER);

int
main (int argc, char **argv)
{
  for (size_t i = 1; i < argc; i++)
    printf ("%d ", one_plus (atoi (argv[i])));
  return 0;
}
