# -*- coding: utf-8 -*-

# Copyright (C) 2013 Khaled Hosny and Barry Schwartz
#
# This file is part of Sorts Mill Core Library.
# 
# Sorts Mill Core Library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill Core Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

m4_defun([check_utf8],
[
AT_SETUP([$1 @{:@locale @S|@{utf8_locale}, codeset UTF-8@:}@])
AT_KEYWORDS([xunistr xunistring libunistring])
AT_SKIP_IF([test -z "${utf8_locale}"])
AT_CHECK_UNQUOTED([LC_ALL="${utf8_locale}" LANG="${utf8_locale}" "${abs_builddir}"/test_$1],
                  [$2],[$3],[$4])
AT_CLEANUP
])

check_utf8([xunistr 'Eĥoŝanĝo ĉiuĵaŭde' 0],[0],
[Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
(NULL)
(NULL)
(NULL)






],[])

check_utf8([xunistr 'Eĥoŝanĝo ĉiuĵaŭde' 1],[0],
[Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
(NULL)
(NULL)
(NULL)
E
E
E
E
E
E
],[])

check_utf8([xunistr 'Eĥoŝanĝo ĉiuĵaŭde' 16],[0],
[Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
(NULL)
(NULL)
(NULL)
Eĥoŝanĝo ĉiuĵaŭd
Eĥoŝanĝo ĉiuĵaŭd
Eĥoŝanĝo ĉiuĵaŭd
Eĥoŝanĝo ĉiuĵaŭd
Eĥoŝanĝo ĉiuĵaŭd
Eĥoŝanĝo ĉiuĵaŭd
],[])

m4_foreach_w([n],[17 18 29],
[
check_utf8([xunistr 'Eĥoŝanĝo ĉiuĵaŭde' n],[0],
[Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
(NULL)
(NULL)
(NULL)
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
Eĥoŝanĝo ĉiuĵaŭde
],[])
])

check_utf8([xunistr_strtol_NULL "${abs_srcdir}"/test_xunistr_strtol.txt],
           [0],[ignore],[])

check_utf8([xunistr_strtol_nonNULL "${abs_srcdir}"/test_xunistr_strtol.txt],
           [0],[ignore],[])

check_utf8([xunistr_strtoul_NULL "${abs_srcdir}"/test_xunistr_strtol.txt],
           [0],[ignore],[])

check_utf8([xunistr_strtoul_nonNULL "${abs_srcdir}"/test_xunistr_strtol.txt],
           [0],[ignore],[])

check_utf8([xunistr_strtod_NULL "${abs_srcdir}"/test_xunistr_strtod.txt],
           [0],[ignore],[])

check_utf8([xunistr_strtod_nonNULL "${abs_srcdir}"/test_xunistr_strtod.txt],
           [0],[ignore],[])

# A valid UTF-8 string.
check_utf8([xunistr_valid_prefix "Eĥoŝanĝo ĉiuĵaŭde" 255 8],
           [0],[Eĥoŝanĝo ĉiuĵaŭde|Eĥoŝanĝo ĉiuĵaŭde|Eĥoŝanĝo ĉiuĵaŭde|],[])

# An invalid \xFF 'character'.
check_utf8([xunistr_valid_prefix "Eĥoŝanĝo ĉiuĵ#aŭde" 255 8],
           [0],[Eĥoŝanĝo ĉiuĵ|Eĥoŝanĝo ĉiuĵ|Eĥoŝanĝo ĉiuĵ|],[])

# A ĵ character truncated after the first of its two bytes.
check_utf8([xunistr_valid_prefix "Eĥoŝanĝo ĉiu##aŭde" 196 0 8],
           [0],[Eĥoŝanĝo ĉiu|Eĥoŝanĝo ĉiu|Eĥoŝanĝo ĉiu|],[])

# A valid UTF-16 string.
check_utf8([xunistr_valid_prefix "#E#-#o##" 0 0 0 0 0 16],
           [0],[E-o|E-o|E-o|],[])

# A \xDC00 appearing illegally in a UTF-16 string.
# (It should appear only as a trailing surrogate.)
check_utf8([xunistr_valid_prefix "#E#-####" 0 0 220 0 0 0 16],
           [0],[E-|E-|E-|],[])

# Truncation after a UTF-16 \xD800 lead surrogate.
check_utf8([xunistr_valid_prefix "#E#-####" 0 0 216 0 0 0 16],
           [0],[E-|E-|E-|],[])

# A valid UTF-32 string.
check_utf8([xunistr_valid_prefix "###E###-###o####" 0 0 0 0 0 0 0 0 0 0 0 0 0 32],
           [0],[E-o|E-o|E-o|],[])

# A UCS-4 string that is invalid as UTF-32, due to a \x110000
# character.
check_utf8([xunistr_valid_prefix "###E###-########" 0 0 0 0 0 0 0 17 0 0 0 0 0 0 32],
           [0],[E-|E-|E-|],[])

# A valid UTF-8 string.
check_utf8([xunistr_get_next "Eĥoŝanĝo ĉiuĵaŭde" 8],[0],[Eĥoŝanĝo ĉiuĵaŭde|0],[])

# An invalid UTF-8 string (\xFF 'character').
check_utf8([xunistr_get_next "Eĥoŝanĝo ĉiuĵ#aŭde" 255 8],[0],[Eĥoŝanĝo ĉiuĵ|-1],[])

# Another invalid UTF-8 string (truncated ĵ character).
check_utf8([xunistr_get_next "Eĥoŝanĝo ĉiu##aŭde" 196 0 8],[0],[Eĥoŝanĝo ĉiu|-1],[])

# A valid UTF-16 string.
check_utf8([xunistr_get_next "#E#-#o##" 0 0 0 0 0 16],[0],[E-o|0],[])

# A \xDC00 appearing illegally in a UTF-16 string.
# (It should appear only as a trailing surrogate.)
check_utf8([xunistr_get_next "#E#-####" 0 0 220 0 0 0 16],[0],[E-|-1],[])

# Truncation after a UTF-16 \xD800 lead surrogate.
check_utf8([xunistr_get_next "#E#-####" 0 0 216 0 0 0 16],[0],[E-|-1],[])

# A valid UTF-32 string.
check_utf8([xunistr_get_next "###E###-###o####" 0 0 0 0 0 0 0 0 0 0 0 0 0 32],
           [0],[E-o|0],[])

# A UCS-4 string that is invalid as UTF-32, due to a \x110000
# character.
check_utf8([xunistr_get_next "###E###-########" 0 0 0 0 0 0 0 17 0 0 0 0 0 0 32],
           [0],[E-|-1],[])

check_utf8([x_gc_uNN_strjoin 8],[0],[],[])
check_utf8([x_gc_uNN_strjoin One 8],[0],[One],[])
check_utf8([x_gc_uNN_strjoin Two strings 8],[0],[Twostrings],[])
check_utf8([x_gc_uNN_strjoin One more string 8],[0],[Onemorestring],[])
check_utf8([x_gc_uNN_strjoin Four strings in all 8],[0],[Fourstringsinall],[])

check_utf8([x_gc_uNN_strjoin 16],[0],[],[])
check_utf8([x_gc_uNN_strjoin One 16],[0],[One],[])
check_utf8([x_gc_uNN_strjoin Two strings 16],[0],[Twostrings],[])
check_utf8([x_gc_uNN_strjoin One more string 16],[0],[Onemorestring],[])
check_utf8([x_gc_uNN_strjoin Four strings in all 16],[0],[Fourstringsinall],[])

check_utf8([x_gc_uNN_strjoin 32],[0],[],[])
check_utf8([x_gc_uNN_strjoin One 32],[0],[One],[])
check_utf8([x_gc_uNN_strjoin Two strings 32],[0],[Twostrings],[])
check_utf8([x_gc_uNN_strjoin One more string 32],[0],[Onemorestring],[])
check_utf8([x_gc_uNN_strjoin Four strings in all 32],[0],[Fourstringsinall],[])

check_utf8([uNN_to_uMM_boundaries 0 1],[0],[],[])
check_utf8([uNN_to_uMM_boundaries 1 1000],[0],[],[])
check_utf8([uNN_to_uMM_boundaries 1000 1000],[0],[],[])
check_utf8([uNN_to_uMM_boundaries 2000 100],[0],[],[])
# The followings test sometimes was producing warnings
# from the garbage collector that caused failure:
#check_utf8([uNN_to_uMM_boundaries 10000 100],[0],[],[])
