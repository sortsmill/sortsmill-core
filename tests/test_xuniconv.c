#include <config.h>

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <gc.h>
#include <locale.h>
#include <sortsmill/core.h>
#include <unistdio.h>

int
main (int argc, char **argv)
{
  GC_INIT ();

  setlocale (LC_ALL, "");

  char *s = argv[1];
  uint8_t *s_8 = x_gc_u8_strconv_from_locale (s);
  uint16_t *s_16 = x_gc_u16_strconv_from_locale (s);
  uint32_t *s_32 = x_gc_u32_strconv_from_locale (s);

  fprintf (stdout, "%s\n", s);
  ulc_fprintf (stdout, "%U\n", s_8);
  ulc_fprintf (stdout, "%lU\n", s_16);
  ulc_fprintf (stdout, "%llU\n", s_32);
  fprintf (stdout, "%s\n", x_gc_u8_strconv_to_locale (s_8));
  fprintf (stdout, "%s\n", x_gc_u16_strconv_to_locale (s_16));
  fprintf (stdout, "%s\n", x_gc_u32_strconv_to_locale (s_32));

  return 0;
}
