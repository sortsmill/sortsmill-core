#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sortsmill/core.h>
#include <float.h>
#include <math.h>

static void
mpz_matrix_init (size_t m, size_t n, mpz_t A[m][n])
{
  for (size_t i = 0; i < m; i++)
    for (size_t j = 0; j < n; j++)
      mpz_init (A[i][j]);
}

static void
mpz_matrix_clear (size_t m, size_t n, mpz_t A[m][n])
{
  for (size_t i = 0; i < m; i++)
    for (size_t j = 0; j < n; j++)
      mpz_clear (A[i][j]);
}

static void
mpq_matrix_init (size_t m, size_t n, mpq_t A[m][n])
{
  for (size_t i = 0; i < m; i++)
    for (size_t j = 0; j < n; j++)
      mpq_init (A[i][j]);
}

static void
mpq_matrix_clear (size_t m, size_t n, mpq_t A[m][n])
{
  for (size_t i = 0; i < m; i++)
    for (size_t j = 0; j < n; j++)
      mpq_clear (A[i][j]);
}

int
main (int argc, char **argv)
{
  int exit_status = 0;

  const size_t m = 100;

  mpz_t P[m][m];
  mpq_t Q[m][m];

  mpz_matrix_init (m, m, P);
  mpq_matrix_init (m, m, Q);

  // Compute Pascal’s triangle.
  mpz_set_ui (P[0][0], 1);
  for (size_t i = 1; i < m; i++)
    {
      mpz_set_ui (P[i][0], 1);
      mpz_set_ui (P[i][i], 1);
      for (size_t j = 1; j < m; j++)
        mpz_add (P[i][j], P[i - 1][j - 1], P[i - 1][j]);
    }

  // Compare the results of bincoef() against Pascal’s triangle.
  for (size_t n = 0; n <= 34; n++)
    for (size_t k = 0; k <= 34; k++)
      if (bincoef (n, k) != mpz_get_ui (P[n][k]))
        exit_status = 10;

  if (exit_status == 0)
    // Check for n < k.
    for (size_t n = 0; n <= 34; n++)
      if (bincoef (n, n + 1) != 0)
        exit_status = 20;

  if (exit_status == 0)
    {
      mpz_t C;
      mpz_init (C);

      // Compare the results of mpz_bincoef_ui() against Pascal’s
      // triangle.
      for (size_t n = 0; n <= m - 1; n++)
        for (size_t k = 0; k <= m - 1; k++)
          {
            mpz_bincoef_ui (C, n, k);
            if (mpz_cmp (C, P[n][k]) != 0)
              exit_status = 110;
          }

      mpz_clear (C);
    }

  if (exit_status == 0)
    {
      mpz_t C;
      mpz_init (C);

      // Check for n < k.
      for (size_t n = 0; n <= m - 1; n++)
        {
          mpz_bincoef_ui (C, n, n + 1);
          if (mpz_sgn (C) != 0)
            exit_status = 120;
        }

      mpz_clear (C);
    }

  if (exit_status == 0)
    {
      mpq_t C;
      mpq_init (C);

      // Compare the results of mpq_bincoef_ui() against Pascal’s
      // triangle.
      for (size_t n = 0; n <= m - 1; n++)
        for (size_t k = 0; k <= m - 1; k++)
          {
            mpq_bincoef_ui (C, n, k);
            if (mpz_cmp (mpq_numref (C), P[n][k]) != 0
                || mpz_cmp_ui (mpq_denref (C), 1) != 0)
              exit_status = 210;
          }

      mpq_clear (C);
    }

  if (exit_status == 0)
    {
      mpq_t C;
      mpq_init (C);

      // Check for n < k.
      for (size_t n = 0; n <= m - 1; n++)
        {
          mpq_bincoef_ui (C, n, n + 1);
          if (mpq_sgn (C) != 0)
            exit_status = 220;
        }

      mpq_clear (C);
    }

  mpz_matrix_clear (m, m, P);
  mpq_matrix_clear (m, m, Q);

  return exit_status;
}
