#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sortsmill/core.h>

#define str_to_str smcore_str_to_str
#define str_to_str_t smcore_str_to_str_t
#define str_to_str_cursor_t smcore_str_to_str_cursor_t

const char *associations[][2] = {
  { "mother", "parent" },
  { "father", "parent" },
  { "maternal grandmother", "grandparent" },
  { "maternal grandfather", "grandparent" },
  { "paternal grandmother", "grandparent" },
  { "paternal grandfather", "grandparent" }
};

#define NUM_ASSOC (sizeof (associations) / (2 * sizeof (const char *)))

static void
test_module (void)
{
  assert (str_to_str.hashf == smcore_uint32_strhash);

  str_to_str_t map1 = str_to_str.make ();
  str_to_str_t map2 = str_to_str.make ();
  assert (str_to_str.is_nil (map1));
  assert (str_to_str.is_nil (map2));
  assert (str_to_str.size (map1) == 0);
  assert (str_to_str.size (map2) == 0);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = str_to_str.set (map1, associations[i][0], associations[i][1]);
      map2 = str_to_str.set (map2, associations[j][0], associations[j][1]);
    }
  assert (!str_to_str.is_nil (map1));
  assert (!str_to_str.is_nil (map2));
  assert (str_to_str.size (map1) == NUM_ASSOC);
  assert (str_to_str.size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (str_to_str.has_key (map1, associations[i][0]));
      assert (str_to_str.has_key (map2, associations[i][0]));

      const char *v1a = str_to_str.get (map1, associations[i][0]);
      const char *v2a = str_to_str.get (map2, associations[i][0]);

      bool found1;
      const char *v1b;
      str_to_str.get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      const char *v2b;
      str_to_str.get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  // Reassign all the entries.
  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = str_to_str.set (map1, associations[j][0], associations[j][1]);
      map2 = str_to_str.set (map2, associations[i][0], associations[i][1]);
    }
  assert (!str_to_str.is_nil (map1));
  assert (!str_to_str.is_nil (map2));
  assert (str_to_str.size (map1) == NUM_ASSOC);
  assert (str_to_str.size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (str_to_str.has_key (map1, associations[i][0]));
      assert (str_to_str.has_key (map2, associations[i][0]));

      const char *v1a = str_to_str.get (map1, associations[i][0]);
      const char *v2a = str_to_str.get (map2, associations[i][0]);

      bool found1;
      const char *v1b;
      str_to_str.get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      const char *v2b;
      str_to_str.get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  {
    size_t n = 0;
    for (str_to_str_cursor_t cursor = str_to_str.first (map1);
         cursor != NULL; cursor = str_to_str.next (cursor))
      {
        const char *key = str_to_str.key (cursor);
        const char *value = str_to_str.value (cursor);
        const char *v = str_to_str.get (map1, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        const char *vv;
        str_to_str.pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (str_to_str.equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  {
    size_t n = 0;
    for (str_to_str_cursor_t cursor = str_to_str.first (map2);
         cursor != NULL; cursor = str_to_str.next (cursor))
      {
        const char *key = str_to_str.key (cursor);
        const char *value = str_to_str.value (cursor);
        const char *v = str_to_str.get (map2, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        const char *vv;
        str_to_str.pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (str_to_str.equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  for (str_to_str_cursor_t cursor = str_to_str.first (map1);
       cursor != NULL; cursor = str_to_str.next (cursor))
    {
      const char *key = str_to_str.key (cursor);
      assert (str_to_str.has_key (map1, key));
      map1 = str_to_str.remove (map1, key);
      bool found;
      const char *v;
      str_to_str.get_eff (map1, key, &found, &v);
      assert (!str_to_str.has_key (map1, key));
      assert (!found);
    }
  assert (str_to_str.is_nil (map1));
  assert (str_to_str.size (map1) == 0);

  for (str_to_str_cursor_t cursor = str_to_str.first (map2);
       cursor != NULL; cursor = str_to_str.next (cursor))
    {
      const char *key = str_to_str.key (cursor);
      assert (str_to_str.has_key (map2, key));
      map2 = str_to_str.remove (map2, key);
      bool found;
      const char *v;
      str_to_str.get_eff (map2, key, &found, &v);
      assert (!str_to_str.has_key (map2, key));
      assert (!found);
    }
  assert (str_to_str.is_nil (map2));
  assert (str_to_str.size (map2) == 0);

  {
    char key[100];
    char value[100];
    const size_t n = 100000;
    str_to_str_t map = str_to_str.make ();
    for (size_t i = 0; i < n; i++)
      {
        sprintf (key, "key=%zu", i);
        sprintf (value, "value=%zu", i);
        map = str_to_str.set (map, key, value);
      }
    assert (!str_to_str.is_nil (map));
    assert (str_to_str.size (map) == n);
    for (size_t i = 0; i < n; i++)
      {
        sprintf (key, "key=%zu", i);
        sprintf (value, "value=%zu", i);
        assert (strcmp (str_to_str.get (map, key), value) == 0);
      }
  }
}

static void
test_module_pointer (void)
{
  const smcore_str_to_str_module_t *s2s = &smcore_str_to_str;

  assert (s2s->hashf == smcore_uint32_strhash);

  smcore_str_to_str_t map1 = s2s->make ();
  smcore_str_to_str_t map2 = s2s->make ();
  assert (s2s->is_nil (map1));
  assert (s2s->is_nil (map2));
  assert (s2s->size (map1) == 0);
  assert (s2s->size (map2) == 0);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = s2s->set (map1, associations[i][0], associations[i][1]);
      map2 = s2s->set (map2, associations[j][0], associations[j][1]);
    }
  assert (!s2s->is_nil (map1));
  assert (!s2s->is_nil (map2));
  assert (s2s->size (map1) == NUM_ASSOC);
  assert (s2s->size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (s2s->has_key (map1, associations[i][0]));
      assert (s2s->has_key (map2, associations[i][0]));

      const char *v1a = s2s->get (map1, associations[i][0]);
      const char *v2a = s2s->get (map2, associations[i][0]);

      bool found1;
      const char *v1b;
      s2s->get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      const char *v2b;
      s2s->get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  // Reassign all the entries.
  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = s2s->set (map1, associations[j][0], associations[j][1]);
      map2 = s2s->set (map2, associations[i][0], associations[i][1]);
    }
  assert (!s2s->is_nil (map1));
  assert (!s2s->is_nil (map2));
  assert (s2s->size (map1) == NUM_ASSOC);
  assert (s2s->size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (s2s->has_key (map1, associations[i][0]));
      assert (s2s->has_key (map2, associations[i][0]));

      const char *v1a = s2s->get (map1, associations[i][0]);
      const char *v2a = s2s->get (map2, associations[i][0]);

      bool found1;
      const char *v1b;
      s2s->get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      const char *v2b;
      s2s->get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  {
    size_t n = 0;
    for (smcore_str_to_str_cursor_t cursor = s2s->first (map1);
         cursor != NULL; cursor = s2s->next (cursor))
      {
        const char *key = s2s->key (cursor);
        const char *value = s2s->value (cursor);
        const char *v = s2s->get (map1, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        const char *vv;
        s2s->pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (s2s->equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  {
    size_t n = 0;
    for (smcore_str_to_str_cursor_t cursor = s2s->first (map2);
         cursor != NULL; cursor = s2s->next (cursor))
      {
        const char *key = s2s->key (cursor);
        const char *value = s2s->value (cursor);
        const char *v = s2s->get (map2, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        const char *vv;
        s2s->pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (s2s->equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  for (smcore_str_to_str_cursor_t cursor = s2s->first (map1);
       cursor != NULL; cursor = s2s->next (cursor))
    {
      const char *key = s2s->key (cursor);
      assert (s2s->has_key (map1, key));
      map1 = s2s->remove (map1, key);
      bool found;
      const char *v;
      s2s->get_eff (map1, key, &found, &v);
      assert (!s2s->has_key (map1, key));
      assert (!found);
    }
  assert (s2s->is_nil (map1));
  assert (s2s->size (map1) == 0);

  for (smcore_str_to_str_cursor_t cursor = s2s->first (map2);
       cursor != NULL; cursor = s2s->next (cursor))
    {
      const char *key = s2s->key (cursor);
      assert (s2s->has_key (map2, key));
      map2 = s2s->remove (map2, key);
      bool found;
      const char *v;
      s2s->get_eff (map2, key, &found, &v);
      assert (!s2s->has_key (map2, key));
      assert (!found);
    }
  assert (s2s->is_nil (map2));
  assert (s2s->size (map2) == 0);

  {
    char key[100];
    char value[100];
    const size_t n = 100000;
    smcore_str_to_str_t map = s2s->make ();
    for (size_t i = 0; i < n; i++)
      {
        sprintf (key, "key=%zu", i);
        sprintf (value, "value=%zu", i);
        map = s2s->set (map, key, value);
      }
    assert (!s2s->is_nil (map));
    assert (s2s->size (map) == n);
    for (size_t i = 0; i < n; i++)
      {
        sprintf (key, "key=%zu", i);
        sprintf (value, "value=%zu", i);
        assert (strcmp (s2s->get (map, key), value) == 0);
      }
  }
}

static void
test_module_copy (void)
{
  const smcore_str_to_str_module_t s2s = smcore_str_to_str;

  assert (s2s.hashf == smcore_uint32_strhash);

  smcore_str_to_str_t map1 = s2s.make ();
  smcore_str_to_str_t map2 = s2s.make ();
  assert (s2s.is_nil (map1));
  assert (s2s.is_nil (map2));
  assert (s2s.size (map1) == 0);
  assert (s2s.size (map2) == 0);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = s2s.set (map1, associations[i][0], associations[i][1]);
      map2 = s2s.set (map2, associations[j][0], associations[j][1]);
    }
  assert (!s2s.is_nil (map1));
  assert (!s2s.is_nil (map2));
  assert (s2s.size (map1) == NUM_ASSOC);
  assert (s2s.size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (s2s.has_key (map1, associations[i][0]));
      assert (s2s.has_key (map2, associations[i][0]));

      const char *v1a = s2s.get (map1, associations[i][0]);
      const char *v2a = s2s.get (map2, associations[i][0]);

      bool found1;
      const char *v1b;
      s2s.get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      const char *v2b;
      s2s.get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  // Reassign all the entries.
  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = s2s.set (map1, associations[j][0], associations[j][1]);
      map2 = s2s.set (map2, associations[i][0], associations[i][1]);
    }
  assert (!s2s.is_nil (map1));
  assert (!s2s.is_nil (map2));
  assert (s2s.size (map1) == NUM_ASSOC);
  assert (s2s.size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (s2s.has_key (map1, associations[i][0]));
      assert (s2s.has_key (map2, associations[i][0]));

      const char *v1a = s2s.get (map1, associations[i][0]);
      const char *v2a = s2s.get (map2, associations[i][0]);

      bool found1;
      const char *v1b;
      s2s.get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      const char *v2b;
      s2s.get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  {
    size_t n = 0;
    for (smcore_str_to_str_cursor_t cursor = s2s.first (map1);
         cursor != NULL; cursor = s2s.next (cursor))
      {
        const char *key = s2s.key (cursor);
        const char *value = s2s.value (cursor);
        const char *v = s2s.get (map1, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        const char *vv;
        s2s.pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (s2s.equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  {
    size_t n = 0;
    for (smcore_str_to_str_cursor_t cursor = s2s.first (map2);
         cursor != NULL; cursor = s2s.next (cursor))
      {
        const char *key = s2s.key (cursor);
        const char *value = s2s.value (cursor);
        const char *v = s2s.get (map2, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        const char *vv;
        s2s.pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (s2s.equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  for (smcore_str_to_str_cursor_t cursor = s2s.first (map1);
       cursor != NULL; cursor = s2s.next (cursor))
    {
      const char *key = s2s.key (cursor);
      assert (s2s.has_key (map1, key));
      map1 = s2s.remove (map1, key);
      bool found;
      const char *v;
      s2s.get_eff (map1, key, &found, &v);
      assert (!s2s.has_key (map1, key));
      assert (!found);
    }
  assert (s2s.is_nil (map1));
  assert (s2s.size (map1) == 0);

  for (smcore_str_to_str_cursor_t cursor = s2s.first (map2);
       cursor != NULL; cursor = s2s.next (cursor))
    {
      const char *key = s2s.key (cursor);
      assert (s2s.has_key (map2, key));
      map2 = s2s.remove (map2, key);
      bool found;
      const char *v;
      s2s.get_eff (map2, key, &found, &v);
      assert (!s2s.has_key (map2, key));
      assert (!found);
    }
  assert (s2s.is_nil (map2));
  assert (s2s.size (map2) == 0);

  {
    char key[100];
    char value[100];
    const size_t n = 100000;
    smcore_str_to_str_t map = s2s.make ();
    for (size_t i = 0; i < n; i++)
      {
        sprintf (key, "key=%zu", i);
        sprintf (value, "value=%zu", i);
        map = s2s.set (map, key, value);
      }
    assert (!s2s.is_nil (map));
    assert (s2s.size (map) == n);
    for (size_t i = 0; i < n; i++)
      {
        sprintf (key, "key=%zu", i);
        sprintf (value, "value=%zu", i);
        assert (strcmp (s2s.get (map, key), value) == 0);
      }
  }
}

int
main (int argc, char **argv)
{
  test_module ();
  test_module_pointer ();
  test_module_copy ();

  return 0;
}
