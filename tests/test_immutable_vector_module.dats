// Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
#include "sortsmill/core/HATS/staload.hats"

staload UN = "prelude/SATS/unsafe.sats"
staload STDLIB = "libats/libc/SATS/stdlib.sats"

val node_size = 32

typedef t = Int
typedef vector (n : int, p : addr) = immutable_vector (t, n, p)
typedef vector (n : int) = [p : addr] vector (n, p)
typedef vector = [n : int] vector n
typedef entry_ref (q : addr) = immutable_vector_entry_ref (t, q)
typedef entry_ref = [q : addr] entry_ref q

val iv = make_immutable_vector_module<t> ()

typedef i2i = (Int -<cloref> Int)
val negate = (lam i => ~i) : i2i
val times10 = (lam i => i * 10) : i2i
val double = (lam i => i + i) : i2i

fn {t : t@ype}
shuffle_array {n : nat} (n : size_t n, a : arrayref (t, n)) : void =
  // See https://en.wikipedia.org/w/index.php?title=Fisher%E2%80%93Yates_shuffle&oldid=683105835
  let
    fun loop {i : nat | i < n} (i : size_t i) :<cloref1> void =
      if i2sz 0 < i then
        let
          val () = assertloc (i < n)
          val j =
            $UN.cast {Size_t} ($STDLIB.random () mod ($UN.cast {lint} (succ i)))
          val () = assertloc (j < n)
          val x = a[i]
          val () = a[i] := a[j]
          val () = a[j] := x
        in
          loop (pred i)
        end
  in
    if i2sz 0 < n then
      loop (pred n)
  end

fun
check_many {n, i, j : nat | i <= j; j <= n} .<j - i>.
           (v : vector n, i : int i, j : int j, f : i2i) : void =
  if i < j then
    let
      val x = f i
    in
      assertloc (iv.get (v, i2sz i) = x);
      assertloc (iv.get_exn (v, i2sz i) = x);
      check_many (v, succ i, j, f)
    end

fn
check_next_many {n : pos} {p : addr}
                (v : vector (n, p)) : void =
  let
    fun loop {i : nat | i <= n} {q : addr} .<n - i>.
             (pf : IMMUTABLE_VECTOR_ENTRY_REF (p, i, q) |
              i : size_t i, entry_ref : entry_ref q) :<cloref1> void =
      let
        val n = iv.length v
      in
        if i < n then
          let
            val x1 = iv.entry_ref_get (entry_ref) : t
            val x2 = iv.get (v, i) : t
            val () = assertloc (x1 = x2)
            val (pf | entry_ref) =
              iv.entry_ref_next (pf | v, i, entry_ref)
          in
            loop (pf | succ i, entry_ref)
          end
        else
          assertloc (iv.entry_ref_is_nil entry_ref)
      end

    val (pf | entry_ref) = iv.entry_ref (v, i2sz 0)
  in
    loop (pf | i2sz 0, entry_ref)
  end

fn
check_prev_many {n : pos} {p : addr}
                (v : vector (n, p)) : void =
  let
    fun loop {i : nat | i < n} {q : addr} .<i>.
             (pf : IMMUTABLE_VECTOR_ENTRY_REF (p, i, q) |
              i : size_t i, entry_ref : entry_ref q) :<cloref1> void =
      let
        val x1 = iv.entry_ref_get (entry_ref) : t
        val x2 = iv.get (v, i) : t
        val () = assertloc (x1 = x2)
        val (pf | entry_ref) =
          iv.entry_ref_prev (pf | v, i, entry_ref)
      in
        if i2sz 0 < i then
          loop (pf | pred i, entry_ref)
        else
          assertloc (iv.entry_ref_is_nil entry_ref)
      end

    val n = iv.length v
    val (pf | entry_ref) = iv.entry_ref (v, pred n)
  in
    loop (pf | pred n, entry_ref)
  end

fun
push_many {n, i, j : nat | i <= j} .<j - i>.
          (v : vector n, i : int i, j : int j, f : i2i) :
    vector (n + j - i) =
  if i < j then
    push_many (iv.push (v, f i), succ i, j, f)
  else
    v

fn
pushes_many {n, j : nat | 0 <= j}
            (v : vector n, j : int j, f : i2i) :
    vector (n + j) =
  let
    val a = arrayref_tabulate_cloref (i2sz j, lam i => f (sz2i i))
  in
    iv.pushes_arrayref (v, i2sz j, a)
  end

fn
set_many {n, i, j : nat | i <= j}
         (v : vector n, i : int i, j : int j, f : i2i) :
    vector n =
  let
    // Set the entries in a random order.
    val a = arrayref_tabulate_cloref<Int> (i2sz (j - i), lam k => sz2i k)
    val () = shuffle_array (i2sz (j - i), a)

    fun loop {k : nat | k <= (j - i)} .<(j - i) - k>.
             (w : vector n, k : int k) :<cloref1> vector n =
      if k < (j - i) then
        let
          val m = i + a[k]
          val () = assertloc (0 <= m)
          val () = assertloc (i2sz m < iv.length v)
        in
          loop (iv.set (w, i2sz m, f m), succ k)
        end
      else
        w
  in
    loop (v, 0)
  end

fn
set_exn_many {n, i, j : nat | i <= j}
             (v : vector n, i : int i, j : int j, f : i2i) :
    vector n =
  let
    // Set the entries in a random order.
    val a = arrayref_tabulate_cloref<Int> (i2sz (j - i), lam k => sz2i k)
    val () = shuffle_array (i2sz (j - i), a)

    fun loop {k : nat | k <= (j - i)} .<(j - i) - k>.
             (w : vector n, k : int k) :<cloref1> vector n =
      if k < (j - i) then
        let
          val m = i + a[k]
          val () = assertloc (0 <= m)
          val () = assertloc (i2sz m < iv.length v)
        in
          loop (iv.set_exn (w, i2sz m, f m), succ k)
        end
      else
        w
  in
    loop (v, 0)
  end

fn
check_sets_and_gets {n : nat} (n : int n, f : i2i, g : i2i) : void =
  let
    val v = push_many (iv.nil, 0, n, f)

    fun loop {i : nat} (i : size_t i) :<cloref1> void =
      if sz2i i <= n then
        let
          fun inner_loop {m : nat | m <= n - i + 1} (m : size_t m) :<cloref1>
              void =
            if sz2i (i + m) <= n then
              let
                val a = arrayref_tabulate_cloref (m, lam k => g (sz2i (i + k)))
                val w = iv.sets_arrayref (v, i, m, a)
                val h =
                  (lam k =>
                     if k < sz2i i then
                       f k
                     else if k < sz2i (i + m) then
                       g k
                     else
                       f k) : i2i
                val () = check_many (w, 0, n, h)
                val a1 = iv.gets_arrayref (w, i, m)
                val a2 = iv.gets_arrayref_exn (w, i, m)
                fun compare {j : nat | j <= m} (j : size_t j) :<cloref1> void =
                  if j < m then
                    begin
                      assertloc (a1[j] = a[j]);
                      assertloc (a2[j] = a[j]);
                    end
                val () = compare (i2sz 0)
              in
                inner_loop (succ m)
              end
          val () = inner_loop (i2sz 0)
        in
          if i <= i2sz (3 * node_size) then
            loop (succ i)
          else if sz2i i = n then
            loop (i2sz (succ n))
          else
            loop (i2sz n)
        end
  in
    loop (i2sz 0)
  end

fun
pop_many {n : nat} .<n>.
         (v : vector n, n : int n, f : i2i) : void =
  if i2sz 0 < n then
    let
      val v = iv.pop v
      val () = check_many (v, 0, pred n, f)
    in
      pop_many (v, pred n, f)
    end

fun
pop_exn_many (v : vector, f : i2i) : void =
  let
    val n = iv.length v
  in
    if i2sz 0 < n then
      let
        val v = iv.pop_exn v
        val n = pred n
        val () = assertloc (iv.length v = n)
        val () = check_many (v, 0, sz2i n, f)
      in
        pop_exn_many (v, f)
      end
  end

fun
pops_many {n : nat} {m : nat | m <= n} .<m>.
          (v : vector n, m : int m, f : i2i) : void =
  let
    val n = $UN.cast {int n} (iv.length v)
    val v1 = iv.pops (v, i2sz m)
    val () = check_many (v1, 0, n - m, f)
    val v2 = iv.pops (v1, i2sz (n - m))
    val () = assertloc (iv.is_nil v2)
  in
    if i2sz 0 < m then
      pops_many (v, pred m, f)
  end

fun
pops_exn_many {n : nat} {m : nat | m <= n} .<m>.
              (v : vector n, m : int m, f : i2i) : void =
  let
    val n = $UN.cast {int n} (iv.length v)
    val v1 = iv.pops_exn (v, i2sz m)
    val () = check_many (v1, 0, n - m, f)
    val v2 = iv.pops_exn (v1, i2sz (n - m))
    val () = assertloc (iv.is_nil v2)
  in
    if i2sz 0 < m then
      pops_exn_many (v, pred m, f)
  end

fun
compare_subvectors {i, j, n : nat} .<n>.
                   (u : immutable_vector t, i : size_t i,
                    v : immutable_vector t, j : size_t j,
                    n : size_t n) : void =
  if i2sz 0 < n then
    let
      val () = assertloc (iv.get_exn (u, i) = iv.get_exn (v, j))
    in
      compare_subvectors (u, succ i, v, succ j, pred n)
    end

fn
compare_reverse {n : nat}
                (u : immutable_vector (t, n),
                 v : immutable_vector (t, n)) : void =
  let
    val () = assertloc (iv.length u = iv.length v)
    fun loop {i : nat | i <= n} .<n - i>. (i : size_t i) :<cloref1> void =
      if i < iv.length u then
        let
          val () =
            assertloc
              (iv.get_exn (u, i) = iv.get_exn (v, pred (iv.length v) - i))
        in
          loop (succ i)
        end
  in
    loop (i2sz 0)
  end

implement
main0 () =
  let
    val () = assertloc (ptr_is_null ($UN.cast {ptr} iv.nil))
    val () = assertloc (iv.is_nil ($UN.cast {immutable_vector t} the_null_ptr))

    val v = iv.nil
    val () = assertloc (iv.is_nil v)
    val () = assertloc (iv.length v = i2sz 0)

    val n = node_size * node_size * node_size + 5 * node_size + 1
    val v = push_many (iv.nil, 0, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val v = set_many (v, 0, n, times10)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, times10)
    val v = set_exn_many (v, 0, n, double)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, double)
    val () = check_next_many v
    val () = check_prev_many v

    val n = 32
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 33
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 64
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 65
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 1024
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 1025
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 2080
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val n = 2081
    val v = pushes_many (iv.nil, n, negate)
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n)
    val () = check_many (v, 0, n, negate)
    val v = pushes_many (v, n, lam i => g1ofg0 ~((g0ofg1 i) mod n))
    val () = assertloc (not (iv.is_nil v))
    val () = assertloc (iv.length v = i2sz n + i2sz n)
    val () = check_many (v, 0, n + n, lam i => g1ofg0 ~((g0ofg1 i) mod n))

    val n = node_size * node_size + 5 * node_size + 1
    val v = push_many (iv.nil, 0, n, negate)
    val () = check_many (v, 0, n, negate)
    val () = pop_many (v, n, negate)
    val () = pop_exn_many (v, negate)
    val () = pops_many (v, n, negate)
    val () = pops_exn_many (v, n, negate)

    val n = node_size * node_size + 5 * node_size + 1
    val () = check_sets_and_gets (n, negate, double)

    val () =
      {
        val n = node_size * node_size + 5 * node_size + 1
        val u = push_many (iv.nil, 0, n, negate)
        val v = push_many (iv.nil, 0, n, double)
        val w = iv.append (iv.nil, u)
        val () = assertloc (iv.length w = iv.length u)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, iv.length u)
        val w = iv.append (u, iv.nil)
        val () = assertloc (iv.length w = iv.length u)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, iv.length u)
        val w = iv.append (u, v)
        val () = assertloc (iv.length w = iv.length u + iv.length v)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, iv.length u)
        val () = compare_subvectors (v, i2sz 0, w, iv.length u, iv.length v)
      }

    val () =
      {
        val n = 17 * node_size + 1
        val [n : int] n = g1ofg0 n
        val () = assertloc (0 < n)
        val u = push_many (iv.nil, 0, n, negate)
        val v = push_many (iv.nil, 0, n, double)
        val n = i2sz n
        fun loop {i, j : nat | i <= n; i <= j; j <= n + 1}
                 .<n - i, n + 1 - j>.
                 (i : size_t i, j : size_t j) :<cloref1> void =
          if i = n then
            ()
          else if j = succ n then
            loop (succ i, succ i)
          else
            let
              val w = iv.slice (v, i, j)
              val () = assertloc (iv.length w = j - i)
              val () = compare_subvectors (v, i, w, i2sz 0, j - i)
              val w = iv.slice_exn (v, i, j)
              val () = assertloc (iv.length w = j - i)
              val () = compare_subvectors (v, i, w, i2sz 0, j - i)
              val w = iv.slice_append (u, v, i, j)
              val () = assertloc (iv.length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_subvectors (v, i, w, n, j - i)
              val w = iv.slice_append_exn (u, v, i, j)
              val () = assertloc (iv.length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_subvectors (v, i, w, n, j - i)
            in
              loop (i, succ j)
            end
        val () = loop (i2sz 0, i2sz 0)
      }

    val () =
      {
        val n = node_size * node_size + 5 * node_size + 1
        val u = pushes_many (iv.nil, n, negate)
        fun loop {i : nat} (i : size_t i) :<cloref1> void =
          if i <= iv.length u then
            let
              val v = iv.slice (u, i2sz 0, i)
              val w = iv.reverse v
              val () = compare_reverse (v, w)
            in
              loop (succ i)
            end
        val () = loop (i2sz 0)
      }

    val () =
      {
        val n = node_size * node_size + 5 * node_size + 1
        val u = push_many (iv.nil, 0, n, negate)
        val v = push_many (iv.nil, 0, n, double)
        val w = iv.reverse_append (iv.nil, u)
        val () = assertloc (iv.length w = iv.length u)
        val () = compare_reverse (u, w)
        val w = iv.reverse_append (u, iv.nil)
        val () = assertloc (iv.length w = iv.length u)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, iv.length u)
        val w = iv.reverse_append (u, v)
        val () = assertloc (iv.length w = iv.length u + iv.length v)
        val () = compare_subvectors (u, i2sz 0, w, i2sz 0, iv.length u)
        val w1 = iv.slice (w, iv.length u, iv.length w)
        val () = compare_reverse (v, w1)
      }

    val () =
      {
        val n = 17 * node_size + 1
        val [n : int] n = g1ofg0 n
        val () = assertloc (0 < n)
        val u = push_many (iv.nil, 0, n, negate)
        val v = push_many (iv.nil, 0, n, double)
        val n = i2sz n
        fun loop {i, j : nat | i <= n; i <= j; j <= n + 1}
                 .<n - i, n + 1 - j>.
                 (i : size_t i, j : size_t j) :<cloref1> void =
          if i = n then
            ()
          else if j = succ n then
            loop (succ i, succ i)
          else
            let
              val w = iv.slice_reverse (v, i, j)
              val () = assertloc (iv.length w = j - i)
              val () = compare_reverse (iv.slice (v, i, j), w)
              val w = iv.slice_reverse_exn (v, i, j)
              val () = assertloc (iv.length w = j - i)
              val () = compare_reverse (iv.slice (v, i, j), w)
              val w = iv.slice_reverse_append (u, v, i, j)
              val () = assertloc (iv.length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_reverse (iv.slice (v, i, j),
                                        iv.slice (w, n, n + (j - i)))
              val w = iv.slice_reverse_append_exn (u, v, i, j)
              val () = assertloc (iv.length w = n + j - i)
              val () = compare_subvectors (u, i2sz 0, w, i2sz 0, n)
              val () = compare_reverse (iv.slice (v, i, j),
                                        iv.slice (w, n, n + (j - i)))
            in
              loop (i, succ j)
            end
        val () = loop (i2sz 0, i2sz 0)
      }
  in
  end
