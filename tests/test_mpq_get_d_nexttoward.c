#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <float.h>
#include <sortsmill/core.h>

#define assertion_check(x)                      \
  do                                            \
    {                                           \
      if (!(x))                                 \
        {                                       \
          fprintf (stderr, "Failed: " #x);      \
          exit (1);                             \
        }                                       \
    }                                           \
  while (false)

static void
check_toward (const mpq_t x, const mpq_t s, const mpq_t y)
{
  const int cmp_xy = mpq_cmp (x, y);
  if (cmp_xy < 0)
    assertion_check (mpq_cmp (x, s) < 0);       // x < s
  else if (cmp_xy == 0)
    assertion_check (mpq_equal (s, y)); // x == s
  else
    assertion_check (0 < mpq_cmp (x, s));       // s < x
}

static void
check_near (const mpq_t x, double s, const mpq_t sq)
{
  const int cmp_xs = mpq_cmp (x, sq);
  if (cmp_xs < 0)
    {
      mpq_t tmp;
      mpq_init (tmp);
      mpq_set_d (tmp, nextafter (s, -HUGE_VAL));
      assertion_check (mpq_cmp (tmp, x) <= 0);
      mpq_clear (tmp);
    }
  else if (0 < cmp_xs)
    {
      mpq_t tmp;
      mpq_init (tmp);
      mpq_set_d (tmp, nextafter (s, HUGE_VAL));
      assertion_check (0 <= mpq_cmp (tmp, x));
      mpq_clear (tmp);
    }
}

int
main (int argc, char **argv)
{
  mpq_t x;
  mpq_t y;
  mpq_t sq;
  mpq_inits (x, y, sq, NULL);

  mpq_set_str (x, argv[1], 10);
  mpq_canonicalize (x);
  mpq_set_str (y, argv[2], 10);
  mpq_canonicalize (y);

  const double s = mpq_get_d_nexttoward (x, y);
  mpq_init (sq);
  mpq_set_d (sq, s);

  assertion_check (mpq_equal (x, y) || !mpq_equal (x, sq));
  check_toward (x, sq, y);
  check_near (x, s, sq);

  mpq_clears (x, y, sq, NULL);

  return 0;
}
