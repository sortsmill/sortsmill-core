#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <sortsmill/core.h>

#define str_to_uint32 smcore_str_to_uint32
#define str_to_uint32_t smcore_str_to_uint32_t
#define str_to_uint32_cursor_t smcore_str_to_uint32_cursor_t

//
// FIXME: This is a very quick and incomplete test.
//

int
main (int argc, char **argv)
{
  assert (str_to_uint32.hashf == smcore_uint32_strhash);

  char key[100];
  const size_t n = 100000;
  str_to_uint32_t map = str_to_uint32.make ();
  for (size_t i = 0; i < n; i++)
    {
      sprintf (key, "key=%zu", i);
      map = str_to_uint32.set (map, key, smcore_uint32_strhash (key));
    }
  assert (!str_to_uint32.is_nil (map));
  assert (str_to_uint32.size (map) == n);
  for (size_t i = 0; i < n; i++)
    {
      sprintf (key, "key=%zu", i);
      assert (str_to_uint32.get (map, key) == smcore_uint32_strhash (key));
    }

  //  uintptr_to_uintptr_map_t values = NULL;
  //  for (size_t i = 0; i < n; i++)
  //    {
  //      sprintf (key, "key=%zu", i);
  //      values =
  //        uintptr_to_uintptr_map_set (values, str_to_uint32.get (map, key), 0);
  //    }
  //  printf ("collisions: %zu\n", n - uintptr_to_uintptr_map_size (values));

  return 0;
}
