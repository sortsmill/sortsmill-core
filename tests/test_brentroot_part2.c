#include <config.h>

// Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include <sortsmill/core.h>

//
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME: Add more test
// cases. Carefully choose them to exercise branches.
//

static long double
f (long double x, void *_unused STM_MAYBE_UNUSED)
{
  return sinl (x);
}

static long double
g (long double x, void *_unused STM_MAYBE_UNUSED)
{
  return cosl (x) - (x * x * x);
}

int
main (int argc, char **argv)
{
  long double root;
  int info;

  lbrentroot (1e-8L, 4, f, NULL, -1, -1, &root, &info);
  if (info != 0)
    {
      printf("info = %d\n", info);
      exit (1);
    }
  if (1e-8L < root - 4.0L * atanl (1.0L))
    {
      printf("root - 4.0L * atanl (1.0L) = %Le\n", root - 4.0L * atanl (1.0L));
      exit (2);
    }

  return 0;
}
