#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (int argc, char **argv)
{
  int dest_row_stride = atoi (argv[1]);
  int dest_col_stride = atoi (argv[2]);
  int dest_offset = atoi (argv[3]);
  int src_row_stride = atoi (argv[4]);
  int src_col_stride = atoi (argv[5]);
  int src_offset = atoi (argv[6]);
  int row_count = atoi (argv[7]);
  int col_count = atoi (argv[8]);
  const char *src_data = argv[9];

  const size_t n = strlen (src_data);

  char dest_data[n + 1];
  for (size_t i = 0; i < n; i++)
    dest_data[i] = ' ';
  dest_data[n] = '\0';

  COPY2_WITH_STRIDES (char, dest_data + dest_offset, src_data + src_offset,
                      dest_row_stride, src_row_stride, row_count,
                      dest_col_stride, src_col_stride, col_count);

  printf ("|%s|", dest_data);

  return 0;
}
