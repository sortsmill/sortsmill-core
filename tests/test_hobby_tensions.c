#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <math.h>
#include <float.h>
#include <sortsmill/core.h>

#define TOLERANCE (1000 * DBL_EPSILON)

int
main (int argc, char **argv)
{
  int retval = 0;

  const double p0x = atof (argv[0]);
  const double p0y = atof (argv[1]);
  const double p1x = atof (argv[2]);
  const double p1y = atof (argv[3]);
  const double p2x = atof (argv[4]);
  const double p2y = atof (argv[5]);
  const double p3x = atof (argv[6]);
  const double p3y = atof (argv[7]);

  const double complex p0 = p0x + p0y * I;
  const double complex p1 = p1x + p1y * I;
  const double complex p2 = p2x + p2y * I;
  const double complex p3 = p3x + p3y * I;

  double tension0;
  double tension3;
  control_points_to_hobby_tensions (p0, p1, p2, p3, &tension0, &tension3);

  double complex q1;
  double complex q2;
  hobby_tensions_to_control_points (false, p0, p1 - p0, tension0, tension3,
                                    p3 - p2, p3, &q1, &q2);

  printf ("tension0 = %g\n", tension0);
  printf ("tension3 = %g\n", tension3);
  printf ("cabs (p1 - q1) = %g\n", cabs (p1 - q1));
  printf ("cabs (p2 - q2) = %g\n", cabs (p2 - q2));

  if (TOLERANCE < cabs (p1 - q1))
    retval |= 1;
  if (TOLERANCE < cabs (p2 - q2))
    retval |= 2;

  return retval;
}
