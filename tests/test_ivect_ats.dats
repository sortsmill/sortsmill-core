// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
#include "sortsmill/core/HATS/staload.hats"

staload UN = "prelude/SATS/unsafe.sats"

typedef boxed_tuple = '(int, string)
val can_we_make_bt_ivect = make_immutable_vector_module_of_boxed<boxed_tuple> ()

typedef unboxed_tuple = @(int, string)
val can_we_make_ut_ivect = make_immutable_vector_module<unboxed_tuple> ()

val can_we_make_u8ivect = make_immutable_vector_module<uint8> ()
val can_we_make_u16ivect = make_immutable_vector_module<uint16> ()
val can_we_make_u32ivect = make_immutable_vector_module<uint32> ()
val can_we_make_u64ivect = make_immutable_vector_module<uint64> ()
val can_we_make_s8ivect = make_immutable_vector_module<int8> ()
val can_we_make_s16ivect = make_immutable_vector_module<int16> ()
val can_we_make_s32ivect = make_immutable_vector_module<int32> ()
val can_we_make_s64ivect = make_immutable_vector_module<int64> ()
val can_we_make_f32ivect = make_immutable_vector_module<float> ()
val can_we_make_f64ivect = make_immutable_vector_module<double> ()

fn test_vect_boxed () : void =
  let
    val vect = make_immutable_vector_module_of_boxed<boxed_tuple> ()

    val v = vect.nil
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val v = vect.push (v, '(1, "unu"))
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 1)

    val v = vect.push (v, '(2, "du"))
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 2)

    val v = vect.push (v, '(3, "tri"))
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)

    val () = assertloc ((vect.get (v, i2sz 0)).0 = 1)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 2)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 3)

    val v = vect.set (v, i2sz 0, '(10, "dek"))
    val v = vect.set (v, i2sz 1, '(20, "dudek"))
    val v = vect.set (v, i2sz 2, '(30, "tridek"))

    val () = assertloc ((vect.get (v, i2sz 0)).0 = 10)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 20)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 30)

    val () =
      {
        val v = vect.pop v
        val () = assertloc (not (vect.is_nil v))
        val () = assertloc (vect.length v = i2sz 2)

        val v = vect.pop v
        val () = assertloc (not (vect.is_nil v))
        val () = assertloc (vect.length v = i2sz 1)

        val v = vect.pop v
        val () = assertloc (vect.is_nil v)
        val () = assertloc (vect.length v = i2sz 0)
      }

    val v = vect.pops (v, i2sz 0)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)

    val v = vect.pops (v, i2sz 3)
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val a = arrayref ($arrpsz {boxed_tuple} ('(101, "cent unu"),
                                             '(201, "ducent unu"),
                                             '(301, "tricent unu")))
    val v = vect.sets (v, i2sz 0, i2sz 0, a)
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val a = arrayref ($arrpsz {boxed_tuple} ('(100, "cent"),
                                             '(200, "ducent"),
                                             '(300, "tricent")))
    val v = vect.pushes (v, i2sz 3, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 100)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 200)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 300)

    val a = arrayref ($arrpsz {boxed_tuple} ('(201, "ducent unu"),
                                             '(301, "tricent unu")))
    val v = vect.sets (v, i2sz 1, i2sz 2, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 100)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 201)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 301)

    val a = arrayref ($arrpsz {boxed_tuple} ('(101, "cent unu")))
    val v = vect.sets (v, i2sz 0, i2sz 1, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 101)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 201)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 301)

    val a = arrayref ($arrpsz {boxed_tuple} ('(101, "cent unu"),
                                             '(201, "ducent unu"),
                                             '(301, "tricent unu")))
    val v = vect.sets (v, i2sz 0, i2sz 3, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 101)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 201)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 301)
    val () =
      let
        val a0 = vect.gets (v, i2sz 0, i2sz 0)
        val a1 = vect.gets (v, i2sz 0, i2sz 1)
        val a2 = vect.gets (v, i2sz 0, i2sz 2)
        val a3 = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a1[0].0 = 101);
        assertloc (a2[0].0 = 101);
        assertloc (a2[1].0 = 201);
        assertloc (a3[0].0 = 101);
        assertloc (a3[1].0 = 201);
        assertloc (a3[2].0 = 301);
      end
    val () =
      let
        val a0 = arrayptr_make_uninitized<boxed_tuple> (i2sz 0)
        val a1 = arrayptr_make_uninitized<boxed_tuple> (i2sz 1)
        val a2 = arrayptr_make_uninitized<boxed_tuple> (i2sz 2)
        val a3 = arrayptr_make_uninitized<boxed_tuple> (i2sz 3)
      in
        vect.gets_eff (v, i2sz 0, i2sz 0, a0);
        vect.gets_eff (v, i2sz 0, i2sz 1, a1);
        vect.gets_eff (v, i2sz 0, i2sz 2, a2);
        vect.gets_eff (v, i2sz 0, i2sz 3, a3);
        assertloc (a1[0].0 = 101);
        assertloc (a2[0].0 = 101);
        assertloc (a2[1].0 = 201);
        assertloc (a3[0].0 = 101);
        assertloc (a3[1].0 = 201);
        assertloc (a3[2].0 = 301);
        arrayptr_free a0;
        arrayptr_free a1;
        arrayptr_free a2;
        arrayptr_free a3
      end
    fun test_slice (i : Size_t, j : Size_t) : void =
      let
        val n = 3
      in
        if i <= n then
          let
            val slice = vect.slice (v, i, j)
            fun compare (u : Size_t) :<cloref1> void =
              if u < j - i then
                let
                  val () =
                    assertloc ((vect.get (v, i + u)).0 = (vect.get (slice, u)).0)
                in
                  compare (succ u)
                end
            val () = compare (i2sz 0)
          in
            if j < n then
              test_slice (i, succ j)
            else
              test_slice (succ i, succ i)
          end
      end
    val () = test_slice (i2sz 0, i2sz 0)
    fun test_append (i : Size_t) : void =
      let
        val n = i2sz 3
      in
        if i < n then
          let
            val slice1 = vect.slice (v, i2sz 0, i)
            val slice2 = vect.slice (v, i, n)
            val w = vect.append (slice1, slice2)
            fun compare (u : Size_t) :<cloref1> void =
              if u < n then
                let
                  val () =
                    assertloc ((vect.get (v, u)).0 = (vect.get (w, u)).0)
                in
                  compare (succ u)
                end
            val () = compare (i2sz 0)
          in
            test_append (succ i)
          end
      end
    val () = test_append (i2sz 0)
  in
  end

fn test_vect_unboxed () : void =
  let
    val vect = make_immutable_vector_module<unboxed_tuple> ()

    val v = vect.nil
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val v = vect.push (v, @(1, "unu"))
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 1)

    val v = vect.push (v, @(2, "du"))
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 2)

    val v = vect.push (v, @(3, "tri"))
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)

    val () = assertloc ((vect.get (v, i2sz 0)).0 = 1)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 2)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 3)

    val v = vect.set (v, i2sz 0, @(10, "dek"))
    val v = vect.set (v, i2sz 1, @(20, "dudek"))
    val v = vect.set (v, i2sz 2, @(30, "tridek"))

    val () = assertloc ((vect.get (v, i2sz 0)).0 = 10)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 20)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 30)

    val () =
      {
        val v = vect.pop v
        val () = assertloc (not (vect.is_nil v))
        val () = assertloc (vect.length v = i2sz 2)

        val v = vect.pop v
        val () = assertloc (not (vect.is_nil v))
        val () = assertloc (vect.length v = i2sz 1)

        val v = vect.pop v
        val () = assertloc (vect.is_nil v)
        val () = assertloc (vect.length v = i2sz 0)
      }

    val v = vect.pops (v, i2sz 0)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)

    val v = vect.pops (v, i2sz 3)
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val a = arrayref ($arrpsz {unboxed_tuple} (@(101, "cent unu"),
                                               @(201, "ducent unu"),
                                               @(301, "tricent unu")))
    val v = vect.sets (v, i2sz 0, i2sz 0, a)
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val a = arrayref ($arrpsz {unboxed_tuple} (@(100, "cent"),
                                               @(200, "ducent"),
                                               @(300, "tricent")))
    val v = vect.pushes (v, i2sz 3, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 100)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 200)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 300)

    val a = arrayref ($arrpsz {unboxed_tuple} (@(201, "ducent unu"),
                                               @(301, "tricent unu")))
    val v = vect.sets (v, i2sz 1, i2sz 2, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 100)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 201)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 301)

    val a = arrayref ($arrpsz {unboxed_tuple} (@(101, "cent unu")))
    val v = vect.sets (v, i2sz 0, i2sz 1, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 101)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 201)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 301)
    val () =
      let
        val a0 = vect.gets (v, i2sz 0, i2sz 0)
        val a1 = vect.gets (v, i2sz 0, i2sz 1)
        val a2 = vect.gets (v, i2sz 0, i2sz 2)
        val a3 = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a1[0].0 = 101);
        assertloc (a2[0].0 = 101);
        assertloc (a2[1].0 = 201);
        assertloc (a3[0].0 = 101);
        assertloc (a3[1].0 = 201);
        assertloc (a3[2].0 = 301);
      end
    val () =
      let
        val a0 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 0)
        val a1 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 1)
        val a2 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 2)
        val a3 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 3)
      in
        vect.gets_eff (v, i2sz 0, i2sz 0, a0);
        vect.gets_eff (v, i2sz 0, i2sz 1, a1);
        vect.gets_eff (v, i2sz 0, i2sz 2, a2);
        vect.gets_eff (v, i2sz 0, i2sz 3, a3);
        assertloc (a1[0].0 = 101);
        assertloc (a2[0].0 = 101);
        assertloc (a2[1].0 = 201);
        assertloc (a3[0].0 = 101);
        assertloc (a3[1].0 = 201);
        assertloc (a3[2].0 = 301);
        arrayptr_free a0;
        arrayptr_free a1;
        arrayptr_free a2;
        arrayptr_free a3
      end

    val a = arrayref ($arrpsz {unboxed_tuple} (@(101, "cent unu"),
                                               @(201, "ducent unu"),
                                               @(301, "tricent unu")))
    val v = vect.sets (v, i2sz 0, i2sz 3, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc ((vect.get (v, i2sz 0)).0 = 101)
    val () = assertloc ((vect.get (v, i2sz 1)).0 = 201)
    val () = assertloc ((vect.get (v, i2sz 2)).0 = 301)
    val () =
      let
        val a0 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 0)
        val a1 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 1)
        val a2 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 2)
        val a3 = arrayptr_make_uninitized<unboxed_tuple> (i2sz 3)
      in
        vect.gets_eff (v, i2sz 0, i2sz 0, a0);
        vect.gets_eff (v, i2sz 0, i2sz 1, a1);
        vect.gets_eff (v, i2sz 0, i2sz 2, a2);
        vect.gets_eff (v, i2sz 0, i2sz 3, a3);
        assertloc (a1[0].0 = 101);
        assertloc (a2[0].0 = 101);
        assertloc (a2[1].0 = 201);
        assertloc (a3[0].0 = 101);
        assertloc (a3[1].0 = 201);
        assertloc (a3[2].0 = 301);
        arrayptr_free a0;
        arrayptr_free a1;
        arrayptr_free a2;
        arrayptr_free a3
      end
    fun test_slice (i : Size_t, j : Size_t) : void =
      let
        val n = 3
      in
        if i <= n then
          let
            val slice = vect.slice (v, i, j)
            fun compare (u : Size_t) :<cloref1> void =
              if u < j - i then
                let
                  val () =
                    assertloc ((vect.get (v, i + u)).0 = (vect.get (slice, u)).0)
                in
                  compare (succ u)
                end
            val () = compare (i2sz 0)
          in
            if j < n then
              test_slice (i, succ j)
            else
              test_slice (succ i, succ i)
          end
      end
    val () = test_slice (i2sz 0, i2sz 0)
    fun test_append (i : Size_t) : void =
      let
        val n = i2sz 3
      in
        if i < n then
          let
            val slice1 = vect.slice (v, i2sz 0, i)
            val slice2 = vect.slice (v, i, n)
            val w = vect.append (slice1, slice2)
            fun compare (u : Size_t) :<cloref1> void =
              if u < n then
                let
                  val () =
                    assertloc ((vect.get (v, u)).0 = (vect.get (w, u)).0)
                in
                  compare (succ u)
                end
            val () = compare (i2sz 0)
          in
            test_append (succ i)
          end
      end
    val () = test_append (i2sz 0)

    val () =
      // Test gets_eff with a big vector, because the implementation
      // in this case is block-by-block and so we should test with
      // multiple blocks.
      let
        #define LENGTH 5000
        val length = i2sz LENGTH

        typedef t = unboxed_tuple

        val a = arrayref_tabulate_cloref<t> (length, lam i => @(sz2i i, ""))
        val v = vect.pushes (vect.nil, length, a)

        val b = arrayptr_make_uninitized<t> length
        val () = vect.gets_eff (v, i2sz 0, length, b)

        val () =
          let
            val b = $UN.castvwtp1 {arrayref (t, LENGTH)} b
            fun test_entries {i : nat | i <= LENGTH} (i : size_t i) :<cloref1> void =
              if i < length then
                begin
                  assertloc (a[i].0 = b[i].0);
                  test_entries (succ i)
                end
          in
            test_entries (i2sz 0)
          end
        
        val () = arrayptr_free b
      in
      end
    val () =
      let
        #define LENGTH 5000
        val length = i2sz LENGTH

        typedef t = unboxed_tuple

        val a = arrayref_tabulate_cloref<t> (length, lam i => @(sz2i i, ""))
        val v = vect.pushes (vect.nil, length, a)

(*????????????????????????????????????????????????????????????????????????????????????????????????????????
        val () = assertloc (vect.index (vect.first v) = i2sz 0)
        val () = assertloc (vect.index (vect.last v) = pred (vect.length v))
??????????????????????????????????????????????????????????????????????????????????????????????????????????*)
      in
      end
  in
  end

extern fn {t : t@ype} test_vect$eq : (t, t) -> bool

fn {t : t@ype} test_vect () : void =
  let
    overload = with test_vect$eq

    val vect = make_immutable_vector_module<t> ()

    val v = vect.nil
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val v = vect.push (v, $UN.cast{t} 1)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 1)

    val v = vect.push (v, $UN.cast{t} 2)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 2)

    val v = vect.push (v, $UN.cast{t} 3)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)

    val () = assertloc (vect.get (v, i2sz 0) = $UN.cast{t} 1)
    val () = assertloc (vect.get (v, i2sz 1) = $UN.cast{t} 2)
    val () = assertloc (vect.get (v, i2sz 2) = $UN.cast{t} 3)
    val () =
      let
        val a = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a[0] = $UN.cast{t} 1);
        assertloc (a[1] = $UN.cast{t} 2);
        assertloc (a[2] = $UN.cast{t} 3);
      end

    val v = vect.set (v, i2sz 0, $UN.cast{t} 10)
    val v = vect.set (v, i2sz 1, $UN.cast{t} 20)
    val v = vect.set (v, i2sz 2, $UN.cast{t} 30)

    val () = assertloc (vect.get (v, i2sz 0) = $UN.cast{t} 10)
    val () = assertloc (vect.get (v, i2sz 1) = $UN.cast{t} 20)
    val () = assertloc (vect.get (v, i2sz 2) = $UN.cast{t} 30)
    val () =
      let
        val a = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a[0] = $UN.cast{t} 10);
        assertloc (a[1] = $UN.cast{t} 20);
        assertloc (a[2] = $UN.cast{t} 30);
      end

    val () =
      {
        val v = vect.pop v
        val () = assertloc (not (vect.is_nil v))
        val () = assertloc (vect.length v = i2sz 2)

        val v = vect.pop v
        val () = assertloc (not (vect.is_nil v))
        val () = assertloc (vect.length v = i2sz 1)

        val v = vect.pop v
        val () = assertloc (vect.is_nil v)
        val () = assertloc (vect.length v = i2sz 0)
      }

    val v = vect.pops (v, i2sz 0)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)

    val v = vect.pops (v, i2sz 3)
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val a = arrayref ($arrpsz {t} ($UN.cast{t} 101,
                                   $UN.cast{t} 201,
                                   $UN.cast{t} 301))
    val v = vect.sets (v, i2sz 0, i2sz 0, a)
    val () = assertloc (vect.is_nil v)
    val () = assertloc (vect.length v = i2sz 0)

    val a = arrayref ($arrpsz {t} ($UN.cast{t} 100,
                                   $UN.cast{t} 200,
                                   $UN.cast{t} 300))
    val v = vect.pushes (v, i2sz 3, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc (vect.get (v, i2sz 0) = $UN.cast{t} 100)
    val () = assertloc (vect.get (v, i2sz 1) = $UN.cast{t} 200)
    val () = assertloc (vect.get (v, i2sz 2) = $UN.cast{t} 300)
    val () =
      let
        val a = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a[0] = $UN.cast{t} 100);
        assertloc (a[1] = $UN.cast{t} 200);
        assertloc (a[2] = $UN.cast{t} 300);
      end

    val a = arrayref ($arrpsz {t} ($UN.cast{t} 201,
                                   $UN.cast{t} 301))
    val v = vect.sets (v, i2sz 1, i2sz 2, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc (vect.get (v, i2sz 0) = $UN.cast{t} 100)
    val () = assertloc (vect.get (v, i2sz 1) = $UN.cast{t} 201)
    val () = assertloc (vect.get (v, i2sz 2) = $UN.cast{t} 301)
    val () =
      let
        val a = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a[0] = $UN.cast{t} 100);
        assertloc (a[1] = $UN.cast{t} 201);
        assertloc (a[2] = $UN.cast{t} 301);
      end

    val a = arrayref ($arrpsz {t} ($UN.cast{t} 101))
    val v = vect.sets (v, i2sz 0, i2sz 1, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc (vect.get (v, i2sz 0) = $UN.cast{t} 101)
    val () = assertloc (vect.get (v, i2sz 1) = $UN.cast{t} 201)
    val () = assertloc (vect.get (v, i2sz 2) = $UN.cast{t} 301)
    val () =
      let
        val a = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a[0] = $UN.cast{t} 101);
        assertloc (a[1] = $UN.cast{t} 201);
        assertloc (a[2] = $UN.cast{t} 301);
      end

    val a = arrayref ($arrpsz {t} ($UN.cast{t} 101,
                                   $UN.cast{t} 201,
                                   $UN.cast{t} 301))
    val v = vect.sets (v, i2sz 0, i2sz 3, a)
    val () = assertloc (not (vect.is_nil v))
    val () = assertloc (vect.length v = i2sz 3)
    val () = assertloc (vect.get (v, i2sz 0) = $UN.cast{t} 101)
    val () = assertloc (vect.get (v, i2sz 1) = $UN.cast{t} 201)
    val () = assertloc (vect.get (v, i2sz 2) = $UN.cast{t} 301)
    val () =
      let
        val a0 = vect.gets (v, i2sz 0, i2sz 0)
        val a1 = vect.gets (v, i2sz 0, i2sz 1)
        val a2 = vect.gets (v, i2sz 0, i2sz 2)
        val a3 = vect.gets (v, i2sz 0, i2sz 3)
      in
        assertloc (a1[0] = $UN.cast{t} 101);
        assertloc (a2[0] = $UN.cast{t} 101);
        assertloc (a2[1] = $UN.cast{t} 201);
        assertloc (a3[0] = $UN.cast{t} 101);
        assertloc (a3[1] = $UN.cast{t} 201);
        assertloc (a3[2] = $UN.cast{t} 301);
      end
    val () =
      let
        val a0 = arrayptr_make_uninitized<t> (i2sz 0)
        val a1 = arrayptr_make_uninitized<t> (i2sz 1)
        val a2 = arrayptr_make_uninitized<t> (i2sz 2)
        val a3 = arrayptr_make_uninitized<t> (i2sz 3)
      in
        vect.gets_eff (v, i2sz 0, i2sz 0, a0);
        vect.gets_eff (v, i2sz 0, i2sz 1, a1);
        vect.gets_eff (v, i2sz 0, i2sz 2, a2);
        vect.gets_eff (v, i2sz 0, i2sz 3, a3);
        assertloc (a1[0] = $UN.cast {t} 101);
        assertloc (a2[0] = $UN.cast {t} 101);
        assertloc (a2[1] = $UN.cast {t} 201);
        assertloc (a3[0] = $UN.cast {t} 101);
        assertloc (a3[1] = $UN.cast {t} 201);
        assertloc (a3[2] = $UN.cast {t} 301);
        arrayptr_free a0;
        arrayptr_free a1;
        arrayptr_free a2;
        arrayptr_free a3
      end
    fun test_slice (i : Size_t, j : Size_t) : void =
      let
        val n = 3
      in
        if i <= n then
          let
            val slice = vect.slice (v, i, j)
            fun compare (u : Size_t) :<cloref1> void =
              if u < j - i then
                let
                  val () =
                    assertloc (vect.get (v, i + u) = vect.get (slice, u))
                in
                  compare (succ u)
                end
            val () = compare (i2sz 0)
          in
            if j < n then
              test_slice (i, succ j)
            else
              test_slice (succ i, succ i)
          end
      end
    val () = test_slice (i2sz 0, i2sz 0)
    fun test_append (i : Size_t) : void =
      let
        val n = i2sz 3
      in
        if i < n then
          let
            val slice1 = vect.slice (v, i2sz 0, i)
            val slice2 = vect.slice (v, i, n)
            val w = vect.append (slice1, slice2)
            fun compare (u : Size_t) :<cloref1> void =
              if u < n then
                let
                  val () =
                    assertloc (vect.get (v, u) = vect.get (w, u))
                in
                  compare (succ u)
                end
            val () = compare (i2sz 0)
          in
            test_append (succ i)
          end
      end
    val () = test_append (i2sz 0)

    val () =
      let
        #define LENGTH 5000
        val length = i2sz LENGTH

        val a = arrayref_tabulate_cloref<t> (length, lam i => $UN.cast {t} (succ i))
        val v = vect.pushes (vect.nil, length, a)

        val () = assertloc (vect.index (vect.first v) = i2sz 0)
        val () = assertloc (vect.index_exn (vect.first v) = i2sz 0)
        val () = assertloc (vect.value (vect.first v) = $UN.cast {t} 1)
        val () = assertloc (vect.value_exn (vect.first v) = $UN.cast {t} 1)
        val () = assertloc (vect.index (vect.last v) = pred (vect.length v))
        val () = assertloc (vect.index_exn (vect.last v) = pred (vect.length v))
        val () = assertloc (vect.value (vect.last v) = $UN.cast {t} (vect.length v))
        val () = assertloc (vect.value_exn (vect.last v) = $UN.cast {t} (vect.length v))
        fun test_find (i : Size_t) : void =
          if i < length then
            let
              val () = assertloc (vect.index (vect.find (v, i)) = i)
              val () = assertloc (vect.index_exn (vect.find (v, i)) = i)
              val () = assertloc (vect.value (vect.find (v, i)) = $UN.cast {t} (succ i))
              val () = assertloc (vect.value_exn (vect.find (v, i)) = $UN.cast {t} (succ i))
            in
              test_find (succ i)
            end
        val () = test_find (i2sz 0)
(* forever loop in patsopt, below? * )
        fun test_next (c : immutable_vector_cursor t, i : Size_t) : void =
          if not (vect.cursor_is_nil c) then
            let
              val () = assertloc (vect.index (vect.find (v, i)) = i)
              val () = assertloc (vect.index_exn (vect.find (v, i)) = i)
              val () = assertloc (vect.value (vect.find (v, i)) = $UN.cast {t} (succ i))
              val () = assertloc (vect.value_exn (vect.find (v, i)) = $UN.cast {t} (succ i))
            in
              test_next (vect.next c, succ i)
            end
        val () = test_next (vect.first v, i2sz 0)
( * forever loop in patsopt, above? *)
      in
      end
  in
  end

fn test_u8ivect () : void =
  let
    implement test_vect$eq<uint8> (a, b) = (a = b)
  in
    test_vect<uint8> ()
  end

fn test_u16ivect () : void =
  let
    implement test_vect$eq<uint16> (a, b) = (a = b)
  in
    test_vect<uint16> ()
  end

fn test_u32ivect () : void =
  let
    implement test_vect$eq<uint32> (a, b) = (a = b)
  in
    test_vect<uint32> ()
  end

fn test_u64ivect () : void =
  let
    implement test_vect$eq<uint64> (a, b) = (a = b)
  in
    test_vect<uint64> ()
  end

fn test_s8ivect () : void =
  let
    implement test_vect$eq<int8> (a, b) = (a = b)
  in
    test_vect<int8> ()
  end

fn test_s16ivect () : void =
  let
    implement test_vect$eq<int16> (a, b) = (a = b)
  in
    test_vect<int16> ()
  end

fn test_s32ivect () : void =
  let
    implement test_vect$eq<int32> (a, b) = (a = b)
  in
    test_vect<int32> ()
  end

fn test_s64ivect () : void =
  let
    implement test_vect$eq<int64> (a, b) = (a = b)
  in
    test_vect<int64> ()
  end

fn test_f32ivect () : void =
  let
    implement test_vect$eq<float> (a, b) = (a = b)
  in
    test_vect<float> ()
  end

fn test_f64ivect () : void =
  let
    implement test_vect$eq<double> (a, b) = (a = b)
  in
    test_vect<double> ()
  end

implement main0 () =
  let
  in
    test_vect_boxed ();
    test_vect_unboxed ();
    test_u8ivect ();
    test_u16ivect ();
    test_u32ivect ();
    test_u64ivect ();
    test_s8ivect ();
    test_s16ivect ();
    test_s32ivect ();
    test_s64ivect ();
    test_f32ivect ();
    test_f64ivect ();
  end
