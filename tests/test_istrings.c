#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sortsmill/core.h>

const char *test_strings[] = {
  "",
  "1",
  "12",
  "123",
  "1234",
  "12345",
  "123456",
  "1234567",
  "12345678",
  "123456789",
  "1234567890",
  "12345678901",
  "123456789012",
  "1234567890123",
  "12345678901234",
  "123456789012345",
  "1234567890123456",
  "12345678901234567",
  "123456789012345678",
  "1234567890123456789",
  "12345678901234567890",
  "12345678901234567890123456789012",
  "123456789012345678901234567890123",
  "1234567890123456789012345678901234567890",
  "Gallia est omnis divisa in partes tres, quarum unam incolunt Belgae, aliam Aquitani, tertiam qui ipsorum lingua Celtae, nostra Galli appellantur. Hi omnes lingua, institutis, legibus inter se differunt.",
  NULL
};

int
main (int argc, char **argv)
{
  for (size_t i = 0; test_strings[i] != NULL; i++)
    {
      istring_t s = str2istring (test_strings[i]);
      assert (strcmp (istring2str (s), test_strings[i]) == 0);
      assert (istring_str_cmp (s, test_strings[i]) == 0);
      assert (str_istring_cmp (test_strings[i], s) == 0);
      assert (istring_cmp (s, s) == 0);
    }
  assert (istring_cmp (str2istring ("012"), str2istring ("123")) < 0);
  assert (istring_cmp (str2istring ("012"), str2istring ("0123")) < 0);
  assert (istring_cmp (str2istring ("123"), str2istring ("012")) > 0);
  assert (istring_cmp (str2istring ("0123"), str2istring ("012")) > 0);

  assert (istring_str_cmp (strslice2istring ("0123456789", 1, 5), "1234") == 0);
  assert (strcmp (istringslice2str (str2istring ("0123456789"), 1, 5), "1234")
          == 0);

  assert (istring_str_cmp (str2istring2 ("0", "1"), "01") == 0);
  assert (istring_str_cmp (str2istring3 ("0", "1", "2"), "012") == 0);
  assert (istring_str_cmp (str2istring4 ("0", "1", "2", "3"), "0123") == 0);
  assert (istring_str_cmp (str2istring5 ("0", "1", "2", "3", "4"), "01234") ==
          0);
  assert (istring_str_cmp (str2istringn (NULL), "") == 0);
  assert (istring_str_cmp (str2istringn ("0", NULL), "0") == 0);
  assert (istring_str_cmp
          (str2istringn ("0", "1", "2", "3", "4", "5", NULL), "012345") == 0);

  {
    istring_t s = str2istring ("0");
    assert (istring_str_cmp (istring_str_append (s, "1"), "01") == 0);
    assert (istring_str_cmp (istring_str_append2 (s, "1", "2"), "012") == 0);
    assert (istring_str_cmp (istring_str_append3 (s, "1", "2", "3"), "0123") ==
            0);
    assert (istring_str_cmp
            (istring_str_append4 (s, "1", "2", "3", "4"), "01234") == 0);
    assert (istring_str_cmp
            (istring_str_append5 (s, "1", "2", "3", "4", "5"), "012345") == 0);
    assert (istring_str_cmp (istring_str_appendn (s, NULL), "0") == 0);
    assert (istring_str_cmp (istring_str_appendn (s, "1", NULL), "01") == 0);
    assert (istring_str_cmp
            (istring_str_appendn (s, "1", "2", "3", "4", "5", "6", NULL),
             "0123456") == 0);
  }

  {
    istring_t s0 = str2istring ("0");
    istring_t s1 = str2istring ("1");
    istring_t s2 = str2istring ("2");
    istring_t s3 = str2istring ("3");
    istring_t s4 = str2istring ("4");
    istring_t s5 = str2istring ("5");
    assert (istring_str_cmp (istring_concat (s0, s1), "01") == 0);
    assert (istring_str_cmp (istring_concat3 (s0, s1, s2), "012") == 0);
    assert (istring_str_cmp (istring_concat4 (s0, s1, s2, s3), "0123") == 0);
    assert (istring_str_cmp (istring_concat5 (s0, s1, s2, s3, s4), "01234") ==
            0);
    assert (istring_str_cmp (istring_concatn (0), "") == 0);
    assert (istring_str_cmp (istring_concatn (1, s0), "0") == 0);
    assert (istring_str_cmp (istring_concatn (2, s0, s1), "01") == 0);
    assert (istring_str_cmp
            (istring_concatn (6, s0, s1, s2, s3, s4, s5), "012345") == 0);
  }

  for (size_t i = 0; test_strings[i] != NULL; i++)
    {
      const size_t n = strlen (test_strings[i]);
      istring_t s = str2istring (test_strings[i]);
      assert (n == istring_length (s));
      for (size_t j = 0; j < n; j++)
        {
          assert (istring_get (s, j) == test_strings[i][j]);
          istring_t s1a = istring_set (s, j, '#');
          char *s1b = x_gc_strdup (test_strings[i]);
          s1b[j] = '#';
          assert (istring_str_cmp (s1a, s1b) == 0);
        }
    }

  for (size_t i = 0; test_strings[i] != NULL; i++)
    {
      const size_t n = strlen (test_strings[i]);
      istring_t s = str2istring (test_strings[i]);
      assert (n == istring_length (s));
      for (size_t j = 0; j <= n; j++)
        for (size_t k = j; k <= n; k++)
          {
            const char *t = x_gc_strndup (&test_strings[i][j], k - j);
            istring_t s1 = istring_slice (s, j, k);
            assert (istring_str_cmp (s1, t) == 0);
          }
    }

  for (size_t i = 0; test_strings[i] != NULL; i++)
    for (unsigned int seed = 0; seed < 100; seed += 10)
      {
        istring_t s = str2istring (test_strings[i]);
        unsigned int h1 =
          smcore_uint32_murmurhash3 (test_strings[i], strlen (test_strings[i]),
                                     seed);
        unsigned int h2 = istring_murmur3_32 (s, seed);
        assert (h1 == h2);
    }

  return 0;
}
