#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <sortsmill/core.h>
#define ip2ip_t smcore_intptr_to_intptr_t
#define ip2ip_cursor_t smcore_intptr_to_intptr_cursor_t
#define ip2ip smcore_intptr_to_intptr

//
// FIXME: This is not a very good test. It does not even check all the
// functions.
//

#define MAX2(a, b) (((a) < (b)) ? (b) : (a))
#define MAX3(a, b, c) (MAX2 (MAX2 ((a), (b)), (c)))

#define IMAX (MAX3 (RAND_MAX, LONG_MAX, INTPTR_MAX))

static intptr_t
irand (intmax_t imax)
{
  const intptr_t sign = ((random () & 1) == 0) ? -1 : 1;
  const intptr_t n = sign * (random () % imax);
  return n;
}

typedef struct
{
  intptr_t key;
  intptr_t value;
} pair_t;

#define NUMPAIRS 10000
static pair_t *pairs;

static void
make_pairs (intmax_t imax)
{
  pairs = malloc (NUMPAIRS * sizeof (pair_t));
  for (size_t i = 0; i < NUMPAIRS; i++)
    {
      pairs[i].key = irand (imax);
      pairs[i].value = irand (imax);
    }
}

static size_t
count_pairs (void)
{
  bool *marks = calloc (NUMPAIRS, sizeof (bool));
  size_t count = 0;
  for (size_t i = 0; i < NUMPAIRS; i++)
    if (!marks[i])
      {
        count++;
        for (size_t j = i; j < NUMPAIRS; j++)
          if (pairs[j].key == pairs[i].key)
            marks[j] = true;
      }
  free (marks);
  return count;
}

static ip2ip_t
fill_map (ip2ip_t map)
{
  for (size_t i = 0; i < NUMPAIRS; i++)
    map = ip2ip.set (map, pairs[i].key, pairs[i].value);
  return map;
}

static void
check_has_key_clear (ip2ip_t map)
{
  for (size_t i = 0; i < NUMPAIRS; i++)
    assert (!ip2ip.has_key (map, pairs[i].key));
}

static void
check_has_key_set (ip2ip_t map)
{
  for (size_t i = 0; i < NUMPAIRS; i++)
    assert (ip2ip.has_key (map, pairs[i].key));
}

static void
check_values (ip2ip_t map)
{
  for (ip2ip_cursor_t cursor = ip2ip.first (map);
       cursor != NULL; cursor = ip2ip.next (cursor))
    {
      intptr_t k;
      intptr_t v;
      ip2ip.pair_eff (cursor, &k, &v);
      assert (ip2ip.get (map, k) == v);
      assert (ip2ip.key (cursor) == k);
      assert (ip2ip.value (cursor) == v);

      size_t i = 0;
      while (i < NUMPAIRS && pairs[NUMPAIRS - 1 - i].key != k)
        i++;
      assert (i != NUMPAIRS);
      assert (pairs[NUMPAIRS - 1 - i].value == v);
    }
}

static void
removal_check (ip2ip_t map)
{
  const size_t orig_size = ip2ip.size (map);
  size_t n = orig_size;
  ip2ip_t m = map;
  for (ip2ip_cursor_t cursor = ip2ip.last (m);
       cursor != NULL; cursor = ip2ip.prev (cursor))
    {
      //      printf ("n = %zu\n", n);
      //      printf ("m = %p\n", m);
      //      printf ("ip2ip.size (m) = %zu\n", ip2ip.size (m));
      assert (ip2ip.size (map) == orig_size);
      assert (ip2ip.size (m) == n);
      m = ip2ip.remove (m, ip2ip.key (cursor));
      n--;
    }
  assert (ip2ip.size (map) == orig_size);
  assert (ip2ip.size (m) == 0);
}

static void
do_checks (intmax_t imax)
{
  make_pairs (IMAX);
  const size_t num_pairs = count_pairs ();

  ip2ip_t map = NULL;
  assert (ip2ip.size (map) == 0);
  check_has_key_clear (map);

  map = fill_map (map);
  assert (ip2ip.size (map) == num_pairs);
  check_has_key_set (map);

  check_values (map);
  removal_check (map);

  free (pairs);
}

int
main (int argc, char **argv)
{
  do_checks (1);
  do_checks (10);
  do_checks (50);
  do_checks (100);
  do_checks (500);
  do_checks (1000);
  do_checks (IMAX);
  return 0;
}
