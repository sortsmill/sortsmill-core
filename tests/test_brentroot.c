#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#define BRENTROOT_DEBUG stdout
#include "../dbrentroot.c"

#include <sortsmill/core.h>

//
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME: Add more test
// cases. Carefully choose them to exercise branches.
//

static double
f (double x, void *_unused STM_MAYBE_UNUSED)
{
  return sin (x);
}

static double
g (double x, void *_unused STM_MAYBE_UNUSED)
{
  return cos (x) - (x * x * x);
}

int
main (int argc, char **argv)
{
  double root;
  int info;

  dbrentroot (0.1, 4, f, NULL, -1, -1, &root, &info);
  printf (" .\n");
  dbrentroot (4, 0, g, NULL, -1, -1, &root, &info);

  return 0;
}
