#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sortsmill/core.h>

#define str_to_voidp smcore_str_to_voidp
#define str_to_voidp_t smcore_str_to_voidp_t
#define str_to_voidp_cursor_t smcore_str_to_voidp_cursor_t

char *associations[][2] = {
  { "mother", "parent" },
  { "father", "parent" },
  { "maternal grandmother", "grandparent" },
  { "maternal grandfather", "grandparent" },
  { "paternal grandmother", "grandparent" },
  { "paternal grandfather", "grandparent" }
};

#define NUM_ASSOC (sizeof (associations) / (2 * sizeof (const char *)))

int
main (int argc, char **argv)
{
  assert (str_to_voidp.hashf == smcore_uint32_strhash);

  str_to_voidp_t map1 = str_to_voidp.make ();
  str_to_voidp_t map2 = str_to_voidp.make ();
  assert (str_to_voidp.is_nil (map1));
  assert (str_to_voidp.is_nil (map2));
  assert (str_to_voidp.size (map1) == 0);
  assert (str_to_voidp.size (map2) == 0);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = str_to_voidp.set (map1, associations[i][0], associations[i][1]);
      map2 = str_to_voidp.set (map2, associations[j][0], associations[j][1]);
    }
  assert (!str_to_voidp.is_nil (map1));
  assert (!str_to_voidp.is_nil (map2));
  assert (str_to_voidp.size (map1) == NUM_ASSOC);
  assert (str_to_voidp.size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (str_to_voidp.has_key (map1, associations[i][0]));
      assert (str_to_voidp.has_key (map2, associations[i][0]));

      const char *v1a = str_to_voidp.get (map1, associations[i][0]);
      const char *v2a = str_to_voidp.get (map2, associations[i][0]);

      bool found1;
      void *v1b;
      str_to_voidp.get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      void *v2b;
      str_to_voidp.get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  // Reassign all the entries.
  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      const size_t j = NUM_ASSOC - 1 - i;
      map1 = str_to_voidp.set (map1, associations[j][0], associations[j][1]);
      map2 = str_to_voidp.set (map2, associations[i][0], associations[i][1]);
    }
  assert (!str_to_voidp.is_nil (map1));
  assert (!str_to_voidp.is_nil (map2));
  assert (str_to_voidp.size (map1) == NUM_ASSOC);
  assert (str_to_voidp.size (map2) == NUM_ASSOC);

  for (size_t i = 0; i < NUM_ASSOC; i++)
    {
      assert (str_to_voidp.has_key (map1, associations[i][0]));
      assert (str_to_voidp.has_key (map2, associations[i][0]));

      const char *v1a = str_to_voidp.get (map1, associations[i][0]);
      const char *v2a = str_to_voidp.get (map2, associations[i][0]);

      bool found1;
      void *v1b;
      str_to_voidp.get_eff (map1, associations[i][0], &found1, &v1b);

      bool found2;
      void *v2b;
      str_to_voidp.get_eff (map2, associations[i][0], &found2, &v2b);

      assert (found1);
      assert (found2);
      assert (strcmp (v1a, v1b) == 0);
      assert (strcmp (v2a, v2b) == 0);
      assert (strcmp (v1a, v2a) == 0);
      assert (strcmp (v1a, associations[i][1]) == 0);
    }

  {
    size_t n = 0;
    for (str_to_voidp_cursor_t cursor = str_to_voidp.first (map1);
         cursor != NULL; cursor = str_to_voidp.next (cursor))
      {
        const char *key = str_to_voidp.key (cursor);
        const char *value = str_to_voidp.value (cursor);
        const char *v = str_to_voidp.get (map1, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        void *vv;
        str_to_voidp.pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (str_to_voidp.equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  {
    size_t n = 0;
    for (str_to_voidp_cursor_t cursor = str_to_voidp.first (map2);
         cursor != NULL; cursor = str_to_voidp.next (cursor))
      {
        const char *key = str_to_voidp.key (cursor);
        const char *value = str_to_voidp.value (cursor);
        const char *v = str_to_voidp.get (map2, key);
        assert (strcmp (value, v) == 0);

        const char *kk;
        void *vv;
        str_to_voidp.pair_eff (cursor, &kk, &vv);
        assert (strcmp (kk, key) == 0);
        assert (strcmp (vv, value) == 0);
        assert (str_to_voidp.equalf (kk, key));

        n++;
      }
    assert (n == NUM_ASSOC);
  }

  for (str_to_voidp_cursor_t cursor = str_to_voidp.first (map1);
       cursor != NULL; cursor = str_to_voidp.next (cursor))
    {
      const char *key = str_to_voidp.key (cursor);
      assert (str_to_voidp.has_key (map1, key));
      map1 = str_to_voidp.remove (map1, key);
      bool found;
      void *v;
      str_to_voidp.get_eff (map1, key, &found, &v);
      assert (!str_to_voidp.has_key (map1, key));
      assert (!found);
    }
  assert (str_to_voidp.is_nil (map1));
  assert (str_to_voidp.size (map1) == 0);

  for (str_to_voidp_cursor_t cursor = str_to_voidp.first (map2);
       cursor != NULL; cursor = str_to_voidp.next (cursor))
    {
      const char *key = str_to_voidp.key (cursor);
      assert (str_to_voidp.has_key (map2, key));
      map2 = str_to_voidp.remove (map2, key);
      bool found;
      void *v;
      str_to_voidp.get_eff (map2, key, &found, &v);
      assert (!str_to_voidp.has_key (map2, key));
      assert (!found);
    }
  assert (str_to_voidp.is_nil (map2));
  assert (str_to_voidp.size (map2) == 0);

  return 0;
}
