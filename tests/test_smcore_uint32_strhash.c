#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <sortsmill/core.h>

inline uint32_t rotl32 (uint32_t x, int8_t r);
inline uint64_t rotl64 (uint64_t x, int8_t r);
inline uint32_t getblock32 (const uint32_t *p, int i);
inline uint64_t getblock64 (const uint64_t *p, int i);
inline uint32_t fmix32 (uint32_t h);
inline uint64_t fmix64 (uint64_t k);
#include "MurmurHash3.h"
#include "MurmurHash3.cpp"

#define NUM_KEYS 1000000

// Make each key take up 32 bytes, and thus help ensure 32-bit
// alignments of the data, when running the canonical implementation
// of MurmurHash3.
#define MAX_KEY_LEN 31

typedef struct
{
  char key[MAX_KEY_LEN + 1];
  uint32_t hash;
} key_hash_pair;

static key_hash_pair kh[NUM_KEYS];

static int
hashcmp (const void *p, const void *q)
{
  const uint32_t hp = ((key_hash_pair *) p)->hash;
  const uint32_t hq = ((key_hash_pair *) q)->hash;
  int retval;
  if (hp < hq)
    retval = -1;
  else if (hp == hq)
    retval = 0;
  else
    retval = 1;
  return retval;
}

STM_MAYBE_UNUSED static void
show_collisions (void)
{
  size_t i = 0;
  while (i < NUM_KEYS - 1)
    {
      if (kh[i].hash == kh[i + 1].hash)
        {
          printf ("%s %s", kh[i].key, kh[i + 1].key);
          i++;
          while (i < NUM_KEYS - 1 && kh[i].hash == kh[i + 1].hash)
            {
              printf (" %s", kh[i + 1].key);
              i++;
            }
          printf ("\n");
        }
      else
        i++;
    }
}

static void
check_against_canonical_MurmurHash3 (void)
{
  for (size_t i = 0; i < NUM_KEYS; i++)
    {
      uint32_t hash_value;
      MurmurHash3_x86_32 (kh[i].key, strlen (kh[i].key), SMCORE_HASH_SEED,
                          &hash_value);
      //printf ("%s : %u\n", kh[i].key, (unsigned int) hash_value);
      assert (kh[i].hash == hash_value);
    }
}

int
main (int argc, char **argv)
{
  for (size_t i = 0; i < NUM_KEYS; i++)
    {
      sprintf (kh[i].key, "key=%zu", i);
      kh[i].hash = smcore_uint32_strhash (kh[i].key);
    }
  qsort (kh, NUM_KEYS, sizeof (key_hash_pair), hashcmp);

  //show_collisions ();
  check_against_canonical_MurmurHash3 ();

  return 0;
}
