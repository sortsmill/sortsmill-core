#include <config.h>

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdbool.h>
#include <sortsmill/core.h>

static const char *
truth_val (bool b)
{
  return (b ? "T" : "F");
}

int
main (int argc, char **argv)
{
  char *s = (argc < 2) ? "" : argv[1];

  printf ("%s", truth_val (is_postscript_integer (s)));
  printf ("%s", truth_val (is_postscript_real (s)));
  printf ("%s", truth_val (is_postscript_radix_number (s)));
  printf ("%s", truth_val (is_postscript_number (s)));

  return 0;
}
