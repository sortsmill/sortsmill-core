#include <config.h>

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <gc.h>
#include <locale.h>
#include <sortsmill/core.h>

int
main (int argc, char **argv)
{
  GC_INIT ();

  setlocale (LC_ALL, "");

  uint8_t *s8 = (uint8_t *) argv[1];
  uint16_t *s8_16 = x_gc_u8_to_u16 (s8);
  uint32_t *s8_32 = x_gc_u8_to_u32 (s8);
  uint8_t *s8_16_8 = x_gc_u16_to_u8 (s8_16);
  uint8_t *s8_32_8 = x_gc_u32_to_u8 (s8_32);
  uint32_t *s8_16_32 = x_gc_u16_to_u32 (s8_16);
  uint16_t *s8_32_16 = x_gc_u32_to_u16 (s8_32);

  size_t n = atoi (argv[2]);

  ulc_fprintf (stdout, "%U\n", s8);
  ulc_fprintf (stdout, "%lU\n", s8_16);
  ulc_fprintf (stdout, "%llU\n", s8_32);
  ulc_fprintf (stdout, "%U\n", s8_16_8);
  ulc_fprintf (stdout, "%U\n", s8_32_8);
  ulc_fprintf (stdout, "%llU\n", s8_16_32);
  ulc_fprintf (stdout, "%lU\n", s8_32_16);
  ulc_fprintf (stdout, "%U\n", x_gc_u8_strdup (s8));
  ulc_fprintf (stdout, "%lU\n", x_gc_u16_strdup (s8_16));
  ulc_fprintf (stdout, "%llU\n", x_gc_u32_strdup (s8_32));
  ulc_fprintf (stdout, "%U\n", x_u8_strdup_or_null (s8));
  ulc_fprintf (stdout, "%lU\n", x_u16_strdup_or_null (s8_16));
  ulc_fprintf (stdout, "%llU\n", x_u32_strdup_or_null (s8_32));
  ulc_fprintf (stdout, "%U\n", x_u8_strdup_or_null (NULL));
  ulc_fprintf (stdout, "%lU\n", x_u16_strdup_or_null (NULL));
  ulc_fprintf (stdout, "%llU\n", x_u32_strdup_or_null (NULL));
  ulc_fprintf (stdout, "%U\n", x_u8_strmbndup (s8, n));
  ulc_fprintf (stdout, "%lU\n", x_u16_strmbndup (s8_16, n));
  ulc_fprintf (stdout, "%llU\n", x_u32_strmbndup (s8_32, n));
  ulc_fprintf (stdout, "%U\n", x_gc_u8_strmbndup (s8, n));
  ulc_fprintf (stdout, "%lU\n", x_gc_u16_strmbndup (s8_16, n));
  ulc_fprintf (stdout, "%llU\n", x_gc_u32_strmbndup (s8_32, n));

  return 0;
}
