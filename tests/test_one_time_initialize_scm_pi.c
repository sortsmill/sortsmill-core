#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>
#include <libguile.h>
#include <stdio.h>

#if SMCORE_N_ENABLE_THREADS

#include <pthread.h>

static void
pthread_mutex_unlocker (void *mutex_ptr)
{
  pthread_mutex_unlock ((pthread_mutex_t *) mutex_ptr);
}

static void
scm_dynwind_pthread_mutex_unlock (void *mutex_ptr)
{
  scm_dynwind_unwind_handler (pthread_mutex_unlocker, mutex_ptr,
                              SCM_F_WIND_EXPLICITLY);
}

pthread_mutex_t scm_pi__Mutex = PTHREAD_MUTEX_INITIALIZER;

STM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, scm_pi,
                         scm_dynwind_begin (0);
                         pthread_mutex_lock (&scm_pi__Mutex);
                         scm_dynwind_pthread_mutex_unlock (&scm_pi__Mutex),
                         scm_dynwind_end (),
                         (scm_pi__Value =
                          scm_angle
                          (scm_make_rectangular (scm_from_int (-1),
                                                 scm_from_int (0)))));

#else

STM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, scm_pi,
                         /* no locking */ ,
                         /* no unlocking */ ,
                         (scm_pi__Value =
                          scm_angle
                          (scm_make_rectangular (scm_from_int (-1),
                                                 scm_from_int (0)))));

#endif

static void *
run_in_guile_mode (void *p)
{
  printf ("pi   = %lf\n", scm_to_double (scm_pi ()));
  printf ("pi/2 = %lf\n",
          scm_to_double (scm_divide (scm_pi (), scm_from_int (2))));
  printf ("pi/3 = %lf\n",
          scm_to_double (scm_divide (scm_pi (), scm_from_int (3))));
  printf ("pi/4 = %lf\n",
          scm_to_double (scm_divide (scm_pi (), scm_from_int (4))));
  return NULL;
}

int
main (int argc, char **argv)
{
  scm_with_guile (run_in_guile_mode, NULL);
  return 0;
}
