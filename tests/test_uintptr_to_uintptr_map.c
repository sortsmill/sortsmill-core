#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <sortsmill/core.h>
#define up2up_t smcore_uintptr_to_uintptr_t
#define up2up_cursor_t smcore_uintptr_to_uintptr_cursor_t
#define up2up smcore_uintptr_to_uintptr

//
// FIXME: This is not a very good test. It does not even check all the
// functions.
//

#define MAX2(a, b) (((a) < (b)) ? (b) : (a))
#define MAX3(a, b, c) (MAX2 (MAX2 ((a), (b)), (c)))

#define IMAX (MAX3 (RAND_MAX, LONG_MAX, UINTPTR_MAX))

static uintptr_t
irand (uintmax_t imax)
{
  const uintptr_t n = random () % imax;
  return n;
}

typedef struct
{
  uintptr_t key;
  uintptr_t value;
} pair_t;

#define NUMPAIRS 10000
static pair_t *pairs;

static void
make_pairs (uintmax_t imax)
{
  pairs = malloc (NUMPAIRS * sizeof (pair_t));
  for (size_t i = 0; i < NUMPAIRS; i++)
    {
      pairs[i].key = irand (imax);
      pairs[i].value = irand (imax);
    }
}

static size_t
count_pairs (void)
{
  bool *marks = calloc (NUMPAIRS, sizeof (bool));
  size_t count = 0;
  for (size_t i = 0; i < NUMPAIRS; i++)
    if (!marks[i])
      {
        count++;
        for (size_t j = i; j < NUMPAIRS; j++)
          if (pairs[j].key == pairs[i].key)
            marks[j] = true;
      }
  free (marks);
  return count;
}

static up2up_t
fill_map (up2up_t map)
{
  for (size_t i = 0; i < NUMPAIRS; i++)
    map = up2up.set (map, pairs[i].key, pairs[i].value);
  return map;
}

static void
check_has_key_clear (up2up_t map)
{
  for (size_t i = 0; i < NUMPAIRS; i++)
    assert (!up2up.has_key (map, pairs[i].key));
}

static void
check_has_key_set (up2up_t map)
{
  for (size_t i = 0; i < NUMPAIRS; i++)
    assert (up2up.has_key (map, pairs[i].key));
}

static void
check_values (up2up_t map)
{
  for (up2up_cursor_t cursor =
       up2up.first (map);
       cursor != NULL; cursor = up2up.next (cursor))
    {
      uintptr_t k;
      uintptr_t v;
      up2up.pair_eff (cursor, &k, &v);
      assert (up2up.get (map, k) == v);
      assert (up2up.key (cursor) == k);
      assert (up2up.value (cursor) == v);

      size_t i = 0;
      while (i < NUMPAIRS && pairs[NUMPAIRS - 1 - i].key != k)
        i++;
      assert (i != NUMPAIRS);
      assert (pairs[NUMPAIRS - 1 - i].value == v);
    }
}

static void
removal_check (up2up_t map)
{
  const size_t orig_size = up2up.size (map);
  size_t n = orig_size;
  up2up_t m = map;
  for (up2up_cursor_t cursor = up2up.last (m);
       cursor != NULL; cursor = up2up.prev (cursor))
    {
      assert (up2up.size (map) == orig_size);
      assert (up2up.size (m) == n);
      m =
        up2up.remove (m, up2up.key (cursor));
      n--;
    }
  assert (up2up.size (map) == orig_size);
  assert (up2up.size (m) == 0);
}

static void
do_checks (uintmax_t imax)
{
  make_pairs (IMAX);
  const size_t num_pairs = count_pairs ();

  up2up_t map = NULL;
  assert (up2up.size (map) == 0);
  check_has_key_clear (map);

  map = fill_map (map);
  assert (up2up.size (map) == num_pairs);
  check_has_key_set (map);

  check_values (map);
  removal_check (map);

  free (pairs);
}

int
main (int argc, char **argv)
{
  do_checks (1);
  do_checks (10);
  do_checks (50);
  do_checks (100);
  do_checks (500);
  do_checks (1000);
  do_checks (IMAX);
  return 0;
}
