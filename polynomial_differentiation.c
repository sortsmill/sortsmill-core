#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

VISIBLE void
mpq_poly_deriv_mono (size_t degree, ssize_t stride, mpq_t *poly,
                     ssize_t result_stride, mpq_t *result)
{
  if (degree == 0)
    mpq_set_ui (result[0], 0, 1);
  else
    {
      mpq_t tmp;
      mpq_init (tmp);

      const size_t result_degree = degree - 1;
      mpq_t *buffer = xmalloc ((result_degree + 1) * sizeof (mpq_t));
      mpq_init_poly (result_degree, buffer);

      poly += stride;
      mpq_set (buffer[0], *poly);
      for (size_t i = 2; i <= degree; i++)
        {
          mpq_set_ui (tmp, i, 1);
          poly += stride;
          mpq_mul (buffer[i - 1], tmp, *poly);
        }

      copy_mpq_with_strides (result_stride, result, 1, buffer,
                             result_degree + 1);

      mpq_clear (tmp);
      mpq_clear_poly (result_degree, buffer);
      free (buffer);
    }
}

VISIBLE void
dpoly_deriv_mono (size_t degree, ssize_t stride, const double *poly,
                  ssize_t deriv_stride, double *deriv)
{
  if (degree == 0)
    deriv[0] = 0.0;
  else
    {
      deriv[0] = poly[stride];
      for (size_t i = 2; i <= degree; i++)
        deriv[deriv_stride * (ssize_t) (i - 1)] =
          i * poly[stride * (ssize_t) i];
    }
}

VISIBLE void
dpoly_deriv_bern (size_t degree, ssize_t stride, const double *poly,
                  ssize_t deriv_stride, double *deriv)
{
  if (degree == 0)
    deriv[0] = 0.0;
  else
    {
      double d[degree];
      for (size_t i = 0; i < degree; i++)
        d[i] = (degree * poly[stride * (ssize_t) (i + 1)] -
                degree * poly[stride * (ssize_t) i]);
      copy_f64_with_strides (deriv_stride, deriv, 1, d, degree);
    }
}

VISIBLE void
dpoly_deriv_sbern (size_t degree, ssize_t stride, const double *poly,
                   ssize_t deriv_stride, double *deriv)
{
  if (degree == 0)
    deriv[0] = 0.0;
  else
    {
      double d[degree];
      for (size_t i = 0; i < degree; i++)
        d[i] = ((i + 1) * poly[stride * (ssize_t) (i + 1)] -
                (degree - i) * poly[stride * (ssize_t) i]);
      copy_f64_with_strides (deriv_stride, deriv, 1, d, degree);
    }
}

VISIBLE void
dpoly_deriv_spower (size_t degree, ssize_t stride, const double *poly,
                    ssize_t deriv_stride, double *deriv)
{
  if (degree == 0)
    deriv[0] = 0.0;
  else
    {
      double s[degree + 1];
      copy_f64_with_strides (1, s, stride, poly, degree + 1);

      double d[degree];

      const size_t n = degree;
      const size_t n2 = n / 2;

      for (size_t i = 1; i <= n2; i++)
        {
          const double like_terms =
            (2 * i - 1) * s[n - i + 1] - (2 * i - 1) * s[i - 1];
          d[i - 1] = like_terms + i * s[i];
          d[n - i] = like_terms - i * s[n - i];
        }

      if (n % 2 == 1)
        d[n2] = (2 * n2 + 1) * s[n2 + 1] - (2 * n2 + 1) * s[n2];

      copy_f64_with_strides (deriv_stride, deriv, 1, d, degree);
    }
}
