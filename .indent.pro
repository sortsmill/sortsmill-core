// -*- fundamental -*-
//
// .indent.pro file for GNU Indent.

--gnu-style
--no-tabs
--line-length80

-T atstype_void
-T atstype_ptr
-T atstype_ptrk
-T atstype_arrptr
-T atstype_byte
-T atstype_char
-T atstype_schar
-T atstype_uchar
-T atstype_int
-T atstype_sint
-T atstype_lint
-T atstype_llint
-T atstype_int8
-T atstype_int16
-T atstype_int32
-T atstype_int64
-T atstype_uint
-T atstype_usint
-T atstype_ulint
-T atstype_ullint
-T atstype_uint8
-T atstype_uint16
-T atstype_uint32
-T atstype_uint64
-T atstype_size
-T atstype_ssize
-T atstype_intptr
-T atstype_uintptr
-T atstype_float
-T atstype_double
-T atstype_ldouble
-T atstype_ref
-T atstype_boxed
-T atstype_string
-T atstype_stropt
-T atstype_strptr

-T FILE
-T _Bool
-T bool
-T _Complex
-T complex

-T int8_t
-T int16_t
-T int32_t
-T int64_t
-T int128_t
-T intptr_t
-T intmax_t
-T ssize_t

-T uint8_t
-T uint16_t
-T uint32_t
-T uint64_t
-T uint128_t
-T uintptr_t
-T uintmax_t
-T size_t

-T mpz_t
-T mpq_t
-T mpf_t

-T pthread_mutex_t
-T regoff_t
-T pcre
-T AO_t

-T rexp_buffer_t
-T rexp_t
-T rexp_match_buffer_t
-T rexp_match_t
-T rexp_interval_t

-T hobby_guide_token
-T _hobby_record
-T _hobby_record_tag

-T BRENTROOT_REAL
-T sbrentroot_func_t
-T dbrentroot_func_t
-T lbrentroot_func_t

-T GAUSSELIMSOLVE_REAL
-T C_DGTSV_REAL

-T polynomial_root_interval_t
-T polynomial_list_entry_t

-T stm_indexed_constant_count_t

-T bisection_stream_t
-T float_plane_intersection_t
-T double_plane_intersection_t

-T intptr_to_intptr_map_t
-T uintptr_to_uintptr_map_t
