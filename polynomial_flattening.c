#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <float.h>
#include <string.h>
#include <sortsmill/core.h>

//----------------------------------------------------------------------

struct _flattening_stream__
{
  size_t degree_a;
  ssize_t stride_ax;
  ssize_t stride_ay;
  const double *ax;
  const double *ay;
  double *bx;
  double *by;
  double absolute_tolerance_squared;
  double relative_tolerance_squared;
  bisection_stream_t intervals;
};

VISIBLE void
flattening_stream_point (flattening_stream_t p, double *x, double *y, double *t)
{
  if (p->intervals == NULL)
    {
      // The last point.
      if (t != NULL)
        *t = 1;
      if (x != NULL)
        *x = p->ax[(ssize_t) p->degree_a * p->stride_ax];
      if (y != NULL)
        *y = p->ay[(ssize_t) p->degree_a * p->stride_ay];
    }
  else
    {
      if (t != NULL)
        *t = p->intervals->a;
      if (x != NULL)
        *x = p->bx[0];
      if (y != NULL)
        *y = p->by[0];
    }
}

VISIBLE flattening_stream_t
flattening_stream_make_spower (size_t degree_a,
                               ssize_t stride_ax, const double *ax,
                               ssize_t stride_ay, const double *ay,
                               double absolute_tolerance,
                               double relative_tolerance)
{
  flattening_stream_t p = x_gc_malloc (sizeof (struct _flattening_stream__));
  p->degree_a = degree_a;
  p->stride_ax = stride_ax;
  p->stride_ay = stride_ay;
  p->ax = ax;
  p->ay = ay;
  p->bx = x_gc_malloc ((degree_a + 1) * sizeof (double));
  p->by = x_gc_malloc ((degree_a + 1) * sizeof (double));
  p->absolute_tolerance_squared = absolute_tolerance * absolute_tolerance;
  p->relative_tolerance_squared = relative_tolerance * relative_tolerance;

  if (degree_a == 0)
    p->intervals = NULL;
  else
    {
      p->intervals = bisection_stream_make (0, 1);

      if (3 <= degree_a)
        {
          // Cut the curve at inflection points and cusps.

          size_t num_inflections;
          double inflections[2 * degree_a - 4];
          dpoly_inflections_spower (degree_a, stride_ax, ax, stride_ay, ay,
                                    &num_inflections, inflections);
          if (num_inflections != 0 && inflections[num_inflections - 1] == 1)
            // Ignore an inflection at one.
            num_inflections--;
          if (num_inflections != 0 && inflections[0] == 0)
            {
              // Ignore an inflection at zero.
              num_inflections--;
              memmove (inflections, inflections + 1,
                       num_inflections * sizeof (double));
            }
          p->intervals =
            bisection_stream_cuts (p->intervals, num_inflections, inflections);
        }

      dpoly_portion_spower (degree_a, stride_ax, ax,
                            p->intervals->a, p->intervals->b, 1, p->bx);
      dpoly_portion_spower (degree_a, stride_ay, ay,
                            p->intervals->a, p->intervals->b, 1, p->by);
    }

  return p;
}

static void
_go_to_next_interval (flattening_stream_t p)
{
  p->intervals = bisection_stream_next (p->intervals);
  if (p->intervals != NULL)
    {
      dpoly_portion_spower (p->degree_a, p->stride_ax, p->ax,
                            p->intervals->a, p->intervals->b, 1, p->bx);
      dpoly_portion_spower (p->degree_a, p->stride_ay, p->ay,
                            p->intervals->a, p->intervals->b, 1, p->by);
    }
}

static void
_cut_interval_in_pieces (flattening_stream_t p)
{
  p->intervals = bisection_stream_bisect (p->intervals);
  dpoly_portion_spower (p->degree_a, p->stride_ax, p->ax,
                        p->intervals->a, p->intervals->b, 1, p->bx);
  dpoly_portion_spower (p->degree_a, p->stride_ay, p->ay,
                        p->intervals->a, p->intervals->b, 1, p->by);
}

VISIBLE flattening_stream_t
flattening_stream_next (flattening_stream_t p)
{
  bool flat = false;
  while (p->intervals != NULL && !flat)
    {
      const double diff_x = p->bx[p->degree_a] - p->bx[0];
      const double diff_y = p->by[p->degree_a] - p->by[0];
      const double square_length = diff_x * diff_x + diff_y * diff_y;

      if (square_length == 0)
        {
          // The segment is so small that due to truncation errors it
          // is just a point. Do nothing with it.
          _go_to_next_interval (p);
        }
      else if (square_length < 2 * DBL_EPSILON)
        {
          // The segment cannot get any smaller without becoming a
          // point due to truncation errors. Assume it is ‘flat
          // enough’.
          _go_to_next_interval (p);
          flat = true;
        }
      else
        {
          const double error_x =
            dpoly_degreechangeerrorbound_spower (p->degree_a, 1, p->bx, 1);
          const double error_y =
            dpoly_degreechangeerrorbound_spower (p->degree_a, 1, p->by, 1);
          const double square_error = error_x * error_x + error_y * error_y;

          if (square_error <= p->absolute_tolerance_squared
              && square_error <= square_length * p->relative_tolerance_squared)
            {
              // The segment is ‘flat enough’.
              _go_to_next_interval (p);
              flat = true;
            }
          else
            // The curve is not ‘flat enough’ on this interval. Try
            // smaller intervals.
            _cut_interval_in_pieces (p);
        }
    }

  return p;
}

VISIBLE bool
flattening_stream_done (flattening_stream_t p)
{
  return (p->intervals == NULL);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_flatten_spower (size_t degree_a, ssize_t stride_ax, const double *ax,
                      ssize_t stride_ay, const double *ay,
                      double absolute_tolerance, double relative_tolerance,
                      size_t max_num_points, size_t *num_points,
                      double *x, double *y, double *t, int *info)
{
  if (max_num_points == 0)
    {
      *num_points = 0;
      *info = SMCORE__FLATTENING_MAX_NUM_POINTS_EXCEEDED;
    }
  else
    {
      flattening_stream_t p =
        flattening_stream_make_spower (degree_a, stride_ax, ax, stride_ay, ay,
                                       absolute_tolerance, relative_tolerance);
      flattening_stream_point (p, x, y, t);
      *num_points = 1;

      *info = 0;
      while (*info == 0 && !flattening_stream_done (p))
        {
          p = flattening_stream_next (p);
          if (max_num_points <= *num_points)
            *info = SMCORE__FLATTENING_MAX_NUM_POINTS_EXCEEDED;
          else
            {
              x = (x == NULL) ? NULL : (x + 1);
              y = (y == NULL) ? NULL : (y + 1);
              t = (t == NULL) ? NULL : (t + 1);
              flattening_stream_point (p, x, y, t);
              *num_points += 1;
            }
        }
    }
}

VISIBLE void
dpoly_flatten_mono (size_t degree_a, ssize_t stride_ax, const double *ax,
                    ssize_t stride_ay, const double *ay,
                    double absolute_tolerance, double relative_tolerance,
                    size_t max_num_points, size_t *num_points,
                    double *x, double *y, double *t, int *info)
{
  double ax_[degree_a + 1];
  double ay_[degree_a + 1];
  dpoly_mono_to_spower (degree_a, stride_ax, ax, 1, ax_);
  dpoly_mono_to_spower (degree_a, stride_ay, ay, 1, ay_);
  dpoly_flatten_spower (degree_a, 1, ax_, 1, ay_,
                        absolute_tolerance, relative_tolerance,
                        max_num_points, num_points, x, y, t, info);
}

VISIBLE void
dpoly_flatten_bern (size_t degree_a, ssize_t stride_ax, const double *ax,
                    ssize_t stride_ay, const double *ay,
                    double absolute_tolerance, double relative_tolerance,
                    size_t max_num_points, size_t *num_points,
                    double *x, double *y, double *t, int *info)
{
  double ax_[degree_a + 1];
  double ay_[degree_a + 1];
  dpoly_bern_to_spower (degree_a, stride_ax, ax, 1, ax_);
  dpoly_bern_to_spower (degree_a, stride_ay, ay, 1, ay_);
  dpoly_flatten_spower (degree_a, 1, ax_, 1, ay_,
                        absolute_tolerance, relative_tolerance,
                        max_num_points, num_points, x, y, t, info);
}

VISIBLE void
dpoly_flatten_sbern (size_t degree_a, ssize_t stride_ax, const double *ax,
                     ssize_t stride_ay, const double *ay,
                     double absolute_tolerance, double relative_tolerance,
                     size_t max_num_points, size_t *num_points,
                     double *x, double *y, double *t, int *info)
{
  double ax_[degree_a + 1];
  double ay_[degree_a + 1];
  dpoly_sbern_to_spower (degree_a, stride_ax, ax, 1, ax_);
  dpoly_sbern_to_spower (degree_a, stride_ay, ay, 1, ay_);
  dpoly_flatten_spower (degree_a, 1, ax_, 1, ay_,
                        absolute_tolerance, relative_tolerance,
                        max_num_points, num_points, x, y, t, info);
}

//----------------------------------------------------------------------
