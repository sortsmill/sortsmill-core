#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>

// Detection of inflections and cusps in polynomial splines. (A cusp
// is a point of two or more coincident inflections.)

static void
_dpoly_inflections (size_t degree_a, ssize_t stride_ax, const double *ax,
                    ssize_t stride_ay, const double *ay,
                    size_t *num_inflections,
                    double inflections[(degree_a <= 2) ? 0 : 2 * degree_a - 4],
                    void (*deriv) (size_t degree,
                                   ssize_t stride, const double *poly,
                                   ssize_t deriv_stride, double *deriv),
                    void (*crossproduct) (size_t degree_a,
                                          ssize_t stride_ax, const double *ax,
                                          ssize_t stride_ay, const double *ay,
                                          size_t degree_b,
                                          ssize_t stride_bx, const double *bx,
                                          ssize_t stride_by, const double *by,
                                          ssize_t stride_cross, double *cross),
                    void (*to_spower) (size_t degree,
                                       ssize_t stride_a, const double *a,
                                       ssize_t stride_b, double *b))
{

  if (degree_a <= 2)
    *num_inflections = 0;
  else
    {
      double ax_dot[degree_a];
      double ax_dot_dot[degree_a - 1];
      double ay_dot[degree_a];
      double ay_dot_dot[degree_a - 1];

      deriv (degree_a, stride_ax, ax, 1, ax_dot);
      deriv (degree_a - 1, 1, ax_dot, 1, ax_dot_dot);
      deriv (degree_a, stride_ay, ay, 1, ay_dot);
      deriv (degree_a - 1, 1, ay_dot, 1, ay_dot_dot);

      const size_t cross_product_degree = (degree_a - 1) + (degree_a - 2);
      double cross_product[cross_product_degree + 1];
      crossproduct (degree_a - 1, 1, ax_dot, 1, ay_dot,
                    degree_a - 2, 1, ax_dot_dot, 1, ay_dot_dot,
                    1, cross_product);
      if (to_spower != NULL)
        to_spower (cross_product_degree, 1, cross_product, 1, cross_product);

      // I figure that the number of inflection points is no more than
      // 2*degree_a - 4, and that we can reduce the degree of
      // cross_product by at least 1 before solving. (That is, the
      // minimum or actual degree is at least one less.) I do not know
      // if doing this is worth it for efficiency or introduces excess
      // error, but it may help avoid having the rootfinder return too
      // many roots.

      dpoly_changedegree_spower (cross_product_degree, 1, cross_product,
                                 cross_product_degree - 1, 1, cross_product);
      size_t actual_degree =
        dpoly_mindegree_spower (cross_product_degree - 1, 1, cross_product);
      if (actual_degree < cross_product_degree - 1)
        dpoly_changedegree_spower (cross_product_degree - 1, 1, cross_product,
                                   actual_degree, 1, cross_product);

      dpoly_findroots_spower (actual_degree, 1, cross_product, 0, 1, NULL,
                              num_inflections, inflections);
    }
}

VISIBLE void
dpoly_inflections_mono (size_t degree_a, ssize_t stride_ax, const double *ax,
                        ssize_t stride_ay, const double *ay,
                        size_t *num_inflections,
                        double inflections
                        [(degree_a <= 2) ? 0 : 2 * degree_a - 4])
{
  _dpoly_inflections (degree_a, stride_ax, ax, stride_ay, ay,
                      num_inflections, inflections, dpoly_deriv_mono,
                      dpoly_crossproduct_mono, dpoly_mono_to_spower);
}

VISIBLE void
dpoly_inflections_bern (size_t degree_a, ssize_t stride_ax, const double *ax,
                        ssize_t stride_ay, const double *ay,
                        size_t *num_inflections,
                        double inflections
                        [(degree_a <= 2) ? 0 : 2 * degree_a - 4])
{
  _dpoly_inflections (degree_a, stride_ax, ax, stride_ay, ay,
                      num_inflections, inflections, dpoly_deriv_bern,
                      dpoly_crossproduct_bern, dpoly_bern_to_spower);
}

VISIBLE void
dpoly_inflections_sbern (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay,
                         size_t *num_inflections,
                         double inflections
                         [(degree_a <= 2) ? 0 : 2 * degree_a - 4])
{
  _dpoly_inflections (degree_a, stride_ax, ax, stride_ay, ay,
                      num_inflections, inflections, dpoly_deriv_sbern,
                      dpoly_crossproduct_sbern, dpoly_sbern_to_spower);
}

VISIBLE void
dpoly_inflections_spower (size_t degree_a, ssize_t stride_ax, const double *ax,
                          ssize_t stride_ay, const double *ay,
                          size_t *num_inflections,
                          double inflections
                          [(degree_a <= 2) ? 0 : 2 * degree_a - 4])
{
  _dpoly_inflections (degree_a, stride_ax, ax, stride_ay, ay,
                      num_inflections, inflections, dpoly_deriv_spower,
                      dpoly_crossproduct_spower, NULL);
}
