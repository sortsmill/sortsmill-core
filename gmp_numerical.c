#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <math.h>
#include <float.h>
#include <sortsmill/core.h>

VISIBLE double
mpq_get_d_nexttoward (const mpq_t x, const mpq_t y)
{
  double result;

  const int cmp_xy = mpq_cmp (x, y);
  if (cmp_xy == 0)              // x == y
    {
      // The direction is ambiguous. Simply truncate towards zero.
      result = mpq_get_d (x);
    }
  else
    {
      const double xd = mpq_get_d (x);
      const int sign_x = mpq_sgn (x);
      if (cmp_xy < 0)           // x < y
        {
          if (sign_x < 0)
            {
              // x < 0; therefore mpq_get_d() truncates towards
              // HUGE_VAL.  Truncation may have done our job for us.
              mpq_t tmp;
              mpq_init (tmp);
              mpq_set_d (tmp, xd);
              result = (mpq_equal (tmp, x)) ? nextafter (xd, HUGE_VAL) : xd;
              mpq_clear (tmp);
            }
          else
            {
              // 0 <= x; therefore mpq_get_d() truncates away from HUGE_VAL.
              result = nextafter (xd, HUGE_VAL);
            }
        }
      else                      // y < x
        {
          if (sign_x <= 0)
            {
              // x <= 0; therefore mpq_get_d() truncates away from
              // -HUGE_VAL.
              result = nextafter (xd, -HUGE_VAL);
            }
          else
            {
              // 0 < x; therefore mpq_get_d() truncates towards
              // -HUGE_VAL.  Truncation may have done our job for us.
              mpq_t tmp;
              mpq_init (tmp);
              mpq_set_d (tmp, xd);
              result = (mpq_equal (tmp, x)) ? nextafter (xd, -HUGE_VAL) : xd;
              mpq_clear (tmp);
            }
        }
    }

  return result;
}

VISIBLE double
mpq_get_d_roundtoward (const mpq_t x, const mpq_t y)
{
  const double xd = mpq_get_d (x);

  mpq_t xdq;
  mpq_init (xdq);
  mpq_set_d (xdq, xd);

  const double result =
    (mpq_equal (x, xdq)) ? xd : (mpq_get_d_nexttoward (x, y));

  mpq_clear (xdq);

  return result;
}
