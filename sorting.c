#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sortsmill/core/sorting.h>
#include <sortsmill/core/xgc.h>

//----------------------------------------------------------------------

VISIBLE void
smcore_insertion_sort (void *base, size_t nmemb, size_t size,
                       int (*compar) (const void *, const void *, void *),
                       void *arg)
{
  unsigned char *const b = base;
  for (size_t i = 0; i < nmemb; i++)
    {
      size_t j = i;
      while (j != 0 && compar (&b[i * size], &b[(j - 1) * size], arg) < 0)
        j--;
      if (j != i)
        // Circularly shift the slice from j to i, inclusive,
        // rightwards by one member.
        for (size_t u = 0; u < size; u++)
          {
            // Circularly shift the uth bytes of the elements.
            unsigned char *bu = &b[u];
            size_t isize = i * size;
            unsigned char tmp = bu[isize];
            for (size_t v = i; j < v; v--)
              {
                const size_t next_isize = isize - size;
                bu[isize] = bu[next_isize];
                isize = next_isize;
              }
            bu[isize] = tmp;
          }
    }
}

//----------------------------------------------------------------------

// Use insertion sort on portions of the array equal in size to
// INSERTION_THRESHOLD_ or fewer members. For larger portions, use
// merge sort.
//
// FIXME: Choose a better threshold, if there is one.
//
#define INSERTION_THRESHOLD__ 10

#define MIN__(a, b) (((a) < (b)) ? (a) : (b))

// Use COPY_BYTES__() to avoid calling memcpy when copying a ‘small’
// number of bytes.
#define COPY_BYTES__(Q, P, N)                   \
  do                                            \
    {                                           \
      unsigned char *p = (P);                   \
      unsigned char *q = (Q);                   \
      const size_t n = (N);                     \
      for (size_t i = 0; i < n; i++)            \
        {                                       \
          *q = *p;                              \
          q++;                                  \
          p++;                                  \
        }                                       \
    } while (false);

static size_t
_num_divisions (size_t nmemb)
{

  size_t n = nmemb / INSERTION_THRESHOLD__;
  if (n * INSERTION_THRESHOLD__ < nmemb)
    n++;                        // Do not exceed the insertion-sort threshold.
  return n;
}

static void
_stable_sort (void *base, size_t nmemb, size_t size,
              int (*compar) (const void *, const void *, void *), void *arg)
{
  unsigned char *const pb = base;

  // Do insertion sorts on small portions of the array.
  size_t num_subdivisions = _num_divisions (nmemb);
  size_t members_per_subdivision = nmemb / num_subdivisions;
  size_t subdivision_size = members_per_subdivision * size;
  unsigned char *pi = pb;
  for (size_t i = 0; i < num_subdivisions; i++)
    {
      smcore_insertion_sort (pi, members_per_subdivision, size, compar, arg);
      pi += subdivision_size;
    }
  smcore_insertion_sort (pi, nmemb % num_subdivisions, size, compar, arg);

  // Repeatedly merge. The result will end up in the space pointed to
  // by p1.
  unsigned char *const buffer =
    x_gc_malloc (nmemb * size * sizeof (unsigned char));
  unsigned char *p1 = pb;
  unsigned char *p2 = buffer;
  while (members_per_subdivision < nmemb)
    {
      size_t i = 0;
      while (i < nmemb)
        {
          const size_t j = MIN__ (i + members_per_subdivision, nmemb);
          const size_t k = MIN__ (j + members_per_subdivision, nmemb);

          // Merge [i,j) with [j,k).
          const size_t isize = i * size;
          const size_t jsize = j * size;
          const size_t ksize = k * size;
          bool done = false;
          size_t u = isize;
          size_t v = jsize;
          size_t w = u;
          do
            {
              if (u == jsize)
                {
                  memcpy (&p2[w], &p1[v], ksize - v);
                  done = true;
                }
              else if (v == ksize)
                {
                  memcpy (&p2[w], &p1[u], jsize - u);
                  done = true;
                }
              else
                {
                  if (compar (&p1[u], &p1[v], arg) < 0)
                    {
                      COPY_BYTES__ (&p2[w], &p1[u], size);
                      u += size;
                    }
                  else
                    {
                      COPY_BYTES__ (&p2[w], &p1[v], size);
                      v += size;
                    }
                  w += size;
                }
            }
          while (!done);

          i = k;
        }

      // Interchange the source and destination buffers.
      unsigned char *const p_tmp = p1;
      p1 = p2;
      p2 = p_tmp;

      // Double members_per_subdivision, but avoid overflow (though
      // overflow is _extremely_ unlikely). The <= sign must not be
      // changed to a < sign.
      members_per_subdivision = (members_per_subdivision <= nmemb / 2) ?
        (2 * members_per_subdivision) : nmemb;
    }

  if (p1 != pb)
    // The final result is in the ‘wrong’ buffer. Copy it to the
    // correct buffer, using a fast memcpy().
    memcpy (pb, p1, nmemb * size * sizeof (unsigned char));
}

VISIBLE void
smcore_stable_sort (void *base, size_t nmemb, size_t size,
                    int (*compar) (const void *, const void *, void *),
                    void *arg)
{
  if (nmemb != 0)
    {
      if (nmemb <= INSERTION_THRESHOLD__)
        // If the array is very small, do an insertion sort.
        smcore_insertion_sort (base, nmemb, size, compar, arg);
      else
        _stable_sort (base, nmemb, size, compar, arg);
    }
}

//----------------------------------------------------------------------
