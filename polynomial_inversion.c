#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdbool.h>
#include <math.h>
#include <float.h>
#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

static inline size_t
szmax (size_t a, size_t b)
{
  return (a < b) ? b : a;
}

static int
compare_abs_values (const mpq_t a, const mpq_t b)
{
  mpq_t abs_a, abs_b;
  mpq_inits (abs_a, abs_b, NULL);
  mpq_abs (abs_a, a);
  mpq_abs (abs_b, b);
  const int cmp = mpq_cmp (abs_a, abs_b);
  mpq_clears (abs_a, abs_b, NULL);
  return cmp;
}

static void
_solve_linear (ssize_t stride_x, mpq_t *poly_x, const mpq_t x, mpq_t t)
{
  //    x = a0 + a1 * t
  //
  // therefore
  //
  //    t = -a0 / a1 + x / a1 = (x - a0) / a1
  //
  mpq_sub (t, x, poly_x[0]);    //  t = x - a0
  mpq_div (t, t, poly_x[stride_x]);     //  t = (x - a0) / a1
}

VISIBLE void
mpq_poly_invertdegree1_mono (ssize_t stride_x, mpq_t *poly_x,
                             ssize_t stride_y, mpq_t *poly_y,
                             const mpq_t x, const mpq_t y, mpq_t t, int *info)
{
  if (mpq_sgn (poly_x[stride_x]) == 0 && mpq_sgn (poly_y[stride_y]) == 0)
    *info = SMCORE__INVERSION_PARAMETER_IS_ARBITRARY;
  else
    {
      *info = 0;
      if (compare_abs_values (poly_x[stride_x], poly_y[stride_y]) < 0)
        _solve_linear (stride_y, poly_y, y, t);
      else
        _solve_linear (stride_x, poly_x, x, t);
    }
}


static void
_find_inverses (size_t degree, ssize_t stride, mpq_t *poly,
                const mpq_t poly_value,
                const mpq_t ta, const mpq_t tb,
                size_t *num_solutions, double t[degree])
{
  mpq_t *p = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, p);
  copy_mpq_with_strides (1, p, stride, poly, degree + 1);
  mpq_sub (p[0], p[0], poly_value);

  // FIXME: Consider using a double-precision Bernstein-basis
  // evaluator for the polynomial.
  dpoly_findroots (degree, 1, p, ta, tb, NULL, NULL, NULL,
                   num_solutions, t, NULL);

  mpq_clear_poly (degree, p);
  free (p);
}

static ssize_t
_find_best_match1 (size_t num1, size_t num2, double differences[num1][num2],
                   size_t i)
{
  ssize_t best_match = -1;
  double diff = HUGE_VAL;
  for (size_t j = 0; j < num2; j++)
    if (differences[i][j] < diff)
      {
        best_match = j;
        diff = differences[i][j];
      }
  return best_match;
}

static ssize_t
_find_best_match2 (size_t num1, size_t num2, double differences[num1][num2],
                   size_t j)
{
  ssize_t best_match = -1;
  double diff = HUGE_VAL;
  for (size_t i = 0; i < num1; i++)
    if (differences[i][j] < diff)
      {
        best_match = i;
        diff = differences[i][j];
      }
  return best_match;
}

VISIBLE void
dpoly_invert_mono (size_t degree, ssize_t stride_x, mpq_t *poly_x,
                   ssize_t stride_y, mpq_t *poly_y,
                   const mpq_t x, const mpq_t y,
                   const mpq_t ta, const mpq_t tb,
                   double absolute_tolerance,
                   size_t *num_solutions, double t[degree], int *info)
{
  ///////////////////////////////////////////////////////////////////////////
  // FIXME FIXME FIXME FIXME FIXME FIXME FIXME
  //
  // THIS ROUTINE SEEMS NOT TO GIVE CORRECT RESULTS.
  //
  // FIXME FIXME FIXME FIXME FIXME FIXME FIXME
  ///////////////////////////////////////////////////////////////////////////

  // NOTE: An _absolute_ tolerance seems reasonable for the
  // application of inverting a parametric plane spline on [0,1] that
  // is part of a font outline.

  const bool poly_x_is_constant =
    mpq_poly_equalszero (degree - 1, stride_x, poly_x + stride_x);
  const bool poly_y_is_constant =
    mpq_poly_equalszero (degree - 1, stride_y, poly_y + stride_y);

  if (poly_x_is_constant)
    {
      if (poly_y_is_constant)
        {
          *num_solutions = 0;
          *info = SMCORE__INVERSION_PARAMETER_IS_ARBITRARY;
        }
      else
        {
          _find_inverses (degree, stride_y, poly_y, y, ta, tb,
                          num_solutions, t);
          *info = 0;
        }
    }
  else if (poly_y_is_constant)
    {
      _find_inverses (degree, stride_x, poly_x, x, ta, tb, num_solutions, t);
      *info = 0;
    }
  else
    {
      // Find roots that are common to within tolerance.

      double *tx = xmalloc (degree * sizeof (double));
      double *ty = xmalloc (degree * sizeof (double));
      size_t num_x_solutions;
      size_t num_y_solutions;

      _find_inverses (degree, stride_x, poly_x, x, ta, tb,
                      &num_x_solutions, tx);
      _find_inverses (degree, stride_y, poly_y, y, ta, tb,
                      &num_y_solutions, ty);

      double *t1;
      size_t num1;
      double *t2;
      size_t num2;
      if (num_x_solutions < num_y_solutions)
        {
          t1 = tx;
          num1 = num_x_solutions;
          t2 = ty;
          num2 = num_y_solutions;
        }
      else
        {
          t1 = ty;
          num1 = num_y_solutions;
          t2 = tx;
          num2 = num_x_solutions;
        }

      double (*differences)[num2] = xmalloc (num1 * num2 * sizeof (double));

      for (size_t i = 0; i < num1; i++)
        for (size_t j = 0; j < num2; j++)
          differences[i][j] = fabs (t1[i] - t2[j]);

      // For each entry in t1, find the nearest match in t2.  Accept
      // it if their difference is within tolerance and the element in
      // t2 has no nearer match in t1.
      *num_solutions = 0;
      for (size_t i = 0; i < num1; i++)
        {
          const size_t best_match =
            _find_best_match1 (num1, num2, differences, i);
          if (0 <= best_match)
            {
              const size_t what_it_matches_best =
                _find_best_match2 (num1, num2, differences, best_match);
              if (i == what_it_matches_best)
                {
                  // Accept the mean of the matches as a solution.
                  t[*num_solutions] = (t1[i] + t2[best_match]) / 2;
                  *num_solutions += 1;
                }
            }
        }

      free (differences);
      free (ty);
      free (tx);
    }
}
