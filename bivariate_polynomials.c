#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>

// Bivariate polynomials represented as skew upper triangles within
// two-dimensional arrays. The rest of the array is left unused.

static inline size_t
szmin (size_t a, size_t b)
{
  return (a < b) ? a : b;
}

//----------------------------------------------------------------------

VISIBLE void
mpq_bipoly_init (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1])
{
  // The area below the skew diagonal is not used and therefore is
  // left uninitialized.

  for (size_t i = 0; i <= degree_a; i++)
    for (size_t j = 0; j <= degree_a - i; j++)
      mpq_init (a[i][j]);
}

VISIBLE void
mpq_bipoly_clear (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1])
{
  for (size_t i = 0; i <= degree_a; i++)
    for (size_t j = 0; j <= degree_a - i; j++)
      mpq_clear (a[i][j]);
}

VISIBLE void
mpq_bipoly_add (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                size_t degree_b, mpq_t b[degree_b + 1][degree_b + 1],
                mpq_t sum
                [((degree_a < degree_b) ? degree_b : degree_a) + 1]
                [((degree_a < degree_b) ? degree_b : degree_a) + 1])
{
  // This routine allows sum to be the same array as a or b.

  const size_t degree_min = szmin (degree_a, degree_b);

  for (size_t i = 0; i <= degree_min; i++)
    for (size_t j = 0; j <= degree_min - i; j++)
      mpq_add (sum[i][j], a[i][j], b[i][j]);

  if (degree_a < degree_b)
    for (size_t i = degree_a + 1; i <= degree_b; i++)
      for (size_t j = 0; j <= i; j++)
        mpq_set (sum[j][i - j], b[j][i - j]);
  else
    for (size_t i = degree_b + 1; i <= degree_a; i++)
      for (size_t j = 0; j <= i; j++)
        mpq_set (sum[j][i - j], a[j][i - j]);
}

VISIBLE void
mpq_bipoly_sub (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                size_t degree_b, mpq_t b[degree_b + 1][degree_b + 1],
                mpq_t difference
                [((degree_a < degree_b) ? degree_b : degree_a) + 1]
                [((degree_a < degree_b) ? degree_b : degree_a) + 1])
{
  // This routine allows difference to be the same array as a or b.

  const size_t degree_min = szmin (degree_a, degree_b);

  for (size_t i = 0; i <= degree_min; i++)
    for (size_t j = 0; j <= degree_min - i; j++)
      mpq_sub (difference[i][j], a[i][j], b[i][j]);

  if (degree_a < degree_b)
    for (size_t i = degree_a + 1; i <= degree_b; i++)
      for (size_t j = 0; j <= i; j++)
        mpq_neg (difference[j][i - j], b[j][i - j]);
  else
    for (size_t i = degree_b + 1; i <= degree_a; i++)
      for (size_t j = 0; j <= i; j++)
        mpq_set (difference[j][i - j], a[j][i - j]);
}

VISIBLE void
mpq_bipoly_mul (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                size_t degree_b, mpq_t b[degree_b + 1][degree_b + 1],
                mpq_t product[degree_a + degree_b + 1][degree_a + degree_b + 1])
{
  mpq_t tmp;
  mpq_init (tmp);

  for (size_t i = 0; i <= degree_a + degree_b; i++)
    for (size_t j = 0; j <= (degree_a + degree_b) - i; j++)
      mpq_set_ui (product[i][j], 0, 1);

  for (size_t i = 0; i <= degree_a; i++)
    for (size_t j = 0; j <= degree_a - i; j++)
      for (size_t s = 0; s <= degree_b; s++)
        for (size_t t = 0; t <= degree_b - s; t++)
          {
            mpq_mul (tmp, a[i][j], b[s][t]);
            mpq_add (product[i + s][j + t], product[i + s][j + t], tmp);
          }

  mpq_clear (tmp);
}

VISIBLE void
mpq_bipoly_scalarmul (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                      const mpq_t r, mpq_t product[degree_a + 1][degree_a + 1])
{
  // This routine allows a and product to be the same array.

  for (size_t i = 0; i <= degree_a; i++)
    for (size_t j = 0; j <= degree_a - i; j++)
      mpq_mul (product[i][j], a[i][j], r);
}

VISIBLE void
mpq_bipoly_partial_deriv_wrt_x (size_t degree_a,
                                mpq_t a[degree_a + 1][degree_a + 1],
                                mpq_t Dx
                                [(degree_a == 0) ? 1 : degree_a]
                                [(degree_a == 0) ? 1 : degree_a])
{
  if (degree_a == 0)
    mpq_set_ui (Dx[0][0], 0, 1);
  else
    {
      mpq_t n;
      mpq_init (n);

      const size_t deg = degree_a - 1;

      for (size_t j = 0; j <= deg; j++)
        mpq_set (Dx[0][j], a[1][j]);

      for (size_t i = 1; i <= deg; i++)
        for (size_t j = 0; j <= deg - i; j++)
          {
            mpq_set_ui (n, i + 1, 1);
            mpq_mul (Dx[i][j], n, a[i + 1][j]);
          }

      mpq_clear (n);
    }
}

VISIBLE void
mpq_bipoly_partial_deriv_wrt_y (size_t degree_a,
                                mpq_t a[degree_a + 1][degree_a + 1],
                                mpq_t Dy
                                [(degree_a == 0) ? 1 : degree_a]
                                [(degree_a == 0) ? 1 : degree_a])
{
  if (degree_a == 0)
    mpq_set_ui (Dy[0][0], 0, 1);
  else
    {
      mpq_t n;
      mpq_init (n);

      const size_t deg = degree_a - 1;

      for (size_t j = 0; j <= deg; j++)
        mpq_set (Dy[j][0], a[j][1]);

      for (size_t i = 1; i <= deg; i++)
        for (size_t j = 0; j <= deg - i; j++)
          {
            mpq_set_ui (n, i + 1, 1);
            mpq_mul (Dy[j][i], n, a[j][i + 1]);
          }

      mpq_clear (n);
    }
}

//----------------------------------------------------------------------

VISIBLE void
dbipoly_add (size_t degree_a, double a[degree_a + 1][degree_a + 1],
             size_t degree_b, double b[degree_b + 1][degree_b + 1],
             double sum
             [((degree_a < degree_b) ? degree_b : degree_a) + 1]
             [((degree_a < degree_b) ? degree_b : degree_a) + 1])
{
  // This routine allows sum to be the same array as a or b.

  const size_t degree_min = szmin (degree_a, degree_b);

  for (size_t i = 0; i <= degree_min; i++)
    for (size_t j = 0; j <= degree_min - i; j++)
      sum[i][j] = a[i][j] + b[i][j];

  if (degree_a < degree_b)
    for (size_t i = degree_a + 1; i <= degree_b; i++)
      for (size_t j = 0; j <= i; j++)
        sum[j][i - j] = b[j][i - j];
  else
    for (size_t i = degree_b + 1; i <= degree_a; i++)
      for (size_t j = 0; j <= i; j++)
        sum[j][i - j] = a[j][i - j];
}

VISIBLE void
dbipoly_sub (size_t degree_a, double a[degree_a + 1][degree_a + 1],
             size_t degree_b, double b[degree_b + 1][degree_b + 1],
             double difference
             [((degree_a < degree_b) ? degree_b : degree_a) + 1]
             [((degree_a < degree_b) ? degree_b : degree_a) + 1])
{
  // This routine allows difference to be the same array as a or b.

  const size_t degree_min = szmin (degree_a, degree_b);

  for (size_t i = 0; i <= degree_min; i++)
    for (size_t j = 0; j <= degree_min - i; j++)
      difference[i][j] = a[i][j] - b[i][j];

  if (degree_a < degree_b)
    for (size_t i = degree_a + 1; i <= degree_b; i++)
      for (size_t j = 0; j <= i; j++)
        difference[j][i - j] = -b[j][i - j];
  else
    for (size_t i = degree_b + 1; i <= degree_a; i++)
      for (size_t j = 0; j <= i; j++)
        difference[j][i - j] = a[j][i - j];
}

VISIBLE void
dbipoly_mul (size_t degree_a, double a[degree_a + 1][degree_a + 1],
             size_t degree_b, double b[degree_b + 1][degree_b + 1],
             double product[degree_a + degree_b + 1][degree_a + degree_b + 1])
{
  for (size_t i = 0; i <= degree_a + degree_b; i++)
    for (size_t j = 0; j <= (degree_a + degree_b) - i; j++)
      product[i][j] = 0;

  for (size_t i = 0; i <= degree_a; i++)
    for (size_t j = 0; j <= degree_a - i; j++)
      for (size_t s = 0; s <= degree_b; s++)
        for (size_t t = 0; t <= degree_b - s; t++)
          product[i + s][j + t] += a[i][j] * b[s][t];
}

VISIBLE void
dbipoly_scalarmul (size_t degree_a, double a[degree_a + 1][degree_a + 1],
                   double r, double product[degree_a + 1][degree_a + 1])
{
  // This routine allows a and product to be the same array.

  for (size_t i = 0; i <= degree_a; i++)
    for (size_t j = 0; j <= degree_a - i; j++)
      product[i][j] = a[i][j] * r;
}

//----------------------------------------------------------------------
