#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core/polynomial_lists.h>
#include "polynomial_helpers.h"

VISIBLE void
clear_polynomial_list (polynomial_list_entry_t *lst)
{
  while (lst != NULL)
    {
      polynomial_list_entry_t *p = lst;
      lst = p->next;
      mpq_clear_poly (p->degree, p->poly);
      free (p);
    }
}

VISIBLE size_t
polynomial_list_length (polynomial_list_entry_t *lst)
{
  size_t i = 0;
  while (lst != NULL)
    {
      i++;
      lst = lst->next;
    }
  return i;
}

VISIBLE polynomial_list_entry_t *
reverse_polynomial_list_in_place (polynomial_list_entry_t *lst)
{
  polynomial_list_entry_t *t = NULL;
  while (lst != NULL)
    {
      polynomial_list_entry_t *u = lst->next;
      lst->next = t;
      t = lst;
      lst = u;
    }
  return t;
}
