/*
 * Copyright (C) 2013, 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_H_
#define SORTSMILL_CORE_H_

#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <sortsmill/smcore-pkginfo.h>
#include <sortsmill/smcore-dirlayout.h>
#include <sortsmill/smcore-buildinfo.h>

#include <sortsmill/core/attributes.h>
#include <sortsmill/core/bincoef.h>
#include <sortsmill/core/bisection_streams.h>
#include <sortsmill/core/bivariate_polynomials.h>
#include <sortsmill/core/brentroot.h>
#include <sortsmill/core/c-fprintf.h>
#include <sortsmill/core/convolution.h>
#include <sortsmill/core/copy_gmp_with_strides.h>
#include <sortsmill/core/copy_with_strides.h>
#include <sortsmill/core/copy_with_strides_macros.h>
#include <sortsmill/core/cyclictridiagsolve.h>
#include <sortsmill/core/double_checked_locking.h>
#include <sortsmill/core/gausselimsolve.h>
#include <sortsmill/core/gmp_numerical.h>
#include <sortsmill/core/hash_functions.h>
#include <sortsmill/core/hash_maps.h>
#include <sortsmill/core/hobby.h>
#include <sortsmill/core/immutable_vectors.h>
#include <sortsmill/core/immutable_vectors_misc.h>
#include <sortsmill/core/indexed_constants.h>
#include <sortsmill/core/integer_maps.h>
#include <sortsmill/core/istrings.h>
#include <sortsmill/core/murmurhash3.h>
#include <sortsmill/core/null_passthru.h>
#include <sortsmill/core/one_time_initialization.h>
#include <sortsmill/core/polynomial_addition.h>
#include <sortsmill/core/polynomial_bases.h>
#include <sortsmill/core/polynomial_composition.h>
#include <sortsmill/core/polynomial_critical_points.h>
#include <sortsmill/core/polynomial_crossproducts.h>
#include <sortsmill/core/polynomial_degrees.h>
#include <sortsmill/core/polynomial_differentiation.h>
#include <sortsmill/core/polynomial_directions.h>
#include <sortsmill/core/polynomial_division.h>
#include <sortsmill/core/polynomial_evaluation.h>
#include <sortsmill/core/polynomial_flattening.h>
#include <sortsmill/core/polynomial_implicitization.h>
#include <sortsmill/core/polynomial_inflections.h>
#include <sortsmill/core/polynomial_intersections.h>
#include <sortsmill/core/polynomial_inversion.h>
#include <sortsmill/core/polynomial_lists.h>
#include <sortsmill/core/polynomial_multiplication.h>
#include <sortsmill/core/polynomial_precisions.h>
#include <sortsmill/core/polynomial_roots.h>
#include <sortsmill/core/polynomial_subdivision.h>
#include <sortsmill/core/popcount.h>
#include <sortsmill/core/ps_number.h>
#include <sortsmill/core/rb.h>
#include <sortsmill/core/rexp.h>
#include <sortsmill/core/sorting.h>
#include <sortsmill/core/tridiagsolve.h>
#include <sortsmill/core/vlists.h>
#include <sortsmill/core/x_alloc.h>
#include <sortsmill/core/xdie_on_null.h>
#include <sortsmill/core/xgc.h>
#include <sortsmill/core/xunistring.h>

#endif /* SORTSMILL_CORE_H_ */
