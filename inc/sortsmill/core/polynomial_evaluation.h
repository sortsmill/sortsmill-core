/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_EVALUATION_H_
#define SORTSMILL_CORE_POLYNOMIAL_EVALUATION_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* Monomial basis. */
void mpq_poly_eval_mono (size_t degree, ssize_t stride,
                         mpq_t *poly, const mpq_t t, mpq_t value);
double dpoly_eval_mono (size_t degree, ssize_t stride,
                        const double *poly, double t);

/* Scaled Bernstein basis. The Schumaker-Volk method is faster and has
   a good reputation for stability; the de Casteljau method is the
   most stable. */
double dpoly_eval_sbernsv (size_t degree, ssize_t stride,
                           const double *poly, double t);
double dpoly_eval_sberndc (size_t degree, ssize_t stride,
                           const double *poly, double t);

/* Bernstein basis. The Schumaker-Volk method is faster and has a good
   reputation for stability; the de Casteljau method is the most
   stable.*/
double dpoly_eval_bernsv (size_t degree, ssize_t stride,
                          const double *poly, double t);
double dpoly_eval_berndc (size_t degree, ssize_t stride,
                          const double *poly, double t);

/* Symmetric power (Sanchez-Reyes) basis. */
double dpoly_eval_spower (size_t degree, ssize_t stride,
                          const double *poly, double t);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_EVALUATION_H_ */
