/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_VLISTS_H_
#define SORTSMILL_CORE_VLISTS_H_

/*
 * VLists.
 *
 * See Phil Bagwell, `Fast Functional Lists, Hash-Lists, Deques and
 * Variable Length Arrays', in Implementation of Functional Languages,
 * 14th International Workshop, 2002, p. 34.
 *
 * The paper may be cached at
 * <http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.20.4164>
 */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <sortsmill/core/attributes.h>
#include <sortsmill/core/xgc.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#ifndef SMCORE_VLIST_ASSERT
#define SMCORE_VLIST_ASSERT assert
#endif

#ifndef SMCORE_VLIST_MAX_BLOCK_SIZE
/* This limit is ridiculously optimistic, by which I mean the
   allocator will not let us have this much memory. */
#define SMCORE_VLIST_MAX_BLOCK_SIZE SIZE_MAX
#endif

#ifndef SMCORE_VLIST_DEFAULT_R_INVERSE
#define SMCORE_VLIST_DEFAULT_R_INVERSE 2
#endif

#ifndef SMCORE_VLIST_DEFAULT_START_SIZE
#define SMCORE_VLIST_DEFAULT_START_SIZE 4
#endif

/*--------------------------------------------------------------------*/

/*
 * Section 2.2 of the paper explains how to use VLists with ordinary
 * pointers. However, that method will not work with Boehm GC unless
 * internal pointers are enabled. We are _not_ going to require
 * internal pointers support; thus let us define a special `pointer'
 * type.
 *
 * // In .h file, to declare a type for vlist of int.
 * DECLARE_SMCORE_VLIST_T (intvlst_t, int);
 */
#define DECLARE_SMCORE_VLIST_T(T, ENTRY_T)                      \
                                                                \
  struct T##__block__                                           \
  {                                                             \
    struct T##__block__ *base;                                  \
    size_t offset;                                              \
    size_t size;                                                \
    size_t last_used;                                           \
    /* The following `flexible array' field assumes C99. */     \
    ENTRY_T data[];                                             \
  };                                                            \
                                                                \
  typedef struct                                                \
  {                                                             \
    struct T##__block__ *base;                                  \
    size_t offset;                                              \
  } T;

#define VLIST_BLOCK__(ENTRY_T, SIZE)            \
  struct                                        \
  {                                             \
    struct T##__block__ *base;                  \
    size_t offset;                              \
    size_t size;                                \
    size_t last_used;                           \
    ENTRY_T data[SIZE];                         \
  }

/*
 * // To create a function @code{intvlst_t intvlst_null (void)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_NULL (intvlst_null, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_NULL (intvlst_null, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_NULL(NAME, T)              \
  inline STM_ATTRIBUTE_CONST T NAME (void);             \
  inline STM_ATTRIBUTE_CONST T                          \
  NAME (void)                                           \
  {                                                     \
    const T p = { (struct T##__block__ *) 0, 0 };       \
    return p;                                           \
  }
#define DEFINE_SMCORE_VLIST_NULL(NAME, T)       \
  STM_ATTRIBUTE_CONST T NAME (void);

/*
 * // To create a function @code{bool intvlst_is_null (intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_IS_NULL (intvlst_is_null, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_IS_NULL (intvlst_is_null, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_IS_NULL(NAME, T)           \
  inline STM_ATTRIBUTE_CONST bool NAME (T);             \
  inline STM_ATTRIBUTE_CONST bool                       \
  NAME (T p)                                            \
  {                                                     \
    return (p.base == (struct T##__block__ *) 0);       \
  }
#define DEFINE_SMCORE_VLIST_IS_NULL(NAME, T)    \
  STM_ATTRIBUTE_CONST bool NAME (T);

/*
 * // To create a function @code{bool intvlst_is_eq (intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_IS_EQ (intvlst_is_eq, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_IS_EQ (intvlst_is_eq, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_IS_EQ(NAME, T)             \
  inline STM_ATTRIBUTE_CONST bool NAME (T, T);          \
  inline STM_ATTRIBUTE_CONST bool                       \
  NAME (T p, T q)                                       \
  {                                                     \
    return (p.base == q.base && p.offset == q.offset);  \
  }
#define DEFINE_SMCORE_VLIST_IS_EQ(NAME, T)      \
  STM_ATTRIBUTE_CONST bool NAME (T, T);

/*
 * // To create a function @code{int intvlst_car (intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_CAR (intvlst_car, intvlst_t, int);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_CAR (intvlst_car, intvlst_t, int);
 */
#define DECLARE_SMCORE_VLIST_CAR(NAME, T, ENTRY_T)              \
  inline ENTRY_T NAME (T);                                      \
  inline ENTRY_T                                                \
  NAME (T p)                                                    \
  {                                                             \
    SMCORE_VLIST_ASSERT (p.base != (struct T##__block__ *) 0);  \
    return (p.base->data[p.offset]);                            \
  }
#define DEFINE_SMCORE_VLIST_CAR(NAME, T, ENTRY_T)       \
  ENTRY_T NAME (T);

/*
 * // To create a function @code{intvlst_t intvlst_cdr (intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_CDR (intvlst_cdr, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_CDR (intvlst_cdr, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_CDR(NAME, T)                       \
  inline T NAME (T);                                            \
  inline T                                                      \
  NAME (T p)                                                    \
  {                                                             \
    SMCORE_VLIST_ASSERT (p.base != (struct T##__block__ *) 0);  \
    T q;                                                        \
    if (p.offset == 0)                                          \
      {                                                         \
        q.base = p.base->base;                                  \
        q.offset = p.base->offset;                              \
      }                                                         \
    else                                                        \
      {                                                         \
        q.base = p.base;                                        \
        q.offset = p.offset - 1;                                \
      };                                                        \
    return q;                                                   \
  }
#define DEFINE_SMCORE_VLIST_CDR(NAME, T)        \
  T NAME (T);

/*
 * // To create a function @code{intvlst_t intvlst_cons (int, intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_CONS (intvlst_cons, intvlst_t, int);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_CONS (intvlst_cons, intvlst_t, int);
 */
#define DECLARE_SMCORE_VLIST_CONS(NAME, T, ENTRY_T)     \
  T NAME (ENTRY_T, T);
#define DEFINE_SMCORE_VLIST_CONS(NAME, T, ENTRY_T,                      \
                                 R_INVERSE, START_SIZE)                 \
  T                                                                     \
  NAME (ENTRY_T e, T p)                                                 \
  {                                                                     \
    T q;                                                                \
    if (p.base == (struct T##__block__ *) 0)                            \
      {                                                                 \
        /* The list is null. Allocate a `leaf' block of the */          \
        /* start size.                                      */          \
        typedef VLIST_BLOCK__ (ENTRY_T, (START_SIZE)) block;            \
        q.offset = 0;                                                   \
        q.base = x_gc_malloc (sizeof (block));                          \
        q.base->size = (START_SIZE);                                    \
        q.base->data[0] = e;                                            \
      }                                                                 \
    else if (p.offset == p.base->last_used)                             \
      {                                                                 \
        if (p.offset + 1 < p.base->size)                                \
          {                                                             \
            /* The current block has room. Append an entry to it. */    \
            q.base = p.base;                                            \
            q.base->last_used++;                                        \
            q.offset = q.base->last_used;                               \
            q.base->data[q.offset] = e;                                 \
          }                                                             \
        else                                                            \
          {                                                             \
            /* The current block size would be exceeded. Allocate a */  \
            /* new block of increased size; link it to the current  */  \
            /* block.                                               */  \
            q.offset = 0;                                               \
            const size_t size =                                         \
              (p.base->size                                             \
               <= SMCORE_VLIST_MAX_BLOCK_SIZE / (R_INVERSE)) ?          \
              ((R_INVERSE) * p.base->size) :                            \
              SMCORE_VLIST_MAX_BLOCK_SIZE;                              \
            typedef VLIST_BLOCK__ (ENTRY_T, size) block;                \
            q.base = x_gc_malloc (sizeof (block));                      \
            q.base->base = p.base;                                      \
            q.base->offset = p.offset;                                  \
            q.base->size = size;                                        \
            q.base->data[0] = e;                                        \
          }                                                             \
      }                                                                 \
    else                                                                \
      {                                                                 \
        /* We are using the `interior' of a block as a shared tail.  */ \
        /* Allocate a block of the start size; link it to the        */ \
        /* current block. */                                            \
        typedef VLIST_BLOCK__ (ENTRY_T, (START_SIZE)) block;            \
        q.offset = 0;                                                   \
        q.base = x_gc_malloc (sizeof (block));                          \
        q.base->base = p.base;                                          \
        q.base->offset = p.offset;                                      \
        q.base->size = 1;                                               \
        q.base->data[0] = e;                                            \
      }                                                                 \
    return q;                                                           \
  }

/*
 * // To create a function @code{int intvlst_ref (intvlst_t, size_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_REF (intvlst_ref, intvlst_t, int);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_REF (intvlst_ref, intvlst_t, int);
 */
#define DECLARE_SMCORE_VLIST_REF(NAME, T, ENTRY_T)                      \
  inline ENTRY_T NAME (T, size_t);                                      \
  inline ENTRY_T                                                        \
  NAME (T p, size_t i)                                                  \
  {                                                                     \
    SMCORE_VLIST_ASSERT (p.base != (struct T##__block__ *) 0);          \
    while (p.offset < i)                                                \
      {                                                                 \
        i -= p.offset + 1;                                              \
        p.offset = p.base->offset;                                      \
        p.base = p.base->base;                                          \
        SMCORE_VLIST_ASSERT (p.base != (struct T##__block__ *) 0);      \
      }                                                                 \
    return (p.base->data[p.offset - i]);                                \
  }
#define DEFINE_SMCORE_VLIST_REF(NAME, T, ENTRY_T)       \
  ENTRY_T NAME (T, size_t);

/*
 * // To create a function @code{const int *intvlst_ptr (intvlst_t, size_t)}
 * // that is like intvlst_ref() but returns a const pointer to the entry,
 * // rather than its value, or returns NULL if the index is out of range.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_PTR (intvlst_ptr, intvlst_t, int);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_PTR (intvlst_ptr, intvlst_t, int);
 */
#define DECLARE_SMCORE_VLIST_PTR(NAME, T, ENTRY_T)              \
  inline const ENTRY_T *NAME (T, size_t);                       \
  inline const ENTRY_T *                                        \
  NAME (T p, size_t i)                                          \
  {                                                             \
    while (p.base != (struct T##__block__ *) 0 && p.offset < i) \
      {                                                         \
        i -= p.offset + 1;                                      \
        p.offset = p.base->offset;                              \
        p.base = p.base->base;                                  \
      }                                                         \
    return (p.base != (struct T##__block__ *) 0) ?              \
      (&p.base->data[p.offset - i]) : ((const ENTRY_T *) 0);    \
  }
#define DEFINE_SMCORE_VLIST_PTR(NAME, T, ENTRY_T)       \
  const ENTRY_T *NAME (T, size_t);

/*
 * // To create a function @code{size_t intvlst_length (intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_LENGTH (intvlst_length, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_LENGTH (intvlst_length, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_LENGTH(NAME, T)    \
  inline size_t NAME (T);                       \
  inline size_t                                 \
  NAME (T p)                                    \
  {                                             \
    size_t len = 0;                             \
    while (p.base != (struct T##__block__ *) 0) \
      {                                         \
        len += p.offset + 1;                    \
        p.offset = p.base->offset;              \
        p.base = p.base->base;                  \
      }                                         \
    return len;                                 \
  }
#define DEFINE_SMCORE_VLIST_LENGTH(NAME, T)     \
  size_t NAME (T);

/*
 * // To create a function @code{intvlst_t intvlst_copy (intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_COPY (intvlst_copy, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_COPY (intvlst_copy, intvlst_t, intvlst_cons);
 */
#define DECLARE_SMCORE_VLIST_COPY(NAME, T)      \
  T NAME (T);
#define DEFINE_SMCORE_VLIST_COPY(NAME, T, CONS)                         \
  T                                                                     \
  NAME (T p)                                                            \
  {                                                                     \
    T q = { (struct T##__block__ *) 0, 0 };                             \
                                                                        \
    if (p.base != (struct T##__block__ *) 0)                            \
      {                                                                 \
        /* Build a list of the blocks in reverse. */                    \
        struct blk {                                                    \
          struct T##__block__ *base;                                    \
          size_t offset;                                                \
          struct blk *next;                                             \
        };                                                              \
        struct blk *bp = x_gc_malloc (sizeof (struct blk));             \
        bp->base = p.base;                                              \
        bp->offset = p.offset;                                          \
        while (p.base->base != (struct T##__block__ *) 0)               \
          {                                                             \
            p.offset = p.base->offset;                                  \
            p.base = p.base->base;                                      \
            struct blk *bp1 = x_gc_malloc (sizeof (struct blk));        \
            bp1->base = p.base;                                         \
            bp1->offset = p.offset;                                     \
            bp1->next = bp;                                             \
            bp = bp1;                                                   \
          }                                                             \
                                                                        \
        /* Use that list of blocks to build the vlist by consing. */    \
        while (bp != (struct blk *) 0)                                  \
          {                                                             \
            for (size_t i = 0; i <= bp->offset; i++)                    \
              q = CONS (bp->base->data[i], q);                          \
            bp = bp->next;                                              \
          }                                                             \
      }                                                                 \
                                                                        \
    return q;                                                           \
  }

/*
 * // To create a function @code{intvlst_t intvlst_reverse (intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_REVERSE (intvlst_reverse, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_REVERSE (intvlst_reverse, intvlst_t, intvlst_cons);
 */
#define DECLARE_SMCORE_VLIST_REVERSE(NAME, T)   \
  T NAME (T);
#define DEFINE_SMCORE_VLIST_REVERSE(NAME, T, CONS)      \
  T                                                     \
  NAME (T p)                                            \
  {                                                     \
    T q = { (struct T##__block__ *) 0, 0 };             \
    while (p.base != (struct T##__block__ *) 0)         \
      {                                                 \
        q = CONS (p.base->data[p.offset], q);           \
        if (p.offset == 0)                              \
          {                                             \
            p.offset = p.base->offset;                  \
            p.base = p.base->base;                      \
          }                                             \
        else                                            \
          p.offset--;                                   \
      }                                                 \
    return q;                                           \
  }

/*
 * // To create a function
 * // @code{intvlst_t intvlst_drop (intvlst_t, size_t, bool *overflow)}.
 *
 * The writer of this code dislikes it when a function with a return
 * value has a side effect, like `overflow' in this case, but at least
 * in this case the side effect is optional.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_DROP (intvlst_drop, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_DROP (intvlst_drop, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_DROP(NAME, T)      \
  T NAME (T, size_t, bool *);
#define DEFINE_SMCORE_VLIST_DROP(NAME, T)                       \
  T                                                             \
  NAME (T p, size_t n, bool *overflow)                          \
  {                                                             \
    while (p.base != (struct T##__block__ *) 0 && p.offset < n) \
      {                                                         \
        n -= p.offset + 1;                                      \
        p.offset = p.base->offset;                              \
        p.base = p.base->base;                                  \
      }                                                         \
    if (p.base == (struct T##__block__ *) 0)                    \
      {                                                         \
        if (overflow != NULL)                                   \
          *overflow = (n != 0);                                 \
      }                                                         \
    else                                                        \
      {                                                         \
        if (overflow != NULL)                                   \
          *overflow = false;                                    \
        p.offset -= n;                                          \
      }                                                         \
    return p;                                                   \
  }

/*
 * // To create a function
 * // @code{intvlst_t intvlst_take (intvlst_t, size_t, bool *overflow)}.
 *
 * The writer of this code dislikes it when a function with a return
 * value has a side effect, like `overflow' in this case, but at least
 * in this case the side effect is optional.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_TAKE (intvlst_take, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_TAKE (intvlst_take, intvlst_t, intvlst_cons);
 *
 * FIXME: This operation could be done without the `reverse' operation
 * at the end, by something like the way we do `reverse-order
 * iteration'.
 */
#define DECLARE_SMCORE_VLIST_TAKE(NAME, T)      \
  T NAME (T, size_t, bool *);
#define DEFINE_SMCORE_VLIST_TAKE(NAME, T, CONS)                 \
  T                                                             \
  NAME (T p, size_t n, bool *overflow)                          \
  {                                                             \
    T q = { (struct T##__block__ *) 0, 0 };                     \
    while (p.base != (struct T##__block__ *) 0 && 0 < n)        \
      {                                                         \
        q = CONS (p.base->data[p.offset], q);                   \
        if (p.offset == 0)                                      \
          {                                                     \
            p.offset = p.base->offset;                          \
            p.base = p.base->base;                              \
          }                                                     \
        else                                                    \
          p.offset--;                                           \
        n--;                                                    \
      }                                                         \
                                                                \
    if (overflow != NULL)                                       \
      *overflow = (0 < n);                                      \
                                                                \
    /* Reverse the result. */                                   \
    T r = { (struct T##__block__ *) 0, 0 };                     \
    while (q.base != (struct T##__block__ *) 0)                 \
      {                                                         \
        r = CONS (q.base->data[q.offset], r);                   \
        if (q.offset == 0)                                      \
          {                                                     \
            q.offset = q.base->offset;                          \
            q.base = q.base->base;                              \
          }                                                     \
        else                                                    \
          q.offset--;                                           \
      }                                                         \
    return r;                                                   \
  }

/*
 * // To create a function
 * // @code{intvlst_t intvlst_drop_right (intvlst_t, size_t, bool *overflow)}.
 *
 * The writer of this code dislikes it when a function with a return
 * value has a side effect, like `overflow' in this case, but at least
 * in this case the side effect is optional.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_DROP_RIGHT (intvlst_drop_right, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_DROP_RIGHT (intvlst_drop_right, intvlst_t,
 *                                     intvlst_take);
 */
#define DECLARE_SMCORE_VLIST_DROP_RIGHT(NAME, T)        \
  T NAME (T, size_t, bool *);
#define DEFINE_SMCORE_VLIST_DROP_RIGHT(NAME, T, TAKE)   \
  T                                                     \
  NAME (T p, size_t n, bool *overflow)                  \
  {                                                     \
    size_t len = 0;                                     \
    T q = p;                                            \
    while (q.base != (struct T##__block__ *) 0)         \
      {                                                 \
        len += q.offset + 1;                            \
        q.offset = q.base->offset;                      \
        q.base = q.base->base;                          \
      }                                                 \
                                                        \
    if (overflow != NULL)                               \
      *overflow = (len < n);                            \
                                                        \
    if (len < n)                                        \
      {                                                 \
        p.base = (struct T##__block__ *) 0;             \
        p.offset = 0;                                   \
      }                                                 \
    else                                                \
      p = TAKE (p, len - n, NULL);                      \
                                                        \
    return p;                                           \
  }

/*
 * // To create a function
 * // @code{intvlst_t intvlst_take_right (intvlst_t, size_t, bool *overflow)}.
 *
 * The writer of this code dislikes it when a function with a return
 * value has a side effect, like `overflow' in this case, but at least
 * in this case the side effect is optional.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_TAKE_RIGHT (intvlst_take_right, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_TAKE_RIGHT (intvlst_take_right, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_TAKE_RIGHT(NAME, T)        \
  T NAME (T, size_t, bool *);
#define DEFINE_SMCORE_VLIST_TAKE_RIGHT(NAME, T) \
  T                                             \
  NAME (T p, size_t n, bool *overflow)          \
  {                                             \
    size_t len = 0;                             \
    T q = p;                                    \
    while (q.base != (struct T##__block__ *) 0) \
      {                                         \
        len += q.offset + 1;                    \
        q.offset = q.base->offset;              \
        q.base = q.base->base;                  \
      }                                         \
    if (overflow != NULL)                       \
      *overflow = (len < n);                    \
    if (n <= len)                               \
      {                                         \
        n = len - n;                            \
        while (p.offset < n)                    \
          {                                     \
            n -= p.offset + 1;                  \
            p.offset = p.base->offset;          \
            p.base = p.base->base;              \
          }                                     \
        p.offset -= n;                          \
      }                                         \
    return p;                                   \
  }

/*
 * // To create a function
 * // @code{void intvlst_split_at (intvlst_t, size_t,
 *                                 intvlst_t *, intvlst_t *,
 *                                 bool *overflow)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_SPLIT_AT (intvlst_split_at, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_SPLIT_AT (intvlst_split_at, intvlst_t,
 *                                   intvlst_cons);
 *
 * FIXME: This operation could be done without the `reverse' operation
 * at the end, by something like the way we do `reverse-order
 * iteration'.
 */
#define DECLARE_SMCORE_VLIST_SPLIT_AT(NAME, T)  \
  void NAME (T, size_t, T *, T *, bool *);
#define DEFINE_SMCORE_VLIST_SPLIT_AT(NAME, T, CONS)             \
  void                                                          \
  NAME (T p, size_t n, T *q1, T *q2, bool *overflow)            \
  {                                                             \
    T q = { (struct T##__block__ *) 0, 0 };                     \
    while (p.base != (struct T##__block__ *) 0 && 0 < n)        \
      {                                                         \
        q = CONS (p.base->data[p.offset], q);                   \
        if (p.offset == 0)                                      \
          {                                                     \
            p.offset = p.base->offset;                          \
            p.base = p.base->base;                              \
          }                                                     \
        else                                                    \
          p.offset--;                                           \
        n--;                                                    \
      }                                                         \
                                                                \
    if (overflow != NULL)                                       \
      *overflow = (0 < n);                                      \
                                                                \
    /* Reverse the result. */                                   \
    T r = { (struct T##__block__ *) 0, 0 };                     \
    while (q.base != (struct T##__block__ *) 0)                 \
      {                                                         \
        r = CONS (q.base->data[q.offset], r);                   \
        if (q.offset == 0)                                      \
          {                                                     \
            q.offset = q.base->offset;                          \
            q.base = q.base->base;                              \
          }                                                     \
        else                                                    \
          q.offset--;                                           \
      }                                                         \
                                                                \
    *q1 = r;                                                    \
    *q2 = p;                                                    \
  }

/*
 * // To create a function @code{intvlst_t intvlst_last_pair (intvlst_t)}.
 * //
 * // If the input list is null, return the null vlist. Otherwise return
 * // the last `pseudopair', that is, the sublist comprising just the
 * // last element of the list.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_LAST_PAIR (intvlst_last_pair, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_LAST_PAIR (intvlst_last_pair, intvlst_t);
 */
#define DECLARE_SMCORE_VLIST_LAST_PAIR(NAME, T)                 \
  inline T NAME (T);                                            \
  inline T                                                      \
  NAME (T p)                                                    \
  {                                                             \
    if (p.base != (struct T##__block__ *) 0)                    \
      {                                                         \
        while (p.base->base != (struct T##__block__ *) 0)       \
          p.base = p.base->base;                                \
        p.offset = 0;                                           \
      }                                                         \
    return p;                                                   \
  }
#define DEFINE_SMCORE_VLIST_LAST_PAIR(NAME, T)  \
  T NAME (T);

/*
 * // To create a function
 * // @code{intvlst_t intvlst_append (intvlst_t, intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_APPEND (intvlst_append, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_APPEND (intvlst_append, intvlst_t, intvlst_cons);
 */
#define DECLARE_SMCORE_VLIST_APPEND(NAME, T)    \
  T NAME (T, T);
#define DEFINE_SMCORE_VLIST_APPEND(NAME, T, CONS)                       \
  T                                                                     \
  NAME (T p, T q)                                                       \
  {                                                                     \
    if (q.base == (struct T##__block__ *) 0)                            \
      q = p;                                                            \
    else                                                                \
      {                                                                 \
        if (p.base != (struct T##__block__ *) 0)                        \
          {                                                             \
            /* Build a list of the blocks in reverse. */                \
            struct blk {                                                \
              struct T##__block__ *base;                                \
              size_t offset;                                            \
              struct blk *next;                                         \
            };                                                          \
            struct blk *bp = x_gc_malloc (sizeof (struct blk));         \
            bp->base = p.base;                                          \
            bp->offset = p.offset;                                      \
            while (p.base->base != (struct T##__block__ *) 0)           \
              {                                                         \
                p.offset = p.base->offset;                              \
                p.base = p.base->base;                                  \
                struct blk *bp1 = x_gc_malloc (sizeof (struct blk));    \
                bp1->base = p.base;                                     \
                bp1->offset = p.offset;                                 \
                bp1->next = bp;                                         \
                bp = bp1;                                               \
              }                                                         \
                                                                        \
            /* Use that list of blocks to build the vlist by */         \
            /* consing.                                      */         \
            while (bp != (struct blk *) 0)                              \
              {                                                         \
                for (size_t i = 0; i <= bp->offset; i++)                \
                  q = CONS (bp->base->data[i], q);                      \
                bp = bp->next;                                          \
              }                                                         \
          }                                                             \
      }                                                                 \
    return q;                                                           \
  }

/*
 * // To create a function
 * // @code{intvlst_t intvlst_append_reverse (intvlst_t, intvlst_t)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_APPEND_REVERSE (intvlst_append_reverse, intvlst_t);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_APPEND_REVERSE (intvlst_append_reverse, intvlst_t,
 *                                         intvlst_cons);
 */
#define DECLARE_SMCORE_VLIST_APPEND_REVERSE(NAME, T)    \
  T NAME (T, T);
#define DEFINE_SMCORE_VLIST_APPEND_REVERSE(NAME, T, CONS)       \
  T                                                             \
  NAME (T p, T q)                                               \
  {                                                             \
    while (p.base != (struct T##__block__ *) 0)                 \
      {                                                         \
        q = CONS (p.base->data[p.offset], q);                   \
        if (p.offset == 0)                                      \
          {                                                     \
            p.offset = p.base->offset;                          \
            p.base = p.base->base;                              \
          }                                                     \
        else                                                    \
          {                                                     \
            p.offset = p.offset - 1;                            \
            p.base = p.base;                                    \
          };                                                    \
      }                                                         \
    return q;                                                   \
  }

/*
 * // To create a function
 * // @code{intvlst_t intvlst_make (size_t, int)}.
 *
 * // In .h file.
 * DECLARE_SMCORE_VLIST_MAKE (intvlst_make, int);
 *
 * // In .c file.
 * __attribute__((__visibility__("default")))
 *     DEFINE_SMCORE_VLIST_MAKE (intvlst_make, int);
 */
#define DECLARE_SMCORE_VLIST_MAKE(NAME, T, ENTRY_T)     \
  T NAME (size_t n, ENTRY_T);
#define DEFINE_SMCORE_VLIST_MAKE(NAME, T, ENTRY_T, CONS)        \
  T                                                             \
  NAME (size_t n, ENTRY_T fill)                                 \
  {                                                             \
    T p = { (struct T##__block__ *) 0, 0 };                     \
    for (size_t i = 0; i < n; i++)                              \
      p = CONS (fill, p);                                       \
    return p;                                                   \
  }

/*
 * Reverse-order iteration.
 *
 * // In .h file.
 * DEFINE_SMCORE_VLIST_RITER(, intvlst_riter_t,
 *                           intvlst_riter_make, intvlst_riter_car,
 *                           intvlst_riter_cdr, intvlst_riter_pair,
 *                           intvlst_riter_next, intvlst_t, int);
 *
 * // In .c file.
 * DECLARE_SMCORE_VLIST_RITER(__attribute__((__visibility__("default"))),
 *                            intvlst_riter_t,
 *                            intvlst_riter_make, intvlst_riter_car,
 *                            intvlst_riter_cdr, intvlst_riter_pair,
 *                            intvlst_riter_next, intvlst_t, int);
 *
 * // Or, in .c file.
 * DEFINE_SMCORE_VLIST_RITER(static, intvlst_riter_t,
 *                           intvlst_riter_make, intvlst_riter_car,
 *                           intvlst_riter_cdr, intvlst_riter_pair,
 *                           intvlst_riter_next, intvlst_t, int);
 * DECLARE_SMCORE_VLIST_RITER(static, intvlst_riter_t,
 *                            intvlst_riter_make, intvlst_riter_car,
 *                            intvlst_riter_cdr, intvlst_riter_pair,
 *                            intvlst_riter_next, intvlst_t, int);
 *
 * // Example of use:
 * intvlst_t p = intvlst_cons (1, intvlst_cons (2, intvlst_null ()));
 * for (intvlst_riter_t ri = intvlst_riter_make (p);
 *      ri != NULL;
 *      ri = intvlst_riter_next (ri))
 *   printf ("%d\n", intvlst_riter_car (ri));
 *
 */
#define DECLARE_SMCORE_VLIST_RITER(MODIFIER, RITER_T, RITERMAKE,        \
                                   RITERCAR, RITERCDR, RITERPAIR,       \
                                   RITERNEXT, T, ENTRY_T)               \
                                                                        \
  /* An abstract pointer type. */                                       \
  typedef struct T##__riter__ *RITER_T;                                 \
                                                                        \
  /* Make a new reverse-order iterator. */                              \
  MODIFIER RITER_T RITERMAKE (T);                                       \
                                                                        \
  /* Return the current car. */                                         \
  MODIFIER ENTRY_T RITERCAR (RITER_T);                                  \
                                                                        \
  /* Return the current cdr. */                                         \
  MODIFIER T RITERCDR (RITER_T);                                        \
                                                                        \
  /* Return the current `pseudopair'. */                                \
  MODIFIER T RITERPAIR (RITER_T);                                       \
                                                                        \
  /* Move the iterator to the next position; */                         \
  /* return a null pointer if done.          */                         \
  MODIFIER RITER_T RITERNEXT (RITER_T)
/* */
#define DEFINE_SMCORE_VLIST_RITER(MODIFIER, RITER_T, RITERMAKE,         \
                                  RITERCAR, RITERCDR, RITERPAIR,        \
                                  RITERNEXT, T, ENTRY_T)                \
                                                                        \
  /* Make a new reverse-order iterator. */                              \
  MODIFIER RITER_T                                                      \
  RITERMAKE (T p)                                                       \
  {                                                                     \
    RITER_T ri;                                                         \
    if (p.base == (struct T##__block__ *) 0)                            \
      ri = (RITER_T) 0;                                                 \
    else                                                                \
      {                                                                 \
        /* Build a list of the blocks in reverse. */                    \
        struct blk {                                                    \
          struct T##__block__ *base;                                    \
          size_t offset;                                                \
          size_t i;                                                     \
          struct blk *next;                                             \
        };                                                              \
        struct blk *bp = x_gc_malloc (sizeof (struct blk));             \
        bp->base = p.base;                                              \
        bp->offset = p.offset;                                          \
        while (p.base->base != (struct T##__block__ *) 0)               \
          {                                                             \
            p.offset = p.base->offset;                                  \
            p.base = p.base->base;                                      \
            struct blk *bp1 = x_gc_malloc (sizeof (struct blk));        \
            bp1->base = p.base;                                         \
            bp1->offset = p.offset;                                     \
            bp1->next = bp;                                             \
            bp = bp1;                                                   \
          }                                                             \
        ri = (RITER_T) bp;                                              \
      }                                                                 \
    return ri;                                                          \
  }                                                                     \
                                                                        \
  /* Return the current car. */                                         \
  MODIFIER ENTRY_T                                                      \
  RITERCAR (RITER_T ri)                                                 \
  {                                                                     \
    SMCORE_VLIST_ASSERT (ri != (RITER_T) 0);                            \
    struct blk {                                                        \
      struct T##__block__ *base;                                        \
      size_t offset;                                                    \
      size_t i;                                                         \
      struct blk *next;                                                 \
    };                                                                  \
    struct blk *bp = (struct blk *) ri;                                 \
    return bp->base->data[bp->i];                                       \
  }                                                                     \
                                                                        \
  /* Return the current cdr. */                                         \
  MODIFIER T                                                            \
  RITERCDR (RITER_T ri)                                                 \
  {                                                                     \
    SMCORE_VLIST_ASSERT (ri != (RITER_T) 0);                            \
    struct blk {                                                        \
      struct T##__block__ *base;                                        \
      size_t offset;                                                    \
      size_t i;                                                         \
      struct blk *next;                                                 \
    };                                                                  \
    struct blk *bp = (struct blk *) ri;                                 \
    T p;                                                                \
    if (bp->i == 0)                                                     \
      {                                                                 \
        p.base = bp->base->base;                                        \
        p.offset = bp->base->offset;                                    \
      }                                                                 \
    else                                                                \
      {                                                                 \
        p.base = bp->base;                                              \
        p.offset = bp->i - 1;                                           \
      }                                                                 \
    return p;                                                           \
  }                                                                     \
                                                                        \
  /* Return the current `pseudopair'; that is, the tail   */            \
  /* including the `car'.                                 */            \
  MODIFIER T                                                            \
  RITERPAIR (RITER_T ri)                                                \
  {                                                                     \
    SMCORE_VLIST_ASSERT (ri != (RITER_T) 0);                            \
    struct blk {                                                        \
      struct T##__block__ *base;                                        \
      size_t offset;                                                    \
      size_t i;                                                         \
      struct blk *next;                                                 \
    };                                                                  \
    struct blk *bp = (struct blk *) ri;                                 \
    T p = { bp->base, bp->i };                                          \
    return p;                                                           \
  }                                                                     \
                                                                        \
  /* Move the iterator to the next position; */                         \
  /* return a null pointer if done.          */                         \
  MODIFIER RITER_T                                                      \
  RITERNEXT (RITER_T ri)                                                \
  {                                                                     \
    RITER_T rn;                                                         \
    if (ri == (RITER_T) 0)                                              \
      rn = (RITER_T) 0;                                                 \
    else                                                                \
      {                                                                 \
        struct blk {                                                    \
          struct T##__block__ *base;                                    \
          size_t offset;                                                \
          size_t i;                                                     \
          struct blk *next;                                             \
        };                                                              \
        struct blk *bp = (struct blk *) ri;                             \
        if (bp->i == bp->offset)                                        \
          rn = (RITER_T) bp->next;                                      \
        else                                                            \
          {                                                             \
            bp->i++;                                                    \
            rn = (RITER_T) bp;                                          \
          }                                                             \
      }                                                                 \
    return rn;                                                          \
  }

/*--------------------------------------------------------------------*/

/*
 * Effort-savers that do a whole lot at once.
 */

#define DECLARE_SMCORE_VLIST_DATATYPE(MODIFIER, TNAME, ENTRY_T)         \
  DECLARE_SMCORE_VLIST_T (TNAME##_t, ENTRY_T);                          \
  MODIFIER DECLARE_SMCORE_VLIST_NULL (TNAME##_null, TNAME##_t);         \
  MODIFIER DECLARE_SMCORE_VLIST_IS_NULL (TNAME##_is_null, TNAME##_t);   \
  MODIFIER DECLARE_SMCORE_VLIST_IS_EQ (TNAME##_is_eq, TNAME##_t);       \
  MODIFIER DECLARE_SMCORE_VLIST_CAR (TNAME##_car, TNAME##_t, ENTRY_T);  \
  MODIFIER DECLARE_SMCORE_VLIST_CDR (TNAME##_cdr, TNAME##_t);           \
  MODIFIER DECLARE_SMCORE_VLIST_CONS (TNAME##_cons, TNAME##_t,          \
                                      ENTRY_T);                         \
  MODIFIER DECLARE_SMCORE_VLIST_REF (TNAME##_ref, TNAME##_t, ENTRY_T);  \
  MODIFIER DECLARE_SMCORE_VLIST_PTR (TNAME##_ptr, TNAME##_t, ENTRY_T);  \
  MODIFIER DECLARE_SMCORE_VLIST_LENGTH (TNAME##_length, TNAME##_t);     \
  MODIFIER DECLARE_SMCORE_VLIST_COPY (TNAME##_copy, TNAME##_t);         \
  MODIFIER DECLARE_SMCORE_VLIST_REVERSE (TNAME##_reverse, TNAME##_t);   \
  MODIFIER DECLARE_SMCORE_VLIST_DROP (TNAME##_drop, TNAME##_t);         \
  MODIFIER DECLARE_SMCORE_VLIST_TAKE (TNAME##_take, TNAME##_t);         \
  MODIFIER DECLARE_SMCORE_VLIST_SPLIT_AT (TNAME##_split_at, TNAME##_t); \
  MODIFIER DECLARE_SMCORE_VLIST_DROP_RIGHT (TNAME##_drop_right,         \
                                            TNAME##_t);                 \
  MODIFIER DECLARE_SMCORE_VLIST_TAKE_RIGHT (TNAME##_take_right,         \
                                            TNAME##_t);                 \
  MODIFIER DECLARE_SMCORE_VLIST_LAST_PAIR (TNAME##_last_pair,           \
                                           TNAME##_t);                  \
  MODIFIER DECLARE_SMCORE_VLIST_APPEND (TNAME##_append, TNAME##_t);     \
  MODIFIER DECLARE_SMCORE_VLIST_APPEND_REVERSE (TNAME##_append_reverse, \
                                                TNAME##_t);             \
  MODIFIER DECLARE_SMCORE_VLIST_MAKE (TNAME##_make, TNAME##_t,          \
                                      ENTRY_T);                         \
  DECLARE_SMCORE_VLIST_RITER(MODIFIER, TNAME##_riter_t,                 \
                             TNAME##_riter_make, TNAME##_riter_car,     \
                             TNAME##_riter_cdr, TNAME##_riter_pair,     \
                             TNAME##_riter_next, TNAME##_t, ENTRY_T)

#define DEFINE_SMCORE_VLIST_DATATYPE(MODIFIER, TNAME, ENTRY_T,          \
                                     R_INVERSE, START_SIZE)             \
  MODIFIER DEFINE_SMCORE_VLIST_NULL (TNAME##_null, TNAME##_t);          \
  MODIFIER DEFINE_SMCORE_VLIST_IS_NULL (TNAME##_is_null, TNAME##_t);    \
  MODIFIER DEFINE_SMCORE_VLIST_IS_EQ (TNAME##_is_eq, TNAME##_t);        \
  MODIFIER DEFINE_SMCORE_VLIST_CAR (TNAME##_car, TNAME##_t, ENTRY_T);   \
  MODIFIER DEFINE_SMCORE_VLIST_CDR (TNAME##_cdr, TNAME##_t);            \
  MODIFIER DEFINE_SMCORE_VLIST_CONS (TNAME##_cons, TNAME##_t, ENTRY_T,  \
                                     R_INVERSE, START_SIZE);            \
  MODIFIER DEFINE_SMCORE_VLIST_REF (TNAME##_ref, TNAME##_t, ENTRY_T);   \
  MODIFIER DEFINE_SMCORE_VLIST_PTR (TNAME##_ptr, TNAME##_t, ENTRY_T);   \
  MODIFIER DEFINE_SMCORE_VLIST_LENGTH (TNAME##_length, TNAME##_t);      \
  MODIFIER DEFINE_SMCORE_VLIST_COPY (TNAME##_copy, TNAME##_t,           \
                                     TNAME##_cons);                     \
  MODIFIER DEFINE_SMCORE_VLIST_REVERSE (TNAME##_reverse, TNAME##_t,     \
                                        TNAME##_cons);                  \
  MODIFIER DEFINE_SMCORE_VLIST_DROP (TNAME##_drop, TNAME##_t);          \
  MODIFIER DEFINE_SMCORE_VLIST_TAKE (TNAME##_take, TNAME##_t,           \
                                     TNAME##_cons);                     \
  MODIFIER DEFINE_SMCORE_VLIST_SPLIT_AT (TNAME##_split_at, TNAME##_t,   \
                                         TNAME##_cons);                 \
  MODIFIER DEFINE_SMCORE_VLIST_DROP_RIGHT (TNAME##_drop_right,          \
                                           TNAME##_t, TNAME##_take);    \
  MODIFIER DEFINE_SMCORE_VLIST_TAKE_RIGHT (TNAME##_take_right,          \
                                           TNAME##_t);                  \
  MODIFIER DEFINE_SMCORE_VLIST_LAST_PAIR (TNAME##_last_pair,            \
                                          TNAME##_t);                   \
  MODIFIER DEFINE_SMCORE_VLIST_APPEND (TNAME##_append, TNAME##_t,       \
                                       TNAME##_cons);                   \
  MODIFIER DEFINE_SMCORE_VLIST_APPEND_REVERSE (TNAME##_append_reverse,  \
                                               TNAME##_t,               \
                                               TNAME##_cons);           \
  MODIFIER DEFINE_SMCORE_VLIST_MAKE (TNAME##_make, TNAME##_t, ENTRY_T,  \
                                     TNAME##_cons);                     \
  DEFINE_SMCORE_VLIST_RITER(MODIFIER, TNAME##_riter_t,                  \
                            TNAME##_riter_make, TNAME##_riter_car,      \
                            TNAME##_riter_cdr, TNAME##_riter_pair,      \
                            TNAME##_riter_next, TNAME##_t, ENTRY_T)

#define DEFINE_SMCORE_VLIST_DATATYPE_WITH_DEFAULTS(MODIFIER, TNAME,     \
                                                   ENTRY_T)             \
  DEFINE_SMCORE_VLIST_DATATYPE (MODIFIER, TNAME, ENTRY_T,               \
                                (SMCORE_VLIST_DEFAULT_R_INVERSE),       \
                                (SMCORE_VLIST_DEFAULT_START_SIZE))

/*--------------------------------------------------------------------*/

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_VLISTS_H_ */
