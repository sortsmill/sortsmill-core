/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_CONVOLUTION_H_
#define SORTSMILL_CORE_CONVOLUTION_H_

#include <sys/types.h>
#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void mpq_convolve (size_t na, ssize_t stride1, mpq_t *a,
                   size_t nb, ssize_t stride2, mpq_t *b,
                   ssize_t c_stride, mpq_t *c);
void f64convolve (size_t na, ssize_t stride1, const double *poly1,
                  size_t nb, ssize_t stride2, const double *poly2,
                  ssize_t result_stride, double *result);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_CONVOLUTION_H_ */
