/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_HOBBY_H_
#define SORTSMILL_CORE_HOBBY_H_

/*
 * John Hobby’s algorithms for piecewise cubic splines.
 */

#include <stdio.h>
#include <complex.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void hobby_tensions_to_control_points (bool perhaps_increase_tensions,
                                       double complex p0, double complex dir0,
                                       double tension0, double tension3,
                                       double complex dir3, double complex p3,
                                       double complex *p1, double complex *p2);
void control_points_to_hobby_tensions (double complex p0, double complex p1,
                                       double complex p2, double complex p3,
                                       double *tension0, double *tension3);
void hobby_guess_directions (double complex start_dir, double complex end_dir,
                             double start_curl, double end_curl,
                             size_t n, const double a_tensions[n - 1],
                             const double b_tensions[n - 1],
                             const double complex points[n],
                             double complex directions[n],
                             int *info, const char **info_message);
void hobby_periodic_guess_directions (size_t n, const double a_tensions[n],
                                      const double b_tensions[n],
                                      const double complex points[n],
                                      double complex directions[n],
                                      int *info, const char **info_message);

typedef enum
{
  /* IMPORTANT: Do not change these values, unless you know what you
     are doing. */
  HOBBY_GUIDE_POINT = 0,
  HOBBY_GUIDE_CYCLE = 1,
  HOBBY_GUIDE_TENSION = 2,
  HOBBY_GUIDE_ATLEAST = 3,
  HOBBY_GUIDE_DIR = 4,
  HOBBY_GUIDE_CURL = 5,
  HOBBY_GUIDE_CTRL = 6
} hobby_guide_token;

enum
{
  /* IMPORTANT: Do not change these values, unless you know what you
     are doing. */
  HOBBY_GUIDE_TOKEN_ERROR = -1,
  HOBBY_GUIDE_SOLUTION_NOT_FOUND = -2
};

void solve_hobby_guide_tokens (size_t n_input,
                               const double input_tokens[3 * n_input],
                               size_t *n_output,
                               double output_tokens[(9 * n_input) / 2],
                               int *info, const char **info_message,
                               size_t *bad_token_index);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_HOBBY_H_ */
