// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Immutable hash maps.
//

#define ATS_PACKNAME "SMCORE.hash_maps"
#define ATS_EXTERN_PREFIX "_smcore_hash_maps__"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

staload BITSTRING_MAPS = "sortsmill/core/SATS/bitstring_maps.sats"

fn {key : t@ype}
hash_map_hash_to_table_index : $BITSTRING_MAPS.bitstring_map_table_indexer key

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// key       The type of a key.
// hash      The type of a hash value (an integer).
// value     The type of a stored value.
// size      The number of key-value pairs stored in the map.

abstype nonnil_hash_map (key : t@ype+, hash : t@ype+, value : t@ype+,
                         size : int) =
  ptr

datatype hash_map (key : t@ype+, hash : t@ype+, value : t@ype+, size : int) =
  | hash_map_nil (key, hash, value, 0) of () (* The NULL pointer. *)
  | {size : pos}
    hash_map_nonnil (key, hash, value, size) of 
      nonnil_hash_map (key, hash, value, size)

typedef hash_map (key, hash, value : t@ype+) =
  [size : nat] hash_map (key, hash, value, size)
typedef hash_map (size : int) =
  [key, hash, value : t@ype] hash_map (key, hash, value, size)
typedef hash_map = [size : nat] hash_map size

typedef nil_hash_map =
  {key, hash, value : t@ype}
  hash_map (key, hash, value, 0)

prfn lemma_hash_map_param :
  {key, hash, value : t@ype} {size : int}
  hash_map (key, hash, value, size) -<prf> [0 <= size] void

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Functions to be implemented by the user of this module.
//

// The second argument is the stored key.
fn {key : t@ype}
hash_map$keys_match : (key, key) -> bool

fn {key, hash : t@ype}
hash_map$hash_function : key -> hash

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Values and operations that do not involve keys.
//

// Return true if the hash_map is empty. This is a fast operation.
fn {}
hash_map_is_nil :
  {key, hash, value : t@ype} {size : int}
  hash_map (key, hash, value, size) -<>
    [b : bool | (size == 0 && b) || (0 < size && ~b)]
    bool b

// Return the size of the hash_map. This is a fast operation.
fn {}
hash_map_size : {size : int} hash_map size -> size_t size

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Values and operations that do involve keys.
//

exception HashMapSubscriptExn of ()

// Try to match a key. Return true if the key was matched.
// Otherwise, return false.
fn {key, hash, value : t@ype}
hash_map_has_key :
  (hash_map (key, hash, value), key) -> bool

// Try to match a key. Return (Some value) if there was a match,
// or (None ()) if there was no match.
fn {key, hash, value : t@ype}
hash_map_get :
  (hash_map (key, hash, value), key) -> Option value

// Try to match a key. Return the value if there was a match.
// Otherwise raise a HashMapSubscriptExn exception.
fn {key, hash, value : t@ype}
hash_map_get_exn :
  (hash_map (key, hash, value), key) -> value

// Try to match a key. Return the value by side effect.
fn {key, hash, value : t@ype}
hash_map_get_eff :
  (hash_map (key, hash, value), key, &value? >> opt (value, b)) ->
    #[b : bool] bool b

// Replace or add a key-value pair, returning a new hash_map.
// The original hash_map is left unchanged. The two hash_maps
// may share structure.
fn {key, hash, value : t@ype}
hash_map_set :
  {orig_size : nat}
  (hash_map (key, hash, value, orig_size), key, value) ->
    [new_size : int | (orig_size == 0 && new_size == 1) ||
                      (0 < orig_size && new_size == orig_size) ||
                      (0 < orig_size && new_size == orig_size + 1)]
    hash_map (key, hash, value, new_size)

// If a key is present, remove the key-value pair, returning a new
// hash_map. The original hash_map is left unchanged.
// The two hash_maps may share structure. If the key is
// not present, return the input hash_map; the implementation
// may (but by no means is required to) return an equivalent
// hash_map at another address.
fn {key, hash, value : t@ype}
hash_map_remove :
  {orig_size : nat}
  (hash_map (key, hash, value, orig_size), key) ->
    [new_size : int | (orig_size == 0 && new_size == 0) ||
                      (0 < orig_size && new_size == orig_size) ||
                      (0 < orig_size && new_size == orig_size - 1)]
    hash_map (key, hash, value, new_size)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Walking through the structure.
//
// Cursors are immutable. The order of the walk should be
// considered indeterminate.
//

abstype nonnil_hash_map_cursor (key : t@ype+, hash : t@ype+, value : t@ype+,
                                addr : addr) =
  ptr addr

datatype hash_map_cursor (key : t@ype+, hash : t@ype+, value : t@ype+,
                          addr : addr) =
  | hash_map_cursor_nil (key, hash, value, null) of () (* The NULL pointer. *)
  | {addr : agz}
    hash_map_cursor_nonnil (key, hash, value, addr) of 
      nonnil_hash_map_cursor (key, hash, value, addr)

typedef hash_map_cursor (key : t@ype+, hash : t@ype+, value : t@ype+) =
  [addr : addr] hash_map_cursor (key, hash, value, addr)
typedef hash_map_cursor =
  [key, hash, value : t@ype] hash_map_cursor (key, hash, value)

typedef nil_hash_map_cursor =
  {key, hash, value : t@ype}
  hash_map_cursor (key, hash, value, null)

prfn lemma_hash_map_cursor_param :
  {key, hash, value : t@ype} {addr : addr}
  hash_map_cursor (key, hash, value, addr) -<prf> [null <= addr] void

// Is the argument the cursor that points to nothing?
fn {}
hash_map_cursor_is_nil :
  {key, hash, value : t@ype} {addr : addr}
  hash_map_cursor (key, hash, value, addr) -<>
    [b : bool | (addr == null && b) || (null < addr && ~b)]
    bool b

// Find the first entry in the map.
fn {key, hash, value : t@ype}
hash_map_first :
  hash_map (key, hash, value) -> hash_map_cursor (key, hash, value)

// Move to the next entry.
fn {key, hash, value : t@ype}
hash_map_next :
  hash_map_cursor (key, hash, value) -> hash_map_cursor (key, hash, value)

exception HashMapCursorNilExn of ()

// Return data from the current cursor position.
fn {key, hash, value : t@ype}
hash_map_pair :
  {addr : addr | addr != null}
  hash_map_cursor (key, hash, value, addr) -> @(key, value)
fn {key, hash, value : t@ype}
hash_map_pair_exn :
  hash_map_cursor (key, hash, value) -> @(key, value)
fn {key, hash, value : t@ype}
hash_map_pair_eff :
  {addr : addr | addr != null}
  (hash_map_cursor (key, hash, value, addr), &key? >> key, &value? >> value) ->
    void
fn {key, hash, value : t@ype}
hash_map_pair_effexn :
  (hash_map_cursor (key, hash, value), &key? >> key, &value? >> value) -> void
fn {key, hash, value : t@ype}
hash_map_key :
  {addr : addr | addr != null}
  hash_map_cursor (key, hash, value, addr) -> key
fn {key, hash, value : t@ype}
hash_map_key_exn :
  hash_map_cursor (key, hash, value) -> key
fn {key, hash, value : t@ype}
hash_map_value :
  {addr : addr | addr != null}
  hash_map_cursor (key, hash, value, addr) -> value
fn {key, hash, value : t@ype}
hash_map_value_exn :
  hash_map_cursor (key, hash, value) -> value

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Modules.
//

abstype modular_hash_map (key : t@ype+, hash : t@ype+, value : t@ype+,
                          size : int) = ptr
typedef modular_hash_map (key : t@ype+, hash : t@ype+, value : t@ype+) =
  [size : nat] modular_hash_map (key, hash, value, size)

typedef hash_map_module (key : t@ype+, hash : t@ype+, value : t@ype+) =
  '{
    // Instance introspection.
    hash_map =
      {size : int}
      modular_hash_map (key, hash, value, size) ->
        hash_map (key, hash, value, size),
    hashf =
      modular_hash_map (key, hash, value) -> key -> hash,
    equalf =
      modular_hash_map (key, hash, value) -> (key, key) -> bool,

    // Operations not involving keys.
    make =
      (key -> hash, (key, key) -> bool) ->
        modular_hash_map (key, hash, value, 0),
    is_nil =
      {size : int}
      modular_hash_map (key, hash, value, size) ->
        [b : bool | (size == 0 && b == true) || (0 < size && b == false)]
        bool b,
    size =
      {size : int}
      modular_hash_map (key, hash, value, size) -> size_t size,

    // Operations involving keys.
    has_key =
      (modular_hash_map (key, hash, value), key) -> bool,
    get =
      (modular_hash_map (key, hash, value), key) -> Option value,
    get_exn =
      (modular_hash_map (key, hash, value), key) -> value,
    get_eff =
      (modular_hash_map (key, hash, value),
       key, &value? >> opt (value, b)) -> #[b : bool] bool b,
    set =
      {orig_size : nat}
      (modular_hash_map (key, hash, value, orig_size), key, value) ->
        [new_size : int | (orig_size == 0 && new_size == 1) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size + 1)]
        modular_hash_map (key, hash, value, new_size),
    remove =
      {orig_size : nat}
      (modular_hash_map (key, hash, value, orig_size), key) ->
        [new_size : int | (orig_size == 0 && new_size == 0) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size - 1)]
        modular_hash_map (key, hash, value, new_size),

    // Cursors.
    cursor_is_nil =
      {addr : addr}
      hash_map_cursor (key, hash, value, addr) -<>
        [b : bool | (addr == null && b == true) || (addr != null && b == false)]
        bool b,
    first =
      modular_hash_map (key, hash, value) -> hash_map_cursor (key, hash, value),
    next =
      hash_map_cursor (key, hash, value) -> hash_map_cursor (key, hash, value),
    pair =
      {addr : addr | addr != null}
      hash_map_cursor (key, hash, value, addr) -> @(key, value),
    pair_exn =
      hash_map_cursor (key, hash, value) -> @(key, value),
    pair_eff =
      {addr : addr | addr != null}
      (hash_map_cursor (key, hash, value, addr),
       &key? >> key,
       &value? >> value) -> void,
    pair_effexn =
      (hash_map_cursor (key, hash, value),
       &key? >> key,
       &value? >> value) -> void,
    key =
      {addr : addr | addr != null}
      hash_map_cursor (key, hash, value, addr) -> key,
    key_exn =
      hash_map_cursor (key, hash, value) -> key,
    value =
      {addr : addr | addr != null}
      hash_map_cursor (key, hash, value, addr) -> value,
    value_exn =
      hash_map_cursor (key, hash, value) -> value
  }

fn {key, hash, value : t@ype+}
make_hash_map_module : () -> hash_map_module (key, hash, value)

//--------------------------------------------------------------------
