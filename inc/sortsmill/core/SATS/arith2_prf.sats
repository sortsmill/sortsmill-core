// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

(*--------------------------------------------------------------------*)

prfun pos_mul_pos_pos :
    {m, n : int | 0 < n} {p : int | 0 < p; m * n == p} () -<prf>
      [0 < m] void

prfun pos_mul_neg_neg :
    {m, n : int | n < 0} {p : int | p < 0; m * n == p} () -<prf>
      [0 < m] void

prfun neg_mul_pos_neg :
    {m, n : int | 0 < n} {p : int | p < 0; m * n == p} () -<prf>
      [m < 0] void

prfun neg_mul_neg_pos :
    {m, n : int | n < 0} {p : int | 0 < p; m * n == p} () -<prf>
      [m < 0] void

(*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

prfun pos_mul_pos_pos2 :
    {m, n : int | 0 < n} {p : int | 0 < p} MUL (m, n, p) -<prf>
      [0 < m] void

prfun pos_mul_neg_neg2 :
    {m, n : int | n < 0} {p : int | p < 0} MUL (m, n, p) -<prf>
      [0 < m] void

prfun neg_mul_pos_neg2 :
    {m, n : int | 0 < n} {p : int | p < 0} MUL (m, n, p) -<prf>
      [m < 0] void

prfun neg_mul_neg_pos2 :
    {m, n : int | n < 0} {p : int | 0 < p} MUL (m, n, p) -<prf>
      [m < 0] void

(*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

prfun mul_eqz_eqz_eqz :
    {x, y : int | x == 0; y == 0} () -<prf> [x * y == 0] void

prfun mul_eqz_neqz_eqz :
    {x, y : int | x == 0; y != 0} () -<prf> [x * y == 0] void

prfun mul_neqz_eqz_eqz :
    {x, y : int | x != 0; y == 0} () -<prf> [x * y == 0] void

prfun mul_neqz_neqz_neqz :
    {x, y : int | x != 0; y != 0} () -<prf> [x * y != 0] void

(*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

prfun mul_eqz_eqz_eqz2 :
    {x, y : int | x == 0; y == 0} {xy : int} MUL (x, y, xy) -<prf>
      [x * y == xy; xy == 0] void

prfun mul_eqz_neqz_eqz2 :
    {x, y : int | x == 0; y != 0} {xy : int} MUL (x, y, xy) -<prf>
      [x * y == xy; xy == 0] void

prfun mul_neqz_eqz_eqz2 :
    {x, y : int | x != 0; y == 0} {xy : int} MUL (x, y, xy) -<prf>
      [x * y == xy; xy == 0] void

prfun mul_neqz_neqz_neqz2 :
    {x, y : int | x != 0; y != 0} {xy : int} MUL (x, y, xy) -<prf>
      [x * y == xy; xy != 0] void

(*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

prfun eqz_eqz_mul_eqz :
    {x, y : int | x * y == 0} () -<prf> [x == 0 || y == 0] void

prfun neqz_neqz_mul_neqz :
    {x, y : int | x * y != 0} () -<prf> [x != 0 && y != 0] void

prfun eqz_eqz_mul_eqz2 :
    {x, y : int} {xy : int | xy == 0} MUL (x, y, xy) -<prf>
      [x * y == xy; x == 0 || y == 0] void

prfun neqz_neqz_mul_neqz2 :
    {x, y : int} {xy : int | xy != 0} MUL (x, y, xy) -<prf>
      [x * y == xy; x != 0 && y != 0] void

(*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

prfun mul_mul_substitute :
    {a, b : int} {c : int} {d, e : int}
    (MUL (a, b, c), MUL (d, e, b)) -<prf> MUL (a, d * e, c)

prfun mul_mul_substitute2 :
    {a, b : int} {c : int} {d, e : int}
    (MUL (a, b, c), MUL (d, e, b)) -<prf> MUL (a * d, e, c)

(*--------------------------------------------------------------------*)

prfun divmod_qualitative :
    {x : nat; y : pos} {q, r : int}
    DIVMOD (x, y, q, r) -<prf>
      [x < y && q == 0 && r == x ||
       x == y && q == 1 && r == 0 ||
       x > y && 1 <= q && r < y]
      void

praxi divmod_remainderless_expand_linear :
    {a, b : int}                (* constants *)
    {x, y : int | y != 0}
    {q : int}
    DIVMOD (x, y, q, 0) -<prf>
      DIVMOD (a * x + b * y, y, a * (x / y) + b, 0)

prfun mul_to_divmod_given_remainder :
    {r : nat} {x : pos; y : nat | r < x} {xy : nat}
    MUL (x, y, xy) -<prf> DIVMOD (xy + r, x, y, r)

prfun mul_to_divmod_given_remainder2 :
    {r : nat} {x : nat; y : pos | r < y} {xy : nat}
    MUL (x, y, xy) -<prf> DIVMOD (xy + r, y, x, r)

prfun mul_to_divmod_remainderless :
    (* If x * y = xy, then xy div y = x, without remainder. *)
    {x : nat; y : pos} {xy : nat}
    MUL (x, y, xy) -<prf> DIVMOD (xy, y, x, 0)

prfun mul_to_mod_remainderless :
    (* If there exists q such that q * y = x, then y divides x. *)
    {x : nat; y : pos}
    ([q : pos] MUL (q, y, x)) -<prf> MOD (x, y, 0)

prfun divisor_hops_from_itself_to_zero :
    {x, y : pos | y <= x}
    MOD (x, y, 0) -<prf> [x == y || 2 * y <= x] void

(*--------------------------------------------------------------------*)

propdef ODD (x : int) = MOD (x, 2, 1)
propdef EVEN (x : int) = MOD (x, 2, 0)

propdef EITHER_OR_BOTH_ODD (x : int, y : int) =
    [i, j : int | i == 0 && j == 1 ||
                  i == 1 && j == 0 ||
                  i == 1 && j == 1]
    (MOD (x, 2, i), MOD (y, 2, j))

praxi odd_negate : {m : int} ODD m -<prf> ODD (~m)
praxi even_negate : {m : int} EVEN m -<prf> EVEN (~m)

prfun positive_power_of_two_times_anything_is_even :
    {p : pos} {s : int} {m : int} EXP2 (p, s) -<prf>
      [sm : int | sgn sm == sgn m] (MUL (s, m, sm), EVEN (sm))

prfun odd_divided_by_positive_power_of_two :
    {m : nat} {p : pos} {s : int}
    (ODD m, EXP2 (p, s)) -<prf> [r : pos] (MOD (m, s, r), ODD r)

prfun other_is_odd :
    {m, n : nat}
    (EITHER_OR_BOTH_ODD (m, n), EVEN m) -<prf> ODD n

(*--------------------------------------------------------------------*)
