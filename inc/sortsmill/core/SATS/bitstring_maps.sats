// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Immutable maps from strings of bits, such as machine integers,
// to key-value pairs.
// The implementation employs array mapped tries and might be used
// to implement such things as ideal hash maps.
//

//
// References.
//
//   Bagwell, Phil. Ideal hash trees. 2001.
//
//   Bagwell, Phil. Fast and space efficient trie searches. 2000.
//

//
// NOTE/WARNING: Regarding POPCOUNT instructions.
//
// The implementation uses smcore_popcount() and so, if possible,
// one ought to enable compiler options to emit POPCOUNT
// instructions. Otherwise a POPCOUNT subroutine might be called or
// inline code used instead.
//
// If you are using GCC and building just for the machine on which you
// are compiling, the ‘-march=native’ option likely will suffice.
//

#define ATS_PACKNAME "SMCORE.bitstring_maps"
#define ATS_EXTERN_PREFIX "_smcore_bitstring_maps__"

%{#
#include "sortsmill/core/CATS/bitstring_maps.cats"
%}

#include "sortsmill/core/CATS/SHARE/bitstring_maps_defines.cats"

//--------------------------------------------------------------------

local

  #define TABLE_SIZE_MAX _smcore_bitstring_maps__TABLE_SIZE_MAX

in

  abstype nonnil_bitstring_map (key : t@ype+, value : t@ype+, size : int) = ptr

  datatype bitstring_map (key : t@ype+, value : t@ype+, size : int) =
    | bitstring_map_nil (key, value, 0) of () (* The NULL pointer. *)
    | {size : pos}
      bitstring_map_nonnil (key, value, size) of 
        nonnil_bitstring_map (key, value, size)

  typedef bitstring_map (key : t@ype+, value : t@ype+) =
    [size : nat] bitstring_map (key, value, size)
  typedef bitstring_map (size : int) =
    [key : t@ype; value : t@ype+] bitstring_map (key, value, size)
  typedef bitstring_map =
    [key : t@ype; value : t@ype+] bitstring_map (key, value)

  typedef nil_bitstring_map =
    {key : t@ype; value : t@ype} bitstring_map (key, value, 0)

  prfn lemma_bitstring_map_param :
    {key : t@ype; value : t@ype} {size : int}
    bitstring_map (key, value, size) -<prf> [0 <= size] void

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // FIXME: Somehow constrain this to ensure there is a
  // level at which i == ~1. Otherwise we cannot guarantee
  // termination.
  typedef bitstring_map_table_indexer (key : t@ype+) =
    {level : nat} (key, size_t level) ->
      [i : int | (level == 0 && 0 <= i) || (0 < level && ~1 <= i);
                 i < TABLE_SIZE_MAX]
      int i

  // key_to_table_index<key> is instantiated separately
  // for each key type. Programmers may define their own
  // functions.
  fn {key : t@ype} bitstring_map$key_to_table_index :
    bitstring_map_table_indexer key

  // Some implementations.
  fn {key : t@ype}
  unsigned_key_to_table_index : bitstring_map_table_indexer key
  fn {key : t@ype}
  signed_key_to_table_index : bitstring_map_table_indexer key

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // The second argument is the stored key.
  typedef bitstring_map_keys_matcher (key : t@ype+) = (key, key) -> bool

  // bitstring_map$keys_match<key> is instantiated separately
  // for each key type. Programmers may define their own
  // functions.
  fn {key : t@ype} bitstring_map$keys_match : bitstring_map_keys_matcher key

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Values and operations that do not involve keys.
  //

  // Return true if the bitstring_map is empty. This is a fast operation.
  fn {}
  bitstring_map_is_nil :
    {key : t@ype; value : t@ype} {size : int}
    bitstring_map (key, value, size) -<>
      [b : bool | (size == 0 && b) || (0 < size && ~b)]
      bool b

  // Return the size of the bitstring_map. This is a fast operation.
  fn {}
  bitstring_map_size :
    {size : int} (bitstring_map size) -> [0 <= size] size_t size

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Values and operations that do involve keys.
  //

  exception BitstringMapSubscriptExn of ()
  exception BitstringMapKeyExhaustedExn of ()
  exception BitstringMapStoredKeyExhaustedExn of ()

  // Try to match a key. Return true if the key was matched.
  // Otherwise, return false.
  fn {key : t@ype; value : t@ype}
  bitstring_map_has_key : (bitstring_map (key, value), key) -> bool

  // Try to match a key. Return (Some value) if there was a match,
  // or (None ()) if there was no match.
  fn {key : t@ype; value : t@ype}
  bitstring_map_get : (bitstring_map (key, value), key) -> Option value

  // Try to match a key. Return the value if there was a match.
  // Otherwise raise a BitstringMapSubscriptExn exception.
  fn {key : t@ype; value : t@ype}
  bitstring_map_get_exn : (bitstring_map (key, value), key) -> value

  // Try to match a key. Return the results by side effects.
  fn {key : t@ype; value : t@ype}
  bitstring_map_get_eff :
    (bitstring_map (key, value), key,
     &value? >> opt (value, b)) ->
      #[b : bool] bool b

  // Replace or add a key-value pair, returning a new bitstring_map.
  // The original bitstring_map is left unchanged. The two bitstring_maps
  // may share structure.
  fn {key : t@ype; value : t@ype}
  bitstring_map_set :
    {orig_size : nat} (bitstring_map (key, value, orig_size), key, value) ->
      [new_size : int | (orig_size == 0 && new_size == 1) ||
                        (0 < orig_size && new_size == orig_size) ||
                        (0 < orig_size && new_size == orig_size + 1)]
      bitstring_map (key, value, new_size)

  // If a key is present, remove the key-value pair, returning a new
  // bitstring_map. The original bitstring_map is left unchanged.
  // The two bitstring_maps may share structure. If the key is
  // not present, return the input bitstring_map; the implementation
  // may (but by no means is required to) return an equivalent
  // bitstring_map at another address.
  fn {key : t@ype; value : t@ype}
  bitstring_map_remove:
    {orig_size : nat}
    (bitstring_map (key, value, orig_size), key) ->
      [new_size : int | (orig_size == 0 && new_size == 0) ||
                        (0 < orig_size && new_size == orig_size) ||
                        (0 < orig_size && new_size == orig_size - 1)]
      bitstring_map (key, value, new_size)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Walking through the structure.
  //
  // (Cursors are immutable.)
  //

  abstype nonnil_bitstring_map_cursor (key : t@ype+, value : t@ype+,
                                       addr : addr) =
    ptr addr

  datatype bitstring_map_cursor (key : t@ype+, value : t@ype+, addr : addr) =
    | bitstring_map_cursor_nil (key, value, null) of () (* The NULL pointer. *)
    | {addr : agz}
      bitstring_map_cursor_nonnil (key, value, addr) of 
        nonnil_bitstring_map_cursor (key, value, addr)

  typedef bitstring_map_cursor (key : t@ype+, value : t@ype+) =
    [addr : addr] bitstring_map_cursor (key, value, addr)
  typedef bitstring_map_cursor =
    [key : t@ype; value : t@ype] bitstring_map_cursor (key, value)

  typedef nil_bitstring_map_cursor =
    {key : t@ype; value : t@ype} bitstring_map_cursor (key, value, null)

  prfn lemma_bitstring_map_cursor_param :
    {key : t@ype; value : t@ype} {addr : addr}
    bitstring_map_cursor (key, value, addr) -<prf> [null <= addr] void

  // Is the argument the cursor that points to nothing?
  fn {}
  bitstring_map_cursor_is_nil :
    {key : t@ype; value : t@ype} {addr : addr}
    bitstring_map_cursor (key, value, addr) -<>
      [b : bool | (addr == null && b == true) || (addr != null && b == false)]
      bool b

  // Find the first entry in the map.
  fn {key : t@ype; value : t@ype}
  bitstring_map_first :
    bitstring_map (key, value) -> bitstring_map_cursor (key, value)

  // Find the last entry in the map.
  fn {key : t@ype; value : t@ype}
  bitstring_map_last :
    bitstring_map (key, value) -> bitstring_map_cursor (key, value)

  // Find the entry having a given key.
  fn {key : t@ype; value : t@ype}
  bitstring_map_find :
    (bitstring_map (key, value), key) -> bitstring_map_cursor (key, value)

  // Move to the next entry.
  fn {key : t@ype; value : t@ype}
  bitstring_map_next :
    bitstring_map_cursor (key, value) -> bitstring_map_cursor (key, value)

  // Move to the previous entry.
  fn {key : t@ype; value : t@ype}
  bitstring_map_prev :
    bitstring_map_cursor (key, value) -> bitstring_map_cursor (key, value)

  exception BitstringMapCursorNilExn of ()

  // Return data from the current cursor position.
  fn {key : t@ype; value : t@ype}
  bitstring_map_pair :
    {addr : addr | addr != null}
    bitstring_map_cursor (key, value, addr) -> @(key, value)
  fn {key : t@ype; value : t@ype}
  bitstring_map_pair_exn :
    bitstring_map_cursor (key, value) -> @(key, value)
  fn {key : t@ype; value : t@ype}
  bitstring_map_pair_eff:
    {addr : addr | addr != null}
    (bitstring_map_cursor (key, value, addr),
     &key? >> key, &value? >> value) ->
      void
  fn {key : t@ype; value : t@ype}
  bitstring_map_pair_effexn :
    (bitstring_map_cursor (key, value),
     &key? >> key, &value? >> value) ->
      void
  fn {key : t@ype; value : t@ype}
  bitstring_map_key :
    {addr : addr | addr != null}
    bitstring_map_cursor (key, value, addr) -> key
  fn {key : t@ype; value : t@ype}
  bitstring_map_key_exn :
    bitstring_map_cursor (key, value) -> key
  fn {key : t@ype; value : t@ype}
  bitstring_map_value :
    {addr : addr | addr != null}
    bitstring_map_cursor (key, value, addr) -> value
  fn {key : t@ype; value : t@ype}
  bitstring_map_value_exn :
    bitstring_map_cursor (key, value) -> value

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Dump to standard output, for debugging.
  //

  fn {key : t@ype; value : t@ype}
  _dump_bitstring_map :
    (bitstring_map (key, value), (key, value) -<cloref1> void) -> void

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

end

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Modules.
//

typedef bitstring_map_module (key : t@ype+, value : t@ype+) =
  '{
    // Operations not involving keys.
    nil = bitstring_map (key, value, 0),
    is_nil =
      {size : int}
      bitstring_map (key, value, size) -<>
        [b : bool | (size == 0 && b == true) || (0 < size && b == false)]
        bool b,
    size =
      {size : int}
      bitstring_map (key, value, size) -> size_t size,

    // Operations involving keys.
    has_key = (bitstring_map (key, value), key) -> bool,
    get = (bitstring_map (key, value), key) -> Option value,
    get_exn = (bitstring_map (key, value), key) -> value,
    get_eff =
      (bitstring_map (key, value), key,
       &value? >> opt (value, b)) ->
        #[b : bool] bool b,
    set =
      {orig_size : nat} 
      (bitstring_map (key, value, orig_size), key, value) ->
        [new_size : int | (orig_size == 0 && new_size == 1) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size + 1)]
        bitstring_map (key, value, new_size),
    remove =
      {orig_size : nat}
      (bitstring_map (key, value, orig_size), key) ->
        [new_size : int | (orig_size == 0 && new_size == 0) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size - 1)]
        bitstring_map (key, value, new_size),

    // Cursors.
    cursor_is_nil =
      {addr : addr}
      bitstring_map_cursor (key, value, addr) -<>
        [b : bool | (addr == null && b == true) || (addr != null && b == false)]
        bool b,
    first =
      bitstring_map (key, value) -> bitstring_map_cursor (key, value),
    last =
      bitstring_map (key, value) -> bitstring_map_cursor (key, value),
    find =
      (bitstring_map (key, value), key) -> bitstring_map_cursor (key, value),
    next =
      bitstring_map_cursor (key, value) -> bitstring_map_cursor (key, value),
    prev =
      bitstring_map_cursor (key, value) -> bitstring_map_cursor (key, value),
    pair =
      {addr : addr | addr != null}
      bitstring_map_cursor (key, value, addr) -> @(key, value),
    pair_exn =
      bitstring_map_cursor (key, value) -> @(key, value),
    pair_effexn =
      (bitstring_map_cursor (key, value), &key? >> key, &value? >> value) ->
        void,
    key =
      {addr : addr | addr != null}
      bitstring_map_cursor (key, value, addr) -> key,
    key_exn =
      bitstring_map_cursor (key, value) -> key,
    value =
      {addr : addr | addr != null}
      bitstring_map_cursor (key, value, addr) -> value,
    value_exn =
      bitstring_map_cursor (key, value) -> value
   }

fn {key, value : t@ype+}
make_bitstring_map_module : () -> bitstring_map_module (key, value)

//--------------------------------------------------------------------
