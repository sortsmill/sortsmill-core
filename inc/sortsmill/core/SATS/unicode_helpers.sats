// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_PACKNAME "SMCORE.unicode_helpers"
#define ATS_EXTERN_PREFIX "_smcore_unicode_helpers__"

%{#
#include "sortsmill/core/CATS/unicode_helpers.cats"
%}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

stadef
is_valid_unicode_code_point (u : int) : bool =
  (0x0 <= u && u < 0xD800) || (0xE000 <= u && u <= 0x10FFFF)

fn {}
is_valid_unicode_code_point :
  {u : int} int u -<>
    [b : bool | b == is_valid_unicode_code_point u]
    bool b

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

stadef
is_extended_utf8_1byte_first_byte (c0 : int) : bool =
  0x00 <= c0 && c0 <= 0x7F

stadef
is_extended_utf8_2byte_first_byte (c0 : int) : bool =
  0xC0 <= c0 && c0 <= 0xDF

stadef
is_extended_utf8_3byte_first_byte (c0 : int) : bool =
  0xE0 <= c0 && c0 <= 0xEF

stadef
is_extended_utf8_4byte_first_byte (c0 : int) : bool =
  0xF0 <= c0 && c0 <= 0xF7

stadef
is_extended_utf8_5byte_first_byte (c0 : int) : bool =
  0xF8 <= c0 && c0 <= 0xFB

stadef
is_extended_utf8_6byte_first_byte (c0 : int) : bool =
  0xFC <= c0 && c0 <= 0xFD

stadef
extended_utf8_character_length (c0 : int) : int =
  ifint (is_extended_utf8_1byte_first_byte c0, 1,
    ifint (is_extended_utf8_2byte_first_byte c0, 2,
      ifint (is_extended_utf8_3byte_first_byte c0, 3,
        ifint (is_extended_utf8_4byte_first_byte c0, 4,
          ifint (is_extended_utf8_5byte_first_byte c0, 5,
            ifint (is_extended_utf8_6byte_first_byte c0, 6, ~1))))))

stadef
extended_utf8_char_length_relation (c0 : int, n : int) : bool =
  (n == 1 && is_extended_utf8_1byte_first_byte c0) ||
  (n == 2 && is_extended_utf8_2byte_first_byte c0) ||
  (n == 3 && is_extended_utf8_3byte_first_byte c0) ||
  (n == 4 && is_extended_utf8_4byte_first_byte c0) ||
  (n == 5 && is_extended_utf8_5byte_first_byte c0) ||
  (n == 6 && is_extended_utf8_6byte_first_byte c0)

prfn
extended_utf8_char_length_relation_to_length :
  {c0 : int}
  {n : int | extended_utf8_char_length_relation (c0, n) ||
             (n == ~1 && ((c0 < 0x00) ||
                          (0x7F < c0 && c0 < 0xC0) ||
                          (0xFD < c0)))}
  () -<prf> [n == extended_utf8_character_length c0] void

prfn
extended_utf8_char_length_to_length_relation :
  {c0 : int}
  {n : int | n == extended_utf8_character_length c0}
  () -<prf>
    [extended_utf8_char_length_relation (c0, n) ||
     (n == ~1 && ((c0 < 0x00) || (0x7F < c0 && c0 < 0xC0) || (0xFD < c0)))]
    void

// extended_utf8_character_length:
//
// Return value = 1, 2, 3, or 4 indicates that there may be a valid
// UTF-8 character, of the given length. It may also be a `valid'
// overlong sequence. Otherwise there definitely is not a valid
// character of any sort starting with the given byte. Return
// value = 5 or 6 indicates the possible start of an `extended
// UTF-8' character of the given length, including code points up
// to 0xffffffff. Return value = ~1 means the byte is not the
// start of a valid sequence.
fn
extended_utf8_character_length :
  {c0 : int | 0x00 <= c0; c0 <= 0xFF} int c0 -<>
    [n : int | n == extended_utf8_character_length c0] int n = "mac#%"

stadef
utf8_character_length (c0 : int) : int =
  ifint (is_extended_utf8_1byte_first_byte c0, 1,
    ifint (is_extended_utf8_2byte_first_byte c0, 2,
      ifint (is_extended_utf8_3byte_first_byte c0, 3,
        ifint (is_extended_utf8_4byte_first_byte c0, 4, ~1))))

stadef
utf8_char_length_relation (c0 : int, n : int) : bool =
  (n == 1 && is_extended_utf8_1byte_first_byte c0) ||
  (n == 2 && is_extended_utf8_2byte_first_byte c0) ||
  (n == 3 && is_extended_utf8_3byte_first_byte c0) ||
  (n == 4 && is_extended_utf8_4byte_first_byte c0)

prfn
utf8_char_length_relation_to_length :
  {c0 : int}
  {n : int | utf8_char_length_relation (c0, n) ||
             (n == ~1 && ((c0 < 0x00) ||
                          (0x7F < c0 && c0 < 0xC0) ||
                          (0xF7 < c0)))}
  () -<prf> [n == utf8_character_length c0] void

prfn
utf8_char_length_to_length_relation :
  {c0 : int}
  {n : int | n == utf8_character_length c0}
  () -<prf>
    [utf8_char_length_relation (c0, n) ||
     (n == ~1 && ((c0 < 0x00) || (0x7F < c0 && c0 < 0xC0) || (0xF7 < c0)))]
    void

fn
utf8_character_length :
  {c0 : int | 0x00 <= c0; c0 <= 0xFF} int c0 -<>
    [n : int | n == utf8_character_length c0] int n = "mac#%"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

stadef
is_valid_utf8_continuation_byte (c : int) : bool =
  0x80 <= c && c <= 0xBF

stadef
is_invalid_utf8_continuation_byte (c : int) : bool =
  c < 0x80 || 0xBF < c

fn {}
is_valid_utf8_continuation_byte :
  {c : int} int c -<>
    [b : bool | b == is_valid_utf8_continuation_byte c] bool b

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

stadef
extended_utf8_char_1byte_decoding (c0 : int) : int =
  c0

stadef
extended_utf8_char_2byte_decoding (c0 : int, c1 : int) : int =
  64 * (c0 - 0xC0) + (c1 - 0x80)

stadef
extended_utf8_char_3byte_decoding (c0 : int, c1 : int, c2 : int) : int =
  64 * 64 * (c0 - 0xE0) + 64 * (c1 - 0x80) + (c2 - 0x80)

stadef
extended_utf8_char_4byte_decoding (c0 : int, c1 : int, c2 : int,
                                   c3 : int) : int =
  64 * 64 * 64 * (c0 - 0xF0) + 64 * 64 * (c1 - 0x80) +
    64 * (c2 - 0x80) + (c3 - 0x80)

stadef
extended_utf8_char_5byte_decoding (c0 : int, c1 : int, c2 : int,
                                   c3 : int, c4 : int) : int =
  64 * 64 * 64 * 64 * (c0 - 0xF8) + 64 * 64 * 64 * (c1 - 0x80) +
    64 * 64 * (c2 - 0x80) + 64 * (c3 - 0x80) + (c4 - 0x80)

stadef
extended_utf8_char_6byte_decoding (c0 : int, c1 : int, c2 : int,
                                   c3 : int, c4 : int, c5 : int) : int =
  64 * 64 * 64 * 64 * 64 * (c0 - 0xFC) + 64 * 64 * 64 * 64 * (c1 - 0x80) +
    64 * 64 * 64 * (c2 - 0x80) + 64 * 64 * (c3 - 0x80) +
       64 * (c4 - 0x80) + (c5 - 0x80)

dataprop EXTENDED_UTF8_CHAR (length : int, u : int, c0 : int, c1 : int,
                             c2 : int, c3 : int, c4 : int, c5 : int) =
  | {u, c0 : int |
     0 <= u; u <= 0x7F;
     is_extended_utf8_1byte_first_byte c0;
     u == extended_utf8_char_1byte_decoding (c0)}
    EXTENDED_UTF8_CHAR_1byte (1, u, c0, ~1, ~1, ~1, ~1, ~1)
    //
  | {u, c0, c1 : int |
     0 <= u; u <= 0x7FF;
     is_extended_utf8_2byte_first_byte c0;
     is_valid_utf8_continuation_byte c1;
     u == extended_utf8_char_2byte_decoding (c0, c1)}
    EXTENDED_UTF8_CHAR_2byte (2, u, c0, c1, ~1, ~1, ~1, ~1)
    //
  | {u, c0, c1, c2 : int |
     0 <= u; u <= 0xFFFF;
     is_extended_utf8_3byte_first_byte c0;
     is_valid_utf8_continuation_byte c1;
     is_valid_utf8_continuation_byte c2;
     u == extended_utf8_char_3byte_decoding (c0, c1, c2)}
    EXTENDED_UTF8_CHAR_3byte (3, u, c0, c1, c2, ~1, ~1, ~1)
    //
  | {u, c0, c1, c2, c3 : int |
     0 <= u; u <= 0x1FFFFF;
     is_extended_utf8_4byte_first_byte c0;
     is_valid_utf8_continuation_byte c1;
     is_valid_utf8_continuation_byte c2;
     is_valid_utf8_continuation_byte c3;
     u == extended_utf8_char_4byte_decoding (c0, c1, c2, c3)}
    EXTENDED_UTF8_CHAR_4byte (4, u, c0, c1, c2, c3, ~1, ~1)
    //
  | {u, c0, c1, c2, c3, c4 : int |
     0 <= u; u <= 0x3FFFFFF;
     is_extended_utf8_5byte_first_byte c0;
     is_valid_utf8_continuation_byte c1;
     is_valid_utf8_continuation_byte c2;
     is_valid_utf8_continuation_byte c3;
     is_valid_utf8_continuation_byte c4;
     u == extended_utf8_char_5byte_decoding (c0, c1, c2, c3, c4)}
    EXTENDED_UTF8_CHAR_5byte (5, u, c0, c1, c2, c3, c4, ~1)
    //
  | {u, c0, c1, c2, c3, c4, c5 : int |
     0 <= u; u <= 0x7FFFFFFF;
     is_extended_utf8_6byte_first_byte c0;
     is_valid_utf8_continuation_byte c1;
     is_valid_utf8_continuation_byte c2;
     is_valid_utf8_continuation_byte c3;
     is_valid_utf8_continuation_byte c4;
     is_valid_utf8_continuation_byte c5;
     u == extended_utf8_char_6byte_decoding (c0, c1, c2, c3, c4, c5)}
    EXTENDED_UTF8_CHAR_6byte (6, u, c0, c1, c2, c3, c4, c5)

prfn
decode_extended_utf8_istot :
  {n : int | 1 <= n; n <= 6}
  {c0, c1, c2, c3, c4, c5 : int |
   extended_utf8_char_length_relation (c0, n);
   n <= 1 || is_valid_utf8_continuation_byte c1;
   n <= 2 || is_valid_utf8_continuation_byte c2;
   n <= 3 || is_valid_utf8_continuation_byte c3;
   n <= 4 || is_valid_utf8_continuation_byte c4;
   n <= 5 || is_valid_utf8_continuation_byte c5;
   1 < n || c1 == ~1;
   2 < n || c2 == ~1;
   3 < n || c3 == ~1;
   4 < n || c4 == ~1;
   5 < n || c5 == ~1}
   () -<prf> [u : int] EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5)

prfn
decode_extended_utf8_isfun :
  {na : int | 1 <= na; na <= 6}
  {ua : int}
  {c0a, c1a, c2a, c3a, c4a, c5a : int}
  {nb : int | nb == na}
  {ub : int}
  {c0b, c1b, c2b, c3b, c4b, c5b : int |
   c0b == c0a;
   c1b == c1a;
   c2b == c2a;
   c3b == c3a;
   c4b == c4a;
   c5b == c5a}
  (EXTENDED_UTF8_CHAR (na, ua, c0a, c1a, c2a, c3a, c4a, c5a),
   EXTENDED_UTF8_CHAR (nb, ub, c0b, c1b, c2b, c3b, c4b, c5b)) -<prf>
    [ua == ub] void

prfn
lemma_extended_utf8_char_length :
  {n : int} {u : int} {c0, c1, c2, c3, c4, c5 : int}
  EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5) -<prf>
    [n == extended_utf8_character_length c0] void

fn {}
decode_extended_utf8_1byte :
  {c0 : int | 0x00 <= c0; c0 <= 0x7F} int c0 -<>
    [u : int] (EXTENDED_UTF8_CHAR (1, u, c0, ~1, ~1, ~1, ~1, ~1) | int u)

fn {}
decode_extended_utf8_2byte :
  {c0, c1 : int | 0xC0 <= c0; c0 <= 0xDF;
                  is_valid_utf8_continuation_byte c1}
  (int c0, int c1) -<>
    [u : int] (EXTENDED_UTF8_CHAR (2, u, c0, c1, ~1, ~1, ~1, ~1) | int u)

fn {}
decode_extended_utf8_3byte :
  {c0, c1, c2 : int | 0xE0 <= c0; c0 <= 0xEF;
                      is_valid_utf8_continuation_byte c1;
                      is_valid_utf8_continuation_byte c2}
  (int c0, int c1, int c2) -<>
    [u : int] (EXTENDED_UTF8_CHAR (3, u, c0, c1, c2, ~1, ~1, ~1) | int u)

fn {}
decode_extended_utf8_4byte :
  {c0, c1, c2, c3 : int | 0xF0 <= c0; c0 <= 0xF7;
                          is_valid_utf8_continuation_byte c1;
                          is_valid_utf8_continuation_byte c2;
                          is_valid_utf8_continuation_byte c3}
  (int c0, int c1, int c2, int c3) -<>
    [u : int] (EXTENDED_UTF8_CHAR (4, u, c0, c1, c2, c3, ~1, ~1) | int u)

fn {}
decode_extended_utf8_5byte :
  {c0, c1, c2, c3, c4 : int | 0xF8 <= c0; c0 <= 0xFB;
                              is_valid_utf8_continuation_byte c1;
                              is_valid_utf8_continuation_byte c2;
                              is_valid_utf8_continuation_byte c3;
                              is_valid_utf8_continuation_byte c4}
  (int c0, int c1, int c2, int c3, int c4) -<>
    [u : int] (EXTENDED_UTF8_CHAR (5, u, c0, c1, c2, c3, c4, ~1) | int u)

fn {}
decode_extended_utf8_6byte :
  {c0, c1, c2, c3, c4, c5 : int | 0xFC <= c0; c0 <= 0xFD;
                                  is_valid_utf8_continuation_byte c1;
                                  is_valid_utf8_continuation_byte c2;
                                  is_valid_utf8_continuation_byte c3;
                                  is_valid_utf8_continuation_byte c4;
                                  is_valid_utf8_continuation_byte c5}
  (int c0, int c1, int c2, int c3, int c4, int c5) -<>
    [u : int]
    (EXTENDED_UTF8_CHAR (6, u, c0, c1, c2, c3, c4, c5) | int u)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

stadef extended_utf8_shortest_length (u : int) =
  ifint (u < 0, ~1,
    ifint (u <= 0x7F, 1,
      ifint (u <= 0x7FF, 2,
        ifint (u <= 0xFFFF, 3,
          ifint (u <= 0x1FFFFF, 4,
            ifint (u <= 0x3FFFFFF, 5,
              ifint (u <= 0x7FFFFFFF, 6, ~1)))))))

dataprop EXTENDED_UTF8_SHORTEST (length : int, u : int, c0 : int, c1 : int,
                                 c2 : int, c3 : int, c4 : int, c5 : int) =
  | {u, c0 : int |
     0 <= u; u <= 0x7F;
     c0 == u}
    EXTENDED_UTF8_SHORTEST_1byte (1, u, c0, ~1, ~1, ~1, ~1, ~1) of
      EXTENDED_UTF8_CHAR (1, u, c0, ~1, ~1, ~1, ~1, ~1)
    //
  | {u, c0, c1 : int |
     0x7F < u; u <= 0x7FF;
     c0 == 0xC0 + (u \ndiv 64);
     c1 == 0x80 + (u \nmod 64)}
    EXTENDED_UTF8_SHORTEST_2byte (2, u, c0, c1, ~1, ~1, ~1, ~1) of
      EXTENDED_UTF8_CHAR (2, u, c0, c1, ~1, ~1, ~1, ~1)
    //
  | {u, c0, c1, c2 : int |
     0x7FF < u; u <= 0xFFFF;
     c0 == 0xE0 + (u \ndiv (64 * 64));
     c1 == 0x80 + ((u \ndiv 64) \nmod 64);
     c2 == 0x80 + (u \nmod 64)}
    EXTENDED_UTF8_SHORTEST_3byte (3, u, c0, c1, c2, ~1, ~1, ~1) of
      EXTENDED_UTF8_CHAR (3, u, c0, c1, c2, ~1, ~1, ~1)
    //
  | {u, c0, c1, c2, c3 : int |
     0xFFFF < u; u <= 0x1FFFFF;
     c0 == 0xF0 + (u \ndiv (64 * 64 * 64));
     c1 == 0x80 + ((u \ndiv (64 * 64)) \nmod 64);
     c2 == 0x80 + ((u \ndiv 64) \nmod 64);
     c3 == 0x80 + (u \nmod 64)}
    EXTENDED_UTF8_SHORTEST_4byte (4, u, c0, c1, c2, c3, ~1, ~1) of
      EXTENDED_UTF8_CHAR (4, u, c0, c1, c2, c3, ~1, ~1)
    //
  | {u, c0, c1, c2, c3, c4 : int |
     0x1FFFFF < u; u <= 0x3FFFFFF;
     c0 == 0xF8 + (u \ndiv (64 * 64 * 64 * 64));
     c1 == 0x80 + ((u \ndiv (64 * 64 * 64)) \nmod 64);
     c2 == 0x80 + ((u \ndiv (64 * 64)) \nmod 64);
     c3 == 0x80 + ((u \ndiv 64) \nmod 64);
     c4 == 0x80 + (u \nmod 64)}
    EXTENDED_UTF8_SHORTEST_5byte (5, u, c0, c1, c2, c3, c4, ~1) of
      EXTENDED_UTF8_CHAR (5, u, c0, c1, c2, c3, c4, ~1)
    //
  | {u, c0, c1, c2, c3, c4, c5 : int |
     0x3FFFFFF < u; u <= 0x7FFFFFFF;
     c0 == 0xFC + (u \ndiv (64 * 64 * 64 * 64 * 64));
     c1 == 0x80 + ((u \ndiv (64 * 64 * 64 * 64)) \nmod 64);
     c2 == 0x80 + ((u \ndiv (64 * 64 * 64)) \nmod 64);
     c3 == 0x80 + ((u \ndiv (64 * 64)) \nmod 64);
     c4 == 0x80 + ((u \ndiv 64) \nmod 64);
     c5 == 0x80 + (u \nmod 64)}
    EXTENDED_UTF8_SHORTEST_6byte (6, u, c0, c1, c2, c3, c4, c5) of
      EXTENDED_UTF8_CHAR (6, u, c0, c1, c2, c3, c4, c5)

prfn
extended_utf8_shortest_is_char :
  {n : int} {u : int} {c0, c1, c2, c3, c4, c5 : int}
  EXTENDED_UTF8_SHORTEST (n, u, c0, c1, c2, c3, c4, c5) -<prf>
    EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5)

prfn
lemma_extended_utf8_shortest_length :
  {n : int} {u : int} {c0, c1, c2, c3, c4, c5 : int}
  EXTENDED_UTF8_SHORTEST (n, u, c0, c1, c2, c3, c4, c5) -<prf>
    [n == extended_utf8_shortest_length u] void

fn {}
encode_extended_utf8_character :
  {u : nat | u <= 0x7FFFFFFF}
  int u -<>
    [n : int] [c0, c1, c2, c3, c4, c5 : int]
    @(EXTENDED_UTF8_SHORTEST (n, u, c0, c1, c2, c3, c4, c5) |
      int n, int c0, int c1, int c2, int c3, int c4, int c5)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// A valid UTF-8 character is one that encodes a valid Unicode
// code point and is not overlong.
//

dataprop UTF8_CHAR_INVALID_CASES (c0 : int, c1 : int, c2 : int, c3 : int) =
  // The cases are not mutually exclusive.
  | {c0, c1, c2, c3 : int | extended_utf8_character_length c0 == ~1}
    // We might not be using this case, presently (has that changed?),
    // but it is included for completeness.
    UTF8_CHAR_INVALID_bad_c0 (c0, c1, c2, c3)
  | {c0, c1, c2, c3 : int | 2 <= extended_utf8_character_length c0;
                            ~(is_valid_utf8_continuation_byte c1)}
    UTF8_CHAR_INVALID_bad_c1 (c0, c1, c2, c3)
  | {c0, c1, c2, c3 : int | 3 <= extended_utf8_character_length c0;
                            ~(is_valid_utf8_continuation_byte c2)}
    UTF8_CHAR_INVALID_bad_c2 (c0, c1, c2, c3)
  | {c0, c1, c2, c3 : int | extended_utf8_character_length c0 == 4;
                            ~(is_valid_utf8_continuation_byte c3)}
    UTF8_CHAR_INVALID_bad_c3 (c0, c1, c2, c3)
  | {c0, c1, c2, c3 : int |
     extended_utf8_character_length c0 == 2;
     extended_utf8_char_2byte_decoding (c0, c1) <= 0x7F}
    UTF8_CHAR_INVALID_invalid_2byte (c0, c1, c2, c3)
  | {c0, c1, c2, c3 : int |
     extended_utf8_character_length c0 == 3
       && (extended_utf8_char_3byte_decoding (c0, c1, c2) <= 0x7FF
             || ~(is_valid_unicode_code_point
                    (extended_utf8_char_3byte_decoding (c0, c1, c2))))}
    UTF8_CHAR_INVALID_invalid_3byte (c0, c1, c2, c3)
  | {c0, c1, c2, c3 : int |
     extended_utf8_character_length c0 == 4
       && (extended_utf8_char_4byte_decoding (c0, c1, c2, c3) <= 0xFFFF
             || ~(is_valid_unicode_code_point
                    (extended_utf8_char_4byte_decoding (c0, c1, c2, c3))))}
    UTF8_CHAR_INVALID_invalid_4byte (c0, c1, c2, c3)

dataprop UTF8_CHAR_VALID_BYTES (c0 : int, c1 : int, c2 : int, c3 : int) =
  | {c0 : int | 0 <= c0 && c0 <= 0x7F}
    UTF8_CHAR_VALID_BYTES_1byte (c0, ~1, ~1, ~1)
  | {c0, c1 : int | 0xC2 <= c0 && c0 <= 0xDF;
                    is_valid_utf8_continuation_byte c1}
    UTF8_CHAR_VALID_BYTES_2byte (c0, c1, ~1, ~1)
  | {c0, c1, c2 : int | (0xE1 <= c0 && c0 <= 0xEC)
                          || c0 == 0xEE
                          || c0 == 0xEF
                          || (c0 == 0xE0 && 0xA0 <= c1)
                          || (c0 == 0xED && c1 < 0xA0);
                        is_valid_utf8_continuation_byte c1;
                        is_valid_utf8_continuation_byte c2}
    UTF8_CHAR_VALID_BYTES_3byte (c0, c1, c2, ~1)
  | {c0, c1, c2, c3 : int | (0xF1 <= c0 && c0 <= 0xF3)
                              || (c0 == 0xF0 && 0x90 <= c1)
                              || (c0 == 0xF4 && c1 < 0x90);
                            is_valid_utf8_continuation_byte c1;
                            is_valid_utf8_continuation_byte c2;
                            is_valid_utf8_continuation_byte c3}
    UTF8_CHAR_VALID_BYTES_4byte (c0, c1, c2, c3)

dataprop UTF8_CHAR_INVALID_BYTES (c0 : int, c1 : int, c2 : int, c3 : int) =
  | // This should never occur.
    {c0 : int | is_extended_utf8_1byte_first_byte c0}
    UTF8_CHAR_INVALID_BYTES_1byte (c0, ~1, ~1, ~1)
  | {c0, c1 : int | is_extended_utf8_2byte_first_byte c0;
                    c0 == 0xC0 || c0 == 0xC1 || 
                    is_invalid_utf8_continuation_byte c1}
    UTF8_CHAR_INVALID_BYTES_2byte (c0, c1, ~1, ~1)
  | {c0, c1, c2 : int | is_extended_utf8_3byte_first_byte c0;
                        (c0 == 0xE0 && c1 < 0xA0) ||
                        (c0 == 0xED && 0xA0 <= c1) ||
                        is_invalid_utf8_continuation_byte c1 ||
                        is_invalid_utf8_continuation_byte c2}
    UTF8_CHAR_INVALID_BYTES_3byte (c0, c1, c2, ~1)
  | {c0, c1, c2, c3 : int | is_extended_utf8_4byte_first_byte c0;
                            0xF4 < c0 ||
                            (c0 == 0xF0 && c1 < 0x90) ||
                            (c0 == 0xF4 && 0x90 <= c1) ||
                            is_invalid_utf8_continuation_byte c1 ||
                            is_invalid_utf8_continuation_byte c2 ||
                            is_invalid_utf8_continuation_byte c3}
    UTF8_CHAR_INVALID_BYTES_4byte (c0, c1, c2, c3)
  | {c0, c1, c2, c3 : int | ~(is_extended_utf8_1byte_first_byte c0);
                            ~(is_extended_utf8_2byte_first_byte c0);
                            ~(is_extended_utf8_3byte_first_byte c0);
                            ~(is_extended_utf8_4byte_first_byte c0)}
    UTF8_CHAR_INVALID_BYTES_bad_c0 (c0, c1, c2, c3)

dataprop UTF8_CHAR_VALIDITY (n : int, u : int, c0 : int, c1 : int,
                             c2 : int, c3 : int, b : bool) =
  | {n : int} {u : int | is_valid_unicode_code_point u}
    {c0, c1, c2, c3 : int}
    UTF8_CHAR_valid (n, u, c0, c1, c2, c3, true) of
      (EXTENDED_UTF8_SHORTEST (n, u, c0, c1, c2, c3, ~1, ~1),
       UTF8_CHAR_VALID_BYTES (c0, c1, c2, c3))
  | {n : int} {u : int} {c0, c1, c2, c3 : int}
    UTF8_CHAR_invalid (n, u, c0, c1, c2, c3, false) of
      (UTF8_CHAR_INVALID_CASES (c0, c1, c2, c3),
       UTF8_CHAR_INVALID_BYTES (c0, c1, c2, c3))

propdef UTF8_CHAR_VALID (n : int, u : int, c0 : int, c1 : int,
                         c2 : int, c3 : int) =
  UTF8_CHAR_VALIDITY (n, u, c0, c1, c2, c3, true)

propdef UTF8_CHAR_INVALID (c0 : int, c1 : int, c2 : int, c3 : int) =
  [n : int] [u : int] UTF8_CHAR_VALIDITY (n, u, c0, c1, c2, c3, false)

prfn
utf8_char_valid_implies_shortest :
  {n : int} {u : int} {c0, c1, c2, c3 : int}
  UTF8_CHAR_VALID (n, u, c0, c1, c2, c3) -<prf>
    EXTENDED_UTF8_SHORTEST (n, u, c0, c1, c2, c3, ~1, ~1)

prfn
lemma_valid_utf8_character_1byte :
  {u, c0 : int |
   is_extended_utf8_1byte_first_byte c0;
   u == extended_utf8_char_1byte_decoding c0;
   0 <= u; u <= 0x7F}
  () -<prf> UTF8_CHAR_VALID (1, u, c0, ~1, ~1, ~1)

prfn
lemma_valid_utf8_character_2byte :
  {u, c0, c1 : int |
   is_extended_utf8_2byte_first_byte c0;
   is_valid_utf8_continuation_byte c1;
   u == extended_utf8_char_2byte_decoding (c0, c1);
   0x7F < u; u <= 0x7FF}
  () -<prf> UTF8_CHAR_VALID (2, u, c0, c1, ~1, ~1)

prfn
lemma_valid_utf8_character_3byte :
  {u, c0, c1, c2 : int |
   is_extended_utf8_3byte_first_byte c0;
   is_valid_utf8_continuation_byte c1;
   is_valid_utf8_continuation_byte c2;
   u == extended_utf8_char_3byte_decoding (c0, c1, c2);
   0x7FF < u; u <= 0xFFFF;
   //
   // Exclude the UTF-16 surrogate halves.
   //
   ~(0xD800 <= u && u < 0xE000)}
  () -<prf> UTF8_CHAR_VALID (3, u, c0, c1, c2, ~1)

prfn
lemma_valid_utf8_character_4byte :
  {u, c0, c1, c2, c3 : int |
   is_extended_utf8_4byte_first_byte c0;
   is_valid_utf8_continuation_byte c1;
   is_valid_utf8_continuation_byte c2;
   is_valid_utf8_continuation_byte c3;
   u == extended_utf8_char_4byte_decoding (c0, c1, c2, c3);
   0xFFFF < u; u <= 0x10FFFF}
  () -<prf> UTF8_CHAR_VALID (4, u, c0, c1, c2, c3)

prfn
utf8_character_1byte_valid_bytes :
  // This does not really do anything, but is included
  // for completeness.
  {u, c0 : int | is_extended_utf8_1byte_first_byte c0}
  UTF8_CHAR_VALID (1, u, c0, ~1, ~1, ~1) -<prf> void

prfn
utf8_character_2byte_valid_bytes :
  {u, c0, c1 : int | is_extended_utf8_2byte_first_byte c0}
  UTF8_CHAR_VALID (2, u, c0, c1, ~1, ~1) -<prf>
    [0xC2 <= c0; c0 <= 0xDF;
     is_valid_utf8_continuation_byte c1]
    void

prfn
utf8_character_3byte_valid_bytes :
  {u, c0, c1, c2 : int | is_extended_utf8_3byte_first_byte c0}
  UTF8_CHAR_VALID (3, u, c0, c1, c2, ~1) -<prf>
    [(0xE1 <= c0 && c0 <= 0xEC)
        || c0 == 0xEE
        || c0 == 0xEF
        || (c0 == 0xE0 && 0xA0 <= c1)
        || (c0 == 0xED && c1 < 0xA0);
     is_valid_utf8_continuation_byte c1;
     is_valid_utf8_continuation_byte c2]
    void

prfn
utf8_character_4byte_valid_bytes :
  {u, c0, c1, c2, c3 : int | is_extended_utf8_4byte_first_byte c0}
  UTF8_CHAR_VALID (4, u, c0, c1, c2, c3) -<prf>
    [(0xF1 <= c0 && c0 <= 0xF3)
        || (c0 == 0xF0 && 0x90 <= c1)
        || (c0 == 0xF4 && c1 < 0x90);
     is_valid_utf8_continuation_byte c1;
     is_valid_utf8_continuation_byte c2;
     is_valid_utf8_continuation_byte c3]
    void

prfn
utf8_character_1byte_invalid_bytes :
  // This does not really do anything, but is included
  // for completeness.
  {c0 : int | is_extended_utf8_1byte_first_byte c0}
  UTF8_CHAR_INVALID (c0, ~1, ~1, ~1) -<prf> void

prfn
utf8_character_2byte_invalid_bytes :
  {c0, c1 : int | is_extended_utf8_2byte_first_byte c0}
  UTF8_CHAR_INVALID (c0, c1, ~1, ~1) -<prf>
    [c0 == 0xC0 || c0 == 0xc1 || is_invalid_utf8_continuation_byte c1]
    void

prfn
utf8_character_3byte_invalid_bytes :
  {c0, c1, c2 : int | is_extended_utf8_3byte_first_byte c0}
  UTF8_CHAR_INVALID (c0, c1, c2, ~1) -<prf>
    [(c0 == 0xE0 && c1 < 0xA0) ||
     (c0 == 0xED && 0xA0 <= c1) ||
     is_invalid_utf8_continuation_byte c1 ||
     is_invalid_utf8_continuation_byte c2]
    void

prfn
utf8_character_4byte_invalid_bytes :
  {c0, c1, c2, c3 : int | is_extended_utf8_4byte_first_byte c0}
  UTF8_CHAR_INVALID (c0, c1, c2, c3) -<prf>
    [0xF4 < c0 ||
     (c0 == 0xF0 && c1 < 0x90) ||
     (c0 == 0xF4 && 0x90 <= c1) ||
     is_invalid_utf8_continuation_byte c1 ||
     is_invalid_utf8_continuation_byte c2 ||
     is_invalid_utf8_continuation_byte c3]
    void

fn {}
is_valid_utf8_character_1byte :
  {c0 : int | is_extended_utf8_1byte_first_byte c0}
  int c0 -<>
    [b : bool | b == true] [u : int]
    (UTF8_CHAR_VALIDITY (1, u, c0, ~1, ~1, ~1, b) | bool b)

fn {}
is_valid_utf8_character_2byte :
  {c0, c1 : int | is_extended_utf8_2byte_first_byte c0}
  (int c0, int c1) -<>
    [b : bool] [u : int]
    (UTF8_CHAR_VALIDITY (2, u, c0, c1, ~1, ~1, b) | bool b)

fn {}
is_valid_utf8_character_3byte :
  {c0, c1, c2 : int | is_extended_utf8_3byte_first_byte c0}
  (int c0, int c1, int c2) -<>
    [b : bool] [u : int]
    (UTF8_CHAR_VALIDITY (3, u, c0, c1, c2, ~1, b) | bool b)

fn {}
is_valid_utf8_character_4byte :
  {c0, c1, c2, c3 : int | is_extended_utf8_4byte_first_byte c0}
  (int c0, int c1, int c2, int c3) -<>
    [b : bool] [u : int]
    (UTF8_CHAR_VALIDITY (4, u, c0, c1, c2, c3, b) | bool b)

fn {}
decode_utf8_1byte :
  {c0 : int | is_extended_utf8_1byte_first_byte c0}
  int c0 -<> [u : int | 0 <= u; u <= 0x7F] int u

fn {}
decode_utf8_2byte :
  {c0, c1 : int | 0xC2 <= c0; c0 <= 0xDF;
                  is_valid_utf8_continuation_byte c1}
  (int c0, int c1) -<> [u : int | 0x7F < u; u <= 0x7FF] int u

fn {}
decode_utf8_3byte :
  {c0, c1, c2 : int | (0xE1 <= c0 && c0 <= 0xEC)
                        || c0 == 0xEE
                        || c0 == 0xEF
                        || (c0 == 0xE0 && 0xA0 <= c1)
                        || (c0 == 0xED && c1 < 0xA0);
                      is_valid_utf8_continuation_byte c1;
                      is_valid_utf8_continuation_byte c2}
  (int c0, int c1, int c2) -<>
    [u : int | 0x7FF < u; u <= 0xFFFF; u < 0xD800 || 0xE000 <= u] int u

fn {}
decode_utf8_4byte :
  {c0, c1, c2, c3 : int | (0xF1 <= c0 && c0 <= 0xF3)
                            || (c0 == 0xF0 && 0x90 <= c1)
                            || (c0 == 0xF4 && c1 < 0x90);
                          is_valid_utf8_continuation_byte c1;
                          is_valid_utf8_continuation_byte c2;
                          is_valid_utf8_continuation_byte c3}
  (int c0, int c1, int c2, int c3) -<>
    [u : int | 0xFFFF < u; u <= 0x10FFFF] int u

fn {}
encode_utf8_character :
  {u : int | is_valid_unicode_code_point u}
  int u -<>
    [n : int]
    [c0, c1, c2, c3 : int | extended_utf8_char_length_relation (c0, n)]
    @(UTF8_CHAR_VALID (n, u, c0, c1, c2, c3) |
      int n, int c0, int c1, int c2, int c3)

//--------------------------------------------------------------------
