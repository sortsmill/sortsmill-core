// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_PACKNAME "SMCORE.unistr"
#define ATS_EXTERN_PREFIX "_smcore_unistr__"

%{#
#include "sortsmill/core/CATS/unistr.cats"
%}

staload "sortsmill/core/SATS/unitypes.sats"

dataview malloc_libunistring_v (addr) =
  | {s : agz} malloc_libunistring_v_succ (s) of mfree_libc_v (s)
  | malloc_libunistring_v_fail (null) of ()

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

typedef uXX_check_unsafe_function =
  {s : agz} (ptr s, size_t) -<>
    [errorp : addr | errorp == null || s <= errorp] ptr errorp

fn u8_check_unsafe : uXX_check_unsafe_function = "mac#%"
fn u16_check_unsafe : uXX_check_unsafe_function = "mac#%"
fn u32_check_unsafe : uXX_check_unsafe_function = "mac#%"

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

typedef uXX_to_uYY_unsafe_function =
  {s, resultbuf, lengthp : agz}
  (ptr s, size_t, ptr resultbuf, ptr lengthp) -<0,!wrt> ptr resultbuf

typedef uXX_to_uYY_alloc_unsafe_function =
  {s, lengthp : agz} (ptr s, size_t, ptr lengthp) -<0,!wrt>
    [stringp : addr] (malloc_libunistring_v stringp | ptr stringp)

fn u8_to_u16_unsafe : uXX_to_uYY_unsafe_function = "mac#%"
fn u8_to_u32_unsafe : uXX_to_uYY_unsafe_function = "mac#%"
fn u16_to_u8_unsafe : uXX_to_uYY_unsafe_function = "mac#%"
fn u16_to_u32_unsafe : uXX_to_uYY_unsafe_function = "mac#%"
fn u32_to_u8_unsafe : uXX_to_uYY_unsafe_function = "mac#%"
fn u32_to_u16_unsafe : uXX_to_uYY_unsafe_function = "mac#%"

fn u8_to_u16_alloc_unsafe : uXX_to_uYY_alloc_unsafe_function = "mac#%"
fn u8_to_u32_alloc_unsafe : uXX_to_uYY_alloc_unsafe_function = "mac#%"
fn u16_to_u8_alloc_unsafe : uXX_to_uYY_alloc_unsafe_function = "mac#%"
fn u16_to_u32_alloc_unsafe : uXX_to_uYY_alloc_unsafe_function = "mac#%"
fn u32_to_u8_alloc_unsafe : uXX_to_uYY_alloc_unsafe_function = "mac#%"
fn u32_to_u16_alloc_unsafe : uXX_to_uYY_alloc_unsafe_function = "mac#%"

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

typedef uXX_mblen_unsafe_function = {s : agz} (ptr s, size_t) -<> int

typedef uXX_mbtouc_unsafe_function =
  {puc, s : agz} {n : pos} (ptr puc, ptr s, size_t n) -<0,!wrt> int

typedef uXX_uctomb_unsafe_function =
  {s, puc : agz} (ptr s, ptr puc, int) -<0,!wrt> int

fn u8_mblen_unsafe : uXX_mblen_unsafe_function = "mac#%"
fn u16_mblen_unsafe : uXX_mblen_unsafe_function = "mac#%"
fn u32_mblen_unsafe : uXX_mblen_unsafe_function = "mac#%"

// uXX_mbtouc_unsafe_unsafe does not check whether the character
// is valid in the encoding.
//
// uXX_mbtouc_unsafe and uXX_mbtoucr_unsafe do check.
//
fn u8_mbtouc_unsafe_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u8_mbtouc_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u8_mbtoucr_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u16_mbtouc_unsafe_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u16_mbtouc_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u16_mbtoucr_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u32_mbtouc_unsafe_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u32_mbtouc_unsafe : uXX_mbtouc_unsafe_function = "mac#%"
fn u32_mbtoucr_unsafe : uXX_mbtouc_unsafe_function = "mac#%"

fn u8_uctomb_unsafe : uXX_uctomb_unsafe_function = "mac#%"
fn u16_uctomb_unsafe : uXX_uctomb_unsafe_function = "mac#%"
fn u32_uctomb_unsafe : uXX_uctomb_unsafe_function = "mac#%"

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - 

typedef uXX_cpy_unsafe_function =
  {dest, src : agz} (ptr dest, ptr src, size_t) -<0,!wrt> ptr dest
typedef uXX_move_unsafe_function = uXX_cpy_unsafe_function

typedef uXX_set_unsafe_function =
  {dest : agz} (ptr dest, ucs4_char, size_t) -<0,!wrt> ptr dest

fn u8_cpy_unsafe : uXX_cpy_unsafe_function = "mac#%"
fn u16_cpy_unsafe : uXX_cpy_unsafe_function = "mac#%"
fn u32_cpy_unsafe : uXX_cpy_unsafe_function = "mac#%"

fn u8_move_unsafe : uXX_move_unsafe_function = "mac#%"
fn u16_move_unsafe : uXX_move_unsafe_function = "mac#%"
fn u32_move_unsafe : uXX_move_unsafe_function = "mac#%"

fn u8_set_unsafe : uXX_set_unsafe_function = "mac#%"
fn u16_set_unsafe : uXX_set_unsafe_function = "mac#%"
fn u32_set_unsafe : uXX_set_unsafe_function = "mac#%"

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

typedef uXX_cmp_unsafe_function =
  {s1, s2 : agz} (ptr s1, ptr s2, size_t) -<> int

typedef uXX_cmp2_unsafe_function =
  {s1, s2 : agz} (ptr s1, size_t, ptr s2, size_t) -<> int

fn u8_cmp_unsafe : uXX_cmp_unsafe_function = "mac#%"
fn u16_cmp_unsafe : uXX_cmp_unsafe_function = "mac#%"
fn u32_cmp_unsafe : uXX_cmp_unsafe_function = "mac#%"

fn u8_cmp2_unsafe : uXX_cmp2_unsafe_function = "mac#%"
fn u16_cmp2_unsafe : uXX_cmp2_unsafe_function = "mac#%"
fn u32_cmp2_unsafe : uXX_cmp2_unsafe_function = "mac#%"

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

typedef uXX_chr_unsafe_function =
  {s : agz} (ptr s, size_t, ucs4_char) -<>
    [p : addr | p == null || s <= p] ptr p

fn u8_chr_unsafe : uXX_chr_unsafe_function = "mac#%"
fn u16_chr_unsafe : uXX_chr_unsafe_function = "mac#%"
fn u32_chr_unsafe : uXX_chr_unsafe_function = "mac#%"

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

typedef uXX_mbsnlen_unsafe_function =
  {s : agz} {m : int} (ptr s, size_t m) -<> [n : int | n <= m] size_t n

fn u8_mbsnlen_unsafe : uXX_mbsnlen_unsafe_function = "mac#%"
fn u16_mbsnlen_unsafe : uXX_mbsnlen_unsafe_function = "mac#%"
fn u32_mbsnlen_unsafe : uXX_mbsnlen_unsafe_function = "mac#%"

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

typedef uXX_cpy_alloc_unsafe_function =
  {s : agz} (ptr s, size_t) -<0,!wrt>
    [stringp : addr] (malloc_libunistring_v stringp | ptr stringp)

fn u8_cpy_alloc_unsafe : uXX_cpy_alloc_unsafe_function = "mac#%"
fn u16_cpy_alloc_unsafe : uXX_cpy_alloc_unsafe_function = "mac#%"
fn u32_cpy_alloc_unsafe : uXX_cpy_alloc_unsafe_function = "mac#%"

//--------------------------------------------------------------------
