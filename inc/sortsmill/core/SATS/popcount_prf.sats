// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_PACKNAME "SMCORE.popcount_prf"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//--------------------------------------------------------------------

//
// POPCOUNT (i, n): the relation `The population count of i equals n'.
//
dataprop POPCOUNT (i : int, n : int) =
  | POPCOUNT_base (0, 0) of ()
  | (* Shift right, when the least significant bit is 1. *)
    {i1 : nat} {p1 : nat}
    POPCOUNT_odd (2 * i1 + 1, p1 + 1) of POPCOUNT (i1, p1)
  | (* Shift right, when the least significant bit is 0. *)
    {i1 : pos} {p1 : nat}
    POPCOUNT_even (2 * i1, p1) of POPCOUNT (i1, p1)

// Some simple demonstrations of POPCOUNT (i, n).
prfn popcount_0 : () -<prf> POPCOUNT (0, 0)
prfn popcount_1 : () -<prf> POPCOUNT (1, 1)
prfn popcount_2 : () -<prf> POPCOUNT (2, 1)
prfn popcount_3 : () -<prf> POPCOUNT (3, 2)
prfn popcount_4 : () -<prf> POPCOUNT (4, 1)
prfn popcount_5 : () -<prf> POPCOUNT (5, 2)

// The popcount is total. That is: given an unsigned integer,
// one can compute its popcount.
prfn popcount_istot :
  {i : nat} () -<prf> [n : nat] POPCOUNT (i, n)

// The popcount is a function. That is: the popcount
// of a given unsigned integer is unique.
prfn popcount_isfun :
  {i : nat} {n1, n2 : int} (POPCOUNT (i, n1), POPCOUNT (i, n2)) -<prf>
    [n1 == n2] void

// Return the given popcount proof.
prfn popcount_identity :
  {i : nat} {n : int} POPCOUNT (i, n) -<prf> POPCOUNT (i, n)

//--------------------------------------------------------------------

