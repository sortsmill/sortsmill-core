// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_PACKNAME "SMCORE.unitypes"
#define ATS_EXTERN_PREFIX "_smcore_unitypes__"

%{#
#include "sortsmill/core/CATS/unitypes.cats"
%}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef ucs4_t (c : int) = $extype"ucs4_t"
typedef ucs4_char (c : int) = ucs4_t c
typedef u8_char (c : int) = $extype"uint8_t"
typedef u16_char (c : int) = $extype"uint16_t"
typedef u32_char (c : int) = $extype"uint32_t"

typedef ucs4_t = [c : int] ucs4_t c
typedef ucs4_char = [c : int] ucs4_char c
typedef u8_char = [c : int] u8_char c
typedef u16_char = [c : int] u16_char c
typedef u32_char = [c : int] u32_char c

castfn char_to_u8 : {c : nat | c <= 0xff} char c -<> u8_char c
castfn u8_to_char : {c : nat | c <= 0xff} u8_char c -<> char c
castfn uint_to_u8 : {c : nat | c <= 0xff} uint c -<> u8_char c
castfn u8_to_uint : {c : nat | c <= 0xff} u8_char c -<> uint c
castfn int_to_u8 : {c : nat | c <= 0xff} int c -<> u8_char c
castfn u8_to_int : {c : nat | c <= 0xff} u8_char c -<> int c
castfn uint8_to_u8 : {c : nat | c <= 0xff} uint8 c -<> u8_char c
castfn u8_to_uint8 : {c : nat | c <= 0xff} u8_char c -<> uint8 c

castfn uint_to_u16 : {c : nat | c <= 0xffff} uint c -<> u16_char c
castfn u16_to_uint : {c : nat | c <= 0xffff} u16_char c -<> uint c
castfn int_to_u16 : {c : nat | c <= 0xffff} int c -<> u16_char c
castfn u16_to_int : {c : nat | c <= 0xffff} u16_char c -<> int c
castfn uint16_to_u16 : {c : nat | c <= 0xffff} uint16 c -<> u16_char c
castfn u16_to_uint16 : {c : nat | c <= 0xffff} u16_char c -<> uint16 c

castfn ucs4_to_u32 : {c : nat | c <= 0xffffffff} ucs4_char c -<> u32_char c
castfn u32_to_ucs4 : {c : nat | c <= 0xffffffff} u32_char c -<> ucs4_char c
castfn uint_to_u32 : {c : nat | c <= 0xffffffff} uint c -<> u32_char c
castfn u32_to_uint : {c : nat | c <= 0xffffffff} u32_char c -<> uint c
castfn uint_to_ucs4 : {c : nat | c <= 0xffffffff} uint c -<> ucs4_char c
castfn ucs4_to_uint : {c : nat | c <= 0xffffffff} ucs4_char c -<> uint c
castfn uint32_to_u32 : {c : nat | c <= 0xffffffff} uint32 c -<> u32_char c
castfn u32_to_uint32 : {c : nat | c <= 0xffffffff} u32_char c -<> uint32 c
castfn uint32_to_ucs4 : {c : nat | c <= 0xffffffff} uint32 c -<> ucs4_char c
castfn ucs4_to_uint32 : {c : nat | c <= 0xffffffff} ucs4_char c -<> uint32 c

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
