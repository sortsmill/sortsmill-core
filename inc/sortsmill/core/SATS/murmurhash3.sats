// -*- ats -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.


#define ATS_PACKNAME "SMCORE.murmurhash3"
#define ATS_EXTERN_PREFIX "_smcore_murmurhash3__"

%{#
#include "sortsmill/core/CATS/murmurhash3.cats"
%}

//
// FIXME: Actually TEST these complicated declarations.
//

fn smcore_murmurhash3_x86_32 {key_size : nat}
                             {key_buf_size : int | key_size <= key_buf_size}
                             {out_addr : addr}
                             {out_buf_size : int | 4 <= out_buf_size}
                             (pf: !b0ytes out_buf_size @ out_addr >>
                                  bytes out_buf_size @ out_addr |
                              key : &(@[byte][key_buf_size]),
                              len : int key_size,
                              seed : uint32,
                              out : ptr out_addr) :<!wrt> void = "mac#%"

fn smcore_murmurhash3_x86_128 {key_size : nat}
                              {key_buf_size : int | key_size <= key_buf_size}
                              {out_addr : addr}
                              {out_buf_size : int | 8 <= out_buf_size}
                              (pf: !b0ytes out_buf_size @ out_addr >>
                                   bytes out_buf_size @ out_addr |
                               key : &(@[byte][key_buf_size]),
                               len : int key_size,
                               seed : uint32,
                               out : ptr out_addr) :<!wrt> void = "mac#%"

fn smcore_murmurhash3_x64_128 {key_size : nat}
                              {key_buf_size : int | key_size <= key_buf_size}
                              {out_addr : addr}
                              {out_buf_size : int | 8 <= out_buf_size}
                              (pf: !b0ytes out_buf_size @ out_addr >>
                                   bytes out_buf_size @ out_addr |
                               key : &(@[byte][key_buf_size]),
                               len : int key_size,
                               seed : uint32,
                               out : ptr out_addr) :<!wrt> void = "mac#%"

fn smcore_murmurhash3_32 {key_size : nat}
                         {key_buf_size : int | key_size <= key_buf_size}
                         {out_addr : addr}
                         {out_buf_size : int | 4 <= out_buf_size}
                         (pf: !b0ytes out_buf_size @ out_addr >>
                              bytes out_buf_size @ out_addr |
                          key : &(@[byte][key_buf_size]),
                          len : int key_size,
                          seed : uint32,
                          out : ptr out_addr) :<!wrt> void = "mac#%"

fn smcore_murmurhash3_128 {key_size : nat}
                          {key_buf_size : int | key_size <= key_buf_size}
                          {out_addr : addr}
                          {out_buf_size : int | 8 <= out_buf_size}
                          (pf: !b0ytes out_buf_size @ out_addr >>
                               bytes out_buf_size @ out_addr |
                           key : &(@[byte][key_buf_size]),
                           len : int key_size,
                           seed : uint32,
                           out : ptr out_addr) :<!wrt> void = "mac#%"

fn smcore_uint32_murmurhash3 {key_size : nat}
                             {key_buf_size : int | key_size <= key_buf_size}
                             (key : &(@[byte][key_buf_size]),
                              len : int key_size,
                              seed : uint32) :<> uint32 = "mac#%"
