// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Strings based on "sortsmill/core/SATS/immutable_vectors.sats".
//

#define ATS_PACKNAME "SMCORE.istrings"
#define ATS_EXTERN_PREFIX "_smcore_istrings__"

staload "sortsmill/core/SATS/immutable_vectors.sats"
staload "sortsmill/core/SATS/unicode_helpers.sats"

%{#
#include "sortsmill/core/CATS/istrings.cats"
%}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef istring (length : int, addr : addr) =
  immutable_vector (char, length, addr)
typedef istring (length : int) = [addr : addr] istring (length, addr)
typedef istring = [length : int] istring length

prfn lemma_istring_param :
  {n : int} {p : addr} istring (n, p) -<prf>
    [0 <= n; null <= p; (n == 0) == (p == null)] void

#define istring_nil immutable_vector_nil

fn {} istring_is_nil :
  {n : int} {p : addr} istring (n, p) ->
    [b : bool | b == (n == 0); b == (p == null); 0 <= n; null <= p] bool b

fn {} istring_length :
  {n : int} {p : addr} istring (n, p) ->
    [(n == 0) == (p == null); 0 <= n; null <= p] size_t n

fn istring_get : {n : int} {i : nat | i < n} (istring n, size_t i) -> char
fn istring_get_exn : (istring, size_t) -> char
fn istring_set :
  {n : int} {i : nat | i < n} (istring n, size_t i, char) -> istring n
fn istring_set_exn : {n : int} (istring n, size_t, char) -> istring n
fn istring_push : {n : int} (istring n, char) -> istring (n + 1)
fn istring_pop : {n : int | 1 <= n} istring n -> istring (n - 1)
fn istring_pop_exn : {n : int} istring n -> istring (n - 1)
fn istring_pops :
  {n : int} {num : nat | 0 <= n - num}
  (istring n, size_t num) -> istring (n - num)
fn istring_pops_exn :
  {n : int} {num : nat}
  (istring n, size_t num) -> istring (n - num)

fn istring_slice :
  {n : int}
  {i_start, i_end : nat | 0 <= i_end - i_start; i_end <= n}
  (istring n, size_t i_start, size_t i_end) -> istring (i_end - i_start)

fn istring_slice_exn :
  {n : int} {i_start, i_end : nat}
  (istring n, size_t i_start, size_t i_end) -> istring (i_end - i_start)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fn istring_slice_to_strnptr :
  {n : int}
  {i_start, i_end : nat | 0 <= i_end - i_start; i_end <= n}
  (istring n, size_t i_start, size_t i_end) -> strnptr (i_end - i_start)
fn istring_slice_to_string :
  {n : int}
  {i_start, i_end : nat | 0 <= i_end - i_start; i_end <= n}
  (istring n, size_t i_start, size_t i_end) -> string (i_end - i_start)

fn istring_slice_to_strnptr_exn :
  {n : int} {i_start, i_end : nat}
  (istring n, size_t i_start, size_t i_end) -> strnptr (i_end - i_start)
fn istring_slice_to_string_exn :
  {n : int} {i_start, i_end : nat}
  (istring n, size_t i_start, size_t i_end) -> string (i_end - i_start)

fn istring_to_strnptr : {n : int} istring n -> strnptr n
fn istring_to_string : {n : int} istring n -> string n

symintr istring2strnptr
overload istring2strnptr with istring_slice_to_strnptr
overload istring2strnptr with istring_to_strnptr

symintr istring2string
overload istring2string with istring_slice_to_string
overload istring2string with istring_to_string

symintr istring2strnptr_exn
overload istring2strnptr_exn with istring_slice_to_strnptr_exn
symintr istring2string_exn
overload istring2string_exn with istring_slice_to_string_exn

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fn append_string1_slice_to_istring :
  {m : int} {n : int}
  {i_start, i_end : nat | 0 <= i_end - i_start; i_end <= n}
  (istring m, string n, size_t i_start, size_t i_end) ->
    istring (m + (i_end - i_start))

fn string1_slice_to_istring :
  {n : int}
  {i_start, i_end : nat | 0 <= i_end - i_start; i_end <= n}
  (string n, size_t i_start, size_t i_end) -> istring (i_end - i_start)

fn append_string1_slice_to_istring_exn :
  {m : int} {n : int} {i_start, i_end : nat}
  (istring m, string n, size_t i_start, size_t i_end) ->
    istring (m + (i_end - i_start))

fn string0_slice_to_istring_exn :
   {i_start, i_end : nat} (string, size_t i_start, size_t i_end) -> istring

fn string1_slice_to_istring_exn :
  {n : int} {i_start, i_end : nat}
  (string n, size_t i_start, size_t i_end) -> istring (i_end - i_start)

fn append_string0_to_istring : (istring, string) -> istring
fn append_string1_to_istring :
  {m : int} {n : int} (istring m, string n) -> istring (m + n)
overload append_string_to_istring with append_string0_to_istring
overload append_string_to_istring with append_string1_to_istring

fn string0_to_istring : string -> istring
fn string1_to_istring : {n : int} string n -> istring n

fn append_istring_to_istring :
  {m : int} {n : int} (istring m, istring n) -> istring (m + n)

symintr string2istring
overload string2istring with string1_slice_to_istring
overload string2istring with string0_to_istring of 0
overload string2istring with string1_to_istring of 10

symintr string2istring_exn
overload string2istring_exn with string0_slice_to_istring_exn of 0
overload string2istring_exn with string1_slice_to_istring_exn of 10

symintr istring_append
overload istring_append with append_istring_to_istring of 0
overload istring_append with append_string0_to_istring of 0
overload istring_append with append_string1_to_istring of 10
overload + with append_istring_to_istring of 0
overload + with append_string0_to_istring of 0
overload + with append_string1_to_istring of 10

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Comparisons are defined as function templates so their
// behavior can be modified.
//
// By default, strings are compared blockwise, using the
// istring$strcmp template for comparison of each block.
// Comparison stops as soon as there a difference between
// corresponding blocks. The default implementation is to
// call strncmp(3).
//
// The blockwise algorithm may not be correct, for instance,
// if the strings are in a multibyte encoding such as UTF-8.
//

// The following three functions _do not_ treat nul characters
// as terminators.
fn strncmp_for_istrings :       (* case sensitive *)
  {n : int} (&(@[char][n]), &(@[char][n]), size_t n) -> int = "mac#%"
fn strncasecmp_for_istrings :   (* case insensitive *)
  {n : int} (&(@[char][n]), &(@[char][n]), size_t n) -> int = "mac#%"
fn c_strncasecmp_for_istrings : (* case insensitive in the C locale *)
  {n : int} (&(@[char][n]), &(@[char][n]), size_t n) -> int = "mac#%"

// The default implementation for istring$strncmp is strncmp_for_istrings.
fn {}
istring$strncmp :
  {n : int} (&(@[char][n]), &(@[char][n]), size_t n) -> int

fn {} cmp_istring_and_string : (istring, string) -> int
fn {} cmp_string_and_istring : (string, istring) -> int
fn {} cmp_istring_and_istring : (istring, istring) -> int

symintr istring_cmp
overload istring_cmp with cmp_istring_and_string
overload istring_cmp with cmp_string_and_istring
overload istring_cmp with cmp_istring_and_istring

fn {} lt_istring_string : (istring, string) -> bool
fn {} lt_string_istring : (string, istring) -> bool
fn {} lt_istring_istring : (istring, istring) -> bool
overload < with lt_istring_string
overload < with lt_string_istring
overload < with lt_istring_istring

fn {} lte_istring_string : (istring, string) -> bool
fn {} lte_string_istring : (string, istring) -> bool
fn {} lte_istring_istring : (istring, istring) -> bool
overload <= with lte_istring_string
overload <= with lte_string_istring
overload <= with lte_istring_istring

fn {} gt_istring_string : (istring, string) -> bool
fn {} gt_string_istring : (string, istring) -> bool
fn {} gt_istring_istring : (istring, istring) -> bool
overload > with gt_istring_string
overload > with gt_string_istring
overload > with gt_istring_istring

fn {} gte_istring_string : (istring, string) -> bool
fn {} gte_string_istring : (string, istring) -> bool
fn {} gte_istring_istring : (istring, istring) -> bool
overload >= with gte_istring_string
overload >= with gte_string_istring
overload >= with gte_istring_istring

fn {} eq_istring_string : (istring, string) -> bool
fn {} eq_string_istring : (string, istring) -> bool
fn {} eq_istring_istring : (istring, istring) -> bool
overload = with eq_istring_string
overload = with eq_string_istring
overload = with eq_istring_istring

fn {} neq_istring_string : (istring, string) -> bool
fn {} neq_string_istring : (string, istring) -> bool
fn {} neq_istring_istring : (istring, istring) -> bool
overload != with neq_istring_string
overload != with neq_string_istring
overload != with neq_istring_istring
overload <> with neq_istring_string
overload <> with neq_string_istring
overload <> with neq_istring_istring

fn {} compare_istring_string : (istring, string) -> Sgn
fn {} compare_string_istring : (string, istring) -> Sgn
fn {} compare_istring_istring : (istring, istring) -> Sgn
overload compare with compare_istring_string
overload compare with compare_string_istring
overload compare with compare_istring_istring

//--------------------------------------------------------------------
//
// 32-bit Murmur-3 hash functions.
//

fn istring_murmur3_32 : immutable_vector_murmur3_32_function char
fn istring_murmur3_32_exn : immutable_vector_murmur3_32_exn_function char

//--------------------------------------------------------------------
//
// Unicode support.
//
// Unicode strings are stored internally as `extended UTF-8':
// that is, UTF-8 without the restriction that the code point
// be valid Unicode. Any code point between 0 and INT32_MAX
// (2147483647 = 0x7FFFFFFF) can be represented, as a multibyte
// character of 1, 2, 3, 4, 5, or 6 bytes. (For valid Unicode
// code points, no more than 4 bytes are required to store the
// character.)
//

absprop ISTRING_IS_MULTIBYTE (p : addr)
absprop ISTRING_IS_UNICODE (p : addr)

praxi istring_nil_is_multibyte :
  {p : addr} istring (0, p) -<prf> ISTRING_IS_MULTIBYTE p
praxi istring_nil_is_unicode :
  {p : addr} istring (0, p) -<prf> ISTRING_IS_UNICODE p
praxi istring_unicode_is_multibyte :
  {p : addr} ISTRING_IS_UNICODE p -<prf> ISTRING_IS_MULTIBYTE p

exception IstringMultibyteExn of ()
exception IstringUnicodeExn of ()

fn {}
make_istring_multibyte_nil :
  () -<> (ISTRING_IS_MULTIBYTE null | istring (0, null))
fn {}
make_istring_unicode_nil :
  () -<> (ISTRING_IS_UNICODE null | istring (0, null))

// Use `#define' instead of `val': we want our subroutines
// to be usable in C programs that do not use ATS2 runtime
// initialization.
#define istring_multibyte_nil (make_istring_multibyte_nil ())
#define istring_unicode_nil (make_istring_unicode_nil ())

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

dataprop ISTRING_MULTIBYTE_VALIDITY (p : addr, b : bool) =
  | {p : addr} ISTRING_MULTIBYTE_valid (p, true) of ISTRING_IS_MULTIBYTE p
  | {p : addr} ISTRING_MULTIBYTE_invalid (p, false) of ()

dataprop ISTRING_UNICODE_VALIDITY (p : addr, b : bool) =
  | {p : addr} ISTRING_UNICODE_valid (p, true) of ISTRING_IS_UNICODE p
  | {p : addr} ISTRING_UNICODE_invalid (p, false) of ()

fn istring_is_multibyte :
  {n : int} {p : addr} istring (n, p) ->
    [b : bool] (ISTRING_MULTIBYTE_VALIDITY (p, b) | bool b)

fn istring_is_unicode :
  {n : int} {p : addr} istring (n, p) ->
    [b : bool] (ISTRING_UNICODE_VALIDITY (p, b) | bool b)

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

absprop ISTRING_MULTIBYTE_INDEX (i : int, p : addr)

praxi istring_multibyte_index_elim :
  {i : int} {p : addr}
  ISTRING_MULTIBYTE_INDEX (i, p) -<prf> ISTRING_IS_MULTIBYTE p

fn {}
istring_multibyte_first :
  {n : int} {p : addr}
  (ISTRING_IS_MULTIBYTE p | istring (n, p)) -<>
    [i : int | i == 0] (ISTRING_MULTIBYTE_INDEX (i, p) | size_t i)

fn {}
istring_multibyte_after_last :
  {n : int} {p : addr}
  (ISTRING_IS_MULTIBYTE p | istring (n, p)) ->
    [i : int | i == n] (ISTRING_MULTIBYTE_INDEX (i, p) | size_t i)

fn {}
istring_multibyte_last :
  {n : pos} {p : addr}
  (ISTRING_IS_MULTIBYTE p | istring (n, p)) ->
    [i : int | i < n;  n <= i + 6] (ISTRING_MULTIBYTE_INDEX (i, p) | size_t i)

fn istring_multibyte_next :
  {n : int} {i : nat | i < n} {p : addr}
  (ISTRING_MULTIBYTE_INDEX (i, p) | istring (n, p), size_t i) ->
    [j : int | 1 <= j - i; j - i <= 6; j <= n]
    (ISTRING_MULTIBYTE_INDEX (j, p) | size_t j)

fn istring_multibyte_prev :
  {n : int} {i : pos | i <= n} {p : addr}
  (ISTRING_MULTIBYTE_INDEX (i, p) |
   istring (n, p), size_t i) ->
    [j : int | 1 <= i - j; i - j <= 6; j <= i]
    (ISTRING_MULTIBYTE_INDEX (j, p) | size_t j)

// -  -  -  -
//
// Some permissive-at-compile-times versions
// for use mainly in C code. There is no need
// for `first' or `after_last' functions; just
// use `0' or `istring_length', respectively.
//

fn {}
istring_multibyte_last_unsafe_exn :
  {n : int} {p : addr}
  istring (n, p) -> [i : int | i < n;  n <= i + 6] size_t i

fn istring_multibyte_next_unsafe_exn :
  {n : int} {i : nat} {p : addr}
  (istring (n, p), size_t i) ->
    [j : int | 1 <= j - i; j - i <= 6; j <= n] size_t j

fn istring_multibyte_prev_unsafe_exn :
  {n : int} {i : nat} {p : addr}
  (istring (n, p), size_t i) ->
    [j : int | 1 <= i - j; i - j <= 6; j <= i] size_t j

//
// -  -  -  -

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

absprop ISTRING_UNICODE_INDEX (i : int, p : addr)

praxi istring_unicode_index_elim :
  {i : int} {p : addr}
  ISTRING_UNICODE_INDEX (i, p) -<prf> ISTRING_IS_UNICODE p

fn {}
istring_unicode_first :
  {n : nat} {p : addr}
  (ISTRING_IS_UNICODE p | istring (n, p)) -<>
    [i : int | i == 0] (ISTRING_UNICODE_INDEX (i, p) | size_t i)

fn {}
istring_unicode_after_last :
  {n : int} {p : addr}
  (ISTRING_IS_UNICODE p | istring (n, p)) ->
    [i : int | i == n] (ISTRING_UNICODE_INDEX (i, p) | size_t i)

fn {}
istring_unicode_last :
  {n : pos} {p : addr}
  (ISTRING_IS_UNICODE p | istring (n, p)) ->
    [i : int | i < n;  n <= i + 4] (ISTRING_UNICODE_INDEX (i, p) | size_t i)

fn istring_unicode_next :
  {n : int} {i : nat | i < n} {p : addr}
  (ISTRING_UNICODE_INDEX (i, p) | istring (n, p), size_t i) ->
    [j : int | 1 <= j - i; j - i <= 4; j <= n]
    (ISTRING_UNICODE_INDEX (j, p) | size_t j)

fn istring_unicode_prev :
  {n : int} {i : pos | i <= n} {p : addr}
  (ISTRING_UNICODE_INDEX (i, p) | istring (n, p), size_t i) ->
    [j : int | 1 <= i - j; i - j <= 4; j <= i]
    (ISTRING_UNICODE_INDEX (j, p) | size_t j)

// -  -  -  -
//
// Some permissive-at-compile-times versions
// for use mainly in C code. There is no need
// for `first' or `after_last' functions; just
// use `0' or `istring_length', respectively.
//

fn {}
istring_unicode_last_unsafe_exn :
  {n : int} {p : addr}
  istring (n, p) -> [i : int | i < n;  n <= i + 4] size_t i

fn istring_unicode_next_unsafe_exn :
  {n : int} {i : nat} {p : addr}
  (istring (n, p), size_t i) ->
    [j : int | 1 <= j - i; j - i <= 4; j <= n] size_t j

fn istring_unicode_prev_unsafe_exn :
  {n : int} {i : nat} {p : addr}
  (istring (n, p), size_t i) ->
    [j : int | 1 <= i - j; i - j <= 4; j <= i] size_t j

//
// -  -  -  -

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

fn istring_multibyte_get :
  {n : int} {i : nat | i < n} {p : addr}
  (ISTRING_MULTIBYTE_INDEX (i, p) | istring (n, p), size_t i) ->
    [u : int | 0 <= u; u <= 0x7FFFFFFF] int u

fn istring_unicode_get :
  {n : int} {i : nat | i < n} {p : addr}
  (ISTRING_UNICODE_INDEX (i, p) | istring (n, p), size_t i) ->
    [u : int | is_valid_unicode_code_point u] int u

fn istring_multibyte_get_unsafe_exn :
  {n : int} {p : addr}
  (istring (n, p), size_t) ->
    [u : int | 0 <= u; u <= 0x7FFFFFFF]
    [length : int | 1 <= length; length <= 6]
    @(int u, size_t length)

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

fn istring_multibyte_push :
  {n : int} {u : int | 0 <= u; u <= 0x7FFFFFFF} {p : addr}
  (ISTRING_IS_MULTIBYTE p | istring (n, p), int u) ->
    [q : addr]
    (ISTRING_IS_MULTIBYTE q | istring (n + extended_utf8_shortest_length u, q))

fn istring_multibyte_push_exn :
  {n : int} {u : int} {p : addr}
  (ISTRING_IS_MULTIBYTE p | istring (n, p), int u) ->
    [q : addr]
    (ISTRING_IS_MULTIBYTE q | istring (n + extended_utf8_shortest_length u, q))

fn istring_multibyte_push_unsafe :
  {n : int} {u : int | 0 <= u; u <= 0x7FFFFFFF}
  (istring n, int u) -> istring (n + extended_utf8_shortest_length u)

fn istring_multibyte_push_unsafe_exn :
  {n : int} {u : int}
  (istring n, int u) -> istring (n + extended_utf8_shortest_length u)

fn istring_unicode_push :
  {n : int} {u : int | is_valid_unicode_code_point u} {p : addr}
  (ISTRING_IS_UNICODE p | istring n, int u) ->
    [q : addr]
    (ISTRING_IS_UNICODE q | istring (n + extended_utf8_shortest_length u))

fn istring_unicode_push_exn :
  {n : int} {u : int} {p : addr}
  (ISTRING_IS_UNICODE p | istring n, int u) ->
    [q : addr]
    (ISTRING_IS_UNICODE q | istring (n + extended_utf8_shortest_length u))

fn istring_unicode_push_unsafe :
  {n : int} {u : int | is_valid_unicode_code_point u}
  (istring n, int u) -> istring (n + extended_utf8_shortest_length u)

fn istring_unicode_push_unsafe_exn :
  {n : int} {u : int}
  (istring n, int u) -> istring (n + extended_utf8_shortest_length u)

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

fn istring_multibyte_pop :
  {n : pos} {p : addr}
  (ISTRING_IS_MULTIBYTE p | istring (n, p)) ->
    [m : nat | m <= n - 1] [q : addr]
    (ISTRING_IS_MULTIBYTE q | istring (m, q))

fn istring_unicode_pop :
  {n : pos} {p : addr}
  (ISTRING_IS_UNICODE p | istring (n, p)) ->
    [m : nat | m <= n - 1] [q : addr]
    (ISTRING_IS_UNICODE q | istring (m, q))

//
// The following `pops' functions may pop fewer than
// `num' characters, if the result is an empty istring.
// (This behavior is to avoid having to know how many multibyte
// characters are stored in the structure.)
//

fn istring_multibyte_pops :
  {n : int} {num : nat} {p : addr}
  (ISTRING_IS_MULTIBYTE p | istring (n, p), size_t num) ->
    [m : int | m <= max (0, n - num)] [q : addr]
    (ISTRING_IS_MULTIBYTE q | istring (m, q))

fn istring_unicode_pops :
  {n : int} {num : nat} {p : addr}
  (ISTRING_IS_UNICODE p | istring (n, p), size_t num) ->
    [m : nat | m <= max (0, n - num)] [q : addr]
    (ISTRING_IS_UNICODE q | istring (m, q))

//
// The following unsafe `pop' and `pops' functions are not very
// intelligent. For each `character' popped, they simply pop all
// UTF-8 continuation bytes and whatever byte precedes them. If
// at any point the istring is left empty, then the empty istring
// is returned.
//

fn istring_multibyte_pop_unsafe :
  {n : int} istring n -> [m : nat | m <= max (0, n - 1)] istring m

fn istring_multibyte_pops_unsafe :
  {n : int} {num : nat}
  (istring n, size_t num) -> [m : nat | m <= max (0, n - num)] istring m

//--------------------------------------------------------------------
