// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Immutable vectors.
//

#define ATS_PACKNAME "SMCORE.immutable_vectors"
#define ATS_EXTERN_PREFIX "_smcore_immutable_vectors__"

%{#
#include "sortsmill/core/CATS/immutable_vectors.cats"
%}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

abstype nonnil_immutable_vector (entry_t : t@ype+, length : int, addr : addr) =
  ptr addr

datatype immutable_vector (entry_t : t@ype+, length : int, addr : addr) =
  | immutable_vector_nil (entry_t, 0, null) of () (* The NULL pointer. *)
  | {n : pos} {p : agz}
    immutable_vector_nonnil (entry_t, n, p) of
      nonnil_immutable_vector (entry_t, n, p)

typedef immutable_vector (entry_t : t@ype+, length : int) =
  [addr : addr] immutable_vector (entry_t, length, addr)
typedef immutable_vector (entry_t : t@ype+) =
  [length : int] immutable_vector (entry_t, length)

typedef nil_immutable_vector =
  {entry_t : t@ype} {length : int | length == 0}
  immutable_vector (entry_t, length)

prfn lemma_immutable_vector_param :
  {entry_t : t@ype} {n : int} {p : addr}
  immutable_vector (entry_t, n, p) -<prf>
    [0 <= n; null <= p; (n == 0) == (p == null)] void

absprop IMMUTABLE_VECTOR_ENTRY_REF (vector : addr, index : int,
                                    entry_ref : addr)
abstype immutable_vector_entry_ref (entry_t : t@ype+, addr : addr) = ptr addr
typedef immutable_vector_entry_ref (entry_t : t@ype+) =
  [entry_addr : addr] immutable_vector_entry_ref (entry_t, entry_addr)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Function types.
//

typedef immutable_vector_is_nil_function =
  {entry_t : t@ype} {n : int} {p : addr}
  immutable_vector (entry_t, n, p) ->
    [b : bool | b == (n == 0); b == (p == null); 0 <= n; null <= p] bool b

typedef immutable_vector_length_function =
  {entry_t : t@ype} {n : int} {p : addr}
  immutable_vector (entry_t, n, p) ->
    [(n == 0) == (p == null); 0 <= n; null <= p] size_t n

typedef immutable_vector_get_function (entry_t : t@ype) = 
  {n : int} {i : nat | i < n}
  (immutable_vector (entry_t, n), size_t i) -> entry_t

typedef immutable_vector_get_exn_function (entry_t : t@ype) = 
  (immutable_vector entry_t, size_t) -> entry_t

typedef immutable_vector_gets_as_viewptr_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat | i + num <= n}
  (immutable_vector (entry_t, n), size_t i, size_t num) ->
    [addr : addr] (array_v (entry_t, addr, num) | ptr addr)

typedef immutable_vector_gets_as_arrayref_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat | i + num <= n}
  (immutable_vector (entry_t, n), size_t i, size_t num) ->
    arrayref (entry_t, num)

typedef immutable_vector_gets_as_viewptr_exn_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat}
  (immutable_vector (entry_t, n), size_t i, size_t num) ->
    [addr : addr] (array_v (entry_t, addr, num) | ptr addr)

typedef immutable_vector_gets_as_arrayref_exn_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat}
  (immutable_vector (entry_t, n), size_t i, size_t num) ->
    arrayref (entry_t, num)

typedef immutable_vector_gets_to_viewptr_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat | i + num <= n} {addr : addr}
  (!array_v (entry_t?, addr, num) >> array_v (entry_t, addr, num) |
   immutable_vector (entry_t, n), size_t i, size_t num, ptr addr) -> void

typedef immutable_vector_gets_to_array_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat | i + num <= n}
  (immutable_vector (entry_t, n), size_t i, size_t num,
   &(@[entry_t?][num]) >> @[entry_t][num]) -> void

typedef immutable_vector_gets_to_arrayref_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat | i + num <= n}
  {size : nat | num <= size}
  (immutable_vector (entry_t, n), size_t i, size_t num,
   arrayref (entry_t, size)) -> void

typedef immutable_vector_gets_to_arrayref_slice_function (entry_t : t@ype) =
  {n : int} {i : nat} {num : nat | i + num <= n}
  {size, i_start : nat | i_start + num <= size}
  (immutable_vector (entry_t, n), size_t i, size_t num,
   arrayref (entry_t, size), size_t i_start) -> void

typedef immutable_vector_set_function (entry_t : t@ype) =
  {n : int} {i : nat | i < n}
  (immutable_vector (entry_t, n), size_t i, entry_t) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_set_exn_function (entry_t : t@ype) =
  {n : int}
  (immutable_vector (entry_t, n), size_t, entry_t) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_sets_from_viewptr_function (entry_t : t@ype) =
  {n : int} {i : nat}
  {num, size : nat | num <= size; i + num <= n}
  {addr : addr}
  (!array_v (entry_t, addr, size) >> _ |
   immutable_vector (entry_t, n), size_t i, size_t num, ptr addr) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_sets_from_array_function (entry_t : t@ype) =
  {n : int} {i : nat}
  {num, size : nat | num <= size; i + num <= n}
  (immutable_vector (entry_t, n), size_t i,
   size_t num, &(@[entry_t][size])) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_sets_from_arrayref_function (entry_t : t@ype) =
  {n : int} {i : nat}
  {num, size : nat | num <= size; i + num <= n}
  (immutable_vector (entry_t, n), size_t i,
   size_t num, arrayref (entry_t, size)) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_sets_from_viewptr_exn_function (entry_t : t@ype) =
  // These functions raise an exception if n < i + num.
  {n : int} {i : nat}
  {num, size : nat | num <= size}
  {addr : addr}
  (!array_v (entry_t, addr, size) >> _ |
   immutable_vector (entry_t, n), size_t i, size_t num, ptr addr) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_sets_from_array_exn_function (entry_t : t@ype) =
  // These functions raise an exception if n < i + num.
  {n : int} {i : nat}
  {num, size : nat | num <= size}
  (immutable_vector (entry_t, n), size_t i,
   size_t num, &(@[entry_t][size])) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_sets_from_arrayref_exn_function (entry_t : t@ype) =
  // These functions raise an exception if n < i + num.
  {n : int} {i : nat}
  {num, size : nat | num <= size}
  (immutable_vector (entry_t, n), size_t i,
   size_t num, arrayref (entry_t, size)) ->
    immutable_vector (entry_t, n)

typedef immutable_vector_push_function (entry_t : t@ype) =
  {n : int}
  (immutable_vector (entry_t, n), entry_t) ->
    immutable_vector (entry_t, n + 1)

typedef immutable_vector_pushes_from_viewptr_function (entry_t : t@ype) =
  {n : int} {num, size : nat | num <= size} {addr : addr}
  (!array_v (entry_t, addr, size) >> _ |
   immutable_vector (entry_t, n), size_t num, ptr addr) ->
    immutable_vector (entry_t, n + num)

typedef immutable_vector_pushes_from_array_function (entry_t : t@ype) =
  {n : int} {num, size : nat | num <= size}
  (immutable_vector (entry_t, n), size_t num, &(@[entry_t][size])) ->
    immutable_vector (entry_t, n + num)

typedef immutable_vector_pushes_from_arrayref_function (entry_t : t@ype) =
  {n : int} {num, size : nat | num <= size}
  (immutable_vector (entry_t, n), size_t num, arrayref (entry_t, size)) ->
    immutable_vector (entry_t, n + num)

typedef immutable_vector_pop_function (entry_t : t@ype) =
  {n : int | 1 <= n}
  immutable_vector (entry_t, n) -> immutable_vector (entry_t, n - 1)

typedef immutable_vector_pop_exn_function (entry_t : t@ype) =
  // These functions actually raise an exception if n = 0.
  {n : int}
  immutable_vector (entry_t, n) -> immutable_vector (entry_t, n - 1)

typedef immutable_vector_pops_function (entry_t : t@ype) =
  {n : int} {num : nat | 0 <= n - num}
  (immutable_vector (entry_t, n), size_t num) ->
   immutable_vector (entry_t, n - num)

typedef immutable_vector_pops_exn_function (entry_t : t@ype) =
  // These functions actually raise an exception if n < num.
  {n : int} {num : nat}
  (immutable_vector (entry_t, n), size_t num) ->
    immutable_vector (entry_t, n - num)

typedef immutable_vector_slice_append_function (entry_t : t@ype) =
  {m, n : int}
  {i_start, i_end : nat | 0 <= i_end - i_start; i_end <= n}
  (immutable_vector (entry_t, m), immutable_vector (entry_t, n),
   size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, m + (i_end - i_start))

typedef immutable_vector_slice_append_exn_function (entry_t : t@ype) =
  {m, n : int} {i_start, i_end : nat}
  (immutable_vector (entry_t, m), immutable_vector (entry_t, n),
   size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, m + (i_end - i_start))

typedef immutable_vector_slice_function (entry_t : t@ype) =
  {n : int}
  {i_start, i_end : nat | 0 <= i_end - i_start; i_end <= n}
  (immutable_vector (entry_t, n), size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, i_end - i_start)

typedef immutable_vector_slice_exn_function (entry_t : t@ype) =
  {n : int} {i_start, i_end : nat}
  (immutable_vector (entry_t, n), size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, i_end - i_start)

typedef immutable_vector_append_function (entry_t : t@ype) =
  {m, n : nat}
  (immutable_vector (entry_t, m), immutable_vector (entry_t, n)) ->
    immutable_vector (entry_t, m + n)

typedef immutable_vector_slice_reverse_append_function (entry_t : t@ype) =
  {m, n : int}
  {i_start, i_end : nat | i_start <= i_end; i_end <= n}
  (immutable_vector (entry_t, m), immutable_vector (entry_t, n),
   size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, m + (i_end - i_start))

typedef immutable_vector_slice_reverse_append_exn_function (entry_t : t@ype) =
  {m, n : int} {i_start, i_end : nat}
  (immutable_vector (entry_t, m), immutable_vector (entry_t, n),
   size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, m + (i_end - i_start))

typedef immutable_vector_slice_reverse_function (entry_t : t@ype) =
  {n : int}
  {i_start, i_end : nat | i_start <= i_end; i_end <= n}
  (immutable_vector (entry_t, n), size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, i_end - i_start)

typedef immutable_vector_slice_reverse_exn_function (entry_t : t@ype) =
  {n : int} {i_start, i_end : nat}
  (immutable_vector (entry_t, n), size_t i_start, size_t i_end) ->
    immutable_vector (entry_t, i_end - i_start)

typedef immutable_vector_reverse_append_function (entry_t : t@ype) =
  {m, n : int}
  (immutable_vector (entry_t, m), immutable_vector (entry_t, n)) ->
    immutable_vector (entry_t, m + n)

typedef immutable_vector_reverse_function (entry_t : t@ype) =
  {n : int}
  immutable_vector (entry_t, n) -> immutable_vector (entry_t, n)

typedef immutable_vector_entry_ref_function (entry_t : t@ype) =
  {n : int} {i : nat} {vector_addr : addr}
  (immutable_vector (entry_t, n, vector_addr), size_t i) ->
    [entry_addr : addr]
    (IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i, entry_addr) |
     immutable_vector_entry_ref (entry_t, entry_addr))

typedef immutable_vector_entry_ref_is_nil_function (entry_t : t@ype) =
  {entry_addr : addr}
  immutable_vector_entry_ref (entry_t, entry_addr) ->
    [b : bool | b == (entry_addr == null)] bool b

typedef immutable_vector_entry_ref_next_function (entry_t : t@ype) =
  {vector_addr : addr} {n : pos} {i : nat | i < n} {entry_addr : addr}
  (IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i, entry_addr) |
   immutable_vector (entry_t, n, vector_addr), size_t i,
   immutable_vector_entry_ref (entry_t, entry_addr)) ->
    [new_entry_addr : addr]
    (IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i + 1, new_entry_addr) |
     immutable_vector_entry_ref (entry_t, new_entry_addr))

typedef immutable_vector_entry_ref_prev_function (entry_t : t@ype) =
  {vector_addr : addr} {n : pos} {i : nat | i < n} {entry_addr : addr}
  (IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i, entry_addr) |
   immutable_vector (entry_t, n, vector_addr), size_t i,
   immutable_vector_entry_ref (entry_t, entry_addr)) ->
    [new_entry_addr : addr]
    (IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i - 1, new_entry_addr) |
     immutable_vector_entry_ref (entry_t, new_entry_addr))

typedef immutable_vector_entry_ref_get_function (entry_t : t@ype) =
  {entry_addr : addr}
  immutable_vector_entry_ref (entry_t, entry_addr) -> entry_t

// 32-bit Murmur-3 hash functions.
typedef immutable_vector_murmur3_32_function (entry_t : t@ype) =
  {n : int}
  (immutable_vector (entry_t, n), [i : nat | i <= 0xffffffff] uint i) ->
    [i : nat | i <= 0xffffffff] uint i

// 32-bit Murmur-3 hash functions with range-checking of the seed. 
typedef immutable_vector_murmur3_32_exn_function (entry_t : t@ype) =
  {n : int}
  (immutable_vector (entry_t, n), uint) ->
    [i : nat | i <= 0xffffffff] uint i

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

fn {} immutable_vector_is_nil : immutable_vector_is_nil_function
fn {} immutable_vector_length : immutable_vector_length_function

// -  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -

exception ImmutableVectorSubscriptExn of ()
exception ImmutableVectorPopNilExn of ()
exception ImmutableVectorEntryRefNullExn of ()

// -   -   -   -   -

fn {entry_t : t@ype}
immutable_vector_get : immutable_vector_get_function entry_t
fn {entry_t : t@ype}
immutable_vector_get_exn : immutable_vector_get_exn_function entry_t

fn {entry_t : t@ype}
immutable_vector_gets_as_viewptr :
  immutable_vector_gets_as_viewptr_function entry_t
fn {entry_t : t@ype}
immutable_vector_gets_as_arrayref :
  immutable_vector_gets_as_arrayref_function entry_t

overload immutable_vector_gets with immutable_vector_gets_as_viewptr of 0
overload immutable_vector_gets with immutable_vector_gets_as_arrayref of 10

fn {entry_t : t@ype}
immutable_vector_gets_as_viewptr_exn :
  immutable_vector_gets_as_viewptr_exn_function entry_t
fn {entry_t : t@ype}
immutable_vector_gets_as_arrayref_exn :
  immutable_vector_gets_as_arrayref_exn_function entry_t

overload immutable_vector_gets_exn with
  immutable_vector_gets_as_viewptr_exn of 0
overload immutable_vector_gets_exn with
  immutable_vector_gets_as_arrayref_exn of 10

fn {entry_t : t@ype}
immutable_vector_gets_to_viewptr :
  immutable_vector_gets_to_viewptr_function entry_t
fn {entry_t : t@ype}
immutable_vector_gets_to_array :
  immutable_vector_gets_to_array_function entry_t
fn {entry_t : t@ype}
immutable_vector_gets_to_arrayref :
  immutable_vector_gets_to_arrayref_function entry_t
fn {entry_t : t@ype}
immutable_vector_gets_to_arrayref_slice :
  immutable_vector_gets_to_arrayref_slice_function entry_t

overload immutable_vector_gets_eff with immutable_vector_gets_to_viewptr
overload immutable_vector_gets_eff with immutable_vector_gets_to_array
overload immutable_vector_gets_eff with immutable_vector_gets_to_arrayref
overload immutable_vector_gets_eff with immutable_vector_gets_to_arrayref_slice

fn {entry_t : t@ype}
immutable_vector_set : immutable_vector_set_function entry_t
fn {entry_t : t@ype}
immutable_vector_set_exn : immutable_vector_set_exn_function entry_t

fn {entry_t : t@ype}
immutable_vector_sets_from_viewptr :
  immutable_vector_sets_from_viewptr_function entry_t
fn {entry_t : t@ype}
immutable_vector_sets_from_array :
  immutable_vector_sets_from_array_function entry_t
fn {entry_t : t@ype}
immutable_vector_sets_from_arrayref :
  immutable_vector_sets_from_arrayref_function entry_t

overload immutable_vector_sets with immutable_vector_sets_from_viewptr
overload immutable_vector_sets with immutable_vector_sets_from_array
overload immutable_vector_sets with immutable_vector_sets_from_arrayref

fn {entry_t : t@ype}
immutable_vector_sets_from_viewptr_exn :
  immutable_vector_sets_from_viewptr_exn_function entry_t
fn {entry_t : t@ype}
immutable_vector_sets_from_array_exn :
  immutable_vector_sets_from_array_exn_function entry_t
fn {entry_t : t@ype}
immutable_vector_sets_from_arrayref_exn :
  immutable_vector_sets_from_arrayref_exn_function entry_t

overload immutable_vector_sets_exn with immutable_vector_sets_from_viewptr_exn
overload immutable_vector_sets_exn with immutable_vector_sets_from_array_exn
overload immutable_vector_sets_exn with immutable_vector_sets_from_arrayref_exn

fn {entry_t : t@ype}
immutable_vector_push : immutable_vector_push_function entry_t

fn {entry_t : t@ype}
immutable_vector_pushes_from_viewptr :
  immutable_vector_pushes_from_viewptr_function entry_t
fn {entry_t : t@ype}
immutable_vector_pushes_from_array :
  immutable_vector_pushes_from_array_function entry_t
fn {entry_t : t@ype}
immutable_vector_pushes_from_arrayref :
  immutable_vector_pushes_from_arrayref_function entry_t

overload immutable_vector_pushes with immutable_vector_pushes_from_viewptr
overload immutable_vector_pushes with immutable_vector_pushes_from_array
overload immutable_vector_pushes with immutable_vector_pushes_from_arrayref

// The default return value of immutable_vector_pop$clear_popped_entries
// it `true'. Set it to `false' if there is no need to clear popped
// entries: if they are not collectible as garbage, or if you are
// content to allow a limited amount of avoidable leakage in exchange
// for a bit of speed.
fn {entry_t : t@ype}
immutable_vector_pop$clear_popped_entries : () -> bool

fn {entry_t : t@ype}
immutable_vector_pop : immutable_vector_pop_function entry_t
fn {entry_t : t@ype}
immutable_vector_pop_exn : immutable_vector_pop_exn_function entry_t

fn {entry_t : t@ype}
immutable_vector_pops : immutable_vector_pops_function entry_t
fn {entry_t : t@ype}
immutable_vector_pops_exn : immutable_vector_pops_exn_function entry_t

// -   -   -   -   -

fn {entry_t : t@ype}
immutable_vector_slice_append : immutable_vector_slice_append_function entry_t
fn {entry_t : t@ype}
immutable_vector_slice_append_exn :
  immutable_vector_slice_append_exn_function entry_t

fn {entry_t : t@ype}
immutable_vector_slice : immutable_vector_slice_function entry_t
fn {entry_t : t@ype}
immutable_vector_slice_exn : immutable_vector_slice_exn_function entry_t

fn {entry_t : t@ype}
immutable_vector_append : immutable_vector_append_function entry_t

fn {entry_t : t@ype}
immutable_vector_slice_reverse_append :
  immutable_vector_slice_reverse_append_function entry_t
fn {entry_t : t@ype}
immutable_vector_slice_reverse_append_exn :
  immutable_vector_slice_reverse_append_exn_function entry_t

fn {entry_t : t@ype}
immutable_vector_slice_reverse : immutable_vector_slice_reverse_function entry_t
fn {entry_t : t@ype}
immutable_vector_slice_reverse_exn :
  immutable_vector_slice_reverse_exn_function entry_t

fn {entry_t : t@ype}
immutable_vector_reverse_append :
  immutable_vector_reverse_append_function entry_t

fn {entry_t : t@ype}
immutable_vector_reverse : immutable_vector_reverse_function entry_t

// -   -   -   -   -

fn {entry_t : t@ype}
immutable_vector_entry_ref : immutable_vector_entry_ref_function entry_t

fn {entry_t : t@ype}
immutable_vector_entry_ref_is_nil :
  immutable_vector_entry_ref_is_nil_function entry_t

fn {entry_t : t@ype}
immutable_vector_entry_ref_next :
  immutable_vector_entry_ref_next_function entry_t

fn {entry_t : t@ype}
immutable_vector_entry_ref_prev :
  immutable_vector_entry_ref_prev_function entry_t

fn {entry_t : t@ype}
immutable_vector_entry_ref_get :
  immutable_vector_entry_ref_get_function entry_t

// -   -   -   -   -

exception ImmutableVectorHashSeedExn of ()

fn {entry_t : t@ype}
immutable_vector_murmur3_32 : immutable_vector_murmur3_32_function entry_t

fn {entry_t : t@ype}
immutable_vector_murmur3_32_exn :
  immutable_vector_murmur3_32_exn_function entry_t

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Modules.
//

typedef immutable_vector_module (entry_t : t@ype+) =
  '{
    nil = immutable_vector (entry_t, 0),
    is_nil = immutable_vector_is_nil_function,
    length = immutable_vector_length_function,
    get = immutable_vector_get_function entry_t,
    get_exn = immutable_vector_get_exn_function entry_t,
    gets_viewptr = immutable_vector_gets_as_viewptr_function entry_t,
    gets_arrayref = immutable_vector_gets_as_arrayref_function entry_t,
    gets_viewptr_exn = immutable_vector_gets_as_viewptr_exn_function entry_t,
    gets_arrayref_exn = immutable_vector_gets_as_arrayref_exn_function entry_t,
    gets_eff_viewptr = immutable_vector_gets_to_viewptr_function entry_t,
    gets_eff_array = immutable_vector_gets_to_array_function entry_t,
    gets_eff_arrayref = immutable_vector_gets_to_arrayref_function entry_t,
    gets_eff_arrayref_slice =
      immutable_vector_gets_to_arrayref_slice_function entry_t,
    set = immutable_vector_set_function entry_t,
    set_exn = immutable_vector_set_exn_function entry_t,
    sets_viewptr = immutable_vector_sets_from_viewptr_function entry_t,
    sets_array = immutable_vector_sets_from_array_function entry_t,
    sets_arrayref = immutable_vector_sets_from_arrayref_function entry_t,
    sets_viewptr_exn = immutable_vector_sets_from_viewptr_exn_function entry_t,
    sets_array_exn = immutable_vector_sets_from_array_exn_function entry_t,
    sets_arrayref_exn = immutable_vector_sets_from_arrayref_exn_function entry_t,
    push = immutable_vector_push_function entry_t,
    pushes_viewptr = immutable_vector_pushes_from_viewptr_function entry_t,
    pushes_array = immutable_vector_pushes_from_array_function entry_t,
    pushes_arrayref = immutable_vector_pushes_from_arrayref_function entry_t,
    pop = immutable_vector_pop_function entry_t,
    pop_exn = immutable_vector_pop_exn_function entry_t,
    pops = immutable_vector_pops_function entry_t,
    pops_exn = immutable_vector_pops_exn_function entry_t,
    slice_append = immutable_vector_slice_append_function entry_t,
    slice_append_exn = immutable_vector_slice_append_exn_function entry_t,
    slice = immutable_vector_slice_function entry_t,
    slice_exn = immutable_vector_slice_exn_function entry_t,
    append = immutable_vector_append_function entry_t,
    slice_reverse_append =
      immutable_vector_slice_reverse_append_function entry_t,
    slice_reverse_append_exn =
      immutable_vector_slice_reverse_append_exn_function entry_t,
    slice_reverse = immutable_vector_slice_reverse_function entry_t,
    slice_reverse_exn = immutable_vector_slice_reverse_exn_function entry_t,
    reverse_append = immutable_vector_reverse_append_function entry_t,
    reverse = immutable_vector_reverse_function entry_t,
    entry_ref = immutable_vector_entry_ref_function entry_t,
    entry_ref_is_nil = immutable_vector_entry_ref_is_nil_function entry_t,
    entry_ref_next = immutable_vector_entry_ref_next_function entry_t,
    entry_ref_prev = immutable_vector_entry_ref_prev_function entry_t,
    entry_ref_get = immutable_vector_entry_ref_get_function entry_t,
    murmur3_32 = immutable_vector_murmur3_32_function entry_t,
    murmur3_32_exn = immutable_vector_murmur3_32_exn_function entry_t
  }

fn {entry_t : t@ype}
make_immutable_vector_module : () -> immutable_vector_module entry_t

//--------------------------------------------------------------------
