// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_PACKNAME "SMCORE.array_misc"

// unvbox is adapted from a praxi in libats/ML/DATS/array0.dats
praxi unvbox : {v : view} vbox v -<prf> (v, v -<lin,prf> void)

praxi array_v_unsafely_force_initialized :
  {t : vt@ype} {p : addr} {n : int}
  (!array_v (t?, p, n) >> array_v (t, p, n)) -<prf> void

praxi array_v_uninitize :
  {t : vt@ype} {p : addr} {n : int}
    (!array_v (t, p, n) >> array_v (t?, p, n)) -<prf> void

fn {t : vt@ype}
array_ptr_split_at :
  {p : addr} {n : int} {i : nat | i < n}
  (array_v (t, p, n) | ptr p, size_t i) -<>
    (array_v (t, p, i), array_v (t, p + i * sizeof t, n - i) |
     ptr p, ptr (p + i * sizeof t))

//--------------------------------------------------------------------
