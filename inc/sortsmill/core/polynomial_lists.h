/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_LISTS_H_
#define SORTSMILL_CORE_POLYNOMIAL_LISTS_H_

#include <stdlib.h>
#include <stdbool.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

typedef struct polynomial_list_entry_t
{
  struct polynomial_list_entry_t *next;
  size_t degree;
  mpq_t *poly;
} polynomial_list_entry_t;

void clear_polynomial_list (polynomial_list_entry_t *);
size_t polynomial_list_length (polynomial_list_entry_t *);

/* *INDENT-OFF* */

polynomial_list_entry_t *
reverse_polynomial_list_in_place (polynomial_list_entry_t *);

/* *INDENT-ON* */

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_LISTS_H_ */
