/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_ISTRINGS_H_
#define SORTSMILL_CORE_ISTRINGS_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* The length-zero istring_t is equal to the NULL pointer. */
typedef struct _istring__ *istring_t;

size_t istring_length (istring_t s);

istring_t str2istring (const char *s);
istring_t str2istring2 (const char *s0, const char *s1);
istring_t str2istring3 (const char *s0, const char *s1, const char *s2);
istring_t str2istring4 (const char *s0, const char *s1, const char *s2,
                        const char *s3);
istring_t str2istring5 (const char *s0, const char *s1, const char *s2,
                        const char *s3, const char *s4);
istring_t str2istringn (const char *s0, ...);

char *istring2str (istring_t s);

istring_t strslice2istring (const char *s, size_t i_start, size_t i_end);
char *istringslice2str (istring_t s, size_t i_start, size_t i_end);
istring_t istring_slice (istring_t s, size_t i_start, size_t i_end);

istring_t istring_str_append (istring_t s, const char *s1);
istring_t istring_str_append2 (istring_t s, const char *s1, const char *s2);
istring_t istring_str_append3 (istring_t s, const char *s1, const char *s2,
                               const char *s3);
istring_t istring_str_append4 (istring_t s, const char *s1, const char *s2,
                               const char *s3, const char *s4);
istring_t istring_str_append5 (istring_t s, const char *s1, const char *s2,
                               const char *s3, const char *s4, const char *s5);
istring_t istring_str_appendn (istring_t s, ...);

istring_t istring_concat (istring_t s0, istring_t s1);
istring_t istring_concat3 (istring_t s0, istring_t s1, istring_t s2);
istring_t istring_concat4 (istring_t s0, istring_t s1, istring_t s2,
                           istring_t s3);
istring_t istring_concat5 (istring_t s0, istring_t s1, istring_t s2,
                           istring_t s3, istring_t s4);
istring_t istring_concatn (size_t n, ...);

int istring_cmp (istring_t s, istring_t t);
int istring_str_cmp (istring_t s, const char *t);
int str_istring_cmp (const char *s, istring_t t);

int istring_casecmp (istring_t s, istring_t t);
int istring_str_casecmp (istring_t s, const char *t);
int str_istring_casecmp (const char *s, istring_t t);

int istring_c_casecmp (istring_t s, istring_t t);
int istring_str_c_casecmp (istring_t s, const char *t);
int str_istring_c_casecmp (const char *s, istring_t t);

char istring_get (istring_t s, size_t i);
istring_t istring_set (istring_t s, size_t i, char c);
istring_t istring_push (istring_t s, char c);
istring_t istring_pop (istring_t s);
istring_t istring_pops (istring_t s, size_t n);

unsigned int istring_murmur3_32 (istring_t s, unsigned int seed);

/* Support for multibyte characters representing all 31-bit code
 * points. The representation is `extended UTF-8': up to 6 bytes are
 * used per character, and overlong sequences are allowed.  (The
 * latter fact means that the representation of a given code point is
 * not necessarily unique.)
 */
bool istring_is_multibyte (istring_t s); /* Examines the entire
                                            string. */
size_t istring_multibyte_last (istring_t s);
size_t istring_multibyte_next (istring_t s, size_t i);
size_t istring_multibyte_prev (istring_t s, size_t i);
int istring_multibyte_get (istring_t s, size_t i);
void istring_multibyte_get_eff (istring_t s, size_t i,
                                int *code_point, size_t *length);
istring_t istring_multibyte_push (istring_t s, int u);
istring_t istring_multibyte_pop (istring_t s);
istring_t istring_multibyte_pops (istring_t s, size_t num);

/* Support for Unicode characters represented in (valid) UTF-8. */
bool istring_is_unicode (istring_t s); /* Examines the entire
                                          string. */
size_t istring_unicode_last (istring_t s);
size_t istring_unicode_next (istring_t s, size_t i);
size_t istring_unicode_prev (istring_t s, size_t i);
int istring_unicode_get (istring_t s, size_t i);
void istring_unicode_get_eff (istring_t s, size_t i,
                              int *code_point, size_t *length);
istring_t istring_unicode_push (istring_t s, int u);
istring_t istring_unicode_pop (istring_t s);
istring_t istring_unicode_pops (istring_t s, size_t num);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_ISTRINGS_H_ */
