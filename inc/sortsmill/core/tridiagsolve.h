/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_TRIDIAGSOLVE_H_
#define SORTSMILL_CORE_TRIDIAGSOLVE_H_

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Solve a tridiagonal n-by-n matrix, by gaussian elimination with
 * partial pivoting.
 *
 * The parameters and behavior are like those of SGTSV and DGTSV of
 * LAPACK.
 */

void stridiagsolve (int n, int nrhs, float *dl, float *d__, float *du,
                    float *b, int ldb, int *info);
void dtridiagsolve (int n, int nrhs, double *dl, double *d__, double *du,
                    double *b, int ldb, int *info);
void ltridiagsolve (int n, int nrhs, long double *dl, long double *d__,
                    long double *du, long double *b, int ldb, int *info);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_TRIDIAGSOLVE_H_ */
