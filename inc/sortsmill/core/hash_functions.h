/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 * This file is part of Sorts Mill Tools.
 * 
 * Sorts Mill Tools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_HASH_FUNCTIONS_H_
#define SORTSMILL_CORE_HASH_FUNCTIONS_H_

/*
 * Non-cryptographic hash functions.
 *
 * See also <sortsmill/core/murmurhash3.h>
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* A more-or-less arbitrary seed value that we currently define as
   follows. */
#define SMCORE_HASH_SEED ((uint32_t) 12345)

/* Hashes of various simple types. */
uint32_t smcore_uint32_bool (bool);
uint32_t smcore_uint32_char (char);
uint32_t smcore_uint32_schar (signed char);
uint32_t smcore_uint32_uchar (unsigned char);
uint32_t smcore_uint32_short (short int);
uint32_t smcore_uint32_ushort (unsigned short int);
uint32_t smcore_uint32_int (int);
uint32_t smcore_uint32_uint (unsigned int);
uint32_t smcore_uint32_long (long int);
uint32_t smcore_uint32_ulong (unsigned long int);
uint32_t smcore_uint32_longlong (long long int);
uint32_t smcore_uint32_ulonglong (unsigned long long int);
uint32_t smcore_uint32_int8 (int8_t);
uint32_t smcore_uint32_uint8 (uint8_t);
uint32_t smcore_uint32_int16 (int16_t);
uint32_t smcore_uint32_uint16 (uint16_t);
uint32_t smcore_uint32_int32 (int32_t);
uint32_t smcore_uint32_uint32 (uint32_t);
uint32_t smcore_uint32_int64 (int64_t);
uint32_t smcore_uint32_uint64 (uint64_t);
uint32_t smcore_uint32_ssize_t (ssize_t);
uint32_t smcore_uint32_size_t (size_t);
uint32_t smcore_uint32_intptr (intptr_t);
uint32_t smcore_uint32_uintptr (uintptr_t);
uint32_t smcore_uint32_intmax (intmax_t);
uint32_t smcore_uint32_uintmax (uintmax_t);
uint32_t smcore_uint32_pointer (void *);

/* Hash of a nul-terminated string, _not_ taking into account the nul
   character. The string must be no longer than INT_MAX, not counting
   the terminating nul. */
uint32_t smcore_uint32_strhash (const char *);

/* Hash of a zero-terminated Unicode string, _not_ taking into account
   the terminating zero. The string must contain no more than INT_MAX
   bytes, not counting the terminating zero. WARNING: These functions
   are naive about Unicode. You will get different results for the
   `same' string stored in different encodings or if they are not
   canonicalized to exactly the same byte sequence. */
uint32_t smcore_uint32_u8_strhash (const uint8_t *);
uint32_t smcore_uint32_u16_strhash (const uint16_t *);
uint32_t smcore_uint32_u32_strhash (const uint32_t *);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_HASH_FUNCTIONS_H_ */
