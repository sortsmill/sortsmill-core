/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCORE_UNISTR__CATS_
#define SMCORE_UNISTR__CATS_

#include <unistr.h>

#define _smcore_unistr__u8_check_unsafe(s, n) ((uint8_t *) u8_check ((s), (n)))
#define _smcore_unistr__u16_check_unsafe(s, n) ((uint16_t *) u16_check ((s), (n)))
#define _smcore_unistr__u32_check_unsafe(s, n) ((uint32_t *) u32_check ((s), (n)))

#define _smcore_unistr__u8_to_u16_unsafe u8_to_u16
#define _smcore_unistr__u8_to_u16_alloc_unsafe(s, n, lengthp) (u8_to_u16 ((s), (n), 0, (lengthp)))
#define _smcore_unistr__u8_to_u32_unsafe u8_to_u32
#define _smcore_unistr__u8_to_u32_alloc_unsafe(s, n, lengthp) (u8_to_u32 ((s), (n), 0, (lengthp)))
#define _smcore_unistr__u16_to_u8_unsafe u16_to_u8
#define _smcore_unistr__u16_to_u8_alloc_unsafe(s, n, lengthp) (u16_to_u8 ((s), (n), 0, (lengthp)))
#define _smcore_unistr__u16_to_u32_unsafe u16_to_u32
#define _smcore_unistr__u16_to_u32_alloc_unsafe(s, n, lengthp) (u16_to_u32 ((s), (n), 0, (lengthp)))
#define _smcore_unistr__u32_to_u8_unsafe u32_to_u8
#define _smcore_unistr__u32_to_u8_alloc_unsafe(s, n, lengthp) (u32_to_u8 ((s), (n), 0, (lengthp)))
#define _smcore_unistr__u32_to_u16_unsafe u32_to_u16
#define _smcore_unistr__u32_to_u16_alloc_unsafe(s, n, lengthp) (u32_to_u16 ((s), (n), 0, (lengthp)))

#define _smcore_unistr__u8_mblen_unsafe u8_mblen
#define _smcore_unistr__u16_mblen_unsafe u16_mblen
#define _smcore_unistr__u32_mblen_unsafe u32_mblen

#define _smcore_unistr__u8_mbtouc_unsafe_unsafe u8_mbtouc_unsafe
#define _smcore_unistr__u16_mbtouc_unsafe_unsafe u16_mbtouc_unsafe
#define _smcore_unistr__u32_mbtouc_unsafe_unsafe u32_mbtouc_unsafe
#define _smcore_unistr__u8_mbtouc_unsafe u8_mbtouc
#define _smcore_unistr__u16_mbtouc_unsafe u16_mbtouc
#define _smcore_unistr__u32_mbtouc_unsafe u32_mbtouc
#define _smcore_unistr__u8_mbtoucr_unsafe u8_mbtoucr
#define _smcore_unistr__u16_mbtoucr_unsafe u16_mbtoucr
#define _smcore_unistr__u32_mbtoucr_unsafe u32_mbtoucr

#define _smcore_unistr__u8_uctomb_unsafe u8_uctomb
#define _smcore_unistr__u16_uctomb_unsafe u16_uctomb
#define _smcore_unistr__u32_uctomb_unsafe u32_uctomb

#define _smcore_unistr__u8_cpy_unsafe u8_cpy
#define _smcore_unistr__u16_cpy_unsafe u16_cpy
#define _smcore_unistr__u32_cpy_unsafe u32_cpy

#define _smcore_unistr__u8_move_unsafe u8_move
#define _smcore_unistr__u16_move_unsafe u16_move
#define _smcore_unistr__u32_move_unsafe u32_move

#define _smcore_unistr__u8_set_unsafe u8_set
#define _smcore_unistr__u16_set_unsafe u16_set
#define _smcore_unistr__u32_set_unsafe u32_set

#define _smcore_unistr__u8_cmp_unsafe u8_cmp
#define _smcore_unistr__u16_cmp_unsafe u16_cmp
#define _smcore_unistr__u32_cmp_unsafe u32_cmp

#define _smcore_unistr__u8_cmp2_unsafe u8_cmp2
#define _smcore_unistr__u16_cmp2_unsafe u16_cmp2
#define _smcore_unistr__u32_cmp2_unsafe u32_cmp2

#define _smcore_unistr__u8_chr_unsafe u8_chr
#define _smcore_unistr__u16_chr_unsafe u16_chr
#define _smcore_unistr__u32_chr_unsafe u32_chr

#define _smcore_unistr__u8_mbsnlen_unsafe u8_mbsnlen
#define _smcore_unistr__u16_mbsnlen_unsafe u16_mbsnlen
#define _smcore_unistr__u32_mbsnlen_unsafe u32_mbsnlen

#define _smcore_unistr__u8_cpy_alloc_unsafe u8_cpy_alloc
#define _smcore_unistr__u16_cpy_alloc_unsafe u16_cpy_alloc
#define _smcore_unistr__u32_cpy_alloc_unsafe u32_cpy_alloc


#endif /* SMCORE_UNISTR__CATS_ */
