/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCORE_UNICODE_HELPERS__CATS_
#define SMCORE_UNICODE_HELPERS__CATS_

#define _smcore_unicode_helpers__2_entries(v)   \
  (v), (v)

#define _smcore_unicode_helpers__4_entries(v)   \
  _smcore_unicode_helpers__2_entries (v),       \
    _smcore_unicode_helpers__2_entries (v)

#define _smcore_unicode_helpers__8_entries(v)   \
  _smcore_unicode_helpers__4_entries (v),       \
    _smcore_unicode_helpers__4_entries (v)

#define _smcore_unicode_helpers__16_entries(v)  \
  _smcore_unicode_helpers__8_entries (v),       \
    _smcore_unicode_helpers__8_entries (v)

#define _smcore_unicode_helpers__32_entries(v)  \
  _smcore_unicode_helpers__16_entries (v),      \
    _smcore_unicode_helpers__16_entries (v)

#define _smcore_unicode_helpers__64_entries(v)  \
  _smcore_unicode_helpers__32_entries (v),      \
    _smcore_unicode_helpers__32_entries (v)

#define _smcore_unicode_helpers__128_entries(v) \
  _smcore_unicode_helpers__64_entries (v),      \
    _smcore_unicode_helpers__64_entries (v)

static const atstype_int8 _smcore_unicode_helpers__extended_utf8_lengths__[256] = {
  _smcore_unicode_helpers__128_entries (1),
  _smcore_unicode_helpers__64_entries (-1),
  _smcore_unicode_helpers__32_entries (2),
  _smcore_unicode_helpers__16_entries (3),
  _smcore_unicode_helpers__8_entries (4),
  _smcore_unicode_helpers__4_entries (5),
  _smcore_unicode_helpers__2_entries (6),
  _smcore_unicode_helpers__2_entries (-1)
};

static const atstype_int8 _smcore_unicode_helpers__utf8_lengths__[256] = {
  _smcore_unicode_helpers__128_entries (1),
  _smcore_unicode_helpers__64_entries (-1),
  _smcore_unicode_helpers__32_entries (2),
  _smcore_unicode_helpers__16_entries (3),
  _smcore_unicode_helpers__8_entries (4),
  _smcore_unicode_helpers__4_entries (-1),
  _smcore_unicode_helpers__2_entries (-1),
  _smcore_unicode_helpers__2_entries (-1)
};

#define _smcore_unicode_helpers__extended_utf8_character_length(c)      \
  (_smcore_unicode_helpers__extended_utf8_lengths__[(atstype_uint8) (c)])

#define _smcore_unicode_helpers__utf8_character_length(c)               \
  (_smcore_unicode_helpers__utf8_lengths__[(atstype_uint8) (c)])

#endif /* SMCORE_UNICODE_HELPERS__CATS_ */
