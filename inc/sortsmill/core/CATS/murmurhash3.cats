/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCORE_MURMURHASH3__CATS_
#define SMCORE_MURMURHASH3__CATS_

#include <sortsmill/core/murmurhash3.h>

#define _smcore_murmurhash3__smcore_murmurhash3_x86_32 smcore_murmurhash3_x86_32
#define _smcore_murmurhash3__smcore_murmurhash3_x86_128 smcore_murmurhash3_x86_128
#define _smcore_murmurhash3__smcore_murmurhash3_x64_128 smcore_murmurhash3_x64_128
#define _smcore_murmurhash3__smcore_murmurhash3_32 smcore_murmurhash3_32
#define _smcore_murmurhash3__smcore_murmurhash3_128 smcore_murmurhash3_128
#define _smcore_murmurhash3__smcore_uint32_murmurhash3 smcore_uint32_murmurhash3

#endif /* SMCORE_MURMURHASH3__CATS_ */
