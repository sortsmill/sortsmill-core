/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCORE_ISTRINGS__CATS_
#define SMCORE_ISTRINGS__CATS_

#include <string.h>

#define _smcore_istrings__strncmp_for_istrings memcmp
#define _smcore_istrings__strncasecmp_for_istrings smcore_istrings__memcasecmp_for_istrings
#define _smcore_istrings__c_strncasecmp_for_istrings smcore_istrings__c_memcasecmp_for_istrings

#define _smcore_istrings__c2i(c) ((atstype_int) ((atstype_uint8) (c)))

#endif /* SMCORE_ISTRINGS__CATS_ */
