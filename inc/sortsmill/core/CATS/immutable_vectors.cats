/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * Part of this file are adapted from the Gnulib file byteswap.in.h, which is
 * Copyright (C) 2005, 2007, 2009-2015 Free Software Foundation, Inc.
 *  Written by Oskar Liljeblad <oskar@osk.mine.nu>, 2005.
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCORE_IMMUTABLE_VECTORS__CATS_
#define SMCORE_IMMUTABLE_VECTORS__CATS_

#include <assert.h>
#include <sortsmill/core.h>

#define _smcore_immutable_vectors__struct_wrapper_size(num_bytes) \
  (sizeof (struct { char x[num_bytes]; }))

#define _smcore_immutable_vectors__struct_wrapper_copy(dest, src,       \
                                                       num_bytes)       \
  do                                                                    \
    {                                                                   \
      struct wrapper { char x[num_bytes]; };                            \
      *(struct wrapper *) (dest) = *(struct wrapper *) (src);           \
    }                                                                   \
  while (0)

#define _smcore_immutable_vectors__null() ((void *) 0)
#define _smcore_immutable_vectors__is_null(p) ((p) == (void *) 0)

ATSinline () atstype_bool
_smcore_immutable_vectors__immutable_vector_is_nil (atstype_boxed p)
{
  return (p == (void *) 0);
}

ATSinline () atstype_size
_smcore_immutable_vectors__shift_left (atstype_size i, atstype_size n)
{
  return (i << n);
}

ATSinline () atstype_size
_smcore_immutable_vectors__shift_right (atstype_size i, atstype_size n)
{
  return (i >> n);
}

/* Given an unsigned 32-bit argument X, return the value corresponding
   to X with reversed byte order. This definition is adapted from the
   Gnulib file byteswap.in.h */
#define _smcore_immutable_vectors__bswap_32(x)  \
  ((((x) & 0x000000FF) << 24) |                 \
   (((x) & 0x0000FF00) << 8) |                  \
   (((x) & 0x00FF0000) >> 8) |                  \
   (((x) & 0xFF000000) >> 24))

ATSinline () atstype_bool
_smcore_immutable_vectors__test_if_little_endian (void)
{
  const unsigned int i = 1;
  return (*(atstype_uint8 *) &i);
}

ATSinline () atstype_uint32
_smcore_immutable_vectors__murmur3_blocks (atstype_uint32 h,
                                           atstype_ptr p,
                                           atstype_size nblocks,
                                           atstype_bool little_endian)
{
  const atstype_uint8 *p1 = (const atstype_uint8 *) p;
  const atstype_uint8 *const p_end = p1 + 4 * nblocks;
  while (p1 != p_end)
    {
      atstype_uint32 k;
#if defined __GNUC__
      __builtin_memcpy (&k, p1, 4);
#else
      memcpy (&k, p1, 4);
#endif
      if (!little_endian)
        k = _smcore_immutable_vectors__bswap_32 (k);

      k *= 0xcc9e2d51;
      k = (k << 15) | (k >> (32 - 15)); // Rotate left 15 bits.
      k *= 0x1b873593;

      h ^= k;
      h = (h << 13) | (h >> (32 - 13)); // Rotate left 13 bits.
      h = h * 5 + 0xe6546b64;

      p1 += 4;
    }
  return h;
}

ATSinline () atstype_uint32
_smcore_immutable_vectors__murmur3_tail (atstype_uint32 h,
                                         atstype_ptr p,
                                         atstype_size tail_length)
{
  atstype_uint32 k = 0;
  switch (tail_length)
    {
    case 3:
      k ^= ((atstype_uint32) ((const atstype_uint8 *) p)[2]) << 16;
    case 2:
      k ^= ((atstype_uint32) ((const atstype_uint8 *) p)[1]) << 8;
    case 1:
      k ^= ((atstype_uint32) ((const atstype_uint8 *) p)[0]);
      k *= 0xcc9e2d51;
      k = (k << 15) | (k >> (32 - 15)); // Rotate left 15 bits.
      k *= 0x1b873593;
      h ^= k;
    };
  return h;
}

ATSinline () atstype_uint32
_smcore_immutable_vectors__murmur3_fmix32 (atstype_uint32 h)
{
  h ^= h >> 16;
  h *= 0x85ebca6b;
  h ^= h >> 13;
  h *= 0xc2b2ae35;
  h ^= h >> 16;
  return h;
}

ATSinline () atstype_uint32
_smcore_immutable_vectors__murmur3_32_finalize (atstype_uint32 h,
                                                atstype_size length)
{
  return
    _smcore_immutable_vectors__murmur3_fmix32 (h ^ (atstype_uint32) length);
}

#endif /* SMCORE_IMMUTABLE_VECTORS__CATS_ */
