/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCORE_BITSTRING_MAPS__CATS_
#define SMCORE_BITSTRING_MAPS__CATS_

/*
 * NOTE/WARNING: Regarding POPCOUNT instructions.
 *
 * The following implementation uses smcore_popcount() and so, if
 * possible, one ought to enable compiler options to emit POPCOUNT
 * instructions. Otherwise a POPCOUNT subroutine might be called or
 * inline code used instead.
 *
 * If you are using GCC and building just for the machine on which you
 * are compiling, the ‘-march=native’ option likely will suffice.
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <sortsmill/core/attributes.h>
#include <sortsmill/core/popcount.h>

#include <sortsmill/core/CATS/SHARE/bitstring_maps_defines.cats>

#define _smcore_bitstring_maps__inline ATSinline()

/*--------------------------------------------------------------------*/

#if _smcore_bitstring_maps__BITS_PER_LEVEL < 1 || 6 < _smcore_bitstring_maps__BITS_PER_LEVEL
#error The setting of _smcore_bitstring_maps__BITS_PER_LEVEL is not supported.
#endif

#if (1 << (_smcore_bitstring_maps__BITS_PER_LEVEL)) != _smcore_bitstring_maps__TABLE_SIZE_MAX
#error The settings of _smcore_bitstring_maps__BITS_PER_LEVEL and _smcore_bitstring_maps__TABLE_SIZE_MAX are inconsistent.
#endif

#define _smcore_bitstring_maps__bitmap _smcore_bitstring_maps__BITMAP_C_TYPE

/*--------------------------------------------------------------------*/

#define _smcore_bitstring_maps__lnot(a) (~(a))
#define _smcore_bitstring_maps__land(a, b) ((a) & (b))
#define _smcore_bitstring_maps__lor(a, b) ((a) | (b))
#define _smcore_bitstring_maps__lxor(a, b) ((a) ^ (b))

#define _smcore_bitstring_maps__eq(a, b) ((a) == (b))
#define _smcore_bitstring_maps__neq(a, b) ((a) != (b))

#define _smcore_bitstring_maps__shift_left(a, b) ((a) << (b))
#define _smcore_bitstring_maps__shift_right(a, b) ((a) >> (b))

#define _smcore_bitstring_maps__popcount(a)     \
  (_smcore_bitstring_maps__POPCOUNT (a))

#define _smcore_bitstring_maps__mask_right(TYPE, a, i)  \
  ((a) & ~(~((TYPE) 0) << (i)))

#define _smcore_bitstring_maps__popcount_upto(a, i)             \
  (_smcore_bitstring_maps__POPCOUNT                             \
   _smcore_bitstring_maps__mask_right                           \
   (_smcore_bitstring_maps__bitmap, a, i))

/*--------------------------------------------------------------------*/

typedef struct
{
  /* Bitmap of which entries are stored. */
  _smcore_bitstring_maps__bitmap is_stored;

  /* Bitmap of entries that contain a key-value pair. */
  _smcore_bitstring_maps__bitmap is_pair;

  /* Size of the table. This is redundant with the `is_stored' field,
     whose popcount equals the table size (once fully set);
     however, the extra field makes things a little simpler. */
  atstype_int size;

  /* An array of stored entries. */
  atstype_ptr cells[];
} _smcore_bitstring_maps__ptr_table;

/*--------------------------------------------------------------------*/

#define _smcore_bitstring_maps__get_is_stored(t)                \
  (((_smcore_bitstring_maps__ptr_table *) t)->is_stored)
#define _smcore_bitstring_maps__set_is_stored(t, v)                     \
  do                                                                    \
    {                                                                   \
      ((_smcore_bitstring_maps__ptr_table *) t)->is_stored = (v);       \
    }                                                                   \
  while (0);

#define _smcore_bitstring_maps__get_is_pair(t)          \
  (((_smcore_bitstring_maps__ptr_table *) t)->is_pair)
#define _smcore_bitstring_maps__set_is_pair(t, v)               \
  do                                                            \
    {                                                           \
      ((_smcore_bitstring_maps__ptr_table *) t)->is_pair = (v); \
    }                                                           \
  while (0);

#define _smcore_bitstring_maps__get_ptr_table_size(t)   \
  (((_smcore_bitstring_maps__ptr_table *) t)->size)

#define _smcore_bitstring_maps__get_cell(t, i)                  \
  (((_smcore_bitstring_maps__ptr_table *) t)->cells[(i)])
#define _smcore_bitstring_maps__set_cell(t, i, v)                       \
  do                                                                    \
    {                                                                   \
      ((_smcore_bitstring_maps__ptr_table *) t)->cells[(i)] = (v);      \
    }                                                                   \
  while (0);

/*--------------------------------------------------------------------*/

_smcore_bitstring_maps__inline atstype_ptr
_smcore_bitstring_maps__ptr_table_malloc (atstype_int size)
{
  const atstype_size byte_count =
    sizeof (_smcore_bitstring_maps__ptr_table) +
    size * sizeof (atstype_ptr);
  _smcore_bitstring_maps__ptr_table *table =
    (_smcore_bitstring_maps__ptr_table *) ATS_MALLOC (byte_count);
  table->size = size;
  return (atstype_ptr) table;
}

/*--------------------------------------------------------------------*/

_smcore_bitstring_maps__inline atstype_ptr
_smcore_bitstring_maps__ptr_table_copy (atstype_ptr p)
{
  _smcore_bitstring_maps__ptr_table *table =
    (_smcore_bitstring_maps__ptr_table *) p;

  const atstype_size size = table->size;

  const atstype_size byte_count =
    sizeof (_smcore_bitstring_maps__ptr_table) +
    size * sizeof (atstype_ptr);
  _smcore_bitstring_maps__ptr_table *the_copy =
    (_smcore_bitstring_maps__ptr_table *) ATS_MALLOC (byte_count);
  memcpy (the_copy, table, byte_count);

  return (atstype_ptr) the_copy;
}

/*--------------------------------------------------------------------*/

typedef struct
{
  atstype_ptrk atslab__table;
  atstype_int atslab__offset;
} _smcore_bitstring_maps__return_value_of_grow;

_smcore_bitstring_maps__inline _smcore_bitstring_maps__return_value_of_grow
_smcore_bitstring_maps__ptr_table_grow (atstype_ptr p, atstype_int index,
                                        atstype_bool is_pair)
{
  _smcore_bitstring_maps__ptr_table *table =
    (_smcore_bitstring_maps__ptr_table *) p;

  const atstype_size old_size = table->size;

  const atstype_size byte_count =
    sizeof (_smcore_bitstring_maps__ptr_table) +
    (old_size + 1) * sizeof (atstype_ptr);
  _smcore_bitstring_maps__ptr_table *the_copy =
    (_smcore_bitstring_maps__ptr_table *) ATS_MALLOC (byte_count);

  _smcore_bitstring_maps__bitmap mask =
    (((_smcore_bitstring_maps__bitmap) 1) << index);
  the_copy->is_stored = table->is_stored | mask;
  if (is_pair)
    the_copy->is_pair = table->is_pair | mask;
  else
    the_copy->is_pair = table->is_pair;
  the_copy->size = old_size + 1;

  const size_t j =
    (size_t) _smcore_bitstring_maps__popcount_upto (table->is_stored, index);
  memcpy (the_copy->cells, table->cells, j * sizeof (atstype_ptr));
  the_copy->cells[j] = (atstype_ptr) 0;
  memcpy (&the_copy->cells[j + 1], &table->cells[j],
          (old_size - j) * sizeof (atstype_ptr));

  _smcore_bitstring_maps__return_value_of_grow retval;
  retval.atslab__table = (atstype_ptrk) the_copy;
  retval.atslab__offset = (atstype_int) j;
  return retval;
}

/*--------------------------------------------------------------------*/

_smcore_bitstring_maps__inline atstype_ptr
_smcore_bitstring_maps__ptr_table_shrink (atstype_ptr p, atstype_int index)
{
  _smcore_bitstring_maps__ptr_table *table =
    (_smcore_bitstring_maps__ptr_table *) p;

  const atstype_size old_size = table->size;
  assert (old_size != 0);

  const atstype_size byte_count =
    sizeof (_smcore_bitstring_maps__ptr_table) +
    (old_size - 1) * sizeof (atstype_ptr);
  _smcore_bitstring_maps__ptr_table *the_copy =
    (_smcore_bitstring_maps__ptr_table *) ATS_MALLOC (byte_count);

  _smcore_bitstring_maps__bitmap mask =
    (((_smcore_bitstring_maps__bitmap) 1) << index);
  assert ((table->is_stored | mask) != 0);
  the_copy->is_stored = table->is_stored & ~mask;
  the_copy->is_pair = table->is_pair & ~mask;
  the_copy->size = old_size - 1;

  const size_t j =
    (size_t) _smcore_bitstring_maps__popcount_upto (table->is_stored, index);
  memcpy (the_copy->cells, table->cells, j * sizeof (atstype_ptr));
  the_copy->cells[j] = (atstype_ptr) 0;
  memcpy (&the_copy->cells[j], &table->cells[j + 1],
          ((old_size - 1) - j) * sizeof (atstype_ptr));

  return (atstype_ptr) the_copy;
}

/*--------------------------------------------------------------------*/

#endif /* SMCORE_BITSTRING_MAPS__CATS_ */
