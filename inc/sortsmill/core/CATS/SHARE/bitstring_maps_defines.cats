/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCORE_BITSTRING_MAPS_DEFINES__CATS_
#define SMCORE_BITSTRING_MAPS_DEFINES__CATS_

/*
 * _smcore_bitstring_maps__TABLE_SIZE_MAX must equal two raised to the
 * power _smcore_bitstring_maps__BITS_PER_LEVEL.
 *
 * The current implementation requires
 * 1<=_smcore_bitstring_maps__BITS_PER_LEVEL and
 * _smcore_bitstring_maps__BITS_PER_LEVEL<=6.
 *
 * The value of _smcore_bitstring_maps__BITMAP_TYPE must be an ATS
 * type that is an unsigned integer containing at least
 * _smcore_bitstring_maps__TABLE_SIZE_MAX bits. The value of
 * _smcore_bitstring_maps__BITMAP_C_TYPE must be an equivalent type in
 * C code.
 *
 * The _smcore_bitstring_maps__POPCOUNT must be a popcount function
 * that can take an argument of type
 * _smcore_bitstring_maps__BITMAP_TYPE.
 */

/*
 * NOTE: VERY informal testing on a 64-bit machine (dual 4-core
 * Opteron with popcount instructions, 8GB RAM) recommends 32-bit
 * bitmap fields over 64-bit fields.
 */

/* For 32-bit bitmap fields. */
#define _smcore_bitstring_maps__BITS_PER_LEVEL 5
#define _smcore_bitstring_maps__TABLE_SIZE_MAX 32
#define _smcore_bitstring_maps__BITMAP_TYPE uint32
#define _smcore_bitstring_maps__BITMAP_C_TYPE atstype_uint32
#define _smcore_bitstring_maps__POPCOUNT smcore_popcount32

/* For 64-bit bitmap fields. */
/*
#define _smcore_bitstring_maps__BITS_PER_LEVEL 6
#define _smcore_bitstring_maps__TABLE_SIZE_MAX 64
#define _smcore_bitstring_maps__BITMAP_TYPE uint64
#define _smcore_bitstring_maps__BITMAP_C_TYPE atstype_uint64
#define _smcore_bitstring_maps__POPCOUNT smcore_popcount64
*/

#endif /* SMCORE_BITSTRING_MAPS_DEFINES__CATS_ */
