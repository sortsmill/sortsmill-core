/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_INVERSION_H_
#define SORTSMILL_CORE_POLYNOMIAL_INVERSION_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

enum
{
  SMCORE__INVERSION_PARAMETER_IS_ARBITRARY = 1
};

void mpq_poly_invertdegree1_mono (ssize_t stride_x, mpq_t *poly_x,
                                  ssize_t stride_y, mpq_t *poly_y,
                                  const mpq_t x, const mpq_t y,
                                  mpq_t t, int *info);
void dpoly_invert_mono (size_t degree, ssize_t stride_x, mpq_t *poly_x,
                        ssize_t stride_y, mpq_t *poly_y,
                        const mpq_t x, const mpq_t y,
                        const mpq_t ta, const mpq_t tb,
                        double absolute_tolerance,
                        size_t *num_solutions, double t[degree], int *info);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_INVERSION_H_ */
