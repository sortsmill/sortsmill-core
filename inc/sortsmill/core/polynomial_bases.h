/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_BASES_H_
#define SORTSMILL_CORE_POLYNOMIAL_BASES_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void mpq_poly_mono_to_bern (size_t degree, ssize_t stride_a, mpq_t *a,
                            ssize_t stride_b, mpq_t *b);
void mpq_poly_bern_to_mono (size_t degree, ssize_t stride_a, mpq_t *a,
                            ssize_t stride_b, mpq_t *b);
void mpq_poly_mono_to_sbern (size_t degree, ssize_t stride_a, mpq_t *a,
                             ssize_t stride_b, mpq_t *b);
void mpq_poly_sbern_to_mono (size_t degree, ssize_t stride_a, mpq_t *a,
                             ssize_t stride_b, mpq_t *b);
void mpq_poly_bern_to_sbern (size_t degree, ssize_t stride_a, mpq_t *a,
                             ssize_t stride_b, mpq_t *b);
void mpq_poly_sbern_to_bern (size_t degree, ssize_t stride_a, mpq_t *a,
                             ssize_t stride_b, mpq_t *b);
void mpq_poly_sbern_to_spower (size_t degree, ssize_t stride_a, mpq_t *a,
                               ssize_t stride_b, mpq_t *b);
void mpq_poly_spower_to_sbern (size_t degree, ssize_t stride_a, mpq_t *a,
                               ssize_t stride_b, mpq_t *b);
void mpq_poly_bern_to_spower (size_t degree, ssize_t stride_a, mpq_t *a,
                              ssize_t stride_b, mpq_t *b);
void mpq_poly_spower_to_bern (size_t degree, ssize_t stride_a, mpq_t *a,
                              ssize_t stride_b, mpq_t *b);
void mpq_poly_mono_to_spower (size_t degree, ssize_t stride_a, mpq_t *a,
                              ssize_t stride_b, mpq_t *b);
void mpq_poly_spower_to_mono (size_t degree, ssize_t stride_a, mpq_t *a,
                              ssize_t stride_b, mpq_t *b);

void dpoly_mono_to_bern (size_t degree, ssize_t stride_a, const double *a,
                         ssize_t stride_b, double *b);
void dpoly_bern_to_mono (size_t degree, ssize_t stride_a, const double *a,
                         ssize_t stride_b, double *b);
void dpoly_mono_to_sbern (size_t degree, ssize_t stride_a, const double *a,
                          ssize_t stride_b, double *b);
void dpoly_sbern_to_mono (size_t degree, ssize_t stride_a, const double *a,
                          ssize_t stride_b, double *b);
void dpoly_bern_to_sbern (size_t degree, ssize_t stride_a, const double *a,
                          ssize_t stride_b, double *b);
void dpoly_sbern_to_bern (size_t degree, ssize_t stride_a, const double *a,
                          ssize_t stride_b, double *b);
void dpoly_sbern_to_spower (size_t degree, ssize_t stride_a, const double *a,
                            ssize_t stride_b, double *b);
void dpoly_spower_to_sbern (size_t degree, ssize_t stride_a, const double *a,
                            ssize_t stride_b, double *b);
void dpoly_bern_to_spower (size_t degree, ssize_t stride_a, const double *a,
                           ssize_t stride_b, double *b);
void dpoly_spower_to_bern (size_t degree, ssize_t stride_a, const double *a,
                           ssize_t stride_b, double *b);
void dpoly_mono_to_spower (size_t degree, ssize_t stride_a, const double *a,
                           ssize_t stride_b, double *b);
void dpoly_spower_to_mono (size_t degree, ssize_t stride_a, const double *a,
                           ssize_t stride_b, double *b);

void dpoly_unsplit_spower (size_t degree,
                           ssize_t stride0, const double *a0,
                           ssize_t stride1, const double *a1,
                           ssize_t result_stride, double *result);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_BASES_H_ */
