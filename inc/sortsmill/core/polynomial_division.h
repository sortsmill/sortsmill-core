/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_DIVISION_H_
#define SORTSMILL_CORE_POLYNOMIAL_DIVISION_H_

#include <stdlib.h>
#include <stdbool.h>
#include <gmp.h>
#include <sortsmill/core/polynomial_lists.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void mpq_poly_div_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                        size_t degree2, ssize_t stride2, mpq_t *poly2,
                        size_t *degree_q, ssize_t stride_q, mpq_t *quotient,
                        size_t *degree_r, ssize_t stride_r, mpq_t *remainder,
                        bool *division_by_zero);

/* WARNING. Remember, when using a polynomial gcd function: if one of
   the given polynomials is zero, the gcd will be of the _same degree_
   as the other given polynomial. It is _not true_ that the degree of
   the gcd must be less than or equal to that of the lower degree
   input polynomial. */
void mpq_poly_gcd_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                        size_t degree2, ssize_t stride2, mpq_t *poly2,
                        size_t *degree, ssize_t stride, mpq_t *gcd);

void mpq_poly_squarefree_mono (size_t degree, ssize_t stride, mpq_t *poly,
                               polynomial_list_entry_t **result,
                               mpq_t lead_coef);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_DIVISION_H_ */
