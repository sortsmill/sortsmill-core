// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//
// Static loading of most things at once.
//

staload "sortsmill/core/SATS/arith2_prf.sats"
staload _ = "sortsmill/core/DATS/arith2_prf.dats"

staload "sortsmill/core/SATS/array_misc.sats"
staload _ = "sortsmill/core/DATS/array_misc.dats"

staload "sortsmill/core/SATS/bitstring_maps.sats"
staload _ = "sortsmill/core/DATS/bitstring_maps.dats"

staload "sortsmill/core/SATS/hash_maps.sats"
staload "sortsmill/core/SATS/murmurhash3.sats"
staload _ = "sortsmill/core/DATS/hash_maps.dats"

staload "sortsmill/core/SATS/immutable_vectors.sats"
staload _ = "sortsmill/core/DATS/immutable_vectors.dats"

staload "sortsmill/core/SATS/istrings.sats"
staload _ = "sortsmill/core/DATS/istrings.dats"

staload "sortsmill/core/SATS/popcount_prf.sats"
staload _ = "sortsmill/core/DATS/popcount_prf.dats"
