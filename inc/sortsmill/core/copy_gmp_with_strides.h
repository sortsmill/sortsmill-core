/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_COPY_GMP_WITH_STRIDES_H_
#define SORTSMILL_CORE_COPY_GMP_WITH_STRIDES_H_

#include <sys/types.h>
#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void copy_mpz_with_strides (ssize_t dest_stride, mpz_t *dest,
                            ssize_t src_stride, mpz_t *src, size_t count);
void copy_mpq_with_strides (ssize_t dest_stride, mpq_t *dest,
                            ssize_t src_stride, mpq_t *src, size_t count);
void copy_mpf_with_strides (ssize_t dest_stride, mpf_t *dest,
                            ssize_t src_stride, mpf_t *src, size_t count);

void copy_negate_mpz_with_strides (ssize_t dest_stride, mpz_t *dest,
                                   ssize_t src_stride, mpz_t *src,
                                   size_t count);
void copy_negate_mpq_with_strides (ssize_t dest_stride, mpq_t *dest,
                                   ssize_t src_stride, mpq_t *src,
                                   size_t count);
void copy_negate_mpf_with_strides (ssize_t dest_stride, mpf_t *dest,
                                   ssize_t src_stride, mpf_t *src,
                                   size_t count);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_COPY_GMP_WITH_STRIDES_H_ */
