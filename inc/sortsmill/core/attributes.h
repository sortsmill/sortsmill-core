/*
 * Copyright (C) 2012, 2013 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_ATTRIBUTES_H_
#define SORTSMILL_CORE_ATTRIBUTES_H_

#define STM_GNUC_VERSION_AT_LEAST(MAJOR, MINOR)				\
  (defined __GNUC__ && defined __GNUC_MINOR__ &&			\
   (MAJOR < __GNUC__ || (MAJOR == __GNUC__ && MINOR <= __GNUC_MINOR__)))

#if !defined STM_ATTRIBUTE_PURE
#if STM_GNUC_VERSION_AT_LEAST (2, 96)
#define STM_ATTRIBUTE_PURE __attribute__ ((__pure__))
#else
#define STM_ATTRIBUTE_PURE      /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_CONST
#if STM_GNUC_VERSION_AT_LEAST (2, 5)
#define STM_ATTRIBUTE_CONST __attribute__ ((__const__))
#else
#define STM_ATTRIBUTE_CONST     /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_HOT
#if STM_GNUC_VERSION_AT_LEAST (4, 3)
#define STM_ATTRIBUTE_HOT __attribute__ ((__hot__))
#else
#define STM_ATTRIBUTE_HOT       /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_COLD
#if STM_GNUC_VERSION_AT_LEAST (4, 3)
#define STM_ATTRIBUTE_COLD __attribute__ ((__cold__))
#else
#define STM_ATTRIBUTE_COLD      /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_DEPRECATED
#if defined __GNUC__
#define STM_ATTRIBUTE_DEPRECATED __attribute__ ((__deprecated__))
#else
#define STM_ATTRIBUTE_DEPRECATED        /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_SENTINEL
#if defined __GNUC__
#define STM_ATTRIBUTE_SENTINEL __attribute__ ((__sentinel__))
#else
#define STM_ATTRIBUTE_SENTINEL  /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_WARN_UNUSED_RESULT
#if defined __GNUC__
#define STM_ATTRIBUTE_WARN_UNUSED_RESULT __attribute__ ((__warn_unused_result__))
#else
#define STM_ATTRIBUTE_WARN_UNUSED_RESULT        /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_NORETURN
#if STM_GNUC_VERSION_AT_LEAST (2, 5)
#define STM_ATTRIBUTE_NORETURN __attribute__ ((__noreturn__))
#else
#define STM_ATTRIBUTE_NORETURN  /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_NOINLINE
#if defined __GNUC__
#define STM_ATTRIBUTE_NOINLINE __attribute__ ((__noinline__))
#else
#define STM_ATTRIBUTE_NOINLINE  /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_ALWAYS_INLINE
#if defined __GNUC__
#define STM_ATTRIBUTE_ALWAYS_INLINE __attribute__ ((__always_inline__))
#else
#define STM_ATTRIBUTE_ALWAYS_INLINE     /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_FLATTEN
#if defined __GNUC__
#define STM_ATTRIBUTE_FLATTEN __attribute__ ((__flatten__))
#else
#define STM_ATTRIBUTE_FLATTEN   /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_MALLOC
#if defined __GNUC__
#define STM_ATTRIBUTE_MALLOC __attribute__ ((__malloc__))
#else
#define STM_ATTRIBUTE_MALLOC    /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_ALLOC_SIZE
#if STM_GNUC_VERSION_AT_LEAST (4, 3)
#define STM_ATTRIBUTE_ALLOC_SIZE(ARGS) __attribute__ ((__alloc_size__ ARGS))
#else
#define STM_ATTRIBUTE_ALLOC_SIZE(ARGS)  /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_FORMAT
#if defined __GNUC__
#define STM_ATTRIBUTE_FORMAT(STYLE, FORMAT, FIRST) \
  __attribute__ ((__format__ (STYLE, FORMAT, FIRST)))
#else
#define STM_ATTRIBUTE_FORMAT(STYLE, FORMAT, FIRST)      /* empty */
#endif
#endif

#if !defined STM_ATTRIBUTE_FORMAT_PRINTF
#define STM_ATTRIBUTE_FORMAT_PRINTF(FORMAT, FIRST) \
  STM_ATTRIBUTE_FORMAT(__printf__, FORMAT, FIRST)
#endif

#if !defined STM_ATTRIBUTE_FORMAT_SCANF
#define STM_ATTRIBUTE_FORMAT_SCANF(FORMAT, FIRST) \
  STM_ATTRIBUTE_FORMAT(__scanf__, FORMAT, FIRST)
#endif

#if !defined STM_ATTRIBUTE_FORMAT_STRFTIME
#define STM_ATTRIBUTE_FORMAT_STRFTIME(FORMAT, FIRST) \
  STM_ATTRIBUTE_FORMAT(__strftime__, FORMAT, FIRST)
#endif

#if !defined STM_ATTRIBUTE_FORMAT_STRFMON
#define STM_ATTRIBUTE_FORMAT_STRFMON(FORMAT, FIRST) \
  STM_ATTRIBUTE_FORMAT(__strfmon__, FORMAT, FIRST)
#endif

#if !defined STM_MAYBE_UNUSED
#if STM_GNUC_VERSION_AT_LEAST (2, 7)
#define STM_MAYBE_UNUSED __attribute__ ((__unused__))
#else
#define STM_MAYBE_UNUSED
#endif
#endif

#if !defined STM_UNUSED
#if STM_GNUC_VERSION_AT_LEAST (2, 7)
#define STM_UNUSED(x) x __attribute__ ((__unused__))
#elif defined __LCLINT__
#define STM_UNUSED(x) /*@unused@*/ x
#else
#define STM_UNUSED(x) x
#endif
#endif

#if STM_GNUC_VERSION_AT_LEAST (2, 0) || 0x5110 <= __SUNPRO_C
#if !defined STM_HAVE_TYPEOF
#define STM_HAVE_TYPEOF 1
#endif
#if !defined STM_TYPEOF
#define STM_TYPEOF(x) __typeof__ (x)
#endif
#if !defined STM_CAST_TYPEOF
#define STM_CAST_TYPEOF(x) (__typeof__ (x))
#endif
#else /* typeof not supported */
#if !defined STM_HAVE_TYPEOF
#define STM_HAVE_TYPEOF 0
#endif
#if !defined STM_TYPEOF
#define STM_TYPEOF(x)           /* empty */
#endif
#if !defined STM_CAST_TYPEOF
#define STM_CAST_TYPEOF(x)      /* empty */
#endif
#endif

#if !defined STM_AT_LEAST_C89
#if defined __STDC__ && __STDC__
#define STM_AT_LEAST_C89 1
#else
#define STM_AT_LEAST_C89 0
#endif
#endif

#if !defined STM_AT_LEAST_C90
#if defined __STDC__ && __STDC__
#define STM_AT_LEAST_C90 1
#else
#define STM_AT_LEAST_C90 0
#endif
#endif

#if !defined STM_AT_LEAST_C94
#if defined __STDC_VERSION__ && 199409L <= __STDC_VERSION__
#define STM_AT_LEAST_C94 1
#else
#define STM_AT_LEAST_C94 0
#endif
#endif

#if !defined STM_AT_LEAST_C95
#if defined __STDC_VERSION__ && 199409L <= __STDC_VERSION__
#define STM_AT_LEAST_C95 1
#else
#define STM_AT_LEAST_C95 0
#endif
#endif

#if !defined STM_AT_LEAST_C99
#if defined __STDC_VERSION__ && 199901L <= __STDC_VERSION__
#define STM_AT_LEAST_C99 1
#else
#define STM_AT_LEAST_C99 0
#endif
#endif

#if !defined STM_AT_LEAST_C11
#if defined __STDC_VERSION__ && 201112L <= __STDC_VERSION__
#define STM_AT_LEAST_C11 1
#else
#define STM_AT_LEAST_C11 0
#endif
#endif

/* FIXME: One could also put in tests for variadic macro support in
   C++. However, C++ users can override this test in their program
   configuration, so the need is not desperate. */
#if !defined STM_HAVE_VARIADIC_MACROS
#if STM_AT_LEAST_C99
#define STM_HAVE_VARIADIC_MACROS 1
#else
#define STM_HAVE_VARIADIC_MACROS 0
#endif
#endif

#endif /* SORTSMILL_CORE_ATTRIBUTES_H_ */
