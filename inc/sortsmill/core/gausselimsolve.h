/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_GAUSSELIMSOLVE_H_
#define SORTSMILL_CORE_GAUSSELIMSOLVE_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

// Solve a linear system TX=R by gaussian elimination with partial
// pivoting. The matrix T is m-by-m; the matrices X and R are
// m-by-(n-m).
//
// On input:
//
//    A is an array of pointers to the rows of the augmented matrix
//    [T,R].
//
// On successful output:
//
//    The R part of A=[T,R] is replaced by the solution X. The T part
//    should be considered destroyed. The pointers in A are permuted,
//    due to the partial pivoting.
//
//    If signum is not NULL, then the sign of the permutation is
//    stored in *signum.
//
//    If info is not NULL, then zero is stored in *info.
//
// On failure:
//
//    If info is not NULL, and no nonzero pivot could be found for the
//    ith row (0 <= i <= m-1), then i+1 is stored in *info.
//
void sgausselimsolve (int m, int n, float *A[m], int *signum, int *info);
void dgausselimsolve (int m, int n, double *A[m], int *signum, int *info);
void lgausselimsolve (int m, int n, long double *A[m], int *signum, int *info);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_GAUSSELIMSOLVE_H_ */
