/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_ADDITION_H_
#define SORTSMILL_CORE_POLYNOMIAL_ADDITION_H_

#include <stdlib.h>
#include <stdbool.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

bool mpq_poly_equalszero (size_t degree1, ssize_t stride1, mpq_t *poly1);
bool dpoly_equalszero (size_t degree1, ssize_t stride1, const double *poly1);

void mpq_poly_neg (size_t degree1, ssize_t stride1, mpq_t *poly1,
                   ssize_t result_stride, mpq_t *result);
void dpoly_neg (size_t degree1, ssize_t stride1, const double *poly1,
                ssize_t result_stride, double *result);

/* If two polynomials are of the same degree, then adding or
   subtracting them is done the same way, regardless of basis. */
void dpoly_add (size_t degree, ssize_t stride1, const double *poly1,
                ssize_t stride2, const double *poly2,
                ssize_t stride3, double *poly3);
void dpoly_sub (size_t degree, ssize_t stride1, const double *poly1,
                ssize_t stride2, const double *poly2,
                ssize_t stride3, double *poly3);

void mpq_poly_add_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                        size_t degree2, ssize_t stride2, mpq_t *poly2,
                        ssize_t result_stride, mpq_t *result);
void dpoly_add_mono (size_t degree1, ssize_t stride1, const double *poly1,
                     size_t degree2, ssize_t stride2, const double *poly2,
                     ssize_t result_stride, double *result);
void dpoly_add_bern (size_t degree1, ssize_t stride1, const double *poly1,
                     size_t degree2, ssize_t stride2, const double *poly2,
                     ssize_t result_stride, double *result);
void dpoly_add_sbern (size_t degree1, ssize_t stride1, const double *poly1,
                      size_t degree2, ssize_t stride2, const double *poly2,
                      ssize_t result_stride, double *result);
void dpoly_add_spower (size_t degree1, ssize_t stride1, const double *poly1,
                       size_t degree2, ssize_t stride2, const double *poly2,
                       ssize_t result_stride, double *result);

void mpq_poly_sub_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                        size_t degree2, ssize_t stride2, mpq_t *poly2,
                        ssize_t result_stride, mpq_t *result);
void dpoly_sub_mono (size_t degree1, ssize_t stride1, const double *poly1,
                     size_t degree2, ssize_t stride2, const double *poly2,
                     ssize_t result_stride, double *result);
void dpoly_sub_bern (size_t degree1, ssize_t stride1, const double *poly1,
                     size_t degree2, ssize_t stride2, const double *poly2,
                     ssize_t result_stride, double *result);
void dpoly_sub_sbern (size_t degree1, ssize_t stride1, const double *poly1,
                      size_t degree2, ssize_t stride2, const double *poly2,
                      ssize_t result_stride, double *result);
void dpoly_sub_spower (size_t degree1, ssize_t stride1, const double *poly1,
                       size_t degree2, ssize_t stride2, const double *poly2,
                       ssize_t result_stride, double *result);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_ADDITION_H_ */
