/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_ONE_TIME_INITIALIZATION_H_
#define SORTSMILL_CORE_ONE_TIME_INITIALIZATION_H_

#include <sortsmill/core/double_checked_locking.h>

/*
 * We use `double-checked locking'.
 * See http://www.hpl.hp.com/research/linux/atomic_ops/example.php4
 *
 * Values defined with this macro are `lazy'; that is, they are not
 * initialized until the first time they are used.
 */

#define STM_ONE_TIME_INITIALIZE(MODIFIERS, TYPE, NAME,                  \
                                THREAD_LOCK, THREAD_UNLOCK,             \
                                INITIALIZATION)                         \
                                                                        \
  static volatile stm_dcl_indicator_t __##NAME##__is_initialized =      \
    false;                                                              \
  static TYPE NAME##__Value;                                            \
                                                                        \
  MODIFIERS TYPE                                                        \
  NAME (void)                                                           \
  {                                                                     \
    if (!stm_dcl_load_indicator (&__##NAME##__is_initialized))          \
      {                                                                 \
        {                                                               \
          THREAD_LOCK;                                                  \
        };                                                              \
        if (!__##NAME##__is_initialized)                                \
          {                                                             \
            {                                                           \
              INITIALIZATION;                                           \
            }                                                           \
            stm_dcl_store_indicator (&__##NAME##__is_initialized,       \
                                     true);                             \
          }                                                             \
        {                                                               \
          THREAD_UNLOCK;                                                \
        }                                                               \
      }                                                                 \
    return NAME##__Value;                                               \
  }

#endif /* SORTSMILL_CORE_ONE_TIME_INITIALIZATION_H_ */
