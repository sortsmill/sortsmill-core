// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.hash_maps"
#define ATS_EXTERN_PREFIX "_smcore_hash_maps__"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload UN = "prelude/SATS/unsafe.sats"

staload "sortsmill/core/SATS/hash_maps.sats"
staload "sortsmill/core/SATS/bitstring_maps.sats"
staload _ = "sortsmill/core/DATS/bitstring_maps.dats"

//--------------------------------------------------------------------

local

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Use this macros instead of hash_map$keys_match()
  // to help prevent misordering of arguments.
  // (Fancier methods could be devised, using the type system,
  // but I’m not sure it is worth the trouble.)
  
  macdef KEYS_MATCH (key, tuple) =
    hash_map$keys_match (,(key), ,(tuple).0)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  #define BITS_PER_LEVEL _smcore_bitstring_maps__BITS_PER_LEVEL
  #define TABLE_SIZE_MAX _smcore_bitstring_maps__TABLE_SIZE_MAX

  typedef right_shifter (a : t@ype, i : t@ype) = (a, i) -<> a
  typedef mask_applier (a : t@ype) =
    {mask : nat} (a, int mask) -<> [i : nat | i <= mask] int i

  fn {t : t@ype}
  bitsizeof () : size_t = $extval (size_t, "CHAR_BIT") * sizeof<t>

  implement {key}
  hash_map_hash_to_table_index (key, level) =
    // Extract the levelth group of bits from a key.
    // Start with the least significant bits, to use
    // bits efficiently even if the greatest hash value
    // is less than what can be stored in the hash type.
    let
      val zero = i2sz 0
      val one = i2sz 1

      val bits_per_level = i2sz BITS_PER_LEVEL
      val bitsize = bitsizeof<key> ()

      val num_levels =
        (bitsize / bits_per_level) +
        (if bitsize mod bits_per_level = zero then zero else one)

      fn {}
      get_bits () :<cloref0> [i : nat | i < TABLE_SIZE_MAX] int i =
        let
          extern fn shift_right_key : right_shifter (key, size_t) =
            "mac#_smcore_bitstring_maps__shift_right"
          overload >> with shift_right_key

          extern fn apply_mask : mask_applier (key) =
              "mac#_smcore_bitstring_maps__land"

          val mask = pred TABLE_SIZE_MAX
          val j = bits_per_level * (g0ofg1 level)
        in
          (key >> j) \apply_mask mask
        end
    in
      if level = zero then
        get_bits ()
      else if level < num_levels then
        get_bits ()
      else
        ~1
    end

  implement bitstring_map$key_to_table_index<uint> = hash_map_hash_to_table_index<uint>
  implement bitstring_map$key_to_table_index<ulint> = hash_map_hash_to_table_index<ulint>
  implement bitstring_map$key_to_table_index<ullint> = hash_map_hash_to_table_index<ullint>
  implement bitstring_map$key_to_table_index<uint8> = hash_map_hash_to_table_index<uint8>
  implement bitstring_map$key_to_table_index<uint16> = hash_map_hash_to_table_index<uint16>
  implement bitstring_map$key_to_table_index<uint32> = hash_map_hash_to_table_index<uint32>
  implement bitstring_map$key_to_table_index<uint64> = hash_map_hash_to_table_index<uint64>
  implement bitstring_map$key_to_table_index<size_t> = hash_map_hash_to_table_index<size_t>
  implement bitstring_map$key_to_table_index<uintptr> = hash_map_hash_to_table_index<uintptr>

  implement bitstring_map$key_to_table_index<int> = hash_map_hash_to_table_index<int>
  implement bitstring_map$key_to_table_index<lint> = hash_map_hash_to_table_index<lint>
  implement bitstring_map$key_to_table_index<llint> = hash_map_hash_to_table_index<llint>
  implement bitstring_map$key_to_table_index<int8> = hash_map_hash_to_table_index<int8>
  implement bitstring_map$key_to_table_index<int16> = hash_map_hash_to_table_index<int16>
  implement bitstring_map$key_to_table_index<int32> = hash_map_hash_to_table_index<int32>
  implement bitstring_map$key_to_table_index<int64> = hash_map_hash_to_table_index<int64>
  implement bitstring_map$key_to_table_index<ssize_t> = hash_map_hash_to_table_index<ssize_t>
  implement bitstring_map$key_to_table_index<intptr> = hash_map_hash_to_table_index<intptr>

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  typedef chain_list (key : t@ype+, value : t@ype+, n : int) =
    list (@(key, value), n)
  typedef chain (key : t@ype+, value : t@ype+, n : int) =
    @(key, value, chain_list (key, value, n))
  typedef chain (key : t@ype+, value : t@ype+) =
    [n : nat] chain (key, value, n)
  typedef storage (key, hash, value : t@ype+) =
    bitstring_map (hash, chain (key, value))

  typedef map_struct (key : t@ype+, hash : t@ype+, value : t@ype+, size : int) =
    '(size_t size, storage (key, hash, value))
  typedef map_struct (size : int) =
    [key, hash, value : t@ype] map_struct (key, hash, value, size)

  fn {key, hash, value : t@ype}
  make_nonnil_hash_map {size : nat}
                       (size : size_t size,
                        storage : storage (key, hash, value)) :
      hash_map (key, hash, value, size) =
    $UN.cast{hash_map (key, hash, value, size)} '(size, storage)

  fn {key, hash, value : t@ype}
  nonnil_hash_map_size {size : nat}
                       (map : hash_map (key, hash, value, size)) :
      size_t size =
    ($UN.cast{map_struct (key, hash, value, size)} map).0

  fn {key, hash, value : t@ype} {size : int}
  nonnil_hash_map_storage (map : hash_map (key, hash, value, size)) :
      storage (key, hash, value) =
    ($UN.cast{map_struct (key, hash, value, size)} map).1

  overload .size with nonnil_hash_map_size
  overload .storage with nonnil_hash_map_storage

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // Interfaces for doing different things with values recovered
  // from the structure.
  extern fn {result : t@ype}
  hash_map$not_found_value : () -> result
  extern fn {result : t@ype} {value : t@ype} 
  hash_map$found_value : value -> result

  fn {result : t@ype} {key : t@ype; value : t@ype}
  find_in_chain (chn : chain (key, value), key : key) : result =
    let
      fun
      search_list {m : nat} .<m>.
                  (lst : chain_list (key, value, m), key : key) : result =
        case+ lst of
          | list_nil () => hash_map$not_found_value ()
          | list_cons (pair, tail) =>
            if KEYS_MATCH (key, pair) then
              hash_map$found_value (pair.1)
            else
              search_list (tail, key)

      val @(_, v, lst) = chn
    in
      if KEYS_MATCH (key, chn) then
        hash_map$found_value (v)
      else
        search_list (lst, key)
    end

  fn {result : t@ype} {key, hash, value : t@ype}
  find_in_hash_map {size : nat}
                   (map : hash_map (key, hash, value, size),
                    key : key) :
      result =
    let
      val is_nil = hash_map_is_nil map
    in
      if is_nil then
        hash_map$not_found_value ()
      else
        let
          val storage = map.storage ()
          val hash = hash_map$hash_function (key)
        in
          case+ bitstring_map_get<hash, chain (key, value)> (storage, hash) of
            | None () => hash_map$not_found_value ()
            | Some (chn) => find_in_chain (chn, key)
        end
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  fn {key : t@ype; value : t@ype}
  find_in_chain_eff (chn : chain (key, value),
                     key : key,
                     value : &value? >> opt (value, b)) :
      #[b : bool] bool b =
    let
      fun
      search_list {m : nat} .<m>.
                  (lst : chain_list (key, value, m),
                   key : key,
                   value : &value? >> opt (value, b)) :
          #[b : bool] bool b =
        case+ lst of
          | list_nil () =>
            let
              prval () = opt_none{value} (value)
            in
              false
            end
          | list_cons (pair, tail) =>
            if KEYS_MATCH (key, pair) then
              let
                val () = value := pair.1
                prval () = opt_some{value} (value)
              in
                true
              end
            else
              search_list (tail, key, value)

      val @(_, v, lst) = chn
    in
      if KEYS_MATCH (key, chn) then
        let
          val () = value := v
          prval () = opt_some{value} (value)
        in
          true
        end
      else
        search_list (lst, key, value)
    end

  fn {key, hash, value : t@ype}
  find_in_hash_map_eff {size : nat}
                       (map : hash_map (key, hash, value, size),
                        key : key,
                        value : &value? >> opt (value, b)) :
      #[b : bool] bool b =
    let
      val is_nil = hash_map_is_nil map
    in
      if is_nil then
        let
          prval () = opt_none{value} (value)
        in
          false
        end
      else
        let
          val storage = map.storage ()
          val hash = hash_map$hash_function (key)
        in
          case+ bitstring_map_get<hash, chain (key, value)> (storage, hash) of
            | None () =>
              let
                prval () = opt_none{value} (value)
              in
                false
              end
            | Some (chn) => find_in_chain_eff (chn, key, value)
        end
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  fn {key, value : t@ype}
  set_in_chain {orig_size : pos}
               (orig_size : size_t orig_size,
                chn : chain (key, value),
                key : key, value :value) :
      [new_size : int | new_size == orig_size || new_size == orig_size + 1]
      @(size_t new_size, chain (key, value)) =
    let
      fun loop {m, n : nat} .<m>.
               (lst0 : chain_list (key, value, m),
                lst1 : chain_list (key, value, n)) :<cloref1>
          [new_size : int | new_size == orig_size ||
                            new_size == orig_size + 1]
          @(size_t new_size, chain (key, value)) =
        case+ lst0 of
          | list_nil () =>
            let
              val @(k, v, lst) = chn
              val lst = list_cons (@(k, v), lst)
            in
              @(succ orig_size, @(key, value, lst))
            end
          | list_cons (pair, tail) =>
            if KEYS_MATCH (key, pair) then
              let
                val lst = list_reverse_append (lst1, tail)
                val @(k, v, _) = chn
                val lst = list_cons (@(k, v), lst)
              in
                @(orig_size, @(key, value, lst))
              end
            else
              loop (tail, list_cons (pair, lst1))
    in
      case+ chn of
        | @(_, _, lst) =>
          if KEYS_MATCH (key, chn) then
            @(orig_size, @(key, value, lst))
          else
            loop (lst, list_nil)
    end

  fn {key, hash, value : t@ype}
  set_in_hash_map {orig_size : nat}
                  (map : hash_map (key, hash, value, orig_size),
                   key : key, value : value) :
      [new_size : int | (orig_size == 0 && new_size == 1) ||
                        (0 < orig_size && new_size == orig_size) ||
                        (0 < orig_size && new_size == orig_size + 1)]
      hash_map (key, hash, value, new_size) =
    let
      typedef Chain = chain (key, value)
      val orig_size = hash_map_size map
    in
      if orig_size = i2sz 0 then
        let
          val hash = hash_map$hash_function key
          val chn = @(key, value, list_nil)
          val storage =
            bitstring_map_set<hash, Chain> (bitstring_map_nil, hash, chn)
        in
          make_nonnil_hash_map (i2sz 1, storage)
        end
      else
        let
          val storage = map.storage ()
          val hash = hash_map$hash_function (key)
          var chn : Chain?
          val found = bitstring_map_get_eff<hash, Chain> (storage, hash, chn)
        in
          if found then
            let
              val chn = opt_unsome_get chn
              val @(new_size, chn) =
                set_in_chain<key, value> (orig_size, chn, key, value)
              val storage = bitstring_map_set<hash, Chain> (storage, hash, chn)
            in
              make_nonnil_hash_map (new_size, storage)
            end
          else
            let
              prval () = opt_unnone chn
              val chn = @(key, value, list_nil)
              val storage = bitstring_map_set<hash, Chain> (storage, hash, chn)
            in
              make_nonnil_hash_map (succ orig_size, storage)
            end
        end
    end

  fn {key, value : t@ype}
  remove_from_chain {orig_size : pos}
                    (orig_size : size_t orig_size,
                     chn : chain (key, value),
                     key : key) :
      [new_size : int | new_size == orig_size ||
                        new_size == orig_size - 1]
      @(size_t new_size, bool, chain (key, value)) =
    let
      typedef Chain = chain (key, value)

      fun {}
      loop {m, n : nat} .<m>.
           (lst0 : chain_list (key, value, m),
            lst1 : chain_list (key, value, n),
            key : key) :
          [b : bool; k : nat | b == false || k == m + n - 1]
          (bool b, chain_list (key, value, k)) =
        case+ lst0 of
          | list_nil () =>
            // The key was not found.
            @(false, list_nil)
          | list_cons (pair, tail) =>
            if KEYS_MATCH (key, pair) then
              // The key was found. Return the list with the
              // corresponding entry removed.
              @(true, list_reverse_append (lst1, tail))
            else
              loop (tail, list_cons (pair, lst1), key)
    in
      if KEYS_MATCH (key, chn) then
        // The key was found immediately, without need to search
        // the linked list.
        case+ chn.2 of
          | list_nil () =>
            // The chain will be empty after removal of the entry.
            let
              val arbitrary_chain = chn
            in
              @(pred orig_size, true, arbitrary_chain)
            end
          | list_cons (@(k, v), lst) =>
            // Replace the immediate entry with the head of the
            // linked list.
            @(pred orig_size, false, @(k, v, lst))
      else
        let
          val @(key_was_found, lst) = loop (chn.2, list_nil, key)
        in
          if key_was_found then
            // Return the chain with the matching entry removed.
            @(pred orig_size, false, @(chn.0, chn.1, lst))
          else
            // The key was not found, anywhere.
            let
              val arbitrary_chain_is_empty = false
              val arbitrary_chain = chn
            in
              @(orig_size, arbitrary_chain_is_empty, arbitrary_chain)
            end
        end
    end

  fn {key, hash, value : t@ype}
  remove_from_hash_map {orig_size : nat}
                       (map : hash_map (key, hash, value, orig_size),
                        key : key) :
      [new_size : int | (orig_size == 0 && new_size == 0) ||
                        (0 < orig_size && new_size == orig_size) ||
                        (0 < orig_size && new_size == orig_size - 1)]
      hash_map (key, hash, value, new_size) =
    let
      typedef Chain = chain (key, value)
      val orig_size = hash_map_size map
    in
      if orig_size = i2sz 0 then
        map                 (* The map is nil. Return it unchanged. *)
      else
        let
          val storage = map.storage ()
          val hash = hash_map$hash_function (key)
          var chn : Chain?
          val found = bitstring_map_get_eff<hash, Chain> (storage, hash, chn)
        in
          if found then
            let
              val chn = opt_unsome_get chn
              val @(new_size, new_chain_is_empty, new_chain) =
                remove_from_chain<key, value> (orig_size, chn, key)
            in
              if new_size = orig_size then
                map             (* Return the original map. *)
              else if new_size = i2sz 0 then
                hash_map_nil
              else if new_chain_is_empty then
                let
                  val storage =
                    bitstring_map_remove<hash, Chain> (storage, hash)
                in
                  make_nonnil_hash_map (new_size, storage)
                end
              else
                let
                  val storage =
                    bitstring_map_set<hash, Chain> (storage, hash, new_chain)
                in
                  make_nonnil_hash_map (new_size, storage)
                end
            end
          else
            let
              prval () = opt_unnone chn
            in
              map               (* Return the original map. *)
            end
        end
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  typedef cursor (key : t@ype+, hash : t@ype+, value : t@ype+, n : int) =
    [1 <= n] '(bitstring_map_cursor (hash, chain (key, value)),
               list (@(key, value), n))
  typedef cursor (key : t@ype+, hash : t@ype+, value : t@ype+) =
    [n : pos] cursor (key, hash, value, n)

  fn {key, hash, value : t@ype}
  find_first_entry (map : hash_map (key, hash, value)) :
      hash_map_cursor (key, hash, value) =
    let
      val map_is_nil = hash_map_is_nil map
    in
      if map_is_nil then
        hash_map_cursor_nil
      else
        let
          val storage = map.storage ()
          val subcursor =
            bitstring_map_first<hash, chain (key, value)> storage
          (* FIXME: Can we get rid of the following assertloc
             via better constraints? *)
          val () = assertloc (not (bitstring_map_cursor_is_nil subcursor))
          val @(k, v, lst) =
            bitstring_map_value<hash, chain (key, value)> subcursor
          val lst = list_cons (@(k, v), lst)
          (* Ensure it is a legal cursor. *)
          val cursor : cursor (key, hash, value) = '(subcursor, lst)
        in
          $UN.cast{hash_map_cursor (key, hash, value)} cursor
        end
    end

  fn {key, hash, value : t@ype}
  move_to_next_entry (cursor : hash_map_cursor (key, hash, value)) :
      hash_map_cursor (key, hash, value) =
    let
      val cursor_is_nil = hash_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        hash_map_cursor_nil
      else
        let
          val '(subcursor, lst) = $UN.cast{cursor (key, hash, value)} cursor
          val list_cons (_, tail) = lst
        in
          case+ tail of
            | list_nil () =>
              let
                val subcursor =
                  bitstring_map_next<hash, chain (key, value)> subcursor
                val subcursor_is_nil = bitstring_map_cursor_is_nil subcursor
              in
                if subcursor_is_nil then
                  hash_map_cursor_nil
                else
                  let
                    val @(k, v, lst) =
                      bitstring_map_value<hash, chain (key, value)> subcursor
                    val lst = list_cons (@(k, v), lst)
                    (* Ensure it is a legal cursor. *)
                    val cursor : cursor (key, hash, value) = '(subcursor, lst)
                  in
                    $UN.cast{hash_map_cursor (key, hash, value)} cursor
                  end
              end
            | list_cons (_, _) =>
              let
                (* Ensure it is a legal cursor. *)
                val cursor : cursor (key, hash, value) = '(subcursor, tail)
              in
                $UN.cast{hash_map_cursor (key, hash, value)} cursor
              end
        end
    end

  fn {key, hash, value : t@ype}
  get_pair {addr : addr | addr != null}
           (cursor : hash_map_cursor (key, hash, value, addr)) :
      @(key, value) =
    let
      val '(_, list_cons (pair, _)) =
        $UN.cast{cursor (key, hash, value)} cursor
    in
      pair
    end

  fn {key, hash, value : t@ype}
  get_key {addr : addr | addr != null}
          (cursor : hash_map_cursor (key, hash, value, addr)) : key =
    let
      val '(_, list_cons (@(key, _), _)) =
        $UN.cast{cursor (key, hash, value)} cursor
    in
      key
    end

  fn {key, hash, value : t@ype}
  get_value {addr : addr | addr != null}
            (cursor : hash_map_cursor (key, hash, value, addr)) : value =
    let
      val '(_, list_cons (@(_, value), _)) =
        $UN.cast{cursor (key, hash, value)} cursor
    in
      value
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

in

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  primplement
  lemma_hash_map_param map =
    case+ map of
      | hash_map_nil () => ()
      | hash_map_nonnil _ => ()

  implement {}
  hash_map_is_nil map =
    case+ map of
      | hash_map_nil () => true
      | hash_map_nonnil _ => false

  implement {}
  hash_map_size map =
    case+ map of
      | hash_map_nil () => i2sz 0
      | hash_map_nonnil _ => map.size ()

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Functions for searching the map.

  implement {key, hash, value}
  hash_map_has_key (map, key) =
    let
      implement
      hash_map$not_found_value<bool> () : bool = false
      implement
      hash_map$found_value<bool><value> (_ : value) : bool = true
    in
      find_in_hash_map<bool><key, hash, value> (map, key)
    end

  implement {key, hash, value}
  hash_map_get (map, key) =
    let
      implement
      hash_map$not_found_value<Option value> () : Option value =
        None ()
      implement
      hash_map$found_value<Option value><value> (v : value) :
          Option value =
        Some v
    in
      find_in_hash_map<Option value><key, hash, value> (map, key)
    end

  implement {key, hash, value}
  hash_map_get_exn (map, key) =
    let
      implement
      hash_map$not_found_value<value> () : value =
        $raise HashMapSubscriptExn ()
      implement
      hash_map$found_value<value><value> (v : value) : value = v
    in
      find_in_hash_map<value><key, hash, value> (map, key)
    end

  implement {key, hash, value}
  hash_map_get_eff (map, key, value) =
    find_in_hash_map_eff<key, hash, value> (map, key, value)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement {key, hash, value}
  hash_map_set (map, key, value) =
    set_in_hash_map<key, hash, value> (map, key, value)

  implement {key, hash, value}
  hash_map_remove (map, key) =
    remove_from_hash_map<key, hash, value> (map, key)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Cursor operations: walking through the tree.
  //

  primplement
  lemma_hash_map_cursor_param cursor =
    case+ cursor of
      | hash_map_cursor_nil () => ()
      | hash_map_cursor_nonnil _ => ()

  implement {}
  hash_map_cursor_is_nil cursor =
    case+ cursor of
      | hash_map_cursor_nil () => true
      | hash_map_cursor_nonnil _ => false

  implement {key, hash, value}
  hash_map_first map = find_first_entry map
  implement {key, hash, value}
  hash_map_next cursor = move_to_next_entry cursor
  
  implement {key, hash, value}
  hash_map_pair cursor = get_pair cursor
  implement {key, hash, value}
  hash_map_pair_exn cursor =
    let
      val cursor_is_nil = hash_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        $raise HashMapCursorNilExn ()
      else
        get_pair cursor
    end

  implement {key, hash, value}
  hash_map_pair_eff (cursor, key, value) =
    let
      val @(k, v) = hash_map_pair<key, hash, value> cursor
    in
      key := k;
      value := v
    end
  implement {key, hash, value}
  hash_map_pair_effexn (cursor, key, value) =
    let
      val @(k, v) = hash_map_pair_exn<key, hash, value> cursor
    in
      key := k;
      value := v
    end

  implement {key, hash, value}
  hash_map_key cursor = get_key cursor
  implement {key, hash, value}
  hash_map_key_exn cursor =
    let
      val cursor_is_nil = hash_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        $raise HashMapCursorNilExn ()
      else
        get_key cursor
    end

  implement {key, hash, value}
  hash_map_value cursor = get_value cursor
  implement {key, hash, value}
  hash_map_value_exn cursor =
    let
      val cursor_is_nil = hash_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        $raise HashMapCursorNilExn ()
      else
        get_value cursor
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

end

//--------------------------------------------------------------------
//
// Modules.
//

// The internals of a modular_hash_map.
typedef mhashmap (key : t@ype+, hash : t@ype+, value : t@ype+, size : int) =
  '{
    hash_map = hash_map (key, hash, value, size),
    hashf = key -> hash,
    equalf = (key, key) -> bool
  }

extern castfn
mhashmap_of_modular_hash_map :
  {key, hash, value : t@ype} {size : int}
  modular_hash_map (key, hash, value, size) -<> mhashmap (key, hash, value, size)

extern castfn
modular_hash_map_of_mhashmap :
  {key, hash, value : t@ype} {size : int}
  mhashmap (key, hash, value, size) -<> modular_hash_map (key, hash, value, size)

implement {key, hash, value}
make_hash_map_module () =
  let
    macdef internal (x) = mhashmap_of_modular_hash_map ,(x)
    macdef external (x) = modular_hash_map_of_mhashmap ,(x)

    typedef map_t (size : int) = modular_hash_map (key, hash, value, size)
    typedef map_t = modular_hash_map (key, hash, value)
    typedef hash_map_t (size : int) = hash_map (key, hash, value, size)
    typedef hash_map_t = hash_map (key, hash, value)
    typedef cursor_t (addr : addr) = hash_map_cursor (key, hash, value, addr)
    typedef cursor_t = hash_map_cursor (key, hash, value)
    
    //
    // Instance introspection.
    //
    
    fn get_hash_map {size : int} (map : map_t size) : hash_map_t size =
      (internal map).hash_map

    fn get_hashf (map : map_t) : key -> hash =
      (internal map).hashf

    fn get_equalf (map : map_t) : (key, key) -> bool =
      (internal map).equalf

    //
    // Operations not involving keys.
    //

    fn make (hashf : key -> hash, equalf : (key, key) -> bool) : map_t 0 =
      external
        '{
          hash_map = hash_map_nil,
          hashf = hashf,
          equalf = equalf
        }

    fn is_nil {size : int} (map : map_t size) :
        [b : bool | (size == 0 && b == true) || (0 < size && b == false)]
        bool b =
      hash_map_is_nil (internal map).hash_map

    fn size {size : int} (map : map_t size) : size_t size =
      hash_map_size (internal map).hash_map

    //
    // Operations involving keys.
    //

    fn has_key (map : map_t, key : key) : bool =
      let
        implement hash_map$hash_function<key, hash> key =
          (internal map).hashf key
        implement hash_map$keys_match<key> (key, stored_key) =
          (internal map).equalf (key, stored_key)
      in
        hash_map_has_key<key, hash, value> ((internal map).hash_map, key)
      end

    fn get (map : map_t, key : key) : Option value =
      let
        implement hash_map$hash_function<key, hash> key =
          (internal map).hashf key
        implement hash_map$keys_match<key> (key, stored_key) =
          (internal map).equalf (key, stored_key)
      in
        hash_map_get<key, hash, value> ((internal map).hash_map, key)
      end

    fn get_exn (map : map_t, key : key) : value =
      let
        implement hash_map$hash_function<key, hash> key =
          (internal map).hashf key
        implement hash_map$keys_match<key> (key, stored_key) =
          (internal map).equalf (key, stored_key)
      in
        hash_map_get_exn<key, hash, value> ((internal map).hash_map, key)
      end

    fn get_eff (map : map_t, key : key,
                 value : &value? >> opt (value, b)) :
        #[b : bool] bool b =
      let
        implement hash_map$hash_function<key, hash> key =
          (internal map).hashf key
        implement hash_map$keys_match<key> (key, stored_key) =
          (internal map).equalf (key, stored_key)
      in
        hash_map_get_eff<key, hash, value> ((internal map).hash_map, key, value)
      end

    fn set {orig_size : nat}
           (map : map_t orig_size, key : key, value : value) :
        [new_size : int | (orig_size == 0 && new_size == 1) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size + 1)]
        map_t new_size =
      let
        implement hash_map$hash_function<key, hash> key =
          (internal map).hashf key
        implement hash_map$keys_match<key> (key, stored_key) =
          (internal map).equalf (key, stored_key)
        val new_map =
          hash_map_set<key, hash, value> ((internal map).hash_map, key, value)
      in
        external
          '{
            hash_map = new_map,
            hashf = (internal map).hashf,
            equalf = (internal map).equalf
          }
      end

    fn remove {orig_size : nat}
              (map : map_t orig_size, key : key) :
        [new_size : int | (orig_size == 0 && new_size == 0) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size - 1)]
        map_t new_size =
      let
        implement hash_map$hash_function<key, hash> key =
          (internal map).hashf key
        implement hash_map$keys_match<key> (key, stored_key) =
          (internal map).equalf (key, stored_key)
        val new_map = hash_map_remove<key, hash, value> ((internal map).hash_map, key)
      in
        external
          '{
            hash_map = new_map,
            hashf = (internal map).hashf,
            equalf = (internal map).equalf
          }
      end

    //
    // Cursors.
    //

    fn cursor_is_nil {addr : addr} (cursor : cursor_t addr) :<>
        [b : bool | (addr == null && b == true) || (addr != null && b == false)]
        bool b =
      hash_map_cursor_is_nil<> cursor

    fn first (map : map_t) : cursor_t =
      hash_map_first<key, hash, value> (internal map).hash_map

    fn next (cursor : cursor_t) : cursor_t =
      hash_map_next<key, hash, value> cursor

    fn get_pair {addr : addr | addr != null}
                (cursor : cursor_t addr) : @(key, value) =
      hash_map_pair<key, hash, value> cursor

    fn get_pair_exn (cursor : cursor_t) : @(key, value) =
      hash_map_pair_exn<key, hash, value> cursor

    fn get_pair_eff {addr : addr | addr != null}
                (cursor : cursor_t addr,
                 key : &key? >> key,
                 value : &value? >> value) : void =
      hash_map_pair_eff<key, hash, value> (cursor, key, value)

    fn get_pair_effexn (cursor : cursor_t,
                        key : &key? >> key,
                        value : &value? >> value) : void =
      hash_map_pair_effexn<key, hash, value> (cursor, key, value)

    fn get_key {addr : addr | addr != null}
               (cursor : cursor_t addr) : key =
      hash_map_key<key, hash, value> cursor

    fn get_key_exn (cursor : cursor_t) : key =
      hash_map_key_exn<key, hash, value> cursor

    fn get_value {addr : addr | addr != null}
                 (cursor : cursor_t addr) : value =
      hash_map_value<key, hash, value> cursor

    fn get_value_exn (cursor : cursor_t) : value =
      hash_map_value_exn<key, hash, value> cursor
  in
    '{
      hash_map = get_hash_map,
      hashf = get_hashf,
      equalf = get_equalf,
      make = make,
      is_nil = is_nil,
      size = size,
      has_key = has_key,
      get = get,
      get_exn = get_exn,
      get_eff = get_eff,
      set = set,
      remove = remove,
      cursor_is_nil = cursor_is_nil,
      first = first,
      next = next,
      pair = get_pair,
      pair_exn = get_pair_exn,
      pair_eff = get_pair_eff,
      pair_effexn = get_pair_effexn,
      key = get_key,
      key_exn = get_key_exn,
      value = get_value,
      value_exn = get_value_exn
    }
  end

//--------------------------------------------------------------------
