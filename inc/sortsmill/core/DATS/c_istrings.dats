// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Strings for C based on "sortsmill/core/SATS/immutable_vectors.sats".
//

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.istrings"
#define ATS_EXTERN_PREFIX "_smcore_istrings__"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload "sortsmill/core/SATS/immutable_vectors.sats"
staload _ = "sortsmill/core/DATS/immutable_vectors.dats"

staload "sortsmill/core/SATS/istrings.sats"
staload _ = "sortsmill/core/DATS/istrings.dats"

staload "sortsmill/core/SATS/unicode_helpers.sats"
staload _ = "sortsmill/core/DATS/unicode_helpers.dats"

%{^
#include <stdlib.h>
#include <stdarg.h>
#include <sortsmill/core/istrings.h>
%}

extern fn c2i (c : char) : [i : nat | i <= 0xFF] int i = "mac#%"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

extern fn _istring_length__ : istring -> size_t = "sta#"
implement _istring_length__ s = istring_length s
%{$
size_t
istring_length (istring_t s)
{
  return _istring_length__ ((atstype_ptr) s);
}
%}

extern fn _str2istring__ : string -> istring = "sta#"
implement _str2istring__ str = string2istring (g1ofg0 str)
%{$
istring_t
str2istring (const char *s)
{
  return _str2istring__ ((atstype_string) s);
}
%}

extern fn _str2istring2__ : (string, string) -> istring = "sta#"
implement _str2istring2__ (str0, str1) = string2istring str0 + str1
%{$
istring_t
str2istring2 (const char *s0, const char *s1)
{
  return _str2istring2__ ((atstype_string) s0, (atstype_string) s1);
}
%}

extern fn _str2istring3__ : (string, string, string) -> istring = "sta#"
implement _str2istring3__ (str0, str1, str2) =
  string2istring str0 + str1 + str2
%{$
istring_t
str2istring3 (const char *s0, const char *s1, const char *s2)
{
  return _str2istring3__ ((atstype_string) s0, (atstype_string) s1,
                          (atstype_string) s2);
}
%}

extern fn _str2istring4__ : (string, string, string, string) -> istring = "sta#"
implement _str2istring4__ (str0, str1, str2, str3) =
  string2istring str0 + str1 + str2 + str3
%{$
istring_t
str2istring4 (const char *s0, const char *s1, const char *s2, const char *s3)
{
  return _str2istring4__ ((atstype_string) s0, (atstype_string) s1,
                          (atstype_string) s2, (atstype_string) s3);
}
%}

extern fn _str2istring5__ :
  (string, string, string, string, string) -> istring = "sta#"
implement _str2istring5__ (str0, str1, str2, str3, str4) =
  string2istring str0 + str1 + str2 + str3 + str4
%{$
istring_t
str2istring5 (const char *s0, const char *s1, const char *s2, const char *s3,
              const char *s4)
{
  return _str2istring5__ ((atstype_string) s0, (atstype_string) s1,
                          (atstype_string) s2, (atstype_string) s3,
                          (atstype_string) s4);
}
%}

%{$
istring_t
str2istringn (const char *s0, ...)
{
  istring_t s;
  if (s0 == NULL)
    s = NULL;
  else
    {
      va_list ap;
      const char *s1;
      va_start (ap, s0);
      s = str2istring (s0);
      s1 = va_arg (ap, const char *);
      while (s1 != NULL)
        {
          s = istring_str_append (s, s1);
          s1 = va_arg (ap, const char *);
        }
      va_end (ap);
    }
  return s;
}
%}

extern fn _istring2str__ : istring -> string = "sta#"
implement _istring2str__ s = istring2string s
%{$
char *
istring2str (istring_t s)
{
  return _istring2str__ ((atstype_ptr) s);
}
%}

extern fn _strslice2istring__ : (string, size_t, size_t) -> istring = "sta#"
implement _strslice2istring__ (str, i_start, i_end) =
  string2istring_exn (g1ofg0 str, g1ofg0 i_start, g1ofg0 i_end)
%{$
istring_t
strslice2istring (const char *s, size_t i_start, size_t i_end)
{
  return _strslice2istring__ ((atstype_ptr) s, i_start, i_end);
}
%}

extern fn _istringslice2str__ : (istring, size_t, size_t) -> string = "sta#"
implement _istringslice2str__ (s, i_start, i_end) =
  istring2string_exn (s, g1ofg0 i_start, g1ofg0 i_end)
%{$
char *
istringslice2str (istring_t s, size_t i_start, size_t i_end)
{
  return _istringslice2str__ ((atstype_ptr) s, i_start, i_end);
}
%}

extern fn _istring_slice__ : (istring, size_t, size_t) -> istring = "sta#"
implement _istring_slice__ (s, i_start, i_end) =
  istring_slice_exn (s, g1ofg0 i_start, g1ofg0 i_end)
%{$
istring_t
istring_slice (istring_t s, size_t i_start, size_t i_end)
{
  return _istring_slice__ (s, i_start, i_end);
}
%}

extern fn _istring_str_append__ : (istring, string) -> istring = "sta#"
implement _istring_str_append__ (s, str1) = s + str1
%{$
istring_t
istring_str_append (istring_t s, const char *s1)
{
  return _istring_str_append__ ((atstype_ptr) s, (atstype_string) s1);
}
%}

extern fn _istring_str_append2__ : (istring, string, string) -> istring = "sta#"
implement _istring_str_append2__ (s, str1, str2) = s + str1 + str2
%{$
istring_t
istring_str_append2 (istring_t s, const char *s1, const char *s2)
{
  return _istring_str_append2__ ((atstype_ptr) s, (atstype_string) s1,
                                 (atstype_string) s2);
}
%}

extern fn _istring_str_append3__ :
  (istring, string, string, string) -> istring = "sta#"
implement _istring_str_append3__ (s, str1, str2, str3) =
  s + str1 + str2 + str3
%{$
istring_t
istring_str_append3 (istring_t s, const char *s1, const char *s2,
                     const char *s3)
{
  return _istring_str_append3__ ((atstype_ptr) s, (atstype_string) s1,
                                 (atstype_string) s2, (atstype_string) s3);
}
%}

extern fn _istring_str_append4__ :
  (istring, string, string, string, string) -> istring = "sta#"
implement _istring_str_append4__ (s, str1, str2, str3, str4) =
  s + str1 + str2 + str3 + str4
%{$
istring_t
istring_str_append4 (istring_t s, const char *s1, const char *s2,
                     const char *s3, const char *s4)
{
  return _istring_str_append4__ ((atstype_ptr) s, (atstype_string) s1,
                                 (atstype_string) s2, (atstype_string) s3,
                                 (atstype_string) s4);
}
%}

extern fn _istring_str_append5__ :
  (istring, string, string, string, string, string) -> istring = "sta#"
implement _istring_str_append5__ (s, str1, str2, str3, str4, str5) =
  s + str1 + str2 + str3 + str4 + str5
%{$
istring_t
istring_str_append5 (istring_t s, const char *s1, const char *s2,
                     const char *s3, const char *s4, const char *s5)
{
  return _istring_str_append5__ ((atstype_ptr) s, (atstype_string) s1,
                                 (atstype_string) s2, (atstype_string) s3,
                                 (atstype_string) s4, (atstype_string) s5);
}
%}

%{$
istring_t
istring_str_appendn (istring_t s, ...)
{
  va_list ap;
  const char *s1;
  va_start (ap, s);
  s1 = va_arg (ap, const char *);
  while (s1 != NULL)
    {
      s = istring_str_append (s, s1);
      s1 = va_arg (ap, const char *);
    }
  va_end (ap);
  return s;
}
%}

extern fn _istring_concat__ : (istring, istring) -> istring = "sta#"
implement _istring_concat__ (s0, s1) = s0 + s1
%{$
istring_t
istring_concat (istring_t s0, istring_t s1)
{
  return _istring_concat__ ((atstype_ptr) s0, (atstype_ptr) s1);
}
%}

extern fn _istring_concat3__ : (istring, istring, istring) -> istring = "sta#"
implement _istring_concat3__ (s0, s1, s2) = s0 + s1 + s2
%{$
istring_t
istring_concat3 (istring_t s0, istring_t s1, istring_t s2)
{
  return _istring_concat3__ ((atstype_ptr) s0, (atstype_ptr) s1,
                             (atstype_ptr) s2);
}
%}

extern fn _istring_concat4__ :
  (istring, istring, istring, istring) -> istring = "sta#"
implement _istring_concat4__ (s0, s1, s2, s3) = s0 + s1 + s2 + s3
%{$
istring_t
istring_concat4 (istring_t s0, istring_t s1, istring_t s2, istring_t s3)
{
  return _istring_concat4__ ((atstype_ptr) s0, (atstype_ptr) s1,
                             (atstype_ptr) s2, (atstype_ptr) s3);
}
%}

extern fn _istring_concat5__ :
  (istring, istring, istring, istring, istring) -> istring = "sta#"
implement _istring_concat5__ (s0, s1, s2, s3, s4) = s0 + s1 + s2 + s3 + s4
%{$
istring_t
istring_concat5 (istring_t s0, istring_t s1, istring_t s2, istring_t s3,
                 istring_t s4)
{
  return _istring_concat5__ ((atstype_ptr) s0, (atstype_ptr) s1,
                             (atstype_ptr) s2, (atstype_ptr) s3,
                             (atstype_ptr) s4);
}
%}

%{$
istring_t
istring_concatn (size_t n, ...)
{
  va_list ap;
  istring_t s = NULL;
  istring_t s0;
  va_start (ap, n);
  for (size_t i = 0; i < n; i++)
    {
      s0 = va_arg (ap, istring_t);
      s = istring_concat (s, s0);
    }
  va_end (ap);
  return s;
}
%}

extern fn _istring_cmp__ : (istring, istring) -> int = "sta#"
implement _istring_cmp__ (s, t) = istring_cmp (s, t)
%{$
int
istring_cmp (istring_t s, istring_t t)
{
  return _istring_cmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _istring_str_cmp__ : (istring, string) -> int = "sta#"
implement _istring_str_cmp__ (s, t) = istring_cmp (s, t)
%{$
int
istring_str_cmp (istring_t s, const char *t)
{
  return _istring_str_cmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _str_istring_cmp__ : (string, istring) -> int = "sta#"
implement _str_istring_cmp__ (s, t) = istring_cmp (s, t)
%{$
int
str_istring_cmp (const char *s, istring_t t)
{
  return _str_istring_cmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _istring_casecmp__ : (istring, istring) -> int = "sta#"
implement _istring_casecmp__ (s, t) =
  let
    implement istring$strncmp<> = strncasecmp_for_istrings
  in
    istring_cmp (s, t)
  end
%{$
int
istring_casecmp (istring_t s, istring_t t)
{
  return _istring_casecmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _istring_str_casecmp__ : (istring, string) -> int = "sta#"
implement _istring_str_casecmp__ (s, t) =
  let
    implement istring$strncmp<> = strncasecmp_for_istrings
  in
    istring_cmp (s, t)
  end
%{$
int
istring_str_casecmp (istring_t s, const char *t)
{
  return _istring_str_casecmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _str_istring_casecmp__ : (string, istring) -> int = "sta#"
implement _str_istring_casecmp__ (s, t) =
  let
    implement istring$strncmp<> = strncasecmp_for_istrings
  in
    istring_cmp (s, t)
  end
%{$
int
str_istring_casecmp (const char *s, istring_t t)
{
  return _str_istring_casecmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _istring_c_casecmp__ : (istring, istring) -> int = "sta#"
implement _istring_c_casecmp__ (s, t) =
  let
    implement istring$strncmp<> = c_strncasecmp_for_istrings
  in
    istring_cmp (s, t)
  end
%{$
int
istring_c_casecmp (istring_t s, istring_t t)
{
  return _istring_c_casecmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _istring_str_c_casecmp__ : (istring, string) -> int = "sta#"
implement _istring_str_c_casecmp__ (s, t) =
  let
    implement istring$strncmp<> = c_strncasecmp_for_istrings
  in
    istring_cmp (s, t)
  end
%{$
int
istring_str_c_casecmp (istring_t s, const char *t)
{
  return _istring_str_c_casecmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _str_istring_c_casecmp__ : (string, istring) -> int = "sta#"
implement _str_istring_c_casecmp__ (s, t) =
  let
    implement istring$strncmp<> = c_strncasecmp_for_istrings
  in
    istring_cmp (s, t)
  end
%{$
int
str_istring_c_casecmp (const char *s, istring_t t)
{
  return _str_istring_c_casecmp__ ((atstype_ptr) s, (atstype_ptr) t);
}
%}

extern fn _istring_get__ : (istring, size_t) -> char = "sta#"
implement _istring_get__ (s, i) = istring_get_exn (s, i)
%{$
char
istring_get (istring_t s, size_t i)
{
  return _istring_get__ (s, i);
}
%}

extern fn _istring_set__ : (istring, size_t, char) -> istring = "sta#"
implement _istring_set__ (s, i, c) = istring_set_exn (s, i, c)
%{$
istring_t
istring_set (istring_t s, size_t i, char c)
{
  return _istring_set__ (s, i, c);
}
%}

extern fn _istring_push__ : (istring, char) -> istring = "sta#"
implement _istring_push__ (s, c) = istring_push (s, c)
%{$
istring_t
istring_push (istring_t s, char c)
{
  return _istring_push__ (s, c);
}
%}

extern fn _istring_pop__ : istring -> istring = "sta#"
implement _istring_pop__ s = istring_pop_exn s
%{$
istring_t
istring_pop (istring_t s)
{
  return _istring_pop__ (s);
}
%}

extern fn _istring_pops__ : (istring, size_t) -> istring = "sta#"
implement _istring_pops__ (s, num) = istring_pops_exn (s, g1ofg0 num)
%{$
istring_t
istring_pops (istring_t s, size_t n)
{
  return _istring_pops__ (s, n);
}
%}

extern fn _istring_murmur3_32__ : (istring, uint) -> uint = "sta#"
implement _istring_murmur3_32__ (s, seed) = istring_murmur3_32_exn (s, seed)
%{$
unsigned int
istring_murmur3_32 (istring_t s, unsigned int seed)
{
  return _istring_murmur3_32__ (s, seed);
}
%}

extern fn _istring_is_multibyte__ : istring -> bool = "sta#"
implement _istring_is_multibyte__ s = (istring_is_multibyte s).1
%{$
bool
istring_is_multibyte (istring_t s)
{
  return _istring_is_multibyte__ (s);
}
%}

extern fn _istring_multibyte_last__ : istring -> size_t = "sta#"
implement _istring_multibyte_last__ s = istring_multibyte_last_unsafe_exn s
%{$
size_t
istring_multibyte_last (istring_t s)
{
  return _istring_multibyte_last__ (s);
}
%}

extern fn _istring_multibyte_next__ : (istring, size_t) -> size_t = "sta#"
implement _istring_multibyte_next__ (s, i) =
  istring_multibyte_next_unsafe_exn (s, g1ofg0 i)
%{$
size_t
istring_multibyte_next (istring_t s, size_t i)
{
  return _istring_multibyte_next__ (s, i);
}
%}

extern fn _istring_multibyte_prev__ : (istring, size_t) -> size_t = "sta#"
implement _istring_multibyte_prev__ (s, i) =
  istring_multibyte_prev_unsafe_exn (s, g1ofg0 i)
%{$
size_t
istring_multibyte_prev (istring_t s, size_t i)
{
  return _istring_multibyte_prev__ (s, i);
}
%}

extern fn _istring_multibyte_get__ : (istring, size_t) -> int = "sta#"
implement _istring_multibyte_get__ (s, i) =
  (istring_multibyte_get_unsafe_exn (s, i)).0
%{$
int
istring_multibyte_get (istring_t s, size_t i)
{
  return _istring_multibyte_get__ (s, i);
}
%}

extern fn _istring_multibyte_get_eff__ :
  (istring, size_t, &int, &size_t) -> void = "sta#"
implement _istring_multibyte_get_eff__ (s, i, u, n) =
  let
    val (u1, n1) = istring_multibyte_get_unsafe_exn (s, i)
  in
    u := g0ofg1 u1;
    n := g0ofg1 n1
  end
%{$
void
istring_multibyte_get_eff (istring_t s, size_t i, int *code_point,
                           size_t *length)
{
  return _istring_multibyte_get_eff__ (s, i, code_point, length);
}
%}

extern fn _istring_is_unicode__ : istring -> bool = "sta#"
implement _istring_is_unicode__ s = (istring_is_unicode s).1
%{$
bool
istring_is_unicode (istring_t s)
{
  return _istring_is_unicode__ (s);
}
%}

extern fn _istring_multibyte_push__ : (istring, int) -> istring = "sta#"
implement _istring_multibyte_push__ (s, u) =
  istring_multibyte_push_unsafe_exn (s, g1ofg0 u)
%{$
istring_t
istring_multibyte_push (istring_t s, int u)
{
  return _istring_multibyte_push__ (s, u);
}
%}

extern fn _istring_multibyte_pop__ : istring -> istring = "sta#"
implement _istring_multibyte_pop__ s = istring_multibyte_pop_unsafe s
%{$
istring_t
istring_multibyte_pop (istring_t s)
{
  return _istring_multibyte_pop__ (s);
}
%}

extern fn _istring_multibyte_pops__ : (istring, size_t) -> istring = "sta#"
implement _istring_multibyte_pops__ (s, num) =
  istring_multibyte_pops_unsafe (s, g1ofg0 num)
%{$
istring_t
istring_multibyte_pops (istring_t s, size_t num)
{
  return _istring_multibyte_pops__ (s, num);
}
%}

extern fn _istring_unicode_last__ : istring -> size_t = "sta#"
implement _istring_unicode_last__ s = istring_unicode_last_unsafe_exn s
%{$
size_t
istring_unicode_last (istring_t s)
{
  return _istring_unicode_last__ (s);
}
%}

extern fn _istring_unicode_next__ : (istring, size_t) -> size_t = "sta#"
implement _istring_unicode_next__ (s, i) =
  istring_unicode_next_unsafe_exn (s, g1ofg0 i)
%{$
size_t
istring_unicode_next (istring_t s, size_t i)
{
  return _istring_unicode_next__ (s, i);
}
%}

extern fn _istring_unicode_prev__ : (istring, size_t) -> size_t = "sta#"
implement _istring_unicode_prev__ (s, i) =
  istring_unicode_prev_unsafe_exn (s, g1ofg0 i)
%{$
size_t
istring_unicode_prev (istring_t s, size_t i)
{
  return _istring_unicode_prev__ (s, i);
}
%}

extern fn _istring_unicode_get_eff__ :
  (istring, size_t, &int, &size_t) -> void = "sta#"
implement _istring_unicode_get_eff__ (s, i, u, n) =
  let
    val (u1, n1) = istring_multibyte_get_unsafe_exn (s, i)
    macdef store () = (u := g0ofg1 u1; n := g0ofg1 n1)
  in
    if is_valid_unicode_code_point u1 then
      // Do not allow overlong sequences.
      case+ sz2i n1 of
        | 1 => store ()
        | 2 => if u <= 0x7F then $raise IstringUnicodeExn () else store ()
        | 3 => if u <= 0x7FF then $raise IstringUnicodeExn () else store ()
        | 4 => if u <= 0xFFFF then $raise IstringUnicodeExn () else store ()
        | _ => $raise IstringUnicodeExn ()
    else
      $raise IstringUnicodeExn ()
  end
%{$
void
istring_unicode_get_eff (istring_t s, size_t i, int *code_point,
                         size_t *length)
{
  return _istring_unicode_get_eff__ (s, i, code_point, length);
}

int
istring_unicode_get (istring_t s, size_t i)
{
  int code_point;
  size_t length;
  istring_unicode_get_eff (s, i, &code_point, &length);
  return code_point;
}
%}

extern fn _istring_unicode_push__ : (istring, int) -> istring = "sta#"
implement _istring_unicode_push__ (s, u) =
  istring_unicode_push_unsafe_exn (s, g1ofg0 u)
%{$
istring_t
istring_unicode_push (istring_t s, int u)
{
  return _istring_unicode_push__ (s, u);
}
%}

extern fn _istring_unicode_pop__ : istring -> istring = "sta#"
implement _istring_unicode_pop__ s = istring_multibyte_pop_unsafe s
%{$
istring_t
istring_unicode_pop (istring_t s)
{
  return _istring_unicode_pop__ (s);
}
%}

extern fn _istring_unicode_pops__ : (istring, size_t) -> istring = "sta#"
implement _istring_unicode_pops__ (s, num) =
  istring_multibyte_pops_unsafe (s, g1ofg0 num)
%{$
istring_t
istring_unicode_pops (istring_t s, size_t num)
{
  return _istring_unicode_pops__ (s, num);
}
%}

//--------------------------------------------------------------------
