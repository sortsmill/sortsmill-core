// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.popcount_prf"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload "sortsmill/core/SATS/popcount_prf.sats"

//--------------------------------------------------------------------
//
// Proofs.
//

primplement popcount_0 () = POPCOUNT_base ()
primplement popcount_1 () = POPCOUNT_odd (POPCOUNT_base ())
primplement popcount_2 () = POPCOUNT_even (POPCOUNT_odd (POPCOUNT_base ()))
primplement popcount_3 () = POPCOUNT_odd (POPCOUNT_odd (POPCOUNT_base ()))
primplement popcount_4 () = POPCOUNT_even
                             (POPCOUNT_even
                              (POPCOUNT_odd
                               (POPCOUNT_base ())))
primplement popcount_5 () = POPCOUNT_odd
                             (POPCOUNT_even
                              (POPCOUNT_odd
                               (POPCOUNT_base ())))

primplement popcount_istot {i} () =
  let
    prfun istot {i : nat} .<i>. () :<prf> [n : nat] POPCOUNT (i, n) =
      sif i == 0 then
        POPCOUNT_base ()
      else sif i mod 2 != 0 then
        POPCOUNT_odd (istot {i / 2} ())
      else
        POPCOUNT_even (istot {i / 2} ())
  in
    istot {i} ()
  end

primplement popcount_isfun {i} (pf1, pf2) =
  let
    prfun isfun {i : nat} {n1, n2 : int} .<i>.
                (pf1 : POPCOUNT (i, n1), pf2 : POPCOUNT (i, n2)) :<prf>
        [n1 == n2] void =
      sif i == 0 then
        let
          prval POPCOUNT_base () = pf1
          and POPCOUNT_base () = pf2
        in
        end
      else sif i mod 2 != 0 then
        let
          prval POPCOUNT_odd (pf1odd) = pf1
          and POPCOUNT_odd (pf2odd) = pf2
        in
          popcount_isfun (pf1odd, pf2odd)
        end
      else
        let
          prval POPCOUNT_even (pf1even) = pf1
          and POPCOUNT_even (pf2even) = pf2
        in
          popcount_isfun (pf1even, pf2even)
        end
  in
    isfun {i} (pf1, pf2)
  end

primplement popcount_identity {i} {n} pf = pf

//--------------------------------------------------------------------
