// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.bitstring_maps"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload UN = "prelude/SATS/unsafe.sats"

staload "sortsmill/core/SATS/bitstring_maps.sats"
staload _ = "sortsmill/core/DATS/bitstring_maps.dats"

%{^
#include <sortsmill/core/integer_maps.h>
#include <sortsmill/core/xgc.h> // For pthreads.
#include <sortsmill/core/double_checked_locking.h>
%}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef uintptr_to_uintptr_integer_map_base_module =
  bitstring_map_module (uintptr, uintptr)
  
extern fn
_make_uintptr_to_uintptr_integer_map_base_module__ :
  () -> uintptr_to_uintptr_integer_map_base_module = "sta#"
implement
_make_uintptr_to_uintptr_integer_map_base_module__ () =
  make_bitstring_map_module<uintptr, uintptr> ()

typedef uintptr_to_uintptr_integer_map_base_module_t =
  $extype_struct"uintptr_to_uintptr_integer_map_base_module_t" of
    {
      size = ptr,
      has_key = ptr,
      get = ptr,
      get_eff = ptr,
      set = ptr,
      remove = ptr,
      first = ptr,
      last = ptr,
      find = ptr,
      next = ptr,
      prev = ptr,
      key = ptr,
      value = ptr,
      pair_eff = ptr
    }

extern fn
_convert_uintptr_to_uintptr_integer_map_base_module__ :
  uintptr_to_uintptr_integer_map_base_module ->
    uintptr_to_uintptr_integer_map_base_module_t = "sta#"
implement
_convert_uintptr_to_uintptr_integer_map_base_module__ module =
  let
    macdef c2p (x) = $UN.cast2ptr ,(x)
    var c_module : uintptr_to_uintptr_integer_map_base_module_t
  in
    c_module.size := c2p module.size;
    c_module.has_key := c2p module.has_key;
    c_module.get := c2p module.get_exn;
    c_module.get_eff := c2p module.get_eff;
    c_module.set := c2p module.set;
    c_module.remove := c2p module.remove;
    c_module.first := c2p module.first;
    c_module.last := c2p module.last;
    c_module.find := c2p module.find;
    c_module.next := c2p module.next;
    c_module.prev := c2p module.prev;
    c_module.key := c2p module.key_exn;
    c_module.value := c2p module.value_exn;
    c_module.pair_eff := c2p module.pair_effexn;
    c_module
  end

%{$

#include <stdbool.h>

static uintptr_to_uintptr_integer_map_base_module_t *_the_uintptr_to_uintptr_integer_map_base_module__;
static volatile stm_dcl_indicator_t _uintptr_to_uintptr_integer_map_base_module_initialized__ = false;
#if SMCORE_N_ENABLE_THREADS
static pthread_mutex_t _uintptr_to_uintptr_integer_map_base_module_mutex__;
#endif

uintptr_to_uintptr_integer_map_base_module_t *
the_uintptr_to_uintptr_integer_map_base_module (void)
{
  //
  // One-time runtime initialization, with double-checked locking.
  //
  if (!stm_dcl_load_indicator (&_uintptr_to_uintptr_integer_map_base_module_initialized__))
    {
#if SMCORE_N_ENABLE_THREADS
      pthread_mutex_lock (&_uintptr_to_uintptr_integer_map_base_module_mutex__);
#endif
      if (!_uintptr_to_uintptr_integer_map_base_module_initialized__)
        {
          // Allocate a C structure to hold the module.
          _the_uintptr_to_uintptr_integer_map_base_module__ =
            ATS_MALLOC (sizeof (uintptr_to_uintptr_integer_map_base_module_t));

          // Create an ATS module and use that to initialize the C structure.
          *_the_uintptr_to_uintptr_integer_map_base_module__ =
            _convert_uintptr_to_uintptr_integer_map_base_module__
              (_make_uintptr_to_uintptr_integer_map_base_module__ ());

          stm_dcl_store_indicator (&_uintptr_to_uintptr_integer_map_base_module_initialized__, true);
        }
#if SMCORE_N_ENABLE_THREADS
      pthread_mutex_unlock (&_uintptr_to_uintptr_integer_map_base_module_mutex__);
#endif
    }
  return _the_uintptr_to_uintptr_integer_map_base_module__;
}

%}

//--------------------------------------------------------------------
