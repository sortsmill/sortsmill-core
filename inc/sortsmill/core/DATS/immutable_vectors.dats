// Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

//
// The current implementation uses ordinary tries with tails (as
// opposed to, for instance, RRB-trees).
//
// Reference:
//
// @mastersthesis{lorange2014rrb,
//   author = {L'orange, Jean Niklas},
//   title  = {{Improving RRB-Tree Performance through Transience}},
//   school = {Norwegian University of Science and Technology},
//   year   = {2014},
//   month  = {June}}
//
// <http://hypirion.com/thesis>
//

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.immutable_vectors"
#define ATS_EXTERN_PREFIX "_smcore_immutable_vectors__"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload UN = "prelude/SATS/unsafe.sats"
staload STRING = "libats/libc/SATS/string.sats"

staload "sortsmill/core/SATS/immutable_vectors.sats"
staload "sortsmill/core/SATS/arith2_prf.sats"

staload "sortsmill/core/SATS/array_misc.sats"
staload _ = "sortsmill/core/DATS/array_misc.dats"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

local
  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  //
  // WARNING/NOTICE: The internals here are designed to match those of
  // <sortsmill/core/immutable_vectors.h> for the particular NODE_SIZE.
  // Do not change the structure layout unless you know what you are doing.
  //

  #define BITS 5
  #define NODE_SIZE 32          (* 1 << BITS *)
  #define MASK 31               (* NODE_SIZE - 1 *)
  #define BUFFER_SIZE 512       (* A multiple of NODE_SIZE *)

  macdef bits () = $UN.cast {size_t BITS} BITS
  macdef node_size () = $UN.cast {size_t NODE_SIZE} NODE_SIZE
  macdef mask () = $UN.cast {size_t MASK} MASK

  extern prfn four_divides_node_size : () -<prf> MOD (NODE_SIZE, 4, 0)
  primplement four_divides_node_size () =
    let
      stadef q = NODE_SIZE / 4
      prval pfmul = mul_istot {q, 4} ()
      prval () = mul_elim pfmul
      prval () = mul_pos_pos_pos pfmul
      prval pfdivmod = mul_to_divmod_remainderless pfmul
    in
      pfdivmod
    end

  // -  -  -  -  -

  // n = the number of used entries
  // p = an address used to identify the node
  abstype node_type (entry_t : t@ype+, n : int, p : addr) = ptr
  abstype internal_node_type (entry_t : t@ype+, n : int, p : addr) = ptr
  abstype leaf_node_type (entry_t : t@ype+, n : int, p : addr) = ptr

  typedef node (entry_t : t@ype+, n : int, p : addr) = node_type (entry_t, n, p)
  typedef node (entry_t : t@ype+, n : int) = [p : addr] node (entry_t, n, p)
  typedef node (entry_t : t@ype+) = [n : int] node (entry_t, n)

  typedef internal_node (entry_t : t@ype+, n : int, p : addr) =
    internal_node_type (entry_t, n, p)
  typedef internal_node (entry_t : t@ype+, n : int) =
    [p : addr] internal_node (entry_t, n, p)
  typedef internal_node (entry_t : t@ype+) =
    [n : int] internal_node (entry_t, n)

  typedef leaf_node (entry_t : t@ype+, n : int, p : addr) =
    leaf_node_type (entry_t, n, p)
  typedef leaf_node (entry_t : t@ype+, n : int) =
    [p : addr] leaf_node (entry_t, n, p)
  typedef leaf_node (entry_t : t@ype+) = [n : int] leaf_node (entry_t, n)

  abstype node_ref (entry_t : t@ype+) = ptr

  typedef null_node = {entry_t : t@ype} node (entry_t, 0, null)
  
  // Define the_null_node in a roundabout way to avoid the need
  // for runtime initialization.
  extern fn get_the_null_node : () -<> null_node =
    "mac#_smcore_immutable_vectors__null"
  #define the_null_node (get_the_null_node ())

  extern fn node_is_null :
    {entry_t : t@ype} {n : int} {p : addr}
    node (entry_t, n, p) -<> [b : bool | b == (p == null)] bool b =
      "mac#_smcore_immutable_vectors__is_null"

  extern castfn node_to_internal :
    {entry_t : t@ype} {n : int} {p : addr}
    node (entry_t, n, p) -<> internal_node (entry_t, n, p)

  extern castfn internal_to_node :
    {entry_t : t@ype} {n : int} {p : addr}
    internal_node (entry_t, n, p) -<> node (entry_t, n, p)

  extern castfn node_to_leaf :
    {entry_t : t@ype} {n : int} {p : addr}
    node (entry_t, n, p) -<> leaf_node (entry_t, n, p)

  extern castfn leaf_to_node :
    {entry_t : t@ype} {n : int} {p : addr}
    leaf_node (entry_t, n, p) -<> node (entry_t, n, p)

  // -  -  -  -  -

  typedef vector_struct (entry_t : t@ype+, n : int, s : int) =
    '{
      length = size_t n,
      shift = uchar s,
      node = node entry_t,
      tail = leaf_node entry_t
    }
  typedef vector (entry_t : t@ype+, n : int, s : int) =
    [0 < n] vector_struct (entry_t, n, s)
  typedef vector (entry_t : t@ype+, n : int) =
    [s : nat] vector (entry_t, n, s)
  typedef vector (entry_t : t@ype+) =
    [n : pos] [s : nat] vector (entry_t, n, s)

  // -  -  -  -  -

  propdef SHIFT (s : int) =
    // Is the argument a legal value of the `shift' field of a vector?
    [0 <= s] MOD (s, BITS, 0)

  propdef TAIL_LENGTH0 (vec_length : int, tail_length0 : int) =
    [0 <= tail_length0; tail_length0 < NODE_SIZE]
    [0 < vec_length]
    MOD (vec_length, NODE_SIZE, tail_length0)

  propdef TAIL_LENGTH (vec_length : int, tail_length : int) =
    [tail_length0 : int | (tail_length0 == 0 && tail_length == NODE_SIZE) ||
                          (0 < tail_length0 && tail_length == tail_length0)]
    [0 < vec_length]
    TAIL_LENGTH0 (vec_length, tail_length0)

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

  extern castfn immutable_vector_to_vector :
    {entry_t : t@ype} {n : pos}
    immutable_vector (entry_t, n) -<>
      [s : nat] (SHIFT s | vector (entry_t, n, s))

  extern castfn vector_to_immutable_vector :
    {entry_t : t@ype} {n : pos} {s : nat}
    (SHIFT s | vector (entry_t, n, s)) -<>
      immutable_vector (entry_t, n)

  // -   -   -   -   -
  //
  // Not all of these proofs are used, but they make
  // good documentation.
  //

  extern prfn shift_0 : () -<prf> SHIFT 0
  primplement shift_0 () =
    mul_to_divmod_remainderless (mul_make {0, BITS} ())

  extern prfn shift_BITS : () -<prf> SHIFT BITS
  primplement shift_BITS () =
    mul_to_divmod_remainderless (mul_make {1, BITS} ())

  extern prfn shift_minus_BITS : {s : pos} SHIFT s -<prf> SHIFT (s - BITS)
  primplement shift_minus_BITS {s} pfshift =
    let
      prval pfmul = divmod_elim pfshift
      prval () = mul_elim pfmul
    in
      mul_to_divmod_remainderless (mul_add_const {~1} pfmul)
    end

  extern prfn shift_plus_BITS : {s : nat} SHIFT s -<prf> SHIFT (s + BITS)
  primplement shift_plus_BITS {s} pfshift =
    sif s == 0 then
      shift_BITS ()
    else
      let
        prval pfmul = divmod_elim pfshift
        prval () = mul_elim pfmul
      in
        mul_to_divmod_remainderless (mul_add_const {1} pfmul)
      end

  extern prfn shift_minus_num_times_BITS :
    {num : int} {s : nat | num * BITS <= s}
    SHIFT s -<prf> SHIFT (s - num * BITS)
  extern prfn shift_plus_num_times_BITS :
    {num : int} {s : nat | ~num * BITS <= s}
    SHIFT s -<prf> SHIFT (s + num * BITS)

  primplement shift_minus_num_times_BITS {num} {s} pfshift =
    let
      prfun loop {n1 : nat} {s1 : nat | n1 * BITS <= s1} .<n1>.
                 (pf1 : SHIFT s1) :<prf> SHIFT (s1 - n1 * BITS) =
        sif n1 == 0 then
          pf1
        else
          loop {n1 - 1} {s1 - BITS} (shift_minus_BITS pf1)
    in
      sif num < 0 then
        shift_plus_num_times_BITS {~num} {s} pfshift
      else
        loop {num} {s} pfshift
    end

  primplement shift_plus_num_times_BITS {num} {s} pfshift =
    let
      prfun loop {n1 : nat} {s1 : nat} .<n1>.
                 (pf1 : SHIFT s1) :<prf> SHIFT (s1 + n1 * BITS) =
        sif n1 == 0 then
          pf1
        else
          loop {n1 - 1} {s1 + BITS} (shift_plus_BITS pf1)
    in
      sif num < 0 then
        shift_minus_num_times_BITS {~num} {s} pfshift
      else
        loop {num} {s} pfshift
    end

  macdef shift_elim pf = divmod_qualitative ,(pf)

  // -   -   -   -   -

  extern fn shift_left :
    {i, n : nat} (size_t i, size_t n) -<> [j : nat] size_t j = "mac#%"

  extern fn shift_right :
    {i, n : nat}
    (size_t i, size_t n) -<>
      [j : nat | (i == 0 && j == 0) || (0 < i && j < i)] size_t j = "mac#%"

  fn {}
  apply_mask {i : nat} (i : size_t i) :<>
      [k : nat | k <= i && k < NODE_SIZE] size_t k =
    $UN.cast {[k : nat | k <= i && k < NODE_SIZE] size_t k}
      ((g0ofg1 i) land (i2sz MASK))

  fn {}
  apply_mask_prf {i : nat} (i : size_t i) :<>
      [k : nat | k <= i && k < NODE_SIZE]
      (MOD (i, NODE_SIZE, k) | size_t k) =
    let
      val [k : int] k = apply_mask i
      extern praxi assert_mod : () -<prf> MOD (i, NODE_SIZE, k)
    in
      (assert_mod () | k)
    end

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

  (* To write to a node, you need a WRITE_PERMISSION
     proof. These proofs are how we enforce immutability:
     you have to create a new node to get a proof. *)
  dataprop WRITE_PERMISSION (p : addr) =
    | {p : addr} WRITE_PERMISSION_ (p) of ()

  prfn
  make_write_permission {p : addr} (p : ptr p) : WRITE_PERMISSION p =
    let
      extern praxi make_permission : () -<prf> WRITE_PERMISSION p
    in
      make_permission ()
    end

  //
  // Use struct_wrapper_size() to make a good guess at the size
  // of a node in the C implementation. The assumption here is
  // that any padding at the end of the struct is independent
  // of the type of the elements of the array that is wrapped
  // in the struct{} construct.
  //
  // FIXME FIXME FIXME FIXME FIXME: Is this assumption reliable?
  //
  extern fn
  struct_wrapper_size (num_bytes : size_t) : Size_t = "mac#%"

  fn {entry_t : t@ype}
  new_internal_node () :
      [p : addr | null < p]
      (WRITE_PERMISSION p | internal_node (entry_t, 0, p)) =
    let
      macdef internal_node_size () =
        (struct_wrapper_size (NODE_SIZE * sizeof<node entry_t>))
      val [p : addr] (pfview, pfgc | p) = malloc_gc (internal_node_size ())
      // NOTE: The following memset is redundant if one is using Boehm GC.
      val _ = $STRING.memset_unsafe (p, 0, NODE_SIZE * sizeof<node entry_t>)
    in
      (make_write_permission p |
       $UN.castvwtp0 {internal_node (entry_t, 0, p)} ((pfview, pfgc | p)))
    end

  fn {entry_t : t@ype}
  new_leaf_node () :
      [p : addr | null < p]
      (WRITE_PERMISSION p | leaf_node (entry_t, 0, p)) =
    let
      macdef leaf_node_size () =
        (struct_wrapper_size (NODE_SIZE * sizeof<entry_t>))
      val [p : addr] (pfview, pfgc | p) = malloc_gc (leaf_node_size ())
    in
      (make_write_permission p |
       $UN.castvwtp0 {leaf_node (entry_t, 0, p)} ((pfview, pfgc | p)))
    end

  fn {entry_t : t@ype}
  copy_internal_node {n : int} (node : internal_node (entry_t, n)) :
      [p : addr | null < p]
      (WRITE_PERMISSION p | internal_node (entry_t, n, p)) =
    let
      macdef internal_node_size () =
        (struct_wrapper_size (NODE_SIZE * sizeof<node entry_t>))
      val [p : addr] (pfview, pfgc | p) = malloc_gc (internal_node_size ())
      val old_node = $UN.cast {ptr} node
      val new_node = $UN.cast {ptr} p
      extern fn
      struct_wrapper_copy (dest : ptr, src : ptr, num_bytes : size_t) : void =
        "mac#%"
      val () =
        if ptr_isnot_null old_node then
          struct_wrapper_copy (new_node, old_node, internal_node_size ())
    in
      (make_write_permission p |
       $UN.castvwtp0 {internal_node (entry_t, n, p)} ((pfview, pfgc | p)))
    end

  fn {entry_t : t@ype}
  copy_leaf_node {n : int} (node : leaf_node (entry_t, n)) :
      [p : addr | null < p]
      (WRITE_PERMISSION p | leaf_node (entry_t, n, p)) =
    let
      macdef leaf_node_size () =
        (struct_wrapper_size (NODE_SIZE * sizeof<entry_t>))
      val [p : addr] (pfview, pfgc | p) = malloc_gc (leaf_node_size ())
      val old_node = $UN.cast {ptr} node
      val new_node = $UN.cast {ptr} p
      extern fn
      struct_wrapper_copy (dest : ptr, src : ptr, num_bytes : size_t) : void =
        "mac#%"
      val () = struct_wrapper_copy (new_node, old_node, leaf_node_size ())
    in
      (make_write_permission p |
       $UN.castvwtp0 {leaf_node (entry_t, n, p)} ((pfview, pfgc | p)))
    end

  fn {entry_t : t@ype}
  internal_node_get {i : nat | i < NODE_SIZE}
                    (node : internal_node entry_t, i : size_t i) :
      node entry_t =
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val a = $UN.cast {arrayref (node entry_t, NODE_SIZE)} node
    in
      a[i]
    end

  fn {entry_t : t@ype}
  leaf_node_get {i : nat | i < NODE_SIZE}
                (node : leaf_node entry_t, i : size_t i) : entry_t =
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val a = $UN.cast {arrayref (entry_t, NODE_SIZE)} node
    in
      a[i]
    end

  fn {entry_t : t@ype}
  leaf_node_gets {i, num : nat | i + num <= NODE_SIZE}
                 {n : int}
                 {addr : addr}
                 (pf : !array_v (entry_t?, addr, num) >>
                        array_v (entry_t, addr, num) |
                  node : leaf_node (entry_t, n),
                  i : size_t i,
                  num : size_t num,
                  p : ptr addr) : void =
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val src = ptr_add<entry_t> ($UN.cast {ptr} node, i)
      val _ = $STRING.memcpy_unsafe (p, src, num * sizeof<entry_t>)
      prval () = array_v_unsafely_force_initialized pf
    in
    end

  fn {entry_t : t@ype}
  internal_node_set {i : nat | i < NODE_SIZE} {p : addr | null < p}
                    {n : int}
                    (perm : WRITE_PERMISSION p |
                     node : internal_node (entry_t, n, p),
                     i : size_t i,
                     x : node entry_t) : void =
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val a = $UN.cast {arrayref (node entry_t, NODE_SIZE)} node
    in
      a[i] := x
    end

  fn {entry_t : t@ype}
  leaf_node_set {i : nat | i < NODE_SIZE} {p : addr | null < p}
                {n : int}
                (perm : WRITE_PERMISSION p |
                 node : leaf_node (entry_t, n, p),
                 i : size_t i,
                 x : entry_t) : void =
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val a = $UN.cast {arrayref (entry_t, NODE_SIZE)} node
    in
      a[i] := x
    end

  fn {entry_t : t@ype}
  leaf_node_sets {i, num : nat | i + num <= NODE_SIZE}
                 {p : addr | null < p}
                 {n : int}
                 {size : nat | num <= size}
                 {addr : addr}
                 (perm : WRITE_PERMISSION p,
                  pfview : !array_v (entry_t, addr, size) >> _ |
                  node : leaf_node (entry_t, n, p),
                  i : size_t i,
                  num : size_t num,
                  p : ptr addr) : void =
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val q = $UN.cast {ptr} node
      val qi = ptr_add<entry_t> (q, i)
      val _ = $STRING.memcpy_unsafe (g1ofg0 qi, p, num * sizeof<entry_t>)
    in
    end

  fn {entry_t : t@ype}
  leaf_node_clear {i : nat | i < NODE_SIZE} {p : addr | null < p}
                  {n : int}
                  (perm : WRITE_PERMISSION p |
                   node : leaf_node (entry_t, n, p),
                   i : size_t i) : void =
    // Fill an entry with zeros so Boehm GC can collect data
    // that might have been referred to by the entry.
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val p = $UN.cast {ptr} node
      val p = ptr_add<entry_t> (p, i)
      val _ = $STRING.memset_unsafe (g1ofg0 p, 0, sizeof<entry_t>)
    in
    end

  fn {entry_t : t@ype}
  leaf_node_clears {i, num : nat | i + num <= NODE_SIZE}
                   {p : addr | null < p}
                   {n : int}
                   (perm : WRITE_PERMISSION p |
                    node : leaf_node (entry_t, n, p),
                    i : size_t i,
                    num : size_t num) : void =
    // Fill num entries with zeros so Boehm GC can collect data
    // that might have been referred to by the entries.
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val p = $UN.cast {ptr} node
      val p = ptr_add<entry_t> (p, i)
      val _ = $STRING.memset_unsafe (g1ofg0 p, 0, num * sizeof<entry_t>)
    in
    end

  fn {entry_t : t@ype}
  internal_node_get_cell {i : nat | i < NODE_SIZE} {p : addr | null < p}
                         {n : int}
                         (perm : WRITE_PERMISSION p |
                          node : internal_node (entry_t, n, p),
                          i : size_t i) : node_ref entry_t =
    // Get a one-element, left-valued slice of an internal_node.
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val a = $UN.cast {arrayref (node entry_t, NODE_SIZE)} node
      val p = arrayref2ptr a
      val p = ptr_add<ptr> (p, i)
      val (pf, fpf | p) = $UN.ptr_vtake {@[node entry_t][1]} p
    in
      $UN.castvwtp0 {node_ref entry_t} ((pf, fpf | p))
    end

  fn {entry_t : t@ype}
  leaf_node_get_entry_ref {i : nat | i < NODE_SIZE} {p : addr}
                          {n : int}
                          (node : leaf_node (entry_t, n, p), i : size_t i) :
      [entry_addr : addr | null < entry_addr]
      immutable_vector_entry_ref (entry_t, entry_addr) =
    let
      // Take advantage of the following paragraph in C99 section 6.7.2.1:
      //
      //   `Within a structure object, the non-bit-field members
      //    and the units in which bit-fields reside have addresses
      //    that increase in the order in which they are declared.
      //    A pointer to a structure object, suitably converted,
      //    points to its initial member (or if that member is a
      //    bit-field, then to the unit in which it resides), and
      //    vice versa. There may be unnamed padding within a structure
      //    object, but not at its beginning.'
      //
      val a = $UN.cast {arrayref (node entry_t, NODE_SIZE)} node
      val [p : addr] p = arrayref2ptr a
      val entry_addr = ptr_add<entry_t> (p, i)
      val [entry_addr : addr] entry_addr = $UN.cast {Ptr1} entry_addr
    in
      $UN.cast {immutable_vector_entry_ref (entry_t, entry_addr)} entry_addr
    end

  fn {entry_t : t@ype}
  make_node_ref (node_var : &(node entry_t)) : node_ref entry_t =
    $UN.cast {node_ref entry_t} (addr@ node_var)

  fn {entry_t : t@ype}
  make_node_ref_uninitized (node_var : &(node entry_t)?) : node_ref entry_t =
    $UN.cast {node_ref entry_t} (addr@ node_var)

  fn {entry_t : t@ype}
  node_ref_get (q : node_ref entry_t) : node entry_t =
    let
      val (pf, fpf | p) = $UN.ptr_vtake {@[node entry_t][1]} ($UN.cast {ptr} q)
    in
      !($UN.castvwtp0 {ref (node entry_t)} ((pf, fpf | p)))
    end

  fn {entry_t : t@ype}
  node_ref_set (q : node_ref entry_t, node : node entry_t) : void =
    let
      val (pf, fpf | p) = $UN.ptr_vtake {@[node entry_t][1]} ($UN.cast {ptr} q)
    in
      !($UN.castvwtp0 {ref (node entry_t)} ((pf, fpf | p))) := node
    end

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

  fn {entry_t : t@ype}
  tail_length0_given_length {n : pos} (n : size_t n) :
      [k0 : nat | k0 < NODE_SIZE; k0 <= n]
      (TAIL_LENGTH0 (n, k0) | size_t k0) =
    // Return zero if the tail is full; otherwise return
    // the tail length. Also return a proof of the result.
    apply_mask_prf n

  fn {entry_t : t@ype}
  tail_length0_given_vector {n : pos} (vec : vector (entry_t, n)) :
      [k0 : nat | k0 < NODE_SIZE; k0 <= n]
      (TAIL_LENGTH0 (n, k0) | size_t k0) =
    // Return zero if the tail is full; otherwise return
    // the tail length. Also return a proof of the result.
    tail_length0_given_length vec.length

  fn {entry_t : t@ype}
  tail_length_given_length {n : pos} (n : size_t n) :
      [k : pos | k <= NODE_SIZE; k <= n]
      (TAIL_LENGTH (n, k) | size_t k) =
    // Return the tail length, and a proof of the result.
    let
      val (pf0 | k0) = tail_length0_given_length n
    in
      if k0 = i2sz 0 then
        let
          prval () = divmod_mul_elim pf0
        in
          (pf0 | node_size ())
        end
      else
        (pf0 | k0)
    end

  fn {entry_t : t@ype}
  tail_length_given_vector {n : pos} (vec : vector (entry_t, n)) :
      [k : pos | k <= NODE_SIZE; k <= n]
      (TAIL_LENGTH (n, k) | size_t k) =
    // Return the tail length, and a proof of the result.
    tail_length_given_length vec.length

  symintr tail_length0
  overload tail_length0 with tail_length0_given_length
  overload tail_length0 with tail_length0_given_vector
  symintr tail_length
  overload tail_length with tail_length_given_length
  overload tail_length with tail_length_given_vector

  fun {entry_t : t@ype}
  descend_trie_without_copying {s : nat} {i : nat} .<s>.
                               (pfshift : SHIFT s |
                                p : node entry_t,
                                s : size_t s,
                                i : size_t i) :
      leaf_node (entry_t, NODE_SIZE) =
    if s = i2sz 0 then
      // FIXME: Prove the node is full.
      $UN.cast (node_to_leaf p)
    else
      let
        prval () = shift_elim pfshift
        val q = node_to_internal p
        val j = apply_mask (i \shift_right s)
      in
        descend_trie_without_copying<entry_t> (shift_minus_BITS pfshift |
                                               internal_node_get (q, j),
                                               s - bits (), i)
      end

  fun {entry_t : t@ype}
  descend_trie_with_copying {s : nat} {i : nat} .<s>.
                            (pfshift : SHIFT s |
                             p : node entry_t,
                             s : size_t s,
                             i : size_t i,
                             parent : node_ref entry_t) :
      @(leaf_node entry_t, node_ref entry_t) =
    if s = i2sz 0 then
      @(node_to_leaf p, parent)
    else
      let
        prval () = shift_elim pfshift
        val (perm | q) = copy_internal_node (node_to_internal p)
        val () = node_ref_set<entry_t> (parent, internal_to_node q)
        val j = apply_mask (i \shift_right s)
        val parent = internal_node_get_cell (perm | q, j)
      in
        descend_trie_with_copying<entry_t> (shift_minus_BITS pfshift |
                                            node_ref_get<entry_t> parent,
                                            s - bits (), i, parent)
      end

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

  extern fn {entry_t : t@ype}
  get_chunk :
    {n : pos} {i : nat}
    {num : nat | i + num <= n}
    {k : nat} {addr : addr}
    (!array_v (entry_t?, addr + k * sizeof entry_t, num) >>
      array_v (entry_t, addr + k * sizeof entry_t, num) |
     immutable_vector (entry_t, n), size_t i, size_t num, ptr addr, size_t k) ->
      void

  implement {entry_t}
  get_chunk (pf | v, i, num, p, k) =
    let
      val [s : int] (pfshift | vec) = immutable_vector_to_vector v
      val (_ | n_tail) = tail_length vec
      val tail_offset = vec.length - n_tail
      val pk = ptr_add<entry_t> (p, k)
    in
      if tail_offset <= i then
        // Get data from the tail.
        let
          val j = i - tail_offset
          val () = leaf_node_gets<entry_t> (pf | vec.tail, j, num, pk)
        in
        end
      else
        // Get data from a leaf node in the trie.
        let
          val s = $UN.cast {size_t s} {uchar s} vec.shift
          val leaf = descend_trie_without_copying (pfshift | vec.node, s, i)
          val j = i mod (node_size ())
          val () = assertloc (j + num <= node_size ())
          val () = leaf_node_gets<entry_t> (pf | leaf, j, num, pk)
        in
        end
    end

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

  extern fn {entry_t : t@ype}
  set_chunk :
    {n : nat} {i : nat}
    {num : pos | i + num <= n}
    {size : nat}
    {k : nat | k + num <= size}
    {addr : addr}
    (!array_v (entry_t, addr, size) >> _ |
     immutable_vector (entry_t, n), size_t i,
     size_t num, ptr addr, size_t k) ->
      immutable_vector (entry_t, n)

  implement {entry_t}
  set_chunk (pfview | v, i, num, p, k) =
    let
      val [s : int] (pfshift | vec) = immutable_vector_to_vector v
      val (_ | n_tail) = tail_length vec
      val tail_offset = vec.length - n_tail
      val (pfleft, pfright | _, p) = array_ptr_split_at (pfview | p, k)
    in
      if tail_offset <= i then
        // Replace the tail.
        let
          val (perm | tail) = copy_leaf_node vec.tail
          val j = i - tail_offset
          val () = leaf_node_sets<entry_t> (perm, pfright | tail, j, num, p)
          val new_vec =
            '{
              length = vec.length,
              shift = vec.shift,
              node = vec.node,
              tail = tail
            }
          prval () = pfview := array_v_unsplit (pfleft, pfright)
        in
          vector_to_immutable_vector (pfshift | new_vec)
        end
      else
        // Replace a leaf node in the trie, and all
        // the internal nodes along the way.
        let
          var top_parent_var : node entry_t
          val top_parent = make_node_ref_uninitized top_parent_var
          val @(leaf, parent) =
            descend_trie_with_copying (pfshift | vec.node,
                                                 $UN.cast {size_t s} vec.shift,
                                                 i, top_parent)
          val (perm | leaf) = copy_leaf_node leaf
          val () = node_ref_set<entry_t> (parent, leaf_to_node leaf)
          val j = i mod (node_size ())
          val () = assertloc (j + num <= node_size ())
          val () = leaf_node_sets<entry_t> (perm, pfright | leaf, j, num, p)
          val new_vec =
            '{
              length = vec.length,
              shift = vec.shift,
              node = node_ref_get<entry_t> top_parent,
              tail = vec.tail
            }
          prval () = pfview := array_v_unsplit (pfleft, pfright)
        in
          vector_to_immutable_vector (pfshift | new_vec)
        end
    end

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

  extern fn {entry_t : t@ype}
  push_chunk :
    {n : nat}
    {num, size, i : nat | 1 <= num; i + num <= size; num <= NODE_SIZE}
    {addr : addr}
    (!array_v (entry_t, addr, size) >> _ |
     immutable_vector (entry_t, n), size_t num, ptr addr, size_t i) ->
      immutable_vector (entry_t, n + num)

  implement {entry_t}
  push_chunk {n} {num, size, i} {addr} (pfview | v, num, p, i) =
    // Push no more than what it takes to fill the current tail;
    // or, if the tail is full already, push no more than what
    // it take to fill a new tail.
    let
      val v_is_nil = immutable_vector_is_nil v
      val (pfleft, pfright | _, p) = array_ptr_split_at (pfview | p, i)
    in
      if v_is_nil then
        // Start a new vector.
        let
          val (perm | tail) = new_leaf_node<entry_t> ()
          val () =
            leaf_node_sets<entry_t> (perm, pfright | tail, i2sz 0, num, p)
          val new_vec =
            '{
              length = num,
              shift = $UN.cast {uchar 0} 0,
              node = the_null_node,
              tail = tail
            }
          prval () = pfview := array_v_unsplit (pfleft, pfright)
        in
          vector_to_immutable_vector (shift_0 () | new_vec)
        end
      else
        let
          val [s : int] (pfshift | vec) = immutable_vector_to_vector v
          val tail = vec.tail
          val (_ | n_tail) = tail_length vec
        in
          if n_tail + num <= node_size () then
            // Extend the tail.
            let
              val (perm | tail) = copy_leaf_node<entry_t> tail
              val () =
                leaf_node_sets<entry_t> (perm, pfright | tail, n_tail, num, p)
              val new_vec =
                '{
                  length = vec.length + num,
                  shift = vec.shift,
                  node = vec.node,
                  tail = tail
                }
              prval () = pfview := array_v_unsplit (pfleft, pfright)
            in
              vector_to_immutable_vector (pfshift | new_vec)
            end
          else
            // The tail is full. Push it and start a new one.
            let
              val j = vec.length - n_tail
            in
              if j = i2sz 0 then
                // Start a new trie.
                let
                  val (perm | new_tail) = new_leaf_node<entry_t> ()
                  val () =
                    leaf_node_sets<entry_t>
                      (perm, pfright | new_tail, i2sz 0, num, p)
                  val new_vec =
                    '{
                      length = vec.length + num,
                      shift = $UN.cast {uchar 0} 0,
                      node = leaf_to_node tail,
                      tail = new_tail
                    }
                  prval () = pfview := array_v_unsplit (pfleft, pfright)
                in
                  vector_to_immutable_vector (shift_0 () | new_vec)
                end
              else
                let
                  val shift = $UN.cast {size_t s} {uchar s} vec.shift
                  val trie_is_fully_dense =
                    (j = ((i2sz 1) \shift_left (shift + bits ())))
                in
                  if trie_is_fully_dense then
                    // Raise the height of the trie.
                    let
                      // Create a new node for top level.
                      val (perm | new_node) = new_internal_node ()

                      // The old top-level node becomes the entry at
                      // position 0.
                      val () =
                        internal_node_set (perm | new_node, i2sz 0, vec.node)

                      // The new subtrie will start at position 1.
                      val top_parent =
                        internal_node_get_cell (perm | new_node, i2sz 1)

                      // Make new internal nodes.
                      val @(_, parent) =
                        descend_trie_with_copying
                          (pfshift | vec.node, shift, j, top_parent)

                      // Push the old tail.
                      val () = node_ref_set<entry_t> (parent, leaf_to_node tail)

                      // Start a new tail.
                      val (perm | new_tail) = new_leaf_node<entry_t> ()
                      val () =
                        leaf_node_sets<entry_t>
                          (perm, pfright | new_tail, i2sz 0, num, p)

                      val new_vec =
                        '{
                          length = vec.length + num,
                          shift = $UN.cast {uchar (s + BITS)}
                                           {size_t (s + BITS)}
                                           (shift + bits ()),
                          node = internal_to_node new_node,
                          tail = new_tail
                        }                      
                      prval () = pfview := array_v_unsplit (pfleft, pfright)
                    in
                      vector_to_immutable_vector (shift_plus_BITS pfshift |
                                                  new_vec)
                    end
                  else
                    // Keep the current trie height.
                    let
                      var top_parent_var : node entry_t
                      val top_parent = make_node_ref_uninitized top_parent_var

                      // Make new internal nodes.
                      val @(_, parent) =
                        descend_trie_with_copying (pfshift | vec.node, shift,
                                                             j, top_parent)

                      // Push the old tail.
                      val () = node_ref_set<entry_t> (parent, leaf_to_node tail)

                      // Start a new tail.
                      val (perm | new_tail) = new_leaf_node<entry_t> ()
                      val () =
                        leaf_node_sets<entry_t>
                          (perm, pfright | new_tail, i2sz 0, num, p)

                      val new_vec =
                        '{
                          length = vec.length + num,
                          shift = $UN.cast {uchar s} {size_t s} shift,
                          node = node_ref_get<entry_t> top_parent,
                          tail = new_tail
                        }                      
                      prval () = pfview := array_v_unsplit (pfleft, pfright)
                    in
                      vector_to_immutable_vector (pfshift | new_vec)
                    end
                end
            end
        end
    end

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

in

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

  primplement
  lemma_immutable_vector_param v =
    case+ v of
      | immutable_vector_nil () => ()
      | immutable_vector_nonnil _ => ()

  implement {}
  immutable_vector_is_nil v =
    case+ v of
      | immutable_vector_nil () => true
      | immutable_vector_nonnil _ => false

  implement {}
  immutable_vector_length {entry_t} v =
    case+ v of
      | immutable_vector_nil () => i2sz 0
      | immutable_vector_nonnil _ =>
        let
          val (_ | vec) = immutable_vector_to_vector v
        in
          vec.length
        end

  implement {entry_t}
  immutable_vector_get (v, i) =
    let
      val [s : int] (pfshift | vec) = immutable_vector_to_vector v
      val (_ | n_tail) = tail_length vec
      val tail_offset = vec.length - n_tail
    in
      if tail_offset <= i then
        // The desired entry is in the tail.
        let
          val tail = vec.tail
        in
          leaf_node_get (tail, i - tail_offset)
        end
      else
        // The desired entry is in the trie.
        let
          val s = $UN.cast {size_t s} {uchar s} vec.shift
          val p = descend_trie_without_copying (pfshift | vec.node, s, i)
          val leaf = $UN.cast {leaf_node entry_t} p
        in
          leaf_node_get (leaf, apply_mask i)
        end
    end

  implement {entry_t}
  immutable_vector_get_exn (v, i) =
    let
      val i = g1ofg0 i
      val n = immutable_vector_length v
    in
      if i < n then
        immutable_vector_get (v, i)
      else
        $raise ImmutableVectorSubscriptExn ()
    end

  implement {entry_t}
  immutable_vector_gets_to_viewptr {n} {i} {num} {addr}
                                   (pf | v, i, num, p) =
    let
      stadef sz = sizeof entry_t

      fun loop {j : nat | j <= num} .<num - j>.
               (pfleft : array_v (entry_t, addr, j),
                pfright : array_v (entry_t?, addr + j * sz, num - j) |
                j : size_t j) :<cloref1>
          (array_v (entry_t, addr, num) | void) =
        if j = num then
          let
            prval () = array_v_unnil pfright
          in
            (pfleft | ())
          end
        else
          let
            val chunk_size = min (num - j, node_size ())
            prval @(pfchunk, pfright) = array_v_split_at (pfright | chunk_size)
            val () = get_chunk<entry_t> (pfchunk | v, i + j, chunk_size, p, j)
            prval pfleft = array_v_unsplit (pfleft, pfchunk)
          in
            loop (pfleft, pfright | j + chunk_size)
          end

      val chunk_size = min (num, node_size () - (i mod (node_size ())))
    in
      if i2sz 0 < chunk_size then
        let
          prval @(pfleft, pfright) = array_v_split_at (pf | chunk_size)
          val () = get_chunk<entry_t> (pfleft | v, i, chunk_size, p, i2sz 0)
          val (pfresult | ()) = loop (pfleft, pfright | chunk_size)
          prval () = pf := pfresult
        in
        end
      else
        let
          prval () = pf := array_v_unnil_nil pf
        in
        end
    end

  implement {entry_t}
  immutable_vector_gets_to_array (v, i, num, x) =
    immutable_vector_gets_to_viewptr<entry_t> (view@ x | v, i, num, addr@ x)

  implement {entry_t}
  immutable_vector_gets_to_arrayref_slice {..} {..} {num}
                                          (v, i, num, x, i_start) =
    let
      val p = ptr_add<entry_t> (arrayref2ptr x, i_start)
      val (pf, fpf | p) = $UN.ptr_vtake {@[entry_t][num]} p
      val () = immutable_vector_gets_to_array<entry_t> (v, i, num, !p)
      prval () = fpf pf
    in
    end

  implement {entry_t}
  immutable_vector_gets_to_arrayref (v, i, num, x) =
    immutable_vector_gets_to_arrayref_slice (v, i, num, x, i2sz 0)

  implement {entry_t}
  immutable_vector_gets_as_viewptr {..} {..} {num} (v, i, num) =
    let
      val (pfview, pfgc | p) = array_ptr_alloc<entry_t> num
      val () = immutable_vector_gets_eff<entry_t> (pfview | v, i, num, p)
      extern praxi let_gc_handle_it : {p : agz} mfree_gc_v p -<prf> void
      prval () = let_gc_handle_it pfgc
    in
      (pfview | p)
    end

  implement {entry_t}
  immutable_vector_gets_as_arrayref {..} {..} {num} (v, i, num) =
    let
      val (pfview, pfgc | p) = array_ptr_alloc<entry_t> num
      val () = immutable_vector_gets_eff<entry_t> (pfview | v, i, num, p)
    in
      $UN.castvwtp0 {arrayref (entry_t, num)} ((pfview, pfgc | p))
    end

  implement {entry_t}
  immutable_vector_gets_as_viewptr_exn (v, i, num) =
    if i + num <= immutable_vector_length v then
      immutable_vector_gets_as_viewptr<entry_t> (v, i, num)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement {entry_t}
  immutable_vector_gets_as_arrayref_exn (v, i, num) =
    if i + num <= immutable_vector_length v then
      immutable_vector_gets_as_arrayref<entry_t> (v, i, num)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement {entry_t}
  immutable_vector_set (v, i, x) =
    let
      val [s : int] (pfshift | vec) = immutable_vector_to_vector v
      val (_ | n_tail) = tail_length vec
      val tail_offset = vec.length - n_tail
    in
      if tail_offset <= i then
        // The desired entry is in the tail. Make a new tail.
        let
          val tail = vec.tail
          val (perm | tail) = copy_leaf_node<entry_t> tail
          val () = leaf_node_set<entry_t> (perm | tail, i - tail_offset, x)
          val new_vec =
            '{
              length = vec.length,
              shift = vec.shift,
              node = vec.node,
              tail = tail
            }
        in
          vector_to_immutable_vector (pfshift | new_vec)
        end
      else
        // The desired entry is in the trie. Make new nodes.
        let
          var top_level_node_var : node entry_t
          val top_level_node = make_node_ref_uninitized top_level_node_var

          val s = $UN.cast {size_t s} {uchar s} vec.shift
          val @(leaf, parent) =
            descend_trie_with_copying (pfshift | vec.node, s, i, top_level_node)
          val (perm | leaf) = copy_leaf_node<entry_t> leaf
          val j = apply_mask i
          val () = leaf_node_set<entry_t> (perm | leaf, j, x)
          val () = node_ref_set<entry_t> (parent, leaf_to_node leaf)
          val new_vec =
            '{
              length = vec.length,
              shift = vec.shift,
              node = node_ref_get<entry_t> top_level_node,
              tail = vec.tail
            }
        in
          vector_to_immutable_vector (pfshift | new_vec)
        end
    end

  implement {entry_t}
  immutable_vector_set_exn (v, i, x) =
    let
      val i = g1ofg0 i
      val n = immutable_vector_length v
    in
      if i < n then
        immutable_vector_set (v, i, x)
      else
        $raise ImmutableVectorSubscriptExn ()
    end

  implement {entry_t}
  immutable_vector_sets_from_viewptr {n} {i} {num, size} {addr}
                                       (pfview | v, i, num, p) =
    let
      fun loop {j : nat | j <= num} .<num - j>.
               (pfview : !array_v (entry_t, addr, size) >> _ |
                v : immutable_vector (entry_t, n),
                j : size_t j) :<cloref1>
          immutable_vector (entry_t, n) =
        if j = num then
          v
        else
          let
            val chunk_size = min (num - j, node_size ())
          in
            loop (pfview |
                  set_chunk<entry_t> (pfview | v, i + j, chunk_size, p, j),
                  j + chunk_size)
          end

      val chunk_size = min (num, node_size () - (i mod (node_size ())))
    in
      if i2sz 0 < num then
        loop (pfview |
              set_chunk<entry_t> (pfview | v, i, chunk_size, p, i2sz 0),
              chunk_size)
      else
        v
    end

  implement {entry_t}
  immutable_vector_sets_from_array (v, i, num, x) =
    immutable_vector_sets_from_viewptr (view@ x | v, i, num, addr@ x)

  implement {entry_t}
  immutable_vector_sets_from_arrayref {n} {i} {num, size} (v, i, num, x) =
    let
      val (vboxed_pfview | p) = arrayref_get_viewptr x
      prval (pfview, consumepf) = unvbox vboxed_pfview
      val new_ivect =
        immutable_vector_sets_from_viewptr<entry_t> (pfview | v, i, num, p)
      prval () = consumepf pfview
    in
      new_ivect
    end

  implement {entry_t}
  immutable_vector_sets_from_viewptr_exn {n} {i} {num, size}
                                         (pfview | v, i, num, p) =
    let
      val n = immutable_vector_length v
    in
      if i + num < n then
        immutable_vector_sets {n} {i} {num, size} (pfview | v, i, num, p)
      else
        $raise ImmutableVectorSubscriptExn ()
    end

  implement {entry_t}
  immutable_vector_sets_from_array_exn {n} {i} {num, size} (v, i, num, x) =
    let
      val n = immutable_vector_length v
    in
      if i + num < n then
        immutable_vector_sets {n} {i} {num, size} (v, i, num, x)
      else
        $raise ImmutableVectorSubscriptExn ()
    end

  implement {entry_t}
  immutable_vector_sets_from_arrayref_exn {n} {i} {num, size} (v, i, num, x) =
    let
      val n = immutable_vector_length v
    in
      if i + num < n then
        immutable_vector_sets {n} {i} {num, size} (v, i, num, x)
      else
        $raise ImmutableVectorSubscriptExn ()
    end

  implement {entry_t}
  immutable_vector_push (v, x) =
    let
      val v_is_nil = immutable_vector_is_nil v
    in
      if v_is_nil then
        // Start a new vector.
        let
          val (perm | tail) = new_leaf_node<entry_t> ()
          val () = leaf_node_set<entry_t> (perm | tail, i2sz 0, x)
          val new_vec =
            '{
              length = $UN.cast {size_t 1} 1,
              shift = $UN.cast {uchar 0} 0,
              node = the_null_node,
              tail = tail
            }
        in
          vector_to_immutable_vector (shift_0 () | new_vec)
        end
      else
        let
          val [s : int] (pfshift | vec) = immutable_vector_to_vector v
          val tail = vec.tail
          val (_ | n_tail) = tail_length vec
        in
          if n_tail < node_size () then
            // Extend the tail.
            let
              val (perm | tail) = copy_leaf_node<entry_t> tail
              val () = leaf_node_set<entry_t> (perm | tail, n_tail, x)
              val new_vec =
                '{
                  length = succ vec.length,
                  shift = vec.shift,
                  node = vec.node,
                  tail = tail
                }
            in
              vector_to_immutable_vector (pfshift | new_vec)
            end
          else
            // The tail is full. Push it and start a new one.
            let
              val i = vec.length - n_tail
            in
              if i = i2sz 0 then
                // Start a new trie.
                let
                  val (perm | new_tail) = new_leaf_node<entry_t> ()
                  val () = leaf_node_set<entry_t> (perm | new_tail, i2sz 0, x)
                  val new_vec =
                    '{
                      length = succ vec.length,
                      shift = $UN.cast {uchar 0} 0,
                      node = leaf_to_node tail,
                      tail = new_tail
                    }
                in
                  vector_to_immutable_vector (shift_0 () | new_vec)
                end
              else
                let
                  val shift = $UN.cast {size_t s} {uchar s} vec.shift
                  val trie_is_fully_dense =
                    (i = ((i2sz 1) \shift_left (shift + bits ())))
                in
                  if trie_is_fully_dense then
                    // Raise the height of the trie.
                    let
                      // Create a new node for top level.
                      val (perm | new_node) = new_internal_node ()

                      // The old top-level node becomes the entry at
                      // position 0.
                      val () =
                        internal_node_set (perm | new_node, i2sz 0, vec.node)

                      // The new subtrie will start at position 1.
                      val top_parent =
                        internal_node_get_cell (perm | new_node, i2sz 1)

                      // Make new internal nodes.
                      val @(p, parent) =
                        descend_trie_with_copying
                          (pfshift | vec.node, shift, i, top_parent)

                      // Push the old tail.
                      val () = node_ref_set<entry_t> (parent, leaf_to_node tail)

                      // Start a new tail.
                      val (perm | new_tail) = new_leaf_node<entry_t> ()
                      val () =
                        leaf_node_set<entry_t> (perm | new_tail, i2sz 0, x)

                      val new_vec =
                        '{
                          length = succ vec.length,
                          shift = $UN.cast {uchar (s + BITS)}
                                           {size_t (s + BITS)}
                                           (shift + bits ()),
                          node = internal_to_node new_node,
                          tail = new_tail
                        }                      
                    in
                      vector_to_immutable_vector (shift_plus_BITS pfshift |
                                                  new_vec)
                    end
                  else
                    // Keep the current trie height.
                    let
                      var top_parent_var : node entry_t
                      val top_parent = make_node_ref_uninitized top_parent_var

                      // Make new internal nodes.
                      val @(p, parent) =
                        descend_trie_with_copying (pfshift | vec.node, shift,
                                                             i, top_parent)

                      // Push the old tail.
                      val () = node_ref_set<entry_t> (parent, leaf_to_node tail)

                      // Start a new tail.
                      val (perm | new_tail) = new_leaf_node<entry_t> ()
                      val () =
                        leaf_node_set<entry_t> (perm | new_tail, i2sz 0, x)

                      val new_vec =
                        '{
                          length = succ vec.length,
                          shift = $UN.cast {uchar s} {size_t s} shift,
                          node = node_ref_get<entry_t> top_parent,
                          tail = new_tail
                        }                      
                    in
                      vector_to_immutable_vector (pfshift | new_vec)
                    end
                end
            end
        end
    end

  implement {entry_t}
  immutable_vector_pushes_from_viewptr {n} {num, size} {addr}
                                       (pfview | v, num, p) =
    let
      fun loop {i : nat | i <= num} .<num - i>.
               (pfview : !array_v (entry_t, addr, size) >> _ |
                v : immutable_vector (entry_t, n + i),
                i : size_t i) :<cloref1>
          immutable_vector (entry_t, n + num) =
        if i = num then
          v
        else if immutable_vector_is_nil v then
          let
            val chunk_size = min (num - i, node_size ())
            val v = push_chunk<entry_t> (pfview | v, chunk_size, p, i)
          in
            loop (pfview | v, i + chunk_size)
          end
        else
          let
            val (_ | vec) = immutable_vector_to_vector v
            val (_ | n0_tail) = tail_length0 vec
            val chunk_size = min (num - i, node_size () - n0_tail)
            val v = push_chunk<entry_t> (pfview | v, chunk_size, p, i)
          in
            loop (pfview | v, i + chunk_size)
          end
    in
      loop (pfview | v, i2sz 0)
    end

  implement {entry_t}
  immutable_vector_pushes_from_array (v, num, x) =
    immutable_vector_pushes_from_viewptr<entry_t> (view@ x | v, num, addr@ x)

  implement {entry_t}
  immutable_vector_pushes_from_arrayref (v, num, x) =
    let
      val (vboxed_pfview | p) = arrayref_get_viewptr x
      prval (pfview, consumepf) = unvbox vboxed_pfview
      val new_ivect =
        immutable_vector_pushes_from_viewptr<entry_t> (pfview | v, num, p)
      prval () = consumepf pfview
    in
      new_ivect
    end

  implement {entry_t}
  immutable_vector_pop$clear_popped_entries () = true

  implement {entry_t}
  immutable_vector_pop v =
    let
      val n = immutable_vector_length v
    in
      if n = i2sz 1 then
        immutable_vector_nil
      else
        let
          val [s : int] (pfshift | vec) = immutable_vector_to_vector v
          val (_ | n_tail) = tail_length vec
        in
          if n_tail = i2sz 1 then
            // We have to take the last block of the trie
            // and make it a new tail.
            let
              val () = assertloc (node_size () < n)
              val i = pred n - node_size ()
            in
              if i = i2sz 0 then
                // All entries now are in the tail; the trie
                // is empty.
                let
                  val new_vec =
                    '{
                      length = pred vec.length,
                      shift = vec.shift,
                      node = the_null_node,
                      tail = node_to_leaf vec.node
                    }
                  in
                    vector_to_immutable_vector (pfshift | new_vec)
                  end
              else if i = ((i2sz 1) \shift_left
                           ($UN.cast {Size_t} vec.shift)) then
                // Lower the height of the trie, yielding
                // a fully dense trie.
                let
                  val s = $UN.cast {size_t s} {uchar s} vec.shift
                  val () = assertloc (bits () <= s)
                  val p =
                    descend_trie_without_copying (pfshift | vec.node, s, i)
                  val new_vec =
                    '{
                      length = pred vec.length,
                      shift = $UN.cast {uchar (s - BITS)} (s - bits ()),
                      node = internal_node_get (node_to_internal vec.node,
                                                i2sz 0),
                      tail = p
                    }
                in
                  vector_to_immutable_vector (shift_minus_BITS pfshift |
                                              new_vec)
                end
              else
                let
                  var top_parent_var : node entry_t = vec.node
                  val top_parent = make_node_ref top_parent_var

                  val s = $UN.cast {size_t s} {uchar s} vec.shift

                  // Copy internal nodes until reaching either the leaf
                  // or a point at which the trie can be pruned.                  
                  fun descend_to_pruning_point {s : nat} .<s>.
                                               (pfshift : SHIFT s |
                                                p : node entry_t,
                                                s : size_t s,
                                                parent :
                                                node_ref entry_t) :<cloref1>
                      [s1 : nat | s1 <= s]
                      @(SHIFT s1 | node entry_t, size_t s1, node_ref entry_t) =
                    if s = i2sz 0 then
                      @(pfshift | p, s, parent)
                    else
                      let
                        prval () = shift_elim pfshift
                        val (perm | q) = copy_internal_node (node_to_internal p)
                        val () = node_ref_set<entry_t> (parent,
                                                        internal_to_node q)
                        val j = apply_mask (i \shift_right s)
                        val parent = internal_node_get_cell (perm | q, j)
                        val pruning_point_reached =
                          (i = ((i \shift_right s) \shift_left s))
                      in
                        if pruning_point_reached then
                          @(shift_minus_BITS pfshift |
                            node_ref_get<entry_t> parent, s - bits (), parent)
                        else
                          descend_to_pruning_point
                            (shift_minus_BITS pfshift |
                             node_ref_get<entry_t> parent, s - bits (), parent)
                      end
                  val @(pfshift1 | p, s, pruning_point) =
                    descend_to_pruning_point (pfshift | vec.node, s, top_parent)

                  // We have found the place at which to prune the trie
                  // by putting a null node there, but must continue
                  // searching for the new tail. All the indices from
                  // now on are zero.
                  fun descend_to_leaf {s : nat} .<s>.
                                      (pfshift : SHIFT s |
                                       p : node entry_t,
                                       s : size_t s) : leaf_node entry_t =
                    if s = i2sz 0 then
                      node_to_leaf p
                    else
                      let
                        prval () = shift_elim pfshift
                      in
                        descend_to_leaf (shift_minus_BITS pfshift |
                                         internal_node_get (node_to_internal p,
                                                            i2sz 0),
                                         s - bits ())
                      end
                  val leaf = descend_to_leaf (pfshift1 | p, s)

                  val () = node_ref_set<entry_t> (pruning_point, the_null_node)
                  val new_vec =
                    '{
                      length = pred vec.length,
                      shift = vec.shift,
                      node = node_ref_get<entry_t> top_parent,
                      tail = leaf
                    }
                in
                  vector_to_immutable_vector (pfshift | new_vec)
                end
            end
          else if immutable_vector_pop$clear_popped_entries () then
            // Clear the popped entry, so it can be collected
            // as garbage if not referenced elsewhere.
            let
              val (perm | tail) = copy_leaf_node<entry_t> vec.tail
              val () = leaf_node_clear (perm | tail, pred n_tail)
              val new_vec =
                '{
                  length = pred vec.length,
                  shift = vec.shift,
                  node = vec.node,
                  tail = tail
                }
            in
              vector_to_immutable_vector (pfshift | new_vec)
            end
          else
            // Do not bother to clear the popped entry.
            let
              val new_vec =
                '{
                  length = pred vec.length,
                  shift = vec.shift,
                  node = vec.node,
                  tail = vec.tail
                }
            in
              vector_to_immutable_vector (pfshift | new_vec)
            end
        end        
    end

  implement {entry_t}
  immutable_vector_pop_exn v =
    let
      prval () = lemma_immutable_vector_param v
      val is_nil = immutable_vector_is_nil v
    in
      if not is_nil then
        immutable_vector_pop v
      else
        $raise ImmutableVectorPopNilExn ()
    end

  implement {entry_t}
  immutable_vector_pops {..} {num} (v, num) =
    if num = i2sz 0 then
      v                         // No change.
    else
      let
        val n = immutable_vector_length v
      in
        if num = n then
          immutable_vector_nil  // Pop the entire vector.
        else
          let
            val [s : int] (pfshift | vec) = immutable_vector_to_vector v
            val (_ | n_tail) = tail_length vec
          in
            if num < n_tail then
              // Merely shorten the tail.
              let
                val (perm | new_tail) = copy_leaf_node<entry_t> vec.tail
                val () =
                  if immutable_vector_pop$clear_popped_entries () then
                    leaf_node_clears (perm | new_tail, n_tail - num, num)
                val new_vec =
                  '{
                    length = vec.length - num,
                    shift = vec.shift,
                    node = vec.node,
                    tail = new_tail
                  }                      
              in
                vector_to_immutable_vector (pfshift | new_vec)
              end
            else
              let
                val (_ | n_new_tail) = tail_length (n - num)
                val new_tail_offset = (n - num) - n_new_tail
                val s = $UN.cast {size_t s} {uchar s} vec.shift
                val leaf =
                  descend_trie_without_copying (pfshift | vec.node, s,
                                                          new_tail_offset)
                val (perm | new_tail) = copy_leaf_node<entry_t> leaf
                val () =
                  if immutable_vector_pop$clear_popped_entries () then
                    leaf_node_clears (perm | new_tail, n_new_tail,
                                             node_size () - n_new_tail)
              in
                if new_tail_offset = i2sz 0 then
                  // Delete the trie. We now have only a tail.
                  let
                    val new_vec =
                      '{
                        length = vec.length - num,
                        shift = $UN.cast {uchar 0} 0,
                        node = the_null_node,
                        tail = new_tail
                      }                      
                  in
                    vector_to_immutable_vector (shift_0 () | new_vec)
                  end
                else
                  let
                    // The index of the last entry in the new trie.
                    val i = pred new_tail_offset

                    // Possibly reduce trie height.
                    fun reduce_height {shift : nat | shift <= s}
                                      (pfshift : SHIFT shift |
                                       node : node entry_t,
                                       shift : size_t shift) :<cloref1>
                        [s1 : nat | s1 <= shift]
                        @(SHIFT s1 | node entry_t, size_t s1) =
                      if shift = i2sz 0 then
                        @(pfshift | node, shift)
                      else if apply_mask (i \shift_right shift) != i2sz 0 then
                        @(pfshift | node, shift)
                      else
                        let
                          val () = assertloc (bits () <= shift)
                          val child_node =
                            internal_node_get (node_to_internal node, i2sz 0)
                        in
                          reduce_height (shift_minus_BITS pfshift |
                                         child_node, shift - bits ())
                        end
                    val [s1 : int] @(pfshift1 | top_node, s1) =
                      reduce_height (pfshift | vec.node, s)

                    // Make new internal nodes.
                    var top_parent_var : node entry_t = top_node
                    val top_parent = make_node_ref top_parent_var
                    val _ =
                      descend_trie_with_copying (pfshift1 | top_node, s1, i,
                                                            top_parent)
                    val new_vec =
                      '{
                        length = vec.length - num,
                        shift = $UN.cast {uchar s1} s1,
                        node = node_ref_get<entry_t> top_parent,
                        tail = new_tail
                      }                      
                  in
                    vector_to_immutable_vector (pfshift1 | new_vec)
                  end
              end
          end
      end

  implement {entry_t}
  immutable_vector_pops_exn (v, num) =
    let
      prval () = lemma_immutable_vector_param v
      val n = immutable_vector_length v
    in
      if num <= n then
        immutable_vector_pops<entry_t> (v, num)
      else
        $raise ImmutableVectorPopNilExn ()
    end

  implement {entry_t}
  immutable_vector_slice_append {m, n} {i_start, i_end} (u, v, i_start, i_end) =
    if (immutable_vector_is_nil u) * (i_start = i2sz 0) then
      immutable_vector_pops<entry_t> (v, immutable_vector_length v - i_end)
    else if i_start = i_end then
      u
    else if i_end - i_start <= i2sz BUFFER_SIZE then
      let
        var buffer : @[entry_t][BUFFER_SIZE]
        val num = i_end - i_start
        prval @(pfleft, pfright) = array_v_split_at (view@ buffer | num)
        val () =
          immutable_vector_gets_to_viewptr<entry_t>
            (pfleft | v, i_start, num, addr@ buffer)
        val new_ivect =
          immutable_vector_pushes_from_viewptr<entry_t>
            (pfleft | u, num, addr@ buffer)
        prval () = array_v_uninitize pfleft
        prval () = view@ buffer := array_v_unsplit (pfleft, pfright)
      in
        new_ivect
      end
    else
      let
        var buffer : @[entry_t][BUFFER_SIZE]

        // Try to push blocks that will end on node boundaries.
        val r = $UN.cast {[r : nat | r <= NODE_SIZE] size_t r}
                         ((immutable_vector_length u) mod (node_size ()))
        val num1 = i2sz BUFFER_SIZE - r
        prval @(pfleft, pfright) = array_v_split_at (view@ buffer | num1)
        val () =
          immutable_vector_gets_to_viewptr<entry_t>
            (pfleft | v, i_start, num1, addr@ buffer)
        val new_ivect =
          immutable_vector_pushes_from_viewptr<entry_t>
            (pfleft | u, num1, addr@ buffer)
        prval () = array_v_uninitize pfleft
        prval () = view@ buffer := array_v_unsplit (pfleft, pfright)

        fun loop {i : nat | i < i_end} .<i_end - i>.
                 (buffer : &(@[entry_t?][BUFFER_SIZE]) >> _,
                  new_ivect : immutable_vector (entry_t, m + (i - i_start)),
                  i : size_t i) :<cloref1>
            immutable_vector (entry_t, m + (i_end - i_start)) =
          let
            prval () = lemma_immutable_vector_param new_ivect
            val num1 = min (i_end - i, i2sz BUFFER_SIZE)
            prval @(pfleft, pfright) = array_v_split_at (view@ buffer | num1)
            val () =
              immutable_vector_gets_to_viewptr<entry_t>
                (pfleft | v, i, num1, addr@ buffer)
            val new_ivect =
              immutable_vector_pushes_from_viewptr<entry_t>
                (pfleft | new_ivect, num1, addr@ buffer)
            prval () = array_v_uninitize pfleft
            prval () = view@ buffer := array_v_unsplit (pfleft, pfright)
            val i = i + num1
          in
            if i = i_end then
              new_ivect
            else
              loop (buffer, new_ivect, i)
          end

        val i = i_start + num1
        val new_ivect = loop (buffer, new_ivect, i)
      in
        new_ivect
      end

  implement {entry_t}
  immutable_vector_slice_append_exn (u, v, i_start, i_end) =
    if (i_start <= i_end) * (i_end <= immutable_vector_length v) then
      immutable_vector_slice_append<entry_t> (u, v, i_start, i_end)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement {entry_t}
  immutable_vector_slice (v, i_start, i_end) =
    immutable_vector_slice_append<entry_t> (immutable_vector_nil,
                                            v, i_start, i_end)

  implement {entry_t}
  immutable_vector_slice_exn (v, i_start, i_end) =
    immutable_vector_slice_append_exn<entry_t> (immutable_vector_nil,
                                                v, i_start, i_end)

  implement {entry_t}
  immutable_vector_append (u, v) =
    let
      val i_start = i2sz 0
      val i_end = immutable_vector_length v
    in
      immutable_vector_slice_append<entry_t> (u, v, i_start, i_end)
    end

  implement {entry_t}
  immutable_vector_slice_reverse_append {m, n} {i_start, i_end}
                                        (u, v, i_start, i_end) =
    if i_start = i_end then
      u
    else if i_end - i_start <= i2sz BUFFER_SIZE then
      let
        var buffer : @[entry_t][BUFFER_SIZE]
        val num = i_end - i_start
        prval @(pfleft, pfright) = array_v_split_at (view@ buffer | num)
        val () =
          immutable_vector_gets_to_viewptr<entry_t>
            (pfleft | v, i_start, num, addr@ buffer)
        val () = array_subreverse<entry_t> (buffer, i2sz 0, num)
        val new_ivect =
          immutable_vector_pushes_from_viewptr<entry_t>
            (pfleft | u, num, addr@ buffer)
        prval () = array_v_uninitize pfleft
        prval () = view@ buffer := array_v_unsplit (pfleft, pfright)
      in
        new_ivect
      end
    else
      let
        var buffer : @[entry_t][BUFFER_SIZE]

        // Try to push blocks that will end on node boundaries.
        val r = $UN.cast {[r : nat | r <= NODE_SIZE] size_t r}
                         ((immutable_vector_length u) mod (node_size ()))
        val num1 = i2sz BUFFER_SIZE - r
        prval @(pfleft, pfright) = array_v_split_at (view@ buffer | num1)
        val () =
          immutable_vector_gets_to_viewptr<entry_t>
            (pfleft | v, i_end - num1, num1, addr@ buffer)
        val () = array_subreverse<entry_t> (buffer, i2sz 0, num1)
        val new_ivect =
          immutable_vector_pushes_from_viewptr<entry_t>
            (pfleft | u, num1, addr@ buffer)
        prval () = array_v_uninitize pfleft
        prval () = view@ buffer := array_v_unsplit (pfleft, pfright)

        fun loop {i : nat | i_start < i; i < i_end} .<i>.
                 (buffer : &(@[entry_t?][BUFFER_SIZE]) >> _,
                  new_ivect : immutable_vector (entry_t, m + (i_end - i)),
                  i : size_t i) :<cloref1>
            immutable_vector (entry_t, m + (i_end - i_start)) =
          let
            val num1 = min (i - i_start, i2sz BUFFER_SIZE)
            prval @(pfleft, pfright) = array_v_split_at (view@ buffer | num1)
            val () =
              immutable_vector_gets_to_viewptr<entry_t>
                (pfleft | v, i - num1, num1, addr@ buffer)
            val () = array_subreverse<entry_t> (buffer, i2sz 0, num1)
            val new_ivect =
              immutable_vector_pushes_from_viewptr<entry_t>
                (pfleft | new_ivect, num1, addr@ buffer)
            prval () = array_v_uninitize pfleft
            prval () = view@ buffer := array_v_unsplit (pfleft, pfright)
            val i = i - num1
          in
            if i = i_start then
              new_ivect
            else
              loop (buffer, new_ivect, i)
          end

        val i = i_end - num1
        val new_ivect = loop (buffer, new_ivect, i)
      in
        new_ivect
      end

  implement {entry_t}
  immutable_vector_slice_reverse_append_exn (u, v, i_start, i_end) =
    if (i_start <= i_end) * (i_end <= immutable_vector_length v) then
      immutable_vector_slice_reverse_append<entry_t> (u, v, i_start, i_end)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement {entry_t}
  immutable_vector_slice_reverse (v, i_start, i_end) =
    immutable_vector_slice_reverse_append<entry_t> (immutable_vector_nil,
                                                    v, i_start, i_end)

  implement {entry_t}
  immutable_vector_slice_reverse_exn (v, i_start, i_end) =
    immutable_vector_slice_reverse_append_exn<entry_t> (immutable_vector_nil,
                                                        v, i_start, i_end)

  implement {entry_t}
  immutable_vector_reverse_append (u, v) =
    let
      val i_start = i2sz 0
      val i_end = immutable_vector_length v
    in
      immutable_vector_slice_reverse_append<entry_t> (u, v, i_start, i_end)
    end

  implement {entry_t}
  immutable_vector_reverse v =
    immutable_vector_reverse_append<entry_t> (immutable_vector_nil, v)

  implement {entry_t}
  immutable_vector_entry_ref {..} {i} {vector_addr} (v, i) =
    let
      val n = immutable_vector_length v
    in
      if i < n then
        let
          val [s : int] (pfshift | vec) = immutable_vector_to_vector v
          val (_ | n_tail) = tail_length vec
          val tail_offset = vec.length - n_tail
        in
          if tail_offset <= i then
            // The desired entry is in the tail.
            let
              val tail = vec.tail
              val [entry_addr : addr] entry_ref =
                leaf_node_get_entry_ref (tail, i - tail_offset)
              extern praxi make_proof :
                () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i,
                                                      entry_addr)
            in
              (make_proof () | entry_ref)
            end
          else
            // The desired entry is in the trie.
            let
              val s = $UN.cast {size_t s} {uchar s} vec.shift
              val p = descend_trie_without_copying (pfshift | vec.node, s, i)
              val leaf = $UN.cast {leaf_node entry_t} p
              val [entry_addr : addr] entry_ref =
                leaf_node_get_entry_ref (leaf, apply_mask i)
              extern praxi make_proof :
                () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i,
                                                      entry_addr)
            in
              (make_proof () | entry_ref)
            end
        end
      else
        let
          extern praxi make_proof :
            () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i, null)
        in
          (make_proof () |
           $UN.cast {immutable_vector_entry_ref (entry_t, null)} the_null_ptr)
        end
    end

  implement {entry_t}
  immutable_vector_entry_ref_is_nil {entry_addr} eref =
    ptr_is_null ($UN.cast {ptr entry_addr} eref)

  implement {entry_t}
  immutable_vector_entry_ref_next {vector_addr} {n} {i} {entry_addr}
                                  (pf_entry_ref | v, i, eref) =
    let
      val n = immutable_vector_length v
      val j = succ i
    in
      if j = n then
        // We are going `past the end of the vector'.
        let
          extern praxi make_proof :
            () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i + 1, null)
        in
          (make_proof () |
           $UN.cast {immutable_vector_entry_ref (entry_t, null)} the_null_ptr)
        end
      else if apply_mask j = i2sz 0 then
        // We have to find another node.
        let
          val [s : int] (pfshift | vec) = immutable_vector_to_vector v
          val (_ | n_tail) = tail_length vec
          val tail_offset = n - n_tail
        in
          if tail_offset <= j then
            // The desired entry is in the tail.
            let
              val [entry_addr : addr] entry_ref =
                leaf_node_get_entry_ref (vec.tail, j - tail_offset)
              extern praxi make_proof :
                () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i + 1,
                                                      entry_addr)
            in
              (make_proof () | entry_ref)
            end
          else
            // The desired entry is in the trie.
            let
              val s = $UN.cast {size_t s} {uchar s} vec.shift
              val leaf =
                descend_trie_without_copying (pfshift | vec.node, s, j)
              val [entry_addr : addr] entry_ref =
                leaf_node_get_entry_ref (leaf, i2sz 0)
              extern praxi make_proof :
                () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i + 1,
                                                      entry_addr)
            in
              (make_proof () | entry_ref)
            end
        end
      else
        // Move to the next entry within the current node.
        let
          val p = ptr_succ<entry_t> ($UN.cast {Ptr1} eref)
          val [p : addr] p = $UN.cast {Ptr1} p
          extern praxi make_proof :
            () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i + 1, p)
        in
          (make_proof () |
           $UN.cast {immutable_vector_entry_ref (entry_t, p)} p)
        end
    end

  implement {entry_t}
  immutable_vector_entry_ref_prev {vector_addr} {n} {i} {entry_addr}
                                  (pf_entry_ref | v, i, eref) =
    let
      val n = immutable_vector_length v
    in
      if i = i2sz 0 then
        // We are going `before the beginning of the vector'.
        let
          extern praxi make_proof :
            () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i - 1, null)
        in
          (make_proof () |
           $UN.cast {immutable_vector_entry_ref (entry_t, null)} the_null_ptr)
        end
      else if apply_mask i = i2sz 0 then
        // We have to find another node. The desired entry is in the trie.
        // (A little consideration shows that it cannot be in the tail.)
        let
          val j = pred i
          val [s : int] (pfshift | vec) = immutable_vector_to_vector v
          val (_ | n_tail) = tail_length vec
          val tail_offset = n - n_tail
        in
          let
            val s = $UN.cast {size_t s} {uchar s} vec.shift
            val leaf =
              descend_trie_without_copying (pfshift | vec.node, s, j)
            val [entry_addr : addr] entry_ref =
              leaf_node_get_entry_ref (leaf, apply_mask j)
            extern praxi make_proof :
              () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i - 1,
                                                    entry_addr)
          in
            (make_proof () | entry_ref)
          end
        end
      else
        // Move to the previous entry within the current node.
        let
          val p = ptr_pred<entry_t> ($UN.cast {Ptr1} eref)
          val [p : addr] p = $UN.cast {Ptr1} p
          extern praxi make_proof :
            () -<prf> IMMUTABLE_VECTOR_ENTRY_REF (vector_addr, i - 1, p)
        in
          (make_proof () |
           $UN.cast {immutable_vector_entry_ref (entry_t, p)} p)
        end
    end

  implement {entry_t}
  immutable_vector_entry_ref_get eref =
    let
      val p = $UN.cast {ptr} eref
    in
      if ptr_isnot_null p then
        !($UN.castvwtp0 {ref entry_t} ($UN.ptr_vtake p))
      else
        $raise ImmutableVectorEntryRefNullExn ()
    end

  implement {entry_t}
  immutable_vector_murmur3_32 {n} (v, seed) =
    let
      extern fn test_if_little_endian : () -> bool = "mac#%"
      extern fn murmur3_blocks : (uint32, ptr, size_t, bool) -> uint32 = "mac#%"
      extern fn murmur3_tail : (uint32, ptr, size_t) -> uint32 = "mac#%"
      extern fn murmur3_fmix32 : uint32 -<> uint32 = "mac#%"
      extern fn murmur3_32_finalize : (uint32, size_t) -<> uint32 = "mac#%"

      fn process_leaf (h : uint32,
                       leaf : leaf_node (entry_t, NODE_SIZE),
                       little_endian : bool) : uint32 =
        let
          val byte_count = sizeof<entry_t> * node_size ()
          val nblocks = byte_count / (i2sz 4)
        in
          murmur3_blocks (h, $UN.cast leaf, nblocks, little_endian)
        end

      fn process_trie {s : nat} {tail_offset : nat}
                      (pfshift : SHIFT s |
                       h : uint32,
                       vec : vector (entry_t, n),
                       tail_offset : size_t tail_offset,
                       little_endian : bool) : uint32 =
        let
          fun loop {i : nat} (h : uint32, i : size_t i) :<cloref1> uint32 =
            if i < tail_offset then
              let
                val leaf =
                  descend_trie_without_copying
                    (pfshift | vec.node, $UN.cast vec.shift, i)
                val h = process_leaf (h, leaf, little_endian)
              in
                loop (h, i + node_size ())
              end
            else
              h
        in
          loop (h, i2sz 0)
        end

      fn process_tail {n_tail : pos | n_tail <= n}
                      (h : uint32,
                       vec : vector (entry_t, n),
                       n_tail : size_t n_tail,
                       little_endian : bool) : uint32 =
        let
          val byte_count = sizeof<entry_t> * n_tail
          val nblocks = byte_count / (i2sz 4)
          val offset = (i2sz 4) * nblocks
          val p = $UN.cast {ptr} vec.tail
          val h = murmur3_blocks (h, p, nblocks, little_endian)
          val h = murmur3_tail (h, ptr_add<byte> (p, offset),
                                byte_count land (i2sz 3))
        in
          h
        end
    in
      case+ v of
        | immutable_vector_nil () => $UN.cast (murmur3_fmix32 ($UN.cast seed))
        | immutable_vector_nonnil _ =>
          let
            val little_endian = test_if_little_endian ()
            val [s : int] (pfshift | vec) = immutable_vector_to_vector v
            val (_ | n_tail) = tail_length vec
            val tail_offset = vec.length - n_tail
            val h = $UN.cast {uint32} seed
            val h = process_trie (pfshift | h, vec, tail_offset,
                                            little_endian)
            val h = process_tail (h, vec, n_tail, little_endian)
          in
            $UN.cast (murmur3_32_finalize (h, vec.length))
          end
    end

  implement {entry_t}
  immutable_vector_murmur3_32_exn (v, seed) =
    let
      val seed = g1ofg0 seed
    in
      // Note: On x86 and amd64 architectures, with common
      // C compilers, the following condition always is true,
      // because `unsigned int' is 32 bits long.
      if seed <= 0xffffffffu then
        immutable_vector_murmur3_32<entry_t> (v, seed)
      else
        $raise ImmutableVectorHashSeedExn ()
    end

  // -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

end

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

implement {entry_t}
make_immutable_vector_module () =
  let
  in
    '{
      nil = immutable_vector_nil,
      is_nil = immutable_vector_is_nil<>,
      length = immutable_vector_length<>,
      get = immutable_vector_get<entry_t>,
      get_exn = immutable_vector_get_exn<entry_t>,
      gets_viewptr = immutable_vector_gets_as_viewptr<entry_t>,
      gets_arrayref = immutable_vector_gets_as_arrayref<entry_t>,
      gets_viewptr_exn = immutable_vector_gets_as_viewptr_exn<entry_t>,
      gets_arrayref_exn = immutable_vector_gets_as_arrayref_exn<entry_t>,
      gets_eff_viewptr = immutable_vector_gets_to_viewptr<entry_t>,
      gets_eff_array = immutable_vector_gets_to_array<entry_t>,
      gets_eff_arrayref = immutable_vector_gets_to_arrayref<entry_t>,
      gets_eff_arrayref_slice =
        immutable_vector_gets_to_arrayref_slice<entry_t>,
      set = immutable_vector_set<entry_t>,
      set_exn = immutable_vector_set_exn<entry_t>,
      sets_viewptr = immutable_vector_sets_from_viewptr<entry_t>,
      sets_array = immutable_vector_sets_from_array<entry_t>,
      sets_arrayref = immutable_vector_sets_from_arrayref<entry_t>,
      sets_viewptr_exn = immutable_vector_sets_from_viewptr_exn<entry_t>,
      sets_array_exn = immutable_vector_sets_from_array_exn<entry_t>,
      sets_arrayref_exn = immutable_vector_sets_from_arrayref_exn<entry_t>,
      push = immutable_vector_push<entry_t>,
      pushes_viewptr = immutable_vector_pushes_from_viewptr<entry_t>,
      pushes_array = immutable_vector_pushes_from_array<entry_t>,
      pushes_arrayref = immutable_vector_pushes_from_arrayref<entry_t>,
      pop = immutable_vector_pop<entry_t>,
      pop_exn = immutable_vector_pop_exn<entry_t>,
      pops = immutable_vector_pops<entry_t>,
      pops_exn = immutable_vector_pops_exn<entry_t>,
      slice_append = immutable_vector_slice_append<entry_t>,
      slice_append_exn = immutable_vector_slice_append_exn<entry_t>,
      slice = immutable_vector_slice<entry_t>,
      slice_exn = immutable_vector_slice_exn<entry_t>,
      append = immutable_vector_append<entry_t>,
      slice_reverse_append = immutable_vector_slice_reverse_append<entry_t>,
      slice_reverse_append_exn =
        immutable_vector_slice_reverse_append_exn<entry_t>,
      slice_reverse = immutable_vector_slice_reverse<entry_t>,
      slice_reverse_exn = immutable_vector_slice_reverse_exn<entry_t>,
      reverse_append = immutable_vector_reverse_append<entry_t>,
      reverse = immutable_vector_reverse<entry_t>,
      entry_ref = immutable_vector_entry_ref<entry_t>,
      entry_ref_is_nil = immutable_vector_entry_ref_is_nil<entry_t>,
      entry_ref_next = immutable_vector_entry_ref_next<entry_t>,
      entry_ref_prev = immutable_vector_entry_ref_prev<entry_t>,
      entry_ref_get = immutable_vector_entry_ref_get<entry_t>,
      murmur3_32 = immutable_vector_murmur3_32<entry_t>,
      murmur3_32_exn = immutable_vector_murmur3_32_exn<entry_t>
    }
  end

//--------------------------------------------------------------------
