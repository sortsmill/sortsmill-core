// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.
#define ATS_PACKNAME "SMCORE.array_misc"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload "sortsmill/core/SATS/array_misc.sats"

implement {t}
array_ptr_split_at (pfview | p, i) =
  let
    prval (pfleft, pfright) = array_v_split_at (pfview | i)
  in
    (pfleft, pfright | p, ptr_add<t> (p, i))
  end

//--------------------------------------------------------------------
