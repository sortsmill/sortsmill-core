// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Strings based on "sortsmill/core/SATS/immutable_vectors.sats".
//

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.istrings"
#define ATS_EXTERN_PREFIX "_smcore_istrings__"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload UN = "prelude/SATS/unsafe.sats"

staload "sortsmill/core/SATS/array_misc.sats"
staload _ = "sortsmill/core/DATS/array_misc.dats"

staload "sortsmill/core/SATS/immutable_vectors.sats"
staload _ = "sortsmill/core/DATS/immutable_vectors.dats"

staload "sortsmill/core/SATS/unicode_helpers.sats"
staload _ = "sortsmill/core/DATS/unicode_helpers.dats"

staload "sortsmill/core/SATS/istrings.sats"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

local

  #define CNUL '\000'

  fn clamp_to_Sgn (i : int) =
    case+ 0 of
      | _ when i < 0 => ~1
      | _ when i = 0 => 0
      | _ => 1

  extern fn c2i (c : char) : [i : nat | i <= 0xFF] int i = "mac#%"

  extern fn copy_chars :
    {n : nat} {i, m : nat | i + m <= n} {p : addr}
    (!array_v (char?, p, m) >> array_v (char, p, m) |
     istring n, size_t i, size_t m, ptr p) ->
      void
  implement copy_chars (pfview | s, i, m, p) =
    immutable_vector_gets_eff<char> (pfview | s, i, m, p)

  fn copy_string {n : nat} {i, m : nat | i + m <= n} {p : addr}
                 (pfview : !array_v (char?, p, m + 1) >>
                            array_v (char, p, m + 1) |
                  s : istring n,
                  i : size_t i,
                  m : size_t m,
                  p : ptr p) : void =
    let
      // Split the string into its contents and its terminating nul.
      prval @(pfstr, pfnul) = array_v_split_at (pfview | m)

      // Copy the contents from the vector to the storage.
      val () = immutable_vector_gets_eff<char> (pfstr | s, i, m, p)

      // Set the terminating nul.
      val p_nul = ptr_add<char> (p, m)
      val (pf, fpf | p_nul) = $UN.ptr_vtake {@[char?][1]} p_nul
      val () = array_initize_elt<char> (!p_nul, i2sz 1, CNUL)
      prval () = fpf pfnul
      prval () = pfnul := pf

      // Rejoin the contents with its terminating nul.
      prval () = pfview := array_v_unsplit (pfstr, pfnul)
    in
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

in // local

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  primplement lemma_istring_param s = lemma_immutable_vector_param s

  implement {} istring_is_nil s = immutable_vector_is_nil<> s
  implement {} istring_length s = immutable_vector_length<> s

  implement istring_get (s, i) = immutable_vector_get<char> (s, i)
  implement istring_get_exn (s, i) =
    let
      prval () = lemma_istring_param s
    in
      immutable_vector_get_exn<char> (s, i)
    end
  implement istring_set (s, i, c) = immutable_vector_set<char> (s, i, c)
  implement istring_set_exn (s, i, c) = immutable_vector_set_exn<char> (s, i, c)
  implement istring_push (s, c) = immutable_vector_push<char> (s, c)

  implement istring_pop s =
    let
      // Normally there is no advantage to clearing chars no longer used.
      implement immutable_vector_pop$clear_popped_entries<char> () = false
    in
      immutable_vector_pop<char> s
    end

  implement istring_pop_exn s =
    let
      // Normally there is no advantage to clearing chars no longer used.
      implement immutable_vector_pop$clear_popped_entries<char> () = false
    in
      immutable_vector_pop_exn<char> s
    end

  implement istring_pops (s, num) =
    let
      // Normally there is no advantage to clearing chars no longer used.
      implement immutable_vector_pop$clear_popped_entries<char> () = false
    in
      immutable_vector_pops<char> (s, num)
    end

  implement istring_pops_exn (s, num) =
    let
      // Normally there is no advantage to clearing chars no longer used.
      implement immutable_vector_pop$clear_popped_entries<char> () = false
    in
      immutable_vector_pops_exn<char> (s, num)
    end

  implement istring_slice (s, i, j) = immutable_vector_slice<char> (s, i, j)
  implement istring_slice_exn (s, i, j) =
    let
      prval () = lemma_istring_param s
    in
      immutable_vector_slice_exn<char> (s, i, j)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement istring_slice_to_strnptr {..} {i_start, i_end} (s, i_start, i_end) =
    let
      val m = i_end - i_start
      stadef m = i_end - i_start
      val (pfview, pfgc | p) = array_ptr_alloc<char> (succ m)
      val () = copy_string (pfview | s, i_start, m, p)
    in
      $UN.castvwtp0 {strnptr m} ((pfview, pfgc | p))
    end

  implement istring_slice_to_string (s, i_start, i_end) =
    strnptr2string (istring_slice_to_strnptr (s, i_start, i_end))

  implement istring_slice_to_strnptr_exn (s, i_start, i_end) =
    if (i_start <= i_end) * (i_end <= istring_length s) then
      istring_slice_to_strnptr (s, i_start, i_end)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement istring_slice_to_string_exn (s, i_start, i_end) =
    if (i_start <= i_end) * (i_end <= istring_length s) then
      istring_slice_to_string (s, i_start, i_end)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement istring_to_strnptr s =
    istring_slice_to_strnptr (s, i2sz 0, istring_length s)

  implement istring_to_string s =
    let
      prval () = lemma_istring_param s
    in
      strnptr2string (istring_to_strnptr s)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement append_string1_slice_to_istring {m} {n} {i_start, i_end}
                                            (s, str, i_start, i_end) =
    let
      prval () = lemma_istring_param s

      val num = i_end - i_start
      stadef num = i_end - i_start

      extern castfn make_view :
        {n : int} {p : addr} ptr p -<>
          (array_v (char, p, n), array_v (char, p, n) -<lin,prf> void | ptr p)
      val (pf, fpf | p) =
        make_view {num} (ptr_add<char> (string2ptr str, i_start))
      val s1 = immutable_vector_pushes<char> (pf | s, num, p)
      prval () = fpf pf
    in
      s1
    end

  implement append_string1_slice_to_istring_exn (s, str, i_start, i_end) =
    if (i_start <= i_end) * (i_end <= string_length str) then
      append_string1_slice_to_istring (s, str, i_start, i_end)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement string1_slice_to_istring (str, i_start, i_end) =
    append_string1_slice_to_istring (istring_nil, str, i_start, i_end)

  implement string0_slice_to_istring_exn (str, i_start, i_end) =
    append_string1_slice_to_istring_exn (istring_nil, g1ofg0 str,
                                         i_start, i_end)

  implement string1_slice_to_istring_exn (str, i_start, i_end) =
    if (i_start <= i_end) * (i_end <= string_length str) then
      append_string1_slice_to_istring (istring_nil, str, i_start, i_end)
    else
      $raise ImmutableVectorSubscriptExn ()

  implement append_string0_to_istring (s, str) =
    append_string1_to_istring (s, g1ofg0 str)

  implement append_string1_to_istring (s, str) =
    let
      prval () = lemma_string_param str
    in
      append_string1_slice_to_istring (s, str, i2sz 0, length str)
    end

  implement append_istring_to_istring (s0, s1) =
    let
      prval () = lemma_istring_param s0
      prval () = lemma_istring_param s1
    in
      immutable_vector_append<char> (s0, s1)
    end

  implement string0_to_istring str = string1_to_istring (g1ofg0 str)

  implement string1_to_istring str =
    let
      prval () = lemma_string_param str
    in
      append_string1_slice_to_istring (istring_nil, str, i2sz 0, length str)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement {}
  istring$strncmp (s, t, n) = strncmp_for_istrings (s, t, n)

  extern fn default_cmp_istring_and_string : (istring, string) -> int
  implement default_cmp_istring_and_string (s, t) =
    let
      val [m : int] s = $UN.cast {[m : int] istring m} s
      val [n : int] t = $UN.cast {[n : int] string n} t
      val m = istring_length s
      val n = string_length t
      prval () = lemma_string_param t

      fun loop {i : nat | i <= m} {j : nat | j <= n} .<m - i>.
               (i : size_t i, j : size_t j) :<cloref1> int =
        if (i < m) * (j < n) then
          let
            #define BUFFER_SIZE 1024
            var sbuf : @[char][BUFFER_SIZE]
            val i1 = min (m - i, i2sz BUFFER_SIZE)
            val j1 = min (n - j, i2sz BUFFER_SIZE)
            val [k : int] k =
              min (i1, j1) :
                [k : pos | k <= BUFFER_SIZE; k <= m - i; k <= n - j] size_t k
            prval @(pfleft_s, pfright_s) = array_v_split_at (view@ sbuf | k)
            val () = copy_chars (pfleft_s | s, i, k, addr@ sbuf)
            val (pft, consume_pft | tptr) =
              $UN.ptr_vtake {@[char][k]} (ptr_add<char> (ptrcast t, j))
            val cmp = istring$strncmp (sbuf, !tptr, k)
            prval () = array_v_uninitize pfleft_s
            prval () = view@ sbuf := array_v_unsplit (pfleft_s, pfright_s)
            prval () = consume_pft pft
          in
            if cmp = 0 then
              loop (i + k, j + k)
            else
              cmp
          end
        else
          case+ 0 of
            | _ when m < n => ~1
            | _ when m = n => 0
            | _ => 1
    in
      loop (i2sz 0, i2sz 0)
    end

  extern fn default_cmp_istring_and_istring : (istring, istring) -> int
  implement default_cmp_istring_and_istring (s, t) =
    let
      val [m : int] s = $UN.cast {[m : int] istring m} s
      val [n : int] t = $UN.cast {[n : int] istring n} t
      val m = istring_length s
      val n = istring_length t

      fun loop {i : nat | i <= m} {j : nat | j <= n} .<m - i>.
               (i : size_t i, j : size_t j) :<cloref1> int =
        if (i < m) * (j < n) then
          let
            #define BUFFER_SIZE 1024
            var sbuf : @[char][BUFFER_SIZE]
            var tbuf : @[char][BUFFER_SIZE]
            val i1 = min (m - i, i2sz BUFFER_SIZE)
            val j1 = min (n - j, i2sz BUFFER_SIZE)
            val k = min (i1, j1)
            prval @(pfleft_s, pfright_s) = array_v_split_at (view@ sbuf | k)
            prval @(pfleft_t, pfright_t) = array_v_split_at (view@ tbuf | k)
            val () = copy_chars (pfleft_s | s, i, k, addr@ sbuf)
            val () = copy_chars (pfleft_t | t, j, k, addr@ tbuf)
            val cmp = istring$strncmp (sbuf, tbuf, k)
            prval () = array_v_uninitize pfleft_s
            prval () = array_v_uninitize pfleft_t
            prval () = view@ sbuf := array_v_unsplit (pfleft_s, pfright_s)
            prval () = view@ tbuf := array_v_unsplit (pfleft_t, pfright_t)
          in
            if cmp = 0 then
              loop (i + k, j + k)
            else
              cmp
          end
        else
          case+ 0 of
            | _ when m < n => ~1
            | _ when m = n => 0
            | _ => 1
    in
      loop (i2sz 0, i2sz 0)
    end

  implement {}
  cmp_istring_and_string (s, t) =
    default_cmp_istring_and_string (s, t)

  implement {}
  cmp_string_and_istring (s, t) =
    ~(cmp_istring_and_string<> (t, s))

  implement {}
  cmp_istring_and_istring (s, t) =
    default_cmp_istring_and_istring (s, t)

  implement {} lt_istring_string (s, t) = (istring_cmp (s, t) < 0)
  implement {} lt_string_istring (s, t) = (istring_cmp (s, t) < 0)
  implement {} lt_istring_istring (s, t) = (istring_cmp (s, t) < 0)

  implement {} lte_istring_string (s, t) = (istring_cmp (s, t) <= 0)
  implement {} lte_string_istring (s, t) = (istring_cmp (s, t) <= 0)
  implement {} lte_istring_istring (s, t) = (istring_cmp (s, t) <= 0)

  implement {} gt_istring_string (s, t) = (istring_cmp (s, t) > 0)
  implement {} gt_string_istring (s, t) = (istring_cmp (s, t) > 0)
  implement {} gt_istring_istring (s, t) = (istring_cmp (s, t) > 0)

  implement {} gte_istring_string (s, t) = (istring_cmp (s, t) >= 0)
  implement {} gte_string_istring (s, t) = (istring_cmp (s, t) >= 0)
  implement {} gte_istring_istring (s, t) = (istring_cmp (s, t) >= 0)

  implement {} eq_istring_string (s, t) = (istring_cmp (s, t) = 0)
  implement {} eq_string_istring (s, t) = (istring_cmp (s, t) = 0)
  implement {} eq_istring_istring (s, t) = (istring_cmp (s, t) = 0)

  implement {} neq_istring_string (s, t) = (istring_cmp (s, t) != 0)
  implement {} neq_string_istring (s, t) = (istring_cmp (s, t) != 0)
  implement {} neq_istring_istring (s, t) = (istring_cmp (s, t) != 0)

  implement {} compare_istring_string (s, t) =
    clamp_to_Sgn (istring_cmp (s, t))
  implement {} compare_string_istring (s, t) =
    clamp_to_Sgn (istring_cmp (s, t))
  implement {} compare_istring_istring (s, t) =
    clamp_to_Sgn (istring_cmp (s, t))

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement istring_murmur3_32 (s, seed) =
    immutable_vector_murmur3_32<char> (s, seed)

  implement istring_murmur3_32_exn (s, seed) =
    immutable_vector_murmur3_32_exn<char> (s, seed)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement {}
  make_istring_multibyte_nil () =
    let
      val nil = istring_nil
      prval () = lemma_istring_param nil      
    in
      (istring_nil_is_multibyte nil | nil)
    end

  implement {}
  make_istring_unicode_nil () =
    let
      val nil = istring_nil
      prval () = lemma_istring_param nil      
    in
      (istring_nil_is_unicode nil | nil)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement istring_is_multibyte {n} s =
    let
      prval () = lemma_istring_param s

      macdef get (s, i) = c2i (istring_get (,(s), ,(i)))

      fun check {i : nat} {i <= n} .<n - i>.
                (i : size_t i) :<cloref1> bool =
        let
          val n = istring_length s
        in
          if i = n then
            true
          else
            case+ extended_utf8_character_length (get (s, i)) of
              | 1 => check (succ i)
              | 2 =>
                if n - i < i2sz 2 then
                  false
                else if is_valid_utf8_continuation_byte (get (s, succ i)) then
                  check (i + i2sz 2)
                else
                  false
              | 3 =>
                if n - i < i2sz 3 then
                  false
                else
                  let
                    var a : @[char][2]
                    val () =
                      immutable_vector_gets_eff<char> (s, succ i, i2sz 2, a)
                  in
                    if (is_valid_utf8_continuation_byte (c2i a[0])
                        && is_valid_utf8_continuation_byte (c2i a[1])) then
                      check (i + i2sz 3)
                    else
                      false
                  end
              | 4 =>
                if n - i < i2sz 4 then
                  false
                else
                  let
                    var a : @[char][3]
                    val () =
                      immutable_vector_gets_eff<char> (s, succ i, i2sz 3, a)
                  in
                    if (is_valid_utf8_continuation_byte (c2i a[0])
                        && is_valid_utf8_continuation_byte (c2i a[1])
                        && is_valid_utf8_continuation_byte (c2i a[2])) then
                      check (i + i2sz 4)
                    else
                      false
                  end
              | 5 =>
                if n - i < i2sz 5 then
                  false
                else
                  let
                    var a : @[char][4]
                    val () =
                      immutable_vector_gets_eff<char> (s, succ i, i2sz 4, a)
                  in
                    if (is_valid_utf8_continuation_byte (c2i a[0])
                        && is_valid_utf8_continuation_byte (c2i a[1])
                        && is_valid_utf8_continuation_byte (c2i a[2])
                        && is_valid_utf8_continuation_byte (c2i a[3])) then
                      check (i + i2sz 5)
                    else
                      false
                  end
              | 6 =>
                if n - i < i2sz 6 then
                  false
                else
                  let
                    var a : @[char][5]
                    val () =
                      immutable_vector_gets_eff<char> (s, succ i, i2sz 5, a)
                  in
                    if (is_valid_utf8_continuation_byte (c2i a[0])
                        && is_valid_utf8_continuation_byte (c2i a[1])
                        && is_valid_utf8_continuation_byte (c2i a[2])
                        && is_valid_utf8_continuation_byte (c2i a[3])
                        && is_valid_utf8_continuation_byte (c2i a[4])) then
                      check (i + i2sz 6)
                    else
                      false
                  end
              | _ => false
        end

      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_MULTIBYTE q
    in
      if check (i2sz 0) then
        (ISTRING_MULTIBYTE_valid (make_pf s) | true)
      else
        (ISTRING_MULTIBYTE_invalid () | false)
    end

  implement istring_is_unicode {n} s =
    let
      prval () = lemma_istring_param s

      macdef get (s, i) = c2i (istring_get (,(s), ,(i)))

      fun check {i : nat} {i <= n} .<n - i>.
                (i : size_t i) :<cloref1> bool =
        let
          val n = istring_length s
        in
          if i = n then
            true
          else
            let
              val c0 = get (s, i)
            in
              case+ utf8_character_length c0 of
                | 1 => check (succ i)
                | 2 =>
                  if n - i < i2sz 2 then
                    false
                  else
                    let
                      val c1 = get (s, i + i2sz 1)
                    in
                      if not (is_valid_utf8_continuation_byte c1) then
                        false
                      else if not (is_valid_utf8_character_2byte (c0, c1)).1 then
                        false
                      else
                        check (i + i2sz 2)
                    end
                | 3 =>
                  if n - i < i2sz 3 then
                    false
                  else
                    let
                      val c1 = get (s, i + i2sz 1)
                      val c2 = get (s, i + i2sz 2)
                    in
                      if not (is_valid_utf8_continuation_byte c1) then
                        false
                      else if not (is_valid_utf8_continuation_byte c2) then
                        false
                      else if not (is_valid_utf8_character_3byte (c0, c1, c2)).1 then
                        false
                      else
                        check (i + i2sz 3)
                    end
                | 4 =>
                  if n - i < i2sz 4 then
                    false
                  else
                    let
                      val c1 = get (s, i + i2sz 1)
                      val c2 = get (s, i + i2sz 2)
                      val c3 = get (s, i + i2sz 3)
                    in
                      if not (is_valid_utf8_continuation_byte c1) then
                        false
                      else if not (is_valid_utf8_continuation_byte c2) then
                        false
                      else if not (is_valid_utf8_continuation_byte c3) then
                        false
                      else if not (is_valid_utf8_character_4byte (c0, c1, c2, c3)).1 then
                        false
                      else
                        check (i + i2sz 4)
                    end
                | _ => false
            end
        end

      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_UNICODE q
    in
      if check (i2sz 0) then
        (ISTRING_UNICODE_valid (make_pf s) | true)
      else
        (ISTRING_UNICODE_invalid () | false)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  absprop MULTIBYTE_SEQUENCE_LENGTH (i : int, p : addr, length : int)

  extern praxi
  lemma_multibyte_sequence_length :
    {n : int} {i : int} {p : addr} {length : int}
    (MULTIBYTE_SEQUENCE_LENGTH (i, p, length) | istring (n, p)) -<prf>
      [i + length <= n] void

  fn {}
  get_multibyte_c0 {n : int} {i : nat | i < n} {p : addr}
                   (_pfi : ISTRING_MULTIBYTE_INDEX (i, p) |
                    s : istring (n, p),
                    i : size_t i) :
      [c0 : int | 1 <= extended_utf8_character_length c0]
      (MULTIBYTE_SEQUENCE_LENGTH (i, p, extended_utf8_character_length c0) |
       int c0) =
    let
      val c0 = c2i (istring_get (s, i))
      val [c0 : int] c0 =
        $UN.cast {[c0 : int | 1 <= extended_utf8_character_length c0] int c0} c0
      extern praxi make_pf :
        {i : int; p : addr; c0 : int} () -<prf>
          MULTIBYTE_SEQUENCE_LENGTH (i, p, extended_utf8_character_length c0)
    in
      (make_pf {i, p, c0} () | c0)
    end

  fn {}
  get_multibyte_c1plus {n : int} {length : int}
                       {i : nat | i + length <= n}
                       {j : int | i < j; j < i + length}
                       {p : addr}
                       (_pfi : MULTIBYTE_SEQUENCE_LENGTH (i, p, length) |
                        s : istring (n, p),
                        j : size_t j) :
      [c : int | is_valid_utf8_continuation_byte c] int c =
    let
      val c = c2i (istring_get (s, j))
      val c = $UN.cast {[c : int | is_valid_utf8_continuation_byte c] int c} c
    in
      c
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  absprop UNICODE_SEQUENCE_LENGTH (i : int, p : addr, length : int)

  extern praxi
  lemma_unicode_sequence_length :
    {n : int} {i : int} {p : addr} {length : int}
    (UNICODE_SEQUENCE_LENGTH (i, p, length) | istring (n, p)) -<prf>
      [i + length <= n] void

  fn {}
  get_unicode_c0 {n : int} {i : nat | i < n} {p : addr}
                   (_pfi : ISTRING_UNICODE_INDEX (i, p) |
                    s : istring (n, p),
                    i : size_t i) :
      [c0 : int | 1 <= utf8_character_length c0]
      (UNICODE_SEQUENCE_LENGTH (i, p, utf8_character_length c0) | int c0) =
    let
      val c0 = c2i (istring_get (s, i))
      val [c0 : int] c0 =
        $UN.cast {[c0 : int | 1 <= utf8_character_length c0] int c0} c0
      extern praxi make_pf :
        {i : int; p : addr; c0 : int} () -<prf>
          UNICODE_SEQUENCE_LENGTH (i, p, utf8_character_length c0)
    in
      (make_pf {i, p, c0} () | c0)
    end

  fn {}
  get_unicode_c1plus {n : int} {length : int}
                       {i : nat | i + length <= n}
                       {j : int | i < j; j < i + length}
                       {p : addr}
                       (_pfseq : UNICODE_SEQUENCE_LENGTH (i, p, length) |
                        s : istring (n, p),
                        j : size_t j) :
      [c : int | is_valid_utf8_continuation_byte c] int c =
    let
      val c = c2i (istring_get (s, j))
      val c = $UN.cast {[c : int | is_valid_utf8_continuation_byte c] int c} c
    in
      c
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement {}
  istring_multibyte_first (pfs | s) =
    let
      extern praxi make_pf :
        {i : int} {p : addr}
        ISTRING_IS_MULTIBYTE p -<prf> ISTRING_MULTIBYTE_INDEX (i, p)
    in
      (make_pf {0} pfs | i2sz 0)
    end

  implement {}
  istring_multibyte_after_last {n} (pfs | s) =
    let
      extern praxi make_pf :
        {i : int} {p : addr}
        ISTRING_IS_MULTIBYTE p -<prf> ISTRING_MULTIBYTE_INDEX (i, p)
    in
      (make_pf {n} pfs | istring_length s)
    end

  implement {}
  istring_multibyte_last (pfs | s) =
    let
      val (pfj | j) = istring_multibyte_after_last (pfs | s)
    in
      istring_multibyte_prev (pfj | s, j)
    end

  implement {}
  istring_multibyte_last_unsafe_exn s =
    istring_multibyte_prev_unsafe_exn (s, istring_length s)

  implement
  istring_multibyte_next {..} {i} (pfi | s, i) =
    let
      prval () = lemma_istring_param s

      val (pfseq | c0) = get_multibyte_c0 (pfi | s, i)
      prval () = lemma_multibyte_sequence_length (pfseq | s)
      val [incr : int] incr = extended_utf8_character_length c0
      val j = i + incr

      extern praxi make_pf :
        {incr : int} {i : int} {p : addr}
        ISTRING_MULTIBYTE_INDEX (i, p) -<prf>
          ISTRING_MULTIBYTE_INDEX (i + incr, p)
    in
      (make_pf {incr} pfi | j)
    end

  implement
  istring_multibyte_next_unsafe_exn (s, i) =
    let
      val n = istring_length s
      prval () = lemma_istring_param s
      val () = assertloc (i < n)

      val c0 = c2i (istring_get (s, i))
      val incr = extended_utf8_character_length c0
    in
      if incr < 1 then
        $raise IstringMultibyteExn ()
      else if n - i < i2sz incr then
        $raise IstringMultibyteExn ()
      else
        i + i2sz incr
    end

  implement
  istring_multibyte_prev {..} {i} (pfi | s, i) =
    let
      prval () = lemma_istring_param s

      macdef get (s, i) = c2i (istring_get (,(s), ,(i)))

      fun loop {k : nat | k < i} (k : size_t k) :<cloref1>
          [j : int | 1 <= i - j; i - j <= 6; j <= i] size_t j =
        if is_valid_utf8_continuation_byte (get (s, k)) then
          let
            val () = assertloc (k != i2sz 0)
          in
            loop (pred k)
          end
        else
          let
            val () = assertloc (i - k <= 6)
          in
            k
          end

      val [j : int] j = loop (pred i)

      extern praxi make_pf :
        {incr : int} {i : int} {p : addr}
        ISTRING_MULTIBYTE_INDEX (i, p) -<prf>
          ISTRING_MULTIBYTE_INDEX (i + incr, p)
    in
      (make_pf {j - i} pfi | j)
    end

  implement
  istring_multibyte_prev_unsafe_exn {..} {i} (s, i) =
    let
      prval () = lemma_istring_param s

      macdef get (s, i) = c2i (istring_get (,(s), ,(i)))

      val n = istring_length s
      val () = assertloc (i2sz 0 < i)
      val () = assertloc (i <= n)

      fun loop {k : nat | k < i} (k : size_t k) :<cloref1>
          [j : int | 1 <= i - j; i - j <= 6; j <= i] size_t j =
        if is_valid_utf8_continuation_byte (get (s, k)) then
          if k != i2sz 0 then
            loop (pred k)
          else
            $raise IstringMultibyteExn ()
        else
          if i - k <= 6 then
            k
          else
            $raise IstringMultibyteExn ()
    in
      loop (pred i)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement {}
  istring_unicode_first (pfs | s) =
    let
      extern praxi make_pf :
        {i : int} {p : addr}
        ISTRING_IS_UNICODE p -<prf> ISTRING_UNICODE_INDEX (i, p)
    in
      (make_pf {0} pfs | i2sz 0)
    end

  implement {}
  istring_unicode_after_last {n} (pfs | s) =
    let
      extern praxi make_pf :
        {i : int} {p : addr}
        ISTRING_IS_UNICODE p -<prf> ISTRING_UNICODE_INDEX (i, p)
    in
      (make_pf {n} pfs | istring_length s)
    end

  implement {}
  istring_unicode_last (pfs | s) =
    let
      val (pfj | j) = istring_unicode_after_last (pfs | s)
    in
      istring_unicode_prev (pfj | s, j)
    end

  implement {}
  istring_unicode_last_unsafe_exn s =
    istring_unicode_prev_unsafe_exn (s, istring_length s)

  implement
  istring_unicode_next {..} {i} (pfi | s, i) =
    let
      prval () = lemma_istring_param s

      val (pfseq | c0) = get_unicode_c0 (pfi | s, i)
      prval () = lemma_unicode_sequence_length (pfseq | s)
      val [incr : int] incr = utf8_character_length c0
      val j = i + incr

      extern praxi make_pf :
        {incr : int} {i : int} {p : addr}
        ISTRING_UNICODE_INDEX (i, p) -<prf>
          ISTRING_UNICODE_INDEX (i + incr, p)
    in
      (make_pf {incr} pfi | j)
    end

  implement
  istring_unicode_next_unsafe_exn (s, i) =
    let
      val n = istring_length s
      prval () = lemma_istring_param s
      val () = assertloc (i < n)

      val c0 = c2i (istring_get (s, i))
      val incr = utf8_character_length c0
    in
      if incr < 1 then
        $raise IstringUnicodeExn ()
      else if n - i < i2sz incr then
        $raise IstringUnicodeExn ()
      else
        i + i2sz incr
    end

  implement
  istring_unicode_prev {..} {i} (pfi | s, i) =
    let
      prval () = lemma_istring_param s

      macdef get (s, i) = c2i (istring_get (,(s), ,(i)))

      fun loop {k : nat | k < i} (k : size_t k) :<cloref1>
          [j : int | 1 <= i - j; i - j <= 4; j <= i] size_t j =
        if is_valid_utf8_continuation_byte (get (s, k)) then
          let
            val () = assertloc (k != i2sz 0)
          in
            loop (pred k)
          end
        else
          let
            val () = assertloc (i - k <= 4)
          in
            k
          end

      val [j : int] j = loop (pred i)

      extern praxi make_pf :
        {incr : int} {i : int} {p : addr}
        ISTRING_UNICODE_INDEX (i, p) -<prf>
          ISTRING_UNICODE_INDEX (i + incr, p)
    in
      (make_pf {j - i} pfi | j)
    end

  implement
  istring_unicode_prev_unsafe_exn {..} {i} (s, i) =
    let
      prval () = lemma_istring_param s

      macdef get (s, i) = c2i (istring_get (,(s), ,(i)))

      val n = istring_length s
      val () = assertloc (i2sz 0 < i)
      val () = assertloc (i <= n)

      fun loop {k : nat | k < i} (k : size_t k) :<cloref1>
          [j : int | 1 <= i - j; i - j <= 4; j <= i] size_t j =
        if is_valid_utf8_continuation_byte (get (s, k)) then
          if k != i2sz 0 then
            loop (pred k)
          else
            $raise IstringUnicodeExn ()
        else
          if i - k <= 4 then
            k
          else
            $raise IstringUnicodeExn ()
    in
      loop (pred i)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement istring_multibyte_get (pfi | s, i) =
    let
      prval () = lemma_istring_param s

      val (pfseq | c0) = get_multibyte_c0 (pfi | s, i)
      prval () = lemma_multibyte_sequence_length (pfseq | s)
    in
      case+ extended_utf8_character_length c0 of
        | 1 =>
          let
            val (pf | u) = decode_extended_utf8_1byte c0
            prval EXTENDED_UTF8_CHAR_1byte () = pf
          in
            u
          end
        | 2 =>
          let
            val c1 = get_multibyte_c1plus (pfseq | s, i + i2sz 1)
            val (pf | u) = decode_extended_utf8_2byte (c0, c1)
            prval EXTENDED_UTF8_CHAR_2byte () = pf
          in
            u
          end
        | 3 =>
          let
            val c1 = get_multibyte_c1plus (pfseq | s, i + i2sz 1)
            val c2 = get_multibyte_c1plus (pfseq | s, i + i2sz 2)
            val (pf | u) = decode_extended_utf8_3byte (c0, c1, c2)
            prval EXTENDED_UTF8_CHAR_3byte () = pf
          in
            u
          end
        | 4 =>
          let
            val c1 = get_multibyte_c1plus (pfseq | s, i + i2sz 1)
            val c2 = get_multibyte_c1plus (pfseq | s, i + i2sz 2)
            val c3 = get_multibyte_c1plus (pfseq | s, i + i2sz 3)
            val (pf | u) = decode_extended_utf8_4byte (c0, c1, c2, c3)
            prval EXTENDED_UTF8_CHAR_4byte () = pf
          in
            u
          end
        | 5 =>
          let
            val c1 = get_multibyte_c1plus (pfseq | s, i + i2sz 1)
            val c2 = get_multibyte_c1plus (pfseq | s, i + i2sz 2)
            val c3 = get_multibyte_c1plus (pfseq | s, i + i2sz 3)
            val c4 = get_multibyte_c1plus (pfseq | s, i + i2sz 4)
            val (pf | u) = decode_extended_utf8_5byte (c0, c1, c2, c3, c4)
            prval EXTENDED_UTF8_CHAR_5byte () = pf
          in
            u
          end
        | 6 =>
          let
            val c1 = get_multibyte_c1plus (pfseq | s, i + i2sz 1)
            val c2 = get_multibyte_c1plus (pfseq | s, i + i2sz 2)
            val c3 = get_multibyte_c1plus (pfseq | s, i + i2sz 3)
            val c4 = get_multibyte_c1plus (pfseq | s, i + i2sz 4)
            val c5 = get_multibyte_c1plus (pfseq | s, i + i2sz 5)
            val (pf | u) = decode_extended_utf8_6byte (c0, c1, c2, c3, c4, c5)
            prval EXTENDED_UTF8_CHAR_6byte () = pf
          in
            u
          end
    end

  implement istring_unicode_get (pfi | s, i) =
    let
      prval () = lemma_istring_param s

      val [c0 : int] (pfseq | c0) = get_unicode_c0 (pfi | s, i)
      prval () = lemma_unicode_sequence_length (pfseq | s)
    in
      case+ utf8_character_length c0 of
        | 1 => decode_utf8_1byte c0
        | 2 =>
          let
            val [c1 : int] c1 = get_unicode_c1plus (pfseq | s, i + i2sz 1)
            extern praxi _valid :
              () -<prf> [u : int] UTF8_CHAR_VALID (2, u, c0, c1, ~1, ~1)
            prval () = utf8_character_2byte_valid_bytes (_valid ())
          in
            decode_utf8_2byte (c0, c1)
          end
        | 3 =>
          let
            val [c1 : int] c1 = get_unicode_c1plus (pfseq | s, i + i2sz 1)
            val [c2 : int] c2 = get_unicode_c1plus (pfseq | s, i + i2sz 2)
            extern praxi _valid :
              () -<prf> [u : int] UTF8_CHAR_VALID (3, u, c0, c1, c2, ~1)
            prval () = utf8_character_3byte_valid_bytes (_valid ())
          in
            decode_utf8_3byte (c0, c1, c2)
          end
        | 4 =>
          let
            val [c1 : int] c1 = get_unicode_c1plus (pfseq | s, i + i2sz 1)
            val [c2 : int] c2 = get_unicode_c1plus (pfseq | s, i + i2sz 2)
            val [c3 : int] c3 = get_unicode_c1plus (pfseq | s, i + i2sz 3)
            extern praxi _valid :
              () -<prf> [u : int] UTF8_CHAR_VALID (4, u, c0, c1, c2, c3)
            prval () = utf8_character_4byte_valid_bytes (_valid ())
          in
            decode_utf8_4byte (c0, c1, c2, c3)
          end
    end

  implement istring_multibyte_get_unsafe_exn (s, i) =
    let
      prval () = lemma_istring_param s

      macdef get (s, i) = c2i (istring_get (,(s), ,(i)))

      val i = g1ofg0 i
      val n = istring_length s
    in
      if n <= i then
        $raise ImmutableVectorSubscriptExn ()
      else
        let
          val c0 = get (s, i)
        in
          case+ extended_utf8_character_length c0 of
            | 1 =>
              let
                val (pf | u) = decode_extended_utf8_1byte c0
                prval EXTENDED_UTF8_CHAR_1byte () = pf
              in
                @(u, i2sz 1)
              end
            | 2 =>
              if n - i < i2sz 2 then
                $raise IstringMultibyteExn ()
              else
                let
                  val c1 = get (s, i + i2sz 1)
                in
                  if is_valid_utf8_continuation_byte c1 then
                    let
                      val (pf | u) = decode_extended_utf8_2byte (c0, c1)
                      prval EXTENDED_UTF8_CHAR_2byte () = pf
                    in
                      @(u, i2sz 2)
                    end
                  else
                    $raise IstringMultibyteExn ()
                end
            | 3 =>
              if n - i < i2sz 3 then
                $raise IstringMultibyteExn ()
              else
                let
                  val c1 = get (s, i + i2sz 1)
                  val c2 = get (s, i + i2sz 2)
                in
                  if (is_valid_utf8_continuation_byte c1) *
                     (is_valid_utf8_continuation_byte c2) then
                    let
                      val (pf | u) = decode_extended_utf8_3byte (c0, c1, c2)
                      prval EXTENDED_UTF8_CHAR_3byte () = pf
                    in
                      @(u, i2sz 3)
                    end
                  else
                    $raise IstringMultibyteExn ()
                end
            | 4 =>
              if n - i < i2sz 4 then
                $raise IstringMultibyteExn ()
              else
                let
                  val c1 = get (s, i + i2sz 1)
                  val c2 = get (s, i + i2sz 2)
                  val c3 = get (s, i + i2sz 3)
                in
                  if (is_valid_utf8_continuation_byte c1) *
                     (is_valid_utf8_continuation_byte c2) *
                     (is_valid_utf8_continuation_byte c3) then
                    let
                      val (pf | u) = decode_extended_utf8_4byte (c0, c1, c2, c3)
                      prval EXTENDED_UTF8_CHAR_4byte () = pf
                    in
                      @(u, i2sz 4)
                    end
                  else
                    $raise IstringMultibyteExn ()
                end
            | 5 =>
              if n - i < i2sz 5 then
                $raise IstringMultibyteExn ()
              else
                let
                  val c1 = get (s, i + i2sz 1)
                  val c2 = get (s, i + i2sz 2)
                  val c3 = get (s, i + i2sz 3)
                  val c4 = get (s, i + i2sz 4)
                in
                  if (is_valid_utf8_continuation_byte c1) *
                     (is_valid_utf8_continuation_byte c2) *
                     (is_valid_utf8_continuation_byte c3) *
                     (is_valid_utf8_continuation_byte c4) then
                    let
                      val (pf | u) =
                        decode_extended_utf8_5byte (c0, c1, c2, c3, c4)
                      prval EXTENDED_UTF8_CHAR_5byte () = pf
                    in
                      @(u, i2sz 5)
                    end
                  else
                    $raise IstringMultibyteExn ()
                end
            | 6 =>
              if n - i < i2sz 6 then
                $raise IstringMultibyteExn ()
              else
                let
                  val c1 = get (s, i + i2sz 1)
                  val c2 = get (s, i + i2sz 2)
                  val c3 = get (s, i + i2sz 3)
                  val c4 = get (s, i + i2sz 4)
                  val c5 = get (s, i + i2sz 5)
                in
                  if (is_valid_utf8_continuation_byte c1) *
                     (is_valid_utf8_continuation_byte c2) *
                     (is_valid_utf8_continuation_byte c3) *
                     (is_valid_utf8_continuation_byte c4) *
                     (is_valid_utf8_continuation_byte c5) then
                    let
                      val (pf | u) =
                        decode_extended_utf8_6byte (c0, c1, c2, c3, c4, c5)
                      prval EXTENDED_UTF8_CHAR_6byte () = pf
                    in
                      @(u, i2sz 6)
                    end
                  else
                    $raise IstringMultibyteExn ()
                end
            | _ => $raise IstringMultibyteExn ()
        end
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement istring_multibyte_push (_pf | s, u) =
    let
      val t = istring_multibyte_push_unsafe (s, u)
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_MULTIBYTE q
    in
      (make_pf t | t)
    end

  implement istring_multibyte_push_exn (_pf | s, u) =
    let
      val t = istring_multibyte_push_unsafe_exn (s, u)
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_MULTIBYTE q
    in
      (make_pf t | t)
    end

  implement istring_multibyte_push_unsafe (s, u) =
    let
      macdef i2c (c) = int2char0 (g0ofg1 ,(c))
      val @(pf | n, c0, c1, c2, c3, c4, c5) = encode_extended_utf8_character u
      prval () = lemma_extended_utf8_shortest_length pf
    in
      case+ n of
        | 1 => immutable_vector_push<char> (s, i2c c0)
        | _ =>
          let
            var a = @[char](i2c c0, i2c c1, i2c c2, i2c c3, i2c c4, i2c c5)
          in
            immutable_vector_pushes<char> (s, i2sz n, a)
          end
    end

  implement istring_multibyte_push_unsafe_exn (s, u) =
    if (0 <= u) * (u <= 0x7FFFFFFF) then
      istring_multibyte_push_unsafe (s, u)
    else
      $raise IstringMultibyteExn ()

  implement istring_unicode_push_unsafe (s, u) =
    let
      macdef i2c (c) = int2char0 (g0ofg1 ,(c))
      val @(pf_valid | n, c0, c1, c2, c3) = encode_utf8_character u
      prval pf_shortest = utf8_char_valid_implies_shortest pf_valid
      prval () = lemma_extended_utf8_shortest_length pf_shortest
    in
      case+ n of
        | 1 => immutable_vector_push<char> (s, i2c c0)
        | 2 =>
          let
            var a = @[char](i2c c0, i2c c1)
          in
            immutable_vector_pushes<char> (s, i2sz 2, a)
          end
        | 3 =>
          let
            var a = @[char](i2c c0, i2c c1, i2c c2)
          in
            immutable_vector_pushes<char> (s, i2sz 3, a)
          end
        | 4 =>
          let
            var a = @[char](i2c c0, i2c c1, i2c c2, i2c c3)
          in
            immutable_vector_pushes<char> (s, i2sz 4, a)
          end
    end

  implement istring_unicode_push (_pf | s, u) =
    let
      val t = istring_unicode_push_unsafe (s, u)
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_UNICODE q
    in
      (make_pf t | t)
    end

  implement istring_unicode_push_exn (_pf | s, u) =
    let
      val t = istring_unicode_push_unsafe_exn (s, u)
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_UNICODE q
    in
      (make_pf t | t)
    end

  implement istring_unicode_push_unsafe_exn (s, u) =
    if is_valid_unicode_code_point u then
      istring_unicode_push_unsafe (s, u)
    else
      $raise IstringUnicodeExn ()

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  fun
  length_after_pop {n : int} {n1 : nat | n1 <= n} .<n1>.
                   (s : istring n, n1 : size_t n1) :
      [n2 : nat | n2 <= max (0, n1 - 1)] size_t n2 =
    if n1 = i2sz 0 then
      n1
    else
      let
        val n2 = pred n1
        val c = g1ofg0 (char2int0 (istring_get (s, n2)))
      in
        if is_valid_utf8_continuation_byte c then
          length_after_pop (s, n2)
        else
          n2
      end

  fn
  length_after_pops {num : nat} {n : int} {n1 : nat | n1 <= n}
                    (s : istring n, num : size_t num, n1 : size_t n1) :
      [n2 : nat | n2 <= max (0, n1 - num)] size_t n2 =
    let
      fun loop {num1 : nat}
               {n11 : nat | n11 <= n; n11 <= max (0, n1 - (num - num1))}
               .<num1>.
               (num1 : size_t num1, n11 : size_t n11) :<cloref1>
          [n2 : nat | n2 <= max (0, n1 - num)] size_t n2 =
        if (num1 = i2sz 0) + (n11 = i2sz 0) then
          n11
        else
          loop (pred num1, length_after_pop (s, n11))
    in
      loop (num, n1)
    end

  implement
  istring_multibyte_pop (pf | s) =
    let
      val t = istring_multibyte_pop_unsafe s
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_MULTIBYTE q
    in
      (make_pf t | t)
    end

  implement
  istring_multibyte_pops (pf | s, num) =
    let
      val t = istring_multibyte_pops_unsafe (s, num)
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_MULTIBYTE q
    in
      (make_pf t | t)
    end

  implement
  istring_unicode_pop (pf | s) =
    let
      val t = istring_multibyte_pop_unsafe s
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_UNICODE q
    in
      (make_pf t | t)
    end

  implement
  istring_unicode_pops (pf | s, num) =
    let
      val t = istring_multibyte_pops_unsafe (s, num)
      extern praxi make_pf :
        {n : int} {q : addr} istring (n, q) -<prf> ISTRING_IS_UNICODE q
    in
      (make_pf t | t)
    end

  implement
  istring_multibyte_pop_unsafe s =
    let
      val n = istring_length s
      val n2 = length_after_pop (s, n)
    in
      istring_pops (s, n - n2)
    end

  implement
  istring_multibyte_pops_unsafe (s, num) =
    let
      val n = istring_length s
      val n2 = length_after_pops (s, num, n)
    in
      istring_pops (s, n - n2)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

end // local

//--------------------------------------------------------------------
