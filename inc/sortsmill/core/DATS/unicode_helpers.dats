// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.unicode_helpers"
#define ATS_EXTERN_PREFIX "_smcore_unicode_helpers__"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload "sortsmill/core/SATS/unicode_helpers.sats"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Integer division by 64 is equivalent to shifting right
// by 6 bits.

extern prfn _shift_right_twelve :
  {x, y, z : nat | y == (x \ndiv 64);
                   z == (y \ndiv 64)}
  (int x, int y, int z) -<prf> [z == (x \ndiv (64 * 64))] void

primplement _shift_right_twelve (x, y, z) =
  ()

extern prfn _shift_right_eighteen :
  {x, y, z, u : nat | y == (x \ndiv 64);
                      z == (y \ndiv 64);
                      u == (z \ndiv 64)}
  (int x, int y, int z, int u) -<prf> [u == (x \ndiv (64 * 64 * 64))] void

primplement _shift_right_eighteen (x, y, z, u) = ()


extern prfn _shift_right_twenty_four :
  {x, y, z, u, v : nat | y == (x \ndiv 64);
                         z == (y \ndiv 64);
                         u == (z \ndiv 64);
                         v == (u \ndiv 64)}
  (int x, int y, int z, int u, int v) -<prf>
    [v == (x \ndiv (64 * 64 * 64 * 64))] void

primplement _shift_right_twenty_four (x, y, z, u, v) = ()

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

implement {}
is_valid_unicode_code_point u =
  ((0x0 <= u) * (u < 0xD800)) + ((0xE000 <= u) * (u <= 0x10FFFF))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

primplement
extended_utf8_char_length_relation_to_length () = ()

primplement
extended_utf8_char_length_to_length_relation () = ()

primplement
utf8_char_length_relation_to_length () = ()

primplement
utf8_char_length_to_length_relation () = ()

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

implement {}
is_valid_utf8_continuation_byte c =
  (0x80 <= c) * (c <= 0xBF)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

primplement
decode_extended_utf8_istot {n} {c0, c1, c2, c3, c4, c5} () =
  sif n == 1 then
    let
      prfn
      make_pf {u : int | u == extended_utf8_char_1byte_decoding (c0)}
              () :<prf>
          EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5) =
        EXTENDED_UTF8_CHAR_1byte ()
      stadef u = extended_utf8_char_1byte_decoding (c0)
    in
      make_pf {u} ()
    end
  else sif n == 2 then
    let
      prfn
      make_pf {u : int | u == extended_utf8_char_2byte_decoding (c0, c1)}
              () :<prf>
          EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5) =
        EXTENDED_UTF8_CHAR_2byte ()
      stadef u = extended_utf8_char_2byte_decoding (c0, c1)
    in
      make_pf {u} ()
    end
  else sif n == 3 then
    let
      prfn
      make_pf {u : int | u == extended_utf8_char_3byte_decoding (c0, c1, c2)}
              () :<prf>
          EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5) =
        EXTENDED_UTF8_CHAR_3byte ()
      stadef u = extended_utf8_char_3byte_decoding (c0, c1, c2)
    in
      make_pf {u} ()
    end
  else sif n == 4 then
    let
      prfn
      make_pf {u : int |
               u == extended_utf8_char_4byte_decoding (c0, c1, c2, c3)}
              () :<prf>
          EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5) =
        EXTENDED_UTF8_CHAR_4byte ()
      stadef u = extended_utf8_char_4byte_decoding (c0, c1, c2, c3)
    in
      make_pf {u} ()
    end
  else sif n == 5 then
    let
      prfn
      make_pf {u : int |
               u == extended_utf8_char_5byte_decoding (c0, c1, c2, c3, c4)}
              () :<prf>
          EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5) =
        EXTENDED_UTF8_CHAR_5byte ()
      stadef u = extended_utf8_char_5byte_decoding (c0, c1, c2, c3, c4)
    in
      make_pf {u} ()
    end
  else
    let
      prfn
      make_pf {u : int |
               u == extended_utf8_char_6byte_decoding (c0, c1, c2, c3, c4, c5)}
              () :<prf>
          EXTENDED_UTF8_CHAR (n, u, c0, c1, c2, c3, c4, c5) =
        EXTENDED_UTF8_CHAR_6byte ()
      stadef u = extended_utf8_char_6byte_decoding (c0, c1, c2, c3, c4, c5)
    in
      make_pf {u} ()
    end

primplement
decode_extended_utf8_isfun {na} (pf_a, pf_b) =
  sif na == 1 then
    let
      prval EXTENDED_UTF8_CHAR_1byte () = pf_a
      prval EXTENDED_UTF8_CHAR_1byte () = pf_b
    in
    end
  else sif na == 2 then
    let
      prval EXTENDED_UTF8_CHAR_2byte () = pf_a
      prval EXTENDED_UTF8_CHAR_2byte () = pf_b
    in
    end
  else sif na == 3 then
    let
      prval EXTENDED_UTF8_CHAR_3byte () = pf_a
      prval EXTENDED_UTF8_CHAR_3byte () = pf_b
    in
    end
  else sif na == 4 then
    let
      prval EXTENDED_UTF8_CHAR_4byte () = pf_a
      prval EXTENDED_UTF8_CHAR_4byte () = pf_b
    in
    end
  else sif na == 5 then
    let
      prval EXTENDED_UTF8_CHAR_5byte () = pf_a
      prval EXTENDED_UTF8_CHAR_5byte () = pf_b
    in
    end
  else
    let
      prval EXTENDED_UTF8_CHAR_6byte () = pf_a
      prval EXTENDED_UTF8_CHAR_6byte () = pf_b
    in
    end


primplement
lemma_extended_utf8_char_length pf_char =
  case+ pf_char of
    | EXTENDED_UTF8_CHAR_1byte () => ()
    | EXTENDED_UTF8_CHAR_2byte () => ()
    | EXTENDED_UTF8_CHAR_3byte () => ()
    | EXTENDED_UTF8_CHAR_4byte () => ()
    | EXTENDED_UTF8_CHAR_5byte () => ()
    | EXTENDED_UTF8_CHAR_6byte () => ()

implement {}
decode_extended_utf8_1byte c0 =
  (EXTENDED_UTF8_CHAR_1byte () | c0)

implement {}
decode_extended_utf8_2byte (c0, c1) =
  let
    val u0 = c0 - 0xC0
    val u1 = c1 - 0x80
  in
    (EXTENDED_UTF8_CHAR_2byte () | 64 * u0 + u1)
  end

implement {}
decode_extended_utf8_3byte (c0, c1, c2) =
  let
    val u0 = c0 - 0xE0
    val u1 = c1 - 0x80
    val u2 = c2 - 0x80
  in
    (EXTENDED_UTF8_CHAR_3byte () | 64 * (64 * u0 + u1) + u2)
  end

implement {}
decode_extended_utf8_4byte (c0, c1, c2, c3) =
  let
    val u0 = c0 - 0xF0
    val u1 = c1 - 0x80
    val u2 = c2 - 0x80
    val u3 = c3 - 0x80
  in
    (EXTENDED_UTF8_CHAR_4byte () | 64 * (64 * (64 * u0 + u1) + u2) + u3)
  end

implement {}
decode_extended_utf8_5byte (c0, c1, c2, c3, c4) =
  let
    val u0 = c0 - 0xF8
    val u1 = c1 - 0x80
    val u2 = c2 - 0x80
    val u3 = c3 - 0x80
    val u4 = c4 - 0x80
  in
    (EXTENDED_UTF8_CHAR_5byte () |
     64 * (64 * (64 * (64 * u0 + u1) + u2) + u3) + u4)
  end

implement {}
decode_extended_utf8_6byte (c0, c1, c2, c3, c4, c5) =
  let
    val u0 = c0 - 0xFC
    val u1 = c1 - 0x80
    val u2 = c2 - 0x80
    val u3 = c3 - 0x80
    val u4 = c4 - 0x80
    val u5 = c5 - 0x80
  in
    (EXTENDED_UTF8_CHAR_6byte () |
     64 * (64 * (64 * (64 * (64 * u0 + u1) + u2) + u3) + u4) + u5)
  end

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

primplement
extended_utf8_shortest_is_char pf_shortest =
  case+ pf_shortest of
    | EXTENDED_UTF8_SHORTEST_1byte pf_char => pf_char
    | EXTENDED_UTF8_SHORTEST_2byte pf_char => pf_char
    | EXTENDED_UTF8_SHORTEST_3byte pf_char => pf_char
    | EXTENDED_UTF8_SHORTEST_4byte pf_char => pf_char
    | EXTENDED_UTF8_SHORTEST_5byte pf_char => pf_char
    | EXTENDED_UTF8_SHORTEST_6byte pf_char => pf_char

primplement
lemma_extended_utf8_shortest_length pf_shortest =
  case+ pf_shortest of
    | EXTENDED_UTF8_SHORTEST_1byte pf_char =>
      { prval EXTENDED_UTF8_CHAR_1byte () = pf_char }
    | EXTENDED_UTF8_SHORTEST_2byte pf_char =>
      { prval EXTENDED_UTF8_CHAR_2byte () = pf_char }
    | EXTENDED_UTF8_SHORTEST_3byte pf_char =>
      { prval EXTENDED_UTF8_CHAR_3byte () = pf_char }
    | EXTENDED_UTF8_SHORTEST_4byte pf_char =>
      { prval EXTENDED_UTF8_CHAR_4byte () = pf_char }
    | EXTENDED_UTF8_SHORTEST_5byte pf_char =>
      { prval EXTENDED_UTF8_CHAR_5byte () = pf_char }
    | EXTENDED_UTF8_SHORTEST_6byte pf_char =>
      { prval EXTENDED_UTF8_CHAR_6byte () = pf_char }

implement {}
encode_extended_utf8_character u =
  if u <= 0x7F then
    let
      val c0 = u
    in
      @(EXTENDED_UTF8_SHORTEST_1byte (EXTENDED_UTF8_CHAR_1byte ()) |
        1, c0, ~1, ~1, ~1, ~1, ~1)
    end
  else if u <= 0x7FF then
    let
      val c1 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c0 = 0xC0 + u1
    in
      @(EXTENDED_UTF8_SHORTEST_2byte (EXTENDED_UTF8_CHAR_2byte ()) |
        2, c0, c1, ~1, ~1, ~1, ~1)
    end
  else if u <= 0xFFFF then
    let
      val c2 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c1 = 0x80 + (u1 \nmod 64)
      val u2 = u1 \ndiv 64
      val c0 = 0xE0 + u2
    in
      @(EXTENDED_UTF8_SHORTEST_3byte (EXTENDED_UTF8_CHAR_3byte ()) |
        3, c0, c1, c2, ~1, ~1, ~1)
    end
  else if u <= 0x1FFFFF then
    let
      val c3 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c2 = 0x80 + (u1 \nmod 64)
      val u2 = u1 \ndiv 64
      val c1 = 0x80 + (u2 \nmod 64)
      val u3 = u2 \ndiv 64
      val c0 = 0xF0 + u3
      prval () = _shift_right_twelve (u, u1, u2)
    in
      @(EXTENDED_UTF8_SHORTEST_4byte (EXTENDED_UTF8_CHAR_4byte ()) |
        4, c0, c1, c2, c3, ~1, ~1)
    end
  else if u <= 0x3FFFFFF then
    let
      val c4 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c3 = 0x80 + (u1 \nmod 64)
      val u2 = u1 \ndiv 64
      val c2 = 0x80 + (u2 \nmod 64)
      val u3 = u2 \ndiv 64
      val c1 = 0x80 + (u3 \nmod 64)
      val u4 = u3 \ndiv 64
      val c0 = 0xF8 + u4
      prval () = _shift_right_twelve (u, u1, u2)
      prval () = _shift_right_eighteen (u, u1, u2, u3)
    in
      @(EXTENDED_UTF8_SHORTEST_5byte (EXTENDED_UTF8_CHAR_5byte ()) |
        5, c0, c1, c2, c3, c4, ~1)
    end
  else
    let
      val c5 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c4 = 0x80 + (u1 \nmod 64)
      val u2 = u1 \ndiv 64
      val c3 = 0x80 + (u2 \nmod 64)
      val u3 = u2 \ndiv 64
      val c2 = 0x80 + (u3 \nmod 64)
      val u4 = u3 \ndiv 64
      val c1 = 0x80 + (u4 \nmod 64)
      val u5 = u4 \ndiv 64
      val c0 = 0xFC + u5
      prval () = _shift_right_twelve (u, u1, u2)
      prval () = _shift_right_eighteen (u, u1, u2, u3)
      prval () = _shift_right_twenty_four (u, u1, u2, u3, u4)
    in
      @(EXTENDED_UTF8_SHORTEST_6byte (EXTENDED_UTF8_CHAR_6byte ()) |
        6, c0, c1, c2, c3, c4, c5)
    end

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

primplement
utf8_char_valid_implies_shortest pf_valid =
  case+ pf_valid of
    | UTF8_CHAR_valid (pf_shortest, _) => pf_shortest

primplement
lemma_valid_utf8_character_1byte {u, c0} () =
  let
    prfn
    make_pf {u : int | u == extended_utf8_char_1byte_decoding (c0)}
            () :<prf> EXTENDED_UTF8_CHAR (1, u, c0, ~1, ~1, ~1, ~1, ~1) =
      EXTENDED_UTF8_CHAR_1byte ()
    prval pf_char = make_pf {u} ()
    prval pf_shortest = EXTENDED_UTF8_SHORTEST_1byte pf_char
    prval pf_bytes = UTF8_CHAR_VALID_BYTES_1byte ()
    prval pf_valid = UTF8_CHAR_valid (pf_shortest, pf_bytes)
  in
    pf_valid
  end

primplement
lemma_valid_utf8_character_2byte {u, c0, c1} () =
  let
    prfn
    make_pf {u : int | u == extended_utf8_char_2byte_decoding (c0, c1)}
            () :<prf> EXTENDED_UTF8_CHAR (2, u, c0, c1, ~1, ~1, ~1, ~1) =
      EXTENDED_UTF8_CHAR_2byte ()
    prval pf_char = make_pf {u} ()
    prval pf_shortest = EXTENDED_UTF8_SHORTEST_2byte pf_char
    prval pf_bytes = UTF8_CHAR_VALID_BYTES_2byte ()
    prval pf_valid = UTF8_CHAR_valid (pf_shortest, pf_bytes)
  in
    pf_valid
  end

primplement
lemma_valid_utf8_character_3byte {u, c0, c1, c2} () =
  let
    prfn
    make_pf {u : int | u == extended_utf8_char_3byte_decoding (c0, c1, c2)}
            () :<prf> EXTENDED_UTF8_CHAR (3, u, c0, c1, c2, ~1, ~1, ~1) =
      EXTENDED_UTF8_CHAR_3byte ()
    prval pf_char = make_pf {u} ()
    prfn lemma_c1 () :<prf> [c1 == 0x80 + ((u \ndiv 64) \nmod 64)] void =
      {
        // Convert to Horner form.
        stadef u1 = 64 * (64 * (c0 - 0xE0) + (c1 - 0x80)) + (c2 - 0x80)
        prval EQINT () = eqint_make {u, u1} ()
      }
    prval () = lemma_c1 ()
    prval pf_shortest = EXTENDED_UTF8_SHORTEST_3byte pf_char
    prval pf_bytes = UTF8_CHAR_VALID_BYTES_3byte ()
    prval pf_valid = UTF8_CHAR_valid (pf_shortest, pf_bytes)
  in
    pf_valid
  end

primplement
lemma_valid_utf8_character_4byte {u, c0, c1, c2, c3} () =
  let
    prfn
    make_pf {u : int | u == extended_utf8_char_4byte_decoding (c0, c1, c2, c3)}
            () :<prf> EXTENDED_UTF8_CHAR (4, u, c0, c1, c2, c3, ~1, ~1) =
      EXTENDED_UTF8_CHAR_4byte ()
    prval pf_char = make_pf {u} ()
    prfn lemma_c1 () :<prf> [c1 == 0x80 + ((u \ndiv (64 * 64)) \nmod 64)] void =
      {
        prval EQINT () =
          eqint_make {(64 * 64 * (c0 - 0xE0)) \ndiv (64 * 64), c0 - 0xE0} ()
        prval pfd = divmod_istot {u \ndiv (64 * 64), 64} ()
        prval pfm = divmod_elim pfd
        prval () = mul_elim pfm
      }
    prval () = lemma_c1 ()
    prfn lemma_c2 () :<prf> [c2 == 0x80 + ((u \ndiv 64) \nmod 64)] void =
      {
        // Convert to Horner form.
        stadef u1 = 64 * (64 * (64 * (c0 - 0xF0) + (c1 - 0x80)) + (c2 - 0x80))
                     + (c3 - 0x80)
        prval EQINT () = eqint_make {u, u1} ()
      }
    prval () = lemma_c2 ()
    prval pf_shortest = EXTENDED_UTF8_SHORTEST_4byte pf_char
    prval pf_bytes = UTF8_CHAR_VALID_BYTES_4byte ()
    prval pf_valid = UTF8_CHAR_valid (pf_shortest, pf_bytes)
  in
    pf_valid
  end

primplement
utf8_character_1byte_valid_bytes pf_valid = ()

primplement
utf8_character_2byte_valid_bytes pf_valid =
  {
    prval UTF8_CHAR_valid (_, pf_bytes) = pf_valid
    prval UTF8_CHAR_VALID_BYTES_2byte () = pf_bytes
  }

primplement
utf8_character_3byte_valid_bytes pf_valid =
  {
    prval UTF8_CHAR_valid (_, pf_bytes) = pf_valid
    prval UTF8_CHAR_VALID_BYTES_3byte () = pf_bytes
  }

primplement
utf8_character_4byte_valid_bytes pf_valid =
  {
    prval UTF8_CHAR_valid (_, pf_bytes) = pf_valid
    prval UTF8_CHAR_VALID_BYTES_4byte () = pf_bytes
  }

primplement
utf8_character_1byte_invalid_bytes pf_invalid =
  {
    prval UTF8_CHAR_invalid (_, pf_bytes) = pf_invalid
    prval UTF8_CHAR_INVALID_BYTES_1byte () = pf_bytes
  }

primplement
utf8_character_2byte_invalid_bytes pf_invalid =
  {
    prval UTF8_CHAR_invalid (_, pf_bytes) = pf_invalid
    prval UTF8_CHAR_INVALID_BYTES_2byte () = pf_bytes
  }

primplement
utf8_character_3byte_invalid_bytes pf_invalid =
  {
    prval UTF8_CHAR_invalid (_, pf_bytes) = pf_invalid
    prval UTF8_CHAR_INVALID_BYTES_3byte () = pf_bytes
  }

primplement
utf8_character_4byte_invalid_bytes pf_invalid =
  {
    prval UTF8_CHAR_invalid (_, pf_bytes) = pf_invalid
    prval UTF8_CHAR_INVALID_BYTES_4byte () = pf_bytes
  }

implement {}
is_valid_utf8_character_1byte {c0} c0 =
  let
    stadef u = extended_utf8_char_1byte_decoding c0
  in
    (lemma_valid_utf8_character_1byte {u, c0} () | true)
  end

implement {}
is_valid_utf8_character_2byte {c0, c1} (c0, c1) =
  let
    stadef u = extended_utf8_char_2byte_decoding (c0, c1)
  in
    if not (is_valid_utf8_continuation_byte c1) then
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_bad_c1 (),
                          UTF8_CHAR_INVALID_BYTES_2byte ()) |
       false)
    else if c0 < 0xC2 then
      // The sequence is overlong.
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_invalid_2byte (),
                          UTF8_CHAR_INVALID_BYTES_2byte ()) |
       false)
    else
      (lemma_valid_utf8_character_2byte {u, c0, c1} () | true)
  end

implement {}
is_valid_utf8_character_3byte {c0, c1, c2} (c0, c1, c2) =
  let
    stadef u = extended_utf8_char_3byte_decoding (c0, c1, c2)
  in
    if not (is_valid_utf8_continuation_byte c1) then
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_bad_c1 (),
                          UTF8_CHAR_INVALID_BYTES_3byte ()) |
       false)
    else if not (is_valid_utf8_continuation_byte c2) then
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_bad_c2 (),
                          UTF8_CHAR_INVALID_BYTES_3byte ()) |
       false)
    else if (0xE1 <= c0) * (c0 <= 0xEC) then
      (lemma_valid_utf8_character_3byte {u, c0, c1, c2} () | true)
    else if c0 = 0xEE then
      (lemma_valid_utf8_character_3byte {u, c0, c1, c2} () | true)
    else if c0 = 0xEF then
      (lemma_valid_utf8_character_3byte {u, c0, c1, c2} () | true)
    else if (c0 = 0xE0) * (0xA0 <= c1) then
      (lemma_valid_utf8_character_3byte {u, c0, c1, c2} () | true)
    else if (c0 = 0xED) * (c1 < 0xA0) then
      (lemma_valid_utf8_character_3byte {u, c0, c1, c2} () | true)
    else
      // Either the sequence is overlong or it decodes to
      // an invalid code point.
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_invalid_3byte (),
                          UTF8_CHAR_INVALID_BYTES_3byte ()) |
       false)
  end

implement {}
is_valid_utf8_character_4byte {c0, c1, c2, c3} (c0, c1, c2, c3) =
  let
    stadef u = extended_utf8_char_4byte_decoding (c0, c1, c2, c3)
  in
    if not (is_valid_utf8_continuation_byte c1) then
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_bad_c1 (),
                          UTF8_CHAR_INVALID_BYTES_4byte ()) |
       false)
    else if not (is_valid_utf8_continuation_byte c2) then
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_bad_c2 (),
                          UTF8_CHAR_INVALID_BYTES_4byte ()) |
       false)
    else if not (is_valid_utf8_continuation_byte c3) then
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_bad_c3 (),
                          UTF8_CHAR_INVALID_BYTES_4byte ()) |
       false)
    else if (0xF1 <= c0) * (c0 <= 0xF3) then
      (lemma_valid_utf8_character_4byte {u, c0, c1, c2, c3} () | true)
    else if (c0 = 0xF0) * (0x90 <= c1) then
      (lemma_valid_utf8_character_4byte {u, c0, c1, c2, c3} () | true)
    else if (c0 = 0xF4) * (c1 < 0x90) then
      (lemma_valid_utf8_character_4byte {u, c0, c1, c2, c3} () | true)
    else
      // Either the sequence is overlong or it decodes to
      // an invalid code point.
      (UTF8_CHAR_invalid (UTF8_CHAR_INVALID_invalid_4byte (),
                          UTF8_CHAR_INVALID_BYTES_4byte ()) |
       false)
  end

implement {}
decode_utf8_1byte c0 = c0

implement {}
decode_utf8_2byte (c0, c1) =
  let
    val u0 = c0 - 0xC0
    val u1 = c1 - 0x80
  in
    64 * u0 + u1
  end

implement {}
decode_utf8_3byte (c0, c1, c2) =
  let
    val u0 = c0 - 0xE0
    val u1 = c1 - 0x80
    val u2 = c2 - 0x80
  in
    64 * (64 * u0 + u1) + u2
  end

implement {}
decode_utf8_4byte (c0, c1, c2, c3) =
  let
    val u0 = c0 - 0xF0
    val u1 = c1 - 0x80
    val u2 = c2 - 0x80
    val u3 = c3 - 0x80
  in
    64 * (64 * (64 * u0 + u1) + u2) + u3
  end

implement {}
encode_utf8_character {u} u =
  if u <= 0x7F then
    let
      val c0 = u
      stadef c0 = u
    in
      @(lemma_valid_utf8_character_1byte {u, c0} () | 1, c0, ~1, ~1, ~1)
    end
  else if u <= 0x7FF then
    let
      val c1 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c0 = 0xC0 + u1
      stadef c1 = 0x80 + (u \nmod 64)
      stadef u1 = u \ndiv 64
      stadef c0 = 0xC0 + u1
    in
      @(lemma_valid_utf8_character_2byte {u, c0, c1} () | 2, c0, c1, ~1, ~1)
    end
  else if u <= 0xFFFF then
    let
      val c2 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c1 = 0x80 + (u1 \nmod 64)
      val u2 = u1 \ndiv 64
      val c0 = 0xE0 + u2
      stadef c2 = 0x80 + (u \nmod 64)
      stadef u1 = u \ndiv 64
      stadef c1 = 0x80 + (u1 \nmod 64)
      stadef u2 = u1 \ndiv 64
      stadef c0 = 0xE0 + u2
    in
      @(lemma_valid_utf8_character_3byte {u, c0, c1, c2} () | 3, c0, c1, c2, ~1)
    end
  else
    let
      val c3 = 0x80 + (u \nmod 64)
      val u1 = u \ndiv 64
      val c2 = 0x80 + (u1 \nmod 64)
      val u2 = u1 \ndiv 64
      val c1 = 0x80 + (u2 \nmod 64)
      val u3 = u2 \ndiv 64
      val c0 = 0xF0 + u3
      stadef c3 = 0x80 + (u \nmod 64)
      stadef u1 = u \ndiv 64
      stadef c2 = 0x80 + (u1 \nmod 64)
      stadef u2 = u1 \ndiv 64
      stadef c1 = 0x80 + (u2 \nmod 64)
      stadef u3 = u2 \ndiv 64
      stadef c0 = 0xF0 + u3
      prval () = _shift_right_twelve (u, u1, u2)
    in
      @(lemma_valid_utf8_character_4byte {u, c0, c1, c2, c3} () |
        4, c0, c1, c2, c3)
    end

//--------------------------------------------------------------------
