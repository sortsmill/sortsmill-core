// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#define ATS_PACKNAME "SMCORE.bitstring_maps"
#define ATS_EXTERN_PREFIX "_smcore_bitstring_maps__"

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload UN = "prelude/SATS/unsafe.sats"

staload "sortsmill/core/SATS/bitstring_maps.sats"
staload "sortsmill/core/SATS/popcount_prf.sats"

//--------------------------------------------------------------------

local

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  macdef cast_to_ptr (obj) = $UN.cast{ptr} ,(obj)

  fn {t : t@ype}
  bitsizeof () : size_t = $extval (size_t, "CHAR_BIT") * sizeof<t>

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  #define BITS_PER_LEVEL _smcore_bitstring_maps__BITS_PER_LEVEL
  #define TABLE_SIZE_MAX _smcore_bitstring_maps__TABLE_SIZE_MAX
  #define BITMAP_TYPE _smcore_bitstring_maps__BITMAP_TYPE
  
  typedef bitmap (n : int) =
    [0 <= n] _smcore_bitstring_maps__BITMAP_TYPE n
  typedef bitmap = [n : nat] bitmap n
  macdef cast_to_bitmap (a) = $UN.cast{bitmap} ,(a)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  typedef left_shifter (a : t@ype, i : t@ype) = (a, i) -<> a
  typedef right_shifter (a : t@ype, i : t@ype) = (a, i) -<> a
  typedef mask_applier (a : t@ype) =
    {mask : nat} (a, int mask) -<> [i : nat | i <= mask] int i
  
  extern fn lnot_bitmap :
    bitmap -<> bitmap = "mac#_smcore_bitstring_maps__lnot"
  extern fn land_bitmap_bitmap :
    {m, n : nat}
    (bitmap m, bitmap n) -<> [i : nat | i <= m; i <= n] bitmap i =
      "mac#_smcore_bitstring_maps__land"
  extern fn lor_bitmap_bitmap :
    (bitmap, bitmap) -<> bitmap = "mac#_smcore_bitstring_maps__lor"
  extern fn lxor_bitmap_bitmap :
    (bitmap, bitmap) -<> bitmap = "mac#_smcore_bitstring_maps__lxor"
  overload lnot with lnot_bitmap of 100000
  overload land with land_bitmap_bitmap of 100000
  overload lor with lor_bitmap_bitmap of 100000
  overload lxor with lxor_bitmap_bitmap of 100000

  extern fn eq_bitmap_bitmap :
    (bitmap, bitmap) -<> bool = "mac#_smcore_bitstring_maps__eq"
  extern fn neq_bitmap_bitmap :
    (bitmap, bitmap) -<> bool = "mac#_smcore_bitstring_maps__neq"
  overload = with eq_bitmap_bitmap of 100000
  overload != with neq_bitmap_bitmap of 100000

  extern fn shift_left_bitmap_Nat :
    left_shifter (bitmap, Nat) = "mac#_smcore_bitstring_maps__shift_left"
  extern fn shift_right_bitmap_Nat :
    right_shifter (bitmap, Nat) = "mac#_smcore_bitstring_maps__shift_right"
  overload << with shift_left_bitmap_Nat of 100000
  overload >> with shift_right_bitmap_Nat of 100000

  extern fn popcount_prf :
    {i : nat} bitmap i -<> [n : nat] (POPCOUNT (i, n) | int n) =
      "mac#_smcore_bitstring_maps__popcount"

  fn
  popcount (i : bitmap) : Nat =
    let
      val (_ | popcnt) = popcount_prf i
    in
      popcnt
    end

  extern fn popcount_upto : (bitmap, Nat) -<> Nat = "mac#%"

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Use this macro instead of bitstring_map$keys_match()
  // to help prevent misordering of arguments.
  // (Fancier methods could be devised, using the type system,
  // but I’m not sure it is worth the trouble.)
  
  macdef KEYS_MATCH (key, pair) = bitstring_map$keys_match (,(key), ,(pair).0)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement {key}
  unsigned_key_to_table_index (key, level) =
    // Extract the levelth group of bits from a key.
    // Start with the most significant bits, so trie
    // traversal will be in ascending order if the key
    // is an unsigned integer.
    let
      val zero = i2sz 0
      val one = i2sz 1

      val bits_per_level = i2sz BITS_PER_LEVEL
      val bitsize = bitsizeof<key> ()

      val num_levels =
        (bitsize / bits_per_level) +
        (if bitsize mod bits_per_level = zero then zero else one)

      fn {}
      get_bits () :<cloref0> [i : nat | i < TABLE_SIZE_MAX] int i =
        let
          extern fn shift_left_key : left_shifter (key, size_t) =
              "mac#_smcore_bitstring_maps__shift_left"
          extern fn shift_right_key : right_shifter (key, size_t) =
              "mac#_smcore_bitstring_maps__shift_right"
          overload << with shift_left_key
          overload >> with shift_right_key

          extern fn apply_mask : mask_applier (key) =
            "mac#_smcore_bitstring_maps__land"

          val mask = pred TABLE_SIZE_MAX
          val j = bits_per_level * (succ (g0ofg1 level))
        in
          if bitsize < j then
            (key << (j - bitsize)) \apply_mask mask
          else
            (key >> (bitsize - j)) \apply_mask mask
        end
    in
      if level = zero then
        get_bits ()
      else if level < num_levels then
        get_bits ()
      else
        ~1
    end

  implement {key}
  signed_key_to_table_index (key, level) =
    // Reverse the sign bit, so keys will be sorted
    // into ascending order.
    let
      extern fn shift_left_key :
        left_shifter (key, size_t) = "mac#_smcore_bitstring_maps__shift_left"
      extern fn xor_key_key :
        (key, key) -<> key = "mac#_smcore_bitstring_maps__lxor"
      overload << with shift_left_key
      overload lxor with xor_key_key

      val mask = ($UN.cast{key} 1) << (pred (bitsizeof<key> ()))
      val altered_key = key lxor mask
    in
      unsigned_key_to_table_index (altered_key, level)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  abstype ptr_table_type (addr : addr, size : int) = ptr addr

  typedef ptr_table (addr : addr, size : int) =
    [addr != null; 1 <= size; size <= TABLE_SIZE_MAX]
    ptr_table_type (addr, size)
  typedef ptr_table (addr : addr) =
    [size : int] ptr_table (addr, size)
  typedef ptr_table (size : int) =
    [addr : addr] ptr_table (addr, size)
  typedef ptr_table =
    [size : int] ptr_table size

  macdef cast_to_ptr_table (p) = $UN.cast{ptr_table} ,(p)

  typedef ptr_table_size (i : int) = [1 <= i; i <= TABLE_SIZE_MAX] int i
  typedef ptr_table_size = [i : int] ptr_table_size i

  typedef ptr_table_index (i : int) = [0 <= i; i <= TABLE_SIZE_MAX - 1] int i
  typedef ptr_table_index = [i : int] ptr_table_index i

  typedef ptr_table_offset (limit : int, i : int) =
    [0 <= i; limit <= TABLE_SIZE_MAX; i < limit] int i
  typedef ptr_table_offset (limit : int) =
    [i : int] ptr_table_offset (limit, i)

  (* To write to a ptr_table, you need a WRITE_PERMISSION
     proof. These proofs are how we enforce immutability:
     you have to create a new table to get a proof. *)
  dataprop WRITE_PERMISSION (addr : addr) =
    | {addr : addr} WRITE_PERMISSION_ (addr) of ()

  extern fn get_is_stored : ptr_table -> bitmap = "mac#%"
  extern fn set_is_stored :
    {addr : addr; size : int}
    (WRITE_PERMISSION (addr) | ptr_table (addr, size), bitmap) ->
      void = "mac#%"

  extern fn get_is_pair : ptr_table -> bitmap = "mac#%"
  extern fn set_is_pair :
    {addr : addr; size : int}
    (WRITE_PERMISSION (addr) | ptr_table (addr, size), bitmap) ->
      void = "mac#%"

  extern fn get_ptr_table_size :
    {size : pos} ptr_table size -> ptr_table_size size = "mac#%"
  overload .size with get_ptr_table_size

  extern fn get_cell :
    {size : int} (ptr_table size, ptr_table_offset size) -> ptr = "mac#%"
  extern fn set_cell :
    {addr : addr; size : int}
    (WRITE_PERMISSION (addr) |
     ptr_table (addr, size), ptr_table_offset size, ptr) -> void = "mac#%"

  overload .is_stored with get_is_stored
  overload .is_pair with get_is_pair
  overload [] with get_cell

  extern fn
  ptr_table_malloc :
    {size : pos}
    ptr_table_size size ->
      [addr : addr]
      (WRITE_PERMISSION (addr) | ptr_table (addr, size)) = "mac#%"

  extern fn
  ptr_table_copy :
    {size : int}
    (ptr_table size) ->
      [addr : addr]
      (WRITE_PERMISSION (addr) | ptr_table (addr, size)) = "mac#%"

  typedef return_value_of_grow (addr : addr, size : int) =
    $extype_struct"_smcore_bitstring_maps__return_value_of_grow" of
      { table = ptr_table (addr, size),
        offset = ptr_table_offset size }
  extern fn
  ptr_table_grow :
    {size : int} (ptr_table size, ptr_table_index, bool) ->
      [addr : addr]
      (WRITE_PERMISSION (addr) | return_value_of_grow (addr, size + 1)) = "mac#%"

  extern fn
  ptr_table_shrink :
    {size : int} (ptr_table size, ptr_table_index) ->
      [addr : addr]
      (WRITE_PERMISSION (addr) | ptr_table (addr, size - 1)) = "mac#%"

  overload .copy with ptr_table_copy
  overload .grow with ptr_table_grow
  overload .shrink with ptr_table_shrink

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  fn {}
  bit_is_set (m : bitmap, i : ptr_table_index) : bool =
    let
      val zero = cast_to_bitmap 0
      val one = cast_to_bitmap 1
    in
      (m land (one << i)) != zero
    end

  fn {}
  bit_is_clear (m : bitmap, i : ptr_table_index) : bool =
    not (bit_is_set (m, i))

  fn {}
  set_bit (m : bitmap, i : ptr_table_index) : bitmap =
    let
      val zero = cast_to_bitmap 0
      val one = cast_to_bitmap 1
    in
      (m lor (one << i))
    end

  fn {}
  clear_bit (m : bitmap, i : ptr_table_index) : bitmap =
    let
      val zero = cast_to_bitmap 0
      val one = cast_to_bitmap 1
    in
      (m land (lnot (one << i)))
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  typedef map_struct (size : int) = '(size_t size, ptr_table)

  fn {key : t@ype; value : t@ype} {size : int}
  make_nonnil_bitstring_map (size : size_t size, table : ptr_table) :
      bitstring_map (key, value, size) =
    $UN.cast{bitstring_map (key, value, size)} '(size, table)

  fn {key : t@ype; value : t@ype} {size : int}
  make_nil_bitstring_map () : bitstring_map (key, value, 0) =
    $UN.cast{bitstring_map (key, value, 0)} the_null_ptr

  fn {key : t@ype; value : t@ype} {size : int}
  nonnil_bitstring_map_size (map : bitstring_map (key, value, size)) :
      size_t size =
    ($UN.cast{map_struct size} map).0

  fn {key : t@ype; value : t@ype} {size : int}
  nonnil_bitstring_map_table (map : bitstring_map (key, value, size)) :
      ptr_table =
    ($UN.cast{map_struct size} map).1

  overload make_bitstring_map with make_nonnil_bitstring_map
  overload make_bitstring_map with make_nil_bitstring_map
  overload .size with nonnil_bitstring_map_size
  overload .table with nonnil_bitstring_map_table

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // Try to find a value in the map, and return the results by side effects.
  fn {key : t@ype; value : t@ype}
  find_value_in_bitstring_map_eff (map : bitstring_map (key, value), key : key,
                                   value : &value? >> opt (value, b)) :
      #[b : bool] bool b =
    let
      macdef cast_to_pair (p) = $UN.cast{'(key, value)} ,(p)

      fun {}
      loop (table : ptr_table,
            level : [level : nat] size_t level,
            value : &value? >> opt (value, b)) :<cloref1>
          #[b : bool] bool b =
        let
          val index = bitstring_map$key_to_table_index (key, level)
        in
          if index = ~1 then              
            let
              prval () = opt_none{value} value
            in
              false
            end
          else if bit_is_clear (table.is_stored (), index) then
            let
              prval () = opt_none{value} value
            in
              false
            end
          else if bit_is_set (table.is_pair (), index) then
            let
              val offset = popcount_upto (table.is_stored (), index)
              val () = assertloc (offset < table.size ())
              val pair = cast_to_pair table[offset]
            in
              if KEYS_MATCH (key, pair) then
                let
                  val () = value := pair.1
                  prval () = opt_some{value} value
                in
                  true
                end
              else
                let
                  prval () = opt_none{value} value
                in
                  false
                end
            end
          else
            let
              val offset = popcount_upto (table.is_stored (), index)
              val () = assertloc (offset < table.size ())
              val child = cast_to_ptr_table table[offset]
            in
              loop (child, succ level, value)
            end
        end

      val map_is_nil = bitstring_map_is_nil map
    in
      if map_is_nil then
        let
          prval () = opt_none{value} value
        in
          false
        end
      else
        loop (map.table (), i2sz 0, value)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // Interfaces for doing different things with values recovered
  // from the trie.
  extern fn {result : t@ype}
  bitstring_map$not_found_value : () -> result
  extern fn {result : t@ype} {value : t@ype} 
  bitstring_map$found_value : value -> result

  // Find a value in the trie, and then do something with it.
  fn {result : t@ype} {key : t@ype; value : t@ype}
  find_value_in_ptr_table (table : ptr_table, key : key) : result =
    let
      macdef cast_to_pair (p) = $UN.cast{'(key, value)} ,(p)

      fun {}
      loop (table : ptr_table,
            level : [level : nat] size_t level) :<cloref1> result =
        let
          val index = bitstring_map$key_to_table_index (key, level)
        in
          if index = ~1 then              
            bitstring_map$not_found_value ()
          else if bit_is_clear (table.is_stored (), index) then
            bitstring_map$not_found_value ()
          else if bit_is_set (table.is_pair (), index) then
            let
              val offset = popcount_upto (table.is_stored (), index)
              val () = assertloc (offset < table.size ())
              val pair = cast_to_pair table[offset]
            in
              if KEYS_MATCH (key, pair) then
                bitstring_map$found_value (pair.1)
              else
                bitstring_map$not_found_value ()
            end
          else
            let
              val offset = popcount_upto (table.is_stored (), index)
              val () = assertloc (offset < table.size ())
              val child = cast_to_ptr_table table[offset]
            in
              loop (child, succ level)
            end
        end
    in
      loop (table, i2sz 0)
    end

  // Find a value in the map, and then do something with it.
  fn {result : t@ype} {key : t@ype; value : t@ype}
  find_value_in_bitstring_map (map : bitstring_map (key, value), key : key) :
      result =
    let
      val map_is_nil = bitstring_map_is_nil map
    in
      if map_is_nil then
        bitstring_map$not_found_value ()
      else
        find_value_in_ptr_table<result><key, value> (map.table (), key)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  typedef map_size_after_set (orig_size : int) =
    [i : int | i == orig_size || i == orig_size + 1] size_t i

  fn {key : t@ype; value : t@ype}
  set_value_in_ptr_table {orig_size : pos}
                         (orig_size : size_t orig_size,
                          table : ptr_table,
                          key : key, value : value) :
      @(ptr_table, map_size_after_set orig_size) =
    let
      macdef cast_to_pair (p) = $UN.cast{'(key, value)} ,(p)

      fun {}
      create_children {addr : addr; size : int}
                      (pf_parent : WRITE_PERMISSION (addr) |
                       parent : ptr_table (addr, size),
                       parent_offset : ptr_table_offset size,
                       stored_pair : '(key, value),
                       level : Size_t) : void =
        let
          val index1 = bitstring_map$key_to_table_index (key, level)
          val index2 = bitstring_map$key_to_table_index (stored_pair.0, level)
        in
          if index1 = ~1 then
            $raise BitstringMapKeyExhaustedExn ()
          else if index2 = ~1 then
            $raise BitstringMapStoredKeyExhaustedExn ()
          else if index1 = index2 then
            let
              val (pf_child | child) = ptr_table_malloc 1
              val mask = set_bit (cast_to_bitmap 0, index1)
              val () = set_is_stored (pf_child | child, mask)
              val () = set_is_pair (pf_child | child, cast_to_bitmap 0)
              val () = set_cell (pf_parent | parent, parent_offset,
                                             cast_to_ptr child)
            in
              (* BE SURE to do this last, so the recursion is a tail call. *)
              create_children (pf_child | child, 0, stored_pair, succ level)
            end
          else
            let
              val (pf_child | child) = ptr_table_malloc 2
              val mask = set_bit (set_bit (cast_to_bitmap 0, index1), index2)
              val () = set_is_stored (pf_child | child, mask)
              val () = set_is_pair (pf_child | child, mask)
              val new_pair = '(key, value)
              val () =
                if index1 < index2 then
                  (set_cell (pf_child | child, 0, cast_to_ptr new_pair);
                   set_cell (pf_child | child, 1, cast_to_ptr stored_pair))
                else
                  (set_cell (pf_child | child, 0, cast_to_ptr stored_pair);
                   set_cell (pf_child | child, 1, cast_to_ptr new_pair))
              val () = set_cell (pf_parent | parent, parent_offset,
                                             cast_to_ptr child)
            in
            end
        end

      fun {}
      go_to_child {addr : addr}
                  (pf_parent : WRITE_PERMISSION (addr) |
                   parent : ptr_table addr,
                   parent_index : ptr_table_index,
                   level : Size_t) :<cloref1>
          map_size_after_set orig_size =
        let
          val () = assertloc (bit_is_clear (parent.is_pair (),
                                            parent_index)) (* Safety check. *)
          val parent_offset = popcount_upto (parent.is_stored (), parent_index)
          val () = assertloc (parent_offset < parent.size ())
          val child = cast_to_ptr_table parent[parent_offset]
          val child_index = bitstring_map$key_to_table_index (key, level)
        in
          if child_index = ~1 then
            $raise BitstringMapKeyExhaustedExn ()
          else if bit_is_clear (child.is_stored (), child_index) then          
            let
              val (pf_child | @{ table = child, offset = child_offset }) =
                child.grow (child_index, true)
              val () = assertloc (child[child_offset] = the_null_ptr) (* Safety check. *)
              val () = set_cell (pf_child | child, child_offset,
                                            cast_to_ptr '(key, value))
              val () = set_cell (pf_parent | parent, parent_offset,
                                             cast_to_ptr child)
            in
              succ orig_size
            end
          else if bit_is_set (child.is_pair (), child_index) then
            let
              val child_offset = popcount_upto (child.is_stored (),
                                                child_index)
              val () = assertloc (child_offset < child.size ())
              val stored_pair = cast_to_pair child[child_offset]
            in
              if KEYS_MATCH (key, stored_pair) then
                let
                  val (pf_child | child) = child.copy ()
                  val () = set_cell (pf_child | child, child_offset,
                                                cast_to_ptr '(key, value))
                  val () = set_cell (pf_parent | parent, parent_offset,
                                                 cast_to_ptr child)
                in
                  orig_size
                end
              else
                let
                  val (pf_child | child) = child.copy ()
                  val () =
                    set_is_pair (pf_child | child, clear_bit (child.is_pair (),
                                                              child_index))
                  val () = create_children (pf_child | child, child_offset,
                                                       stored_pair, succ level)
                  val () = set_cell (pf_parent | parent, parent_offset,
                                                 cast_to_ptr child)
                in
                  succ orig_size
                end
            end
          else
            let
              val (pf_child | child) = child.copy ()
              val () = set_cell (pf_parent | parent, parent_offset,
                                             cast_to_ptr child)
            in
              go_to_child (pf_child | child, child_index, succ level)
            end
        end

      val index0 = bitstring_map$key_to_table_index (key, i2sz 0)
    in
      if bit_is_clear (table.is_stored (), index0) then
        let
          val (pf_write | @{ table = table, offset = offset0 }) =
            table.grow (index0, true)
          val () = assertloc (table[offset0] = the_null_ptr) (* Safety check. *)
          val () = set_cell (pf_write | table, offset0,
                                        cast_to_ptr '(key, value))
        in
          @(table, succ orig_size)
        end
      else if bit_is_set (table.is_pair (), index0) then
        let
          val offset0 = popcount_upto (table.is_stored (), index0)
          val () = assertloc (offset0 < table.size ())
          val stored_pair = cast_to_pair table[offset0]
        in
          if KEYS_MATCH (key, stored_pair) then
            let
              val (pf_write | table) = table.copy ()
              val () = set_cell (pf_write | table, offset0,
                                            cast_to_ptr '(key, value))
            in
              @(table, orig_size)
            end
          else
            let
              val (pf_write | table) = table.copy ()
              val () = set_is_pair (pf_write |
                                    table, clear_bit (table.is_pair (), index0))
              val () = create_children (pf_write | table, offset0, stored_pair,
                                                   g1ofg0 (i2sz 1))
            in
              @(table, succ orig_size)
            end
        end
      else
        let
          val (pf_write | table) = table.copy ()
          val new_size = go_to_child (pf_write | table, index0, g1ofg0 (i2sz 1))
        in
          @(table, new_size)
        end
    end

  fn {key : t@ype; value : t@ype}
  set_value_in_bitstring_map {orig_size : nat}
                             (map : bitstring_map (key, value, orig_size),
                              key : key, value : value) :
      [new_size : int | (orig_size == 0 && new_size == 1) ||
                        (0 < orig_size && new_size == orig_size) ||
                        (0 < orig_size && new_size == orig_size + 1)]
      bitstring_map (key, value, new_size) =
    let
      val orig_size = bitstring_map_size map
    in
      if orig_size = i2sz 0 then
        let
          val (pf_write | table) = ptr_table_malloc 1
          val index0 = bitstring_map$key_to_table_index (key, i2sz 0)
          val mask = set_bit (cast_to_bitmap 0, index0)
          val () = set_is_stored (pf_write | table, mask)
          val () = set_is_pair (pf_write | table, mask)
          val () = set_cell (pf_write | table, 0, cast_to_ptr '(key, value))
        in
          make_bitstring_map (i2sz 1, table)
        end
      else
        let
          val @(table, new_size) =
            set_value_in_ptr_table (orig_size, map.table (), key, value)
        in
          make_bitstring_map (new_size, table)
        end
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  typedef bitstring_map_after_remove (key : t@ype+, value : t@ype+,
                                      orig_size : int) =
      [new_size : int | (orig_size == 0 && new_size == 0) ||
                        (0 < orig_size && new_size == orig_size) ||
                        (0 < orig_size && new_size == orig_size - 1)]
      bitstring_map (key, value, new_size)

  fn {key : t@ype; value : t@ype}
  remove_value_from_ptr_table {orig_size : pos}
                              (orig_size : size_t orig_size,
                               table : ptr_table,
                               key : key) :
      Option_vt (bitstring_map_after_remove (key, value, orig_size)) =
    let
      macdef cast_to_pair (p) = $UN.cast{'(key, value)} ,(p)

      fun {}
      reconstruct_from_pedigree
          {level : pos} .<level>.
          (pedigree : list (@(ptr_table, ptr_table_index), level),
           child : ptr_table) : ptr_table =
        let
          val list_cons (@(parent, parent_index), parent_pedigree) = pedigree
          val (pf_write | parent) = parent.copy ()
          val () = set_is_pair (pf_write |
                                parent,
                                clear_bit (parent.is_pair (), parent_index))
          val parent_offset = popcount_upto (parent.is_stored (), parent_index)
          val () = assertloc (parent_offset < parent.size ())
          val () = set_cell (pf_write | parent, parent_offset,
                                        cast_to_ptr child)
        in
          case+ parent_pedigree of
            | list_nil () => parent
            | list_cons (_, _) =>
              reconstruct_from_pedigree (parent_pedigree, parent)
        end

      fun {}
      replace_cell_somewhere_in_pedigree
          {level : pos} .<level>.
          (pedigree : list (@(ptr_table, ptr_table_index), level),
           retained_pair : '(key, value)) : ptr_table =
        let
          val list_cons (@(parent, parent_index), parent_pedigree) = pedigree
        in
          case+ parent_pedigree of
            | list_nil () =>
              let
                val (pf_write | parent) = parent.copy ()
                val () = set_is_pair (pf_write |
                                      parent,
                                      set_bit (parent.is_pair (), parent_index))
                val parent_offset = popcount_upto (parent.is_stored (),
                                                   parent_index)
                val () = assertloc (parent_offset < parent.size ())
                val () = set_cell (pf_write | parent, parent_offset,
                                              cast_to_ptr retained_pair)
              in
                parent
              end
            | list_cons (_, _) =>
              if parent.size () = 1 then
                replace_cell_somewhere_in_pedigree (parent_pedigree,
                                                    retained_pair)
              else
                let
                  val (pf_write | parent) = parent.copy ()
                  val () = set_is_pair (pf_write |
                                        parent,
                                        set_bit (parent.is_pair (), parent_index))
                  val parent_offset = popcount_upto (parent.is_stored (),
                                                     parent_index)
                  val () = assertloc (parent_offset < parent.size ())
                  val () = set_cell (pf_write | parent, parent_offset,
                                                cast_to_ptr retained_pair)
                in
                  reconstruct_from_pedigree (parent_pedigree, parent)
                end
        end

      fun {}
      go_to_child {orig_size : pos} {level : pos}
                  (orig_size : size_t orig_size,
                   pedigree : list (@(ptr_table, ptr_table_index), level),
                   level : size_t level) :<cloref1>
          Option_vt (bitstring_map_after_remove (key, value, orig_size)) =
        let
          val list_cons (@(parent, parent_index), _) = pedigree
          val () = assertloc (bit_is_clear (parent.is_pair (),
                                            parent_index)) (* Safety check. *)
          val parent_offset = popcount_upto (parent.is_stored (), parent_index)
          val () = assertloc (parent_offset < parent.size ())
          val child = cast_to_ptr_table parent[parent_offset]
          val child_index = bitstring_map$key_to_table_index (key, level)
        in
          if child_index = ~1 then
            None_vt ()
          else if bit_is_clear (child.is_stored (), child_index) then
            None_vt ()
          else if bit_is_set (child.is_pair (), child_index) then
            let
              val child_offset = popcount_upto (child.is_stored (),
                                                child_index)
              val () = assertloc (child_offset < child.size ())
              val stored_pair = cast_to_pair child[child_offset]
            in
              if KEYS_MATCH (key, stored_pair) then
                let
                  (* Safety check. *)
                  val () = assertloc (1 < child.size ())
                in
                  if child.size () = 2 then
                    if clear_bit (child.is_pair (), child_index) !=
                        cast_to_bitmap 0 then
                      (* Also would work:
                         if popcount (child.is_pair ()) = 2 then *)
                      let
                        val i = 1 - child_offset
                        val retained_pair = cast_to_pair child[i]
                        val table =
                          replace_cell_somewhere_in_pedigree (pedigree,
                                                              retained_pair)
                      in
                        Some_vt (make_bitstring_map (pred orig_size, table))
                      end
                    else
                      let
                        val (pf_write | child) =
                          child.shrink (child_index)
                        val table = reconstruct_from_pedigree (pedigree, child)
                      in
                        Some_vt (make_bitstring_map (pred orig_size, table))
                      end
                  else
                    let
                      val (pf_write | child) =
                        child.shrink (child_index)
                      val table = reconstruct_from_pedigree (pedigree, child)
                    in
                      Some_vt (make_bitstring_map (pred orig_size, table))
                    end
                end
              else
                None_vt ()
            end
          else
            let
              val pedigree = list_cons (@(child, child_index), pedigree)
            in
              go_to_child (orig_size, pedigree, succ level)
            end
        end

      val index0 = bitstring_map$key_to_table_index (key, i2sz 0)
    in
      if bit_is_clear (table.is_stored (), index0) then
        None_vt ()
      else if bit_is_set (table.is_pair (), index0) then
        let
          val offset0 = popcount_upto (table.is_stored (), index0)
          val () = assertloc (offset0 < table.size ())
          val stored_pair = cast_to_pair table[offset0]
        in
          if KEYS_MATCH (key, stored_pair) then
            if orig_size = i2sz 1 then
              Some_vt (bitstring_map_nil)
            else
              let
                val (pf_write | table) = table.shrink (index0)
                val map = make_bitstring_map (pred orig_size, table)
              in
                Some_vt (map)
              end
          else
            None_vt ()
        end
      else
        let
          val pedigree = list_cons (@(table, index0), list_nil)
        in
          go_to_child (orig_size, pedigree, i2sz 1)
        end
    end

  fn {key : t@ype; value : t@ype}
  remove_value_from_bitstring_map {orig_size : nat}
                                  (map : bitstring_map (key, value, orig_size),
                                   key : key) :
      bitstring_map_after_remove (key, value, orig_size) =
    let
      val orig_size = bitstring_map_size map
    in
      if orig_size = i2sz 0 then
        map
      else
        let
          val table = map.table ()
          val new_map =
            remove_value_from_ptr_table<key, value> (orig_size, table, key)
        in
          case+ new_map of
            | ~None_vt () => map
            | ~Some_vt map => map
        end
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  fn {key : t@ype; value : t@ype}
  dump_ptr_table (table : ptr_table,
                  dump_pair : (key, value) -<cloref1> void) : void =
    let
      macdef cast_to_pair (p) = $UN.cast{'(key, value)} ,(p)

      extern fn ull_shift_right : (ullint, Nat) -> ullint =
        "mac#_smcore_bitstring_maps__shift_right"

      fun
      loop (table : ptr_table,
            level : [level : nat] size_t level) :<cloref1> void =
        let
          fn dump_space () :<cloref1> void =
            let
              fun
              dump_sp (i : [i : nat] size_t i) :<cloref1> void =
                if i = level then
                  ()
                else
                  begin
                    $extfcall (void, "printf", "  ");
                    dump_sp (succ i)
                  end
            in
              dump_sp (g1ofg0 (i2sz 0))
            end
          val is_stored_field = $UN.cast{ullint} (table.is_stored ())
          val is_pair_field = $UN.cast{ullint} (table.is_pair ())
          val size = table.size ()
          val () = dump_space ()
          val () = $extfcall (void, "printf", "level     : %zu\n", level)
          val () = dump_space ()
          val () = $extfcall (void, "printf",
                              "is_stored : %.16llX\n", is_stored_field)
          val () = dump_space ()
          val () = $extfcall (void, "printf",
                              "is_pair   : %.16llX\n", is_pair_field)
          val () = dump_space ()
          val () = $extfcall (void, "printf", "size      : %u\n", size)
          fn
          dump_cells () :<cloref1> void =
            dump (i2sz 0, i2sz 0, is_stored_field, is_pair_field)
              where
              {
                fun dump (offset : size_t, index : size_t,
                          is_stored_field : ullint,
                          is_pair_field : ullint) :<cloref1> void =
                  if offset = size then
                    ()
                  else if (is_stored_field land 1LLU) != 0LLU then
                    let
                      val () = dump_space ()
                      val () = $extfcall (void, "printf",
                                          "[%.2zu : %.2zu] : ",
                                          offset, index)
                      val i = $UN.cast{[max : pos] ptr_table_offset max} offset
                      val () = assertloc (i < table.size ())
                      val cell = table[i]
                      val () =
                        if (is_pair_field land 1LLU) != 0LLU then
                          begin
                            $extfcall (void, "printf", "key/value = ");
                            dump_pair ((cast_to_pair cell).0,
                                       (cast_to_pair cell).1);
                            $extfcall (void, "printf", "\n")
                          end
                        else
                          begin
                            $extfcall (void, "printf", "table\n");
                            loop (cast_to_ptr_table cell, succ level)
                          end
                    in
                      dump (succ offset, succ index,
                            ull_shift_right (is_stored_field, 1),
                            ull_shift_right (is_pair_field, 1))
                    end
                  else
                    dump (offset, succ index,
                          ull_shift_right (is_stored_field, 1),
                          ull_shift_right (is_pair_field, 1))
              }
            val () = dump_cells ()
        in
        end
    in
      loop (table, g1ofg0 (i2sz 0))
    end

  fn {key : t@ype; value : t@ype}
  dump_bitstring_map (map : bitstring_map (key, value),
                      dump_pair : (key, value) -<cloref1> void) : void =
    let
      val map_is_nil = bitstring_map_is_nil map
    in
      if map_is_nil then
        $extfcall (void, "printf", "nil map\n")
      else
        dump_ptr_table (map.table (), dump_pair)
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  typedef cursor (level : int) =
    [0 <= level] list (@(ptr_table, ptr_table_index), level)
  typedef cursor = [level : nat] cursor level

  fun {}
  next_index {i : nat | i < TABLE_SIZE_MAX} .<TABLE_SIZE_MAX - i>.
             (table : ptr_table, i : ptr_table_index i) :
      [j : int | i <= j;
                 j <= TABLE_SIZE_MAX;
                 i != 0 || j < TABLE_SIZE_MAX] int j =
    if bit_is_set (table.is_stored (), i) then
      i
    else if i = 0 then
      let
        val index = next_index (table, 1)
        (* FIXME: We could get rid of the assertloc() if
           we had proof next_index() will find a set bit
           _somewhere_ within table.is_stored(). *)
        val () = assertloc (index < TABLE_SIZE_MAX)
      in
        index
      end
    else if i < TABLE_SIZE_MAX - 1 then
      next_index (table, succ i)
    else
      TABLE_SIZE_MAX

  fun {}
  prev_index {i : nat | i < TABLE_SIZE_MAX} .<i>.
             (table : ptr_table, i : ptr_table_index i) :
      [j : int | j <= i;
                 ~1 <= j;
                 i != TABLE_SIZE_MAX - 1 || 0 <= j] int j =
    if bit_is_set (table.is_stored (), i) then
      i
    else if i = TABLE_SIZE_MAX - 1 then
      let
        val index = prev_index (table, TABLE_SIZE_MAX - 2)
        (* FIXME: We could get rid of the assertloc() if
           we had proof prev_index() will find a set bit
           _somewhere_ within table.is_stored(). *)
        val () = assertloc (0 <= index)
      in
        index
      end
    else if 0 < i then
      prev_index (table, pred i)
    else
      ~1

  fn {key : t@ype; value : t@ype}
  find_first_entry (map : bitstring_map (key, value)) :
      bitstring_map_cursor (key, value) =
    let
      val map_is_nil = bitstring_map_is_nil map
    in
      if map_is_nil then
        bitstring_map_cursor_nil
      else
        let
          fun {}
          find_entry {level : nat}
                     (table : ptr_table,
                      level : size_t level,
                      cursor : cursor level) :
              bitstring_map_cursor (key, value) =
            let
              val index = next_index (table, 0)
              val cursor = list_cons (@(table, index), cursor)
            in
              if bit_is_set (table.is_pair (), index) then
                $UN.cast{bitstring_map_cursor (key, value)} cursor
              else
                let
                  val offset = popcount_upto (table.is_stored (), index)
                  val () = assertloc (offset < table.size ())
                  val child = cast_to_ptr_table table[offset]
                in
                  find_entry (child, succ level, cursor)
                end
            end
        in
          find_entry (map.table (), i2sz 0, list_nil)
        end
    end

  fn {key : t@ype; value : t@ype}
  find_last_entry (map : bitstring_map (key, value)) :
      bitstring_map_cursor (key, value) =
    let
      val map_is_nil = bitstring_map_is_nil map
    in
      if map_is_nil then
        bitstring_map_cursor_nil
      else
        let
          fun {}
          find_entry {level : nat}
                     (table : ptr_table,
                      level : size_t level,
                      cursor : cursor level) :
              bitstring_map_cursor (key, value) =
            let
              val index = prev_index (table, TABLE_SIZE_MAX - 1)
              val cursor = list_cons (@(table, index), cursor)
            in
              if bit_is_set (table.is_pair (), index) then
                $UN.cast{bitstring_map_cursor (key, value)} cursor
              else
                let
                  val offset = popcount_upto (table.is_stored (), index)
                  val () = assertloc (offset < table.size ())
                  val child = cast_to_ptr_table table[offset]
                in
                  find_entry (child, succ level, cursor)
                end
            end
        in
          find_entry (map.table (), i2sz 0, list_nil)
        end
    end

  fn {key : t@ype; value : t@ype}
  find_entry_by_key (map : bitstring_map (key, value), key : key) :
      bitstring_map_cursor (key, value) =
    let
      val map_is_nil = bitstring_map_is_nil map
    in
      if map_is_nil then
        bitstring_map_cursor_nil
      else
        let
          fun {}
          find_entry {level : nat}
                     (table : ptr_table,
                      level : size_t level,
                      cursor : cursor level) :
              bitstring_map_cursor (key, value) =
            let
              val index = bitstring_map$key_to_table_index (key, level)
            in
              if index = ~1 then
                bitstring_map_cursor_nil
              else
                let
                  val cursor = list_cons (@(table, index), cursor)
                in
                  if bit_is_set (table.is_pair (), index) then
                    $UN.cast{bitstring_map_cursor (key, value)} cursor
                  else
                    let
                      val offset = popcount_upto (table.is_stored (), index)
                      val () = assertloc (offset < table.size ())
                      val child = cast_to_ptr_table table[offset]
                    in
                      find_entry (child, succ level, cursor)
                    end
                end
            end
        in
          find_entry (map.table (), i2sz 0, list_nil)
        end
    end

  fun {key : t@ype; value : t@ype}
  move_to_next_nonnil (cursor : cursor) : bitstring_map_cursor (key, value) =
    let
      fun {}
      go_to_children (table : ptr_table,
                      index : ptr_table_index,
                      pedigree : cursor) :
          bitstring_map_cursor (key, value) =
        let 
          val cursor = list_cons (@(table, index), pedigree)
        in
          if bit_is_set (table.is_pair (), index) then
            $UN.cast{bitstring_map_cursor (key, value)} cursor
          else
            let
              val offset = popcount_upto (table.is_stored (), index)
              val () = assertloc (offset < table.size ())
              val child = cast_to_ptr_table table[offset]
              val child_index = next_index (child, 0)
            in
              go_to_children (child, child_index, cursor)
            end
        end

      fun {}
      loop (cursor : cursor) : bitstring_map_cursor (key, value) =
        case+ cursor of
          | list_nil () => bitstring_map_cursor_nil
          | list_cons (@(table, index), pedigree) =>
            if index = TABLE_SIZE_MAX - 1 then
              move_to_next_nonnil pedigree
            else
              let
                val index = next_index (table, succ index)
              in
                if index = TABLE_SIZE_MAX then
                  move_to_next_nonnil pedigree
                else
                  go_to_children (table, index, pedigree)
              end
    in
      loop cursor
    end

  fn {key : t@ype; value : t@ype}
  move_to_next (cursor : bitstring_map_cursor (key, value)) :
      bitstring_map_cursor (key, value) =
    let
      val cursor_is_nil = bitstring_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        bitstring_map_cursor_nil
      else
        move_to_next_nonnil ($UN.cast{cursor} cursor)
    end
    
  fun {key : t@ype; value : t@ype}
  move_to_prev_nonnil (cursor : cursor) : bitstring_map_cursor (key, value) =
    let
      fun {}
      go_to_children (table : ptr_table,
                      index : ptr_table_index,
                      pedigree : cursor) :
          bitstring_map_cursor (key, value) =
        let 
          val cursor = list_cons (@(table, index), pedigree)
        in
          if bit_is_set (table.is_pair (), index) then
            $UN.cast{bitstring_map_cursor (key, value)} cursor
          else
            let
              val offset = popcount_upto (table.is_stored (), index)
              val () = assertloc (offset < table.size ())
              val child = cast_to_ptr_table table[offset]
              val child_index = prev_index (child, TABLE_SIZE_MAX - 1)
            in
              go_to_children (child, child_index, cursor)
            end
        end

      fun {}
      loop (cursor : cursor) : bitstring_map_cursor (key, value) =
        case+ cursor of
          | list_nil () => bitstring_map_cursor_nil
          | list_cons (@(table, index), pedigree) =>
            if index = 0 then
              move_to_prev_nonnil pedigree
            else
              let
                val index = prev_index (table, pred index)
              in
                if index = ~1 then
                  move_to_prev_nonnil pedigree
                else
                  go_to_children (table, index, pedigree)
              end
    in
      loop cursor
    end

  fn {key : t@ype; value : t@ype}
  move_to_prev (cursor : bitstring_map_cursor (key, value)) :
      bitstring_map_cursor (key, value) =
    let
      val cursor_is_nil = bitstring_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        bitstring_map_cursor_nil
      else
        move_to_prev_nonnil ($UN.cast{cursor} cursor)
    end
    
  fn {key : t@ype; value : t@ype}
  get_boxed_pair (cursor : bitstring_map_cursor (key, value)) : '(key, value) =
    let
      val- list_cons (@(table, index), _) = $UN.cast{cursor} cursor
      val () = assertloc (bit_is_set (table.is_pair (), index)) (* Safety check. *)
      val offset = popcount_upto (table.is_stored (), index)
      val () = assertloc (offset < table.size ())
      val boxed_pair = $UN.cast{'(key, value)} table[offset]
    in
      boxed_pair
    end

  fn {key : t@ype; value : t@ype}
  get_pair (cursor : bitstring_map_cursor (key, value)) : @(key, value) =
    case+ get_boxed_pair cursor of
      | '(key, value) => @(key, value)

  fn {key : t@ype; value : t@ype}
  get_key (cursor : bitstring_map_cursor (key, value)) : key =
    (get_boxed_pair cursor).0

  fn {key : t@ype; value : t@ype}
  get_value (cursor : bitstring_map_cursor (key, value)) : value =
    (get_boxed_pair cursor).1

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

in

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  implement bitstring_map$key_to_table_index<uint> = unsigned_key_to_table_index<uint>
  implement bitstring_map$key_to_table_index<ulint> = unsigned_key_to_table_index<ulint>
  implement bitstring_map$key_to_table_index<ullint> = unsigned_key_to_table_index<ullint>
  implement bitstring_map$key_to_table_index<uint8> = unsigned_key_to_table_index<uint8>
  implement bitstring_map$key_to_table_index<uint16> = unsigned_key_to_table_index<uint16>
  implement bitstring_map$key_to_table_index<uint32> = unsigned_key_to_table_index<uint32>
  implement bitstring_map$key_to_table_index<uint64> = unsigned_key_to_table_index<uint64>
  implement bitstring_map$key_to_table_index<size_t> = unsigned_key_to_table_index<size_t>
  implement bitstring_map$key_to_table_index<uintptr> = unsigned_key_to_table_index<uintptr>

  implement bitstring_map$key_to_table_index<int> = signed_key_to_table_index<int>
  implement bitstring_map$key_to_table_index<lint> = signed_key_to_table_index<lint>
  implement bitstring_map$key_to_table_index<llint> = signed_key_to_table_index<llint>
  implement bitstring_map$key_to_table_index<int8> = signed_key_to_table_index<int8>
  implement bitstring_map$key_to_table_index<int16> = signed_key_to_table_index<int16>
  implement bitstring_map$key_to_table_index<int32> = signed_key_to_table_index<int32>
  implement bitstring_map$key_to_table_index<int64> = signed_key_to_table_index<int64>
  implement bitstring_map$key_to_table_index<ssize_t> = signed_key_to_table_index<ssize_t>
  implement bitstring_map$key_to_table_index<intptr> = signed_key_to_table_index<intptr>

  implement bitstring_map$keys_match<uint> (a, b) = (a = b)
  implement bitstring_map$keys_match<ulint> (a, b) = (a = b)
  implement bitstring_map$keys_match<ullint> (a, b) = (a = b)
  implement bitstring_map$keys_match<uint8> (a, b) = (a = b)
  implement bitstring_map$keys_match<uint16> (a, b) = (a = b)
  implement bitstring_map$keys_match<uint32> (a, b) = (a = b)
  implement bitstring_map$keys_match<uint64> (a, b) = (a = b)
  implement bitstring_map$keys_match<size_t> (a, b) = (a = b)
  implement bitstring_map$keys_match<uintptr> (a, b) = (a = b)

  implement bitstring_map$keys_match<int> (a, b) = (a = b)
  implement bitstring_map$keys_match<lint> (a, b) = (a = b)
  implement bitstring_map$keys_match<llint> (a, b) = (a = b)
  implement bitstring_map$keys_match<int8> (a, b) = (a = b)
  implement bitstring_map$keys_match<int16> (a, b) = (a = b)
  implement bitstring_map$keys_match<int32> (a, b) = (a = b)
  implement bitstring_map$keys_match<int64> (a, b) = (a = b)
  implement bitstring_map$keys_match<ssize_t> (a, b) = (a = b)
  implement bitstring_map$keys_match<intptr> (a, b) = (a = b)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Functions for querying the size of the map, and proofs related
  // to map size.
  //

  primplement
  lemma_bitstring_map_param map =
    case+ map of
      | bitstring_map_nil () => ()
      | bitstring_map_nonnil _ => ()

  implement {}
  bitstring_map_is_nil map =
    case+ map of
      | bitstring_map_nil () => true
      | bitstring_map_nonnil _ => false

  implement {}
  bitstring_map_size map =
    let
      val is_nil = bitstring_map_is_nil map
    in
      if is_nil then
        i2sz 0
      else
        map.size ()
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Functions for searching the map.

  implement {key, value}
  bitstring_map_has_key (map, key) =
    let
      implement
      bitstring_map$not_found_value<bool> () = false
      implement
      bitstring_map$found_value<bool><value> (_) = true
    in
      find_value_in_bitstring_map<bool><key, value> (map, key)
    end

  implement {key, value}
  bitstring_map_get (map, key) =
    let
      implement
      bitstring_map$not_found_value<Option value> () =
        None ()
      implement
      bitstring_map$found_value<Option value><value> (v) =
        Some v
    in
      find_value_in_bitstring_map<Option value><key, value> (map, key)
    end

  implement {key, value}
  bitstring_map_get_exn (map, key) =
    let
      implement
      bitstring_map$not_found_value<value> () =
        $raise BitstringMapSubscriptExn ()
      implement
      bitstring_map$found_value<value><value> (v) = v
    in
      find_value_in_bitstring_map<value><key, value> (map, key)
    end

  implement {key, value}
  bitstring_map_get_eff (map, key, value) =
    find_value_in_bitstring_map_eff (map, key, value)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Functions for setting an entry in or removing an entry
  // from the map.

  implement {key, value}
  bitstring_map_set (map, key, value) =
    set_value_in_bitstring_map (map, key, value)

  implement {key, value}
  bitstring_map_remove (map, key) =
    remove_value_from_bitstring_map (map, key)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Cursor operations: walking through the tree.
  //
  // Cursors are immutable. Currently they are implemented
  // as ordinary lists.
  //

  primplement
  lemma_bitstring_map_cursor_param cursor =
    case+ cursor of
      | bitstring_map_cursor_nil () => ()
      | bitstring_map_cursor_nonnil _ => ()

  implement {}
  bitstring_map_cursor_is_nil cursor =
    case+ cursor of
      | bitstring_map_cursor_nil () => true
      | bitstring_map_cursor_nonnil _ => false

  implement {key, value}
  bitstring_map_first map = find_first_entry map
  implement {key, value}
  bitstring_map_last map = find_last_entry map
  implement {key, value}
  bitstring_map_find (map, key) = find_entry_by_key (map, key)
  implement {key, value}
  bitstring_map_next cursor = move_to_next cursor
  implement {key, value}
  bitstring_map_prev cursor = move_to_prev cursor

  implement {key, value}
  bitstring_map_pair cursor = get_pair cursor
  implement {key, value}
  bitstring_map_pair_exn cursor =
    let
      val cursor_is_nil = bitstring_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        $raise BitstringMapCursorNilExn ()
      else
        get_pair cursor
    end

  implement {key, value}
  bitstring_map_pair_eff (cursor, key, value) =
    let
      val @(k, v) = bitstring_map_pair<key, value> cursor
    in
      key := k;
      value := v
    end
  implement {key, value}
  bitstring_map_pair_effexn (cursor, key, value) =
    let
      val @(k, v) = bitstring_map_pair_exn<key, value> cursor
    in
      key := k;
      value := v
    end

  implement {key, value}
  bitstring_map_key cursor = get_key cursor
  implement {key, value}
  bitstring_map_key_exn cursor =
    let
      val cursor_is_nil = bitstring_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        $raise BitstringMapCursorNilExn ()
      else
        get_key cursor
    end

  implement {key, value}
  bitstring_map_value cursor = get_value cursor
  implement {key, value}
  bitstring_map_value_exn cursor =
    let
      val cursor_is_nil = bitstring_map_cursor_is_nil cursor
    in
      if cursor_is_nil then
        $raise BitstringMapCursorNilExn ()
      else
        get_value cursor
    end

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //
  // Functions for dumping a map.

  implement {key, value}
  _dump_bitstring_map (map, dump_pair) =
    dump_bitstring_map (map, dump_pair)

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
end

//--------------------------------------------------------------------
//
// Modules.
//

implement {key, value}
make_bitstring_map_module () =
  let
    typedef map_t (size : int) = bitstring_map (key, value, size)
    typedef map_t = bitstring_map (key, value)
    typedef bitstring_map_t (size : int) = bitstring_map (key, value, size)
    typedef bitstring_map_t = bitstring_map (key, value)
    typedef cursor_t (addr : addr) = bitstring_map_cursor (key, value, addr)
    typedef cursor_t = bitstring_map_cursor (key, value)

    //
    // Operations not involving keys.
    //

    fn is_nil {size : int} (map : map_t size) :<>
        [b : bool | (size == 0 && b == true) || (0 < size && b == false)]
        bool b =
      bitstring_map_is_nil<> map

    fn get_size {size : int} (map : map_t size) : size_t size =
      bitstring_map_size<> map

    fn has_key (map : map_t, key : key) : bool =
      bitstring_map_has_key<key, value> (map, key)

    fn get (map : map_t, key : key) : Option value =
      bitstring_map_get<key, value> (map, key)

    fn get_exn (map : map_t, key : key) : value =
      bitstring_map_get_exn<key, value> (map, key)

    fn get_eff (map : map_t, key : key, value : &value? >> opt (value, b)) :
        #[b : bool] bool b =
      bitstring_map_get_eff<key, value> (map, key, value)

    fn set {orig_size : nat} (map : map_t orig_size, key : key, value : value) :
        [new_size : int | (orig_size == 0 && new_size == 1) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size + 1)]
        map_t new_size =
      bitstring_map_set<key, value> (map, key, value)

     fn remove {orig_size : nat} (map : map_t orig_size, key : key) :
        [new_size : int | (orig_size == 0 && new_size == 0) ||
                          (0 < orig_size && new_size == orig_size) ||
                          (0 < orig_size && new_size == orig_size - 1)]
        map_t new_size =
      bitstring_map_remove<key, value> (map, key)

    fn cursor_is_nil {addr : addr} (cursor : cursor_t addr) :<>
        [b : bool | (addr == null && b == true) || (addr != null && b == false)]
        bool b =
      bitstring_map_cursor_is_nil cursor

    fn first (map : map_t) : cursor_t =
      bitstring_map_first<key, value> map

    fn last (map : map_t) : cursor_t =
      bitstring_map_last<key, value> map

    fn find (map : map_t, key : key) : cursor_t =
      bitstring_map_find<key, value> (map, key)

    fn next (cursor : cursor_t) : cursor_t =
      bitstring_map_next<key, value> cursor

    fn prev (cursor : cursor_t) : cursor_t =
      bitstring_map_prev<key, value> cursor

    fn get_pair {addr : addr | addr != null} (cursor : cursor_t addr) :
        @(key, value) =
      bitstring_map_pair<key, value> cursor

    fn get_pair_exn (cursor : cursor_t) : @(key, value) =
      bitstring_map_pair_exn<key, value> cursor

    fn get_pair_effexn (cursor : cursor_t, key : &key? >> key,
                        value : &value? >> value) : void =
      bitstring_map_pair_effexn<key, value> (cursor, key, value)

    fn get_key {addr : addr | addr != null} (cursor : cursor_t addr) : key =
      bitstring_map_key<key, value> cursor

    fn get_key_exn (cursor : cursor_t) : key =
      bitstring_map_key_exn<key, value> cursor

    fn get_value {addr : addr | addr != null} (cursor : cursor_t addr) : value =
      bitstring_map_value<key, value> cursor

    fn get_value_exn (cursor : cursor_t) : value =
      bitstring_map_value_exn<key, value> cursor
  in
    '{
      nil = bitstring_map_nil,
      is_nil = is_nil,
      size = get_size,
      has_key = has_key,
      get = get,
      get_exn = get_exn,
      get_eff = get_eff,
      set = set,
      remove = remove,
      cursor_is_nil = cursor_is_nil,
      first = first,
      last = last,
      find = find,
      next = next,
      prev = prev,
      pair = get_pair,
      pair_exn = get_pair_exn,
      pair_effexn = get_pair_effexn,
      key = get_key,
      key_exn = get_key_exn,
      value = get_value,
      value_exn = get_value_exn
    }
  end

//--------------------------------------------------------------------
