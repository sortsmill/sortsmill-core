// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

(*--------------------------------------------------------------------*)

#define ATS_DYNLOADFLAG 0

local

  staload "sortsmill/core/SATS/arith2_prf.sats"
  staload UN = "prelude/SATS/unsafe.sats"

in

  (*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

  primplement pos_mul_pos_pos {m, n} () =
    (* Proof by reductio ad absurdum. *)
    sif m <= 0 then
      case+ 0 of
        | _ =/=> mul_lte_gte_lte {m, n} ()
    else
      ()

  primplement pos_mul_neg_neg {m, n} () =
    (* Proof by reductio ad absurdum. *)
    sif m <= 0 then
      case+ 0 of
        | _ =/=> mul_lte_lte_gte {m, n} ()
    else
      ()

  primplement neg_mul_pos_neg {m, n} () =
    (* Proof by reductio ad absurdum. *)
    sif 0 <= m then
      case+ 0 of
        | _ =/=> mul_gte_gte_gte {m, n} ()
    else
      ()

  primplement neg_mul_neg_pos {m, n} () =
    (* Proof by reductio ad absurdum. *)
    sif 0 <= m then
      case+ 0 of
        | _ =/=> mul_gte_lte_lte {m, n} ()
    else
      ()

  (*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

  primplement pos_mul_pos_pos2 {m, n} {p} pf_mul =
    ()
     where
      {
        prval () = mul_elim pf_mul
        prval () = pos_mul_pos_pos {m, n} {p} ()
      }

  primplement pos_mul_neg_neg2 {m, n} {p} pf_mul =
    ()
     where
      {
        prval () = mul_elim pf_mul
        prval () = pos_mul_neg_neg {m, n} {p} ()
      }

  primplement neg_mul_pos_neg2 {m, n} {p} pf_mul =
    ()
     where
      {
        prval () = mul_elim pf_mul
        prval () = neg_mul_pos_neg {m, n} {p} ()
      }

  primplement neg_mul_neg_pos2 {m, n} {p} pf_mul =
    ()
     where
      {
        prval () = mul_elim pf_mul
        prval () = neg_mul_neg_pos {m, n} {p} ()
      }

  primplement mul_eqz_eqz_eqz {x, y} () =
    let
      prval MULbas () = mul_make {x, y} ()
    in
    end

  primplement mul_eqz_neqz_eqz {x, y} () =
    let
      prval MULbas () = mul_make {x, y} ()
    in
    end

  primplement mul_neqz_eqz_eqz {x, y} () =
    let
      prval MULbas () = mul_make {y, x} ()
    in
    end

  primplement mul_neqz_neqz_neqz {x, y} () =
    sif x < 0 then
      sif y < 0 then
        mul_pos_pos_pos (mul_make {~x, ~y} ())
      else
        mul_pos_pos_pos (mul_make {~x, y} ())
    else sif y < 0 then
      mul_pos_pos_pos (mul_make {x, ~y} ())
    else
      mul_pos_pos_pos (mul_make {x, y} ())

  primplement mul_eqz_eqz_eqz2 {x, y} pf_mul =
    let
      prval () = mul_elim pf_mul
    in
      mul_eqz_eqz_eqz {x, y} ()
    end

  primplement mul_eqz_neqz_eqz2 {x, y} pf_mul =
    let
      prval () = mul_elim pf_mul
    in
      mul_eqz_neqz_eqz {x, y} ()
    end

  primplement mul_neqz_eqz_eqz2 {x, y} pf_mul =
    let
      prval () = mul_elim pf_mul
    in
      mul_neqz_eqz_eqz {x, y} ()
    end

  primplement mul_neqz_neqz_neqz2 {x, y} pf_mul =
    let
      prval () = mul_elim pf_mul
    in
      mul_neqz_neqz_neqz {x, y} ()
    end

  primplement eqz_eqz_mul_eqz {x, y} () =
    sif x == 0 then
      sif y == 0 then
        mul_eqz_eqz_eqz {x, y} ()
      else
        mul_eqz_neqz_eqz {x, y} ()
    else sif y == 0 then
      mul_neqz_eqz_eqz {x, y} ()
    else
      mul_neqz_neqz_neqz {x, y} ()

  primplement neqz_neqz_mul_neqz {x, y} () =
    sif x == 0 then
      sif y == 0 then
        mul_eqz_eqz_eqz {x, y} ()
      else
        mul_eqz_neqz_eqz {x, y} ()
    else sif y == 0 then
      mul_neqz_eqz_eqz {x, y} ()
    else
      mul_neqz_neqz_neqz {x, y} ()

  primplement eqz_eqz_mul_eqz2 {x, y} pf_mul =
    let
      prval () = mul_elim pf_mul
    in
      eqz_eqz_mul_eqz {x, y} ()
    end

  primplement neqz_neqz_mul_neqz2 {x, y} pf_mul =
    let
      prval () = mul_elim pf_mul
    in
      neqz_neqz_mul_neqz {x, y} ()
    end

  primplement mul_mul_substitute {a, b} {c} {d, e} (pf_mul1, pf_mul2) =
    let
      prval pf_mul = mul_istot {a, b} ()
      prval () = mul_elim pf_mul
      and () = mul_elim pf_mul1
      and () = mul_elim pf_mul2
    in
      pf_mul
    end

  primplement mul_mul_substitute2 {a, b} {c} {d, e} (pf_mul1, pf_mul2) =
    let
      prval pf_mul = mul_istot {a * d, e} ()
      and pf_mul0 = mul_mul_substitute (pf_mul1, pf_mul2)
      prval () = mul_elim pf_mul
      and () = mul_elim pf_mul0
    in
      pf_mul
    end

  (*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

  primplement divmod_qualitative {x, y} {q, r} pf_divmod =
    sif x < y then
      divmod_isfun (pf_divmod,
                    mul_to_divmod_given_remainder2 {x} (mul_make {0, y} ()))
    else sif x == y then
      divmod_isfun (pf_divmod, mul_to_divmod_remainderless (mul_make {1, y} ()))
    else
      ()
       where
        {
          prval pfd = div_istot {x, y} ()
          prval pfdm = divmod_istot {x, y} ()
          prval () = divmod_isfun (pfdm, pf_divmod)
          prval () = divmod_isfun (pfd, pfdm)
          prval pf_mul = divmod_elim pf_divmod
          prval () = pos_mul_pos_pos2 pf_mul
        }

  primplement mul_to_divmod_given_remainder {r} {x, y} {xy} pf_mul =
    let
      prval [q1 : int, r1 : int] pf_divmod = divmod_istot {xy + r, x} ()

      (* FIXME FIXME FIXME: Can one get rid of this unsafe cast? *)
      prval EQINT () = $UN.castview0 {EQINT (q1, y)} 0

      prval () = mul_isfun (mul_commute pf_mul, divmod_elim pf_divmod)
    in
      pf_divmod
    end

  primplement mul_to_divmod_given_remainder2 {r} {x, y} {xy} pf_mul =
    mul_to_divmod_given_remainder (mul_commute pf_mul)

  primplement mul_to_divmod_remainderless {x, y} {xy} pf_mul =
    let
      prval [x1 : int, r1 : int] pf_mod = divmod_istot {xy, y} ()
      prval [xy1 : int] pf_mul2 = divmod_elim pf_mod

      (* FIXME FIXME FIXME: Can one get rid of this unsafe cast? *)
      prval EQINT () = $UN.castview0 {EQINT (x, x1)} 0

      prval () = mul_isfun (pf_mul, pf_mul2)

      (* Not needed for the proof, but worth noting. *)
      prval EQINT () = eqint_make {xy1, xy} ()
      prval EQINT () = eqint_make {x1, x} ()
      prval EQINT () = eqint_make {r1, 0} ()
    in
      pf_mod
    end

  primplement mul_to_mod_remainderless pf_mul =
    mul_to_divmod_remainderless pf_mul

  primplement divisor_hops_from_itself_to_zero {x, y} pf_mod =
    let
      prval [p : int] pf_mul = divmod_elim pf_mod
      prval EQINT () = eqint_make {x, p} ()

      prval [q : int, r : int] pf_divmodtot = divmod_istot {x, y} ()
      prval () = divmod_isfun (pf_divmodtot, pf_mod)
      prval EQINT () = eqint_make {r, 0} ()

      (* x and y are positive; therefore q also is positive. *)
      prval () = pos_mul_pos_pos2 {q, y} {x} pf_mul
    in
      recursion {q, y} {x} pf_mul
       where
        {
          prfun recursion
              {s, v : pos} {u : pos} .< s >. (pf : MUL (s, v, u)) :<prf>
                [u == v || 2 * v <= u] void =
            sif s == 1 then
              ()
               where
                {
                  prval MULind (MULbas ()) = pf
                }
            else
              recursion {s - 1, v} {u - v} pf1
               where
                {
                  prval pf1 = mul_add_const {~1} pf
                  prval pf2 = mul_make {s - 1, v} ()
                  prval () = mul_isfun (pf1, pf2)
                  prval EQINT () = eqint_make {(s - 1) * v, u - v} ()
                  prval () = mul_pos_pos_pos {s - 1, v} pf2
                }
        }
    end

  (*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)
  
  primplement positive_power_of_two_times_anything_is_even
      {p} {s} {m} pf_exp2 =
    begin
      sif m < 0 then
        let
          prval () = exp2_ispos pf_exp2
          prval (pf_mul1, pf_even1) = recursion {p} {s} {~m} pf_exp2
          prval pf_mul = mul_negate2 pf_mul1
          prval pf_even = even_negate pf_even1
        in
          (pf_mul, pf_even)
        end
      else sif m == 0 then
        let
          prval pf_mul = mul_make {s, 0} ()
          prval pf_even = divmod_istot {0, 2} ()
          prval () = divmod_mul_elim pf_even
        in
          (pf_mul, pf_even)
        end
      else
        recursion pf_exp2
    end
     where
      {
        prfun recursion
            {p : pos} {s : int} {m : pos} .< p >.
            (pf_exp2 : EXP2 (p, s)) :<prf>
              [sm : int | sgn sm == sgn m] (MUL (s, m, sm), EVEN (sm)) =
          sif p == 1 then
            let
              prval EXP2ind (EXP2bas ()) = pf_exp2
              prval pf_mul = mul_make {2, m} ()
              prval pf_even = divmod_istot {2 * m, 2} ()
              prval () = divmod_mul_elim pf_even
            in
              (pf_mul, pf_even)
            end
          else
            let
              prval () = exp2_ispos pf_exp2

              prval EXP2ind pf_exp2_1 = pf_exp2
              prval [s1 : int] pf1 = exp2_istot {p - 1} ()
              prval () = exp2_isfun (pf1, pf_exp2_1)
              prval (pf_mul1, pf_even1) = recursion {p - 1} {s1} {m} pf_exp2_1

              prval EQINT () = eqint_make {2 * s1, s} ()

              prval [sm : int] pf_mul = mul_istot {s, m} ()
              prval () = mul_pos_pos_pos pf_mul
              prval () = mul_elim pf_mul1
              and   () = mul_elim pf_mul

              prval pf_even = divmod_istot {sm, 2} ()
              prval () = divmod_mul_elim pf_even1
              and   () = divmod_mul_elim pf_even
            in
              (pf_mul, pf_even)
            end              
      }

  primplement odd_divided_by_positive_power_of_two
      {m} {p} {s} (pf_odd, pf_exp2) =
    (* Proof by reductio ad absurdum. *)
    let
      prval () = exp2_ispos pf_exp2
      prval [q : int, r : int] pf_mod = divmod_istot {m, s} ()
      prval [p : int] pf_mul = divmod_elim pf_mod

      (* Note that q * s + r = m. *)
      prval () = divmod_mul_elim pf_mod
      prval EQINT () = eqint_make {q * s + r, m} ()

      prval EQINT () = eqint_make {q * s, p} ()
    in
      sif r == 0 then
        case+ 0 of
          | _ =/=>
            ()
             where
              {
                prval [sm : int] (pf_sm, pf_sm_even) =
                  positive_power_of_two_times_anything_is_even (pf_exp2)

                (* Establishes that 0 < sm. *)
                prval () = mul_isfun (pf_sm, mul_commute pf_mul)

                (* Document some equivalences. *)
                prval EQINT () = eqint_make {p, sm} ()
                prval EQINT () = eqint_make {m, sm} ()

                prval () = divmod_mul_elim pf_sm_even
                prval () = divmod_mul_elim pf_odd
              }
      else
        let
          prval [qr : int, rr : int] pf_r_odd = divmod_istot {r, 2} ()
          prval [pr : int] pf_r_mul = divmod_elim pf_r_odd
        in
          sif rr == 0 then
            case+ 0 of
              | _ =/=>
                ()
                 where
                  {

                    (* If rr = 0, then q * s + r = m must be even; but
                       this contradicts pf_odd. *)

                    prval [sm : int] (pf_sm, pf_sm_even) =
                      positive_power_of_two_times_anything_is_even (pf_exp2)
                    prval () = mul_isfun (pf_sm, mul_commute pf_mul)
                    prval () =
                      mul_elim (mul_distribute2 (divmod_elim pf_sm_even,
                                                 pf_r_mul))

                    prval () = divmod_mul_elim pf_odd
                  }
          else
            (pf_mod, pf_r_odd)
        end
    end

  primplement other_is_odd (pf_either_or_both_odd, pf_first_even) =
    let
      prval (pf1, pf2) = pf_either_or_both_odd
      prval () = divmod_isfun (pf1, pf_first_even)
    in
      pf2
    end

  (*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  *)

end

(*--------------------------------------------------------------------*)
