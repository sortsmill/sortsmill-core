/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_C_FPRINTF_H_
#define SORTSMILL_CORE_C_FPRINTF_H_

/*
 * c_fprintf: like fprintf but always in the C locale.
 */

#include <stdio.h>
#include <sortsmill/core/attributes.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif


/* *INDENT-OFF* */

int c_fprintf (FILE *stream, const char *format, ...)
  STM_ATTRIBUTE_FORMAT_PRINTF (2, 3);

/* *INDENT-ON* */

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_C_FPRINTF_H_ */
