/* -*- coding: utf-8 -*- */
/*
 * Copyright (C) 2013, 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_DOUBLE_CHECKED_LOCKING_H_
#define SORTSMILL_CORE_DOUBLE_CHECKED_LOCKING_H_

/*
 * Primitives for double-checked locking.  See
 * http://www.hpl.hp.com/research/linux/atomic_ops/example.php4
 *
 * The method avoids the need to lock a mutex to use a variable once
 * it has been initialized.
 *
 * WARNING: Thread-safety here depends, in general, on a working
 * <atomic_ops.h>. You can use the code without atomic_ops, but in
 * that case you are simply accepting the risk of operations being
 * performed in the wrong order.
 *
 * Example use:
 *
 *    #include <pthread.h>
 *    #include <sortsmill/core/double_checked_locking.h>
 *
 *    extern void initialize_v (some_type_t *vptr);
 *
 *    volatile stm_dcl_indicator_t v_is_initialized = false;
 *    pthread_mutex_t v_mutex = PTHREAD_MUTEX_INITIALIZER;
 *    some_type_t v;
 *
 *    some_type_t
 *    get_v (void)
 *    {
 *      if (!stm_dcl_load_indicator (&v_is_initialized))
 *        {
 *          pthread_mutex_lock (&v_mutex);
 *          if (!v_is_initialized)
 *            {
 *              initialize_v (&v);
 *              stm_dcl_store_indicator (&v_is_initialized, true);
 *            }
 *          pthread_mutex_unlock (&v_mutex);
 *        }
 *      return v;
 *    }
 */

#include <stdbool.h>
#include <sortsmill/smcore-buildinfo.h>

#if SMCORE_N_WITH_ATOMIC_OPS

#include <atomic_ops.h>

typedef AO_t stm_dcl_indicator_t;

#define stm_dcl_load_indicator(p)               \
  ((bool) (AO_load_acquire_read (p)))

#define stm_dcl_store_indicator(p, value)       \
  ((void) AO_store_release_write ((p), (bool) (value)))

#else /* !SMCORE_N_WITH_ATOMIC_OPS */

#ifdef __GNUC__
#warning Compiling without atomic_ops support.
#endif

typedef bool stm_dcl_indicator_t;

#define stm_dcl_load_indicator(p) (*(p))
#define stm_dcl_store_indicator(p, value) ((void) (*(p) = (value)))

#endif /* !SMCORE_N_WITH_ATOMIC_OPS */

#endif /* SORTSMILL_CORE_DOUBLE_CHECKED_LOCKING_H_ */
