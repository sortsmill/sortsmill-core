/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_CYCLICTRIDIAGSOLVE_H_
#define SORTSMILL_CORE_CYCLICTRIDIAGSOLVE_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Solve a `cyclic tridiagonal' (sometimes called `periodic
 * tridiagonal') n-by-n matrix, by using the Sherman-Morrison formula
 * to treat the problem as a perturbed tridiagonal system. The
 * tridiagonal system is solved in linear time by gaussian elimination
 * with partial pivoting.
 *
 * FIXME: Document these routines more fully.
 */

void scyclictridiagsolve (int n, int nrhs, float subdiag[n - 1],
                          float diag[n], float superdiag[n - 1],
                          float lower_left, float upper_right,
                          float rhs_and_workspace[nrhs + 1][n], int *info);
void dcyclictridiagsolve (int n, int nrhs, double subdiag[n - 1],
                          double diag[n], double superdiag[n - 1],
                          double lower_left, double upper_right,
                          double rhs_and_workspace[nrhs + 1][n], int *info);
void lcyclictridiagsolve (int n, int nrhs, long double subdiag[n - 1],
                          long double diag[n], long double superdiag[n - 1],
                          long double lower_left, long double upper_right,
                          long double rhs_and_workspace[nrhs + 1][n],
                          int *info);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_CYCLICTRIDIAGSOLVE_H_ */
