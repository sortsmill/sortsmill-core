/* -*- C -*-
 *
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_INTEGER_MAPS_H_
#define SORTSMILL_CORE_INTEGER_MAPS_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#define SMCORE_INTEGER_MAP_BASE_MODULE_T(KEY, VALUE,    \
                                         MAP, CURSOR)   \
  struct                                                \
  {                                                     \
    size_t (*size) (MAP);                               \
    int (*has_key) (MAP, KEY);                          \
    VALUE (*get) (MAP, KEY);                            \
    int (*get_eff) (MAP, KEY, VALUE *);                 \
    MAP (*set) (MAP, KEY, VALUE);                       \
    MAP (*remove) (MAP, KEY);                           \
    CURSOR (*first) (MAP);                              \
    CURSOR (*last) (MAP);                               \
    CURSOR (*find) (MAP, KEY);                          \
    CURSOR (*next) (CURSOR);                            \
    CURSOR (*prev) (CURSOR);                            \
    KEY (*key) (CURSOR);                                \
    VALUE (*value) (CURSOR);                            \
    void (*pair_eff) (CURSOR, KEY *, VALUE *);          \
  }

/*---------------------------------------------------------------------*/

/*   *INDENT-OFF*    */

/*
 * the_uintptr_to_uintptr_integer_map_base_module():
 *
 * A module used to make other modules out of keys and values that are
 * no bigger than a uintptr_t.
 */
typedef struct _uintptr_to_uintptr_integer_map__ *uintptr_to_uintptr_integer_map_t;
typedef struct _uintptr_to_uintptr_integer_map_cursor__ *uintptr_to_uintptr_integer_map_cursor_t;
#define UINTPTR_TO_UINTPTR_INTEGER_MAP_BASE_MODULE_T                    \
  SMCORE_INTEGER_MAP_BASE_MODULE_T (uintptr_t, uintptr_t,               \
                                    uintptr_to_uintptr_integer_map_t,   \
                                    uintptr_to_uintptr_integer_map_cursor_t)
typedef UINTPTR_TO_UINTPTR_INTEGER_MAP_BASE_MODULE_T uintptr_to_uintptr_integer_map_base_module_t;
STM_ATTRIBUTE_PURE uintptr_to_uintptr_integer_map_base_module_t *the_uintptr_to_uintptr_integer_map_base_module (void);

/*   *INDENT-ON*    */

/*---------------------------------------------------------------------*/

/*
 * Integer map modules for keys and values that are no bigger than a
 * uintptr_t.
 */

/*
 * Example declarations:
 *
 *   // Abstract types for the maps and cursors.
 *   typedef struct iwidget_dict *iwidget_t;
 *   typedef struct iwidget_cursor *iwidget_cursor_t;
 *
 *   // A macro defined mainly so the following typedef
 *   // will not confuse tools such as Emacs.
 *   #define IWIDGET_MODULE_T                                       \
 *     INTEGER_MAP_MODULE_T (const char *, uint32_t, iwidget_t *,   \
 *                           iwidget_t, iwidget_cursor_t)
 *
 *   // The type of a iwidget dictionary module.
 *   typedef IWIDGET_MODULE_T iwidget_module_t;
 *
 */
#define INTEGER_MAP_MODULE_T(KEY, VALUE, MAP, CURSOR)   \
  struct                                                \
  {                                                     \
    size_t (*size) (MAP);                               \
    bool (*has_key) (MAP, KEY);                         \
    VALUE (*get) (MAP, KEY);                            \
    void (*get_eff) (MAP, KEY, bool *, VALUE *);        \
    MAP (*set) (MAP, KEY, VALUE);                       \
    MAP (*remove) (MAP, KEY);                           \
    CURSOR (*first) (MAP);                              \
    CURSOR (*last) (MAP);                               \
    CURSOR (*find) (MAP, KEY);                          \
    CURSOR (*next) (CURSOR);                            \
    CURSOR (*prev) (CURSOR);                            \
    KEY (*key) (CURSOR);                                \
    VALUE (*value) (CURSOR);                            \
    void (*pair_eff) (CURSOR, KEY *, VALUE *);          \
  }

/*
 * The FROM_KEY, TO_KEY, FROM_VALUE, and TO_VALUE parameters may be
 * simple casts, functions, etc. Type mappings are as follows:
 *
 *       FROM_KEY : KEY -> uintptr_t
 *         TO_KEY : uintptr_t -> KEY
 *     FROM_VALUE : VALUE -> uintptr_t
 *       TO_VALUE : uintptr_t -> VALUE
 *
 */
#define DEFINE_INTEGER_MAP_MODULE_T_FUNCTIONS__(STATIC, KEY, VALUE,     \
                                                MAP, CURSOR,            \
                                                FROM_KEY, TO_KEY,       \
                                                FROM_VALUE, TO_VALUE)   \
                                                                        \
  STATIC size_t                                                         \
  x_##MAP##_size (MAP map)                                              \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return the_uintptr_to_uintptr_integer_map_base_module ()->size (m); \
  }                                                                     \
                                                                        \
  STATIC bool                                                           \
  x_##MAP##_has_key (MAP map, KEY key)                                  \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return (bool)                                                       \
      the_uintptr_to_uintptr_integer_map_base_module ()->               \
      has_key (m, FROM_KEY (key));                                      \
  }                                                                     \
                                                                        \
  STATIC VALUE                                                          \
  x_##MAP##_get (MAP map, KEY key)                                      \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return                                                              \
      TO_VALUE (the_uintptr_to_uintptr_integer_map_base_module ()->     \
                get (m, FROM_KEY (key)));                               \
  }                                                                     \
                                                                        \
  STATIC void                                                           \
  x_##MAP##_get_eff (MAP map, KEY key, bool *found, VALUE *value)       \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    uintptr_t v;                                                        \
    *found = (bool)                                                     \
      the_uintptr_to_uintptr_integer_map_base_module ()->               \
      get_eff (m, FROM_KEY (key), &v);                                  \
    *value = TO_VALUE (v);                                              \
  }                                                                     \
                                                                        \
  STATIC MAP                                                            \
  x_##MAP##_set (MAP map, KEY key, VALUE value)                         \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return (MAP) (void *)                                               \
      the_uintptr_to_uintptr_integer_map_base_module ()->               \
      set (m, FROM_KEY (key), FROM_VALUE (value));                      \
  }                                                                     \
                                                                        \
  STATIC MAP                                                            \
  x_##MAP##_remove (MAP map, KEY key)                                   \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return (MAP) (void *)                                               \
      the_uintptr_to_uintptr_integer_map_base_module ()->               \
      remove (m, FROM_KEY (key));                                       \
  }                                                                     \
                                                                        \
  STATIC CURSOR                                                         \
  x_##MAP##_first (MAP map)                                             \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return (CURSOR) (void *)                                            \
      the_uintptr_to_uintptr_integer_map_base_module ()->first (m);     \
  }                                                                     \
                                                                        \
  STATIC CURSOR                                                         \
  x_##MAP##_last (MAP map)                                              \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return (CURSOR) (void *)                                            \
      the_uintptr_to_uintptr_integer_map_base_module ()->last (m);      \
  }                                                                     \
                                                                        \
  STATIC CURSOR                                                         \
  x_##MAP##_find (MAP map, KEY key)                                     \
  {                                                                     \
    uintptr_to_uintptr_integer_map_t m =                                \
      (uintptr_to_uintptr_integer_map_t) (void *) map;                  \
    return (CURSOR) (void *)                                            \
      the_uintptr_to_uintptr_integer_map_base_module ()->               \
      find (m, FROM_KEY (key));                                         \
  }                                                                     \
                                                                        \
  STATIC CURSOR                                                         \
  x_##MAP##_next (CURSOR cursor)                                        \
  {                                                                     \
    uintptr_to_uintptr_integer_map_cursor_t c =                         \
      (uintptr_to_uintptr_integer_map_cursor_t) (void *) cursor;        \
    return (CURSOR) (void *)                                            \
      the_uintptr_to_uintptr_integer_map_base_module ()->next (c);      \
  }                                                                     \
                                                                        \
  STATIC CURSOR                                                         \
  x_##MAP##_prev (CURSOR cursor)                                        \
  {                                                                     \
    uintptr_to_uintptr_integer_map_cursor_t c =                         \
      (uintptr_to_uintptr_integer_map_cursor_t) (void *) cursor;        \
    return (CURSOR) (void *)                                            \
      the_uintptr_to_uintptr_integer_map_base_module ()->prev (c);      \
  }                                                                     \
                                                                        \
  STATIC KEY                                                            \
  x_##MAP##_key (CURSOR cursor)                                         \
  {                                                                     \
    uintptr_to_uintptr_integer_map_cursor_t c =                         \
      (uintptr_to_uintptr_integer_map_cursor_t) (void *) cursor;        \
    return                                                              \
      TO_KEY (the_uintptr_to_uintptr_integer_map_base_module ()->       \
              key (c));                                                 \
  }                                                                     \
                                                                        \
  STATIC VALUE                                                          \
  x_##MAP##_value (CURSOR cursor)                                       \
  {                                                                     \
    uintptr_to_uintptr_integer_map_cursor_t c =                         \
      (uintptr_to_uintptr_integer_map_cursor_t) (void *) cursor;        \
    return                                                              \
      TO_VALUE (the_uintptr_to_uintptr_integer_map_base_module ()->     \
                value (c));                                             \
  }                                                                     \
                                                                        \
  STATIC void                                                           \
  x_##MAP##_pair_eff (CURSOR cursor, KEY *key, VALUE *value)            \
  {                                                                     \
    uintptr_to_uintptr_integer_map_cursor_t c =                         \
      (uintptr_to_uintptr_integer_map_cursor_t) (void *) cursor;        \
    uintptr_t k;                                                        \
    uintptr_t v;                                                        \
    the_uintptr_to_uintptr_integer_map_base_module ()->                 \
      pair_eff (c, &k, &v);                                             \
    *key = TO_KEY (k);                                                  \
    *value = TO_VALUE (v);                                              \
  }

#define DECLARE_INTEGER_MAP_MODULE_T_FUNCTIONS__(STATIC, KEY, VALUE,    \
                                                 MAP, CURSOR)           \
  STATIC size_t x_##MAP##_size (MAP map);                               \
  STATIC bool x_##MAP##_has_key (MAP map, KEY key);                     \
  STATIC VALUE x_##MAP##_get (MAP map, KEY key);                        \
  STATIC void x_##MAP##_get_eff (MAP map, KEY key,                      \
                                 bool *found, VALUE *value);            \
  STATIC MAP x_##MAP##_set (MAP map, KEY key, VALUE value);             \
  STATIC MAP x_##MAP##_remove (MAP map, KEY key);                       \
  STATIC CURSOR x_##MAP##_first (MAP map);                              \
  STATIC CURSOR x_##MAP##_last (MAP map);                               \
  STATIC CURSOR x_##MAP##_find (MAP map, KEY key);                      \
  STATIC CURSOR x_##MAP##_next (CURSOR cursor);                         \
  STATIC CURSOR x_##MAP##_prev (CURSOR cursor);                         \
  STATIC KEY x_##MAP##_key (CURSOR cursor);                             \
  STATIC VALUE x_##MAP##_value (CURSOR cursor);                         \
  STATIC void x_##MAP##_pair_eff (CURSOR cursor, KEY *key, VALUE *value);

#define INTEGER_MAP_MODULE_INITIALIZER__(KEY, VALUE, MAP, CURSOR)       \
  {   .size = x_##MAP##_size,                                           \
      .has_key = x_##MAP##_has_key,                                     \
      .get = x_##MAP##_get,                                             \
      .get_eff = x_##MAP##_get_eff,                                     \
      .set = x_##MAP##_set,                                             \
      .remove = x_##MAP##_remove,                                       \
      .first = x_##MAP##_first,                                         \
      .last = x_##MAP##_last,                                           \
      .find = x_##MAP##_find,                                           \
      .next = x_##MAP##_next,                                           \
      .prev = x_##MAP##_prev,                                           \
      .key = x_##MAP##_key,                                             \
      .value = x_##MAP##_value,                                         \
      .pair_eff = x_##MAP##_pair_eff   }

/*
 * Examples of the MODULE parameter for a module named
 * iwidget_map_module, of type iwidget_map_module_t:
 *
 *   iwidget_map_module_t iwidget_map_module
 *
 *   static const iwidget_map_module_t iwidget_map_module
 *
 *   __attribute__((__visibility__("default"))) const
 *       iwidget_map_module_t iwidget_map_module
 *
 *
 * The FROM_KEY, TO_KEY, FROM_VALUE, and TO_VALUE parameters may be
 * simple casts, functions, etc. Type mappings are as follows:
 *
 *       FROM_KEY : KEY -> uintptr_t
 *         TO_KEY : uintptr_t -> KEY
 *     FROM_VALUE : VALUE -> uintptr_t
 *       TO_VALUE : uintptr_t -> VALUE
 *
 */
#define DEFINE_INTEGER_MAP_MODULE(MODULE, KEY, VALUE, MAP, CURSOR,      \
                                  FROM_KEY, TO_KEY,                     \
                                  FROM_VALUE, TO_VALUE)                 \
  DECLARE_INTEGER_MAP_MODULE_T_FUNCTIONS__ (static, KEY, VALUE,         \
                                            MAP, CURSOR);               \
  MODULE =                                                              \
    INTEGER_MAP_MODULE_INITIALIZER__ (KEY, VALUE, MAP, CURSOR);         \
  DEFINE_INTEGER_MAP_MODULE_T_FUNCTIONS__ (static, KEY, VALUE,          \
                                           MAP, CURSOR,                 \
                                           FROM_KEY, TO_KEY,            \
                                           FROM_VALUE, TO_VALUE)


/*---------------------------------------------------------------------*/

/* *INDENT-OFF* */

/*
 * An example module that also may be useful: mapping uintptr_t to
 * uintptr_t.
 */

/* Abstract types for the maps and cursors. */
typedef struct _smcore_uintptr_to_uintptr_t__ *smcore_uintptr_to_uintptr_t;
typedef struct _smcore_uintptr_to_uintptr_cursor_t__ *smcore_uintptr_to_uintptr_cursor_t;

/* A macro defined mainly so the following typedef will not confuse
   tools such as Emacs. */
#define SMCORE_UINTPTR_TO_UINTPTR_MODULE_T                      \
  INTEGER_MAP_MODULE_T (uintptr_t, uintptr_t,                   \
                        smcore_uintptr_to_uintptr_t,            \
                        smcore_uintptr_to_uintptr_cursor_t)

/* The type of the module. */
typedef SMCORE_UINTPTR_TO_UINTPTR_MODULE_T smcore_uintptr_to_uintptr_module_t;

/* Declaration of the module. It is defined in integer_maps.c. */
extern const smcore_uintptr_to_uintptr_module_t smcore_uintptr_to_uintptr;

/* *INDENT-ON* */

/*---------------------------------------------------------------------*/

/* *INDENT-OFF* */

/*
 * Another example module that also may be useful: mapping intptr_t to
 * intptr_t.
 */

/* Abstract types for the maps and cursors. */
typedef struct _smcore_intptr_to_intptr_t__ *smcore_intptr_to_intptr_t;
typedef struct _smcore_intptr_to_intptr_cursor_t__ *smcore_intptr_to_intptr_cursor_t;

/* A macro defined mainly so the following typedef will not confuse
   tools such as Emacs. */
#define SMCORE_INTPTR_TO_INTPTR_MODULE_T                        \
  INTEGER_MAP_MODULE_T (intptr_t, intptr_t,                     \
                        smcore_intptr_to_intptr_t,              \
                        smcore_intptr_to_intptr_cursor_t)

/* The type of the module. */
typedef SMCORE_INTPTR_TO_INTPTR_MODULE_T smcore_intptr_to_intptr_module_t;

/* Declaration of the module. It is defined in integer_maps.c. */
extern const smcore_intptr_to_intptr_module_t smcore_intptr_to_intptr;

/* *INDENT-ON* */

/*---------------------------------------------------------------------*/

/* *INDENT-OFF* */

/*
 * Another example module that also may be useful: mapping uintptr_t
 * to void pointer.
 */

/* Abstract types for the maps and cursors. */
typedef struct _smcore_uintptr_to_voidp_t__ *smcore_uintptr_to_voidp_t;
typedef struct _smcore_uintptr_to_voidp_cursor_t__ *smcore_uintptr_to_voidp_cursor_t;

/* A macro defined mainly so the following typedef will not confuse
   tools such as Emacs. */
#define SMCORE_UINTPTR_TO_VOIDP_MODULE_T                        \
  INTEGER_MAP_MODULE_T (uintptr_t, void *,                      \
                        smcore_uintptr_to_voidp_t,              \
                        smcore_uintptr_to_voidp_cursor_t)

/* The type of the module. */
typedef SMCORE_UINTPTR_TO_VOIDP_MODULE_T smcore_uintptr_to_voidp_module_t;

/* Declaration of the module. It is defined in integer_maps.c. */
extern const smcore_uintptr_to_voidp_module_t smcore_uintptr_to_voidp;

/* *INDENT-ON* */

/*---------------------------------------------------------------------*/

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_INTEGER_MAPS_H_ */
