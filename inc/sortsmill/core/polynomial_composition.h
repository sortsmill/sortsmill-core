/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_COMPOSITION_H_
#define SORTSMILL_CORE_POLYNOMIAL_COMPOSITION_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Composition of two polynomials.  The result is a polynomial of
 * degree equal to the product.
 *
 * Given polynomials f(t) and g(t), find a third polynomial
 * h(t) = f(g(t)). You can view this operation as evaluating f at
 * the variable g, and then transforming variables from g to t.
 */

void mpq_poly_compose_mono (size_t degree_f, ssize_t stride_f, mpq_t *f,
                            size_t degree_g, ssize_t stride_g, mpq_t *g,
                            ssize_t stride_h, mpq_t *h);
void dpoly_compose_mono (size_t degree_f, ssize_t stride_f, const double *f,
                         size_t degree_g, ssize_t stride_g, const double *g,
                         ssize_t stride_h, double *h);
void dpoly_compose_berndc (size_t degree_f, ssize_t stride_f, const double *f,
                           size_t degree_g, ssize_t stride_g, const double *g,
                           ssize_t stride_h, double *h);
void dpoly_compose_bernh (size_t degree_f, ssize_t stride_f, const double *f,
                          size_t degree_g, ssize_t stride_g, const double *g,
                          ssize_t stride_h, double *h);
void dpoly_compose_sberndc (size_t degree_f, ssize_t stride_f, const double *f,
                            size_t degree_g, ssize_t stride_g, const double *g,
                            ssize_t stride_h, double *h);
void dpoly_compose_sbernh (size_t degree_f, ssize_t stride_f, const double *f,
                           size_t degree_g, ssize_t stride_g, const double *g,
                           ssize_t stride_h, double *h);
void dpoly_compose_spower (size_t degree_f, ssize_t stride_f, const double *f,
                           size_t degree_g, ssize_t stride_g, const double *g,
                           ssize_t stride_h, double *h);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_COMPOSITION_H_ */
