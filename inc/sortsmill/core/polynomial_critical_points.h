/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_CRITICAL_POINTS_H_
#define SORTSMILL_CORE_POLYNOMIAL_CRITICAL_POINTS_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void poly_criticalpoints_mono (size_t degree, ssize_t stride, mpq_t *poly,
                               const mpq_t a, const mpq_t b,
                               size_t *num_points, double t[degree]);

void dpoly_criticalpoints_bernsv (size_t degree, ssize_t stride,
                                  const double *poly,
                                  double a, double b,
                                  size_t *num_points, double t[degree - 1]);
void dpoly_criticalpoints_berndc (size_t degree, ssize_t stride,
                                  const double *poly,
                                  double a, double b,
                                  size_t *num_points, double t[degree - 1]);
void dpoly_criticalpoints_sbernsv (size_t degree, ssize_t stride,
                                   const double *poly,
                                   double a, double b,
                                   size_t *num_points, double t[degree - 1]);
void dpoly_criticalpoints_sberndc (size_t degree, ssize_t stride,
                                   const double *poly,
                                   double a, double b,
                                   size_t *num_points, double t[degree - 1]);
void dpoly_criticalpoints_spower (size_t degree, ssize_t stride,
                                  const double *poly,
                                  double a, double b,
                                  size_t *num_points, double t[degree - 1]);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_CRITICAL_POINTS_H_ */
