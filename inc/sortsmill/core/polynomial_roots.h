/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_ROOTS_H_
#define SORTSMILL_CORE_POLYNOMIAL_ROOTS_H_

#include <stdlib.h>
#include <stdbool.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

// Budan’s 0_1 roots test.
size_t mpq_budan_0_1 (size_t degree, ssize_t stride, mpq_t *poly);

typedef struct polynomial_root_interval_t
{
  struct polynomial_root_interval_t *next;
  mpq_t a;                      /* Left bound, or the value of an exact root. */
  mpq_t b;                      /* Right bound, or unused if the root is exact. */
  bool root_is_exact;
} polynomial_root_interval_t;

void clear_polynomial_root_interval_list (polynomial_root_interval_t *lst);
size_t polynomial_root_interval_list_length (polynomial_root_interval_t *lst);

/* *INDENT-OFF* */

polynomial_root_interval_t *
isolate_roots_of_square_free_polynomial (size_t degree, ssize_t stride,
                                         mpq_t *poly, const mpq_t a,
                                         const mpq_t b);

/* *INDENT-ON* */

/*
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
//
// CHANGE THE NAMES OF THESE TWO ROUTINES.
//
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
*/
void dpoly_findroots (size_t degree, ssize_t stride, mpq_t *poly,
                      const mpq_t a, const mpq_t b,
                      double (*eval) (double, void *), void *data,
                      bool *identically_zero,
                      size_t *root_count, double *roots,
                      ssize_t *multiplicities);
void dpoly_findroots2 (size_t degree, ssize_t stride, const double *poly,
                       double a, double b,
                       double (*eval) (double, void *), void *data,
                       bool *identically_zero,
                       size_t *root_count, double *roots,
                       ssize_t *multiplicities);

void dpoly_findroots_bernsv (size_t degree, ssize_t stride, const double *poly,
                             double a, double b, bool *identically_zero,
                             size_t *root_count, double *roots);
void dpoly_findroots_berndc (size_t degree, ssize_t stride, const double *poly,
                             double a, double b, bool *identically_zero,
                             size_t *root_count, double *roots);
void dpoly_findroots_sbernsv (size_t degree, ssize_t stride, const double *poly,
                              double a, double b, bool *identically_zero,
                              size_t *root_count, double *roots);
void dpoly_findroots_sberndc (size_t degree, ssize_t stride, const double *poly,
                              double a, double b, bool *identically_zero,
                              size_t *root_count, double *roots);
void dpoly_findroots_spower (size_t degree, ssize_t stride, const double *poly,
                             double a, double b, bool *identically_zero,
                             size_t *root_count, double *roots);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_ROOTS_H_ */
