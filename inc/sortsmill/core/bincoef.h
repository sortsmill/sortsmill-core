/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 * This file is part of Sorts Mill Tools.
 * 
 * Sorts Mill Tools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_BINCOEF_H_
#define SORTSMILL_CORE_BINCOEF_H_

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#include <stdint.h>
#include <gmp.h>

/* The binary coefficient C(n,k). */
uintmax_t bincoef (uintmax_t n, uintmax_t k);
void mpz_bincoef_ui (mpz_t C, uintmax_t n, uintmax_t k);
void mpq_bincoef_ui (mpq_t C, uintmax_t n, uintmax_t k);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_BINCOEF_H_ */
