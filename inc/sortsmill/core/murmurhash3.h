/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 * This file is part of Sorts Mill Tools.
 * 
 * Sorts Mill Tools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_MURMURHASH3_H_
#define SORTSMILL_CORE_MURMURHASH3_H_

/*
 * The MurmurHash3 hash functions.
 *
 * These functions are not for cryptography. The 32-bit hashes should
 * be good for hashmaps, and the 128-bit hashes should be good for
 * creating unique identifiers of blocks of data.
 *
 * See also <sortsmill/core/hash_functions.h>
 */

#include <stdlib.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void smcore_murmurhash3_x86_32 (const void *key, int len, uint32_t seed,
                                void *out);
void smcore_murmurhash3_x86_128 (const void *key, int len, uint32_t seed,
                                 void *out);
void smcore_murmurhash3_x64_128 (const void *key, int len, uint32_t seed,
                                 void *out);

#define smcore_murmurhash3_32 smcore_murmurhash3_x86_32
#if ULONG_MAX <= 0xFFFFFFFFUL
#define smcore_murmurhash3_128 smcore_murmurhash3_x86_128
#else
#define smcore_murmurhash3_128 smcore_murmurhash3_x64_128
#endif

static inline uint32_t
smcore_uint32_murmurhash3 (const void *key, int len, uint32_t seed)
{
  uint32_t hash_value;
  smcore_murmurhash3_32 (key, len, seed, &hash_value);
  return hash_value;
}

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_MURMURHASH3_H_ */
