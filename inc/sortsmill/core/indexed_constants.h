/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_INDEXED_CONSTANTS_H_
#define SORTSMILL_CORE_INDEXED_CONSTANTS_H_

#include <sortsmill/smcore-buildinfo.h>
#include <sortsmill/core/xgc.h> /* Includes gc.h and pthreads.h in the
                                   right order. */
#include <sortsmill/core/x_alloc.h>
#if SMCORE_N_WITH_ATOMIC_OPS
#include <atomic_ops.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#if SMCORE_N_WITH_ATOMIC_OPS

typedef AO_t stm_indexed_constant_AO_t;

#define stm_indexed_constant_load(p) (AO_load_acquire_read (p))
#define stm_indexed_constant_store(p, value)            \
  ((void) AO_store_release_write ((p), (value)))

#else                           /* !SMCORE_N_WITH_ATOMIC_OPS */

#ifdef __GNUC__
#warning Compiling without atomic_ops support.
#endif

typedef size_t stm_indexed_constant_AO_t;

#define stm_indexed_constant_load(p) (*(p))
#define stm_indexed_constant_store(p, value) ((void) (*(p) = (value)))

#endif /* !SMCORE_N_WITH_ATOMIC_OPS */

#if SMCORE_N_ENABLE_THREADS

#define STM_INDEXED_CONSTANTS(ATTRIBUTES, TYPE, NAME, INITIALIZER)      \
                                                                        \
  static volatile stm_indexed_constant_AO_t NAME##__size__ = 0;         \
                                                                        \
  static TYPE *NAME##__storage__ = NULL;                                \
                                                                        \
  pthread_mutex_t NAME##__mutex__ = PTHREAD_MUTEX_INITIALIZER;          \
                                                                        \
  ATTRIBUTES TYPE                                                       \
  NAME (size_t i)                                                       \
  {                                                                     \
    /* Ensure the array of one-time-initialized values    */            \
    /* is big enough.                                     */            \
    if (stm_indexed_constant_load (&NAME##__size__) <= i)               \
      {                                                                 \
        pthread_mutex_lock (&NAME##__mutex__);                          \
        if (NAME##__size__ <= i)                                        \
          {                                                             \
            /* Allocate enough storage for i+1 entries. */              \
            NAME##__storage__ =                                         \
              x_realloc (NAME##__storage__, (i + 1) * sizeof (TYPE));   \
            /* Initialize all the new entries. */                       \
            for (size_t j = NAME##__size__; j <= i; j++)                \
              NAME##__storage__[j] = (INITIALIZER (j));                 \
            stm_indexed_constant_store (&NAME##__size__, i + 1);        \
          }                                                             \
        pthread_mutex_unlock (&NAME##__mutex__);                        \
      }                                                                 \
                                                                        \
    /* The desired value is available now. Return it. */                \
    return NAME##__storage__[i];                                        \
  }

#else /* !SMCORE_N_ENABLE_THREADS */

#ifdef __GNUC__
#warning Compiling without threads support.
#endif

/* There probably is no point in using atomic_ops here without
   threads, but there should be no harm, either. */

#define STM_INDEXED_CONSTANTS(ATTRIBUTES, TYPE, NAME, INITIALIZER)      \
                                                                        \
  static volatile stm_indexed_constant_AO_t NAME##__size__ = 0;         \
                                                                        \
  static TYPE *NAME##__storage__ = NULL;                                \
                                                                        \
  ATTRIBUTES TYPE                                                       \
  NAME (size_t i)                                                       \
  {                                                                     \
    /* Ensure the array of one-time-initialized values    */            \
    /* is big enough.                                     */            \
    if (stm_indexed_constant_load (&NAME##__size__) <= i)               \
      {                                                                 \
        /* Allocate enough storage for i+1 entries. */                  \
        NAME##__storage__ =                                             \
          x_realloc (NAME##__storage__, (i + 1) * sizeof (TYPE));       \
        /* Initialize all the new entries. */                           \
        for (size_t j = NAME##__size__; j <= i; j++)                    \
          NAME##__storage__[j] = (INITIALIZER (j));                     \
        stm_indexed_constant_store (&NAME##__size__, i + 1);            \
      }                                                                 \
                                                                        \
    /* The desired value is available now. Return it. */                \
    return NAME##__storage__[i];                                        \
  }

#endif /* !SMCORE_N_ENABLE_THREADS */

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_INDEXED_CONSTANTS_H_ */
