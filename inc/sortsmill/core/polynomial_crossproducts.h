/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_CROSSPRODUCTS_H_
#define SORTSMILL_CORE_POLYNOMIAL_CROSSPRODUCTS_H_

#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Treating two parametric polynomial splines as vector-valued
 * functions in the plane, compute their cross product. The result
 * will be a real-valued polynomial in t. (The cross product of
 * vectors in the plane always is normal to the plane; the result
 * _implicitly_ is vector-valued.)
 */

void dpoly_crossproduct_mono (size_t degree_a,
                              ssize_t stride_ax, const double *ax,
                              ssize_t stride_ay, const double *ay,
                              size_t degree_b,
                              ssize_t stride_bx, const double *bx,
                              ssize_t stride_by, const double *by,
                              ssize_t stride_cross, double *cross);
void dpoly_crossproduct_bern (size_t degree_a,
                              ssize_t stride_ax, const double *ax,
                              ssize_t stride_ay, const double *ay,
                              size_t degree_b,
                              ssize_t stride_bx, const double *bx,
                              ssize_t stride_by, const double *by,
                              ssize_t stride_cross, double *cross);
void dpoly_crossproduct_sbern (size_t degree_a,
                               ssize_t stride_ax, const double *ax,
                               ssize_t stride_ay, const double *ay,
                               size_t degree_b,
                               ssize_t stride_bx, const double *bx,
                               ssize_t stride_by, const double *by,
                               ssize_t stride_cross, double *cross);
void dpoly_crossproduct_spower (size_t degree_a,
                                ssize_t stride_ax, const double *ax,
                                ssize_t stride_ay, const double *ay,
                                size_t degree_b,
                                ssize_t stride_bx, const double *bx,
                                ssize_t stride_by, const double *by,
                                ssize_t stride_cross, double *cross);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_CROSSPRODUCTS_H_ */
