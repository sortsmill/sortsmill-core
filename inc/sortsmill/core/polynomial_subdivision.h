/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_SUBDIVISION_H_
#define SORTSMILL_CORE_POLYNOMIAL_SUBDIVISION_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*----------------------------------------------------------------------
 *
 * `Portion' routines.
 *
 * Given a polynomial on [a,b]. Via change of variables, map that
 * portion of the polynomial onto [0,1]. (Except for truncation errors
 * and so forth, this operation is equivalent to subdivision twice by
 * de Casteljau's algorithm.)
 */

void mpq_poly_portion_mono (size_t degree, ssize_t stride, mpq_t *poly,
                            const mpq_t a, const mpq_t b,
                            ssize_t result_stride, mpq_t *result);
void dpoly_portion_mono (size_t degree, ssize_t stride, const double *poly,
                         double a, double b,
                         ssize_t result_stride, double *result);
void dpoly_portion_berndc (size_t degree, ssize_t stride, const double *poly,
                           double a, double b,
                           ssize_t result_stride, double *result);
void dpoly_portion_bernh (size_t degree, ssize_t stride, const double *poly,
                          double a, double b,
                          ssize_t result_stride, double *result);
void dpoly_portion_sberndc (size_t degree, ssize_t stride, const double *poly,
                            double a, double b,
                            ssize_t result_stride, double *result);
void dpoly_portion_sbernh (size_t degree, ssize_t stride, const double *poly,
                           double a, double b,
                           ssize_t result_stride, double *result);
void dpoly_portion_spower (size_t degree, ssize_t stride, const double *poly,
                           double a, double b,
                           ssize_t result_stride, double *result);

/*----------------------------------------------------------------------
 *
 * `Subdivision' routines.
 *
 * Given a polynomial on [0,1], in the ordinary monomial basis. Using
 * de Casteljau's algorithm, map both of the portions [0,t] and [t,1]
 * of the polynomial onto [0,1].
 */

void dpoly_subdiv_bern (size_t degree, ssize_t stride, const double *poly,
                        double t, ssize_t stride_a, double *a, ssize_t stride_b,
                        double *b);
void dpoly_subdiv_sbern (size_t degree, ssize_t stride, const double *poly,
                         double t, ssize_t stride_a, double *a,
                         ssize_t stride_b, double *b);

/*--------------------------------------------------------------------*/

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_SUBDIVISION_H_ */
