/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_DEGREES_H_
#define SORTSMILL_CORE_POLYNOMIAL_DEGREES_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* Return the minimum degree (that is, the ‘actual’ degree from a
   mathematical point of view). This operation is not implemented for
   the Bernstein and scaled Bernstein bases. */
size_t mpq_poly_mindegree_mono (size_t degree, ssize_t stride, mpq_t *poly);
size_t dpoly_mindegree_mono (size_t degree, ssize_t stride, const double *poly);
size_t dpoly_mindegree_spower (size_t degree, ssize_t stride,
                               const double *poly);

/* Copying is put in this module due to its being a trivial case of
   changing degree. */
void mpq_poly_copy (size_t degree, ssize_t stride, mpq_t *poly,
                    ssize_t new_stride, mpq_t *new_poly);
void dpoly_copy (size_t degree, ssize_t stride, const double *poly,
                 ssize_t new_stride, double *new_poly);

/* mpq_poly_changedegree_mono() truncates terms if you reduce the
   degree. */
void mpq_poly_changedegree_mono (size_t degree, ssize_t stride, mpq_t *poly,
                                 size_t new_degree, ssize_t new_stride,
                                 mpq_t *new_poly);

/* dpoly_changedegree_mono() truncates terms if you reduce the
   degree. In general it gives results significantly different from
   those of dpoly_changedegree_bern(), dpoly_changedegree_sbern(), and
   dpoly_changedegree_spower(). In particular, the value of a spline
   at t=1 may be changed wildly by degree reduction in monomial
   basis. */
void dpoly_changedegree_mono (size_t degree, ssize_t stride,
                              const double *poly,
                              size_t new_degree, ssize_t new_stride,
                              double *new_poly);

/* Degree reduction in Bernstein and scaled Bernstein basis is done by
   lowering the degree in symmetric power basis, using
   dpoly_changedegree_spower(). */
void dpoly_changedegree_bern (size_t degree, ssize_t stride,
                              const double *poly,
                              size_t new_degree, ssize_t new_stride,
                              double *new_poly);
void dpoly_changedegree_sbern (size_t degree, ssize_t stride,
                               const double *poly,
                               size_t new_degree, ssize_t new_stride,
                               double *new_poly);

/* dpoly_changedegree_spower() removes terms from the middle if you
   reduce the degree. */
void dpoly_changedegree_spower (size_t degree, ssize_t stride,
                                const double *poly,
                                size_t new_degree, ssize_t new_stride,
                                double *new_poly);

double dpoly_degreechangeerrorbound_mono (size_t degree, ssize_t stride,
                                          const double *poly,
                                          size_t new_degree);
double dpoly_degreechangeerrorbound_spower (size_t degree, ssize_t stride,
                                            const double *poly,
                                            size_t new_degree);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_DEGREES_H_ */
