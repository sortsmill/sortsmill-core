/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_FLATTENING_H_
#define SORTSMILL_CORE_POLYNOMIAL_FLATTENING_H_

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*--------------------------------------------------------------------*/

enum
{
  SMCORE__FLATTENING_MAX_NUM_POINTS_EXCEEDED = 1
};

/*
 * Flatten the curve. The two output arrays give the x and y
 * coordinates, respectively, of the segment endpoints.
 */
void dpoly_flatten_spower (size_t degree_a, ssize_t stride_ax, const double *ax,
                           ssize_t stride_ay, const double *ay,
                           double absolute_tolerance, double relative_tolerance,
                           size_t max_num_points, size_t *num_points,
                           double *x, double *y, double *t, int *info);

/* These functions call dpoly_flatten_spower() to do the actual
   work. */
void dpoly_flatten_mono (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay,
                         double absolute_tolerance, double relative_tolerance,
                         size_t max_num_points, size_t *num_points,
                         double *x, double *y, double *t, int *info);
void dpoly_flatten_bern (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay,
                         double absolute_tolerance, double relative_tolerance,
                         size_t max_num_points, size_t *num_points,
                         double *x, double *y, double *t, int *info);
void dpoly_flatten_sbern (size_t degree_a, ssize_t stride_ax, const double *ax,
                          ssize_t stride_ay, const double *ay,
                          double absolute_tolerance, double relative_tolerance,
                          size_t max_num_points, size_t *num_points,
                          double *x, double *y, double *t, int *info);

/*--------------------------------------------------------------------*/

typedef struct _flattening_stream__ *flattening_stream_t;

flattening_stream_t flattening_stream_make_spower (size_t degree_a,
                                                   ssize_t stride_ax,
                                                   const double *ax,
                                                   ssize_t stride_ay,
                                                   const double *ay,
                                                   double absolute_tolerance,
                                                   double relative_tolerance);
void flattening_stream_point (flattening_stream_t p,
                              double *x, double *y, double *t);
flattening_stream_t flattening_stream_next (flattening_stream_t p);
bool flattening_stream_done (flattening_stream_t p);

/*--------------------------------------------------------------------*/

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_FLATTENING_H_ */
