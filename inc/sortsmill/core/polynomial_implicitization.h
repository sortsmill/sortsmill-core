/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_IMPLICITIZATION_H_
#define SORTSMILL_CORE_POLYNOMIAL_IMPLICITIZATION_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void mpq_poly_implicitize_mono (size_t degree,
                                ssize_t stride_a, mpq_t *a,
                                ssize_t stride_b, mpq_t *b,
                                mpq_t implicit_equation[degree + 1][degree + 1],
                                int *info);

void mpq_poly_plugintoimplicit_mono (size_t degree1,
                                     mpq_t
                                     implicit_eq[degree1 + 1][degree1 + 1],
                                     size_t degree2, ssize_t stride_x, mpq_t *x,
                                     ssize_t stride_y, mpq_t *y,
                                     ssize_t result_stride, mpq_t *result);

void dpoly_implicitize_mono (size_t degree,
                             ssize_t stride_x, const double *x,
                             ssize_t stride_y, const double *y,
                             double implicit_equation[degree + 1][degree + 1],
                             int *info);
void dpoly_implicitize_bern (size_t degree,
                             ssize_t stride_x, const double *x,
                             ssize_t stride_y, const double *y,
                             double implicit_equation[degree + 1][degree + 1],
                             int *info);
void dpoly_implicitize_sbern (size_t degree,
                              ssize_t stride_x, const double *x,
                              ssize_t stride_y, const double *y,
                              double implicit_equation[degree + 1][degree + 1],
                              int *info);
void dpoly_implicitize_spower (size_t degree,
                               ssize_t stride_x, const double *x,
                               ssize_t stride_y, const double *y,
                               double implicit_equation[degree + 1][degree + 1],
                               int *info);

void dpoly_plugintoimplicit_mono (size_t degree1,
                                  double implicit_eq[degree1 + 1][degree1 + 1],
                                  size_t degree2,
                                  ssize_t stride_x, const double *x,
                                  ssize_t stride_y, const double *y,
                                  ssize_t result_stride, double *result);
void dpoly_plugintoimplicit_bern (size_t degree1,
                                  double implicit_eq[degree1 + 1][degree1 + 1],
                                  size_t degree2,
                                  ssize_t stride_x, const double *x,
                                  ssize_t stride_y, const double *y,
                                  ssize_t result_stride, double *result);
void dpoly_plugintoimplicit_sbern (size_t degree1,
                                   double implicit_eq[degree1 + 1][degree1 + 1],
                                   size_t degree2,
                                   ssize_t stride_x, const double *x,
                                   ssize_t stride_y, const double *y,
                                   ssize_t result_stride, double *result);
void dpoly_plugintoimplicit_spower (size_t degree1,
                                    double implicit_eq[degree1 + 1][degree1 +
                                                                    1],
                                    size_t degree2, ssize_t stride_x,
                                    const double *x, ssize_t stride_y,
                                    const double *y, ssize_t result_stride,
                                    double *result);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_IMPLICITIZATION_H_ */
