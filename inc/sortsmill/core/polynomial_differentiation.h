/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_DIFFERENTIATION_H_
#define SORTSMILL_CORE_POLYNOMIAL_DIFFERENTIATION_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Formal derivative of a polynomial.  The result is a polynomial of
 * degree one less than that of the given polynomial, except that the
 * degree cannot go below zero.
 */

void mpq_poly_deriv_mono (size_t degree, ssize_t stride, mpq_t *poly,
                          ssize_t result_stride, mpq_t *result);

void dpoly_deriv_mono (size_t degree, ssize_t stride, const double *poly,
                       ssize_t deriv_stride, double *deriv);
void dpoly_deriv_bern (size_t degree, ssize_t stride, const double *poly,
                       ssize_t deriv_stride, double *deriv);
void dpoly_deriv_sbern (size_t degree, ssize_t stride, const double *poly,
                        ssize_t deriv_stride, double *deriv);
void dpoly_deriv_spower (size_t degree, ssize_t stride, const double *poly,
                         ssize_t deriv_stride, double *deriv);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_DIFFERENTIATION_H_ */
