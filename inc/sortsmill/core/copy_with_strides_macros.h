/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_COPY_WITH_STRIDES_MACROS_H_
#define SORTSMILL_CORE_COPY_WITH_STRIDES_MACROS_H_

#include <stdbool.h>
#include <sortsmill/core/attributes.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#if STM_HAVE_VARIADIC_MACROS

#include <sortsmill/core/copy_with_strides_macros__.h>

#define __COPY_WITH_STRIDES_COPY(TYPE, DEST, SRC, ...)  \
  (*(DEST) = *(SRC))

/*
  A non-hygienic macro for copying data with strides, where the data
  elements are of some arbitrary addressible, assignable type. The
  source and destination locations should not overlap (though they may
  be intertwined). The value of DEST_STRIDE should not be zero.

  These variable names are shadowed:

  __COPYX_WITH_STRIDES__count
  __COPYX_WITH_STRIDES__dest_stride
  __COPYX_WITH_STRIDES__src_stride
  __COPYX_WITH_STRIDES__dest
  __COPYX_WITH_STRIDES__src
  __COPYX_WITH_STRIDES__k
*/
#define COPY_WITH_STRIDES(TYPE, DEST, SRC, DEST_STRIDE, SRC_STRIDE,     \
                          COUNT)                                        \
  COPYX_WITH_STRIDES (TYPE, (DEST), (SRC), (DEST_STRIDE),               \
                      (SRC_STRIDE), (COUNT), __COPY_WITH_STRIDES_COPY, )

/*
  A non-hygienic macro for copying two-dimensional data with strides,
  where the data elements are of some arbitrary addressible,
  assignable type. The source and destination locations should not
  overlap (though they may be intertwined). Neither of the values of
  DIM0_DEST_STRIDE and DIM1_DEST_STRIDE should equal zero.

  These variable names are shadowed:

  __COPYX_WITH_STRIDES__count
  __COPYX_WITH_STRIDES__dest_stride
  __COPYX_WITH_STRIDES__src_stride
  __COPYX_WITH_STRIDES__dest
  __COPYX_WITH_STRIDES__src
  __COPYX_WITH_STRIDES__k
  __COPYX_WITH_STRIDES_0__count
  __COPYX_WITH_STRIDES_0__dest_stride
  __COPYX_WITH_STRIDES_0__src_stride
  __COPYX_WITH_STRIDES_0__dest
  __COPYX_WITH_STRIDES_0__src
  __COPYX_WITH_STRIDES_0__k
*/
#define COPY2_WITH_STRIDES(TYPE, DEST, SRC, DIM0_DEST_STRIDE,           \
                           DIM0_SRC_STRIDE, DIM0_COUNT,                 \
                           DIM1_DEST_STRIDE, DIM1_SRC_STRIDE,           \
                           DIM1_COUNT)                                  \
  COPYX_WITH_STRIDES_0 (TYPE, (DEST), (SRC), (DIM0_DEST_STRIDE),        \
                        (DIM0_SRC_STRIDE), (DIM0_COUNT),                \
                        COPY_WITH_STRIDES, (DIM1_DEST_STRIDE),          \
                        (DIM1_SRC_STRIDE), (DIM1_COUNT))

/*
  A non-hygienic macro for copying data with strides and multiplicity,
  where the data elements are of some arbitrary addressible,
  assignable type. The source and destination locations should not
  overlap (though they may be intertwined). The magnitude of
  DEST_STRIDE should not be less than MULTIPLICITY.

  The same variable names are shadowed as for COPY2_WITH_STRIDES.
*/
#define COPY_WITH_STRIDESM(TYPE, DEST, SRC, DEST_STRIDE, SRC_STRIDE,    \
                           COUNT, MULTIPLICITY)                         \
  COPY2_WITH_STRIDES (TYPE, (DEST), (SRC), (DEST_STRIDE), (SRC_STRIDE), \
                      (COUNT), 1, 1, (MULTIPLICITY))

/*
  A non-hygienic macro for copying two-dimensional data with strides
  and multiplicity, where the data elements are of some arbitrary
  addressible, assignable type. The source and destination locations
  should not overlap (though they may be
  intertwined). DIM0_DEST_STRIDE should not equal zero, and the
  magnitude of DIM1_DEST_STRIDE should not be less than MULTIPLICITY.

  These variable names are shadowed:

  __COPYX_WITH_STRIDES__count
  __COPYX_WITH_STRIDES__dest_stride
  __COPYX_WITH_STRIDES__src_stride
  __COPYX_WITH_STRIDES__dest
  __COPYX_WITH_STRIDES__src
  __COPYX_WITH_STRIDES__k
  __COPYX_WITH_STRIDES_0__count
  __COPYX_WITH_STRIDES_0__dest_stride
  __COPYX_WITH_STRIDES_0__src_stride
  __COPYX_WITH_STRIDES_0__dest
  __COPYX_WITH_STRIDES_0__src
  __COPYX_WITH_STRIDES_0__k
  __COPYX_WITH_STRIDES_1__count
  __COPYX_WITH_STRIDES_1__dest_stride
  __COPYX_WITH_STRIDES_1__src_stride
  __COPYX_WITH_STRIDES_1__dest
  __COPYX_WITH_STRIDES_1__src
  __COPYX_WITH_STRIDES_1__k
*/
#define COPY2_WITH_STRIDESM(TYPE, DEST, SRC, DIM0_DEST_STRIDE,          \
                            DIM0_SRC_STRIDE, DIM0_COUNT,                \
                            DIM1_DEST_STRIDE, DIM1_SRC_STRIDE,          \
                            DIM1_COUNT, MULTIPLICITY)                   \
  COPYX_WITH_STRIDES_1 (TYPE, (DEST), (SRC), (DIM0_DEST_STRIDE),        \
                        (DIM0_SRC_STRIDE), (DIM0_COUNT),                \
                        COPY_WITH_STRIDESM, (DIM1_DEST_STRIDE),         \
                        (DIM1_SRC_STRIDE), (DIM1_COUNT), (MULTIPLICITY))

#endif                          /* STM_HAVE_VARIADIC_MACROS */

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_COPY_WITH_STRIDES_MACROS_H_ */
