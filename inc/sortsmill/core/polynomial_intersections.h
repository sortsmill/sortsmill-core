/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_INTERSECTIONS_H_
#define SORTSMILL_CORE_POLYNOMIAL_INTERSECTIONS_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*--------------------------------------------------------------------*/

void intersect_implicit_with_parametric (size_t degree_a,
                                         mpq_t
                                         implicit_a[degree_a + 1][degree_a + 1],
                                         size_t degree_b,
                                         ssize_t stride_bx, mpq_t *bx,
                                         ssize_t stride_by, mpq_t *by,
                                         const mpq_t t0, const mpq_t t1,
                                         bool *the_curves_are_the_same,
                                         size_t *num_intersections,
                                         double t[degree_a * degree_b]);

typedef struct
{
  double x;
  double y;
  size_t num_ta;
  double *ta;
  size_t num_tb;
  double *tb;
} plane_intersections_entry_t;

typedef struct
{
  size_t num_xy;
  plane_intersections_entry_t *xy;
} plane_intersections_t;

plane_intersections_t *poly_findintersections_mono
  (size_t degree_a,
   ssize_t stride_ax, mpq_t *ax,
   ssize_t stride_ay, mpq_t *ay,
   mpq_t (*implicit_a)[degree_a + 1],
   const mpq_t ta0, const mpq_t ta1,
   size_t degree_b,
   ssize_t stride_bx, mpq_t *bx,
   ssize_t stride_by, mpq_t *by,
   mpq_t (*implicit_b)[degree_b + 1],
   const mpq_t tb0, const mpq_t tb1, double tolerance);

plane_intersections_t *dpoly_findintersections_bern
  (size_t degree_a,
   ssize_t stride_ax, const double *ax,
   ssize_t stride_ay, const double *ay,
   mpq_t (*implicit_a)[degree_a + 1], double ta0, double ta1,
   size_t degree_b,
   ssize_t stride_bx, const double *bx,
   ssize_t stride_by, const double *by,
   mpq_t (*implicit_b)[degree_b + 1], double tb0, double tb1, double tolerance);

/*--------------------------------------------------------------------*/

#if 0                           /* Commented out in favor of ‘pure’
                                   implicitization, but left here
                                   temporarily for reference. */

typedef struct _float_plane_intersection__
{
  float t1;
  float t2;
  float x;
  float y;
  struct _float_plane_intersection__ *next;
} float_plane_intersection_t;

size_t float_plane_intersection_list_length (float_plane_intersection_t *lst);
float_plane_intersection_t *reverse_float_plane_intersection_list_in_place
  (float_plane_intersection_t *lst);

float_plane_intersection_t *spoly_findintersections_bernsv
  (size_t degree_a,
   ssize_t stride_ax, const float *ax,
   ssize_t stride_ay, const float *ay,
   size_t degree_b,
   ssize_t stride_bx, const float *bx,
   ssize_t stride_by, const float *by, float tolerance);

float_plane_intersection_t *spoly_findintersections_berndc
  (size_t degree_a,
   ssize_t stride_ax, const float *ax,
   ssize_t stride_ay, const float *ay,
   size_t degree_b,
   ssize_t stride_bx, const float *bx,
   ssize_t stride_by, const float *by, float tolerance);

#endif /* Commented out in favor of ‘pure’ implicitization, but left
          here temporarily for reference. */

/*--------------------------------------------------------------------*/

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_INTERSECTIONS_H_ */
