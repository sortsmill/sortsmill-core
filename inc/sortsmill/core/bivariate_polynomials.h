/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_BIVARIATE_POLYNOMIALS_H_
#define SORTSMILL_CORE_BIVARIATE_POLYNOMIALS_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void mpq_bipoly_init (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1]);
void mpq_bipoly_clear (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1]);

void mpq_bipoly_add (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                     size_t degree_b, mpq_t b[degree_b + 1][degree_b + 1],
                     mpq_t sum
                     [((degree_a < degree_b) ? degree_b : degree_a) + 1]
                     [((degree_a < degree_b) ? degree_b : degree_a) + 1]);
void mpq_bipoly_sub (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                     size_t degree_b, mpq_t b[degree_b + 1][degree_b + 1],
                     mpq_t difference
                     [((degree_a < degree_b) ? degree_b : degree_a) + 1]
                     [((degree_a < degree_b) ? degree_b : degree_a) + 1]);
void mpq_bipoly_mul (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                     size_t degree_b, mpq_t b[degree_b + 1][degree_b + 1],
                     mpq_t product
                     [degree_a + degree_b + 1][degree_a + degree_b + 1]);
void mpq_bipoly_scalarmul (size_t degree_a, mpq_t a[degree_a + 1][degree_a + 1],
                           const mpq_t r,
                           mpq_t product[degree_a + 1][degree_a + 1]);
void mpq_bipoly_partial_deriv_wrt_x (size_t degree_a,
                                     mpq_t a[degree_a + 1][degree_a + 1],
                                     mpq_t Dx[degree_a][degree_a]);
void mpq_bipoly_partial_deriv_wrt_y (size_t degree_a,
                                     mpq_t a[degree_a + 1][degree_a + 1],
                                     mpq_t Dy[degree_a][degree_a]);

void dbipoly_add (size_t degree_a, double a[degree_a + 1][degree_a + 1],
                  size_t degree_b, double b[degree_b + 1][degree_b + 1],
                  double sum
                  [((degree_a < degree_b) ? degree_b : degree_a) + 1]
                  [((degree_a < degree_b) ? degree_b : degree_a) + 1]);
void dbipoly_sub (size_t degree_a, double a[degree_a + 1][degree_a + 1],
                  size_t degree_b, double b[degree_b + 1][degree_b + 1],
                  double difference
                  [((degree_a < degree_b) ? degree_b : degree_a) + 1]
                  [((degree_a < degree_b) ? degree_b : degree_a) + 1]);
void dbipoly_mul (size_t degree_a, double a[degree_a + 1][degree_a + 1],
                  size_t degree_b, double b[degree_b + 1][degree_b + 1],
                  double product
                  [degree_a + degree_b + 1][degree_a + degree_b + 1]);
void dbipoly_scalarmul (size_t degree_a,
                        double a[degree_a + 1][degree_a + 1],
                        double r, double product[degree_a + 1][degree_a + 1]);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_BIVARIATE_POLYNOMIALS_H_ */
