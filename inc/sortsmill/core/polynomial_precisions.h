/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_PRECISIONS_H_
#define SORTSMILL_CORE_POLYNOMIAL_PRECISIONS_H_

#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Conversion between precisions.
 *
 * Like most of our other polynomial routines, these can be used to
 * overwrite the same memory used for their own input. A possible use
 * is to convert the precision of the polynomial stored in a `union'
 * record.
 */

void dpoly_to_spoly (size_t degree, ssize_t stride1, const double *poly1,
                     ssize_t stride2, float *poly2);
void spoly_to_dpoly (size_t degree, ssize_t stride1, const float *poly1,
                     ssize_t stride2, double *poly2);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_PRECISIONS_H_ */
