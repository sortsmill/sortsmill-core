/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_SORTING_H_
#define SORTSMILL_CORE_SORTING_H_

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * A stable insertion sort. This is efficient only for very small
 * arrays. In general, it is better to use smcore_stable_sort().
 *
 * The `compar' function is used only to test for whether or not the
 * first member is less than the second member. In other words, a
 * return value of zero is treated the same way as a positive return
 * value.
 */
void smcore_insertion_sort (void *base, size_t nmemb, size_t size,
                            int (*compar) (const void *, const void *, void *),
                            void *arg);

/*
 * A stable sort. The current implementation is a mixture of merge
 * sort and insertion sort.
 *
 * The `compar' function is used only to test for whether or not the
 * first member is less than the second member. In other words, a
 * return value of zero is treated the same way as a positive return
 * value.
 */
void smcore_stable_sort (void *base, size_t nmemb, size_t size,
                         int (*compar) (const void *, const void *, void *),
                         void *arg);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_SORTING_H_ */
