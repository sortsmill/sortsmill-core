/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_BISECTION_STREAMS_H_
#define SORTSMILL_CORE_BISECTION_STREAMS_H_

#include <stdlib.h>
#include <stdbool.h>
#include <sortsmill/core/xgc.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

typedef struct _bisection_stream__ *bisection_stream_t;
inline bisection_stream_t bisection_stream_make (double a, double b);
inline bisection_stream_t bisection_stream_cut (bisection_stream_t p, double c);
bisection_stream_t bisection_stream_cuts (bisection_stream_t p,
                                          size_t n, double c[n]);
inline bisection_stream_t bisection_stream_bisect (bisection_stream_t p);
inline bisection_stream_t bisection_stream_next (bisection_stream_t p);

/*
 * Do not use the struct _bisection_stream__ type directly. Instead
 * use bisection_stream_t, which is equivalent to a pointer to struct
 * _bisection_stream__.
 */
struct _bisection_stream__
{
  /* Public fields. Do not assign to them. */
  double a;
  double b;

  /* Private fields. */
  struct _bisection_stream__ *_next__;
};

inline bisection_stream_t
bisection_stream_make (double a, double b)
{
  struct _bisection_stream__ *p =
    x_gc_malloc (sizeof (struct _bisection_stream__));
  p->a = a;
  p->b = b;
  p->_next__ = NULL;
  return p;
}

inline bisection_stream_t
bisection_stream_cut (bisection_stream_t p, double c)
{
  /* Push the right half to be dealt with later. */
  struct _bisection_stream__ *right_half =
    x_gc_malloc (sizeof (struct _bisection_stream__));
  right_half->a = c;
  right_half->b = p->b;
  right_half->_next__ = p->_next__;
  p->_next__ = right_half;

  /* Make the left half be the new working interval. */
  p->b = c;

  return p;
}

inline bisection_stream_t
bisection_stream_bisect (bisection_stream_t p)
{
  /* Split the working interval in two. */
  return bisection_stream_cut (p, (p->a + p->b) / 2);
}

inline bisection_stream_t
bisection_stream_next (bisection_stream_t p)
{
  if (p->_next__ != NULL)
    *p = *p->_next__;           /* Pop a new working interval into place. */
  else
    p = NULL;                   /* There are no intervals remaining. */
  return p;
}

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_BISECTION_STREAMS_H_ */
