/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_IMMUTABLE_VECTORS_MISC_H_
#define SORTSMILL_CORE_IMMUTABLE_VECTORS_MISC_H_

#include <stdint.h>
#include <complex.h>
#include <sortsmill/core/immutable_vectors.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

typedef void *smcore_immutable_vector_voidp__;
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, voidp_ivect,
                                          smcore_immutable_vector_voidp__, 5);

/* The following are used to implement corresponding types in Sorts
   Mill Core Guile. */
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u8ivect, uint8_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u16ivect, uint16_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u32ivect, uint32_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u64ivect, uint64_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s8ivect, int8_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s16ivect, int16_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s32ivect, int32_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s64ivect, int64_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, f32ivect, float, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, f64ivect, double, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, c32ivect, float complex, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, c64ivect, double complex, 5);

/* The following are more efficient equivalents for when you know
   there will not be pointers to garbage collected allocations in the
   stored data. */
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u8ivect_atomic, uint8_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u16ivect_atomic, uint16_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u32ivect_atomic, uint32_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, u64ivect_atomic, uint64_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s8ivect_atomic, int8_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s16ivect_atomic, int16_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s32ivect_atomic, int32_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, s64ivect_atomic, int64_t, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, f32ivect_atomic, float, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, f64ivect_atomic, double, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, c32ivect_atomic, float complex, 5);
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, c64ivect_atomic, double complex, 5);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_IMMUTABLE_VECTORS_MISC_H_ */
