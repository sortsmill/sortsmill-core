/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * Based on count-one-bits.h from Gnulib, which was written by Ben
 * Pfaff and is:
 *
 * Copyright (C) 2007-2015 Free Software Foundation, Inc.
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POPCOUNT_H_
#define SORTSMILL_CORE_POPCOUNT_H_

/*
 * The original Gnulib code includes support for Microsoft C. This
 * implementation contains no such support.
 */

#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <sortsmill/core/attributes.h>

inline int smcore_popcount (unsigned int);
inline int smcore_popcountl (unsigned long int);
inline int smcore_popcountll (unsigned long long int);
inline int smcore_popcount32 (uint32_t);
inline int smcore_popcount64 (uint64_t);
inline int smcore_popcountz (size_t);
inline int smcore_popcountp (uintptr_t);

/* Expand to code that computes the number of 1-bits of the local
   variable 'x' of type TYPE (an unsigned integer type) and return it
   from the current function.  */
#define SMCORE_POPCOUNT_GENERIC__(TYPE)                                 \
  do                                                                    \
    {                                                                   \
      int count = 0;                                                    \
      int bits;                                                         \
      for (bits = 0; bits < sizeof (TYPE) * CHAR_BIT; bits += 32)       \
        {                                                               \
          count += _smcore_popcount_32__ (x);                           \
          x = x >> 31 >> 1;                                             \
        }                                                               \
      return count;                                                     \
    }                                                                   \
  while (0)

/* Compute and return the number of 1-bits set in the least
   significant 32 bits of x. */
inline int _smcore_popcount_32__ (unsigned int);

#if STM_GNUC_VERSION_AT_LEAST (3, 4)

/* Use the popcount built-in functions. */

inline int
_smcore_popcount_32__ (unsigned int x)
{
  return __builtin_popcount (x);
}

inline int
smcore_popcount (unsigned int x)
{
  return __builtin_popcount (x);
}

inline int
smcore_popcountl (unsigned long int x)
{
  return __builtin_popcountl (x);
}

inline int
smcore_popcountll (unsigned long long int x)
{
  return __builtin_popcountll (x);
}

inline int
smcore_popcount32 (uint32_t x)
{
  return __builtin_popcount (x);
}

inline int
smcore_popcount64 (uint64_t x)
{
#if 18446744073709551615ULL <= UINT_MAX
  return __builtin_popcount (x);
#elif 18446744073709551615ULL <= ULONG_MAX
  return __builtin_popcountl (x);
#else
  return __builtin_popcountll (x);
#endif
}

inline int
smcore_popcountz (size_t x)
{
#if SIZE_MAX <= UINT_MAX
  return __builtin_popcount (x);
#elif SIZE_MAX <= ULONG_MAX
  return __builtin_popcountl (x);
#elif SIZE_MAX <= ULLONG_MAX
  return __builtin_popcountll (x);
#else
  SMCORE_POPCOUNT_GENERIC__ (size_t);
#endif
}

inline int
smcore_popcountp (uintptr_t x)
{
#if UINTPTR_MAX <= UINT_MAX
  return __builtin_popcount (x);
#elif UINTPTR_MAX <= ULONG_MAX
  return __builtin_popcountl (x);
#elif UINTPTR_MAX <= ULLONG_MAX
  return __builtin_popcountll (x);
#else
  SMCORE_POPCOUNT_GENERIC__ (size_t);
#endif
}

#else /* !STM_GNUC_VERSION_AT_LEAST (3, 4) */

inline int
_smcore_popcount_32__ (unsigned int x)
{
  x = ((x & 0xaaaaaaaaU) >> 1) + (x & 0x55555555U);
  x = ((x & 0xccccccccU) >> 2) + (x & 0x33333333U);
  x = (x >> 16) + (x & 0xffff);
  x = ((x & 0xf0f0) >> 4) + (x & 0x0f0f);
  return (x >> 8) + (x & 0x00ff);
}

inline int
smcore_popcount (unsigned int x)
{
  SMCORE_POPCOUNT_GENERIC__ (unsigned int);
}

inline int
smcore_popcountl (unsigned long int x)
{
  SMCORE_POPCOUNT_GENERIC__ (unsigned long int);
}

inline int
smcore_popcountll (unsigned long long int x)
{
  SMCORE_POPCOUNT_GENERIC__ (unsigned long long int);
}

inline int
smcore_popcount32 (uint32_t x)
{
  SMCORE_POPCOUNT_GENERIC__ (uint32_t);
}

inline int
smcore_popcount64 (uint64_t x)
{
  SMCORE_POPCOUNT_GENERIC__ (uint64_t);
}

inline int
smcore_popcountz (size_t x)
{
  SMCORE_POPCOUNT_GENERIC__ (size_t);
}

inline int
smcore_popcountp (uintptr_t x)
{
  SMCORE_POPCOUNT_GENERIC__ (uintptr_t);
}

#endif /* !STM_GNUC_VERSION_AT_LEAST (3, 4) */

#endif /* SORTSMILL_CORE_POPCOUNT_H_ */
