/*
 * Copyright (C) 2012 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_XDIE_ON_NULL_H_
#define SORTSMILL_CORE_XDIE_ON_NULL_H_

#include <assert.h>
#include <stddef.h>
#include <errno.h>
#include <sortsmill/core/attributes.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

STM_ATTRIBUTE_PURE inline void *xdie_on_null (void *p);
STM_ATTRIBUTE_PURE inline void *xdie_on_enomem (void *p);
void _sortsmill_xalloc_die (void);

inline void *
xdie_on_null (void *p)
{
  if (p == NULL)
    _sortsmill_xalloc_die ();
  return p;
}

inline void *
xdie_on_enomem (void *p)
{
  if (p == NULL && errno == ENOMEM)
    _sortsmill_xalloc_die ();
  assert (p != NULL);           /* May fail if strings have not been validated. */
  return p;
}

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

/* The macro XDIE_ON_NULL tries to avoid implicit type-casting between
   the type of p and (void *). This works with gcc, in particular. */
#define XDIE_ON_NULL(p) (STM_CAST_TYPEOF (p) xdie_on_null ((void *) (p)))

/* The macro XDIE_ON_ENOMEM tries to avoid implicit type-casting
   between the type of p and (void *). This works with gcc, in
   particular. */
#define XDIE_ON_ENOMEM(p) (STM_CAST_TYPEOF (p) xdie_on_enomem ((void *) (p)))

#endif /* SORTSMILL_CORE_XDIE_ON_NULL_H_ */
