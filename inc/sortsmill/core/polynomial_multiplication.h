/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_MULTIPLICATION_H_
#define SORTSMILL_CORE_POLYNOMIAL_MULTIPLICATION_H_

#include <stdlib.h>
#include <gmp.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

void mpq_poly_scalarmul (size_t degree, ssize_t stride, mpq_t *poly,
                         const mpq_t r, ssize_t result_stride, mpq_t *result);
void dpoly_scalarmul (size_t degree, ssize_t stride, const double *poly,
                      double r, ssize_t result_stride, double *result);

void mpq_poly_mul_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                        size_t degree2, ssize_t stride2, mpq_t *poly2,
                        ssize_t result_stride, mpq_t *result);
void dpoly_mul_mono (size_t degree1, ssize_t stride1, const double *poly1,
                     size_t degree2, ssize_t stride2, const double *poly2,
                     ssize_t result_stride, double *result);
void dpoly_mul_bern (size_t degree1, ssize_t stride1, const double *poly1,
                     size_t degree2, ssize_t stride2, const double *poly2,
                     ssize_t result_stride, double *result);
void dpoly_mul_sbern (size_t degree1, ssize_t stride1, const double *poly1,
                      size_t degree2, ssize_t stride2, const double *poly2,
                      ssize_t result_stride, double *result);
void dpoly_mul_spower (size_t degree1, ssize_t stride1, const double *poly1,
                       size_t degree2, ssize_t stride2, const double *poly2,
                       ssize_t result_stride, double *result);

bool mpq_poly_equalsone_mono (size_t degree1, ssize_t stride1, mpq_t *poly1);
bool dpoly_equalsone_mono (size_t degree1, ssize_t stride1,
                           const double *poly1);
bool dpoly_equalsone_bern (size_t degree1, ssize_t stride1,
                           const double *poly1);
bool dpoly_equalsone_sbern (size_t degree1, ssize_t stride1,
                            const double *poly1);
bool dpoly_equalsone_spower (size_t degree1, ssize_t stride1,
                             const double *poly1);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_MULTIPLICATION_H_ */
