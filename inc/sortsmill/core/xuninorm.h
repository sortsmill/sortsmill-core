/*
 * Copyright (C) 2012 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_XUNINORM_H_
#define SORTSMILL_CORE_XUNINORM_H_

#include <uninorm.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

int u8_compare (const uint8_t *s1, const uint8_t *s2);
int u16_compare (const uint16_t *s1, const uint16_t *s2);
int u32_compare (const uint32_t *s1, const uint32_t *s2);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_XUNINORM_H_ */
