/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_DIRECTIONS_H_
#define SORTSMILL_CORE_POLYNOMIAL_DIRECTIONS_H_

#include <stdlib.h>
#include <complex.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Return the instantaneous direction of a polynomial spline, as a
 * complex number.
 *
 * One or the other component of the complex number _may_ be NaN, if
 * there is no unique direction at the parameter value. (Do not rely
 * on this behavior.)
 */

double complex dpoly_direction_bernsv (size_t degree_a, ssize_t stride_ax,
                                       const double *ax, ssize_t stride_ay,
                                       const double *ay, double t);
double complex dpoly_direction_berndc (size_t degree_a, ssize_t stride_ax,
                                       const double *ax, ssize_t stride_ay,
                                       const double *ay, double t);
double complex dpoly_direction_sbernsv (size_t degree_a, ssize_t stride_ax,
                                        const double *ax, ssize_t stride_ay,
                                        const double *ay, double t);
double complex dpoly_direction_sberndc (size_t degree_a, ssize_t stride_ax,
                                        const double *ax, ssize_t stride_ay,
                                        const double *ay, double t);
double complex dpoly_direction_spower (size_t degree_a, ssize_t stride_ax,
                                       const double *ax, ssize_t stride_ay,
                                       const double *ay, double t);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_DIRECTIONS_H_ */
