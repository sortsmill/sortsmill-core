/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_POLYNOMIAL_INFLECTIONS_H_
#define SORTSMILL_CORE_POLYNOMIAL_INFLECTIONS_H_

#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*
 * Find parameters in [0,1] for the inflections and cusps of a
 * parametric polynomial spline in the plane.
 */

void dpoly_inflections_mono (size_t degree_a,
                             ssize_t stride_ax, const double *ax,
                             ssize_t stride_ay, const double *ay,
                             size_t *num_inflections,
                             double inflections
                             [(degree_a <= 2) ? 0 : 2 * degree_a - 4]);
void dpoly_inflections_bern (size_t degree_a,
                             ssize_t stride_ax, const double *ax,
                             ssize_t stride_ay, const double *ay,
                             size_t *num_inflections,
                             double inflections
                             [(degree_a <= 2) ? 0 : 2 * degree_a - 4]);
void dpoly_inflections_sbern (size_t degree_a,
                              ssize_t stride_ax, const double *ax,
                              ssize_t stride_ay, const double *ay,
                              size_t *num_inflections,
                              double inflections
                              [(degree_a <= 2) ? 0 : 2 * degree_a - 4]);
void dpoly_inflections_spower (size_t degree_a,
                               ssize_t stride_ax, const double *ax,
                               ssize_t stride_ay, const double *ay,
                               size_t *num_inflections,
                               double inflections
                               [(degree_a <= 2) ? 0 : 2 * degree_a - 4]);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_POLYNOMIAL_INFLECTIONS_H_ */
