/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 * This file is part of Sorts Mill Tools.
 * 
 * Sorts Mill Tools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_HASH_MAPS_H_
#define SORTSMILL_CORE_HASH_MAPS_H_

/*
 * Immutable hash maps.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <sortsmill/core/attributes.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*---------------------------------------------------------------------*/

/* The hashf and equalf fields return void pointers to work around
   limitations in C syntax, and on the presumption that they will not
   be used routinely. */
#define SMCORE_HASH_MAP_BASE_MODULE_T(KEY, HASH, VALUE, \
                                      MAP, CURSOR)      \
  struct                                                \
  {                                                     \
    void *(*hashf) (MAP);                               \
    void *(*equalf) (MAP);                              \
    MAP (*make) (HASH (*) (KEY), int (*) (KEY, KEY));   \
    int (*is_nil) (MAP);                                \
    size_t (*size) (MAP);                               \
    int (*has_key) (MAP, KEY);                          \
    VALUE (*get) (MAP, KEY);                            \
    int (*get_eff) (MAP, KEY, VALUE *);                 \
    MAP (*set) (MAP, KEY, VALUE);                       \
    MAP (*remove) (MAP, KEY);                           \
    CURSOR (*first) (MAP);                              \
    CURSOR (*next) (CURSOR);                            \
    KEY (*key) (CURSOR);                                \
    VALUE (*value) (CURSOR);                            \
    void (*pair_eff) (CURSOR, KEY *, VALUE *);          \
  }

/*---------------------------------------------------------------------*/

/*   *INDENT-OFF*    */

/*
 * the_uintptr_to_uintptr_hash_map_base_module():
 *
 * A module used to make other modules out of keys and values that are
 * no bigger than a uintptr_t.
 */
typedef struct _uintptr_to_uintptr_hash_map__ *uintptr_to_uintptr_hash_map_t;
typedef struct _uintptr_to_uintptr_hash_map_cursor__ *uintptr_to_uintptr_hash_map_cursor_t;
#define UINTPTR_TO_UINTPTR_HASH_MAP_BASE_MODULE_T                       \
  SMCORE_HASH_MAP_BASE_MODULE_T (uintptr_t, uint32_t, uintptr_t,        \
                                 uintptr_to_uintptr_hash_map_t,         \
                                 uintptr_to_uintptr_hash_map_cursor_t)
typedef UINTPTR_TO_UINTPTR_HASH_MAP_BASE_MODULE_T uintptr_to_uintptr_hash_map_base_module_t;
STM_ATTRIBUTE_PURE uintptr_to_uintptr_hash_map_base_module_t *the_uintptr_to_uintptr_hash_map_base_module (void);

/*   *INDENT-ON*    */

/*---------------------------------------------------------------------*/

/*
 * Hash map modules for keys and values that are no bigger than a
 * uintptr_t.
 */

/*
 * Example declarations:
 *
 *   // Abstract types for the maps and cursors.
 *   typedef struct widget_dict *widget_dict_t;
 *   typedef struct widget_cursor *widget_cursor_t;
 *
 *   // A macro defined mainly so the following typedef
 *   // will not confuse tools such as Emacs.
 *   #define WIDGET_DICT_MODULE_T                               \
 *     HASH_MAP_MODULE_T (const char *, uint32_t, widget_t *,   \
 *                        widget_dict_t, widget_cursor_t)
 *
 *   // The type of a widget dictionary module.
 *   typedef WIDGET_DICT_MODULE_T widget_dict_module_t;
 *
 */
#define HASH_MAP_MODULE_T(KEY, HASH, VALUE, MAP, CURSOR)        \
  struct                                                        \
  {                                                             \
    HASH (*hashf) (KEY);                                        \
    bool (*equalf) (KEY, KEY);                                  \
    MAP (*make) (void);                                         \
    bool (*is_nil) (MAP);                                       \
    size_t (*size) (MAP);                                       \
    bool (*has_key) (MAP, KEY);                                 \
    VALUE (*get) (MAP, KEY);                                    \
    void (*get_eff) (MAP, KEY, bool *, VALUE *);                \
    MAP (*set) (MAP, KEY, VALUE);                               \
    MAP (*remove) (MAP, KEY);                                   \
    CURSOR (*first) (MAP);                                      \
    CURSOR (*next) (CURSOR);                                    \
    KEY (*key) (CURSOR);                                        \
    VALUE (*value) (CURSOR);                                    \
    void (*pair_eff) (CURSOR, KEY *, VALUE *);                  \
  }

/*
 * The FROM_KEY, TO_KEY, FROM_VALUE, and TO_VALUE parameters may be
 * simple casts, functions, etc. Type mappings are as follows:
 *
 *       FROM_KEY : KEY -> uintptr_t
 *         TO_KEY : uintptr_t -> KEY
 *     FROM_VALUE : VALUE -> uintptr_t
 *       TO_VALUE : uintptr_t -> VALUE
 *
 *
 * Either of the COPY_KEY and COPY_VALUE parameters can be left empty,
 * if there is no need to make a copy of the key or value,
 * respectively, when setting an entry.
 */
#define DEFINE_HASH_MAP_MODULE_T_FUNCTIONS__(STATIC, KEY, HASH, VALUE,  \
                                             MAP, CURSOR,               \
                                             HASHF, EQUALF,             \
                                             FROM_KEY, TO_KEY,          \
                                             COPY_KEY,                  \
                                             FROM_VALUE, TO_VALUE,      \
                                             COPY_VALUE)                \
                                                                        \
  STATIC HASH                                                           \
  x_##MAP##_internal_hashf (uintptr_t key)                              \
  {                                                                     \
    return HASHF (TO_KEY (key));                                        \
  }                                                                     \
                                                                        \
  STATIC int                                                            \
  x_##MAP##_internal_equalf (uintptr_t key, uintptr_t stored_key)       \
  {                                                                     \
    return (int) EQUALF (TO_KEY (key), TO_KEY (stored_key));            \
  }                                                                     \
                                                                        \
  STATIC MAP                                                            \
  x_##MAP##_make (void)                                                 \
  {                                                                     \
    return (MAP) (void *)                                               \
      the_uintptr_to_uintptr_hash_map_base_module ()->                  \
      make (x_##MAP##_internal_hashf, x_##MAP##_internal_equalf);       \
  }                                                                     \
                                                                        \
  STATIC bool                                                           \
  x_##MAP##_is_nil (MAP map)                                            \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    return (bool)                                                       \
      the_uintptr_to_uintptr_hash_map_base_module ()->is_nil (m);       \
  }                                                                     \
                                                                        \
  STATIC size_t                                                         \
  x_##MAP##_size (MAP map)                                              \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    return the_uintptr_to_uintptr_hash_map_base_module ()->size (m);    \
  }                                                                     \
                                                                        \
  STATIC bool                                                           \
  x_##MAP##_has_key (MAP map, KEY key)                                  \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    return (bool)                                                       \
      the_uintptr_to_uintptr_hash_map_base_module ()->                  \
      has_key (m, FROM_KEY (key));                                      \
  }                                                                     \
                                                                        \
  STATIC VALUE                                                          \
  x_##MAP##_get (MAP map, KEY key)                                      \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    return                                                              \
      TO_VALUE (the_uintptr_to_uintptr_hash_map_base_module ()->        \
                get (m, FROM_KEY (key)));                               \
  }                                                                     \
                                                                        \
  STATIC void                                                           \
  x_##MAP##_get_eff (MAP map, KEY key, bool *found, VALUE *value)       \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    uintptr_t v;                                                        \
    *found = (bool)                                                     \
      the_uintptr_to_uintptr_hash_map_base_module ()->                  \
      get_eff (m, FROM_KEY (key), &v);                                  \
    *value = TO_VALUE (v);                                              \
  }                                                                     \
                                                                        \
  STATIC MAP                                                            \
  x_##MAP##_set (MAP map, KEY key, VALUE value)                         \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    return (MAP) (void *)                                               \
      the_uintptr_to_uintptr_hash_map_base_module ()->                  \
      set (m, FROM_KEY (COPY_KEY (key)),                                \
           FROM_VALUE (COPY_VALUE (value)));                            \
  }                                                                     \
                                                                        \
  STATIC MAP                                                            \
  x_##MAP##_remove (MAP map, KEY key)                                   \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    return (MAP) (void *)                                               \
      the_uintptr_to_uintptr_hash_map_base_module ()->                  \
      remove (m, FROM_KEY (key));                                       \
  }                                                                     \
                                                                        \
  STATIC CURSOR                                                         \
  x_##MAP##_first (MAP map)                                             \
  {                                                                     \
    uintptr_to_uintptr_hash_map_t m =                                   \
      (uintptr_to_uintptr_hash_map_t) (void *) map;                     \
    return (CURSOR) (void *)                                            \
      the_uintptr_to_uintptr_hash_map_base_module ()->first (m);        \
  }                                                                     \
                                                                        \
  STATIC CURSOR                                                         \
  x_##MAP##_next (CURSOR cursor)                                        \
  {                                                                     \
    uintptr_to_uintptr_hash_map_cursor_t c =                            \
      (uintptr_to_uintptr_hash_map_cursor_t) (void *) cursor;           \
    return (CURSOR) (void *)                                            \
      the_uintptr_to_uintptr_hash_map_base_module ()->next (c);         \
  }                                                                     \
                                                                        \
  STATIC KEY                                                            \
  x_##MAP##_key (CURSOR cursor)                                         \
  {                                                                     \
    uintptr_to_uintptr_hash_map_cursor_t c =                            \
      (uintptr_to_uintptr_hash_map_cursor_t) (void *) cursor;           \
    return                                                              \
      TO_KEY (the_uintptr_to_uintptr_hash_map_base_module ()->key (c)); \
  }                                                                     \
                                                                        \
  STATIC VALUE                                                          \
  x_##MAP##_value (CURSOR cursor)                                       \
  {                                                                     \
    uintptr_to_uintptr_hash_map_cursor_t c =                            \
      (uintptr_to_uintptr_hash_map_cursor_t) (void *) cursor;           \
    return                                                              \
      TO_VALUE (the_uintptr_to_uintptr_hash_map_base_module ()->        \
                value (c));                                             \
  }                                                                     \
                                                                        \
  STATIC void                                                           \
  x_##MAP##_pair_eff (CURSOR cursor, KEY *key, VALUE *value)            \
  {                                                                     \
    uintptr_to_uintptr_hash_map_cursor_t c =                            \
      (uintptr_to_uintptr_hash_map_cursor_t) (void *) cursor;           \
    uintptr_t k;                                                        \
    uintptr_t v;                                                        \
    the_uintptr_to_uintptr_hash_map_base_module ()->                    \
      pair_eff (c, &k, &v);                                             \
    *key = TO_KEY (k);                                                  \
    *value = TO_VALUE (v);                                              \
  }

#define DECLARE_HASH_MAP_MODULE_T_FUNCTIONS__(STATIC, KEY, HASH, VALUE, \
                                              MAP, CURSOR)              \
  STATIC HASH x_##MAP##_internal_hashf (uintptr_t key);                 \
  STATIC int x_##MAP##_internal_equalf (uintptr_t key,                  \
                                        uintptr_t stored_key);          \
  STATIC MAP x_##MAP##_make (void);                                     \
  STATIC bool x_##MAP##_is_nil (MAP map);                               \
  STATIC size_t x_##MAP##_size (MAP map);                               \
  STATIC bool x_##MAP##_has_key (MAP map, KEY key);                     \
  STATIC VALUE x_##MAP##_get (MAP map, KEY key);                        \
  STATIC void x_##MAP##_get_eff (MAP map, KEY key,                      \
                                   bool *found, VALUE *value);          \
  STATIC MAP x_##MAP##_set (MAP map, KEY key, VALUE value);             \
  STATIC MAP x_##MAP##_remove (MAP map, KEY key);                       \
  STATIC CURSOR x_##MAP##_first (MAP map);                              \
  STATIC CURSOR x_##MAP##_next (CURSOR cursor);                         \
  STATIC KEY x_##MAP##_key (CURSOR cursor);                             \
  STATIC VALUE x_##MAP##_value (CURSOR cursor);                         \
  STATIC void x_##MAP##_pair_eff (CURSOR cursor, KEY *key, VALUE *value);

#define HASH_MAP_MODULE_INITIALIZER__(KEY, HASH, VALUE, MAP, CURSOR,    \
                                      HASHF, EQUALF)                    \
  {   .hashf = HASHF,                                                   \
      .equalf = EQUALF,                                                 \
      .make = x_##MAP##_make,                                           \
      .is_nil = x_##MAP##_is_nil,                                       \
      .size = x_##MAP##_size,                                           \
      .has_key = x_##MAP##_has_key,                                     \
      .get = x_##MAP##_get,                                             \
      .get_eff = x_##MAP##_get_eff,                                     \
      .set = x_##MAP##_set,                                             \
      .remove = x_##MAP##_remove,                                       \
      .first = x_##MAP##_first,                                         \
      .next = x_##MAP##_next,                                           \
      .key = x_##MAP##_key,                                             \
      .value = x_##MAP##_value,                                         \
      .pair_eff = x_##MAP##_pair_eff   }

/*
 * Examples of the MODULE parameter for a module named
 * widget_map_module, of type widget_map_module_t:
 *
 *   widget_map_module_t widget_map_module
 *
 *   static const widget_map_module_t widget_map_module
 *
 *   __attribute__((__visibility__("default"))) const
 *       widget_map_module_t widget_map_module
 *
 *
 * The FROM_KEY, TO_KEY, FROM_VALUE, and TO_VALUE parameters may be
 * simple casts, functions, etc. Type mappings are as follows:
 *
 *       FROM_KEY : KEY -> uintptr_t
 *         TO_KEY : uintptr_t -> KEY
 *     FROM_VALUE : VALUE -> uintptr_t
 *       TO_VALUE : uintptr_t -> VALUE
 *
 *
 * Either of the COPY_KEY and COPY_VALUE parameters can be left empty,
 * if there is no need to make a copy of the key or value,
 * respectively, when setting an entry.
 */
#define DEFINE_HASH_MAP_MODULE(MODULE, KEY, HASH, VALUE, MAP, CURSOR,   \
                               HASHF, EQUALF,                           \
                               FROM_KEY, TO_KEY, COPY_KEY,              \
                               FROM_VALUE, TO_VALUE, COPY_VALUE)        \
  DECLARE_HASH_MAP_MODULE_T_FUNCTIONS__ (static, KEY, HASH, VALUE,      \
                                         MAP, CURSOR);                  \
  MODULE =                                                              \
    HASH_MAP_MODULE_INITIALIZER__ (KEY, HASH, VALUE, MAP, CURSOR,       \
                                   HASHF, EQUALF);                      \
  DEFINE_HASH_MAP_MODULE_T_FUNCTIONS__ (static, KEY, HASH, VALUE,       \
                                        MAP, CURSOR,                    \
                                        HASHF, EQUALF,                  \
                                        FROM_KEY, TO_KEY, COPY_KEY,     \
                                        FROM_VALUE, TO_VALUE, COPY_VALUE)

/*---------------------------------------------------------------------*/

/*
 * An example module that also may be useful: mapping strings to
 * strings. Both keys and values are copied when inserted, so that the
 * maps can be considered immutable.
 */

/* Abstract types for the maps and cursors. */
typedef struct _smcore_str_to_str_t__ *smcore_str_to_str_t;
typedef struct _smcore_str_to_str_cursor_t__ *smcore_str_to_str_cursor_t;

/* A macro defined mainly so the following typedef will not confuse
   tools such as Emacs. */
#define SMCORE_STR_TO_STR_MODULE_T                                      \
  HASH_MAP_MODULE_T (const char *, uint32_t, const char *,              \
                     smcore_str_to_str_t, smcore_str_to_str_cursor_t)

/* The type of the module. */
typedef SMCORE_STR_TO_STR_MODULE_T smcore_str_to_str_module_t;

/* Declaration of the module. It is defined in hash_maps.c. */
extern const smcore_str_to_str_module_t smcore_str_to_str;

/*---------------------------------------------------------------------*/

/*
 * Another potentially useful example module: mapping strings to void
 * pointers. The keys (but not the values) are copied when inserted.
 */

/* Abstract types for the maps and cursors. */
typedef struct _smcore_str_to_voidp_t__ *smcore_str_to_voidp_t;
typedef struct _smcore_str_to_voidp_cursor_t__ *smcore_str_to_voidp_cursor_t;

/* A macro defined mainly so the following typedef will not confuse
   tools such as Emacs. */
#define SMCORE_STR_TO_VOIDP_MODULE_T                                    \
  HASH_MAP_MODULE_T (const char *, uint32_t, void *,                    \
                     smcore_str_to_voidp_t, smcore_str_to_voidp_cursor_t)

/* The type of the module. */
typedef SMCORE_STR_TO_VOIDP_MODULE_T smcore_str_to_voidp_module_t;

/* Declaration of the module. It is defined in hash_maps.c. */
extern const smcore_str_to_voidp_module_t smcore_str_to_voidp;

/*---------------------------------------------------------------------*/

/*
 * A family of examples: maps from strings to some types of integer
 * that can fit into uintptr_t.
 */

#define SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__(S, T)                \
  typedef struct _smcore_str_to_##S##_t__ *smcore_str_to_##S##_t;       \
  typedef struct _smcore_str_to_##S##_cursor_t__ *smcore_str_to_##S##_cursor_t; \
  typedef HASH_MAP_MODULE_T (const char *, uint32_t, T,                 \
                             smcore_str_to_##S##_t,                     \
                             smcore_str_to_##S##_cursor_t)              \
       smcore_str_to_##S##_module_t;                                    \
  extern const smcore_str_to_##S##_module_t smcore_str_to_##S;

SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (intptr, intptr_t);
SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (uintptr, uintptr_t);
SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (ssize_t, ssize_t);
SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (size_t, size_t);
SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (int32, int32_t);
SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (uint32, uint32_t);
SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (int, int);
SMCORE_DECLARE_SMCORE_STR_TO_INTEGER_MAP__ (uint, unsigned int);

/*---------------------------------------------------------------------*/

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_HASH_MAPS_H_ */
