/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_CORE_BRENTROOT_H_
#define SORTSMILL_CORE_BRENTROOT_H_

/*
 * Brent’s root-finding algorithm.
 */

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

typedef float sbrentroot_func_t (float, void *);
typedef double dbrentroot_func_t (double, void *);
typedef long double lbrentroot_func_t (long double, void *);

void sbrentroot (float t0, float t1, sbrentroot_func_t *f,
                 void *data, float tolerance, float epsilon,
                 float *root, int *info);
void dbrentroot (double t0, double t1, dbrentroot_func_t *f,
                 void *data, double tolerance, double epsilon,
                 double *root, int *info);
void lbrentroot (long double t0, long double t1, lbrentroot_func_t *f,
                 void *data, long double tolerance, long double epsilon,
                 long double *root, int *info);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_CORE_BRENTROOT_H_ */
