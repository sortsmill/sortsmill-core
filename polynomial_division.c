#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

static inline size_t
szmax (size_t a, size_t b)
{
  return (a < b) ? b : a;
}

static inline ssize_t
sszmax (ssize_t a, ssize_t b)
{
  return (a < b) ? b : a;
}

static void
mpq_poly_zerofill (size_t degree, mpq_t *poly)
{
  for (size_t i = 0; i <= degree; i++)
    mpq_set_ui (poly[i], 0, 1);
}

//----------------------------------------------------------------------
//
// Return the actual (minimum) degree of a polynomial in monomial
// form, or -1 if the polynomial equals zero identically.

static ssize_t
_mpq_poly_mindegree2_mono (size_t degree, mpq_t *poly)
{
  ssize_t d = degree;
  while (0 <= d && mpq_sgn (poly[d]) == 0)
    d--;
  return d;
}

//----------------------------------------------------------------------

VISIBLE void
mpq_poly_div_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                   size_t degree2, ssize_t stride2, mpq_t *poly2,
                   size_t *degree_q, ssize_t stride_q, mpq_t *quotient,
                   size_t *degree_r, ssize_t stride_r, mpq_t *remainder,
                   bool *division_by_zero)
{
  // See
  // http://en.wikipedia.org/w/index.php?title=Polynomial_long_division&oldid=636315864
  // and
  // http://en.wikipedia.org/w/index.php?title=Polynomial_greatest_common_divisor&oldid=649534630#Euclidean_division

  mpq_t *denom = xmalloc ((degree2 + 1) * sizeof (mpq_t));
  mpq_init_poly (degree2, denom);

  copy_mpq_with_strides (1, denom, stride2, poly2, degree2 + 1);
  const ssize_t deg_denom = _mpq_poly_mindegree2_mono (degree2, denom);

  *division_by_zero = (deg_denom < 0);

  if (!*division_by_zero)
    {
      mpq_t tmp;
      mpq_init (tmp);

      mpq_t *q = xmalloc ((degree1 + 1) * sizeof (mpq_t));
      mpq_init_poly (degree1, q);

      mpq_t *r = xmalloc ((degree1 + 1) * sizeof (mpq_t));
      mpq_init_poly (degree1, r);

      mpq_poly_zerofill (degree1, q);
      copy_mpq_with_strides (1, r, stride1, poly1, degree1 + 1);

      ssize_t deg_r = _mpq_poly_mindegree2_mono (degree1, r);
      ssize_t deg_q = sszmax (0, deg_r - deg_denom);

      while (deg_denom <= deg_r)
        {
          mpq_div (q[deg_r - deg_denom], r[deg_r], denom[deg_denom]);

          mpq_set_ui (r[deg_r], 0, 1);
          for (size_t i = 1; i <= deg_denom; i++)
            {
              mpq_mul (tmp, q[deg_r - deg_denom], denom[deg_denom - i]);
              mpq_sub (r[deg_r - i], r[deg_r - i], tmp);
            }

          deg_r = _mpq_poly_mindegree2_mono (degree1, r);
        }
      deg_r = sszmax (0, deg_r);


      // Both the quotient and remainder can be left out of the
      // result.
      if (degree_q != NULL)
        *degree_q = deg_q;
      if (quotient != NULL)
        copy_mpq_with_strides (stride_q, quotient, 1, q, degree1 + 1);
      if (degree_r != NULL)
        *degree_r = deg_r;
      if (remainder != NULL)
        copy_mpq_with_strides (stride_r, remainder, 1, r, degree1 + 1);

      mpq_clear_poly (degree1, r);
      free (r);
      mpq_clear_poly (degree1, q);
      free (q);
      mpq_clear (tmp);
    }

  mpq_clear_poly (degree2, denom);
  free (denom);
}

//----------------------------------------------------------------------

static void
mpq_gcd_iteration (size_t degree_a, mpq_t *a, size_t degree_b, mpq_t *b,
                   size_t *result_degree, mpq_t **result)
{
  degree_a = sszmax (0, _mpq_poly_mindegree2_mono (degree_a, a));
  degree_b = sszmax (0, _mpq_poly_mindegree2_mono (degree_b, b));

  if (degree_a < degree_b)
    {
      // Swap a and b.
      {
        mpq_t *tmp = a;
        a = b;
        b = tmp;
      }
      {
        const size_t tmp = degree_a;
        degree_a = degree_b;
        degree_b = tmp;
      }
    }

  while (0 < degree_b || mpq_sgn (b[0]) != 0)
    {
      size_t degree_r;
      bool division_by_zero;
      mpq_poly_div_mono (sszmax (0, degree_a), 1, a, degree_b, 1, b,
                         NULL, 1, NULL, &degree_r, 1, a, &division_by_zero);
      assert (!division_by_zero);

      // Swap a and b.
      mpq_t *tmp = a;
      a = b;
      b = tmp;
      degree_a = degree_b;
      degree_b = degree_r;
    }

  if (0 < degree_a || mpq_sgn (a[0]) != 0)
    {
      // Make the result monic.
      for (size_t i = 0; i < degree_a; i++)
        mpq_div (a[i], a[i], a[degree_a]);
      mpq_set_ui (a[degree_a], 1, 1);
    }

  *result_degree = degree_a;
  *result = a;
}

VISIBLE void
mpq_poly_gcd_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                   size_t degree2, ssize_t stride2, mpq_t *poly2,
                   size_t *degree, ssize_t stride, mpq_t *gcd)
{
  // See
  // http://en.wikipedia.org/w/index.php?title=Polynomial_greatest_common_divisor&oldid=649534630

  size_t degree_max = szmax (degree1, degree2);

  mpq_t *a = xmalloc ((degree_max + 1) * sizeof (mpq_t));
  mpq_init_poly (degree_max, a);
  copy_mpq_with_strides (1, a, stride1, poly1, degree1 + 1);

  mpq_t *b = xmalloc ((degree_max + 1) * sizeof (mpq_t));
  mpq_init_poly (degree_max, b);
  copy_mpq_with_strides (1, b, stride2, poly2, degree2 + 1);

  size_t result_degree;
  mpq_t *result;
  mpq_gcd_iteration (degree1, a, degree2, b, &result_degree, &result);
  if (result_degree < 0)
    {
      mpq_set_ui (gcd[0], 0, 1);
      *degree = 0;
    }
  else
    {
      copy_mpq_with_strides (stride, gcd, 1, result, result_degree + 1);
      *degree = result_degree;
    }

  mpq_clear_poly (degree_max, b);
  free (b);
  mpq_clear_poly (degree_max, a);
  free (a);
}

//----------------------------------------------------------------------
//
// Yun’s algorithm for the square-free decomposition of a univariate
// polynomial. See
// http://en.wikipedia.org/w/index.php?title=Square-free_polynomial&oldid=647699379
//
// This version first reduces the polynomial to minimum degree (that
// is, to the ‘actual’ degree), then normalizes the polynomial to
// monic form. There are two results: the square-free decomposition of
// the monic polynomial, and the original lead coefficient of the
// minimum degree polynomial. If the input polynomial is equal to
// zero, then the results are a zero polynomial and the number zero,
// respectively.

static polynomial_list_entry_t *
mpq_yun_iteration (size_t b_degree, mpq_t *b, size_t c_degree, mpq_t *c)
{
  assert (c_degree + 1 == b_degree);

  bool division_by_zero;

  const size_t b_degree0 = b_degree;
  const size_t c_degree0 = c_degree;

  mpq_t *bprime = xmalloc ((c_degree0 + 1) * sizeof (mpq_t));
  mpq_init_poly (c_degree0, bprime);

  mpq_t *c_minus_bprime = xmalloc ((c_degree0 + 1) * sizeof (mpq_t));
  mpq_init_poly (c_degree0, c_minus_bprime);

  mpq_t *a = xmalloc ((b_degree0 + 1) * sizeof (mpq_t));
  mpq_init_poly (b_degree0, a);

  bool done = false;
  polynomial_list_entry_t *lst = NULL;
  do
    {
      mpq_poly_deriv_mono (b_degree, 1, b, 1, bprime);
      mpq_poly_sub_mono (c_degree, 1, c, c_degree, 1, bprime, 1,
                         c_minus_bprime);

      size_t a_degree;
      mpq_poly_gcd_mono (b_degree, 1, b, c_degree, 1, c_minus_bprime, &a_degree,
                         1, a);

      // Cons a to the list.
      polynomial_list_entry_t *p = xmalloc (sizeof (polynomial_list_entry_t));
      p->next = lst;
      p->degree = a_degree;
      p->poly = xmalloc ((a_degree + 1) * sizeof (mpq_t));
      mpq_init_poly (a_degree, p->poly);
      copy_mpq_with_strides (1, p->poly, 1, a, a_degree + 1);
      lst = p;

      // Calculate the next value of b.
      mpq_poly_div_mono (b_degree, 1, b, a_degree, 1, a,
                         &b_degree, 1, b, NULL, 1, NULL, &division_by_zero);
      assert (!division_by_zero);

      done = mpq_poly_equalsone_mono (b_degree, 1, b);
      if (!done)
        {
          // Calculate the next value of c.
          mpq_poly_div_mono (c_degree, 1, c_minus_bprime, a_degree, 1, a,
                             &c_degree, 1, c, NULL, 1, NULL, &division_by_zero);
          assert (!division_by_zero);
        }
    }
  while (!done);

  mpq_clear_poly (b_degree0, a);
  free (a);
  mpq_clear_poly (c_degree0, c_minus_bprime);
  free (c_minus_bprime);
  mpq_clear_poly (c_degree0, bprime);
  free (bprime);

  return lst;
}

VISIBLE void
mpq_poly_squarefree_mono (size_t degree, ssize_t stride, mpq_t *poly,
                          polynomial_list_entry_t **result, mpq_t lead_coef)
{
  const ssize_t min_degree = _mpq_poly_mindegree2_mono (degree, poly);
  if (min_degree < 0)
    {
      // The lead coefficient is zero.
      mpq_set_ui (lead_coef, 0, 1);

      // The result is the constant polynomial equal to zero.
      polynomial_list_entry_t *p = xmalloc (sizeof (polynomial_list_entry_t));
      p->next = NULL;
      p->degree = 0;
      p->poly = xmalloc (sizeof (mpq_t));
      mpq_init (p->poly[0]);
      mpq_set_ui (p->poly[0], 0, 1);
      *result = p;
    }
  else
    {
      bool division_by_zero;

      mpq_set (lead_coef, poly[min_degree]);

      const size_t f_degree = min_degree;
      mpq_t *f = xmalloc ((f_degree + 1) * sizeof (mpq_t));
      mpq_init_poly (f_degree, f);
      for (size_t i = 0; i <= f_degree; i++)
        {
          mpq_div (f[i], *poly, lead_coef);
          poly += stride;
        }

      const size_t fprime_degree = (f_degree == 0) ? 0 : (f_degree - 1);
      mpq_t *fprime = xmalloc ((fprime_degree + 1) * sizeof (mpq_t));
      mpq_init_poly (fprime_degree, fprime);
      mpq_poly_deriv_mono (f_degree, 1, f, 1, fprime);

      mpq_t *a0 = xmalloc ((f_degree + 1) * sizeof (mpq_t));
      mpq_init_poly (f_degree, a0);
      size_t a0_degree;
      mpq_poly_gcd_mono (f_degree, 1, f, fprime_degree, 1, fprime,
                         &a0_degree, 1, a0);

      mpq_t *b1 = xmalloc ((f_degree + 1) * sizeof (mpq_t));
      mpq_init_poly (f_degree, b1);
      size_t b1_degree;
      mpq_poly_div_mono (f_degree, 1, f, a0_degree, 1, a0,
                         &b1_degree, 1, b1, NULL, 1, NULL, &division_by_zero);
      assert (!division_by_zero);

      mpq_t *c1 = xmalloc ((fprime_degree + 1) * sizeof (mpq_t));
      mpq_init_poly (fprime_degree, c1);
      size_t c1_degree;
      mpq_poly_div_mono (fprime_degree, 1, fprime, a0_degree, 1, a0,
                         &c1_degree, 1, c1, NULL, 1, NULL, &division_by_zero);
      assert (!division_by_zero);

      if (mpq_poly_equalsone_mono (b1_degree, 1, b1))
        {
          // The result is the constant polynomial equal to 1.
          polynomial_list_entry_t *p =
            xmalloc (sizeof (polynomial_list_entry_t));
          p->next = NULL;
          p->degree = 0;
          p->poly = xmalloc (sizeof (mpq_t));
          mpq_init (p->poly[0]);
          mpq_set_ui (p->poly[0], 1, 1);
          *result = p;
        }
      else
        {
          polynomial_list_entry_t *lst =
            mpq_yun_iteration (b1_degree, b1, c1_degree, c1);
          *result = reverse_polynomial_list_in_place (lst);
        }

      mpq_clear_poly (fprime_degree, c1);
      free (c1);
      mpq_clear_poly (f_degree, b1);
      free (b1);
      mpq_clear_poly (f_degree, a0);
      free (a0);
      mpq_clear_poly (fprime_degree, fprime);
      free (fprime);
      mpq_clear_poly (f_degree, f);
      free (f);
    }
}

//----------------------------------------------------------------------
