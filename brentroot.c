#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//
// An implementation of Brent’s root-finding method, coded in the
// functional style of, for instance, the Scheme language. Tail
// recursions were eliminated in a way that tries to retain the
// readability of tail calls.
//

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <float.h>
#include <sortsmill/core/brentroot.h>

#ifndef BRENTROOT_REAL
#define BRENTROOT_REAL double
#endif

#ifndef BRENTROOT_EPSILON
#define BRENTROOT_EPSILON (DBL_EPSILON)
#endif

#ifndef BRENTROOT_FUNC
#define BRENTROOT_FUNC dbrentroot
#endif

#ifdef BRENTROOT_DEBUG
#define _BRENTROOT_DEBUGGING_CODE(what) do {what;} while (false)
#else
#define _BRENTROOT_DEBUGGING_CODE(what) do {} while (false)
#endif

#define _HALF(x) ((x) / 2)
#define _ABS(x) (((x) < 0) ? -(x) : (x))

#define _IF_NEGATIVE_THEN_DEFAULT(x, dflt) (((x) < 0) ? (dflt) : (x))

// Apply the sign of x to y.  We do not need to deal with x=0.
#define _APPLY_SIGN(x, y) (((x) < 0) ? -(y) : (y))

// If fx=f(x) and fy=f(y), then do x and y bracket the root?
#define _BRACKETED(fx, fy) (((fx) < 0 && 0 < (fy)) || ((fy) < 0 && 0 < (fx)))

#define _DECIDE_STEP(p, q, half_interval, tol1,                         \
                     step0, step1, step0_, step1_)                      \
  do                                                                    \
    {                                                                   \
      const BRENTROOT_REAL two_p = p + p;                               \
      const BRENTROOT_REAL tol1_times_q = tol1 * q;                     \
      if (two_p < (3 * half_interval * q) - _ABS (tol1_times_q))        \
        {                                                               \
          const BRENTROOT_REAL step1_times_q = step1 * q;               \
          if (two_p < _ABS (step1_times_q))                             \
            {                                                           \
              /* Proceed with interpolation. */                         \
              step0_ = p / q;                                           \
              step1_ = step0;                                           \
            }                                                           \
          else                                                          \
            {                                                           \
              /* Revert to bisection. */                                \
              _BRENTROOT_DEBUGGING_CODE                                 \
                (fprintf (BRENTROOT_DEBUG, " revert to bisection\n"));  \
              step0_ = half_interval;                                   \
              step1_ = half_interval;                                   \
            }                                                           \
        }                                                               \
      else                                                              \
        {                                                               \
          /* Revert to bisection. */                                    \
          _BRENTROOT_DEBUGGING_CODE                                     \
            (fprintf (BRENTROOT_DEBUG, " revert to bisection\n"));      \
          step0_ = half_interval;                                       \
          step1_ = half_interval;                                       \
        }                                                               \
    }                                                                   \
  while (false)

#define _LINEAR_INTERPOLATION(fa, fb, half_interval, tol1,      \
                              step0, step1, step0_, step1_)     \
  do                                                            \
    {                                                           \
      const BRENTROOT_REAL s = fb / fa;                         \
      const BRENTROOT_REAL p = 2 * half_interval * s;           \
      const BRENTROOT_REAL q = 1 - s;                           \
      if (0 < p)                                                \
        _DECIDE_STEP (p, -q, half_interval, tol1,               \
                      step0, step1, step0_, step1_);            \
      else                                                      \
        _DECIDE_STEP (-p, q, half_interval, tol1,               \
                      step0, step1, step0_, step1_);            \
    }                                                           \
  while (false)

#define _INVERSE_QUADRATIC_INTERPOLATION(fa, fb, fc,                    \
                                         half_interval, tol1,           \
                                         step0, step1,                  \
                                         step0_, step1_)                \
  do                                                                    \
    {                                                                   \
      const BRENTROOT_REAL q = fa / fc;                                 \
      const BRENTROOT_REAL r = fb / fc;                                 \
      const BRENTROOT_REAL s = fb / fa;                                 \
                                                                        \
      const BRENTROOT_REAL q_minus_1 = q - 1;                           \
      const BRENTROOT_REAL r_minus_1 = r - 1;                           \
      const BRENTROOT_REAL s_minus_1 = s - 1;                           \
                                                                        \
      const BRENTROOT_REAL pp = s * ((2 * half_interval * q * (q - r))  \
                                     - ((b - a) * r_minus_1));          \
      const BRENTROOT_REAL qq = q_minus_1 * r_minus_1 * s_minus_1;      \
                                                                        \
      if (0 < pp)                                                       \
        _DECIDE_STEP (pp, -qq, half_interval, tol1,                     \
                      step0, step1, step0_, step1_);                    \
      else                                                              \
        _DECIDE_STEP (-pp, qq, half_interval, tol1,                     \
                      step0, step1, step0_, step1_);                    \
    }                                                                   \
  while (false)

// Move the bracket by at least tol1.
#define _ACTUAL_STEP(step, half_interval, tol1)                         \
  (((tol1) < _ABS (step)) ? (step) : _APPLY_SIGN ((half_interval), (tol1)))

#define _INITIALIZE_ITERATION(a, a_,            \
                              b, b_,            \
                              c, c_,            \
                              fa, fa_,          \
                              fb, fb_,          \
                              fc, fc_,          \
                              step0, step0_,    \
                              step1, step1_)    \
  do                                            \
    {                                           \
      const BRENTROOT_REAL a___ = (a_);         \
      const BRENTROOT_REAL b___ = (b_);         \
      const BRENTROOT_REAL c___ = (c_);         \
      const BRENTROOT_REAL fa___ = (fa_);       \
      const BRENTROOT_REAL fb___ = (fb_);       \
      const BRENTROOT_REAL fc___ = (fc_);       \
      const BRENTROOT_REAL step0___ = (step0_); \
      const BRENTROOT_REAL step1___ = (step1_); \
                                                \
      a = a___;                                 \
      b = b___;                                 \
      c = c___;                                 \
      fa = fa___;                               \
      fb = fb___;                               \
      fc = fc___;                               \
      step0 = step0___;                         \
      step1 = step1___;                         \
    }                                           \
  while (false)

#define _CHECK_EPSILON(EPS)                                             \
  do                                                                    \
    {                                                                   \
      if ((EPS) == 0)                                                   \
        {                                                               \
          /* (*info = -6) indicates a problem in the 6th parameter. */  \
          *info = -6;                                                   \
          return;                                                       \
        }                                                               \
    }                                                                   \
  while (false)

#define _CHECK_BRACKETED(VAL0, VAL1)                    \
  do                                                    \
    {                                                   \
      if (!_BRACKETED ((VAL0), (VAL1)))                 \
        {                                               \
          /* (*info = 1) indicates the start points */  \
          /* do not evaluate to opposite signs.     */  \
          *info = 1;                                    \
          return;                                       \
        }                                               \
    }                                                   \
  while (false)

VISIBLE void
BRENTROOT_FUNC (BRENTROOT_REAL t0, BRENTROOT_REAL t1,
                BRENTROOT_REAL (*f) (BRENTROOT_REAL, void *),
                void *data, BRENTROOT_REAL tolerance, BRENTROOT_REAL epsilon,
                BRENTROOT_REAL *root, int *info)
{
  assert (root != NULL);
  assert (info != NULL);

  BRENTROOT_REAL a;
  BRENTROOT_REAL b;
  BRENTROOT_REAL c;
  BRENTROOT_REAL fa;
  BRENTROOT_REAL fb;
  BRENTROOT_REAL fc;
  BRENTROOT_REAL step0;
  BRENTROOT_REAL step1;

  tolerance = _IF_NEGATIVE_THEN_DEFAULT (tolerance, 0);
  epsilon = _IF_NEGATIVE_THEN_DEFAULT (epsilon, BRENTROOT_EPSILON);

  _CHECK_EPSILON (epsilon);

  const BRENTROOT_REAL ft0 = f (t0, data);
  const BRENTROOT_REAL ft1 = f (t1, data);

  _CHECK_BRACKETED (ft0, ft1);

  const BRENTROOT_REAL t1_minus_t0 = t1 - t0;
  if (_ABS (ft0) < _ABS (ft1))
    {
      // *INDENT-OFF*
      _INITIALIZE_ITERATION (a, t1,
                             b, t0,
                             c, t1,
                             fa, ft1,
                             fb, ft0,
                             fc, ft1,
                             step0, t1_minus_t0,
                             step1, t1_minus_t0);
      // *INDENT-ON*
    }
  else
    {
      // *INDENT-OFF*
      _INITIALIZE_ITERATION (a, t0,
                             b, t1,
                             c, t0,
                             fa, ft0,
                             fb, ft1,
                             fc, ft0,
                             step0, t1_minus_t0,
                             step1, t1_minus_t0);
      // *INDENT-ON*
    }

iteration:
  {
    const BRENTROOT_REAL tol1 = (2 * epsilon * _ABS (b)) + _HALF (tolerance);
    const BRENTROOT_REAL half_interval = _HALF (c - b);
    if (_ABS (half_interval) <= tol1 || fb == 0)
      {
        // Return the result.
        _BRENTROOT_DEBUGGING_CODE (fprintf
                                   (BRENTROOT_DEBUG, " result = %25.17LE\n",
                                    (long double) b));
        *root = b;
        *info = 0;              // *info=0 indicates ‘success’.
        return;
      }
    else
      {
        BRENTROOT_REAL step0_;
        BRENTROOT_REAL step1_;

        // Set step0_ and step1_.
        if (tol1 <= _ABS (step1) && _ABS (fb) < _ABS (fa))
          {
            if (a == c)
              {
                _BRENTROOT_DEBUGGING_CODE
                  (fprintf (BRENTROOT_DEBUG, " linear interpolation\n"));
                _LINEAR_INTERPOLATION (fa, fb, half_interval, tol1,
                                       step0, step1, step0_, step1_);
              }
            else
              {
                _BRENTROOT_DEBUGGING_CODE
                  (fprintf
                   (BRENTROOT_DEBUG, " inverse quadratic interpolation\n"));
                _INVERSE_QUADRATIC_INTERPOLATION (fa, fb, fc, half_interval,
                                                  tol1, step0, step1, step0_,
                                                  step1_);
              }
          }
        else
          {
            _BRENTROOT_DEBUGGING_CODE (fprintf
                                       (BRENTROOT_DEBUG, " bisection\n"));
            step0_ = half_interval;
            step1_ = half_interval;
          }

        const BRENTROOT_REAL delta = _ACTUAL_STEP (step0_, half_interval, tol1);
        _BRENTROOT_DEBUGGING_CODE
          ((fprintf (BRENTROOT_DEBUG, " (abs (delta) == tol1) ?   %d\n",
                     (_ABS (delta) == tol1)),
            fprintf (BRENTROOT_DEBUG, " delta = %26.17LE\n",
                     (long double) delta)));
        const BRENTROOT_REAL b_ = b + delta;
        const BRENTROOT_REAL fb_ = f (b_, data);

        if (_BRACKETED (fb_, fc))
          {
            if (_ABS (fb_) <= _ABS (fc))
              {
                // *INDENT-OFF*
                _INITIALIZE_ITERATION (a, b,
                                       b, b_,
                                       c, c,
                                       fa, fb,
                                       fb, fb_,
                                       fc, fc,
                                       step0, step0_,
                                       step1, step1_);
                // *INDENT-ON*
              }
            else
              {

                // *INDENT-OFF*
                _INITIALIZE_ITERATION (a, b_,
                                       b, c,
                                       c, b_,
                                       fa, fb_,
                                       fb, fc,
                                       fc, fb_,
                                       step0, step0_,
                                       step1, step1_);
                // *INDENT-ON*
              }
          }
        else
          {
            // Recompute delta, to account for roundoff.
            //
            // FIXME: Is this necessary?
            //
            const double delta = b_ - b;

            if (_ABS (fb_) <= _ABS (fb))
              {
                // *INDENT-OFF*
                _INITIALIZE_ITERATION (a, b,
                                       b, b_,
                                       c, b,
                                       fa, fb,
                                       fb, fb_,
                                       fc, fb,
                                       step0, delta,
                                       step1, delta);
                // *INDENT-ON*
              }
            else
              {
                // *INDENT-OFF*
                _INITIALIZE_ITERATION (a, b_,
                                       b, b,
                                       c, b_,
                                       fa, fb_,
                                       fb, fb,
                                       fc, fb_,
                                       step0, delta,
                                       step1, delta);
                // *INDENT-ON*
              }
          }

        goto iteration;
      }
  }

  // This point never should be reached.
  assert (false);
}
