#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// Based in part on c-strncasecmp.c from Gnulib, which is
// Copyright (C) 1998-1999, 2005-2006, 2009-2015 Free Software Foundation, Inc.
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <limits.h>

#include "memcasecmp.h"
#include "c-ctype.h"

int smcore_istrings__memcasecmp_for_istrings (const void *s1, const void *s2,
                                              size_t n);
int smcore_istrings__c_memcasecmp_for_istrings (const void *s1, const void *s2,
                                                size_t n);

VISIBLE int
smcore_istrings__memcasecmp_for_istrings (const void *s1, const void *s2,
                                          size_t n)
{
  return memcasecmp (s1, s2, n);
}

VISIBLE int
smcore_istrings__c_memcasecmp_for_istrings (const void *s1, const void *s2,
                                            size_t n)
{
  unsigned char c1;
  unsigned char c2;
  int result;
  const unsigned char *p1 = (const unsigned char *) s1;
  const unsigned char *p2 = (const unsigned char *) s2;

  if (p1 == p2 || n == 0)
    result = 0;
  else
    {
      do
        {
          c1 = c_tolower (*p1);
          c2 = c_tolower (*p2);
          p1++;
          p2++;
          n--;
        }
      while (c1 == c2 && n != 0);

      if (UCHAR_MAX <= INT_MAX)
        result = (int) c1 - (int) c2;
      else
        /* On machines where 'char' and 'int' are types of the same
           size, the difference of two 'unsigned char' values -
           including the sign bit - doesn't fit in an 'int'.  */
        result = (c1 > c2) ? 1 : ((c1 < c2) ? -1 : 0);
    }

  return result;
}

