#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

// `LONG DOUBLE' PRECISION CYCLE TRIDIAGONAL ELIMINATION.

#include <sortsmill/core/cyclictridiagsolve.h>

#define PERIODIC_TRIDIAG_REAL long double
#define PERIODIC_TRIDIAG_TRIDIAGSOLVE ltridiagsolve
#define PERIODIC_TRIDIAG_FUNC lcyclictridiagsolve
#include "periodic_tridiag_solve__.c"
