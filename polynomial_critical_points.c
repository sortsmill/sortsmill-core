#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

VISIBLE void
poly_criticalpoints_mono (size_t degree, ssize_t stride, mpq_t *poly,
                          const mpq_t a, const mpq_t b,
                          size_t *num_points, double t[degree - 1])
{
  if (degree < 2)
    *num_points = 0;
  else
    {
      mpq_t *deriv = xmalloc (degree * sizeof (mpq_t));
      mpq_init_poly (degree - 1, deriv);
      mpq_poly_deriv_mono (degree, stride, poly, 1, deriv);

      //
      // FIXME: Perhaps use a fast evaluator.
      //
      dpoly_findroots (degree - 1, 1, deriv, a, b, NULL, NULL,
                       NULL, num_points, t, NULL);

      mpq_clear_poly (degree - 1, deriv);
      free (deriv);
    }
}

static void
_dpoly_criticalpoints (void (*derivative) (size_t, ssize_t, const double *,
                                           ssize_t, double *),
                       void (*findroots) (size_t, ssize_t, const double *,
                                          double, double, bool *,
                                          size_t *, double *),
                       size_t degree, ssize_t stride, const double *poly,
                       double a, double b,
                       size_t *num_points, double t[degree - 1])
{
  if (degree < 2)
    *num_points = 0;
  else
    {
      double deriv[degree];
      derivative (degree, stride, poly, 1, deriv);
      findroots (degree - 1, 1, deriv, a, b, NULL, num_points, t);
    }
}

VISIBLE void
dpoly_criticalpoints_bernsv (size_t degree, ssize_t stride, const double *poly,
                             double a, double b,
                             size_t *num_points, double t[degree - 1])
{
  _dpoly_criticalpoints (dpoly_deriv_bern, dpoly_findroots_bernsv,
                         degree, stride, poly, a, b, num_points, t);
}

VISIBLE void
dpoly_criticalpoints_berndc (size_t degree, ssize_t stride, const double *poly,
                             double a, double b,
                             size_t *num_points, double t[degree - 1])
{
  _dpoly_criticalpoints (dpoly_deriv_bern, dpoly_findroots_berndc,
                         degree, stride, poly, a, b, num_points, t);
}

VISIBLE void
dpoly_criticalpoints_sbernsv (size_t degree, ssize_t stride, const double *poly,
                              double a, double b,
                              size_t *num_points, double t[degree - 1])
{
  _dpoly_criticalpoints (dpoly_deriv_sbern, dpoly_findroots_sbernsv,
                         degree, stride, poly, a, b, num_points, t);
}

VISIBLE void
dpoly_criticalpoints_sberndc (size_t degree, ssize_t stride, const double *poly,
                              double a, double b,
                              size_t *num_points, double t[degree - 1])
{
  _dpoly_criticalpoints (dpoly_deriv_sbern, dpoly_findroots_sberndc,
                         degree, stride, poly, a, b, num_points, t);
}

VISIBLE void
dpoly_criticalpoints_spower (size_t degree, ssize_t stride, const double *poly,
                             double a, double b,
                             size_t *num_points, double t[degree - 1])
{
  _dpoly_criticalpoints (dpoly_deriv_spower, dpoly_findroots_spower,
                         degree, stride, poly, a, b, num_points, t);
}
