#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <math.h>
#include <sortsmill/core.h>

// Evaluate a real polynomial given in the ordinary monomial basis,
VISIBLE void
mpq_poly_eval_mono (size_t degree, ssize_t stride, mpq_t *poly, const mpq_t t,
                    mpq_t value)
{
  // Horner’s rule.

  mpq_t tmp;
  mpq_init (tmp);

  mpq_set (value, poly[stride * (ssize_t) degree]);
  for (size_t i = 1; i <= degree; i++)
    {
      mpq_mul (tmp, value, t);
      mpq_add (value, tmp, poly[stride * (ssize_t) (degree - i)]);
    }

  mpq_clear (tmp);
}

VISIBLE double
dpoly_eval_mono (size_t degree, ssize_t stride, const double *poly, double t)
{
  // Horner’s rule.
  double x = poly[stride * (ssize_t) degree];
  for (size_t i = 1; i <= degree; i++)
    x = fma (x, t, poly[stride * (ssize_t) (degree - i)]);
  return x;
}

VISIBLE double
dpoly_eval_sbernsv (size_t degree, ssize_t stride, const double *poly, double t)
{
  // Schumaker-Volk fast evaluation of a polynomial in scaled
  // Bernstein basis.

  double v;

  const double s = 1.0 - t;

  if (t <= 0.5)
    {
      // Horner form in the variable @var{u} = @var{t} / @var{s}.
      double u = t / s;
      v = poly[stride * (ssize_t) degree];
      for (size_t i = 1; i <= degree; i++)
        v = fma (v, u, poly[stride * (ssize_t) (degree - i)]);

      // Multiply by @var{s} raised to the power @var{degree}.
      double power = s;
      size_t i = degree;
      while (i != 0)
        {
          if ((i & 1) != 0)
            v *= power;
          i >>= 1;
          if (i != 0)
            power *= power;
        }
    }
  else
    {
      // Horner form in the variable @var{u} = @var{s} / @var{t}.
      double u = s / t;
      v = poly[0];
      for (size_t i = 1; i <= degree; i++)
        v = fma (v, u, poly[stride * (ssize_t) i]);

      // Multiply by @var{t} raised to the power @var{degree}.
      double power = t;
      size_t i = degree;
      while (i != 0)
        {
          if ((i & 1) != 0)
            v *= power;
          i >>= 1;
          if (i != 0)
            power *= power;
        }
    }
  return v;
}

VISIBLE double
dpoly_eval_bernsv (size_t degree, ssize_t stride, const double *poly, double t)
{
  // Schumaker-Volk fast evaluation of a polynomial in Bernstein
  // basis.

  double v;

  const double s = 1.0 - t;

  if (t <= 0.5)
    {
      // Horner form in the variable @var{u} = @var{t} / @var{s}.
      double u = t / s;
      v = poly[stride * (ssize_t) degree];
      for (size_t i = 1; i <= degree; i++)
        v = fma (v, u, (bincoef (degree, degree - i) *
                        poly[stride * (ssize_t) (degree - i)]));

      // Multiply by @var{s} raised to the power @var{degree}.
      double power = s;
      size_t i = degree;
      while (i != 0)
        {
          if ((i & 1) != 0)
            v *= power;
          i >>= 1;
          if (i != 0)
            power *= power;
        }
    }
  else
    {
      // Horner form in the variable @var{u} = @var{s} / @var{t}.
      double u = s / t;
      v = poly[0];
      for (size_t i = 1; i <= degree; i++)
        v = fma (v, u, bincoef (degree, i) * poly[stride * (ssize_t) i]);

      // Multiply by @var{t} raised to the power @var{degree}.
      double power = t;
      size_t i = degree;
      while (i != 0)
        {
          if ((i & 1) != 0)
            v *= power;
          i >>= 1;
          if (i != 0)
            power *= power;
        }
    }
  return v;
}

VISIBLE double
dpoly_eval_sberndc (size_t degree, ssize_t stride, const double *poly, double t)
{
  // Evaluation of a polynomial in scaled Bernstein basis by the
  // method of de Casteljau.

  const double t1 = 1.0 - t;

  double b[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    b[i] = poly[stride * (ssize_t) i] / bincoef (degree, i);
  for (size_t i = 0; i < degree; i++)
    for (size_t j = 0; j < degree - i; j++)
      b[j] = t1 * b[j] + t * b[j + 1];
  return b[0];
}

VISIBLE double
dpoly_eval_berndc (size_t degree, ssize_t stride, const double *poly, double t)
{
  // Evaluation of a polynomial in Bernstein basis by the method of de
  // Casteljau.

  const double t1 = 1.0 - t;

  double b[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    b[i] = poly[stride * (ssize_t) i];
  for (size_t i = 0; i < degree; i++)
    for (size_t j = 0; j < degree - i; j++)
      b[j] = t1 * b[j] + t * b[j + 1];
  return b[0];
}

VISIBLE double
dpoly_eval_spower (size_t degree, ssize_t stride, const double *poly, double t)
{
  // Compute a convex combination of two shorter polynomials in
  //
  //    s = t(1 − t)
  //
  // The Sánchez-Reyes coefficients are the monomial coefficients of
  // the two shorter polynomials in s.

  const double t1 = 1.0 - t;
  const double s = t * t1;

  const double left = dpoly_eval_mono (degree / 2, stride, poly, s);
  const double right =
    dpoly_eval_mono (degree / 2, -stride, &poly[stride * (ssize_t) degree], s);

  return (t1 * left + t * right);
}
