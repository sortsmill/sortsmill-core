#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

static inline size_t
szmax (size_t a, size_t b)
{
  return (a < b) ? b : a;
}

//----------------------------------------------------------------------

static void _find_self_intersections (size_t degree_a,
                                      mpq_t
                                      implicit_a[degree_a + 1][degree_a + 1],
                                      size_t degree_b,
                                      ssize_t stride_bx, mpq_t *bx,
                                      ssize_t stride_by, mpq_t *by,
                                      const mpq_t t0, const mpq_t t1,
                                      size_t *num_intersections,
                                      double t[degree_a * degree_b]);

VISIBLE void
intersect_implicit_with_parametric (size_t degree_a,
                                    mpq_t
                                    implicit_a[degree_a + 1][degree_a + 1],
                                    size_t degree_b,
                                    ssize_t stride_bx, mpq_t *bx,
                                    ssize_t stride_by, mpq_t *by,
                                    const mpq_t t0, const mpq_t t1,
                                    bool *the_curves_are_the_same,
                                    size_t *num_intersections,
                                    double t[degree_a * degree_b])
{
  // Find intersections between two polynomial curves in the plane,
  // by the method of implicitization.
  //
  // The first curve is given by an implicit equation, f(x,y)=0. The
  // function f is a bivariate polynomial in x and y. The second is
  // given parametrically by the (bx,by), which is in the monomial
  // basis. Plug x=bx(t), y=by(t) into f, resulting in an equation
  // g(t)=f(x(t),y(t))=0. The function g is a polynomial in t, of
  // degree degree1*degree2. (For instance, for the intersections of
  // two cubic splines, g will be a polynomial of degree 9.) The roots
  // of g, then, are the (ax,bx)-parameters of the points of
  // intersection. Return all such roots in [t0,t1].
  //
  // If the curves have the same implicit equation, a different method
  // must be used. See _find_self_intersections().

  // For efficiency, first reduce the degree of the parametric curve
  // to its ‘actual’ degree, and to help us decide quickly if there
  // might be any self-intersections.
  //
  // FIXME: Consider doing the same with the implicit equation.
  //
  const size_t degree_bx = mpq_poly_mindegree_mono (degree_b, stride_bx, bx);
  const size_t degree_by = mpq_poly_mindegree_mono (degree_b, stride_by, by);
  degree_b = szmax (degree_bx, degree_by);

  mpq_t *big_poly = xmalloc ((degree_a * degree_b + 1) * sizeof (mpq_t));
  mpq_init_poly (degree_a * degree_b, big_poly);

  mpq_poly_plugintoimplicit_mono (degree_a, implicit_a,
                                  degree_b, stride_bx, bx, stride_by, by,
                                  1, big_poly);

  bool identically_zero;

  //
  // FIXME: Perhaps use a fast evaluator for big_poly.
  //
  dpoly_findroots (mpq_poly_mindegree_mono (degree_a * degree_b, 1, big_poly),
                   1, big_poly, t0, t1, NULL, NULL,
                   &identically_zero, num_intersections, t, NULL);

  if (the_curves_are_the_same != NULL)
    *the_curves_are_the_same = identically_zero;

  if (identically_zero && 3 <= degree_b)
    _find_self_intersections (degree_a, implicit_a,
                              degree_b, stride_bx, bx, stride_by, by,
                              t0, t1, num_intersections, t);

  mpq_clear_poly (degree_a * degree_b, big_poly);
  free (big_poly);
}

static void
_find_self_intersections (size_t degree_a,
                          mpq_t implicit_a[degree_a + 1][degree_a + 1],
                          size_t degree_b,
                          ssize_t stride_bx, mpq_t *bx,
                          ssize_t stride_by, mpq_t *by,
                          const mpq_t t0, const mpq_t t1,
                          size_t *num_intersections,
                          double t[degree_a * degree_b])
{
  // This routine assumes implicit_a and (bx,by) describe the same
  // curve. Be sure that is the case before you call this function.
  //
  // Check the surface defined by implicit_a for saddle points, by
  // examining its gradient for zeros. See Jan B. Thomassen,
  // ‘Self-Intersection Problems and Approximate Implicitization’, in
  // ‘Computational Methods for Algebraic Spline Surfaces’, 2005,
  // pp. 155-170. In particular, see page 160. We bother only to check
  // for ‘transversal’ (non-tangential) self-intersections and cusps
  // (which are the only sorts of self-intersections that occur in
  // non-degenerate cubic splines).
  //
  // FIXME (very low priority): Consider finding tangent intersections
  // of higher-degree curves.

  const size_t degree_bx = mpq_poly_mindegree_mono (degree_b, stride_bx, bx);
  const size_t degree_by = mpq_poly_mindegree_mono (degree_b, stride_by, by);
  degree_b = szmax (degree_bx, degree_by);

  *num_intersections = 0;

  if (3 <= degree_b)
    {
      mpq_t minus_one;
      mpq_init (minus_one);
      mpq_set_si (minus_one, -1, 1);

      // Find the partial derivatives (Dx,Dy) of implicit_a, which are the
      // components of the gradient.
      const size_t degree_D = degree_a - 1;
      mpq_t (*Dx)[degree_D + 1] =
        xmalloc ((degree_D + 1) * (degree_D + 1) * sizeof (mpq_t));
      mpq_bipoly_init (degree_D, Dx);
      mpq_bipoly_partial_deriv_wrt_x (degree_a, implicit_a, Dx);
      mpq_t (*Dy)[degree_D + 1] =
        xmalloc ((degree_D + 1) * (degree_D + 1) * sizeof (mpq_t));
      mpq_bipoly_init (degree_D, Dy);
      mpq_bipoly_partial_deriv_wrt_y (degree_a, implicit_a, Dy);

      // Rewrite them in terms of the parameter t.
      const size_t degree_D_of_t = degree_D * degree_b;
      mpq_t *Dx_of_t = xmalloc ((degree_D_of_t + 1) * sizeof (mpq_t));
      mpq_init_poly (degree_D_of_t, Dx_of_t);
      mpq_poly_plugintoimplicit_mono (degree_D, Dx,
                                      degree_b, stride_bx, bx, stride_by, by,
                                      1, Dx_of_t);
      mpq_t *Dy_of_t = xmalloc ((degree_D_of_t + 1) * sizeof (mpq_t));
      mpq_init_poly (degree_D_of_t, Dy_of_t);
      mpq_poly_plugintoimplicit_mono (degree_D, Dy,
                                      degree_b, stride_bx, bx, stride_by, by,
                                      1, Dy_of_t);

      mpq_bipoly_clear (degree_D, Dy);
      free (Dy);
      mpq_bipoly_clear (degree_D, Dx);
      free (Dx);

      // Find a vector normal to the curve, by finding a tangent vector
      // and rotating it. (Here the tangent vector arbitrarily is in the
      // direction of increasing t, and the rotation arbitrarily is
      // counterclockwise.)
      const size_t degree_normal = degree_b - 1;
      mpq_t *normal_x = xmalloc ((degree_normal + 1) * sizeof (mpq_t));
      mpq_init_poly (degree_normal, normal_x);
      mpq_poly_deriv_mono (degree_b, stride_by, by, 1, normal_x);
      mpq_poly_scalarmul (degree_b - 1, 1, normal_x, minus_one, 1, normal_x);
      mpq_t *normal_y = xmalloc ((degree_normal + 1) * sizeof (mpq_t));
      mpq_init_poly (degree_normal, normal_y);
      mpq_poly_deriv_mono (degree_b, stride_bx, bx, 1, normal_y);

      // Form the dot product of the gradient vector of implicit_a
      // with the normal vector of the curve, thus obtaining a scalar
      // function we can search for zeros. A zero will occur when
      // either the gradient or the normal vanishes.
      const size_t degree_dp = degree_D_of_t + degree_normal + 1;
      mpq_t *dot_product = xmalloc ((degree_dp + 1) * sizeof (mpq_t));
      mpq_init_poly (degree_dp, dot_product);
      mpq_t *tmp = xmalloc ((degree_dp + 1) * sizeof (mpq_t));
      mpq_init_poly (degree_dp, tmp);
      mpq_poly_mul_mono (degree_D_of_t, 1, Dx_of_t, degree_normal, 1, normal_x,
                         1, dot_product);
      mpq_poly_mul_mono (degree_D_of_t, 1, Dy_of_t, degree_normal, 1, normal_y,
                         1, tmp);
      // FIXME: The following could be done with a ‘mpq_poly_add()’
      // function that adds polynomials of the same degree.
      mpq_poly_add_mono (degree_dp, 1, dot_product, degree_dp, 1, tmp,
                         1, dot_product);
      mpq_clear_poly (degree_dp, tmp);
      free (tmp);

      mpq_clear_poly (degree_normal, normal_y);
      free (normal_y);
      mpq_clear_poly (degree_normal, normal_x);
      free (normal_x);
      mpq_clear_poly (degree_D_of_t, Dy_of_t);
      free (Dy_of_t);
      mpq_clear_poly (degree_D_of_t, Dx_of_t);
      free (Dx_of_t);

      //
      // FIXME: Perhaps use a fast evaluator.
      //
      dpoly_findroots (mpq_poly_mindegree_mono (degree_dp, 1, dot_product),
                       1, dot_product, t0, t1, NULL, NULL,
                       NULL, num_intersections, t, NULL);

      mpq_clear_poly (degree_dp, dot_product);
      free (dot_product);

      mpq_clear (minus_one);
    }
}

//----------------------------------------------------------------------

static inline double
_distance_estimate (double x1, double y1, double x2, double y2)
{
  return fmax (fabs (x2 - x1), fabs (y2 - y1));
}

static size_t
_count_matches_a (double tolerance,
                  size_t na, double xa[na], double ya[na], bool used_a[na],
                  size_t i)
{
  const double xi = xa[i];
  const double yi = ya[i];
  size_t n = 1;
  for (size_t j = i + 1; j < na; j++)
    n += (!used_a[j] && _distance_estimate (xi, yi, xa[j], ya[j]) <= tolerance);
  return n;
}

static size_t
_count_matches_b (double tolerance, double xa, double ya,
                  size_t nb, double xb[nb], double yb[nb], bool used_b[nb])
{
  size_t n = 0;
  for (size_t j = 0; j < nb; j++)
    n += (!used_b[j] && _distance_estimate (xa, ya, xb[j], yb[j]) <= tolerance);
  return n;
}

static void
_gather_matches_a (double tolerance,
                   size_t na, double ta[na], double xa[na], double ya[na],
                   bool used_a[na], size_t i, double t[])
{
  const double xi = xa[i];
  const double yi = ya[i];

  t[0] = ta[i];
  used_a[i] = true;

  size_t k = 1;
  for (size_t j = i + 1; j < na; j++)
    if (!used_a[j] && _distance_estimate (xi, yi, xa[j], ya[j]) <= tolerance)
      {
        t[k] = ta[j];
        k++;
        used_a[j] = true;
      }
}

static void
_gather_matches_b (double tolerance, double xa, double ya,
                   size_t nb, double tb[nb], double xb[nb], double yb[nb],
                   bool used_b[nb], double t[])
{
  size_t k = 0;
  for (size_t j = 0; j < nb; j++)
    if (!used_b[j] && _distance_estimate (xa, ya, xb[j], yb[j]) <= tolerance)
      {
        t[k] = tb[j];
        k++;
        used_b[j] = true;
      }
}

static plane_intersections_t *
_match_intersections (double tolerance,
                      size_t na, double ta[na], double xa[na], double ya[na],
                      size_t nb, double tb[nb], double xb[nb], double yb[nb])
{
  plane_intersections_t *intersections =
    x_gc_malloc (sizeof (plane_intersections_t));
  intersections->xy =
    x_gc_malloc ((na + nb) * sizeof (plane_intersections_entry_t));

  bool *used_a = xzalloc (na * sizeof (bool));
  bool *used_b = xzalloc (nb * sizeof (bool));

  for (size_t i = 0; i < na; i++)
    if (!used_a[i])
      {
        const size_t num_tb =
          _count_matches_b (tolerance, xa[i], ya[i], nb, xb, yb, used_b);
        if (0 < num_tb)
          {
            plane_intersections_entry_t *entry =
              &intersections->xy[intersections->num_xy];
            intersections->num_xy++;

            entry->x = xa[i];
            entry->y = ya[i];
            entry->num_ta = _count_matches_a (tolerance, na, xa, ya, used_a, i);
            entry->ta = x_gc_malloc_atomic (entry->num_ta * sizeof (double));
            entry->num_tb = num_tb;
            entry->tb = x_gc_malloc_atomic (entry->num_tb * sizeof (double));

            _gather_matches_a (tolerance, na, ta, xa, ya, used_a, i, entry->ta);
            _gather_matches_b (tolerance, xa[i], ya[i], nb, tb, xb, yb, used_b,
                               entry->tb);
          }
      }

  free (used_b);
  free (used_a);

  return intersections;
}

static bool
_splines_are_the_same (size_t degree_a,
                       ssize_t stride_ax, mpq_t *ax,
                       ssize_t stride_ay, mpq_t *ay,
                       const mpq_t ta0, const mpq_t ta1,
                       size_t degree_b,
                       ssize_t stride_bx, mpq_t *bx,
                       ssize_t stride_by, mpq_t *by,
                       const mpq_t tb0, const mpq_t tb1)
{
  bool same = true;
  if (degree_a != degree_b)
    same = false;
  else
    {
      ssize_t i = 0;
      while (i <= degree_a
             && mpq_equal (ax[i * stride_ax], bx[i * stride_bx])
             && mpq_equal (ay[i * stride_ay], by[i * stride_by]))
        i++;
      if (i != degree_a + 1 || !mpq_equal (ta0, tb0) || !mpq_equal (ta1, tb1))
        same = false;
    }
  return same;
}

static double
_mpq_pick_a_tolerance (size_t degree_a,
                       ssize_t stride_ax, mpq_t *ax,
                       ssize_t stride_ay, mpq_t *ay)
{
  double maxval = -HUGE_VAL;
  for (size_t i = 0; i <= degree_a; i++)
    {
      maxval = fmax (maxval, fabs (mpq_get_d (*ax)));
      ax += stride_ax;
      maxval = fmax (maxval, fabs (mpq_get_d (*ay)));
      ay += stride_ay;
    }
  return 100 * maxval * DBL_EPSILON;
}

static plane_intersections_t *
_poly_findintersections_mono_general (size_t degree_a,
                                      ssize_t stride_ax, mpq_t *ax,
                                      ssize_t stride_ay, mpq_t *ay,
                                      mpq_t (*implicit_a)[degree_a + 1],
                                      const mpq_t ta0, const mpq_t ta1,
                                      size_t degree_b,
                                      ssize_t stride_bx, mpq_t *bx,
                                      ssize_t stride_by, mpq_t *by,
                                      mpq_t (*implicit_b)[degree_b + 1],
                                      const mpq_t tb0, const mpq_t tb1,
                                      double tolerance)
{
  plane_intersections_t *intersections;

  if (tolerance < 0)
    tolerance =
      fmax (_mpq_pick_a_tolerance (degree_a, stride_ax, ax, stride_ay, ay),
            _mpq_pick_a_tolerance (degree_b, stride_bx, bx, stride_by, by));

  if (implicit_a == NULL)
    {
      // No implicit equation was given for (ax,ay). Implicitize
      // (ax,ay) and try again.
      implicit_a = xmalloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
      mpq_bipoly_init (degree_a, implicit_a);
      mpq_poly_implicitize_mono (degree_a, stride_ax, ax, stride_ay, ay,
                                 implicit_a, NULL);
      intersections =
        _poly_findintersections_mono_general (degree_a, stride_ax, ax,
                                              stride_ay, ay,
                                              implicit_a, ta0, ta1,
                                              degree_b, stride_bx, bx,
                                              stride_by, by,
                                              implicit_b, tb0, tb1, tolerance);
      mpq_bipoly_clear (degree_a, implicit_a);
      free (implicit_a);
    }
  else if (implicit_b == NULL)
    {
      // No implicit equation was given for (bx,by). Implicitize
      // (bx,by) and try again.
      implicit_b = xmalloc ((degree_b + 1) * (degree_b + 1) * sizeof (mpq_t));
      mpq_bipoly_init (degree_b, implicit_b);
      mpq_poly_implicitize_mono (degree_b, stride_bx, bx, stride_by, by,
                                 implicit_b, NULL);
      intersections =
        _poly_findintersections_mono_general (degree_a, stride_ax, ax,
                                              stride_ay, ay,
                                              implicit_a, ta0, ta1,
                                              degree_b, stride_bx, bx,
                                              stride_by, by,
                                              implicit_b, tb0, tb1, tolerance);
      mpq_bipoly_clear (degree_b, implicit_b);
      free (implicit_b);
    }
  else
    {
      mpq_t t, tmp;
      mpq_inits (t, tmp, NULL);

      size_t na;
      double *ta = xmalloc ((degree_a * degree_b + 1) * sizeof (double));
      intersect_implicit_with_parametric (degree_b, implicit_b,
                                          degree_a, stride_ax, ax,
                                          stride_ay, ay, ta0, ta1,
                                          NULL, &na, ta);

      double *xa = xmalloc (na * sizeof (double));
      double *ya = xmalloc (na * sizeof (double));
      for (size_t i = 0; i < na; i++)
        {
          mpq_set_d (t, ta[i]);
          mpq_poly_eval_mono (degree_a, stride_ax, ax, t, tmp);
          xa[i] = mpq_get_d (tmp);
          mpq_poly_eval_mono (degree_a, stride_ay, ay, t, tmp);
          ya[i] = mpq_get_d (tmp);
        }

      size_t nb;
      double *tb = xmalloc ((degree_a * degree_b + 1) * sizeof (double));
      intersect_implicit_with_parametric (degree_a, implicit_a,
                                          degree_b, stride_bx, bx,
                                          stride_by, by, tb0, tb1,
                                          NULL, &nb, tb);

      double *xb = xmalloc (nb * sizeof (double));
      double *yb = xmalloc (nb * sizeof (double));
      for (size_t i = 0; i < nb; i++)
        {
          mpq_set_d (t, tb[i]);
          mpq_poly_eval_mono (degree_b, stride_bx, bx, t, tmp);
          xb[i] = mpq_get_d (tmp);
          mpq_poly_eval_mono (degree_b, stride_by, by, t, tmp);
          yb[i] = mpq_get_d (tmp);
        }

      intersections =
        _match_intersections (tolerance, na, ta, xa, ya, nb, tb, xb, yb);

      free (yb);
      free (xb);
      free (tb);

      free (ya);
      free (xa);
      free (ta);

      mpq_clears (t, tmp, NULL);
    }

  return intersections;
}

static plane_intersections_t *
_poly_findintersections_mono_self (size_t degree_a,
                                   ssize_t stride_ax, mpq_t *ax,
                                   ssize_t stride_ay, mpq_t *ay,
                                   mpq_t (*implicit_a)[degree_a + 1],
                                   const mpq_t ta0, const mpq_t ta1,
                                   double tolerance)
{
  plane_intersections_t *intersections;

  if (tolerance < 0)
    tolerance = _mpq_pick_a_tolerance (degree_a, stride_ax, ax, stride_ay, ay);

  if (implicit_a == NULL)
    {
      // No implicit equation was given for (ax,ay). Implicitize
      // (ax,ay) and try again.
      implicit_a = xmalloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
      mpq_bipoly_init (degree_a, implicit_a);
      mpq_poly_implicitize_mono (degree_a, stride_ax, ax, stride_ay, ay,
                                 implicit_a, NULL);
      intersections =
        _poly_findintersections_mono_self (degree_a, stride_ax, ax,
                                           stride_ay, ay, implicit_a,
                                           ta0, ta1, tolerance);
      mpq_bipoly_clear (degree_a, implicit_a);
      free (implicit_a);
    }
  else
    {
      mpq_t t, tmp;
      mpq_inits (t, tmp, NULL);

      size_t na;
      double *ta = xmalloc ((degree_a * degree_a + 1) * sizeof (double));
      _find_self_intersections (degree_a, implicit_a,
                                degree_a, stride_ax, ax, stride_ay, ay,
                                ta0, ta1, &na, ta);

      double *xa = xmalloc (na * sizeof (double));
      double *ya = xmalloc (na * sizeof (double));
      for (size_t i = 0; i < na; i++)
        {
          mpq_set_d (t, ta[i]);
          mpq_poly_eval_mono (degree_a, stride_ax, ax, t, tmp);
          xa[i] = mpq_get_d (tmp);
          mpq_poly_eval_mono (degree_a, stride_ay, ay, t, tmp);
          ya[i] = mpq_get_d (tmp);
        }

      intersections =
        _match_intersections (tolerance, na, ta, xa, ya, na, ta, xa, ya);

      free (ya);
      free (xa);
      free (ta);

      mpq_clears (t, tmp, NULL);
    }

  return intersections;
}

VISIBLE plane_intersections_t *
poly_findintersections_mono (size_t degree_a,
                             ssize_t stride_ax, mpq_t *ax,
                             ssize_t stride_ay, mpq_t *ay,
                             mpq_t (*implicit_a)[degree_a + 1],
                             const mpq_t ta0, const mpq_t ta1,
                             size_t degree_b,
                             ssize_t stride_bx, mpq_t *bx,
                             ssize_t stride_by, mpq_t *by,
                             mpq_t (*implicit_b)[degree_b + 1],
                             const mpq_t tb0, const mpq_t tb1, double tolerance)
{
  plane_intersections_t *intersections;

  if (_splines_are_the_same (degree_a, stride_ax, ax, stride_ay, ay, ta0, ta1,
                             degree_b, stride_bx, bx, stride_by, by, tb0, tb1))
    // Find self-intersections of a spline. The general routine can
    // handle this case, but let us call a function that does less
    // work.
    intersections =
      _poly_findintersections_mono_self (degree_a, stride_ax, ax,
                                         stride_ay, ay, implicit_a,
                                         ta0, ta1, tolerance);
  else
    intersections =
      _poly_findintersections_mono_general (degree_a, stride_ax, ax,
                                            stride_ay, ay, implicit_a,
                                            ta0, ta1,
                                            degree_b, stride_bx, bx,
                                            stride_by, by, implicit_b,
                                            tb0, tb1, tolerance);
  return intersections;
}

//----------------------------------------------------------------------

static double
_min_value (size_t n, ssize_t stride, const double *poly)
{
  double minval = *poly;
  for (size_t i = 1; i <= n; i++)
    {
      minval = fmin (minval, *poly);
      poly += stride;
    }
  return minval;
}

static double
_max_value (size_t n, ssize_t stride, const double *poly)
{
  double maxval = *poly;
  for (size_t i = 1; i <= n; i++)
    {
      maxval = fmax (maxval, *poly);
      poly += stride;
    }
  return maxval;
}

static inline plane_intersections_t *
_empty_plane_intersections_t_object (void)
{
  return x_gc_malloc (sizeof (plane_intersections_t));
}

static plane_intersections_t *
_singleton_plane_intersections_t_object (double x, double y,
                                         double ta, double tb)
{
  plane_intersections_t *intersections =
    x_gc_malloc (sizeof (plane_intersections_t));
  intersections->num_xy = 1;
  intersections->xy = x_gc_malloc (sizeof (plane_intersections_entry_t));
  plane_intersections_entry_t *entry = &intersections->xy[0];
  entry->x = x;
  entry->y = y;
  entry->num_ta = 1;
  entry->ta = x_gc_malloc_atomic (sizeof (double));
  entry->ta[0] = ta;
  entry->num_tb = 1;
  entry->tb = x_gc_malloc_atomic (sizeof (double));
  entry->tb[0] = tb;
  return intersections;
}

static double
_pick_a_tolerance (size_t degree_a,
                   ssize_t stride_ax, const double *ax,
                   ssize_t stride_ay, const double *ay)
{
  double maxval = -HUGE_VAL;
  for (size_t i = 0; i <= degree_a; i++)
    {
      maxval = fmax (maxval, fabs (*ax));
      ax += stride_ax;
      maxval = fmax (maxval, fabs (*ay));
      ay += stride_ay;
    }
  return 100 * maxval * DBL_EPSILON;
}

static void
_double_bern_to_mpq_mono (size_t degree, ssize_t stride, const double *poly,
                          mpq_t poly_[degree + 1])
{
  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set_d (poly_[i], *poly);
      poly += stride;
    }
  mpq_poly_bern_to_mono (degree, 1, poly_, 1, poly_);
}

static bool
_control_polygons_cannot_overlap (size_t degree_a,
                                  ssize_t stride_ax, const double *ax,
                                  ssize_t stride_ay, const double *ay,
                                  size_t degree_b,
                                  ssize_t stride_bx, const double *bx,
                                  ssize_t stride_by, const double *by)
{
  // Returns true if the control points of the two curves are
  // separated by a vertical or horizontal line.
  return ((_max_value (degree_a + 1, stride_ax, ax)
           < _min_value (degree_b + 1, stride_bx, bx))
          || (_max_value (degree_b + 1, stride_bx, bx)
              < _min_value (degree_a + 1, stride_ax, ax))
          || (_max_value (degree_a + 1, stride_ay, ay)
              < _min_value (degree_b + 1, stride_by, by))
          || (_max_value (degree_b + 1, stride_by, by)
              < _min_value (degree_a + 1, stride_ay, ay)));
}

static bool
_control_points_are_on_opposite_sides (double x, double y,
                                       size_t na,
                                       ssize_t stride_ax, const double *ax,
                                       ssize_t stride_ay, const double *ay,
                                       size_t nb,
                                       ssize_t stride_bx, const double *bx,
                                       ssize_t stride_by, const double *by)
{
  // Returns true if control points of the two curves, other than
  // (x,y), are separated by a vertical or horizontal line through
  // (x,y).
  return ((_max_value (na, stride_ax, ax) < x
           && x < _min_value (nb, stride_bx, bx))
          || (_max_value (nb, stride_bx, bx) < x
              && x < _min_value (na, stride_ax, ax))
          || (_max_value (na, stride_ay, ay) < y
              && y < _min_value (nb, stride_by, by))
          || (_max_value (nb, stride_by, by) < y
              && y < _min_value (na, stride_ay, ay)));
}

static plane_intersections_t *
_intersection_at_join_only (size_t degree_a,
                            ssize_t stride_ax, const double *ax,
                            ssize_t stride_ay, const double *ay,
                            size_t degree_b,
                            ssize_t stride_bx, const double *bx,
                            ssize_t stride_by, const double *by)
{
  plane_intersections_t *intersections;

  if (ax[(ssize_t) degree_a * stride_ax] == bx[0]
      && ay[(ssize_t) degree_a * stride_ay] == by[0]
      && _control_points_are_on_opposite_sides (bx[0], by[0],
                                                degree_a, stride_ax, ax,
                                                stride_ay, ay,
                                                degree_b, stride_bx,
                                                bx + stride_bx,
                                                stride_by, by + stride_by))
    // (ax(1),ay(1)) equals (bx(0),by(0)), and it is the only point of
    // intersection.
    intersections =
      _singleton_plane_intersections_t_object (bx[0], by[0], 1, 0);
  else if (bx[(ssize_t) degree_b * stride_bx] == ax[0]
           && by[(ssize_t) degree_b * stride_by] == ay[0]
           && _control_points_are_on_opposite_sides (ax[0], ay[0],
                                                     degree_a, stride_ax,
                                                     ax + stride_ax,
                                                     stride_ay, ay + stride_ay,
                                                     degree_b, stride_bx, bx,
                                                     stride_by, by))
    // (bx(1),by(1)) equals (ax(0),ay(0)), and it is the only point of
    // intersection.
    intersections =
      _singleton_plane_intersections_t_object (ax[0], ay[0], 0, 1);
  else
    // We have not excluded the possibility of their being more
    // intersections.
    //
    // FIXME: Consider implementing a more thorough analysis of the
    // convex hulls of the control polygons.
    //
    intersections = NULL;

  return intersections;
}

VISIBLE plane_intersections_t *
dpoly_findintersections_bern (size_t degree_a,
                              ssize_t stride_ax, const double *ax,
                              ssize_t stride_ay, const double *ay,
                              mpq_t (*implicit_a)[degree_a + 1],
                              double ta0, double ta1,
                              size_t degree_b,
                              ssize_t stride_bx, const double *bx,
                              ssize_t stride_by, const double *by,
                              mpq_t (*implicit_b)[degree_b + 1],
                              double tb0, double tb1, double tolerance)
{
  plane_intersections_t *intersections;

  if (degree_a == 0 || degree_b == 0)
    // This case is degenerate. We could not determine values of the
    // parameters, even if there were indisputable intersections.
    intersections = _empty_plane_intersections_t_object ();
  else if (_control_polygons_cannot_overlap (degree_a,
                                             stride_ax, ax, stride_ay, ay,
                                             degree_b,
                                             stride_bx, bx, stride_by, by))
    intersections = _empty_plane_intersections_t_object ();
  else
    {
      intersections =
        _intersection_at_join_only (degree_a, stride_ax, ax, stride_ay, ay,
                                    degree_b, stride_bx, bx, stride_by, by);
      if (intersections == NULL)
        {
          if (tolerance < 0)
            tolerance =
              fmax (_pick_a_tolerance (degree_a, stride_ax, ax, stride_ay, ay),
                    _pick_a_tolerance (degree_b, stride_bx, bx, stride_by, by));

          mpq_t *ax_ = xmalloc ((degree_a + 1) * sizeof (mpq_t));
          mpq_init_poly (degree_a, ax_);
          _double_bern_to_mpq_mono (degree_a, stride_ax, ax, ax_);

          mpq_t *ay_ = xmalloc ((degree_a + 1) * sizeof (mpq_t));
          mpq_init_poly (degree_a, ay_);
          _double_bern_to_mpq_mono (degree_a, stride_ay, ay, ay_);

          mpq_t *bx_ = xmalloc ((degree_b + 1) * sizeof (mpq_t));
          mpq_init_poly (degree_b, bx_);
          _double_bern_to_mpq_mono (degree_b, stride_bx, bx, bx_);

          mpq_t *by_ = xmalloc ((degree_b + 1) * sizeof (mpq_t));
          mpq_init_poly (degree_b, by_);
          _double_bern_to_mpq_mono (degree_b, stride_by, by, by_);

          mpq_t ta0_, ta1_, tb0_, tb1_;
          mpq_inits (ta0_, ta1_, tb0_, tb1_, NULL);
          mpq_set_d (ta0_, ta0);
          mpq_set_d (ta1_, ta1);
          mpq_set_d (tb0_, tb0);
          mpq_set_d (tb1_, tb1);

          intersections =
            poly_findintersections_mono (degree_a, 1, ax_, 1, ay_,
                                         implicit_a, ta0_, ta1_,
                                         degree_b, 1, bx_, 1, by_,
                                         implicit_b, tb0_, tb1_, tolerance);

          mpq_clears (ta0_, ta1_, tb0_, tb1_, NULL);
          mpq_clear_poly (degree_b, by_);
          free (by_);
          mpq_clear_poly (degree_b, bx_);
          free (bx_);
          mpq_clear_poly (degree_a, ay_);
          free (ay_);
          mpq_clear_poly (degree_a, ax_);
          free (ax_);
        }
    }

  return intersections;
}

//----------------------------------------------------------------------

#if 0                           /* Commented out in favor of ‘pure’ implicitization, but left
                                   here temporarily for reference. */
//
// Find intersections by a mixture of flattening and implicitizing.
//
// The idea for such an algorithm may belong to Richard J. Kinch. See
// http://comp.graphics.algorithms.narkive.com/SPErcVSQ/2d-intersection-algorithm-for-cubic-bezier-curves-who-has-one#post13
//
// We like such an algorithm because, though it employs
// implicitization and thus is not an ‘ad hoc’ analysis of the
// geometry, it does not break down if the two splines are in fact
// different parts of the same curve.

static void
_float_bounding_box_bern (size_t degree,
                          ssize_t stride_ax, const float *ax,
                          ssize_t stride_ay, const float *ay,
                          float *left, float *top, float *right, float *bottom)
{
  float lt = HUGE_VALF;
  float up = -HUGE_VALF;
  float rt = -HUGE_VALF;
  float down = HUGE_VALF;

  for (size_t i = 0; i <= degree; i++)
    {
      if (*ax < lt)
        lt = *ax;
      if (rt < *ax)
        rt = *ax;
      ax += stride_ax;

      if (*ay < down)
        down = *ay;
      if (up < *ay)
        up = *ay;
      ay += stride_ay;
    }

  *left = lt;
  *top = up;
  *right = rt;
  *bottom = down;
}

static bool
_float_boxes_overlap (float left1, float top1, float right1, float bottom1,
                      float left2, float top2, float right2, float bottom2)
{
  return !(right1 < left2       // Rectangle 1 is left of 2.
           || right2 < left1    // Rectangle 2 is left of 1.
           || top1 < bottom2    // Rectangle 1 is below 2.
           || top2 < bottom1);  // Rectangle 2 is below 1.
}

static bool
_float_might_intersect_bern (size_t degree_a,
                             ssize_t stride_ax, const float *ax,
                             ssize_t stride_ay, const float *ay,
                             size_t degree_b,
                             ssize_t stride_bx, const float *bx,
                             ssize_t stride_by, const float *by)
{
  //
  // FIXME: Consider adding a check for overlap of the convex hulls,
  // if the boxes overlap.
  //
  float left1, top1, right1, bottom1;
  float left2, top2, right2, bottom2;
  _float_bounding_box_bern (degree_a, stride_ax, ax, stride_ay, ay,
                            &left1, &top1, &right1, &bottom1);
  _float_bounding_box_bern (degree_b, stride_bx, bx, stride_by, by,
                            &left2, &top2, &right2, &bottom2);
  return _float_boxes_overlap (left1, top1, right1, bottom1, left2, top2,
                               right2, bottom2);
}

VISIBLE size_t
float_plane_intersection_list_length (float_plane_intersection_t *lst)
{
  size_t i = 0;
  while (lst != NULL)
    {
      i++;
      lst = lst->next;
    }
  return i;
}

VISIBLE float_plane_intersection_t *
reverse_float_plane_intersection_list_in_place (float_plane_intersection_t *lst)
{
  float_plane_intersection_t *t = NULL;
  while (lst != NULL)
    {
      float_plane_intersection_t *u = lst->next;
      lst->next = t;
      t = lst;
      lst = u;
    }
  return t;
}

static inline float
_round_to_float (double x)
{
  // Use round(3) to avoid dependence on the IEEE rounding mode. The
  // behavior of rint(3) and similar routines _does_ depend on the
  // rounding mode. The default (round to nearest) is what we want,
  // but let us not depend on that setting.
  //
  // The constant r should equal 536870912.0 for IEEE floating point.
  const double r = ((double) FLT_EPSILON / DBL_EPSILON);
  return round (r * x) / r;
}

static float_plane_intersection_t *
_find_intersections (double (*eval) (size_t, ssize_t,
                                     const double *, double),
                     void (*findroots) (size_t, ssize_t,
                                        const double *,
                                        double, double,
                                        bool *, size_t *,
                                        double *),
                     size_t degree_a,
                     ssize_t stride_ax, const double *ax,
                     ssize_t stride_ay, const double *ay,
                     size_t degree_b,
                     ssize_t stride_bx, const double *bx,
                     ssize_t stride_by, const double *by, double tolerance)
{
  float_plane_intersection_t *intersections = NULL;

  double x0, x1, y0, y1, t0, t1;
  double equation[degree_b + 1];
  size_t num_solutions;
  double solutions[degree_b];

  const double fuzz = 0.499 * FLT_EPSILON;

  flattening_stream_t p =
    flattening_stream_make_spower (degree_a, stride_ax, ax, stride_ay, ay,
                                   tolerance, HUGE_VAL);
  flattening_stream_point (p, &x0, &y0, &t0);

  while (!flattening_stream_done (p))
    {
      p = flattening_stream_next (p);
      flattening_stream_point (p, &x1, &y1, &t1);

      const double xdiff = x1 - x0;
      const double xlen = fabs (xdiff);
      const double ydiff = y1 - y0;
      const double ylen = fabs (ydiff);
      const double constant_term = x1 * y0 - x0 * y1;

      // In the Bernstein basis, a constant c is represented by
      //
      //     (c c c ... c c)
      //
      for (ssize_t i = 0; i <= degree_b; i++)
        equation[i] = (constant_term + ydiff * bx[i * stride_bx]
                       - xdiff * by[i * stride_by]);

      findroots (degree_b, 1, equation, -fuzz, 1 + fuzz,
                 NULL, &num_solutions, solutions);

      for (size_t i = 0; i < num_solutions; i++)
        {
          const double t = solutions[i];
          const double xi = eval (degree_b, stride_bx, bx, t);
          const double yi = eval (degree_b, stride_by, by, t);
          const double s =
            (xlen < ylen) ? ((xi - x0) / xdiff) : ((yi - y0) / ydiff);

          if (-fuzz <= s && s <= 1 + fuzz)
            {
              const float t2_f = _round_to_float (t);
              if (intersections == NULL || t2_f != intersections->t2)
                {
                  const float t1_f = _round_to_float ((1 - s) * t0 + s * t1);
                  const float xi_f = _round_to_float (xi);
                  const float yi_f = _round_to_float (yi);

                  float_plane_intersection_t *xsect =
                    x_gc_malloc (sizeof (float_plane_intersection_t));
                  xsect->t1 = t1_f;
                  xsect->t2 = t2_f;
                  xsect->x = xi_f;
                  xsect->y = yi_f;
                  xsect->next = intersections;
                  intersections = xsect;
                }
            }
        }

      x0 = x1;
      y0 = y1;
      t0 = t1;
    }

  return intersections;
}

static float_plane_intersection_t *
_findintersections_bern (double (*eval) (size_t, ssize_t,
                                         const double *, double),
                         void (*findroots) (size_t, ssize_t,
                                            const double *,
                                            double, double,
                                            bool *, size_t *,
                                            double *),
                         size_t degree_a,
                         ssize_t stride_ax, const float *ax,
                         ssize_t stride_ay, const float *ay,
                         size_t degree_b,
                         ssize_t stride_bx, const float *bx,
                         ssize_t stride_by, const float *by, float tolerance)
{
  if (tolerance < 0)
    tolerance = 1e-5;           // FIXME: Is there a better value?

  float_plane_intersection_t *intersections;
  if (degree_a != 0 && degree_b != 0
      && _float_might_intersect_bern (degree_a, stride_ax, ax, stride_ay, ay,
                                      degree_b, stride_bx, bx, stride_by, by))
    {
      double ax_[degree_a + 1];
      spoly_to_dpoly (degree_a, stride_ax, ax, 1, ax_);
      dpoly_bern_to_spower (degree_a, 1, ax_, 1, ax_);

      double ay_[degree_a + 1];
      spoly_to_dpoly (degree_a, stride_ay, ay, 1, ay_);
      dpoly_bern_to_spower (degree_a, 1, ay_, 1, ay_);

      double bx_[degree_b + 1];
      spoly_to_dpoly (degree_b, stride_bx, bx, 1, bx_);

      double by_[degree_b + 1];
      spoly_to_dpoly (degree_b, stride_by, by, 1, by_);

      intersections =
        _find_intersections (eval, findroots,
                             degree_a, 1, ax_, 1, ay_,
                             degree_b, 1, bx_, 1, by_, tolerance);
    }
  else
    intersections = NULL;

  return intersections;
}

VISIBLE float_plane_intersection_t *
spoly_findintersections_bernsv (size_t degree_a,
                                ssize_t stride_ax, const float *ax,
                                ssize_t stride_ay, const float *ay,
                                size_t degree_b,
                                ssize_t stride_bx, const float *bx,
                                ssize_t stride_by, const float *by,
                                float tolerance)
{
  return _findintersections_bern (dpoly_eval_bernsv, dpoly_findroots_bernsv,
                                  degree_a, stride_ax, ax, stride_ay, ay,
                                  degree_b, stride_bx, bx, stride_by, by,
                                  tolerance);
}

VISIBLE float_plane_intersection_t *
spoly_findintersections_berndc (size_t degree_a,
                                ssize_t stride_ax, const float *ax,
                                ssize_t stride_ay, const float *ay,
                                size_t degree_b,
                                ssize_t stride_bx, const float *bx,
                                ssize_t stride_by, const float *by,
                                float tolerance)
{
  return _findintersections_bern (dpoly_eval_berndc, dpoly_findroots_berndc,
                                  degree_a, stride_ax, ax, stride_ay, ay,
                                  degree_b, stride_bx, bx, stride_by, by,
                                  tolerance);
}

#endif /* Commented out in favor of ‘pure’ implicitization, but left
          here temporarily for reference. */

//----------------------------------------------------------------------
