#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>

// Treating two parametric polynomial splines as vector-valued
// functions in the plane, compute their cross product. The result
// will be a polynomial in t, of degree equal to the sum of the
// degrees of the two splines.

static void
_dpoly_crossproduct (size_t degree_a, ssize_t stride_ax, const double *ax,
                     ssize_t stride_ay, const double *ay,
                     size_t degree_b, ssize_t stride_bx, const double *bx,
                     ssize_t stride_by, const double *by,
                     ssize_t stride_cross, double *cross,
                     void (*mul) (size_t, ssize_t, const double *,
                                  size_t, ssize_t, const double *,
                                  ssize_t, double *))
{
  double term1[degree_a + degree_b + 1];
  mul (degree_a, stride_ax, ax, degree_b, stride_by, by, 1, term1);

  double term2[degree_a + degree_b + 1];
  mul (degree_a, stride_ay, ay, degree_b, stride_bx, bx, 1, term2);

  dpoly_sub (degree_a + degree_b, 1, term1, 1, term2, stride_cross, cross);
}

VISIBLE void
dpoly_crossproduct_mono (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay,
                         size_t degree_b, ssize_t stride_bx, const double *bx,
                         ssize_t stride_by, const double *by,
                         ssize_t stride_cross, double *cross)
{
  _dpoly_crossproduct (degree_a, stride_ax, ax, stride_ay, ay,
                       degree_b, stride_bx, bx, stride_by, by,
                       stride_cross, cross, dpoly_mul_mono);
}

VISIBLE void
dpoly_crossproduct_bern (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay,
                         size_t degree_b, ssize_t stride_bx, const double *bx,
                         ssize_t stride_by, const double *by,
                         ssize_t stride_cross, double *cross)
{
  _dpoly_crossproduct (degree_a, stride_ax, ax, stride_ay, ay,
                       degree_b, stride_bx, bx, stride_by, by,
                       stride_cross, cross, dpoly_mul_bern);
}

VISIBLE void
dpoly_crossproduct_sbern (size_t degree_a, ssize_t stride_ax, const double *ax,
                          ssize_t stride_ay, const double *ay,
                          size_t degree_b, ssize_t stride_bx, const double *bx,
                          ssize_t stride_by, const double *by,
                          ssize_t stride_cross, double *cross)
{
  _dpoly_crossproduct (degree_a, stride_ax, ax, stride_ay, ay,
                       degree_b, stride_bx, bx, stride_by, by,
                       stride_cross, cross, dpoly_mul_sbern);
}

VISIBLE void
dpoly_crossproduct_spower (size_t degree_a, ssize_t stride_ax, const double *ax,
                           ssize_t stride_ay, const double *ay,
                           size_t degree_b, ssize_t stride_bx, const double *bx,
                           ssize_t stride_by, const double *by,
                           ssize_t stride_cross, double *cross)
{
  _dpoly_crossproduct (degree_a, stride_ax, ax, stride_ay, ay,
                       degree_b, stride_bx, bx, stride_by, by,
                       stride_cross, cross, dpoly_mul_spower);
}
