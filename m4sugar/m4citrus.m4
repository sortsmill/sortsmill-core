divert(-1)#                                                  -*- Autoconf -*-

# A wrapper around m4sugar to make it friendlier with C and to support
# common Sorts Mill tasks.

# Copyright (C) 2013 Khaled Hosny and Barry Schwartz
#
# This file is part of the Sorts Mill Core Library.
#
# Sorts Mill Core Library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Sorts Mill Core Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

include(`m4sugar.m4')

# Change the comment delimiters to /* */
m4_changecom([/*],[*/])

m4_define([u8_s8_to_c32_c64],
          [u8,s8,u16,s16,u32,s32,u64,s64,f32,f64,c32,c64])

m4_define([u8_s8_etc_multiplicity],
   [m4_bmatch([$1],
              [c32\|c64],[2],
              [1])])

m4_define([m4_s8_to_int8_t_etc],
   [m4_bmatch([$1],
       [scm],[SCM],
       [u8],[uint8_t],
       [s8],[int8_t],
       [u16],[uint16_t],
       [s16],[int16_t],
       [u32],[uint32_t],
       [s32],[int32_t],
       [u64],[uint64_t],
       [s64],[int64_t],
       [f32],[float],
       [f64],[double],
       [c32],[float],
       [c64],[double],
       [***ERROR***])])

m4_divert[]dnl
