#!/bin/sh
#
# Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of the Sorts Mill Core Library.
# 
# Sorts Mill Core Library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill Core Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

list_ats_includes () {
    # Do not copy pats_ccomp_memalloc_user.h, because we provide our
    # own.
    (
        cd "${1:-${PATSHOME}}" &&
            for d in ccomp contrib libats prelude share
            do
                find "${d}" -name '*.mk' &&
                    find "${d}" -name '*.h' &&
                    find "${d}" -name '*.c' &&
                    find "${d}" -name '*.cats'
            done
    ) | sed -e '/^.*\/pats_ccomp_memalloc_user\.h$/d'
}

dest="${1}"
test -z "${dest}" && dest='$(top_srcdir)/ats-include'

patshome="${2}"
test -z "${patshome}" && patshome='$(PATSHOME)'

for f in `list_ats_includes`
do
    echo "EXTRA_DIST += ${dest}/${f}"
    echo "BUILT_SOURCES += ${dest}/${f}"
    echo "if WITH_ATS"
    echo "${dest}/${f}: ${patshome}/${f}"
    echo '	$(MKDIR_P) '`dirname ${dest}/${f}`
    echo "	rm -f ${dest}/${f}"
    echo "	cp ${patshome}/${f} ${dest}/${f}"
    echo "endif WITH_ATS"
    echo
done

exit 0
