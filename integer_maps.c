#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <string.h>
#include <sortsmill/core.h>

//
// smcore_uintptr_to_uintptr: A module for maps from uintptr_t to
// uintptr_t.
//
DEFINE_INTEGER_MAP_MODULE (VISIBLE const smcore_uintptr_to_uintptr_module_t
                           smcore_uintptr_to_uintptr, uintptr_t, uintptr_t,
                           smcore_uintptr_to_uintptr_t,
                           smcore_uintptr_to_uintptr_cursor_t,
                           /* no cast */, /* no cast */,
                           /* no cast */, /* no cast */);

//
// smcore_intptr_to_intptr: A module for maps from intptr_t to
// intptr_t.
//
DEFINE_INTEGER_MAP_MODULE (VISIBLE const smcore_intptr_to_intptr_module_t
                           smcore_intptr_to_intptr, intptr_t, intptr_t,
                           smcore_intptr_to_intptr_t,
                           smcore_intptr_to_intptr_cursor_t,
                           (uintptr_t), (intptr_t), (uintptr_t), (intptr_t));

//
// smcore_uintptr_to_voidp: A module for maps from uintptr_t to void
// pointer.
//
DEFINE_INTEGER_MAP_MODULE (VISIBLE const smcore_uintptr_to_voidp_module_t
                           smcore_uintptr_to_voidp, uintptr_t, void *,
                           smcore_uintptr_to_voidp_t,
                           smcore_uintptr_to_voidp_cursor_t,
                           /* no cast */, /* no cast */, (uintptr_t), (void *));
