#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <string.h>
#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

VISIBLE void
clear_polynomial_root_interval_list (polynomial_root_interval_t *lst)
{
  while (lst != NULL)
    {
      polynomial_root_interval_t *p = lst;
      lst = p->next;
      mpq_clear (p->a);
      if (!p->root_is_exact)
        mpq_clear (p->b);
      free (p);
    }
}

VISIBLE size_t
polynomial_root_interval_list_length (polynomial_root_interval_t *lst)
{
  size_t i = 0;
  while (lst != NULL)
    {
      i++;
      lst = lst->next;
    }
  return i;
}

//----------------------------------------------------------------------

// sign_variations_mpq()
//
// Count sign variations in the coefficients of the given polynomial.
static size_t
sign_variations_mpq (size_t degree, ssize_t stride, mpq_t *poly)
{
  assert (degree <= SSIZE_MAX);

  size_t count = 0;
  int old_sign = mpq_sgn (poly[0]);
  for (ssize_t i = 1; i <= degree; i++)
    {
      const int new_sign = mpq_sgn (poly[stride * i]);
      if (old_sign * new_sign < 0)
        {
          old_sign = new_sign;
          count++;
        }
      else if (old_sign == 0)
        old_sign = new_sign;
    }
  return count;
}

//----------------------------------------------------------------------

// mpq_budan_0_1()
//
// Budan’s 0-1 roots test. See
// http://en.wikipedia.org/w/index.php?title=Budan%27s_theorem&oldid=649494866#Early_applications_of_Budan.27s_theorem
VISIBLE size_t
mpq_budan_0_1 (size_t degree, ssize_t stride, mpq_t *poly)
{
  assert (degree <= SSIZE_MAX);

  // Use the binomial expansion
  //
  //    (1 + x)ⁿ = ∑ᵢC(n,i)xⁱ for i = 0,1,...,n

  size_t count = 0;

  if (degree != 0)
    {
      mpq_t tmp;
      mpq_init (tmp);

      mpq_t C;
      mpq_init (C);

      mpq_t *p = xmalloc ((degree + 1) * sizeof (mpq_t));
      mpq_init_poly (degree, p);

      mpq_set (p[0], poly[0]);
      for (size_t i = 1; i < degree; i++)
        {
          mpq_bincoef_ui (C, degree, i);
          mpq_mul (p[i], C, poly[0]);
        }
      mpq_set (p[degree], poly[0]);

      for (size_t k = 1; k < degree; k++)
        {
          mpq_add (p[0], p[0], poly[(ssize_t) k * stride]);
          for (size_t i = 1; i < degree - k; i++)
            {
              mpq_bincoef_ui (C, degree - k, i);
              mpq_mul (tmp, C, poly[(ssize_t) k * stride]);
              mpq_add (p[i], p[i], tmp);
            }
          mpq_add (p[degree - k], p[degree - k], poly[(ssize_t) k * stride]);
        }

      mpq_add (p[0], p[0], poly[(ssize_t) degree * stride]);

      count = sign_variations_mpq (degree, 1, p);

      mpq_clear_poly (degree, p);
      free (p);
      mpq_clear (C);
      mpq_clear (tmp);
    }

  return count;
}

//----------------------------------------------------------------------
//
// The Vincent-Collins-Akritas method for isolating roots of a
// square-free polynomial (with coefficients given in the ordinary
// monomial basis) in the open interval (0,1). See
// http://en.wikipedia.org/w/index.php?title=Vincent%27s_theorem&oldid=647226920

static void
p_0_mid_to_p_mid_1 (size_t degree, mpq_t *p_0_mid, mpq_t *p_mid_1)
{
  // Use the binomial expansion
  //
  //    (1 + x)ⁿ = ∑ᵢC(n,i)xⁱ for i = 0,1,...,n

  mpq_t tmp;
  mpq_init (tmp);

  mpq_t C;
  mpq_init (C);

  mpq_set (p_mid_1[0], p_0_mid[degree]);
  for (size_t i = 1; i < degree; i++)
    {
      mpq_bincoef_ui (C, degree, i);
      mpq_mul (p_mid_1[i], C, p_0_mid[degree]);
    }
  mpq_set (p_mid_1[degree], p_0_mid[degree]);

  for (size_t k = 1; k < degree; k++)
    {
      mpq_add (p_mid_1[0], p_mid_1[0], p_0_mid[degree - k]);
      for (size_t i = 1; i < degree - k; i++)
        {
          mpq_bincoef_ui (C, degree - k, i);
          mpq_mul (tmp, C, p_0_mid[degree - k]);
          mpq_add (p_mid_1[i], p_mid_1[i], tmp);
        }
      mpq_add (p_mid_1[degree - k], p_mid_1[degree - k], p_0_mid[degree - k]);
    }

  mpq_add (p_mid_1[0], p_mid_1[0], p_0_mid[0]);

  mpq_clear (tmp);
  mpq_clear (C);
}

static polynomial_root_interval_t *
mpq_vca_recursion (size_t degree, ssize_t stride, mpq_t *p,
                   const mpq_t a, const mpq_t b,
                   polynomial_root_interval_t *tail)
{
  polynomial_root_interval_t *intervals;

  switch (mpq_budan_0_1 (degree, stride, p))
    {
    case 0:
      // Add no intervals.
      intervals = tail;
      break;

    case 1:
      // Include the interval (a,b).
      intervals = xmalloc (sizeof (polynomial_root_interval_t));
      intervals->next = tail;
      intervals->root_is_exact = false;
      mpq_init (intervals->a);
      mpq_set (intervals->a, a);
      mpq_init (intervals->b);
      mpq_set (intervals->b, b);
      break;

    default:
      {
        mpq_t one_half;
        mpq_init (one_half);
        mpq_set_ui (one_half, 1, 2);

        mpq_t *p_0_mid = xmalloc ((degree + 1) * sizeof (mpq_t));
        mpq_init_poly (degree, p_0_mid);
        mpq_t *p_mid_1 = xmalloc ((degree + 1) * sizeof (mpq_t));
        mpq_init_poly (degree, p_mid_1);
        mpq_t pow2, mid, p_mid;
        mpq_inits (pow2, mid, p_mid, NULL);

        mpq_set_ui (pow2, 1, 1);        // pow2 ← 1
        mpq_set (p_0_mid[degree], p[stride * (ssize_t) degree]);
        for (size_t i = 1; i <= degree; i++)
          {
            mpq_add (pow2, pow2, pow2); // pow2 ← 2 * pow2
            mpq_mul (p_0_mid[degree - i], pow2,
                     p[stride * (ssize_t) (degree - i)]);
          }

        p_0_mid_to_p_mid_1 (degree, p_0_mid, p_mid_1);

        mpq_add (mid, a, b);
        mpq_mul (mid, mid, one_half);   // mid ← (a + b) / 2

        // The right side.
        intervals = mpq_vca_recursion (degree, 1, p_mid_1, mid, b, tail);

        // p_mid ← value of the polynomial p at 1/2.
        mpq_poly_eval_mono (degree, stride, p, one_half, p_mid);
        if (mpq_sgn (p_mid) == 0)
          {
            // The midpoint p_mid is a root. Include it.
            polynomial_root_interval_t *root =
              xmalloc (sizeof (polynomial_root_interval_t));
            root->next = intervals;
            root->root_is_exact = true;
            mpq_init (root->a);
            mpq_set (root->a, mid);
            intervals = root;
          }

        // The left side.
        intervals = mpq_vca_recursion (degree, 1, p_0_mid, a, mid, intervals);

        mpq_clears (pow2, mid, p_mid, NULL);
        mpq_clear_poly (degree, p_mid_1);
        free (p_mid_1);
        mpq_clear_poly (degree, p_0_mid);
        free (p_0_mid);
        mpq_clear (one_half);
      }
      break;
    }

  return intervals;
}

VISIBLE polynomial_root_interval_t *
isolate_roots_of_square_free_polynomial (size_t degree, ssize_t stride,
                                         mpq_t *poly, const mpq_t a,
                                         const mpq_t b)
{
  mpq_t one;
  mpq_init (one);
  mpq_set_ui (one, 1, 1);

  mpq_t tmp;
  mpq_init (tmp);

  mpq_t *p = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, p);

  if (mpq_sgn (a) == 0 && mpq_equal (b, one))
    copy_mpq_with_strides (1, p, stride, poly, degree + 1);
  else
    mpq_poly_portion_mono (degree, stride, poly, a, b, 1, p);

  polynomial_root_interval_t *intervals = NULL;

  // Is there a root at b?
  mpq_poly_eval_mono (degree, stride, poly, b, tmp);
  if (mpq_sgn (tmp) == 0)
    {
      // Put the root at b last in the list.
      polynomial_root_interval_t *root =
        xmalloc (sizeof (polynomial_root_interval_t));
      root->next = intervals;
      root->root_is_exact = true;
      mpq_init (root->a);
      mpq_set (root->a, b);
      intervals = root;
    }

  // Isolate roots in (a,b).
  intervals = mpq_vca_recursion (degree, 1, p, a, b, intervals);

  // Is there a root at a?
  mpq_poly_eval_mono (degree, stride, poly, a, tmp);
  if (mpq_sgn (tmp) == 0)
    {
      // Put the root at a first in the list.
      polynomial_root_interval_t *root =
        xmalloc (sizeof (polynomial_root_interval_t));
      root->next = intervals;
      root->root_is_exact = true;
      mpq_init (root->a);
      mpq_set (root->a, a);
      intervals = root;
    }

  mpq_clear_poly (degree, p);
  free (p);
  mpq_clear (tmp);
  mpq_clear (one);

  return intervals;
}

//----------------------------------------------------------------------

static void
find_interval_root (size_t degree, ssize_t stride, mpq_t *poly,
                    const mpq_t a, const mpq_t b,
                    double (*eval) (double, void *), void *data, double *root)
{
  // This routine assumes a < b.

  mpq_t a1q, b1q, v1, v0;
  mpq_inits (a1q, b1q, v1, v0, NULL);

  int sign1;
  int sign0;

  const double a1 = mpq_get_d_nexttoward (a, b);
  const double b1 = mpq_get_d_nexttoward (b, a);

  mpq_set_d (a1q, a1);
  mpq_set_d (b1q, b1);

  if (0 <= mpq_cmp (a1q, b) || mpq_cmp (b1q, a) <= 0)
    // a and b are really, really close to each other. Choose either a
    // or b as the root. FIXME: Have we a better choice for the root?
    *root = mpq_get_d_roundtoward (b, a);
  else
    {
      mpq_poly_eval_mono (degree, stride, poly, a1q, v1);
      sign1 = mpq_sgn (v1);
      if (sign1 == 0)
        // a1 is an exact root.
        *root = a1;
      else
        {
          mpq_poly_eval_mono (degree, stride, poly, a, v0);
          sign0 = mpq_sgn (v0);
          if (sign0 * sign1 < 0)
            // a and a1 bracket the root. Choose either a or a1 as the
            // root. FIXME: Have we a better choice for the root?
            *root = a1;
          else
            {
              mpq_poly_eval_mono (degree, stride, poly, b1q, v1);
              sign1 = mpq_sgn (v1);
              if (sign1 == 0)
                // b1 is an exact root.
                *root = b1;
              else
                {
                  mpq_poly_eval_mono (degree, stride, poly, b, v0);
                  sign0 = mpq_sgn (v0);
                  if (sign0 * sign1 < 0)
                    // b1 and b bracket the root. Choose either a or
                    // a1 as the root. FIXME: Have we a better choice
                    // for the root?
                    *root = b1;
                  else
                    {
                      int info;
                      dbrentroot (a1, b1, eval, data, -1, -1, root, &info);
                      if (info != 0)
                        // FIXME FIXME FIXME FIXME: The following is
                        // in case I missed any really tricky
                        // cases. where the interval is miniscule.
                        *root = a1;
                    }
                }
            }
        }
    }

  mpq_clears (a1q, b1q, v1, v0, NULL);
}

static void
find_polynomial_root_interval_root (const polynomial_root_interval_t *interval,
                                    size_t degree, ssize_t stride, mpq_t *poly,
                                    double (*eval) (double, void *), void *data,
                                    double *root)
{
  assert (0 < degree);
  if (interval->root_is_exact)
    *root = mpq_get_d (interval->a);
  else if (degree == 1)
    {
      // The monic polynomial is a straight line. Solve for its root
      // immediately. Use exact arithmetic, for consistency with other
      // roots in how truncation is handled.
      mpq_t rt;
      mpq_init (rt);
      mpq_neg (rt, poly[0]);
      *root = mpq_get_d (rt);
      mpq_clear (rt);
    }
  else if (mpq_cmp (interval->a, interval->b) < 0)
    find_interval_root (degree, stride, poly, interval->a, interval->b,
                        eval, data, root);
  else
    find_interval_root (degree, stride, poly, interval->b, interval->a,
                        eval, data, root);
}

static void
find_polynomial_root_interval_list_roots (const polynomial_root_interval_t
                                          *intervals,
                                          size_t degree, ssize_t stride,
                                          mpq_t *poly,
                                          double (*eval) (double, void *),
                                          void *data,
                                          size_t *root_count, double *roots)
{
  size_t i = 0;
  for (const polynomial_root_interval_t *p = intervals; p != NULL; p = p->next)
    {
      find_polynomial_root_interval_root (p, degree, stride, poly, eval, data,
                                          &roots[i]);
      i++;
    }
  *root_count = i;
}

typedef struct
{
  size_t degree;
  ssize_t stride;
  mpq_t *poly;
} dpoly_findroots_default_eval_data_t;

static double
dpoly_findroots_default_eval (double x, void *data)
{
  mpq_t xq;
  mpq_t yq;
  mpq_init (xq);
  mpq_init (yq);

  mpq_set_d (xq, x);

  dpoly_findroots_default_eval_data_t *p = data;
  mpq_poly_eval_mono (p->degree, p->stride, p->poly, xq, yq);
  const double y = mpq_get_d (yq);

  mpq_clear (yq);
  mpq_clear (xq);

  return y;
}

// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
//
// CHANGE THE NAME OF THIS ROUTINE.
//
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
VISIBLE void
dpoly_findroots (size_t degree, ssize_t stride, mpq_t *poly,
                 const mpq_t a, const mpq_t b,
                 double (*eval) (double, void *), void *data,
                 bool *identically_zero,
                 size_t *root_count, double *roots, ssize_t *multiplicities)
{
  bool poly_is_identically_zero;
  dpoly_findroots_default_eval_data_t my_data;

  if (mpq_poly_equalszero (degree, stride, poly))
    {
      poly_is_identically_zero = true;
      *root_count = 0;
    }
  else
    {
      poly_is_identically_zero = false;

      if (eval == NULL)
        {
          eval = dpoly_findroots_default_eval;
          my_data.degree = degree;
          my_data.stride = stride;
          my_data.poly = poly;
          data = &my_data;
        }

      mpq_t lead_coef;
      mpq_init (lead_coef);
      polynomial_list_entry_t *sqfr_decomp;
      mpq_poly_squarefree_mono (degree, stride, poly, &sqfr_decomp, lead_coef);
      mpq_clear (lead_coef);

      *root_count = 0;
      size_t multiplicity = 1;
      for (polynomial_list_entry_t *p = sqfr_decomp; p != NULL; p = p->next)
        {
          if (!mpq_poly_equalsone_mono (p->degree, 1, p->poly))
            {
              polynomial_root_interval_t *intervals =
                isolate_roots_of_square_free_polynomial (p->degree, 1, p->poly,
                                                         a, b);
              size_t rt_count;
              find_polynomial_root_interval_list_roots (intervals, p->degree, 1,
                                                        p->poly, eval, data,
                                                        &rt_count,
                                                        roots + *root_count);
              if (multiplicities != NULL)
                for (size_t i = 0; i < rt_count; i++)
                  (multiplicities + *root_count)[i] = multiplicity;
              *root_count += rt_count;
              clear_polynomial_root_interval_list (intervals);
            }
          multiplicity++;
        }

      clear_polynomial_list (sqfr_decomp);
    }

  if (identically_zero != NULL)
    *identically_zero = poly_is_identically_zero;
}

// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
//
// CHANGE THE NAME OF THIS ROUTINE.
//
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
// FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
VISIBLE void
dpoly_findroots2 (size_t degree, ssize_t stride, const double *poly,
                  double a, double b,
                  double (*eval) (double, void *), void *data,
                  bool *identically_zero, size_t *root_count, double *roots,
                  ssize_t *multiplicities)
{
  mpq_t a_mpq;
  mpq_init (a_mpq);
  mpq_set_d (a_mpq, a);

  mpq_t b_mpq;
  mpq_init (b_mpq);
  mpq_set_d (b_mpq, b);

  mpq_t *poly_mpq = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, poly_mpq);

  for (size_t i = 0; i <= degree; i++)
    mpq_set_d (poly_mpq[i], poly[i]);

  dpoly_findroots (degree, stride, poly_mpq, a_mpq, b_mpq, eval, data,
                   identically_zero, root_count, roots, multiplicities);

  mpq_clear_poly (degree, poly_mpq);
  free (poly_mpq);
  mpq_clear (b_mpq);
  mpq_clear (a_mpq);
}

//----------------------------------------------------------------------
//
// Recursive search for real roots of a polynomial, without providing
// information about its multiplicities.

static bool
_all_zeros (const double *p, size_t n)
{
  while (0 < n && *p == 0)
    {
      p++;
      n--;
    }
  return (n == 0);
}

typedef struct
{
  size_t degree;
  const double *poly;
  double (*eval) (size_t degree, ssize_t stride, const double *poly, double t);
} dpoly_findroots_recursion_eval_data_t;

static double
_dpoly_findroots_recursion_eval (double x, void *data)
{
  dpoly_findroots_recursion_eval_data_t *p = data;
  return p->eval (p->degree, 1, p->poly, x);
}

static void
_dpoly_findroot_degree1 (const double *poly, double a, double b,
                         size_t *root_count, double *roots,
                         double (*eval) (size_t degree, ssize_t stride,
                                         const double *poly, double t))
{
  const double fa = eval (1, 1, poly, a);
  if (fa == 0)
    {
      *root_count = 1;
      roots[0] = a;
    }
  else
    {
      const double fb = eval (1, 1, poly, b);
      if (fb == 0)
        {
          *root_count = 1;
          roots[0] = b;
        }
      else if ((fa / fabs (fa)) * fb < 0)
        {
          *root_count = 1;
          roots[0] = a + ((b - a) * fa) / (fa - fb);
        }
      else
        *root_count = 0;
    }
}

static void
_dpoly_findroots_recursion (size_t degree, const double *poly,
                            double a, double b,
                            size_t *root_count, double *roots,
                            double (*eval) (size_t degree, ssize_t stride,
                                            const double *poly, double t),
                            void (*deriv) (size_t degree, ssize_t stride,
                                           const double *poly,
                                           ssize_t deriv_stride, double *deriv))
{
  switch (degree)
    {
    case 0:
      *root_count = 0;
      break;
    case 1:
      _dpoly_findroot_degree1 (poly, a, b, root_count, roots, eval);
      break;
    default:
      {
        double derivative[degree];
        double deriv_roots[degree + 1];
        size_t deriv_root_count;
        deriv (degree, 1, poly, 1, derivative);
        _dpoly_findroots_recursion (degree - 1, derivative, a, b,
                                    &deriv_root_count,
                                    deriv_roots, eval, deriv);
        if (deriv_root_count == 0 || deriv_roots[0] != a)
          {
            memmove (deriv_roots + 1, deriv_roots,
                     deriv_root_count * sizeof (double));

            // Not really a root of the derivative.
            deriv_roots[0] = a;
            deriv_root_count++;
          }
        if (deriv_root_count == 0 || deriv_roots[deriv_root_count - 1] != b)
          {
            // Not really a root of the derivative.
            deriv_roots[deriv_root_count] = b;
            deriv_root_count++;
          }

        dpoly_findroots_recursion_eval_data_t data = {
          .degree = degree,
          .poly = poly,
          .eval = eval
        };

        *root_count = 0;
        for (size_t i = 0; i < deriv_root_count; i++)
          {
            const double fi = eval (degree, 1, poly, deriv_roots[i]);
            if (fi == 0)
              {
                roots[*root_count] = deriv_roots[i];
                *root_count += 1;
              }
            else if (i < deriv_root_count - 1)
              {
                const double fj = eval (degree, 1, poly, deriv_roots[i + 1]);
                if ((fi / fabs (fi)) * fj < 0)
                  {
                    int info;
                    double isolated_root;
                    dbrentroot (deriv_roots[i], deriv_roots[i + 1],
                                _dpoly_findroots_recursion_eval, &data, -1, -1,
                                &isolated_root, &info);
                    roots[*root_count] = isolated_root;
                    *root_count += 1;
                  }
              }
          }
      }
      break;
    }
}

static void
_dpoly_findroots_recursively (size_t degree, ssize_t stride, const double *poly,
                              double a, double b, bool *identically_zero,
                              size_t *root_count, double *roots,
                              double (*eval) (size_t degree, ssize_t stride,
                                              const double *poly, double t),
                              void (*deriv) (size_t degree, ssize_t stride,
                                             const double *poly,
                                             ssize_t deriv_stride,
                                             double *deriv))
{
  if (stride != 1)
    {
      double buffer[degree + 1];
      copy_f64_with_strides (1, buffer, stride, poly, degree + 1);
      _dpoly_findroots_recursively (degree, 1, buffer, a, b,
                                    identically_zero, root_count, roots,
                                    eval, deriv);
    }
  else if (_all_zeros (poly, degree + 1))
    {
      if (identically_zero != NULL)
        *identically_zero = true;
      *root_count = 0;
    }
  else
    {
      if (identically_zero != NULL)
        *identically_zero = false;
      _dpoly_findroots_recursion (degree, poly, a, b, root_count, roots,
                                  eval, deriv);
    }
}

VISIBLE void
dpoly_findroots_bernsv (size_t degree, ssize_t stride, const double *poly,
                        double a, double b, bool *identically_zero,
                        size_t *root_count, double *roots)
{
  _dpoly_findroots_recursively (degree, stride, poly, a, b,
                                identically_zero, root_count, roots,
                                dpoly_eval_bernsv, dpoly_deriv_bern);
}

VISIBLE void
dpoly_findroots_berndc (size_t degree, ssize_t stride, const double *poly,
                        double a, double b, bool *identically_zero,
                        size_t *root_count, double *roots)
{
  _dpoly_findroots_recursively (degree, stride, poly, a, b,
                                identically_zero, root_count, roots,
                                dpoly_eval_berndc, dpoly_deriv_bern);
}

VISIBLE void
dpoly_findroots_sbernsv (size_t degree, ssize_t stride, const double *poly,
                         double a, double b, bool *identically_zero,
                         size_t *root_count, double *roots)
{
  _dpoly_findroots_recursively (degree, stride, poly, a, b,
                                identically_zero, root_count, roots,
                                dpoly_eval_sbernsv, dpoly_deriv_sbern);
}

VISIBLE void
dpoly_findroots_sberndc (size_t degree, ssize_t stride, const double *poly,
                         double a, double b, bool *identically_zero,
                         size_t *root_count, double *roots)
{
  _dpoly_findroots_recursively (degree, stride, poly, a, b,
                                identically_zero, root_count, roots,
                                dpoly_eval_sberndc, dpoly_deriv_sbern);
}

VISIBLE void
dpoly_findroots_spower (size_t degree, ssize_t stride, const double *poly,
                        double a, double b, bool *identically_zero,
                        size_t *root_count, double *roots)
{
  _dpoly_findroots_recursively (degree, stride, poly, a, b,
                                identically_zero, root_count, roots,
                                dpoly_eval_spower, dpoly_deriv_spower);
}

//----------------------------------------------------------------------
