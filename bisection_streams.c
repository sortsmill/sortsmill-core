#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>

// Routines that are defined as inline.
VISIBLE bisection_stream_t bisection_stream_make (double a, double b);
VISIBLE bisection_stream_t bisection_stream_cut (bisection_stream_t p,
                                                 double c);
VISIBLE bisection_stream_t bisection_stream_bisect (bisection_stream_t p);
VISIBLE bisection_stream_t bisection_stream_next (bisection_stream_t p);

VISIBLE bisection_stream_t
bisection_stream_cuts (bisection_stream_t p, size_t n, double c[n])
{
  if (n != 0)
    {
      // Push the rightmost cut for handling later.
      struct _bisection_stream__ *q =
        x_gc_malloc (sizeof (struct _bisection_stream__));
      q->a = c[n - 1];
      q->b = p->b;
      q->_next__ = p->_next__;
      p->_next__ = q;

      // Push interior cuts in order from right to left.
      while (2 <= n)
        {
          q = x_gc_malloc (sizeof (struct _bisection_stream__));
          q->a = c[n - 2];
          q->b = c[n - 1];
          q->_next__ = p->_next__;
          p->_next__ = q;
          n--;
        }

      // The leftmost cut becomes the new working interval.
      p->b = c[0];
    }

  return p;
}
