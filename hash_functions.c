#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <limits.h>
#include <string.h>
#include <unistr.h>

#include <sortsmill/core/murmurhash3.h>
#include <sortsmill/core/hash_functions.h>

// A more-or-less arbitrary seed value.
#define MURMUR3_SEED__ (SMCORE_HASH_SEED)

// I imagine we do not want to use the following macro with
// floating-point values. However, it should be fine for
// two’s-complement integers.
#define DEFINE_SIMPLE_TYPE_HASH__(NAME, T)                      \
  VISIBLE uint32_t                                              \
  NAME (T obj)                                                  \
  {                                                             \
    uint32_t hash_value;                                        \
    smcore_murmurhash3_32 (&obj, (int) sizeof (T),              \
                           MURMUR3_SEED__, &hash_value);        \
    return hash_value;                                          \
  }

// A lot of these are duplicates, but I do not feel like sorting that
// out, especially in an architecture-independent way.
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_bool, bool);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_char, char);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_schar, signed char);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uchar, unsigned char);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_short, short int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_ushort, unsigned short int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_int, int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uint, unsigned int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_long, long int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_ulong, unsigned long int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_longlong, long long int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_ulonglong, unsigned long long int);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_int8, int8_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uint8, uint8_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_int16, int16_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uint16, uint16_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_int32, int32_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uint32, uint32_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_int64, int64_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uint64, uint64_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_ssize_t, ssize_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_size_t, size_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_intptr, intptr_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uintptr, uintptr_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_intmax, intmax_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_uintmax, uintmax_t);
DEFINE_SIMPLE_TYPE_HASH__ (smcore_uint32_pointer, void *);

VISIBLE uint32_t
smcore_uint32_strhash (const char *s)
{
  uint32_t hash_value;
  const size_t n = strlen (s);
  assert (n <= INT_MAX);
  smcore_murmurhash3_32 (s, (int) n, MURMUR3_SEED__, &hash_value);
  return hash_value;
}

VISIBLE uint32_t
smcore_uint32_u8_strhash (const uint8_t *s)
{
  uint32_t hash_value;
  const size_t n = u8_strlen (s);
  assert (n <= INT_MAX);
  smcore_murmurhash3_32 (s, (int) n, MURMUR3_SEED__, &hash_value);
  return hash_value;
}

VISIBLE uint32_t
smcore_uint32_u16_strhash (const uint16_t *s)
{
  uint32_t hash_value;
  const size_t n = 2 * u16_strlen (s);
  assert (n <= INT_MAX);
  smcore_murmurhash3_32 (s, (int) n, MURMUR3_SEED__, &hash_value);
  return hash_value;
}

VISIBLE uint32_t
smcore_uint32_u32_strhash (const uint32_t *s)
{
  uint32_t hash_value;
  const size_t n = 4 * u32_strlen (s);
  assert (n <= INT_MAX);
  smcore_murmurhash3_32 (s, (int) n, MURMUR3_SEED__, &hash_value);
  return hash_value;
}

