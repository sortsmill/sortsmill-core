#include <config.h>

// Copyright (C) 2012 by Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core/xdie_on_null.h>
#include <xalloc.h>

// This should result in a non-inline version being generated.
VISIBLE void *xdie_on_null (void *p);
VISIBLE void *xdie_on_enomem (void *p);

// Keep xalloc_die out of the header file.
VISIBLE void
_sortsmill_xalloc_die (void)
{
  xalloc_die ();
}
