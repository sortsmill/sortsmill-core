#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>

VISIBLE void
dpoly_to_spoly (size_t degree, ssize_t stride1, const double *poly1,
                ssize_t stride2, float *poly2)
{
  float buffer[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    {
      buffer[i] = *poly1;
      poly1 += stride1;
    }
  for (size_t i = 0; i <= degree; i++)
    {
      *poly2 = buffer[i];
      poly2 += stride2;
    }
}

VISIBLE void
spoly_to_dpoly (size_t degree, ssize_t stride1, const float *poly1,
                ssize_t stride2, double *poly2)
{
  float buffer[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    {
      buffer[i] = *poly1;
      poly1 += stride1;
    }
  for (size_t i = 0; i <= degree; i++)
    {
      *poly2 = buffer[i];
      poly2 += stride2;
    }
}
