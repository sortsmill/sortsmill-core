#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

//----------------------------------------------------------------------
//
// Multiplication of a polynomial by a scalar.
//
// The same scalar multiplication function works for all polynomial
// bases.

VISIBLE void
mpq_poly_scalarmul (size_t degree, ssize_t stride, mpq_t *poly,
                    const mpq_t r, ssize_t result_stride, mpq_t *result)
{
  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_mul (buffer[i], *poly, r);
      poly += stride;
    }

  copy_mpq_with_strides (result_stride, result, 1, buffer, degree + 1);

  mpq_clear_poly (degree, buffer);
  free (buffer);
}

VISIBLE void
dpoly_scalarmul (size_t degree, ssize_t stride, const double *poly,
                 double r, ssize_t result_stride, double *result)
{
  double *buffer = xmalloc ((degree + 1) * sizeof (double));
  for (size_t i = 0; i <= degree; i++)
    {
      buffer[i] = *poly * r;
      poly += stride;
    }
  copy_f64_with_strides (result_stride, result, 1, buffer, degree + 1);
  free (buffer);
}

//----------------------------------------------------------------------

// Multiply two polynomials given in the ordinary monomial basis,
// obtaining a third polynomial of equal or higher degree.
VISIBLE void
mpq_poly_mul_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                   size_t degree2, ssize_t stride2, mpq_t *poly2,
                   ssize_t result_stride, mpq_t *result)
{
  mpq_convolve (degree1 + 1, stride1, poly1, degree2 + 1, stride2, poly2,
                result_stride, result);
}

VISIBLE void
dpoly_mul_mono (size_t degree1, ssize_t stride1, const double *poly1,
                size_t degree2, ssize_t stride2, const double *poly2,
                ssize_t result_stride, double *result)
{
  f64convolve (degree1 + 1, stride1, poly1, degree2 + 1, stride2, poly2,
               result_stride, result);
}

VISIBLE void
dpoly_mul_bern (size_t degree1, ssize_t stride1, const double *poly1,
                size_t degree2, ssize_t stride2, const double *poly2,
                ssize_t result_stride, double *result)
{
  const size_t degree = degree1 + degree2;
  double buffer[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    buffer[i] = 0.0;
  for (size_t j = 0; j <= degree2; j++)
    for (size_t i = 0; i <= degree1; i++)
      buffer[j + i] +=
        (bincoef (degree2, j) * poly2[stride2 * (ssize_t) j]) *
        (bincoef (degree1, i) * poly1[stride1 * (ssize_t) i]);
  for (size_t i = 0; i <= degree; i++)
    buffer[i] /= bincoef (degree, i);
  copy_f64_with_strides (result_stride, result, 1, buffer, degree + 1);
}

VISIBLE void
dpoly_mul_sbern (size_t degree1, ssize_t stride1, const double *poly1,
                 size_t degree2, ssize_t stride2, const double *poly2,
                 ssize_t result_stride, double *result)
{
  f64convolve (degree1 + 1, stride1, poly1, degree2 + 1, stride2, poly2,
               result_stride, result);
}

VISIBLE void
dpoly_mul_spower (size_t degree1, ssize_t stride1, const double *poly1,
                  size_t degree2, ssize_t stride2, const double *poly2,
                  ssize_t result_stride, double *result)
{
  // See J. Sánchez-Reyes, ‘Applications of the polynomial s-power
  // basis in geometry processing’, ACM Transactions on Graphics, vol
  // 19 no 1, January 2000, page 35. We compute the symmetric halves
  // of the product by the formula
  //
  //    c⁰ = a⁰∗b⁰ − shift₁(Δa∗Δb)
  //    c¹ = a¹∗b¹ − shift₁(Δa∗Δb)
  //
  // where ∗ is the convolution operator, shift₁ is a shift upwards in
  // degree by one (setting the least-degree term to zero), and
  //
  //    Δa = a¹ − a⁰
  //    Δb = b¹ − b⁰
  //

  // q1 = degree of a symmetric half of poly1.
  // q2 = degree of a symmetric half of poly2.
  const size_t q1 = degree1 / 2;
  const size_t q2 = degree2 / 2;

  // Compute Δa∗Δb.
  double DaDb[q1 + q2 + 1];
  {
    double delta1[q1 + 1];
    double delta2[q2 + 1];
    dpoly_sub (q1, -stride1, &poly1[stride1 * (ssize_t) degree1],
               stride1, poly1, 1, delta1);
    dpoly_sub (q2, -stride2, &poly2[stride2 * (ssize_t) degree2],
               stride2, poly2, 1, delta2);
    f64convolve (q1 + 1, 1, delta1, q2 + 1, 1, delta2, 1, DaDb);
  }

  // Compute a⁰∗b⁰ − shift₁(Δa∗Δb).
  double c0[q1 + q2 + 2];
  f64convolve (q1 + 1, stride1, poly1, q2 + 1, stride2, poly2, 1, c0);
  for (size_t i = 1; i <= q1 + q2; i++)
    c0[i] -= DaDb[i - 1];
  c0[q1 + q2 + 1] = -DaDb[q1 + q2];

  // Compute a¹∗b¹ − shift₁(Δa∗Δb).
  double c1[q1 + q2 + 2];
  f64convolve (q1 + 1, -stride1, &poly1[stride1 * (ssize_t) degree1],
               q2 + 1, -stride2, &poly2[stride2 * (ssize_t) degree2], 1, c1);
  for (size_t i = 1; i <= q1 + q2; i++)
    c1[i] -= DaDb[i - 1];
  c1[q1 + q2 + 1] = -DaDb[q1 + q2];

  // Now put it all together.
  dpoly_unsplit_spower (degree1 + degree2, 1, c0, 1, c1, result_stride, result);
}

//----------------------------------------------------------------------

VISIBLE bool
mpq_poly_equalsone_mono (size_t degree1, ssize_t stride1, mpq_t *poly1)
{
  mpq_t one;
  mpq_init (one);
  mpq_set_ui (one, 1, 1);

  bool is_one = mpq_equal (poly1[0], one);
  if (is_one && 0 < degree1)
    is_one = mpq_poly_equalszero (degree1 - 1, stride1, poly1 + stride1);

  mpq_clear (one);

  return is_one;
}

VISIBLE bool
dpoly_equalsone_mono (size_t degree1, ssize_t stride1, const double *poly1)
{
  bool is_one;
  if (poly1[0] == 1)
    {
      ssize_t i = 1;
      while (i <= degree1 && poly1[i * stride1] == 0)
        i++;
      is_one = (degree1 < i);
    }
  else
    is_one = false;
  return is_one;
}

VISIBLE bool
dpoly_equalsone_bern (size_t degree1, ssize_t stride1, const double *poly1)
{
  ssize_t i = 0;
  while (i <= degree1 && poly1[i * stride1] == 1)
    i++;
  return (degree1 < i);
}

VISIBLE bool
dpoly_equalsone_sbern (size_t degree1, ssize_t stride1, const double *poly1)
{
  ssize_t i = 0;
  while (i <= degree1 && poly1[i * stride1] == bincoef (degree1, i))
    i++;
  return (degree1 < i);
}

VISIBLE bool
dpoly_equalsone_spower (size_t degree1, ssize_t stride1, const double *poly1)
{
  bool is_one;
  if (poly1[0] == 1 && poly1[(ssize_t) degree1 * stride1] == 1)
    {
      ssize_t i = 1;
      while (i < degree1 && poly1[i * stride1] == 0)
        i++;
      is_one = (degree1 <= i);
    }
  else
    is_one = false;
  return is_one;
}

//----------------------------------------------------------------------
