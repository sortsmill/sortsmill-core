#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

//----------------------------------------------------------------------
//
// ‘Portion’ routines.
//
// Given a polynomial on [a,b].  Via change of variables, map that
// portion of the polynomial onto [0,1]. This operation is equivalent
// to subdivision twice by de Casteljau’s algorithm. (Presentations of
// subdivision do not always make clear that it is simply a change of
// variables.)

VISIBLE void
mpq_poly_portion_mono (size_t degree, ssize_t stride, mpq_t *poly,
                       const mpq_t a, const mpq_t b,
                       ssize_t result_stride, mpq_t *result)
{
  // Evaluate the polynomial at p(t) = a + (b − a)t.

  mpq_t p[2];
  mpq_init (p[0]);
  mpq_init (p[1]);

  mpq_set (p[0], a);
  mpq_sub (p[1], b, a);
  mpq_poly_compose_mono (degree, stride, poly, 1, 1, p, result_stride, result);

  mpq_clear (p[1]);
  mpq_clear (p[0]);
}

VISIBLE void
dpoly_portion_mono (size_t degree, ssize_t stride, const double *poly,
                    double a, double b, ssize_t result_stride, double *result)
{
  if (a == 0 && b == 1)
    // Fast handling of a special case.
    dpoly_copy (degree, stride, poly, result_stride, result);
  else
    {
      // Evaluate the polynomial at p(t) = a + (b − a)t.
      const double p[2] = { a, b - a };
      dpoly_compose_mono (degree, stride, poly, 1, 1, p, result_stride, result);
    }
}

static void
_dpoly_portion_symmetric (size_t degree, ssize_t stride,
                          const double *poly, double a, double b,
                          ssize_t result_stride, double *result,
                          void (*compose) (size_t, ssize_t, const double *,
                                           size_t, ssize_t, const double *,
                                           ssize_t, double *))
{
  if (a == 0 && b == 1)
    // Fast handling of a special case: copy the coefficients
    // verbatim.
    dpoly_copy (degree, stride, poly, result_stride, result);
  else if (b == 0 && a == 1)
    // Fast handling of a special case: reverse the order of the
    // coefficients. (FIXME: Perhaps this case is not common enough
    // for special handling to tend to speed things up.)
    dpoly_copy (degree, -stride, poly + (ssize_t) degree * stride,
                result_stride, result);
  else
    {
      // Evaluate the polynomial at p(t) = a(1 − t) + bt.
      const double p[2] = { a, b };
      compose (degree, stride, poly, 1, 1, p, result_stride, result);
    }
}

VISIBLE void
dpoly_portion_berndc (size_t degree, ssize_t stride,
                      const double *poly, double a, double b,
                      ssize_t result_stride, double *result)
{
  _dpoly_portion_symmetric (degree, stride, poly, a, b, result_stride, result,
                            dpoly_compose_berndc);
}

VISIBLE void
dpoly_portion_bernh (size_t degree, ssize_t stride, const double *poly,
                     double a, double b, ssize_t result_stride, double *result)
{
  _dpoly_portion_symmetric (degree, stride, poly, a, b, result_stride, result,
                            dpoly_compose_bernh);
}

VISIBLE void
dpoly_portion_sberndc (size_t degree, ssize_t stride, const double *poly,
                       double a, double b, ssize_t result_stride,
                       double *result)
{
  _dpoly_portion_symmetric (degree, stride, poly, a, b, result_stride, result,
                            dpoly_compose_sberndc);
}

VISIBLE void
dpoly_portion_sbernh (size_t degree, ssize_t stride, const double *poly,
                      double a, double b, ssize_t result_stride, double *result)
{
  _dpoly_portion_symmetric (degree, stride, poly, a, b, result_stride, result,
                            dpoly_compose_sbernh);
}

VISIBLE void
dpoly_portion_spower (size_t degree, ssize_t stride, const double *poly,
                      double a, double b, ssize_t result_stride, double *result)
{
  _dpoly_portion_symmetric (degree, stride, poly, a, b, result_stride, result,
                            dpoly_compose_spower);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_subdiv_bern (size_t degree, ssize_t stride, const double *poly, double t,
                   ssize_t stride_a, double *a, ssize_t stride_b, double *b)
{
  // De Casteljau’s algorithm.

  double _a[degree + 1];
  double _b[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    _b[i] = poly[stride * (ssize_t) i];
  for (size_t i = 0; i < degree; i++)
    {
      _a[i] = _b[0];
      for (size_t j = 0; j < degree - i; j++)
        _b[j] += t * (_b[j + 1] - _b[j]);
    }
  _a[degree] = _b[0];
  copy_f64_with_strides (stride_a, a, 1, _a, degree + 1);
  copy_f64_with_strides (stride_b, b, 1, _b, degree + 1);
}

VISIBLE void
dpoly_subdiv_sbern (size_t degree, ssize_t stride, const double *poly, double t,
                    ssize_t stride_a, double *a, ssize_t stride_b, double *b)
{
  // De Casteljau’s algorithm.

  double _a[degree + 1];
  double _b[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    _b[i] = poly[stride * (ssize_t) i] / bincoef (degree, i);
  for (size_t i = 0; i < degree; i++)
    {
      _a[i] = _b[0];
      for (size_t j = 0; j < degree - i; j++)
        _b[j] += t * (_b[j + 1] - _b[j]);
    }
  _a[degree] = _b[0];
  for (size_t i = 0; i <= degree; i++)
    {
      double C = bincoef (degree, i);
      _a[i] *= C;
      _b[i] *= C;
    }
  copy_f64_with_strides (stride_a, a, 1, _a, degree + 1);
  copy_f64_with_strides (stride_b, b, 1, _b, degree + 1);
}

//----------------------------------------------------------------------
