/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 *
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 *
 *
 * An example from the manual page STM_ONE_TIME_INITIALIZE(3).
 *
 * Define a function pi() that returns the value of pi, which is
 * computed at runtime just once, the first time pi() is called.
 */

#include <sortsmill/core.h>
#include <pthread.h>
#include <math.h>
#include <stdio.h>

pthread_mutex_t pi__Mutex = PTHREAD_MUTEX_INITIALIZER;

STM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE, double, pi,
                         pthread_mutex_lock (&pi__Mutex),
                         pthread_mutex_unlock (&pi__Mutex),
                         (pi__Value = 4 * atan (1)));

int
main (int argc, char **argv)
{
  printf ("pi   = %lf\n", pi ());
  printf ("pi/2 = %lf\n", pi () / 2);
  printf ("pi/3 = %lf\n", pi () / 3);
  printf ("pi/4 = %lf\n", pi () / 4);
}
