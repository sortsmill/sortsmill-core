/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 *
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 *
 *
 * An example from the manual page STM_ONE_TIME_INITIALIZE(3).
 *
 * Define a function scm_pi() that returns the Guile value of pi,
 * which is computed at runtime just once, the first time scm_pi() is
 * called. This example is rather contrived.
 */

#include <sortsmill/core.h>
#include <pthread.h>
#include <libguile.h>
#include <stdio.h>

void
pthread_mutex_unlocker (void *mutex_ptr)
{
  pthread_mutex_unlock ((pthread_mutex_t *) mutex_ptr);
}

void
scm_dynwind_pthread_mutex_unlock (void *mutex_ptr)
{
  scm_dynwind_unwind_handler (pthread_mutex_unlocker, mutex_ptr,
                              SCM_F_WIND_EXPLICITLY);
}

pthread_mutex_t scm_pi__Mutex = PTHREAD_MUTEX_INITIALIZER;

STM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE, SCM, scm_pi,
                         scm_dynwind_begin (0);
                         pthread_mutex_lock (&scm_pi__Mutex);
                         scm_dynwind_pthread_mutex_unlock (&scm_pi__Mutex),
                         scm_dynwind_end (),
                         (scm_pi__Value =
                          scm_angle
                          (scm_make_rectangular (scm_from_int (-1),
                                                 scm_from_int (0)))));

void *
run_in_guile_mode (void *p)
{
  printf ("pi   = %lf\n", scm_to_double (scm_pi ()));
  printf ("pi/2 = %lf\n",
          scm_to_double (scm_divide (scm_pi (), scm_from_int (2))));
  printf ("pi/3 = %lf\n",
          scm_to_double (scm_divide (scm_pi (), scm_from_int (3))));
  printf ("pi/4 = %lf\n",
          scm_to_double (scm_divide (scm_pi (), scm_from_int (4))));
}

int
main (int argc, char **argv)
{
  scm_with_guile (run_in_guile_mode, NULL);
}
