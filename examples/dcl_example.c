/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 *
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided the copyright
 * notice and this notice are preserved.  This file is offered as-is,
 * without any warranty.
 *
 * Example of double-checked locking, similar to the example in the
 * manual page stm_dcl_indicator_t(3).
 *
 * get_v() always returns the value of pi.
 */

#include <pthread.h>
#include <sortsmill/core.h>
#include <math.h>
#include <stdio.h>

typedef double some_type_t;

void
initialize_v (some_type_t *vptr)
{
  /*
    4 arctan 1 = pi
  */
  *vptr = 4 * atan (1);
}

volatile stm_dcl_indicator_t v_is_initialized = false;
pthread_mutex_t v_mutex = PTHREAD_MUTEX_INITIALIZER;
some_type_t v;

/* Declare STM_ATTRIBUTE_PURE to tell the optimizing compiler that the
   function always returns the same value. */
STM_ATTRIBUTE_PURE some_type_t
get_v (void)
{
  if (!stm_dcl_load_indicator (&v_is_initialized))
    {
      pthread_mutex_lock (&v_mutex);
      if (!v_is_initialized)
        {
          initialize_v (&v);
          stm_dcl_store_indicator (&v_is_initialized, true);
        }
      pthread_mutex_unlock (&v_mutex);
    }
  return v;
}

int
main (int argc, char **argv)
{
  printf ("pi   = %lf\n", get_v ());
  printf ("pi/2 = %lf\n", get_v () / 2);
  printf ("pi/3 = %lf\n", get_v () / 3);
  printf ("pi/4 = %lf\n", get_v () / 4);
}
