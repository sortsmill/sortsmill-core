#include <config.h>

// Copyright (C) 2012, 2013 by Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core/xgc.h>
#include <unistr.h>

VISIBLE uint8_t *
x_gc_u8_grabstr (uint8_t *s)
{
  uint8_t *copy = x_gc_malloc_atomic ((u8_strlen (s) + 1) * sizeof (uint8_t));
  u8_strcpy (copy, s);
  free (s);
  return copy;
}

VISIBLE uint16_t *
x_gc_u16_grabstr (uint16_t *s)
{
  uint16_t *copy =
    x_gc_malloc_atomic ((u16_strlen (s) + 1) * sizeof (uint16_t));
  u16_strcpy (copy, s);
  free (s);
  return copy;
}

VISIBLE uint32_t *
x_gc_u32_grabstr (uint32_t *s)
{
  uint32_t *copy =
    x_gc_malloc_atomic ((u32_strlen (s) + 1) * sizeof (uint32_t));
  u32_strcpy (copy, s);
  free (s);
  return copy;
}
