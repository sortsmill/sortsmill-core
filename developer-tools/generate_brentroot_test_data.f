c
c     FIXME FIXME FIXME FIXME FIXME FIXME: Add more test
c     cases. Carefully choose them to exercise branches.
c

      program gen
      double precision ax, bx, tol, root
      double precision zeroin, f, g
      external zeroin, f, g
      tol = 0d0
      root = zeroin (0.1d0, 4d0, f, tol)
      print *, '.'
      root = zeroin (0d0, 4d0, g, tol)
      end
      
      double precision function f (x)
      double precision x
      f = sin (x)
      end
      
      double precision function g (x)
      double precision x
      g = cos (x) - (x * x * x)
      end
      
