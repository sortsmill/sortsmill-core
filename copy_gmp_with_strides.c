#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <xalloc.h>
#include <sortsmill/core/copy_gmp_with_strides.h>

VISIBLE void
copy_mpz_with_strides (ssize_t dest_stride, mpz_t *dest,
                       ssize_t src_stride, mpz_t *src, size_t count)
{
  for (size_t i = 0; i < count; i++)
    {
      mpz_set (*dest, *src);
      dest += dest_stride;
      src += src_stride;
    }
}

VISIBLE void
copy_mpq_with_strides (ssize_t dest_stride, mpq_t *dest,
                       ssize_t src_stride, mpq_t *src, size_t count)
{
  for (size_t i = 0; i < count; i++)
    {
      mpq_set (*dest, *src);
      dest += dest_stride;
      src += src_stride;
    }
}

VISIBLE void
copy_mpf_with_strides (ssize_t dest_stride, mpf_t *dest,
                       ssize_t src_stride, mpf_t *src, size_t count)
{
  for (size_t i = 0; i < count; i++)
    {
      mpf_set (*dest, *src);
      dest += dest_stride;
      src += src_stride;
    }
}

VISIBLE void
copy_negate_mpz_with_strides (ssize_t dest_stride, mpz_t *dest,
                              ssize_t src_stride, mpz_t *src, size_t count)
{
  for (size_t i = 0; i < count; i++)
    {
      mpz_neg (*dest, *src);
      dest += dest_stride;
      src += src_stride;
    }
}

VISIBLE void
copy_negate_mpq_with_strides (ssize_t dest_stride, mpq_t *dest,
                              ssize_t src_stride, mpq_t *src, size_t count)
{
  for (size_t i = 0; i < count; i++)
    {
      mpq_neg (*dest, *src);
      dest += dest_stride;
      src += src_stride;
    }
}

VISIBLE void
copy_negate_mpf_with_strides (ssize_t dest_stride, mpf_t *dest,
                              ssize_t src_stride, mpf_t *src, size_t count)
{
  for (size_t i = 0; i < count; i++)
    {
      mpf_neg (*dest, *src);
      dest += dest_stride;
      src += src_stride;
    }
}
