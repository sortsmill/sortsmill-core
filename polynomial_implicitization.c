#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <string.h>
#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

static inline size_t
szmin (size_t a, size_t b)
{
  return (a < b) ? a : b;
}

//----------------------------------------------------------------------
//
// Implicitization of a polynomial curve given parametrically in
// monomial basis, using exact arithmetic.

static void
mpq_poly_implicitize_mono_degreeone (ssize_t stride_a, mpq_t *a,
                                     ssize_t stride_b, mpq_t *b,
                                     mpq_t implicit_equation[2][2])
{
  // WARNING NOTE: The implicit_equation array should _not_ share
  // space with a or b.

  mpq_t tmp;
  mpq_init (tmp);

  // If x = a0 + a1*t and y = b0 + b1*t, then the implicit equation is
  //
  //    0 = (a1*b0 - a0*b1) + b1*x - a1*y

  // Constant term = a1*b0 - a0*b1
  mpq_mul (implicit_equation[0][0], a[stride_a], b[0]);
  mpq_mul (tmp, a[0], b[stride_b]);
  mpq_sub (implicit_equation[0][0], implicit_equation[0][0], tmp);

  // x term = b1
  mpq_set (implicit_equation[1][0], a[stride_a]);

  // y term = -a1
  mpq_neg (implicit_equation[0][1], b[stride_b]);

  // implicit_equation[1][1] is unused.

  mpq_clear (tmp);
}

static void
term_ij (mpq_t result, mpq_t *a, mpq_t *b, size_t i, size_t j, mpq_t workspace)
{
  mpq_mul (result, a[j], b[i]);
  mpq_mul (workspace, a[i], b[j]);
  mpq_sub (result, result, workspace);
}

static void
mpq_poly_implicitize_mono_degreetwo (ssize_t stride_a, mpq_t *a,
                                     ssize_t stride_b, mpq_t *b,
                                     mpq_t implicit_equation[3][3])
{
  // FIXME: No real attempt has been made to minimize the number of
  // computations.

  mpq_t tmp1;
  mpq_init (tmp1);

  mpq_t entry01[2][2];
  mpq_bipoly_init (1, entry01);
  term_ij (entry01[0][0], a, b, 0, 1, tmp1);
  mpq_set (entry01[1][0], b[1]);
  mpq_neg (entry01[0][1], a[1]);

  mpq_t entry02[2][2];
  mpq_bipoly_init (1, entry02);
  term_ij (entry02[0][0], a, b, 0, 2, tmp1);
  mpq_set (entry02[1][0], b[2]);
  mpq_neg (entry02[0][1], a[2]);

  mpq_t d12;
  mpq_init (d12);
  term_ij (d12, a, b, 1, 2, tmp1);

  mpq_bipoly_mul (1, entry02, 1, entry02, implicit_equation);
  mpq_bipoly_scalarmul (1, entry01, d12, entry01);
  mpq_bipoly_sub (2, implicit_equation, 1, entry01, implicit_equation);

  mpq_clear (d12);
  mpq_bipoly_clear (1, entry02);
  mpq_bipoly_clear (1, entry01);
  mpq_clear (tmp1);
}

static void
mpq_poly_implicitize_mono_degreethree (ssize_t stride_a, mpq_t *a,
                                       ssize_t stride_b, mpq_t *b,
                                       mpq_t implicit_equation[4][4])
{
  // FIXME: No real attempt has been made to minimize the number of
  // computations.

  mpq_t tmp1;
  mpq_init (tmp1);

  mpq_t tmp2[3][3];
  mpq_bipoly_init (2, tmp2);

  mpq_t entry01[2][2];
  mpq_bipoly_init (1, entry01);
  term_ij (entry01[0][0], a, b, 0, 1, tmp1);
  mpq_set (entry01[1][0], b[1]);
  mpq_neg (entry01[0][1], a[1]);

  mpq_t entry02[2][2];
  mpq_bipoly_init (1, entry02);
  term_ij (entry02[0][0], a, b, 0, 2, tmp1);
  mpq_set (entry02[1][0], b[2]);
  mpq_neg (entry02[0][1], a[2]);

  mpq_t entry03[2][2];
  mpq_bipoly_init (1, entry03);
  term_ij (entry03[0][0], a, b, 0, 3, tmp1);
  mpq_set (entry03[1][0], b[3]);
  mpq_neg (entry03[0][1], a[3]);

  mpq_t d12, d13, d23, d13_squared;
  mpq_inits (d12, d13, d23, d13_squared, NULL);
  term_ij (d12, a, b, 1, 2, tmp1);
  term_ij (d13, a, b, 1, 3, tmp1);
  term_ij (d23, a, b, 2, 3, tmp1);
  mpq_mul (d13_squared, d13, d13);

  mpq_t entry_middle[2][2];
  mpq_bipoly_init (1, entry_middle);
  mpq_add (entry_middle[0][0], entry03[0][0], d12);
  mpq_set (entry_middle[1][0], entry03[1][0]);
  mpq_set (entry_middle[0][1], entry03[0][1]);

  mpq_t entry02_squared[3][3];
  mpq_bipoly_init (2, entry02_squared);
  mpq_bipoly_mul (1, entry02, 1, entry02, entry02_squared);

  mpq_t entry03_squared[3][3];
  mpq_bipoly_init (2, entry03_squared);
  mpq_bipoly_mul (1, entry03, 1, entry03, entry03_squared);

  mpq_bipoly_mul (2, entry03_squared, 1, entry_middle, implicit_equation);

  mpq_bipoly_mul (1, entry01, 1, entry_middle, tmp2);
  mpq_bipoly_scalarmul (2, tmp2, d23, tmp2);
  mpq_bipoly_sub (3, implicit_equation, 2, tmp2, implicit_equation);

  mpq_bipoly_mul (1, entry02, 1, entry03, tmp2);
  mpq_bipoly_scalarmul (2, tmp2, d13, tmp2);
  mpq_bipoly_add (2, tmp2, 2, tmp2, tmp2);
  mpq_bipoly_sub (3, implicit_equation, 2, tmp2, implicit_equation);

  mpq_bipoly_scalarmul (1, entry01, d13_squared, entry01);
  mpq_bipoly_add (3, implicit_equation, 1, entry01, implicit_equation);

  mpq_bipoly_scalarmul (2, entry02_squared, d23, entry02_squared);
  mpq_bipoly_add (3, implicit_equation, 2, entry02_squared, implicit_equation);

  mpq_bipoly_clear (2, entry03_squared);
  mpq_bipoly_clear (2, entry02_squared);
  mpq_bipoly_clear (1, entry_middle);
  mpq_clears (d12, d13, d23, d13_squared, NULL);
  mpq_bipoly_clear (1, entry03);
  mpq_bipoly_clear (1, entry02);
  mpq_bipoly_clear (1, entry01);
  mpq_bipoly_clear (2, tmp2);
  mpq_clear (tmp1);
}

static void
_mpq_init_square_array (size_t n, mpq_t A[n][n])
{
  for (size_t i = 0; i < n; i++)
    for (size_t j = 0; j < n; j++)
      mpq_init (A[i][j]);
}

static void
_mpq_clear_square_array (size_t n, mpq_t A[n][n])
{
  for (size_t i = 0; i < n; i++)
    for (size_t j = 0; j < n; j++)
      mpq_clear (A[i][j]);
}

static void
_mpq_lij (ssize_t stride_x, mpq_t *x,
          ssize_t stride_y, mpq_t *y, size_t i, size_t j, mpq_t bipoly[2][2])
{
  mpq_mul (bipoly[0][0], x[(ssize_t) j * stride_x], y[(ssize_t) i * stride_y]);
  // Use bipoly[1][0] as temporary storage.
  mpq_mul (bipoly[1][0], x[(ssize_t) i * stride_x], y[(ssize_t) j * stride_y]);
  mpq_sub (bipoly[0][0], bipoly[0][0], bipoly[1][0]);

  if (i == 0)
    {
      mpq_set (bipoly[1][0], y[(ssize_t) j * stride_y]);
      mpq_neg (bipoly[0][1], x[(ssize_t) j * stride_x]);
    }
  else if (j == 0)
    {
      mpq_neg (bipoly[1][0], y[(ssize_t) i * stride_y]);
      mpq_set (bipoly[0][1], x[(ssize_t) i * stride_x]);
    }
  else
    {
      mpq_set_ui (bipoly[1][0], 0, 1);
      mpq_set_ui (bipoly[0][1], 0, 1);
    }
}

static void
_mpq_Lij (size_t degree, ssize_t stride_x, mpq_t *x,
          ssize_t stride_y, mpq_t *y, size_t i, size_t j, mpq_t bipoly[2][2])
{
  mpq_t tmp[2][2];
  mpq_bipoly_init (1, tmp);

  mpq_set_ui (bipoly[0][0], 0, 1);
  mpq_set_ui (bipoly[1][0], 0, 1);
  mpq_set_ui (bipoly[0][1], 0, 1);

  const ssize_t min_ij = szmin (i, j);
  const size_t m0 = (degree < i + j + 1) ? (i + j + 1 - degree) : 0;
  for (size_t m = m0; m <= min_ij; m++)
    {
      size_t k = i + j + 1 - m;
      _mpq_lij (stride_x, x, stride_y, y, k, m, tmp);
      mpq_add (bipoly[0][0], bipoly[0][0], tmp[0][0]);
      mpq_add (bipoly[1][0], bipoly[1][0], tmp[1][0]);
      mpq_add (bipoly[0][1], bipoly[0][1], tmp[0][1]);
    }

  mpq_bipoly_clear (1, tmp);
}

static void
_mpq_make_bezout_matrix (size_t degree,
                         ssize_t stride_x, mpq_t *x,
                         ssize_t stride_y, mpq_t *y,
                         mpq_t bezout[degree][degree][2][2])
{
  //
  // FIXME: Take advantage of symmetries in the matrix.
  //
  for (size_t i = 0; i < degree; i++)
    for (size_t j = 0; j < degree; j++)
      _mpq_Lij (degree, stride_x, x, stride_y, y, i, j, bezout[i][j]);
}

static void
_mpq_bezout_determinant_recursion (size_t degree,
                                   mpq_t *bez[degree][degree],
                                   mpq_t
                                   implicit_equation[degree + 1][degree + 1])
{
  // Expand by cofactors of the top row.
  //
  // NOTE: This is very, very, very inefficient for large matrices.

  switch (degree)
    {
    case 0:
      assert (false);
      break;

    case 1:
      {
        mpq_t (*b)[2] = (mpq_t (*)[2]) bez[0][0];
        mpq_set (implicit_equation[0][0], b[0][0]);
        mpq_set (implicit_equation[1][0], b[1][0]);
        mpq_set (implicit_equation[0][1], b[0][1]);
      }
      break;

    default:
      {
        mpq_t *(*minor)[degree - 1] =
          xmalloc ((degree - 1) * (degree - 1) * sizeof (mpq_t *));
        mpq_t (*det_of_minor)[degree] =
          xmalloc (degree * degree * sizeof (mpq_t));
        _mpq_init_square_array (degree, det_of_minor);
        mpq_t (*term)[degree + 1] =
          xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));
        _mpq_init_square_array (degree + 1, term);

        for (size_t i = 1; i < degree; i++)
          for (size_t j = 1; j < degree; j++)
            minor[i - 1][j - 1] = bez[i][j];
        _mpq_bezout_determinant_recursion (degree - 1, minor, det_of_minor);
        mpq_bipoly_mul (1, (mpq_t (*)[2]) bez[0][0], degree - 1, det_of_minor,
                        implicit_equation);

        int sign = -1;
        for (size_t k = 1; k < degree; k++)
          {
            for (size_t i = 1; i < degree; i++)
              {
                for (size_t j = 0; j < k; j++)
                  minor[i - 1][j] = bez[i][j];
                for (size_t j = k + 1; j < degree; j++)
                  minor[i - 1][j - 1] = bez[i][j];
              }
            _mpq_bezout_determinant_recursion (degree - 1, minor, det_of_minor);
            mpq_bipoly_mul (1, (mpq_t (*)[2]) bez[0][k],
                            degree - 1, det_of_minor, term);
            if (sign < 0)
              mpq_bipoly_sub (degree, implicit_equation, degree, term,
                              implicit_equation);
            else
              mpq_bipoly_add (degree, implicit_equation, degree, term,
                              implicit_equation);
            sign = -sign;
          }

        _mpq_clear_square_array (degree + 1, term);
        free (term);
        _mpq_clear_square_array (degree, det_of_minor);
        free (det_of_minor);
        free (minor);
      }
      break;
    }
}

static void
_mpq_bezout_determinant (size_t degree,
                         mpq_t bezout[degree][degree][2][2],
                         mpq_t implicit_equation[degree + 1][degree + 1])
{
  mpq_t *(*bez)[degree] = xmalloc (degree * degree * sizeof (mpq_t));

  for (size_t i = 0; i < degree; i++)
    for (size_t j = 0; j < degree; j++)
      bez[i][j] = &bezout[i][j][0][0];
  _mpq_bezout_determinant_recursion (degree, bez, implicit_equation);

  free (bez);
}

VISIBLE void
mpq_poly_implicitize_mono (size_t degree,
                           ssize_t stride_a, mpq_t *a,
                           ssize_t stride_b, mpq_t *b,
                           mpq_t implicit_equation[degree + 1][degree + 1],
                           int *info)
{
  // WARNING NOTE: The implicit_equation array should _not_ share
  // space with a or b.

  int info1 = 0;

  switch (degree)
    {
    case 0:
      info1 = -1;               // There is a problem in the first argument.
      break;
    case 1:
      mpq_poly_implicitize_mono_degreeone (stride_a, a, stride_b, b,
                                           implicit_equation);
      break;
    case 2:
      mpq_poly_implicitize_mono_degreetwo (stride_a, a, stride_b, b,
                                           implicit_equation);
      break;
    case 3:
      mpq_poly_implicitize_mono_degreethree (stride_a, a, stride_b, b,
                                             implicit_equation);
      break;
    default:
      {
        mpq_t (*bezout)[degree][2][2] =
          xmalloc (degree * degree * 4 * sizeof (mpq_t));
        for (size_t i = 0; i < degree; i++)
          for (size_t j = 0; j < degree; j++)
            mpq_bipoly_init (1, bezout[i][j]);

        _mpq_make_bezout_matrix (degree, stride_a, a, stride_b, b, bezout);
        _mpq_bezout_determinant (degree, bezout, implicit_equation);

        for (size_t i = 0; i < degree; i++)
          for (size_t j = 0; j < degree; j++)
            mpq_bipoly_clear (1, bezout[i][j]);
        free (bezout);
      }
      break;
    }

  if (info != NULL)
    *info = info1;
}

VISIBLE void
mpq_poly_plugintoimplicit_mono (size_t degree1,
                                mpq_t implicit_eq[degree1 + 1][degree1 + 1],
                                size_t degree2, ssize_t stride_x, mpq_t *x,
                                ssize_t stride_y, mpq_t *y,
                                ssize_t result_stride, mpq_t *result)
{
  // FIXME: No major attempt is made to minimize the number of
  // computations.

  if (degree1 == 0)
    mpq_set (result[0], implicit_eq[0][0]);
  else
    {
      mpq_t (*powers_of_x)[degree1 * degree2 + 1] =
        xmalloc ((degree1 + 1) * (degree1 * degree2 + 1) * sizeof (mpq_t));
      mpq_t (*powers_of_y)[degree1 * degree2 + 1] =
        xmalloc ((degree1 + 1) * (degree1 * degree2 + 1) * sizeof (mpq_t));
      mpq_init (powers_of_x[0][0]);
      mpq_init (powers_of_y[0][0]);
      mpq_set_ui (powers_of_x[0][0], 1, 1);
      mpq_set_ui (powers_of_y[0][0], 1, 1);
      mpq_init_poly (degree2, powers_of_x[1]);
      mpq_init_poly (degree2, powers_of_y[1]);
      copy_mpq_with_strides (1, powers_of_x[1], stride_x, x, degree2 + 1);
      copy_mpq_with_strides (1, powers_of_y[1], stride_y, y, degree2 + 1);
      for (size_t i = 2; i <= degree1; i++)
        {
          mpq_init_poly (i * degree2, powers_of_x[i]);
          mpq_init_poly (i * degree2, powers_of_y[i]);
          mpq_poly_mul_mono (degree2, 1, powers_of_x[1],
                             (i - 1) * degree2, 1, powers_of_x[i - 1],
                             1, powers_of_x[i]);
          mpq_poly_mul_mono (degree2, 1, powers_of_y[1],
                             (i - 1) * degree2, 1, powers_of_y[i - 1],
                             1, powers_of_y[i]);
        }

      mpq_t buffer[degree1 * degree2 + 1];
      mpq_init_poly (degree1 * degree2, buffer);

      mpq_t tmp[degree1 * degree2 + 1];
      mpq_init_poly (degree1 * degree2, tmp);

      // Bivariate polynomial terms of degree degree1.
      mpq_poly_scalarmul (degree1 * degree2, 1, powers_of_x[degree1],
                          implicit_eq[degree1][0], 1, buffer);
      for (size_t j = 1; j <= degree1; j++)
        {
          mpq_poly_scalarmul ((degree1 - j) * degree2, 1,
                              powers_of_x[degree1 - j],
                              implicit_eq[degree1 - j][j], 1, tmp);
          mpq_poly_mul_mono ((degree1 - j) * degree2, 1, tmp, j * degree2, 1,
                             powers_of_y[j], 1, tmp);
          mpq_poly_add_mono (degree1 * degree2, 1, buffer,
                             degree1 * degree2, 1, tmp, 1, buffer);
        }

      // Bivariate polynomial terms of degree 1 to degree1.
      for (size_t i = 1; i < degree1; i++)
        for (size_t j = 0; j <= i; j++)
          {
            mpq_poly_scalarmul ((i - j) * degree2, 1, powers_of_x[i - j],
                                implicit_eq[i - j][j], 1, tmp);
            mpq_poly_mul_mono ((i - j) * degree2, 1, tmp,
                               j * degree2, 1, powers_of_y[j], 1, tmp);
            mpq_poly_add_mono (degree1 * degree2, 1, buffer, i * degree2, 1,
                               tmp, 1, buffer);
          }

      // The bivariate polynomial term of degree zero.
      mpq_add (buffer[0], buffer[0], implicit_eq[0][0]);

      copy_mpq_with_strides (result_stride, result, 1, buffer,
                             degree1 * degree2 + 1);

      mpq_clear_poly (degree1 * degree2, tmp);
      mpq_clear_poly (degree1 * degree2, buffer);
      for (size_t i = 0; i <= degree1; i++)
        {
          mpq_clear_poly (i * degree2, powers_of_x[i]);
          mpq_clear_poly (i * degree2, powers_of_y[i]);
        }

      free (powers_of_x);
      free (powers_of_y);
    }
}

//----------------------------------------------------------------------
//
// Implicitization of a polynomial curve given parametrically in
// Bernstein basis, using floating point arithmetic.

static void
_store_bincoefs (size_t degree, double bincoefs[degree + 1])
{
  for (size_t i = 0; i <= degree; i++)
    bincoefs[i] = bincoef (degree, i);
}

static void
_lij (const double *bincoefs,
      ssize_t stride_x, const double *x,
      ssize_t stride_y, const double *y,
      size_t i, size_t j, double bipoly[2][2])
{
  const double xi = x[(ssize_t) i * stride_x];
  const double xj = x[(ssize_t) j * stride_x];
  const double yi = y[(ssize_t) i * stride_y];
  const double yj = y[(ssize_t) j * stride_y];
  const double s = bincoefs[i] * bincoefs[j];

  bipoly[0][0] = s * (xi * yj - xj * yi);       // Constant term.
  bipoly[1][0] = s * (yi - yj); // Coefficient of x.
  bipoly[0][1] = s * (xj - xi); // Coefficient of y.
  bipoly[1][1] = 0;             // Not used.
}

static void
_Lij (const double *bincoefs,
      size_t degree,
      ssize_t stride_x, const double *x,
      ssize_t stride_y, const double *y,
      size_t i, size_t j, double bipoly[2][2])
{
  double tmp[2][2];

  bipoly[0][0] = 0;
  bipoly[1][0] = 0;
  bipoly[0][1] = 0;
  bipoly[1][1] = 0;

  const ssize_t min_ij = szmin (i, j);
  const size_t m0 = (degree < i + j + 1) ? (i + j + 1 - degree) : 0;
  for (size_t m = m0; m <= min_ij; m++)
    {
      size_t k = i + j + 1 - m;
      _lij (bincoefs, stride_x, x, stride_y, y, k, m, tmp);
      bipoly[0][0] += tmp[0][0];
      bipoly[1][0] += tmp[1][0];
      bipoly[0][1] += tmp[0][1];
    }
}

static void
_make_bezout_matrix (size_t degree,
                     ssize_t stride_x, const double *x,
                     ssize_t stride_y, const double *y,
                     double bezout[degree][degree][2][2])
{
  double bincoefs[degree + 1];
  _store_bincoefs (degree, bincoefs);

  //
  // FIXME: Take advantage of symmetries in the matrix.
  //
  for (size_t i = 0; i < degree; i++)
    for (size_t j = 0; j < degree; j++)
      _Lij (bincoefs, degree, stride_x, x, stride_y, y, i, j, bezout[i][j]);
}

static void
_bezout_determinant_recursion (size_t degree,
                               double *bez[degree][degree],
                               double implicit_equation[degree + 1][degree + 1])
{
  // Expand by cofactors of the top row.
  //
  // NOTE: This is very, very, very inefficient for large matrices.

  switch (degree)
    {
    case 0:
      assert (false);
      break;

    case 1:
      {
        double (*b)[2] = (double (*)[2]) bez[0][0];
        implicit_equation[0][0] = b[0][0];
        implicit_equation[1][0] = b[1][0];
        implicit_equation[0][1] = b[0][1];
        implicit_equation[1][1] = 0;
      }
      break;

    default:
      {
        double *minor[degree - 1][degree - 1];
        double det_of_minor[degree][degree];
        double term[degree + 1][degree + 1];

        for (size_t i = 1; i < degree; i++)
          for (size_t j = 1; j < degree; j++)
            minor[i - 1][j - 1] = bez[i][j];
        _bezout_determinant_recursion (degree - 1, minor, det_of_minor);
        dbipoly_mul (1, (double (*)[2]) bez[0][0], degree - 1, det_of_minor,
                     implicit_equation);

        int sign = -1;
        for (size_t k = 1; k < degree; k++)
          {
            for (size_t i = 1; i < degree; i++)
              {
                for (size_t j = 0; j < k; j++)
                  minor[i - 1][j] = bez[i][j];
                for (size_t j = k + 1; j < degree; j++)
                  minor[i - 1][j - 1] = bez[i][j];
              }
            _bezout_determinant_recursion (degree - 1, minor, det_of_minor);
            dbipoly_mul (1, (double (*)[2]) bez[0][k], degree - 1, det_of_minor,
                         term);
            if (sign < 0)
              dbipoly_sub (degree, implicit_equation, degree, term,
                           implicit_equation);
            else
              dbipoly_add (degree, implicit_equation, degree, term,
                           implicit_equation);
            sign = -sign;
          }
      }
      break;
    }
}

static void
_bezout_determinant (size_t degree,
                     double bezout[degree][degree][2][2],
                     double implicit_equation[degree + 1][degree + 1])
{
  double *bez[degree][degree];
  for (size_t i = 0; i < degree; i++)
    for (size_t j = 0; j < degree; j++)
      bez[i][j] = &bezout[i][j][0][0];
  _bezout_determinant_recursion (degree, bez, implicit_equation);
}

VISIBLE void
dpoly_implicitize_bern (size_t degree,
                        ssize_t stride_x, const double *x,
                        ssize_t stride_y, const double *y,
                        double implicit_equation[degree + 1][degree + 1],
                        int *info)
{
  int info1;
  if (degree == 0)
    info1 = -1;                 // There is a problem in the first argument.
  else
    {
      info1 = 0;
      double bezout[degree][degree][2][2];
      _make_bezout_matrix (degree, stride_x, x, stride_y, y, bezout);
      _bezout_determinant (degree, bezout, implicit_equation);
    }

  if (info != NULL)
    *info = info1;
}

VISIBLE void
dpoly_implicitize_mono (size_t degree,
                        ssize_t stride_x, const double *x,
                        ssize_t stride_y, const double *y,
                        double implicit_equation[degree + 1][degree + 1],
                        int *info)
{
  double x_[degree + 1];
  double y_[degree + 1];
  dpoly_mono_to_bern (degree, stride_x, x, 1, x_);
  dpoly_mono_to_bern (degree, stride_y, y, 1, y_);
  dpoly_implicitize_bern (degree, 1, x_, 1, y_, implicit_equation, info);
}

VISIBLE void
dpoly_implicitize_sbern (size_t degree,
                         ssize_t stride_x, const double *x,
                         ssize_t stride_y, const double *y,
                         double implicit_equation[degree + 1][degree + 1],
                         int *info)
{
  double x_[degree + 1];
  double y_[degree + 1];
  dpoly_sbern_to_bern (degree, stride_x, x, 1, x_);
  dpoly_sbern_to_bern (degree, stride_y, y, 1, y_);
  dpoly_implicitize_bern (degree, 1, x_, 1, y_, implicit_equation, info);
}

VISIBLE void
dpoly_implicitize_spower (size_t degree,
                          ssize_t stride_x, const double *x,
                          ssize_t stride_y, const double *y,
                          double implicit_equation[degree + 1][degree + 1],
                          int *info)
{
  double x_[degree + 1];
  double y_[degree + 1];
  dpoly_spower_to_bern (degree, stride_x, x, 1, x_);
  dpoly_spower_to_bern (degree, stride_y, y, 1, y_);
  dpoly_implicitize_bern (degree, 1, x_, 1, y_, implicit_equation, info);
}

//----------------------------------------------------------------------

static void
_dpoly_plugintoimplicit (void (*add) (size_t, ssize_t, const double *,
                                      size_t, ssize_t, const double *,
                                      ssize_t, double *),
                         void (*mul) (size_t, ssize_t, const double *,
                                      size_t, ssize_t, const double *,
                                      ssize_t, double *),
                         size_t degree1,
                         double implicit_eq[degree1 + 1][degree1 + 1],
                         size_t degree2,
                         ssize_t stride_x, const double *x,
                         ssize_t stride_y, const double *y,
                         ssize_t result_stride, double *result)
{
  // FIXME: No major attempt is made to minimize the number of
  // computations.

  if (degree1 == 0)
    result[0] = implicit_eq[0][0];
  else
    {
      double powers_of_x[degree1 + 1][degree1 * degree2 + 1];
      double powers_of_y[degree1 + 1][degree1 * degree2 + 1];
      powers_of_x[0][0] = 1;
      powers_of_y[0][0] = 1;
      copy_f64_with_strides (1, powers_of_x[1], stride_x, x, degree2 + 1);
      copy_f64_with_strides (1, powers_of_y[1], stride_y, y, degree2 + 1);
      for (size_t i = 2; i <= degree1; i++)
        {
          mul (degree2, 1, powers_of_x[1],
               (i - 1) * degree2, 1, powers_of_x[i - 1], 1, powers_of_x[i]);
          mul (degree2, 1, powers_of_y[1],
               (i - 1) * degree2, 1, powers_of_y[i - 1], 1, powers_of_y[i]);
        }

      double buffer[degree1 * degree2 + 1];
      double tmp[degree1 * degree2 + 1];

      // Bivariate polynomial terms of degree degree1.
      dpoly_scalarmul (degree1 * degree2, 1, powers_of_x[degree1],
                       implicit_eq[degree1][0], 1, buffer);
      for (size_t j = 1; j <= degree1; j++)
        {
          dpoly_scalarmul ((degree1 - j) * degree2, 1, powers_of_x[degree1 - j],
                           implicit_eq[degree1 - j][j], 1, tmp);
          mul ((degree1 - j) * degree2, 1, tmp, j * degree2, 1, powers_of_y[j],
               1, tmp);
          add (degree1 * degree2, 1, buffer, degree1 * degree2, 1, tmp, 1,
               buffer);
        }

      // Bivariate polynomial terms of degree 1 to degree1.
      for (size_t i = 1; i < degree1; i++)
        for (size_t j = 0; j <= i; j++)
          {
            dpoly_scalarmul ((i - j) * degree2, 1, powers_of_x[i - j],
                             implicit_eq[i - j][j], 1, tmp);
            mul ((i - j) * degree2, 1, tmp, j * degree2, 1, powers_of_y[j],
                 1, tmp);
            add (degree1 * degree2, 1, buffer, i * degree2, 1, tmp, 1, buffer);
          }

      // The bivariate polynomial term of degree zero.
      add (degree1 * degree2, 1, buffer, 0, 1, &implicit_eq[0][0], 1, buffer);

      copy_f64_with_strides (result_stride, result, 1, buffer,
                             degree1 * degree2 + 1);
    }
}

VISIBLE void
dpoly_plugintoimplicit_mono (size_t degree1,
                             double implicit_eq[degree1 + 1][degree1 + 1],
                             size_t degree2,
                             ssize_t stride_x, const double *x,
                             ssize_t stride_y, const double *y,
                             ssize_t result_stride, double *result)
{
  _dpoly_plugintoimplicit (dpoly_add_mono, dpoly_mul_mono,
                           degree1, implicit_eq,
                           degree2, stride_x, x, stride_y, y,
                           result_stride, result);
}

VISIBLE void
dpoly_plugintoimplicit_bern (size_t degree1,
                             double implicit_eq[degree1 + 1][degree1 + 1],
                             size_t degree2,
                             ssize_t stride_x, const double *x,
                             ssize_t stride_y, const double *y,
                             ssize_t result_stride, double *result)
{
  _dpoly_plugintoimplicit (dpoly_add_bern, dpoly_mul_bern,
                           degree1, implicit_eq,
                           degree2, stride_x, x, stride_y, y,
                           result_stride, result);
}

VISIBLE void
dpoly_plugintoimplicit_sbern (size_t degree1,
                              double implicit_eq[degree1 + 1][degree1 + 1],
                              size_t degree2,
                              ssize_t stride_x, const double *x,
                              ssize_t stride_y, const double *y,
                              ssize_t result_stride, double *result)
{
  _dpoly_plugintoimplicit (dpoly_add_sbern, dpoly_mul_sbern,
                           degree1, implicit_eq,
                           degree2, stride_x, x, stride_y, y,
                           result_stride, result);
}

VISIBLE void
dpoly_plugintoimplicit_spower (size_t degree1,
                               double implicit_eq[degree1 + 1][degree1 + 1],
                               size_t degree2,
                               ssize_t stride_x, const double *x,
                               ssize_t stride_y, const double *y,
                               ssize_t result_stride, double *result)
{
  _dpoly_plugintoimplicit (dpoly_add_spower, dpoly_mul_spower,
                           degree1, implicit_eq,
                           degree2, stride_x, x, stride_y, y,
                           result_stride, result);
}

//----------------------------------------------------------------------
