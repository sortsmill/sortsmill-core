#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <xalloc.h>
#include <libintl.h>
#include <sortsmill/core/hobby.h>
#include <sortsmill/core/gausselimsolve.h>
#include <sortsmill/core/tridiagsolve.h>
#include <sortsmill/core/cyclictridiagsolve.h>

#define _(msgid) gettext (msgid)

// http://oeis.org/A002193
#define _SQRT2 1.41421356237309504880168872420969807856967187537694807317667973799073247846210703885038753432764157
//#define _SQRT2 M_SQRT2 // <-- alternatively, we could use this.

// http://oeis.org/A002163
#define _SQRT5 2.23606797749978969640917366873127623544061835961152572427089724541052092563780489941441440837878227

static inline size_t
minus_one_mod_n (size_t k, size_t n)
{
  return (k == 0) ? (n - 1) : (k - 1);
}

static inline size_t
plus_one_mod_n (size_t k, size_t n)
{
  return (k == n - 1) ? 0 : (k + 1);
}

static inline size_t
plus_one_but_at_most_INT_MAX (size_t i)
{
  return (i < INT_MAX) ? (i + 1) : INT_MAX;
}

static inline double
angle_from_to (double complex u, double complex v)
{
  return (cabs (u) < cabs (v)) ? (-carg (u / v)) : (carg (v / u));
}

#if 0
// The default allocator for the numerical routines is Boehm GC, in
// case there is some exception mechanism for numerical
// problems. Currently that seems unlikely, but, for instance, if the
// routines were modified to use GSL, then the GSL error handler might
// raise an exception. Also using Boehm GC is more resilient against
// forgetting to free.
#include <sortsmill/core/xgc.h>
#define __HOBBY_ALLOC(n, T) (x_gc_malloc ((n) * sizeof (T)))
#define __HOBBY_ALLOC_ATOMIC(n, T) (x_gc_malloc_atomic ((n) * sizeof (T)))
#define __HOBBY_ALLOC_FREE(p) (x_gc_free (p))
#endif

// The default allocator for the numerical routines is the system
// allocator.
#define __HOBBY_ALLOC(n, T) (xmalloc ((n) * sizeof (T)))
#define __HOBBY_ALLOC_ATOMIC(n, T) (xmalloc ((n) * sizeof (T)))
#define __HOBBY_ALLOC_FREE(p) (free (p))

//-------------------------------------------------------------------------

// A function (or factor) due to John Hobby. See the METAFONTbook.
static double
f_hobby (double theta, double phi)
{
  const double sintheta = sin (theta);
  const double costheta = cos (theta);
  const double sinphi = sin (phi);
  const double cosphi = cos (phi);
  const double top =
    2.0 + (_SQRT2 * (sintheta - sinphi / 16.0) * (sinphi - sintheta / 16.0)
           * (costheta - cosphi));
  const double bottom =
    3.0 * (1.0 + 0.5 * (_SQRT5 - 1.0) * costheta +
           0.5 * (3.0 - _SQRT5) * cosphi);
  return top / bottom;
}

// Find off-curve control points of a cubic bezier, given on-curve
// points, directions, and tensions. See the METAFONTbook.
//
// If dir0 and dir1 have magnitudes that are very close to zero,
// certain ratios may blow up.
static void
find_control_points (double complex p0, double complex dir0,
                     double tension0, double tension3,
                     double complex dir3, double complex p3,
                     double complex *p1, double complex *p2)
{
  const double complex chord = p3 - p0;
  const double abs_chord = cabs (chord);

  const double abs_dir0 = cabs (dir0);  // Should not be near zero.
  const double abs_dir3 = cabs (dir3);  // Should not be near zero.

  // theta is the angle from the chord to dir0.
  const double theta =
    (abs_chord < abs_dir0) ? (-carg (chord / dir0)) : (carg (dir0 / chord));

  // phi is the angle from dir3 to the chord.
  const double phi =
    (abs_chord < abs_dir3) ? (carg (chord / dir3)) : (-carg (dir3 / chord));

  // The following statements could also be written as follows:
  //
  //  *p1 = p0 + cexp (theta * I) * chord * (f_hobby (theta, phi) / tension0);
  //  *p2 = p3 - cexp ((-phi) * I) * chord * (f_hobby (phi, theta) / tension3);
  //
  // We used to use those expressions, but the following track the
  // specified directions more accurately.
  //
  *p1 =
    p0 + dir0 * ((abs_chord * f_hobby (theta, phi)) / (abs_dir0 * tension0));
  *p2 =
    p3 - dir3 * ((abs_chord * f_hobby (phi, theta)) / (abs_dir3 * tension3));
}

// FIXME: This is more generally useful.
static void
find_intersection_of_lines (double complex p1, double complex p2,
                            double complex p3, double complex p4,
                            double complex *intersection, bool *in_segment12,
                            bool *in_segment34)
{
  // NOTE: This implementation assumes IEEE floating point with
  // infinities.

  const double x1 = creal (p1);
  const double y1 = cimag (p1);
  const double x2 = creal (p2);
  const double y2 = cimag (p2);
  const double x3 = creal (p3);
  const double y3 = cimag (p3);
  const double x4 = creal (p4);
  const double y4 = cimag (p4);
  const double denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
  const double numer_a = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
  const double numer_b = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);
  const double ua = numer_a / denom;
  const double ub = numer_b / denom;
  const double x = x1 + ua * (x2 - x1);
  const double y = y1 + ua * (y2 - y1);
  const bool intersection_is_finite = (isfinite (x) && isfinite (y));
  *intersection = x + y * I;
  *in_segment12 = (intersection_is_finite && 0.0 <= ua && ua <= 1.0);
  *in_segment34 = (intersection_is_finite && 0.0 <= ub && ub <= 1.0);
}

static void
remove_inflection (double complex p0, double complex p1, double complex p2,
                   double complex p3, double complex *q1, double complex *q2)
{
  double complex intersection;
  bool in_segment01;
  bool in_segment32;
  find_intersection_of_lines (p0, p1, p3, p2, &intersection,
                              &in_segment01, &in_segment32);
  *q1 = (in_segment01) ? intersection : p1;
  *q2 = (in_segment32) ? intersection : p2;
}

// Find off-curve control points of a cubic bezier, given on-curve
// points, directions, and tensions. See the METAFONTbook. If
// perhaps_increase_tensions is true, then either or both of the
// tensions might be increased to keep the ‘control handles’ from
// getting ‘too long’.
//
// If dir0 and dir1 have magnitudes that are very close to zero,
// certain ratios may blow up.
VISIBLE void
hobby_tensions_to_control_points (bool perhaps_increase_tensions,
                                  double complex p0, double complex dir0,
                                  double tension0, double tension3,
                                  double complex dir3, double complex p3,
                                  double complex *p1, double complex *p2)
{
  double complex q1;
  double complex q2;
  find_control_points (p0, dir0, tension0, tension3, dir3, p3, &q1, &q2);
  if (perhaps_increase_tensions)
    remove_inflection (p0, q1, q2, p3, p1, p2);
  else
    {
      *p1 = q1;
      *p2 = q2;
    }
}

// Reverse the operation of hobby_tensions_to_control_points, to give
// tension values (which may be non-finite). Direction vectors are, of
// course, had trivially as p1 - p0 and p3 - p2.
VISIBLE void
control_points_to_hobby_tensions (double complex p0, double complex p1,
                                  double complex p2, double complex p3,
                                  double *tension0, double *tension3)
{
  const double complex chord = p3 - p0;
  const double abs_chord = cabs (chord);


  const double complex dir0 = p1 - p0;
  const double abs_dir0 = cabs (dir0);
  const double complex dir3 = p3 - p2;
  const double abs_dir3 = cabs (dir3);

  // theta is the angle from the chord to dir0.
  const double theta =
    (abs_chord < abs_dir0) ? (-carg (chord / dir0)) : (carg (dir0 / chord));

  // phi is the angle from dir3 to the chord.
  const double phi =
    (abs_chord < abs_dir3) ? (carg (chord / dir3)) : (-carg (dir3 / chord));

  *tension0 = (abs_chord * f_hobby (theta, phi)) / cabs (p1 - p0);
  *tension3 = (abs_chord * f_hobby (phi, theta)) / cabs (p3 - p2);
}

//-------------------------------------------------------------------------

static double
turn_angle (double complex v0, double complex v1)
{
  const double abs_v0 = cabs (v0);
  const double abs_v1 = cabs (v1);
  const double angle = (abs_v0 < abs_v1) ? (-carg (v0 / v1)) : (carg (v1 / v0));
  return (fabs (angle + M_PI) < DBL_EPSILON) ? M_PI : angle;
}

static double complex
angle_to_dir (double complex vector, double dir_angle)
{
  return (vector / cabs (vector)) * cexp (dir_angle * I);
}

//-------------------------------------------------------------------------

static void
prepare_working_data (size_t n, const double a_tensions[n - 1],
                      const double b_tensions[n - 1],
                      const double complex points[n],
                      double alpha[n - 1], double beta[n - 1],
                      double len[n - 1], double complex vector[n],
                      double turn[n])
{
  for (size_t k = 0; k < n - 1; k++)
    {
      // Reciprocals of the tensions.
      alpha[k] = 1.0 / a_tensions[k];
      beta[k] = 1.0 / b_tensions[k];

      // Point-to-point vectors.
      vector[k] = points[k + 1] - points[k];

      // The lengths of those vectors.
      len[k] = cabs (vector[k]);
    }

  for (size_t k = 1; k < n - 1; k++)
    {
      // Turning angles, in radians.
      turn[k - 1] = turn_angle (vector[k - 1], vector[k]);
    }

  // The last turning angle is arbitrary, and for convenience is set
  // to zero. The vector list is extended to express the same
  // arbitrary choice.
  turn[n - 1] = 0.0;
  vector[n - 1] = vector[n - 2];
}

static void
construct_system (double complex start_dir, double complex end_dir,
                  double start_curl, double end_curl, size_t n,
                  double alpha[n - 1], double beta[n - 1],
                  double len[n - 1], double complex vector[n], double turn[n],
                  double subdiag[n - 1], double diag[n],
                  double superdiag[n - 1], double right_side[n])
{
  if (start_dir != 0)
    {
      diag[0] = 1.0;
      superdiag[0] = 0.0;
      right_side[0] = angle_from_to (vector[0], start_dir);
    }
  else
    {
      const double chi_numer = alpha[0] * alpha[0] * start_curl;
      const double chi_denom = beta[0] * beta[0];
      if (chi_numer == 0 && chi_denom == 0)
        {
          diag[0] = 1.0;
          superdiag[0] = 0.0;
          right_side[0] = 0.0;
        }
      else
        {
          // Use the equations from Metafont, section 277, but
          // multiplying through by the denominators.
          const double c = alpha[0] * chi_numer + (3.0 - beta[0]) * chi_denom;
          const double d = (3.0 - alpha[0]) * chi_numer + beta[0] * chi_denom;
          diag[0] = c;
          superdiag[0] = d;
          right_side[0] = -d * turn[0];
        }
    }

  if (end_dir != 0)
    {
      subdiag[n - 2] = 0.0;
      diag[n - 1] = 1.0;
      right_side[n - 1] = angle_from_to (vector[n - 2], end_dir);
    }
  else
    {
      const double chi_numer = beta[n - 2] * beta[n - 2] * end_curl;
      const double chi_denom = alpha[n - 2] * alpha[n - 2];
      if (chi_numer == 0 && chi_denom == 0)
        {
          subdiag[n - 2] = 0.0;
          diag[n - 1] = 1.0;
          right_side[n - 1] = 0.0;
        }
      else
        {
          // Use the equations from Metafont, section 277, but
          // multiplying through by the denominators.
          const double a =
            (3.0 - beta[n - 2]) * chi_numer + alpha[n - 2] * chi_denom;
          const double b =
            beta[n - 2] * chi_numer + (3.0 - alpha[n - 2]) * chi_denom;
          subdiag[n - 2] = a;
          diag[n - 1] = b;
          right_side[n - 1] = 0.0;
        }
    }

  // Use the equations from Metafont, section 276, but multiplying
  // through by the denominators.
  double *denom_a_b = __HOBBY_ALLOC_ATOMIC (n - 2, double);
  double *denom_c_d = __HOBBY_ALLOC_ATOMIC (n - 2, double);
  for (size_t k = 0; k < n - 2; k++)
    {
      denom_a_b[k] = beta[k] * beta[k] * len[k];
      denom_c_d[k] = alpha[k + 1] * alpha[k + 1] * len[k + 1];
    }

  for (size_t k = 0; k < n - 2; k++)
    {
      const double a = denom_c_d[k] * alpha[k];
      const double b = denom_c_d[k] * (3.0 - alpha[k]);
      const double c = denom_a_b[k] * (3.0 - beta[k + 1]);
      const double d = denom_a_b[k] * beta[k + 1];
      const int vanished = (denom_a_b[k] == 0 && denom_c_d[k] == 0);
      subdiag[k] = a;
      diag[k + 1] = b + c + vanished;
      superdiag[k + 1] = d;
      right_side[k + 1] = -(b * turn[k] + d * turn[k + 1]);
    }

  __HOBBY_ALLOC_FREE (denom_a_b);
  __HOBBY_ALLOC_FREE (denom_c_d);
}

VISIBLE void
hobby_guess_directions (double complex start_dir, double complex end_dir,
                        double start_curl, double end_curl,
                        size_t n, const double a_tensions[n - 1],
                        const double b_tensions[n - 1],
                        const double complex points[n],
                        double complex directions[n], int *info,
                        const char **info_message)
{
  // NOTE: This implementation uses O(n) storage and runs in O(n)
  // time.

  *info_message = "";

  if (n < 2)
    {
      *info = -1001;
      *info_message = _("hobby_guess_directions() requires at least 2 points");
    }
  else if (INT_MAX < n)
    {
      // The requirement n <= INT_MAX is due to dtridiagsolve().
      *info = -1001;
      *info_message = _("too many points for hobby_guess_directions()");
    }
  else
    {
      // Normalize directions to unit vectors.
      if (start_dir != 0)
        start_dir = start_dir / cabs (start_dir);
      if (end_dir != 0)
        end_dir = end_dir / cabs (end_dir);

      // Reciprocals of a_tensions.
      double *alpha = __HOBBY_ALLOC_ATOMIC (n - 1, double);
      // Reciprocals of b_tensions.
      double *beta = __HOBBY_ALLOC_ATOMIC (n - 1, double);
      // The lengths of point-to-point vectors.
      double *len = __HOBBY_ALLOC_ATOMIC (n - 1, double);
      // Point-to-point vectors.
      double complex *vector = __HOBBY_ALLOC_ATOMIC (n, double complex);
      // Turning angles, in radians.
      double *turn = __HOBBY_ALLOC_ATOMIC (n, double);
      prepare_working_data (n, a_tensions, b_tensions, points, alpha, beta, len,
                            vector, turn);

      // Because the piecewise spline is non-periodic, the problem is
      // a tridiagonal system of n linear equations. The following
      // vectors will store the sparse matrices.
      double *subdiag = __HOBBY_ALLOC_ATOMIC (n - 1, double);
      double *diag = __HOBBY_ALLOC_ATOMIC (n, double);
      double *superdiag = __HOBBY_ALLOC_ATOMIC (n - 1, double);
      double *right_side = __HOBBY_ALLOC_ATOMIC (n, double);
      construct_system (start_dir, end_dir, start_curl, end_curl, n, alpha,
                        beta, len, vector, turn, subdiag, diag, superdiag,
                        right_side);

      // Solve the tridiagonal system.
      //
      // dtridiagsolve() overwrites right_side with the solution.
      dtridiagsolve (n, 1, subdiag, diag, superdiag, right_side, n, info);

      if (*info == 0)
        {
          directions[0] = (start_dir != 0) ?
            start_dir : (angle_to_dir (vector[0], right_side[0]));
          for (size_t k = 1; k < n - 1; k++)
            directions[k] = angle_to_dir (vector[k], right_side[k]);
          directions[n - 1] = (end_dir != 0) ?
            end_dir : (angle_to_dir (vector[n - 1], right_side[n - 1]));
        }
      else
        *info_message = _("no solution found, in hobby_guess_directions()");

      __HOBBY_ALLOC_FREE (alpha);
      __HOBBY_ALLOC_FREE (beta);
      __HOBBY_ALLOC_FREE (len);
      __HOBBY_ALLOC_FREE (vector);
      __HOBBY_ALLOC_FREE (turn);

      __HOBBY_ALLOC_FREE (subdiag);
      __HOBBY_ALLOC_FREE (diag);
      __HOBBY_ALLOC_FREE (superdiag);
      __HOBBY_ALLOC_FREE (right_side);
    }
}

//-------------------------------------------------------------------------

static void
prepare_periodic_working_data (size_t n, const double a_tensions[n],
                               const double b_tensions[n],
                               const double complex points[n],
                               double alpha[n], double beta[n],
                               double len[n], double complex vector[n],
                               double turn[n])
{
  for (size_t k = 0; k < n; k++)
    {
      const size_t kp1 = (k == n - 1) ? 0 : (k + 1);

      // Reciprocals of the tensions.
      alpha[k] = 1.0 / a_tensions[k];
      beta[k] = 1.0 / b_tensions[k];

      // Point-to-point vectors.
      vector[k] = points[kp1] - points[k];

      // The lengths of those vectors.
      len[k] = cabs (vector[k]);
    }

  for (size_t k = 0; k < n; k++)
    {
      const size_t kp1 = (k == n - 1) ? 0 : (k + 1);

      // Turning angles, in radians.
      turn[k] = turn_angle (vector[k], vector[kp1]);
    }
}

static void
construct_nonsparse_periodic_system (size_t n, const double alpha[n],
                                     const double beta[n], const double len[n],
                                     const double complex vector[n],
                                     const double turn[n], double *A[n])
{
  // Use the equations from Metafont, section 276, but multiplying
  // through by the denominators.
  double *denom_a_b = __HOBBY_ALLOC_ATOMIC (n, double);
  double *denom_c_d = __HOBBY_ALLOC_ATOMIC (n, double);
  for (size_t k = 0; k < n; k++)
    {
      const size_t km1 = minus_one_mod_n (k, n);
      denom_a_b[k] = beta[km1] * beta[km1] * len[km1];
      denom_c_d[k] = alpha[k] * alpha[k] * len[k];
    }

  for (size_t k = 0; k < n; k++)
    {
      const size_t km1 = minus_one_mod_n (k, n);
      const size_t kp1 = plus_one_mod_n (k, n);

      const double a = denom_c_d[k] * alpha[km1];
      const double b = denom_c_d[k] * (3.0 - alpha[km1]);
      const double c = denom_a_b[k] * (3.0 - beta[k]);
      const double d = denom_a_b[k] * beta[k];
      const int vanished = (denom_a_b[k] == 0 && denom_c_d[k] == 0);

      // Periodic subdiagonal entry. (Superposed with the periodic
      // superdiagonal entry, if n equal 2.)
      A[k][km1] += a;

      // Diagonal entry.
      A[k][k] = b + c + vanished;

      // Periodic superdiagonal entry. (Superposed with the periodic
      // subdiagonal entry, if n equal 2.)
      A[k][kp1] += d;

      // Right side.
      A[k][n] = -(b * turn[km1] + d * turn[k]);
    }

  __HOBBY_ALLOC_FREE (denom_a_b);
  __HOBBY_ALLOC_FREE (denom_c_d);
}

static void
solve_nonsparse_periodic_system (size_t n, const double alpha[n],
                                 const double beta[n], const double len[n],
                                 const double complex vector[n],
                                 const double turn[n],
                                 double complex directions[n], int *info,
                                 const char **info_message)
{
  // This function uses O(n**2) storage and runs in O(n**3) time.

  double **A = __HOBBY_ALLOC (n, double);       // A is the augmented matrix.
  for (size_t i = 0; i < n; i++)
    {
      A[i] = __HOBBY_ALLOC_ATOMIC (n + 1, double);
      for (size_t j = 0; j <= n; j++)
        A[i][j] = 0;            // Initialize the row with zeros.
    }
  construct_nonsparse_periodic_system (n, alpha, beta, len, vector, turn, A);

  // Solve the system.
  //
  // dgausselimsolve() overwrites the rightmost column of the A matrix
  // with the solution.
  dgausselimsolve (n, n + 1, A, NULL, info);

  if (*info == 0)
    for (size_t k = 0; k < n; k++)
      directions[k] = angle_to_dir (vector[k], A[k][n]);
  else
    *info_message =
      _("no solution found, in hobby_periodic_guess_directions()");

  for (size_t i = 0; i < n; i++)
    __HOBBY_ALLOC_FREE (A[i]);
  __HOBBY_ALLOC_FREE (A);
}

static void
construct_sparse_periodic_system (size_t n, const double alpha[n],
                                  const double beta[n], const double len[n],
                                  const double complex vector[n],
                                  const double turn[n], double *lower_left,
                                  double subdiag[n - 1], double diag[n],
                                  double superdiag[n - 1], double *upper_right,
                                  double right_side[2][n])
{
  assert (3 <= n);

  // Use the equations from Metafont, section 276, but multiplying
  // through by the denominators.
  double *denom_a_b = __HOBBY_ALLOC_ATOMIC (n, double);
  double *denom_c_d = __HOBBY_ALLOC_ATOMIC (n, double);
  for (size_t k = 0; k < n; k++)
    {
      const size_t km1 = minus_one_mod_n (k, n);
      denom_a_b[k] = beta[km1] * beta[km1] * len[km1];
      denom_c_d[k] = alpha[k] * alpha[k] * len[k];
    }

  for (size_t k = 0; k < n; k++)
    {
      const size_t km1 = minus_one_mod_n (k, n);
      const size_t kp1 = plus_one_mod_n (k, n);

      const double a = denom_c_d[k] * alpha[km1];
      const double b = denom_c_d[k] * (3.0 - alpha[km1]);
      const double c = denom_a_b[k] * (3.0 - beta[k]);
      const double d = denom_a_b[k] * beta[k];
      const int vanished = (denom_a_b[k] == 0 && denom_c_d[k] == 0);

      if (k == 0)
        *upper_right = a;
      else
        subdiag[km1] = a;

      diag[k] = b + c + vanished;

      if (kp1 == 0)
        *lower_left = d;
      else
        superdiag[k] = d;

      right_side[0][k] = -(b * turn[km1] + d * turn[k]);
    }

  __HOBBY_ALLOC_FREE (denom_a_b);
  __HOBBY_ALLOC_FREE (denom_c_d);
}

static void
solve_sparse_periodic_system (size_t n, const double alpha[n],
                              const double beta[n], const double len[n],
                              const double complex vector[n],
                              const double turn[n],
                              double complex directions[n], int *info,
                              const char **info_message)
{
  // This function uses O(n) storage and runs in O(n) time.

  double *subdiag = __HOBBY_ALLOC_ATOMIC (n - 1, double);
  double *diag = __HOBBY_ALLOC_ATOMIC (n, double);
  double *superdiag = __HOBBY_ALLOC_ATOMIC (n - 1, double);
  double (*right_side)[n] = __HOBBY_ALLOC_ATOMIC (2 * n, double);

  double lower_left;
  double upper_right;
  construct_sparse_periodic_system (n, alpha, beta, len, vector, turn,
                                    &lower_left, subdiag, diag, superdiag,
                                    &upper_right, right_side);

  dcyclictridiagsolve (n, 1, subdiag, diag, superdiag, upper_right, lower_left,
                       right_side, info);

  __HOBBY_ALLOC_FREE (subdiag);
  __HOBBY_ALLOC_FREE (diag);
  __HOBBY_ALLOC_FREE (superdiag);

  if (*info == 0)
    for (size_t k = 0; k < n; k++)
      directions[k] = angle_to_dir (vector[k], right_side[0][k]);
  else
    *info_message =
      _("no solution found, in hobby_periodic_guess_directions()");

  __HOBBY_ALLOC_FREE (right_side);
}

static bool
directions_are_finite (size_t n, const double complex directions[n])
{
  size_t i = 0;
  while (i < n && isfinite (creal (directions[n]))
         && isfinite (cimag (directions[n])))
    i++;
  return (i == n);
}

static void
solve_periodic_system (size_t n, const double alpha[n], const double beta[n],
                       const double len[n], const double complex vector[n],
                       const double turn[n], double complex directions[n],
                       int *info, const char **info_message)
{
  if (n == 2)
    // For n == 2 we might as well use our original code (brute force
    // gaussian elimination). (FIXME: One might also work out the
    // solution by hand and use that directly.)
    solve_nonsparse_periodic_system (n, alpha, beta, len, vector, turn,
                                     directions, info, info_message);
  else
    {
      // For n > 2, try newer code that is O(n).
      solve_sparse_periodic_system (n, alpha, beta, len, vector, turn,
                                    directions, info, info_message);
      if (*info == 0 && !directions_are_finite (n, directions))
        // Try brute force gaussian elimination if the sparse method seems
        // likely to have failed. (FIXME: Is this branch of any value
        // whatsoever?)
        solve_nonsparse_periodic_system (n, alpha, beta, len, vector, turn,
                                         directions, info, info_message);
    }
}

VISIBLE void
hobby_periodic_guess_directions (size_t n, const double a_tensions[n],
                                 const double b_tensions[n],
                                 const double complex points[n],
                                 double complex directions[n],
                                 int *info, const char **info_message)
{
  // NOTE: Normally, this implementation uses O(n) storage and runs in
  // O(n) time. As a very unlikely fallback, it may use O(n**2)
  // storage and run in O(n**3) time.

  *info_message = "";

  if (n < 2)
    {
      *info = -1001;
      *info_message =
        _("hobby_periodic_guess_directions() requires at least 2 points");
    }
  else if (INT_MAX - 1 < n)
    {
      // The requirement n <= INT_MAX - 1 is due to
      // gauss_elim_solve__().
      *info = -1001;
      *info_message =
        _("too many points for hobby_periodic_guess_directions()");
    }
  else
    {
      // Reciprocals of a_tensions.
      double *alpha = __HOBBY_ALLOC_ATOMIC (n, double);
      // Reciprocals of b_tensions.
      double *beta = __HOBBY_ALLOC_ATOMIC (n, double);
      // The lengths of point-to-point vectors.
      double *len = __HOBBY_ALLOC_ATOMIC (n, double);
      // Point-to-point vectors.
      double complex *vector = __HOBBY_ALLOC_ATOMIC (n, double complex);
      // Turning angles, in radians.
      double *turn = __HOBBY_ALLOC_ATOMIC (n, double);
      prepare_periodic_working_data (n, a_tensions, b_tensions, points, alpha,
                                     beta, len, vector, turn);

      // Because the piecewise spline is periodic, the problem is a
      // periodic tridiagonal system of n linear equations.
      solve_periodic_system (n, alpha, beta, len, vector, turn, directions,
                             info, info_message);

      __HOBBY_ALLOC_FREE (alpha);
      __HOBBY_ALLOC_FREE (beta);
      __HOBBY_ALLOC_FREE (len);
      __HOBBY_ALLOC_FREE (vector);
      __HOBBY_ALLOC_FREE (turn);
    }
}

//-------------------------------------------------------------------------

#define _HOBBY_DEFAULT_CURL 1.0

typedef enum
{
  _HOBBY_RECORD_POINT,
  _HOBBY_RECORD_TENSION,
  _HOBBY_RECORD_CTRL
} _hobby_record_tag;

typedef union
{
  // IMPORTANT: ‘_hobby_record_tag tag;’ must come first in each
  // variant.

  struct
  {
    _hobby_record_tag tag;
  } tag;

  struct
  {
    _hobby_record_tag tag;
    bool is_cycle:1;
    complex double p;
  } point;

  struct
  {
    _hobby_record_tag tag;
    bool is_atleast:1;
    bool has_ldir:1;
    bool has_rdir:1;
    bool has_lcurl:1;
    bool has_rcurl:1;
    complex double ldircurl;
    complex double rdircurl;
    double ltension;
    double rtension;
  } tension;

  struct
  {
    _hobby_record_tag tag;
    complex double lctrl;
    complex double rctrl;
  } ctrl;
} _hobby_record;

static inline bool
hobby_guide_token_is_pointlike (double token)
{
  return (token == HOBBY_GUIDE_POINT || token == HOBBY_GUIDE_CYCLE);
}

static inline bool
hobby_guide_token_is_dirlike (double token)
{
  return (token == HOBBY_GUIDE_DIR || token == HOBBY_GUIDE_CURL);
}

static inline bool
hobby_guide_token_is_tension_like (double token)
{
  return (token == HOBBY_GUIDE_TENSION || token == HOBBY_GUIDE_ATLEAST);
}

static inline bool
hobby_guide_token_is_tension_related (double token)
{
  return (hobby_guide_token_is_dirlike (token)
          || hobby_guide_token_is_tension_like (token));
}

static const char *
point_must_be_last (void)
{
  return
    _("HOBBY_GUIDE_POINT or HOBBY_GUIDE_CYCLE must appear as the last token");
}

static void
read_point (size_t n, const double tokens[3 * n], size_t *i,
            _hobby_record *record, int *info, const char **info_message,
            size_t *bad_token_index)
{
  if (tokens[3 * *i] == HOBBY_GUIDE_POINT)
    {
      record->point.tag = _HOBBY_RECORD_POINT;
      record->point.is_cycle = false;
      record->point.p = tokens[(3 * *i) + 1] + tokens[(3 * *i) + 2] * I;
      *i += 1;
    }
  else if (tokens[3 * *i] == HOBBY_GUIDE_CYCLE)
    {
      if (*i != n - 1)
        {
          *info = HOBBY_GUIDE_TOKEN_ERROR;
          *info_message = _("HOBBY_GUIDE_CYCLE appears before the last token");
          *bad_token_index = *i;
        }
      else
        {
          record->point.tag = _HOBBY_RECORD_POINT;
          record->point.is_cycle = true;
          *i += 1;
        }
    }
  else
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = _("expected HOBBY_GUIDE_POINT or HOBBY_GUIDE_CYCLE");
      *bad_token_index = *i;
    }
}

static void
read_tension3 (size_t n, const double tokens[3 * n], size_t *i,
               _hobby_record *record, int *info, const char **info_message,
               size_t *bad_token_index)
{
  if (*i == n - 1)
    {
      if (!hobby_guide_token_is_pointlike (tokens[3 * *i]))
        {
          *info = HOBBY_GUIDE_TOKEN_ERROR;
          *info_message = point_must_be_last ();
          *bad_token_index = *i;
        }
    }
  else if (tokens[3 * *i] == HOBBY_GUIDE_DIR)
    {
      const double complex dir =
        tokens[(3 * *i) + 1] + tokens[(3 * *i) + 2] * I;
      if (dir != 0)
        {
          record->tension.has_rdir = true;
          record->tension.rdircurl = dir;
        }
      *i += 1;
    }
  else if (tokens[3 * *i] == HOBBY_GUIDE_CURL)
    {
      record->tension.has_rcurl = true;
      record->tension.rdircurl = tokens[(3 * *i) + 1];
      *i += 1;
    }
}

static void
read_tension2 (size_t n, const double tokens[3 * n], size_t *i,
               _hobby_record *record, int *info, const char **info_message,
               size_t *bad_token_index)
{
  if (*i == n - 1)
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = point_must_be_last ();
      *bad_token_index = *i;
    }
  else if (hobby_guide_token_is_tension_like (tokens[3 * *i]))
    {
      record->tension.is_atleast = (tokens[3 * *i] == HOBBY_GUIDE_ATLEAST);
      record->tension.ltension = tokens[(3 * *i) + 1];
      record->tension.rtension = tokens[(3 * *i) + 2];
      *i += 1;
      read_tension3 (n, tokens, i, record, info, info_message, bad_token_index);
    }
  else
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = _("expected HOBBY_GUIDE_TENSION or HOBBY_GUIDE_ATLEAST");
      *bad_token_index = *i;
    }
}

static void
read_tension (size_t n, const double tokens[3 * n], size_t *i,
              _hobby_record *record, int *info, const char **info_message,
              size_t *bad_token_index)
{
  assert (hobby_guide_token_is_tension_related (tokens[3 * *i]));

  record->tension.tag = _HOBBY_RECORD_TENSION;
  record->tension.is_atleast = false;
  record->tension.has_ldir = false;
  record->tension.has_lcurl = false;
  record->tension.has_rdir = false;
  record->tension.has_rcurl = false;

  if (*i == n - 1)
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = point_must_be_last ();
      *bad_token_index = *i;
    }
  else if (tokens[3 * *i] == HOBBY_GUIDE_DIR)
    {
      const double complex dir =
        tokens[(3 * *i) + 1] + tokens[(3 * *i) + 2] * I;
      if (dir != 0)
        {
          record->tension.has_ldir = true;
          record->tension.ldircurl = dir;
        }
      *i += 1;
      read_tension2 (n, tokens, i, record, info, info_message, bad_token_index);
    }
  else if (tokens[3 * *i] == HOBBY_GUIDE_CURL)
    {
      record->tension.has_lcurl = true;
      record->tension.ldircurl = tokens[(3 * *i) + 1];
      *i += 1;
      read_tension2 (n, tokens, i, record, info, info_message, bad_token_index);
    }
  else if (hobby_guide_token_is_tension_like (tokens[3 * *i]))
    {
      record->tension.is_atleast = (tokens[3 * *i] == HOBBY_GUIDE_ATLEAST);
      record->tension.ltension = tokens[(3 * *i) + 1];
      record->tension.rtension = tokens[(3 * *i) + 2];
      *i += 1;
      read_tension3 (n, tokens, i, record, info, info_message, bad_token_index);
    }
}

static void
read_ctrl (size_t n, const double tokens[3 * n], size_t *i,
           _hobby_record *record, int *info, const char **info_message,
           size_t *bad_token_index)
{
  assert (tokens[3 * *i] == HOBBY_GUIDE_CTRL);
  if (*i == n - 1)
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = point_must_be_last ();
      *bad_token_index = *i;
    }
  else if (tokens[3 * (*i + 1)] != HOBBY_GUIDE_CTRL)
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = _("HOBBY_GUIDE_CTRL tokens must appear in pairs");
      *bad_token_index = *i + 1;
    }
  else if ((*i + 1) == n - 1)
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = point_must_be_last ();
      *bad_token_index = *i + 1;
    }
  else
    {
      record->ctrl.tag = _HOBBY_RECORD_CTRL;
      record->ctrl.lctrl = tokens[(3 * *i) + 1] + tokens[(3 * *i) + 2] * I;
      record->ctrl.rctrl = tokens[(3 * *i) + 4] + tokens[(3 * *i) + 5] * I;
      *i += 2;
    }
}

static inline void
add_hobby_record (_hobby_record record, int info,
                  size_t *record_count, _hobby_record records[])
{
  if (info == 0)
    {
      records[*record_count] = record;
      *record_count += 1;
    }
}

static inline bool
record_has_no_direction_on_left (_hobby_record record)
{
  return (!record.tension.has_ldir && !record.tension.has_lcurl);
}

static inline bool
record_has_no_direction_on_right (_hobby_record record)
{
  return (!record.tension.has_rdir && !record.tension.has_rcurl);
}


static void
fill_in_left_curl (_hobby_record records[], size_t i)
{
  switch (records[i].tag.tag)
    {
    case _HOBBY_RECORD_TENSION:
      if (record_has_no_direction_on_left (records[i]))
        {
          records[i].tension.has_lcurl = true;
          records[i].tension.ldircurl = _HOBBY_DEFAULT_CURL;
        }
      break;
    case _HOBBY_RECORD_CTRL:
      // Do nothing.
      break;
    default:
      assert (false);
      break;
    }
}

static void
fill_in_right_curl (_hobby_record records[], size_t i)
{
  switch (records[i].tag.tag)
    {
    case _HOBBY_RECORD_TENSION:
      if (record_has_no_direction_on_right (records[i]))
        {
          records[i].tension.has_rcurl = true;
          records[i].tension.rdircurl = _HOBBY_DEFAULT_CURL;
        }
      break;
    case _HOBBY_RECORD_CTRL:
      // Do nothing.
      break;
    default:
      assert (false);
      break;
    }
}

static void
fill_in_tension_pair (_hobby_record records[], size_t i, size_t j)
{
  if (records[i].tension.has_rdir)
    {
      if (record_has_no_direction_on_left (records[j]))
        {
          // Copy the direction so it is the same on both sides of the
          // surrounded point.
          records[j].tension.has_ldir = true;
          records[j].tension.ldircurl = records[i].tension.rdircurl;
        }
    }
  else if (records[i].tension.has_rcurl)
    {
      if (record_has_no_direction_on_left (records[j]))
        {
          records[j].tension.has_lcurl = true;
          records[j].tension.ldircurl = _HOBBY_DEFAULT_CURL;
        }
    }
  else
    {
      if (records[j].tension.has_ldir)
        {
          // Copy the direction so it is the same on both sides of the
          // surrounded point.
          records[i].tension.has_rdir = true;
          records[i].tension.rdircurl = records[j].tension.ldircurl;
        }
      else if (records[j].tension.has_lcurl)
        {
          records[i].tension.has_rcurl = true;
          records[i].tension.rdircurl = _HOBBY_DEFAULT_CURL;
        }
    }
}

static void
fill_in_direction_pair (_hobby_record records[], size_t i, size_t j)
{
  switch (records[i].tag.tag)
    {
    case _HOBBY_RECORD_TENSION:
      switch (records[j].tag.tag)
        {
        case _HOBBY_RECORD_TENSION:
          fill_in_tension_pair (records, i, j);
          break;
        case _HOBBY_RECORD_CTRL:
          fill_in_right_curl (records, i);
          break;
        default:
          assert (false);
          break;
        }
      break;
    case _HOBBY_RECORD_CTRL:
      switch (records[j].tag.tag)
        {
        case _HOBBY_RECORD_TENSION:
          fill_in_left_curl (records, j);
          break;
        case _HOBBY_RECORD_CTRL:
          // Do nothing.
          break;
        default:
          assert (false);
          break;
        }
      break;
    default:
      assert (false);
      break;
    }
}

static void
fill_in_implied_dirs_and_curls (size_t record_count,
                                _hobby_record records[record_count])
{
  if (3 <= record_count)
    {
      for (size_t i = 1; i < record_count - 3; i += 2)
        fill_in_direction_pair (records, i, i + 2);

      assert (records[record_count - 1].tag.tag == _HOBBY_RECORD_POINT);
      if (records[record_count - 1].point.is_cycle)
        fill_in_direction_pair (records, record_count - 2, 1);
      else
        {
          fill_in_left_curl (records, 1);
          fill_in_right_curl (records, record_count - 2);
        }
    }
}

static void
make_records (size_t n, const double tokens[3 * n],
              size_t *record_count, _hobby_record records[n],
              int *info, const char **info_message, size_t *bad_token_index)
{
  assert (0 < n);
  *info = 0;
  *info_message = "";
  *record_count = 0;
  if (tokens[0] != HOBBY_GUIDE_POINT)
    {
      *info = HOBBY_GUIDE_TOKEN_ERROR;
      *info_message = _("the first token must be HOBBY_GUIDE_POINT");
      *bad_token_index = 0;
    }
  else
    {
      size_t i = 0;
      while (*info == 0 && i < n)
        {
          _hobby_record record;
          read_point (n, tokens, &i, &record, info, info_message,
                      bad_token_index);
          add_hobby_record (record, *info, record_count, records);
          if (*info == 0 && i != n)
            {
              if (hobby_guide_token_is_tension_related (tokens[3 * i]))
                {
                  read_tension (n, tokens, &i, &record, info, info_message,
                                bad_token_index);
                  add_hobby_record (record, *info, record_count, records);
                }
              else if (tokens[3 * i] == HOBBY_GUIDE_CTRL)
                {
                  read_ctrl (n, tokens, &i, &record, info, info_message,
                             bad_token_index);
                  add_hobby_record (record, *info, record_count, records);
                }
              else
                {
                  *info = HOBBY_GUIDE_TOKEN_ERROR;
                  *info_message =
                    _("expected a tension or off-curve control points");
                  *bad_token_index = i;
                }
            }
        }
    }
  if (*info == 0)
    fill_in_implied_dirs_and_curls (*record_count, records);
}

static void
periodic_tridiagonal_records_to_solution (size_t record_count,
                                          const _hobby_record
                                          records[record_count],
                                          size_t n_input, size_t *n_output,
                                          double output_tokens[3 * n_input],
                                          int *info, const char **info_message)
{
  *info = 0;
  *info_message = "";

  const size_t n = record_count / 2;

  if (n == 1)
    {
      double complex p = records[0].point.p;
      *n_output = 4;
      output_tokens[0] = HOBBY_GUIDE_POINT;
      output_tokens[1] = creal (p);
      output_tokens[2] = cimag (p);
      output_tokens[3] = HOBBY_GUIDE_CTRL;
      output_tokens[4] = creal (p);
      output_tokens[5] = cimag (p);
      output_tokens[6] = HOBBY_GUIDE_CTRL;
      output_tokens[7] = creal (p);
      output_tokens[8] = cimag (p);
      output_tokens[9] = HOBBY_GUIDE_CYCLE;
      output_tokens[10] = 0;
      output_tokens[11] = 0;
    }
  else
    {
      double *a_tensions = __HOBBY_ALLOC_ATOMIC (n, double);
      double *b_tensions = __HOBBY_ALLOC_ATOMIC (n, double);
      double complex *points = __HOBBY_ALLOC_ATOMIC (n, double complex);
      double complex *directions = __HOBBY_ALLOC_ATOMIC (n, double complex);

      for (size_t i = 0; i < n; i++)
        {
          a_tensions[i] = records[(2 * i) + 1].tension.ltension;
          b_tensions[i] = records[(2 * i) + 1].tension.rtension;
          points[i] = records[2 * i].point.p;
        }

      int solution_info;
      hobby_periodic_guess_directions (n, a_tensions, b_tensions, points,
                                       directions, &solution_info,
                                       info_message);

      if (solution_info != 0)
        *info = HOBBY_GUIDE_SOLUTION_NOT_FOUND;
      else
        {
          *n_output = 0;
          for (size_t i = 0; i < n; i++)
            {
              const bool is_atleast = records[(2 * i) + 1].tension.is_atleast;
              const double complex p0 = points[i];
              const double complex dir0 = directions[i];
              const double tension0 = a_tensions[i];
              const double tension3 = b_tensions[i];
              const double complex dir3 = directions[(i + 1) % n];
              const double complex p3 = points[(i + 1) % n];
              double complex p1;
              double complex p2;
              hobby_tensions_to_control_points (is_atleast, p0, dir0, tension0,
                                                tension3, dir3, p3, &p1, &p2);
              output_tokens[3 * *n_output] = HOBBY_GUIDE_POINT;
              output_tokens[(3 * *n_output) + 1] = creal (p0);
              output_tokens[(3 * *n_output) + 2] = cimag (p0);
              *n_output += 1;
              output_tokens[3 * *n_output] = HOBBY_GUIDE_CTRL;
              output_tokens[(3 * *n_output) + 1] = creal (p1);
              output_tokens[(3 * *n_output) + 2] = cimag (p1);
              *n_output += 1;
              output_tokens[3 * *n_output] = HOBBY_GUIDE_CTRL;
              output_tokens[(3 * *n_output) + 1] = creal (p2);
              output_tokens[(3 * *n_output) + 2] = cimag (p2);
              *n_output += 1;
            }
          output_tokens[3 * *n_output] = HOBBY_GUIDE_CYCLE;
          output_tokens[(3 * *n_output) + 1] = 0;
          output_tokens[(3 * *n_output) + 2] = 0;
          *n_output += 1;
        }

      __HOBBY_ALLOC_FREE (a_tensions);
      __HOBBY_ALLOC_FREE (b_tensions);
      __HOBBY_ALLOC_FREE (points);
      __HOBBY_ALLOC_FREE (directions);
    }
}

static void
find_last_point_of_subcurve (const _hobby_record records[], size_t i,
                             size_t modulus, size_t *i_last,
                             size_t *number_of_points)
{
  assert (records[i].tag.tag == _HOBBY_RECORD_POINT);
  assert (records[i + 1].tag.tag != _HOBBY_RECORD_POINT);
  size_t n = 2;                 /* Two, for the start and end points
                                   of the subcurve. */
  if (records[i + 1].tag.tag == _HOBBY_RECORD_TENSION)
    {
      assert (!record_has_no_direction_on_left (records[i + 1]));
      while (record_has_no_direction_on_right (records[i + 1]))
        {
          n += 1;
          i = (i + 2) % modulus;
          assert (records[i].tag.tag == _HOBBY_RECORD_POINT);
          assert (records[i + 1].tag.tag == _HOBBY_RECORD_TENSION);
        }
    }
  assert (records[(i + 2) % modulus].tag.tag == _HOBBY_RECORD_POINT);
  *i_last = (i + 2) % modulus;
  *number_of_points = n;
}

static bool
subcurve_is_solved (const _hobby_record records[], size_t i)
{
  return (records[i + 1].tag.tag == _HOBBY_RECORD_CTRL);
}

static bool
subcurve_is_curl_curl (const _hobby_record records[], size_t i)
{
  return (records[i + 1].tag.tag == _HOBBY_RECORD_TENSION
          && records[i + 1].tension.has_lcurl
          && records[i + 1].tension.has_rcurl);
}

static void
solve_tridiagonal_subcurve (_hobby_record records[], size_t number_of_points,
                            size_t i_first, size_t i_last, size_t modulus,
                            int *info, const char **info_message)
{
  const size_t n = number_of_points;
  double complex start_dir;
  double complex end_dir;
  double start_curl;
  double end_curl;
  double *a_tensions = __HOBBY_ALLOC_ATOMIC (n - 1, double);
  double *b_tensions = __HOBBY_ALLOC_ATOMIC (n - 1, double);
  double complex *points = __HOBBY_ALLOC_ATOMIC (n, double complex);
  double complex *directions = __HOBBY_ALLOC_ATOMIC (n, double complex);

  assert (records[i_first + 1].tag.tag == _HOBBY_RECORD_TENSION);
  if (records[i_first + 1].tension.has_lcurl)
    {
      start_dir = 0;
      start_curl = records[i_first + 1].tension.ldircurl;
    }
  else
    {
      assert (records[i_first + 1].tension.has_ldir);
      start_dir = records[i_first + 1].tension.ldircurl;
      start_curl = -9999;       /* A weird unused value, to help in
                                   debugging. */
    }

  const size_t i_last_minus_1 = (i_last == 0) ? (modulus - 1) : (i_last - 1);
  if (records[i_last_minus_1].tension.has_rcurl)
    {
      end_dir = 0;
      end_curl = records[i_last_minus_1].tension.rdircurl;
    }
  else
    {
      assert (records[i_last_minus_1].tension.has_rdir);
      end_dir = records[i_last_minus_1].tension.rdircurl;
      end_curl = -9999;         /* A weird unused value, to help in
                                   debugging. */
    }

  size_t i_record = i_first;
  for (size_t i = 0; i < number_of_points - 1; i++)
    {
      assert (records[i_record + 1].tag.tag == _HOBBY_RECORD_TENSION);
      a_tensions[i] = records[i_record + 1].tension.ltension;
      b_tensions[i] = records[i_record + 1].tension.rtension;
      i_record = (i_record + 2) % modulus;
    }

  i_record = i_first;
  for (size_t i = 0; i < number_of_points; i++)
    {
      assert (records[i_record].tag.tag == _HOBBY_RECORD_POINT);
      assert (!records[i_record].point.is_cycle);
      points[i] = records[i_record].point.p;
      i_record = (i_record + 2) % modulus;
    }

  hobby_guess_directions (start_dir, end_dir, start_curl, end_curl, n,
                          a_tensions, b_tensions, points, directions, info,
                          info_message);

  i_record = i_first;
  if (*info == 0)
    for (size_t i = 0; i < number_of_points - 1; i++)
      {
        assert (records[i_record + 1].tag.tag == _HOBBY_RECORD_TENSION);
        const bool is_atleast = records[i_record + 1].tension.is_atleast;
        const double complex p0 = points[i];
        const double complex dir0 = directions[i];
        const double tension0 = a_tensions[i];
        const double tension3 = b_tensions[i];
        const double complex dir3 = directions[(i + 1) % n];
        const double complex p3 = points[(i + 1) % n];
        double complex p1;
        double complex p2;
        hobby_tensions_to_control_points (is_atleast, p0, dir0, tension0,
                                          tension3, dir3, p3, &p1, &p2);
        records[i_record + 1].tag.tag = _HOBBY_RECORD_CTRL;
        records[i_record + 1].ctrl.lctrl = p1;
        records[i_record + 1].ctrl.rctrl = p2;

        i_record = (i_record + 2) % modulus;
      }
}

static void
solve_subcurve (_hobby_record records[], size_t number_of_points,
                size_t i_first, size_t i_last, size_t modulus, int *info,
                const char **info_message)
{
  *info = 0;
  *info_message = "";
  if (!subcurve_is_solved (records, i_first))
    {
      if (subcurve_is_curl_curl (records, i_first))
        {
          // ‘Solve’ the subcurve as a straight line. (Metafont
          // section 283: ‘The simplest case occurs when n=1 and there
          // is a curl at both breakpoints; then we simply draw a
          // straight line.’)
          records[i_first + 1].ctrl.tag = _HOBBY_RECORD_CTRL;
          records[i_first + 1].ctrl.lctrl = records[i_first].point.p;
          records[i_first + 1].ctrl.rctrl =
            records[(i_first + 2) % modulus].point.p;
        }
      else
        solve_tridiagonal_subcurve (records, number_of_points, i_first, i_last,
                                    modulus, info, info_message);
    }
}

static void
solve_records (_hobby_record records[], size_t first, size_t last,
               size_t modulus, int *info, const char **info_message)
{
  size_t i = first;
  do
    {
      size_t j;
      size_t number_of_points;
      find_last_point_of_subcurve (records, i, modulus, &j, &number_of_points);
      solve_subcurve (records, number_of_points, i, j, modulus, info,
                      info_message);
      i = j;
    }
  while (*info == 0 && i != last);
}

static void
find_first_and_last_points (size_t record_count,
                            const _hobby_record records[record_count],
                            size_t *first, size_t *last, size_t *modulus,
                            bool *is_cycle, bool *is_periodic_tridiagonal)
{
  assert (record_count % 2 == 1);       // There should be an odd number.
  assert (records[0].tag.tag == _HOBBY_RECORD_POINT);
  assert (!records[0].point.is_cycle);
  assert (records[1].tag.tag != _HOBBY_RECORD_POINT);

  if (record_count == 1)
    {
      *first = 0;
      *last = 0;
      *modulus = 1;
      *is_cycle = false;
      *is_periodic_tridiagonal = false;
    }
  else
    {
      size_t i = 0;
      while (!records[i].point.is_cycle
             && records[i + 1].tag.tag == _HOBBY_RECORD_TENSION
             && record_has_no_direction_on_left (records[i + 1]))
        {
          i += 2;
          assert (i < record_count);
          assert (records[i].tag.tag == _HOBBY_RECORD_POINT);
          assert (records[i + 1].tag.tag != _HOBBY_RECORD_POINT);
        }
      if (records[i].point.is_cycle)
        {
          *first = 0;
          *last = 0;
          *modulus = record_count - 1;
          *is_cycle = true;
          *is_periodic_tridiagonal = true;
        }
      else
        {
          assert (records[record_count - 1].tag.tag == _HOBBY_RECORD_POINT);
          *is_cycle = records[record_count - 1].point.is_cycle;
          if (*is_cycle)
            {
              *first = i;
              *last = i;
              *modulus = record_count - 1;
              *is_cycle = true;
              *is_periodic_tridiagonal = false;
            }
          else
            {
              assert (i == 0);
              *first = 0;
              *last = record_count - 1;
              *modulus = record_count;
              *is_cycle = false;
              *is_periodic_tridiagonal = false;
            }
        }
    }
}

static void
solved_records_to_output_tokens (size_t record_count,
                                 const _hobby_record records[record_count],
                                 size_t *n_output, double output_tokens[])
{
  *n_output = 0;
  for (size_t i = 0; i < record_count - 1; i += 2)
    {
      assert (records[i].tag.tag == _HOBBY_RECORD_POINT);
      assert (records[i + 1].tag.tag == _HOBBY_RECORD_CTRL);
      assert (records[i + 2].tag.tag == _HOBBY_RECORD_POINT);
      output_tokens[3 * *n_output] = HOBBY_GUIDE_POINT;
      output_tokens[(3 * *n_output) + 1] = creal (records[i].point.p);
      output_tokens[(3 * *n_output) + 2] = cimag (records[i].point.p);
      *n_output += 1;
      output_tokens[3 * *n_output] = HOBBY_GUIDE_CTRL;
      output_tokens[(3 * *n_output) + 1] = creal (records[i + 1].ctrl.lctrl);
      output_tokens[(3 * *n_output) + 2] = cimag (records[i + 1].ctrl.lctrl);
      *n_output += 1;
      output_tokens[3 * *n_output] = HOBBY_GUIDE_CTRL;
      output_tokens[(3 * *n_output) + 1] = creal (records[i + 1].ctrl.rctrl);
      output_tokens[(3 * *n_output) + 2] = cimag (records[i + 1].ctrl.rctrl);
      *n_output += 1;
    }
  assert (records[record_count - 1].tag.tag == _HOBBY_RECORD_POINT);
  output_tokens[3 * *n_output] =
    (records[record_count - 1].point.is_cycle) ?
    HOBBY_GUIDE_CYCLE : HOBBY_GUIDE_POINT;
  output_tokens[(3 * *n_output) + 1] =
    creal (records[record_count - 1].point.p);
  output_tokens[(3 * *n_output) + 2] =
    cimag (records[record_count - 1].point.p);
  *n_output += 1;
}

static void
records_to_solution (size_t record_count, _hobby_record records[record_count],
                     size_t n_input, size_t *n_output,
                     double output_tokens[3 * n_input], int *info,
                     const char **info_message)
{
  if (record_count == 1)
    {
      // Just a single point.
      assert (records[0].tag.tag == _HOBBY_RECORD_POINT);
      assert (!records[0].point.is_cycle);
      *n_output = 1;
      output_tokens[0] = HOBBY_GUIDE_POINT;
      output_tokens[1] = creal (records[0].point.p);
      output_tokens[2] = cimag (records[0].point.p);
      *info = 0;
      *info_message = "";
    }
  else
    {
      size_t first;
      size_t last;
      size_t modulus;
      bool is_cycle;
      bool is_periodic_tridiagonal;
      find_first_and_last_points (record_count, records,
                                  &first, &last, &modulus,
                                  &is_cycle, &is_periodic_tridiagonal);
      if (is_periodic_tridiagonal)
        periodic_tridiagonal_records_to_solution (record_count, records,
                                                  n_input, n_output,
                                                  output_tokens, info,
                                                  info_message);
      else
        {
          solve_records (records, first, last, modulus, info, info_message);
          if (*info == 0)
            solved_records_to_output_tokens (record_count, records, n_output,
                                             output_tokens);
        }
    }
}

// FIXME: The output_tokens buffer can be smaller, although I am not
// at all certain it is worth the bother to be more exact.
VISIBLE void
solve_hobby_guide_tokens (size_t n_input,
                          const double input_tokens[3 * n_input],
                          size_t *n_output,
                          double output_tokens[(9 * n_input) / 2],
                          int *info, const char **info_message,
                          size_t *bad_token_index)
{
  *n_output = 0;
  *info = 0;
  *info_message = "";

  if (n_input != 0)
    {
      _hobby_record *records = __HOBBY_ALLOC_ATOMIC (n_input, _hobby_record);

      size_t record_count;
      make_records (n_input, input_tokens, &record_count, records, info,
                    info_message, bad_token_index);

      if (*info == 0)
        records_to_solution (record_count, records, n_input, n_output,
                             output_tokens, info, info_message);
      assert (*n_output <= (9 * n_input) / 2);

      __HOBBY_ALLOC_FREE (records);
    }
}

//-------------------------------------------------------------------------
