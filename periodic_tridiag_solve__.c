#include <config.h>

/*
// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <sortsmill/core/tridiagsolve.h>

#ifndef PERIODIC_TRIDIAG_REAL
#define PERIODIC_TRIDIAG_REAL double
#endif

#ifndef PERIODIC_TRIDIAG_TRIDIAGSOLVE
#define PERIODIC_TRIDIAG_TRIDIAGSOLVE dtridiagsolve
#endif

#ifndef PERIODIC_TRIDIAG_FUNC
#define PERIODIC_TRIDIAG_FUNC dcyclictridiagsolve
#endif

static PERIODIC_TRIDIAG_REAL
choose_gamma (int n, PERIODIC_TRIDIAG_REAL subdiag[n - 1],
              PERIODIC_TRIDIAG_REAL diag[n],
              PERIODIC_TRIDIAG_REAL superdiag[n - 1])
{
  // The choice gamma = -diag[0] avoids loss of precision due to the
  // subtraction diag[0] - gamma. It will not work, however, if
  // diag[0] is zero; in that case, arbitrarily choose gamma = 1.
  PERIODIC_TRIDIAG_REAL gamma = (diag[0] == 0) ? 1 : (-diag[0]);

#if 1

  // FIXME: Is there any reason to include the following code? We are
  // merely merely sometimes halving or doubling gamma, while always
  // retaining its sign.

  // FIXME: Someone please explain to me what is going on here. I am
  // not sure what this adjustment achieves, but GSL version 1.16 does
  // it. (The tridiagonal solver in GSL 1.16 employs Cholesky
  // decomposition; is this adjustment specific to Cholesky
  // decomposition and/or to positive definite matrices?)
  const PERIODIC_TRIDIAG_REAL q =
    1 - (subdiag[0] * superdiag[0]) / (diag[0] * diag[1]);
  const PERIODIC_TRIDIAG_REAL abs_q_over_gamma = fabs (q / gamma);
  if (0.5 < abs_q_over_gamma && abs_q_over_gamma < 2)
    {
      if (abs_q_over_gamma < 1)
        {
          gamma *= 0.5;
          if (gamma == 0)
            gamma = 1;          /* FIXME: Can we do better? GSL 1.16
                                   does not take this step. */
        }
      else
        gamma *= 2;
    }

#endif

  // FIXME: Perhaps make an adjustment to ensure that diag[n - 1] -
  // (lower_left * upper_right) / gamma does not lose much precision.

  return gamma;
}

VISIBLE void
PERIODIC_TRIDIAG_FUNC (int n, int nrhs, PERIODIC_TRIDIAG_REAL subdiag[n - 1],
                       PERIODIC_TRIDIAG_REAL diag[n],
                       PERIODIC_TRIDIAG_REAL superdiag[n - 1],
                       PERIODIC_TRIDIAG_REAL lower_left,
                       PERIODIC_TRIDIAG_REAL upper_right,
                       PERIODIC_TRIDIAG_REAL rhs_and_workspace[nrhs + 1][n],
                       int *info)
{
  // Solution by the Sherman-Morrison formula: treat the problem as a
  // perturbed tridiagonal system. Solve the tridiagonal system and
  // then apply a correction.

  assert (3 <= n);              // FIXME: We may wish to loosen this restriction.
  assert (nrhs <= INT_MAX - 1);

  // The first nrhs rows are the (transposes of) the right hand sides,
  // and will contain the (transposes of) the solutions.
  PERIODIC_TRIDIAG_REAL (*const b)[n] = &rhs_and_workspace[0];

  // The last row is workspace.
  PERIODIC_TRIDIAG_REAL *const u = rhs_and_workspace[nrhs];

  const PERIODIC_TRIDIAG_REAL gamma =
    choose_gamma (n, subdiag, diag, superdiag);

  // Solve a tridiagonal matrix that is the same as the original
  // matrix, except with the lower-left and upper-right entries set to
  // zero, and with the following changes on the diagonal.
  diag[0] -= gamma;
  diag[n - 1] -= (lower_left * upper_right) / gamma;

  // There will be an additional right-hand side, for the (scaled)
  // correction vector.
  u[0] = gamma;
  for (int i = 1; i < n - 1; i++)
    u[i] = 0;
  u[n - 1] = lower_left;

  // Simultaneously solve all the Ay = b, and also Az = u. The y
  // vectors overwrite the b vectors, and the z vector overwrites u.
  PERIODIC_TRIDIAG_TRIDIAGSOLVE (n, 2, subdiag, diag, superdiag,
                                 (PERIODIC_TRIDIAG_REAL *) rhs_and_workspace,
                                 n, info);

  if (*info == 0)
    {
      // The solutions x are the y vectors plus a correction based on
      // z. The x vectors end up in b (replacing the y vectors that
      // were there).
      const PERIODIC_TRIDIAG_REAL vz = u[0] + (upper_right * u[n - 1]) / gamma;
      const PERIODIC_TRIDIAG_REAL one_plus_vz = 1 + vz;
      int j = 0;
      while (*info == 0 && j < nrhs)
        {
          const PERIODIC_TRIDIAG_REAL vy =
            b[j][0] + (upper_right * b[j][n - 1]) / gamma;
          int i = 0;
          while (*info == 0 && i < n)
            {
              const PERIODIC_TRIDIAG_REAL numer = vy * u[i];
              if (numer == 0 && one_plus_vz == 0)
                *info = -9999;  // Zero over zero singularity.
              else
                b[j][i] -= numer / one_plus_vz;
              i++;
            }
          j++;
        }
    }
}
