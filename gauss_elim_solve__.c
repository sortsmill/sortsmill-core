#include <config.h>

/*
// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <math.h>
#include <float.h>

////////////////////////////////////////////////////////////////////////
//                                                                    //
// FIXME FIXME FIXME: Is the ‘tolerance’ parameter needed or useful?  //
//                                                                    //
// FIXME FIXME FIXME: Is the ‘signum’ parameter useful?               //
//                                                                    //
////////////////////////////////////////////////////////////////////////

#ifndef GAUSSELIMSOLVE_REAL
#define GAUSSELIMSOLVE_REAL double
#endif

static inline int
select_pivot (int m, GAUSSELIMSOLVE_REAL *A[m], int i)
{
  // Search the ith column for a pivot entry with maximum magnitude.

  int pivot = i;
  for (int k = i + 1; k < m; k++)
    if (fabs (A[pivot][i]) < fabs (A[k][i]))
      pivot = k;
  return pivot;
}

static inline void
pivot_row_apply (int m, int n, GAUSSELIMSOLVE_REAL *A[m], int pivot)
{
  const GAUSSELIMSOLVE_REAL pivot_val = A[pivot][pivot];

  // Perform operations on the rows below the pivot.
  for (int j = pivot + 1; j < m; j++)
    {
      const GAUSSELIMSOLVE_REAL A_j_pivot = A[j][pivot];
      A[j][pivot] = 0;
      for (int k = pivot + 1; k < n; k++)
        // FIXME: Is this a good expression for stability?
        A[j][k] = (pivot_val * A[j][k] - A_j_pivot * A[pivot][k]) / pivot_val;
    }
}

// Straightforward gaussian elimination with partial pivoting.
//
//    tolerance = if tolerance >= 0:
//                   how close a pivot has to be to zero before
//                   a matrix is considered too close to singular;
//                   in particular, if tolerance = 0, processing
//                   stops only if no non-zero pivot is found
//                if tolerance < 0:
//                   a default non-zero tolerance is used
//                   (currently 100 * DBL_EPSILON).
//
//    m = the number of rows (0 <= m).
//
//    n = the number of columns (m <= n).
//
//    A = a vector pointing to the rows of an m-by-n matrix
//        on input:  the augmented matrix on which to do
//                   gaussian elimination
//        on output: the augmented matrix in an un-normalized
//                   row-echelon form (diagonal entries possibly
//                   not equal to one). The vector A contains a
//                   permutation of the original vector.
//
//    *signum = the sign of the permutation
//            = (-1)**(number of row interchanges).
//
//    *info = 0: successful exit
//          < 0: if *info = -i, the ith argument had an illegal value
//          > 0: if *info = i + 1, the matrix is singular or nearly
//               singular, and the solution has not been computed.
//
static void
gauss_elim__ (GAUSSELIMSOLVE_REAL tolerance, int m, int n,
              GAUSSELIMSOLVE_REAL *A[m], int *signum, int *info)
{
  if (tolerance < 0)
    tolerance = 100 * DBL_EPSILON;
  if (m < 0)
    *info = -2;
  else if (n < m)
    *info = -3;
  else
    {
      *info = 0;
      *signum = 1;
      if (0 < n)
        {
          int i = 0;
          while (*info == 0 && i < m)
            {
              const int pivot = (i != m - 1) ? (select_pivot (m, A, i)) : i;
              if (fabs (A[pivot][pivot]) <= tolerance)
                *info = pivot + 1;
              else
                {
                  if (pivot != i)
                    {
                      // Interchange the pivot’th and ith rows.
                      GAUSSELIMSOLVE_REAL *const temp = A[pivot];
                      A[pivot] = A[i];
                      A[i] = temp;
                      *signum *= -1;
                    }
                  pivot_row_apply (m, n, A, i);
                  i++;
                }
            }
        }
    }
}

// Back substitution. The solution is stored in the ‘augmented part’
// of A.
//
// FIXME: Document this better.
static void
__back_subst__ (int m, int n, GAUSSELIMSOLVE_REAL *A[m])
{
  for (int i = m - 1; 0 <= i; i--)
    for (int j = m; j < n; j++)
      {
        GAUSSELIMSOLVE_REAL numer = A[i][j];
        for (int k = i + 1; k < m; k++)
          numer -= A[i][k] * A[k][j];
        A[i][j] = numer / A[i][i];
      }
}

// FIXME: Document this.
static void
gauss_elim_solve__ (GAUSSELIMSOLVE_REAL tolerance, int m, int n,
                    GAUSSELIMSOLVE_REAL *A[m], int *signum, int *info)
{
  int _signum;
  int _info;
  gauss_elim__ (tolerance, m, n, A, &_signum, &_info);
  if (signum != NULL)
    *signum = _signum;
  if (info != NULL)
    *info = _info;
  if (_info == 0)
    __back_subst__ (m, n, A);
}
