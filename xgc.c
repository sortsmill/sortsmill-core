#include <config.h>

// Copyright (C) 2012, 2013 by Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core/xgc.h>
#include <string.h>

// The following bunch of declarations should result in non-inline
// versions being generated.
VISIBLE void x_gc_free (void *p);
VISIBLE void *x_gc_malloc (size_t sz);
VISIBLE void *x_gc_malloc_atomic (size_t sz);
VISIBLE void *x_gc_malloc_uncollectable (size_t sz);
VISIBLE void *x_gc_malloc_uncollectible (size_t sz);
VISIBLE void *x_gc_realloc (void *old_pointer, size_t sz);
VISIBLE void *x_gc_malloc_ignore_off_page (size_t sz);
VISIBLE void *x_gc_malloc_atomic_ignore_off_page (size_t sz);
VISIBLE void *x_gc_malloc_stubborn (size_t sz);
VISIBLE char *x_gc_strdup (const char *s);
VISIBLE char *x_gc_grabstr (char *s);

VISIBLE char *
x_gc_strndup (const char *s, size_t n)
{
  size_t i = 0;
  while (i < n && s[i] != '\0')
    i++;
  char *p = x_gc_malloc_atomic ((i + 1) * sizeof (char));
  p[0] = '\0';
  strncat (p, s, i);
  return p;
}
