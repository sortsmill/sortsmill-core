include(`m4citrus.m4')dnl
m4_divert(-1)
m4_include([smcore-pkginfo.m4])
m4_include([smcore-dirlayout.m4])
m4_include([smcore-buildinfo.m4])
m4_include([libs.m4])
m4_define([expand_var], [$1=$2])
m4_define([expand_vars_list],
          [m4_ifnblank([$2], [# $2
])m4_map_sep([expand_var], [m4_newline], m4_split(m4_normalize($1)))])
m4_divert[]# This file was generated with GNU m4.

expand_vars_list([SMCORE_PKGINFO_LIST], [Package information.])

expand_vars_list([SMCORE_DIRLAYOUT_LIST], [Directories.])

expand_vars_list([SMCORE_BUILDINFO_LIST], [Configuration options.])

# ATS2/Postiats flags.
#
# Example use:
#
#   patscc `pkg-config --libs --variable patsccflags sortsmill-core` my_source.dats
#
patsccflags= -DATS_MEMALLOC_GCBDW -IIATS ${includedir}

Name: ${PACKAGE}
Description: ${PACKAGE_NAME}
Version: ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}
Requires.private: libpcre >= 8.10
# Requiring Boehm GC publicly, because expanded ATS2 templates
# are likely to call Boehm GC routines directly.
Requires: bdw-gc >= 7.1
Libs: -L${libdir} -lsortsmill-core LIBUNISTRING LTLIBINTL LIBICONV ATOMIC_OPS_LIBS -lm
Cflags: -I${hostincludedir} -I${includedir}
