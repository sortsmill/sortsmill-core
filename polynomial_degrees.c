#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <math.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

//----------------------------------------------------------------------

VISIBLE size_t
mpq_poly_mindegree_mono (size_t degree, ssize_t stride, mpq_t *poly)
{
  size_t mindegree = degree;
  while (0 < mindegree && mpq_sgn (poly[(ssize_t) mindegree * stride]) == 0)
    mindegree--;
  return mindegree;
}

VISIBLE size_t
dpoly_mindegree_mono (size_t degree, ssize_t stride, const double *poly)
{
  size_t mindegree = degree;
  while (0 < mindegree && poly[(ssize_t) mindegree * stride] == 0)
    mindegree--;
  return mindegree;
}

VISIBLE size_t
dpoly_mindegree_spower (size_t degree, ssize_t stride, const double *poly)
{
  size_t q = degree / 2;

  // Pointer to the highest degree of the left symmetric half.
  const double *p1 = poly + (ssize_t) q * stride;

  // Pointer to the highest degree of the right symmetric half.
  const double *p2 = poly + (ssize_t) (degree - q) * stride;

  // Start with the odd degree one gets by simply juxtaposing the
  // symmetric halves.
  size_t mindegree = q + q + 1;

  // Remove two degrees for each time both symmetric halves have a
  // zero.
  while (1 < mindegree && *p1 == 0 && *p2 == 0)
    {
      mindegree -= 2;
      p1 -= stride;
      p2 += stride;
    }

  // If the coefficients in the symmetric halves are equal, remove
  // another degree, because the actual degree is even.
  if (*p1 == *p2)
    mindegree--;

  return mindegree;
}

//----------------------------------------------------------------------

VISIBLE void
mpq_poly_changedegree_mono (size_t degree, ssize_t stride, mpq_t *poly,
                            size_t new_degree, ssize_t new_stride,
                            mpq_t *new_poly)
{
  // When lowering degree in the monomial basis, simply truncate the
  // sequence.

  mpq_t *buffer = xmalloc ((new_degree + 1) * sizeof (mpq_t));
  mpq_init_poly (new_degree, buffer);

  if (new_degree < degree)
    {
      for (size_t i = 0; i <= new_degree; i++)
        {
          mpq_set (buffer[i], *poly);
          poly += stride;
        }
    }
  else
    {
      for (size_t i = 0; i <= degree; i++)
        {
          mpq_set (buffer[i], *poly);
          poly += stride;
        }
      for (size_t i = degree + 1; i <= new_degree; i++)
        mpq_set_ui (buffer[i], 0, 1);
    }

  for (size_t i = 0; i <= new_degree; i++)
    {
      mpq_set (*new_poly, buffer[i]);
      new_poly += new_stride;
    }

  mpq_clear_poly (new_degree, buffer);
  free (buffer);
}

VISIBLE void
mpq_poly_copy (size_t degree, ssize_t stride, mpq_t *poly,
               ssize_t new_stride, mpq_t *new_poly)
{
  // This works even for bases other than monomial.
  mpq_poly_changedegree_mono (degree, stride, poly,
                              degree, new_stride, new_poly);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_copy (size_t degree, ssize_t stride, const double *poly,
            ssize_t new_stride, double *new_poly)
{
  double buffer[degree + 1];
  copy_f64_with_strides (1, buffer, stride, poly, degree + 1);
  copy_f64_with_strides (new_stride, new_poly, 1, buffer, degree + 1);
}

VISIBLE void
dpoly_changedegree_mono (size_t degree, ssize_t stride, const double *poly,
                         size_t new_degree, ssize_t new_stride,
                         double *new_poly)
{
  // When lowering degree in the monomial basis, simply truncate the
  // sequence.

  double buffer[new_degree + 1];

  if (new_degree < degree)
    {
      for (size_t i = 0; i <= new_degree; i++)
        {
          buffer[i] = *poly;
          poly += stride;
        }
    }
  else
    {
      for (size_t i = 0; i <= degree; i++)
        {
          buffer[i] = *poly;
          poly += stride;
        }
      for (size_t i = degree + 1; i <= new_degree; i++)
        buffer[i] = 0;
    }

  for (size_t i = 0; i <= new_degree; i++)
    {
      *new_poly = buffer[i];
      new_poly += new_stride;
    }
}

VISIBLE void
dpoly_changedegree_bern (size_t degree, ssize_t stride, const double *poly,
                         size_t new_degree, ssize_t new_stride,
                         double *new_poly)
{
  if (new_degree < degree)
    {
      // Reduce degree in symmetric power basis.
      double buffer[degree + 1];
      dpoly_bern_to_spower (degree, stride, poly, 1, buffer);
      dpoly_changedegree_spower (degree, 1, buffer, new_degree, 1, buffer);
      dpoly_spower_to_bern (new_degree, 1, buffer, new_stride, new_poly);
    }
  else if (new_degree == degree)
    dpoly_copy (degree, stride, poly, new_stride, new_poly);
  else
    {
      // To elevate degree, multiply by a polynomial that is
      // identically equal to one.
      double one[(new_degree - degree) + 1];
      for (size_t i = 0; i <= new_degree - degree; i++)
        one[i] = 1;
      dpoly_mul_bern (new_degree - degree, 1, one, degree, stride, poly,
                      new_stride, new_poly);
    }
}

VISIBLE void
dpoly_changedegree_sbern (size_t degree, ssize_t stride, const double *poly,
                          size_t new_degree, ssize_t new_stride,
                          double *new_poly)
{
  if (new_degree < degree)
    {
      // Reduce degree in symmetric power basis.
      double buffer[degree + 1];
      dpoly_sbern_to_spower (degree, stride, poly, 1, buffer);
      dpoly_changedegree_spower (degree, 1, buffer, new_degree, 1, buffer);
      dpoly_spower_to_sbern (new_degree, 1, buffer, new_stride, new_poly);
    }
  else if (new_degree == degree)
    dpoly_copy (degree, stride, poly, new_stride, new_poly);
  else
    {
      // To elevate degree, multiply by a polynomial that is
      // identically equal to one.
      double one[(new_degree - degree) + 1];
      for (size_t i = 0; i <= new_degree - degree; i++)
        one[i] = bincoef (new_degree - degree, i);
      dpoly_mul_sbern (new_degree - degree, 1, one, degree, stride, poly,
                       new_stride, new_poly);
    }
}

VISIBLE void
dpoly_changedegree_spower (size_t degree, ssize_t stride, const double *poly,
                           size_t new_degree, ssize_t new_stride,
                           double *new_poly)
{
  if (new_degree < degree)
    {
      // Lower degree by removing terms from the middle.
      double buffer[degree + 1];
      copy_f64_with_strides (1, buffer, stride, poly, degree + 1);
      dpoly_unsplit_spower (new_degree, 1, buffer, -1, &buffer[degree],
                            new_stride, new_poly);
    }
  else if (new_degree == degree)
    dpoly_copy (degree, stride, poly, new_stride, new_poly);
  else
    {
      // Pad the symmetric halves with zeros.
      double buffer[new_degree + 1];
      copy_f64_with_strides (1, buffer, stride, poly, degree / 2 + 1);
      copy_f64_with_strides (-1, &buffer[new_degree],
                             -stride, &poly[stride * (ssize_t) degree],
                             degree / 2 + 1);
      for (size_t i = degree / 2 + 1; i < new_degree - degree / 2; i++)
        buffer[i] = 0;
      copy_f64_with_strides (new_stride, new_poly, 1, buffer, new_degree + 1);
    }
}

//----------------------------------------------------------------------
//
// An upper bound on the absolute value of error due to change of
// degree, assuming the independent variable is in [0,1].

VISIBLE double
dpoly_degreechangeerrorbound_mono (size_t degree, ssize_t stride,
                                   const double *poly, size_t new_degree)
{
  // FIXME: Is there a practical tighter bound?

  double hedged_up = 0;
  double hedged_down = 0;
  for (size_t i = new_degree + 1; i <= degree; i++)
    {
      const double coef = poly[(ssize_t) i * stride];
      if (coef < 0)
        hedged_down = nextafter (hedged_down + coef, -HUGE_VAL);
      else if (0 < coef)
        hedged_up = nextafter (hedged_up + coef, HUGE_VAL);
    }
  return fmax (hedged_up, fabs (hedged_down));
}

static double
_dpoly_degreechangeerrorbound_spower_general (size_t degree, ssize_t stride,
                                              const double *poly,
                                              size_t new_degree)
{
  double error_bound;
  if (new_degree < degree)
    // FIXME: The general case of degree reduction is NOT YET
    // IMPLEMENTED – the return value _is_ an upper bound, but it is
    // of no practical use.
    error_bound = HUGE_VAL;
  else
    error_bound = 0;
  return error_bound;
}

static inline double
next_after_4_27ths (void)
{
  // FIXME: This could be precomputed.
  return nextafter (4.0 / 27.0, HUGE_VAL);
}

VISIBLE double
dpoly_degreechangeerrorbound_spower (size_t degree, ssize_t stride,
                                     const double *poly, size_t new_degree)
{
  // FIXME: One imagines a tighter bound is possible, in general.
  //
  // FIXME: Implement degree=3, new_degree=2. This can be used to
  // help flatten a cubic to a quadratic, for instance.

  double error_bound;

  switch (degree)
    {
    case 2:
      switch (new_degree)
        {
        case 1:
          // Special handling for flattening of quadratic splines to
          // line segments.
          //
          // Find an extremum of b₁(1 − t)t, where b₁ =
          // poly[stride]. This occurs when t = 1/2, so that (1 − t)t
          // = 1/4.
          //
          error_bound = (poly[stride] == 0) ?
            0 : (nextafter (fabs (poly[stride]) / 4, HUGE_VAL));
          break;
        default:
          error_bound =
            _dpoly_degreechangeerrorbound_spower_general (degree, stride, poly,
                                                          new_degree);
          break;
        }
      break;

    case 3:
      switch (new_degree)
        {
        case 1:
          // Special handling for flattening of cubic splines to line
          // segments.
          //
          // Find an upper bound for |b₁(1 − t)²t + b₂(1 − t)t²|,
          // where b₁ = poly[stride] and b₂ = poly[2 * stride].
          //
          // The term b₁(1 − t)²t has an extremum at t = 1/3, where it
          // has the value 4b₁/27. The term b₂(1 − t)t² has a maximum
          // at t = 2/3, where its value is 4b₂/27. By the triangle
          // inequality
          //
          //    |b₁(1 − t)²t + b₂(1 − t)t²| ≤ |b₁(1 − t)²t| + |b₂(1 − t)t²|
          //
          // and therefore (4/27)⋅(|b₁|+ |b₂|) is an upper bound.
          //
          // FIXME: One could find tighter bounds for particular
          // (b₁,b₂) pairs, but I do not know if it is worth the work.
          //
          {
            double sum = fabs (poly[stride]) + fabs (poly[2 * stride]);
            error_bound = (sum == 0) ?
              0 : (next_after_4_27ths () * nextafter (sum, HUGE_VAL));
          }
          break;
        default:
          error_bound =
            _dpoly_degreechangeerrorbound_spower_general (degree, stride, poly,
                                                          new_degree);
          break;
        }
      break;

    default:
      error_bound =
        _dpoly_degreechangeerrorbound_spower_general (degree, stride, poly,
                                                      new_degree);
      break;
    }

  return error_bound;
}

//----------------------------------------------------------------------
