#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

static inline size_t
szmax (size_t a, size_t b)
{
  return (a < b) ? b : a;
}

//----------------------------------------------------------------------

VISIBLE void
mpq_poly_neg (size_t degree1, ssize_t stride1, mpq_t *poly1,
              ssize_t result_stride, mpq_t *result)
{
  // FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
  //
  // FIXME: This does not necessarily work if there is overlap.
  //
  // FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
  //
  copy_negate_mpq_with_strides (result_stride, result, stride1, poly1,
                                degree1 + 1);
}

VISIBLE void
dpoly_neg (size_t degree1, ssize_t stride1, const double *poly1,
           ssize_t result_stride, double *result)
{
  double buffer[degree1 + 1];
  for (size_t i = 0; i <= degree1; i++)
    {
      buffer[i] = -(*poly1);
      poly1 += stride1;
    }
  for (size_t i = 0; i <= degree1; i++)
    {
      *result = buffer[i];
      result += result_stride;
    }
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_add (size_t degree, ssize_t stride1, const double *poly1,
           ssize_t stride2, const double *poly2, ssize_t stride3, double *poly3)
{
  double buffer[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    {
      buffer[i] = *poly1 + *poly2;
      poly1 += stride1;
      poly2 += stride2;
    }
  for (size_t i = 0; i <= degree; i++)
    {
      *poly3 = buffer[i];
      poly3 += stride3;
    }
}

VISIBLE void
dpoly_sub (size_t degree, ssize_t stride1, const double *poly1,
           ssize_t stride2, const double *poly2, ssize_t stride3, double *poly3)
{
  double buffer[degree + 1];
  for (size_t i = 0; i <= degree; i++)
    {
      buffer[i] = *poly1 - *poly2;
      poly1 += stride1;
      poly2 += stride2;
    }
  for (size_t i = 0; i <= degree; i++)
    {
      *poly3 = buffer[i];
      poly3 += stride3;
    }
}

//----------------------------------------------------------------------

VISIBLE void
mpq_poly_add_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                   size_t degree2, ssize_t stride2, mpq_t *poly2,
                   ssize_t result_stride, mpq_t *result)
{
  const size_t result_degree = szmax (degree1, degree2);

  // buffer ← poly2
  mpq_t *buffer = xmalloc ((result_degree + 1) * sizeof (mpq_t));
  mpq_init_poly (result_degree, buffer);
  copy_mpq_with_strides (1, buffer, stride2, poly2, degree2 + 1);

  // Raise the degree.
  for (size_t i = degree2; i < result_degree; i++)
    mpq_set_ui (buffer[i + 1], 0, 1);

  // buffer ← buffer + poly1
  for (size_t i = 0; i <= degree1; i++)
    {
      mpq_add (buffer[i], buffer[i], *poly1);
      poly1 += stride1;
    }

  copy_mpq_with_strides (result_stride, result, 1, buffer, result_degree + 1);

  mpq_clear_poly (result_degree, buffer);
  free (buffer);
}

VISIBLE void
mpq_poly_sub_mono (size_t degree1, ssize_t stride1, mpq_t *poly1,
                   size_t degree2, ssize_t stride2, mpq_t *poly2,
                   ssize_t result_stride, mpq_t *result)
{
  const size_t result_degree = szmax (degree1, degree2);

  // buffer ← -poly2
  mpq_t *buffer = xmalloc ((result_degree + 1) * sizeof (mpq_t));
  mpq_init_poly (result_degree, buffer);
  copy_negate_mpq_with_strides (1, buffer, stride2, poly2, degree2 + 1);

  // Raise the degree.
  for (size_t i = degree2; i < result_degree; i++)
    mpq_set_ui (buffer[i + 1], 0, 1);

  // buffer ← buffer + poly1
  for (size_t i = 0; i <= degree1; i++)
    {
      mpq_add (buffer[i], buffer[i], *poly1);
      poly1 += stride1;
    }

  copy_mpq_with_strides (result_stride, result, 1, buffer, result_degree + 1);

  mpq_clear_poly (result_degree, buffer);
  free (buffer);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_add_mono (size_t degree1, ssize_t stride1, const double *poly1,
                size_t degree2, ssize_t stride2, const double *poly2,
                ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);

  // buffer ← poly2
  double buffer[result_degree + 1];
  copy_f64_with_strides (1, buffer, stride2, poly2, degree2 + 1);

  // Raise the degree.
  for (size_t i = degree2; i < result_degree; i++)
    buffer[i + 1] = 0;

  // buffer ←  buffer + poly1
  for (size_t i = 0; i <= degree1; i++)
    {
      buffer[i] += *poly1;
      poly1 += stride1;
    }

  copy_f64_with_strides (result_stride, result, 1, buffer, result_degree + 1);
}

VISIBLE void
dpoly_sub_mono (size_t degree1, ssize_t stride1, const double *poly1,
                size_t degree2, ssize_t stride2, const double *poly2,
                ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);

  // buffer ← -poly2
  double buffer[result_degree + 1];
  for (size_t i = 0; i <= degree2; i++)
    {
      buffer[i] = -(*poly2);
      poly2 += stride2;
    }

  // Raise the degree.
  for (size_t i = degree2; i < result_degree; i++)
    buffer[i + 1] = 0;

  // buffer ← buffer + poly1
  for (size_t i = 0; i <= degree1; i++)
    {
      buffer[i] += *poly1;
      poly1 += stride1;
    }

  copy_f64_with_strides (result_stride, result, 1, buffer, result_degree + 1);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_add_bern (size_t degree1, ssize_t stride1, const double *poly1,
                size_t degree2, ssize_t stride2, const double *poly2,
                ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);
  double p1[result_degree + 1];
  double p2[result_degree + 1];
  dpoly_changedegree_bern (degree1, stride1, poly1, result_degree, 1, p1);
  dpoly_changedegree_bern (degree2, stride2, poly2, result_degree, 1, p2);
  dpoly_add (result_degree, 1, p1, 1, p2, result_stride, result);
}

VISIBLE void
dpoly_sub_bern (size_t degree1, ssize_t stride1, const double *poly1,
                size_t degree2, ssize_t stride2, const double *poly2,
                ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);
  double p1[result_degree + 1];
  double p2[result_degree + 1];
  dpoly_changedegree_bern (degree1, stride1, poly1, result_degree, 1, p1);
  dpoly_changedegree_bern (degree2, stride2, poly2, result_degree, 1, p2);
  dpoly_sub (result_degree, 1, p1, 1, p2, result_stride, result);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_add_sbern (size_t degree1, ssize_t stride1, const double *poly1,
                 size_t degree2, ssize_t stride2, const double *poly2,
                 ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);
  double p1[result_degree + 1];
  double p2[result_degree + 1];
  dpoly_changedegree_sbern (degree1, stride1, poly1, result_degree, 1, p1);
  dpoly_changedegree_sbern (degree2, stride2, poly2, result_degree, 1, p2);
  dpoly_add (result_degree, 1, p1, 1, p2, result_stride, result);
}

VISIBLE void
dpoly_sub_sbern (size_t degree1, ssize_t stride1, const double *poly1,
                 size_t degree2, ssize_t stride2, const double *poly2,
                 ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);
  double p1[result_degree + 1];
  double p2[result_degree + 1];
  dpoly_changedegree_sbern (degree1, stride1, poly1, result_degree, 1, p1);
  dpoly_changedegree_sbern (degree2, stride2, poly2, result_degree, 1, p2);
  dpoly_sub (result_degree, 1, p1, 1, p2, result_stride, result);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_add_spower (size_t degree1, ssize_t stride1, const double *poly1,
                  size_t degree2, ssize_t stride2, const double *poly2,
                  ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);
  double p1[result_degree + 1];
  double p2[result_degree + 1];
  dpoly_changedegree_spower (degree1, stride1, poly1, result_degree, 1, p1);
  dpoly_changedegree_spower (degree2, stride2, poly2, result_degree, 1, p2);
  dpoly_add (result_degree, 1, p1, 1, p2, result_stride, result);
}

VISIBLE void
dpoly_sub_spower (size_t degree1, ssize_t stride1, const double *poly1,
                  size_t degree2, ssize_t stride2, const double *poly2,
                  ssize_t result_stride, double *result)
{
  const size_t result_degree = szmax (degree1, degree2);
  double p1[result_degree + 1];
  double p2[result_degree + 1];
  dpoly_changedegree_spower (degree1, stride1, poly1, result_degree, 1, p1);
  dpoly_changedegree_spower (degree2, stride2, poly2, result_degree, 1, p2);
  dpoly_sub (result_degree, 1, p1, 1, p2, result_stride, result);
}

//----------------------------------------------------------------------

VISIBLE bool
mpq_poly_equalszero (size_t degree1, ssize_t stride1, mpq_t *poly1)
{
  ssize_t i = 0;
  while (i <= degree1 && mpq_sgn (*poly1) == 0)
    {
      poly1 += stride1;
      i++;
    }
  return (degree1 < i);
}

VISIBLE bool
dpoly_equalszero (size_t degree1, ssize_t stride1, const double *poly1)
{
  ssize_t i = 0;
  while (i <= degree1 && *poly1 == 0)
    {
      poly1 += stride1;
      i++;
    }
  return (degree1 < i);
}

//----------------------------------------------------------------------
