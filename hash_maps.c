#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <string.h>
#include <sortsmill/core.h>

static bool
_my_str_equalf (const char *key, const char *stored_key)
{
  return (strcmp (key, stored_key) == 0);
}

//
// smcore_str_to_str: A module for hash maps from strings to strings.
//
DEFINE_HASH_MAP_MODULE (VISIBLE const smcore_str_to_str_module_t
                        smcore_str_to_str,
                        const char *, uint32_t, const char *,
                        smcore_str_to_str_t, smcore_str_to_str_cursor_t,
                        smcore_uint32_strhash, _my_str_equalf,
                        (uintptr_t), (const char *), x_gc_strdup,
                        (uintptr_t), (const char *), x_gc_strdup);

//
// smcore_str_to_voidp: A module for hash maps from strings to void pointers.
//
DEFINE_HASH_MAP_MODULE (VISIBLE const smcore_str_to_voidp_module_t
                        smcore_str_to_voidp,
                        const char *, uint32_t, void *,
                        smcore_str_to_voidp_t, smcore_str_to_voidp_cursor_t,
                        smcore_uint32_strhash, _my_str_equalf,
                        (uintptr_t), (const char *), x_gc_strdup,
                        (uintptr_t), (void *), /* empty */);

//
// smcore_str_to_int, smcore_str_to_uint, etc.:
//
// Modules for hash maps from strings to integers that can fit into
// uintptr_t.
//
#define SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__(S, T)                 \
  DEFINE_HASH_MAP_MODULE (VISIBLE const smcore_str_to_##S##_module_t    \
                          smcore_str_to_##S,                            \
                          const char *, uint32_t, T,                    \
                          smcore_str_to_##S##_t,                        \
                          smcore_str_to_##S##_cursor_t,                 \
                          smcore_uint32_strhash, _my_str_equalf,        \
                          (uintptr_t), (const char *), x_gc_strdup,     \
                          (uintptr_t), (T), /* empty */)
//
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (intptr, intptr_t);
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (uintptr, uintptr_t);
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (ssize_t, ssize_t);
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (size_t, size_t);
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (int32, int32_t);
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (uint32, uint32_t);
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (int, int);
SMCORE_DEFINE_SMCORE_STR_TO_INTEGER_MAP__ (uint, unsigned int);
