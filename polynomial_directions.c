#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <math.h>
#include <float.h>
#include <sortsmill/core.h>

static double complex
_dpoly_direction (size_t degree_a, ssize_t stride_ax, const double *ax,
                  ssize_t stride_ay, const double *ay, double t,
                  void (*deriv) (size_t, ssize_t, const double *,
                                 ssize_t, double *),
                  double (*eval) (size_t, ssize_t, const double *, double))
{
  // This routine assumes reversing the sequence of coefficients
  // results in the same curve going in the opposite direction,
  // flipped around t = 1/2.  This is the case for our layout of
  // coefficients in the Bernstein, scaled Bernstein, and symmetric
  // power bases.

  double complex dir;

  if (degree_a <= 0)
    //
    // A point has no direction.
    //
    dir = NAN + I * NAN;
  else if (fabs (t - 1) <= DBL_EPSILON)
    //
    // If t is very close to one, reverse the curve and return the
    // opposite of the direction at zero. (FIXME: I forget why, but my
    // old Fortran code does this, so let us do it here. -- Barry
    // Schwartz, 22 April 2015.)
    //
    dir = -_dpoly_direction (degree_a,
                             -stride_ax, ax + (ssize_t) degree_a * stride_ax,
                             -stride_ay, ay + (ssize_t) degree_a * stride_ay,
                             0, deriv, eval);
  else
    {
      double ax_dot[degree_a];
      double ay_dot[degree_a];
      deriv (degree_a, stride_ax, ax, 1, ax_dot);
      deriv (degree_a, stride_ay, ay, 1, ay_dot);

      double complex v = (eval (degree_a - 1, 1, ax_dot, t) +
                          I * eval (degree_a - 1, 1, ay_dot, t));
      if (v == 0)
        // Use l’Hôpital’s rule.
        dir = _dpoly_direction (degree_a - 1, 1, ax_dot, 1, ay_dot, t,
                                deriv, eval);
      else
        dir = v / cabs (v);
    }

  return dir;
}

VISIBLE double complex
dpoly_direction_bernsv (size_t degree_a, ssize_t stride_ax, const double *ax,
                        ssize_t stride_ay, const double *ay, double t)
{
  return _dpoly_direction (degree_a, stride_ax, ax, stride_ay, ay, t,
                           dpoly_deriv_bern, dpoly_eval_bernsv);
}

VISIBLE double complex
dpoly_direction_berndc (size_t degree_a, ssize_t stride_ax, const double *ax,
                        ssize_t stride_ay, const double *ay, double t)
{
  return _dpoly_direction (degree_a, stride_ax, ax, stride_ay, ay, t,
                           dpoly_deriv_bern, dpoly_eval_berndc);
}

VISIBLE double complex
dpoly_direction_sbernsv (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay, double t)
{
  return _dpoly_direction (degree_a, stride_ax, ax, stride_ay, ay, t,
                           dpoly_deriv_sbern, dpoly_eval_sbernsv);
}

VISIBLE double complex
dpoly_direction_sberndc (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay, double t)
{
  return _dpoly_direction (degree_a, stride_ax, ax, stride_ay, ay, t,
                           dpoly_deriv_sbern, dpoly_eval_sberndc);
}

VISIBLE double complex
dpoly_direction_spower (size_t degree_a, ssize_t stride_ax, const double *ax,
                        ssize_t stride_ay, const double *ay, double t)
{
  return _dpoly_direction (degree_a, stride_ax, ax, stride_ay, ay, t,
                           dpoly_deriv_spower, dpoly_eval_spower);
}
