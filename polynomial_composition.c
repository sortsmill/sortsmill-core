#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <string.h>
#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

static void
_weighted_dpoly_add (size_t degree,
                     double w1, ssize_t stride1, const double *poly1,
                     double w2, ssize_t stride2, const double *poly2,
                     ssize_t result_stride, double *result)
{
  double buffer[degree + 1];
  copy_f64_with_strides (1, buffer, stride1, poly1, degree + 1);
  for (size_t i = 0; i <= degree; i++)
    buffer[i] = w1 * buffer[i] + w2 * poly2[stride2 * (ssize_t) i];
  copy_f64_with_strides (result_stride, result, 1, buffer, degree + 1);
}

// Compose two polynomials given in the ordinary monomial basis,
// obtaining a third polynomial of equal or higher degree.
//
// In particular, given polynomials f(t) and g(t), find a third
// polynomial h(t) = f(g(t)). You can view this operation as
// evaluating f at the variable g, and then transforming variables
// from g to t.
VISIBLE void
mpq_poly_compose_mono (size_t degree_f, ssize_t stride_f, mpq_t *f,
                       size_t degree_g, ssize_t stride_g, mpq_t *g,
                       ssize_t stride_h, mpq_t *h)
{
  // Horner’s rule with polynomial values.

  const size_t degree_h = degree_f * degree_g;
  mpq_t *buffer = xmalloc ((degree_h + 1) * sizeof (mpq_t));
  mpq_init_poly (degree_h, buffer);

  mpq_set (buffer[0], f[stride_f * (ssize_t) degree_f]);
  for (size_t i = 1; i <= degree_f; i++)
    {
      mpq_poly_mul_mono (degree_g, stride_g, g, (i - 1) * degree_g, 1, buffer,
                         1, buffer);
      mpq_add (buffer[0], buffer[0], f[stride_f * ((ssize_t) degree_f - i)]);
    }

  copy_mpq_with_strides (stride_h, h, 1, buffer, degree_h + 1);

  mpq_clear_poly (degree_h, buffer);
  free (buffer);
}

VISIBLE void
dpoly_compose_mono (size_t degree_f, ssize_t stride_f, const double *f,
                    size_t degree_g, ssize_t stride_g, const double *g,
                    ssize_t stride_h, double *h)
{
  // Horner’s rule with polynomial values.

  const size_t degree_h = degree_f * degree_g;
  double buffer[degree_h + 1];
  buffer[0] = f[stride_f * (ssize_t) degree_f];
  for (size_t i = 1; i <= degree_f; i++)
    {
      dpoly_mul_mono (degree_g, stride_g, g, (i - 1) * degree_g, 1, buffer,
                      1, buffer);
      buffer[0] += f[stride_f * ((ssize_t) degree_f - i)];
    }
  copy_f64_with_strides (stride_h, h, 1, buffer, degree_h + 1);
}

VISIBLE void
dpoly_compose_berndc (size_t degree_b, ssize_t stride_b, const double *b,
                      size_t degree_a, ssize_t stride_a, const double *a,
                      ssize_t stride_c, double *c)
{
  // De Casteljau’s algorithm with polynomial values.

  // We could get by with a lot less work space, but instead we are
  // going for ease and clarity of programming. In practice the arrays
  // are not going to be very large, anyway.

  double one_minus_a[degree_a + 1];
  for (size_t i = 0; i <= degree_a; i++)
    one_minus_a[i] = 1 - a[stride_a * (ssize_t) i];

  double _c[degree_b + 1][degree_a * degree_b + 1];
  double temp1[degree_a * degree_b + 1];
  double temp2[degree_a * degree_b + 1];

  for (size_t i = 0; i <= degree_b; i++)
    _c[i][0] = b[stride_b * (ssize_t) i];

  for (size_t i = 0; i < degree_b; i++)
    for (size_t j = 0; j < degree_b - i; j++)
      {
        dpoly_mul_bern (degree_a, 1, one_minus_a,
                        i * degree_a, 1, _c[j], 1, temp1);
        dpoly_mul_bern (degree_a, stride_a, a,
                        i * degree_a, 1, _c[j + 1], 1, temp2);
        dpoly_add ((i + 1) * degree_a, 1, temp1, 1, temp2, 1, _c[j]);
      }

  copy_f64_with_strides (stride_c, c, 1, _c[0], degree_a * degree_b + 1);
}

VISIBLE void
dpoly_compose_bernh (size_t degree_b, ssize_t stride_b, const double *b,
                     size_t degree_a, ssize_t stride_a, const double *a,
                     ssize_t stride_c, double *c)
{
  // Transform the problem to scaled Bernstein basis and then compose
  // by a variant of Horner’s form.

  double _a[degree_a + 1];
  double _b[degree_b + 1];
  double _c[degree_a * degree_b + 1];

  copy_f64_with_strides (1, _a, stride_a, a, degree_a + 1);
  for (size_t i = 1; i < degree_a; i++)
    _a[i] *= bincoef (degree_a, i);

  copy_f64_with_strides (1, _b, stride_b, b, degree_b + 1);
  for (size_t i = 1; i < degree_b; i++)
    _b[i] *= bincoef (degree_b, i);

  dpoly_compose_sbernh (degree_b, 1, _b, degree_a, 1, _a, 1, _c);

  for (size_t i = 1; i < degree_a * degree_b; i++)
    _c[i] /= bincoef (degree_a * degree_b, i);
  copy_f64_with_strides (stride_c, c, 1, _c, degree_a * degree_b + 1);
}

VISIBLE void
dpoly_compose_sberndc (size_t degree_b, ssize_t stride_b, const double *b,
                       size_t degree_a, ssize_t stride_a, const double *a,
                       ssize_t stride_c, double *c)
{
  // De Casteljau’s algorithm with polynomial values.

  // We could get by with a lot less work space, but instead we are
  // going for ease and clarity of programming. In practice the arrays
  // are not going to be very large, anyway.

  double one_minus_a[degree_a + 1];
  for (size_t i = 0; i <= degree_a; i++)
    one_minus_a[i] = bincoef (degree_a, i) - a[stride_a * (ssize_t) i];

  double _c[degree_b + 1][degree_a * degree_b + 1];
  double temp1[degree_a * degree_b + 1];
  double temp2[degree_a * degree_b + 1];

  for (size_t i = 0; i <= degree_b; i++)
    _c[i][0] = b[stride_b * (ssize_t) i] / bincoef (degree_b, i);

  for (size_t i = 0; i < degree_b; i++)
    for (size_t j = 0; j < degree_b - i; j++)
      {
        dpoly_mul_sbern (degree_a, 1, one_minus_a,
                         i * degree_a, 1, _c[j], 1, temp1);
        dpoly_mul_sbern (degree_a, stride_a, a,
                         i * degree_a, 1, _c[j + 1], 1, temp2);
        dpoly_add ((i + 1) * degree_a, 1, temp1, 1, temp2, 1, _c[j]);
      }

  copy_f64_with_strides (stride_c, c, 1, _c[0], degree_a * degree_b + 1);
}

VISIBLE void
dpoly_compose_sbernh (size_t degree_b, ssize_t stride_b, const double *b,
                      size_t degree_a, ssize_t stride_a, const double *a,
                      ssize_t stride_c, double *c)
{
  // Use a simple modified Horner scheme, with polynomial values. The
  // approach is to treat b as a ‘polynomial’ with coefficients
  //
  //    [1 − a(t)]ⁿ⋅b₀
  //    [1 − a(t)]ⁿ⁻¹⋅b₁
  //    [1 − a(t)]ⁿ⁻²⋅b₂
  //    [1 − a(t)]ⁿ⁻³⋅b₃
  //         ⋮
  //    [1 − a(t)]⁰⋅bₙ
  //
  // and evaluate that ‘polynomial’ by Horner’s rule, using polynomial
  // multiplication and polynomial addition.

  double one_minus_a[degree_a + 1];
  for (size_t i = 0; i <= degree_a; i++)
    one_minus_a[i] = bincoef (degree_a, i) - a[stride_a * (ssize_t) i];

  // degree_c = the degree of the result.
  const size_t degree_c = degree_a * degree_b;

  double power_of_one_minus_a[degree_c + 1];
  memcpy (power_of_one_minus_a, one_minus_a, (degree_a + 1) * sizeof (double));

  double accumulator[degree_c + 1];
  accumulator[0] = b[stride_b * (ssize_t) degree_b];

  double new_term[degree_c + 1];
  for (size_t i = 1; i <= degree_b; i++)
    {
      dpoly_mul_sbern ((i - 1) * degree_a, 1, accumulator,
                       degree_a, stride_a, a, 1, accumulator);

      memcpy (new_term, power_of_one_minus_a,
              (i * degree_a + 1) * sizeof (double));
      for (size_t j = 0; j <= i * degree_a; j++)
        new_term[j] *= b[stride_b * (ssize_t) (degree_b - i)];

      dpoly_add (i * degree_a, 1, accumulator, 1, new_term, 1, accumulator);

      if (i < degree_b)
        dpoly_mul_sbern (i * degree_a, 1, power_of_one_minus_a,
                         degree_a, 1, one_minus_a, 1, power_of_one_minus_a);
    }

  copy_f64_with_strides (stride_c, c, 1, accumulator, degree_c + 1);
}

static void
_dpoly_compose_spower_odd_degree (size_t degree_b,
                                  ssize_t stride_b, const double *b,
                                  size_t degree_a,
                                  ssize_t stride_a, const double *a,
                                  ssize_t stride_c, double *c)
{
  // Use Horner’s rule to compute
  //
  //    c(t) = b(a(t)) = b₀(t) + s(t)[b₁(t) + s(t)[b₂(t) + s(t)[b₃(t) + ⋯]]]
  //
  // where
  //
  //    s(t) = [1 − a(t)]⋅a(t)
  //    bₖ(t) = [1 − a(t)]⋅b⁰ₖ + a(t)⋅b¹ₖ
  //
  // and (b⁰ₖ, b¹ₖ) are the symmetric coefficient pairs of b.
  //

  double one_minus_a[degree_a + 1];
  for (size_t i = 0; i <= degree_a; i++)
    one_minus_a[i] = (i == 0 || i == degree_a) - a[stride_a * (ssize_t) i];

  double s[2 * degree_a + 1];
  dpoly_mul_spower (degree_a, 1, one_minus_a, degree_a, stride_a, a, 1, s);

  // qb = the degree of a symmetric half of b.
  const size_t qb = degree_b / 2;

  // degree_c = the degree of the result.
  const size_t degree_c = degree_a * degree_b;

  double _c[degree_c + 1];
  _weighted_dpoly_add (degree_a,
                       b[stride_b * (ssize_t) qb], 1, one_minus_a,
                       b[stride_b * (ssize_t) (degree_b - qb)], stride_a,
                       a, 1, _c);

  double bk[degree_a + 1];
  for (size_t k = 1; k <= qb; k++)
    {
      dpoly_mul_spower ((2 * k - 1) * degree_a, 1, _c, 2 * degree_a, 1, s, 1,
                        _c);
      _weighted_dpoly_add (degree_a, b[stride_b * (ssize_t) (qb - k)], 1,
                           one_minus_a,
                           b[stride_b * (ssize_t) (degree_b - qb + k)],
                           stride_a, a, 1, bk);
      dpoly_add_spower ((2 * k + 1) * degree_a, 1, _c, degree_a, 1, bk, 1, _c);
    }

  copy_f64_with_strides (stride_c, c, 1, _c, degree_c + 1);
}

static void
_dpoly_compose_spower_even_degree (size_t degree_b,
                                   ssize_t stride_b, const double *b,
                                   size_t degree_a,
                                   ssize_t stride_a, const double *a,
                                   ssize_t stride_c, double *c)
{
  // Use Horner’s rule to compute
  //
  //    c(t) = b(a(t)) = b₀(t) + s(t)[b₁(t) + s(t)[b₂(t) + s(t)[b₃(t) + ⋯]]]
  //
  // where
  //
  //    s(t) = [1 − a(t)]⋅a(t)
  //    bₖ(t) = [1 − a(t)]⋅b⁰ₖ + a(t)⋅b¹ₖ
  //
  // and (b⁰ₖ, b¹ₖ) are the symmetric coefficient pairs of b.
  //

  double one_minus_a[degree_a + 1];
  for (size_t i = 0; i <= degree_a; i++)
    one_minus_a[i] = (i == 0 || i == degree_a) - a[stride_a * (ssize_t) i];

  double s[2 * degree_a + 1];
  dpoly_mul_spower (degree_a, 1, one_minus_a, degree_a, stride_a, a, 1, s);

  // qb = the degree of a symmetric half of b.
  const size_t qb = degree_b / 2;

  // degree_c = the degree of the result.
  const size_t degree_c = degree_a * degree_b;

  double _c[degree_c + 1];
  _c[0] = b[stride_b * (ssize_t) qb];

  double bk[degree_a + 1];
  for (size_t k = 1; k <= qb; k++)
    {
      dpoly_mul_spower (2 * (k - 1) * degree_a, 1, _c, 2 * degree_a, 1, s, 1,
                        _c);
      _weighted_dpoly_add (degree_a, b[stride_b * (ssize_t) (qb - k)], 1,
                           one_minus_a,
                           b[stride_b * (ssize_t) (degree_b - qb + k)],
                           stride_a, a, 1, bk);
      dpoly_add_spower (2 * k * degree_a, 1, _c, degree_a, 1, bk, 1, _c);
    }

  copy_f64_with_strides (stride_c, c, 1, _c, degree_c + 1);
}

VISIBLE void
dpoly_compose_spower (size_t degree_f, ssize_t stride_f, const double *f,
                      size_t degree_g, ssize_t stride_g, const double *g,
                      ssize_t stride_h, double *h)
{
  if (degree_f % 2 == 1)
    _dpoly_compose_spower_odd_degree (degree_f, stride_f, f,
                                      degree_g, stride_g, g, stride_h, h);
  else
    _dpoly_compose_spower_even_degree (degree_f, stride_f, f,
                                       degree_g, stride_g, g, stride_h, h);
}
