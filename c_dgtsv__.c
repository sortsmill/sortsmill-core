#include <config.h>

/*
// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

/*
// This module is based on dgtsv.f from LAPACK 3.5.0. LAPACK 3.5.0 is
// copyrighted and licensed as follows (see file LICENSE in the LAPACK
// package).
//
// Copyright (c) 1992-2011 The University of Tennessee and The University
//                         of Tennessee Research Foundation.  All rights
//                         reserved.
// Copyright (c) 2000-2011 The University of California Berkeley. All
//                         rights reserved.
// Copyright (c) 2006-2012 The University of Colorado Denver.  All rights
//                         reserved.
//
// $COPYRIGHT$
//
// Additional copyrights may follow
//
// $HEADER$
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// 
// - Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer listed
//   in this license in the documentation and/or other materials
//   provided with the distribution.
// 
// - Neither the name of the copyright holders nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
// 
// The copyright holders provide no reassurances that the source code
// provided does not infringe any patent, copyright, or any other
// intellectual property rights of third parties.  The copyright holders
// disclaim any liability to any recipient for claims brought against
// recipient by any third party for infringement of that parties
// intellectual property rights.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
*> \brief <b> DGTSV computes the solution to system of linear equations A * X = B for GT matrices <b>
*
*  =========== DOCUMENTATION ===========
*
* Online html documentation available at 
*            http://www.netlib.org/lapack/explore-html/ 
*
*> \htmlonly
*> Download DGTSV + dependencies 
*> <a href="http://www.netlib.org/cgi-bin/netlibfiles.tgz?format=tgz&filename=/lapack/lapack_routine/dgtsv.f"> 
*> [TGZ]</a> 
*> <a href="http://www.netlib.org/cgi-bin/netlibfiles.zip?format=zip&filename=/lapack/lapack_routine/dgtsv.f"> 
*> [ZIP]</a> 
*> <a href="http://www.netlib.org/cgi-bin/netlibfiles.txt?format=txt&filename=/lapack/lapack_routine/dgtsv.f"> 
*> [TXT]</a>
*> \endhtmlonly 
*
*  Definition:
*  ===========
*
*       SUBROUTINE DGTSV( N, NRHS, DL, D, DU, B, LDB, INFO )
* 
*       .. Scalar Arguments ..
*       INTEGER            INFO, LDB, N, NRHS
*       ..
*       .. Array Arguments ..
*       DOUBLE PRECISION   B( LDB, * ), D( * ), DL( * ), DU( * )
*       ..
*  
*
*> \par Purpose:
*  =============
*>
*> \verbatim
*>
*> DGTSV  solves the equation
*>
*>    A*X = B,
*>
*> where A is an n by n tridiagonal matrix, by Gaussian elimination with
*> partial pivoting.
*>
*> Note that the equation  A**T*X = B  may be solved by interchanging the
*> order of the arguments DU and DL.
*> \endverbatim
*
*  Arguments:
*  ==========
*
*> \param[in] N
*> \verbatim
*>          N is INTEGER
*>          The order of the matrix A.  N >= 0.
*> \endverbatim
*>
*> \param[in] NRHS
*> \verbatim
*>          NRHS is INTEGER
*>          The number of right hand sides, i.e., the number of columns
*>          of the matrix B.  NRHS >= 0.
*> \endverbatim
*>
*> \param[in,out] DL
*> \verbatim
*>          DL is DOUBLE PRECISION array, dimension (N-1)
*>          On entry, DL must contain the (n-1) sub-diagonal elements of
*>          A.
*>
*>          On exit, DL is overwritten by the (n-2) elements of the
*>          second super-diagonal of the upper triangular matrix U from
*>          the LU factorization of A, in DL(1), ..., DL(n-2).
*> \endverbatim
*>
*> \param[in,out] D
*> \verbatim
*>          D is DOUBLE PRECISION array, dimension (N)
*>          On entry, D must contain the diagonal elements of A.
*>
*>          On exit, D is overwritten by the n diagonal elements of U.
*> \endverbatim
*>
*> \param[in,out] DU
*> \verbatim
*>          DU is DOUBLE PRECISION array, dimension (N-1)
*>          On entry, DU must contain the (n-1) super-diagonal elements
*>          of A.
*>
*>          On exit, DU is overwritten by the (n-1) elements of the first
*>          super-diagonal of U.
*> \endverbatim
*>
*> \param[in,out] B
*> \verbatim
*>          B is DOUBLE PRECISION array, dimension (LDB,NRHS)
*>          On entry, the N by NRHS matrix of right hand side matrix B.
*>          On exit, if INFO = 0, the N by NRHS solution matrix X.
*> \endverbatim
*>
*> \param[in] LDB
*> \verbatim
*>          LDB is INTEGER
*>          The leading dimension of the array B.  LDB >= max(1,N).
*> \endverbatim
*>
*> \param[out] INFO
*> \verbatim
*>          INFO is INTEGER
*>          = 0: successful exit
*>          < 0: if INFO = -i, the i-th argument had an illegal value
*>          > 0: if INFO = i, U(i,i) is exactly zero, and the solution
*>               has not been computed.  The factorization has not been
*>               completed unless i = N.
*> \endverbatim
*
*  Authors:
*  ========
*
*> \author Univ. of Tennessee 
*> \author Univ. of California Berkeley 
*> \author Univ. of Colorado Denver 
*> \author NAG Ltd. 
*
*> \date September 2012
*
*> \ingroup doubleGTsolve
*/

#include <math.h>

#ifndef C_DGTSV_REAL
#define C_DGTSV_REAL double
#endif

#ifndef C_DGTSV_FUNC
#define C_DGTSV_FUNC dtridiagsolve
#endif

#define _ABS(x) (((x) < 0) ? -(x) : (x))

VISIBLE void
C_DGTSV_FUNC (int n, int nrhs, C_DGTSV_REAL *dl, C_DGTSV_REAL *d__,
              C_DGTSV_REAL *du, C_DGTSV_REAL *b, int ldb, int *info)
{
  // Created from DGTSV with the assistance of f2c, but with
  // hand-editing after the run of f2c.

  /* System generated locals */
  int b_dim1, b_offset, i__1, i__2;
  C_DGTSV_REAL d__1, d__2;

  /* Local variables */
  int i__, j;
  C_DGTSV_REAL fact, temp;

  /* Parameter adjustments */
  --dl;
  --d__;
  --du;
  b_dim1 = ldb;
  b_offset = 1 + b_dim1;
  b -= b_offset;

  /* Function Body */
  *info = 0;
  if (n < 0)
    *info = -1;
  else if (nrhs < 0)
    *info = -2;
  else if (ldb < ((1 < n) ? n : 1))
    *info = -7;
  if (*info != 0)
    return;

  if (n == 0)
    return;

  if (nrhs == 1)
    {
      i__1 = n - 2;
      for (i__ = 1; i__ <= i__1; ++i__)
        {
          if ((d__1 = d__[i__], _ABS (d__1)) >= (d__2 = dl[i__], _ABS (d__2)))
            {

              /*              No row interchange required */

              if (d__[i__] != 0.)
                {
                  fact = dl[i__] / d__[i__];
                  d__[i__ + 1] -= fact * du[i__];
                  b[i__ + 1 + b_dim1] -= fact * b[i__ + b_dim1];
                }
              else
                {
                  *info = i__;
                  return;
                }
              dl[i__] = 0.;
            }
          else
            {

              /*              Interchange rows I and I+1 */

              fact = d__[i__] / dl[i__];
              d__[i__] = dl[i__];
              temp = d__[i__ + 1];
              d__[i__ + 1] = du[i__] - fact * temp;
              dl[i__] = du[i__ + 1];
              du[i__ + 1] = -fact * dl[i__];
              du[i__] = temp;
              temp = b[i__ + b_dim1];
              b[i__ + b_dim1] = b[i__ + 1 + b_dim1];
              b[i__ + 1 + b_dim1] = temp - fact * b[i__ + 1 + b_dim1];
            }
        }
      if (n > 1)
        {
          i__ = n - 1;
          if ((d__1 = d__[i__], _ABS (d__1)) >= (d__2 = dl[i__], _ABS (d__2)))
            {
              if (d__[i__] != 0.)
                {
                  fact = dl[i__] / d__[i__];
                  d__[i__ + 1] -= fact * du[i__];
                  b[i__ + 1 + b_dim1] -= fact * b[i__ + b_dim1];
                }
              else
                {
                  *info = i__;
                  return;
                }
            }
          else
            {
              fact = d__[i__] / dl[i__];
              d__[i__] = dl[i__];
              temp = d__[i__ + 1];
              d__[i__ + 1] = du[i__] - fact * temp;
              du[i__] = temp;
              temp = b[i__ + b_dim1];
              b[i__ + b_dim1] = b[i__ + 1 + b_dim1];
              b[i__ + 1 + b_dim1] = temp - fact * b[i__ + 1 + b_dim1];
            }
        }
      if (d__[n] == 0.)
        {
          *info = n;
          return;
        }
    }
  else
    {
      i__1 = n - 2;
      for (i__ = 1; i__ <= i__1; ++i__)
        {
          if ((d__1 = d__[i__], _ABS (d__1)) >= (d__2 = dl[i__], _ABS (d__2)))
            {

              /*              No row interchange required */

              if (d__[i__] != 0.)
                {
                  fact = dl[i__] / d__[i__];
                  d__[i__ + 1] -= fact * du[i__];
                  i__2 = nrhs;
                  for (j = 1; j <= i__2; ++j)
                    {
                      b[i__ + 1 + j * b_dim1] -= fact * b[i__ + j * b_dim1];
                    }
                }
              else
                {
                  *info = i__;
                  return;
                }
              dl[i__] = 0.;
            }
          else
            {

              /*              Interchange rows I and I+1 */

              fact = d__[i__] / dl[i__];
              d__[i__] = dl[i__];
              temp = d__[i__ + 1];
              d__[i__ + 1] = du[i__] - fact * temp;
              dl[i__] = du[i__ + 1];
              du[i__ + 1] = -fact * dl[i__];
              du[i__] = temp;
              i__2 = nrhs;
              for (j = 1; j <= i__2; ++j)
                {
                  temp = b[i__ + j * b_dim1];
                  b[i__ + j * b_dim1] = b[i__ + 1 + j * b_dim1];
                  b[i__ + 1 + j * b_dim1] =
                    temp - fact * b[i__ + 1 + j * b_dim1];
                }
            }
        }
      if (n > 1)
        {
          i__ = n - 1;
          if ((d__1 = d__[i__], _ABS (d__1)) >= (d__2 = dl[i__], _ABS (d__2)))
            {
              if (d__[i__] != 0.)
                {
                  fact = dl[i__] / d__[i__];
                  d__[i__ + 1] -= fact * du[i__];
                  i__1 = nrhs;
                  for (j = 1; j <= i__1; ++j)
                    {
                      b[i__ + 1 + j * b_dim1] -= fact * b[i__ + j * b_dim1];
                    }
                }
              else
                {
                  *info = i__;
                  return;
                }
            }
          else
            {
              fact = d__[i__] / dl[i__];
              d__[i__] = dl[i__];
              temp = d__[i__ + 1];
              d__[i__ + 1] = du[i__] - fact * temp;
              du[i__] = temp;
              i__1 = nrhs;
              for (j = 1; j <= i__1; ++j)
                {
                  temp = b[i__ + j * b_dim1];
                  b[i__ + j * b_dim1] = b[i__ + 1 + j * b_dim1];
                  b[i__ + 1 + j * b_dim1] =
                    temp - fact * b[i__ + 1 + j * b_dim1];
                }
            }
        }
      if (d__[n] == 0.)
        {
          *info = n;
          return;
        }
    }

  /*     Back solve with the matrix U from the factorization. */

  if (nrhs <= 2)
    {
      j = 1;
    L70:
      b[n + j * b_dim1] /= d__[n];
      if (n > 1)
        {
          b[n - 1 + j * b_dim1] =
            (b[n - 1 + j * b_dim1] -
             du[n - 1] * b[n + j * b_dim1]) / d__[n - 1];
        }
      for (i__ = n - 2; i__ >= 1; --i__)
        {
          b[i__ + j * b_dim1] =
            (b[i__ + j * b_dim1] - du[i__] * b[i__ + 1 + j * b_dim1] -
             dl[i__] * b[i__ + 2 + j * b_dim1]) / d__[i__];
        }
      if (j < nrhs)
        {
          ++j;
          goto L70;
        }
    }
  else
    {
      i__1 = nrhs;
      for (j = 1; j <= i__1; ++j)
        {
          b[n + j * b_dim1] /= d__[n];
          if (n > 1)
            {
              b[n - 1 + j * b_dim1] = (b[n - 1 + j * b_dim1] - du[n - 1]
                                       * b[n + j * b_dim1]) / d__[n - 1];
            }
          for (i__ = n - 2; i__ >= 1; --i__)
            {
              b[i__ + j * b_dim1] =
                (b[i__ + j * b_dim1] - du[i__] * b[i__ + 1 + j * b_dim1] -
                 dl[i__] * b[i__ + 2 + j * b_dim1]) / d__[i__];
            }
        }
    }

  return;
}
