#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core/popcount.h>

// Compiled versions of inline functions.
VISIBLE int _smcore_popcount_32__ (unsigned int);
VISIBLE int smcore_popcount (unsigned int);
VISIBLE int smcore_popcountl (unsigned long int);
VISIBLE int smcore_popcountll (unsigned long long int);
VISIBLE int smcore_popcount32 (uint32_t);
VISIBLE int smcore_popcount64 (uint64_t);
VISIBLE int smcore_popcountz (size_t);
VISIBLE int smcore_popcountp (uintptr_t);
