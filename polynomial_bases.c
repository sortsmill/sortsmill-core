#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <xalloc.h>
#include <sortsmill/core.h>
#include "polynomial_helpers.h"

static void
initialize_matrix_with_zeros (size_t m, size_t n, mpq_t T[m][n])
{
  for (size_t i = 0; i < m; i++)
    for (size_t j = 0; j < n; j++)
      {
        mpq_init (T[i][j]);
        mpq_set_ui (T[i][j], 0, 1);
      }
}

static mpq_t *
product_of_square_matrices (size_t n, mpq_t *A, mpq_t *B)
{
  mpq_t tmp;
  mpq_init (tmp);

  mpq_t (*A_)[n] = (mpq_t (*)[n]) A;
  mpq_t (*B_)[n] = (mpq_t (*)[n]) B;
  mpq_t (*AB)[n] = xmalloc (n * n * sizeof (mpq_t));

  for (size_t i = 0; i < n; i++)
    for (size_t j = 0; j < n; j++)
      {
        mpq_init (AB[i][j]);
        mpq_mul (AB[i][j], A_[i][0], B_[0][j]);
        for (size_t k = 1; k < n; k++)
          {
            mpq_mul (tmp, A_[i][k], B_[k][j]);
            mpq_add (AB[i][j], AB[i][j], tmp);
          }
      }

  mpq_clear (tmp);

  return (mpq_t *) AB;
}

// Multiply a row vector by this matrix to convert coefficients from
// monomial basis to Bernstein basis.
//
// The matrix is upper triangular.
//
// The matrix’s rows are the Bernstein coefficients of the monomial
// basis polynomials. For example:
//
//    1  = 1⋅(1 − t)³ + 1⋅3t(1 − t)² + 1⋅3t²(1 - t) + 1⋅t³
//    t  =             ⅓⋅3t(1 − t)² + ⅔⋅3t²(1 - t) + 1⋅t³
//    t² =                            ⅓⋅3t²(1 - t) + 1⋅t³
//    t³ =                                           1⋅t³
//
static mpq_t *
make_mpq_matrix_mono_to_bern (size_t degree)
{
  mpq_t divisor;
  mpq_init (divisor);

  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  for (size_t i = 0; i <= degree; i++)
    {
      for (size_t j = 0; j < i; j++)
        {
          mpq_init (T[i][j]);
          mpq_set_ui (T[i][j], 0, 1);
        }
      for (size_t j = i; j <= degree; j++)
        {
          mpq_init (T[i][j]);
          mpq_bincoef_ui (T[i][j], degree - i, j - i);
        }
    }

  for (size_t j = 1; j < degree; j++)
    {
      mpq_bincoef_ui (divisor, degree, j);
      for (size_t i = 0; i <= degree; i++)
        mpq_div (T[i][j], T[i][j], divisor);
    }

  mpq_clear (divisor);

  return (mpq_t *) T;
}

// Multiply a row vector by this matrix to convert coefficients from
// Bernstein basis to monomial basis.
//
// The matrix is upper triangular.
//
// The matrix’s rows are the monomial coefficients of the Bernstein
// basis polynomials. For example:
//
//    (1 − t)³   = 1 − 3t + 3t² −  t³
//    3t(1 − t)² =     3t − 6t² + 3t³
//    3t²(1 - t) =          3t² − 3t³
//    t³         =                 t³
//
static mpq_t *
make_mpq_matrix_bern_to_mono (size_t degree)
{
  mpq_t factor;
  mpq_init (factor);

  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  for (size_t i = 0; i <= degree; i++)
    {
      for (size_t j = 0; j < i; j++)
        {
          mpq_init (T[i][j]);
          mpq_set_ui (T[i][j], 0, 1);
        }
      for (size_t j = i; j <= degree; j++)
        {
          mpq_init (T[i][j]);
          mpq_bincoef_ui (T[i][j], degree - i, j - i);
        }
      for (size_t j = i + 1; j <= degree; j += 2)
        mpq_neg (T[i][j], T[i][j]);
    }

  for (size_t i = 1; i < degree; i++)
    {
      mpq_bincoef_ui (factor, degree, i);
      for (size_t j = 0; j <= degree; j++)
        mpq_mul (T[i][j], T[i][j], factor);
    }

  mpq_clear (factor);

  return (mpq_t *) T;
}

// Multiply a row vector by this matrix to convert coefficients from
// monomial basis to scaled Bernstein basis.
//
// The matrix is upper triangular.
//
// The matrix’s rows are the scaled Bernstein coefficients of the
// monomial basis polynomials. For example:
//
//    1  = (1 − t)³ + 3t(1 − t)² + 3t²(1 - t) + t³
//    t  =             t(1 − t)² + 2t²(1 - t) + t³
//    t² =                          t²(1 - t) + t³
//    t³ =                                      t³
//
static mpq_t *
make_mpq_matrix_mono_to_sbern (size_t degree)
{
  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  for (size_t i = 0; i <= degree; i++)
    {
      for (size_t j = 0; j < i; j++)
        {
          mpq_init (T[i][j]);
          mpq_set_ui (T[i][j], 0, 1);
        }
      for (size_t j = i; j <= degree; j++)
        {
          mpq_init (T[i][j]);
          mpq_bincoef_ui (T[i][j], degree - i, j - i);
        }
    }

  return (mpq_t *) T;
}

// Multiply a row vector by this matrix to convert coefficients from
// scaled Bernstein basis to monomial basis.
//
// The matrix is upper triangular.
//
// The matrix’s rows are the monomial coefficients of the scaled
// Bernstein basis polynomials. For example:
//
//    (1 − t)³  = 1 − 3t + 3t² − t³
//    t(1 − t)² =      t − 2t² + t³
//    t²(1 - t) =           t² - t³
//    t³        =                t³
//
static mpq_t *
make_mpq_matrix_sbern_to_mono (size_t degree)
{
  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  for (size_t i = 0; i <= degree; i++)
    {
      for (size_t j = 0; j < i; j++)
        {
          mpq_init (T[i][j]);
          mpq_set_ui (T[i][j], 0, 1);
        }
      for (size_t j = i; j <= degree; j++)
        {
          mpq_init (T[i][j]);
          mpq_bincoef_ui (T[i][j], degree - i, j - i);
        }
      for (size_t j = i + 1; j <= degree; j += 2)
        mpq_neg (T[i][j], T[i][j]);
    }

  return (mpq_t *) T;
}

// Multiply a row vector by this matrix to convert coefficients from
// Bernstein basis to scaled Bernstein basis.
//
// The matrix is diagonal and positive-definite.
//
// The matrix’s rows are the scaled Bernstein coefficients of the
// Bernstein basis polynomials. For example:
//
//    (1 − t)³   = 1⋅(1 − t)³
//    3t(1 − t)² = 3⋅t(1 − t)²
//    3t²(1 - t) = 3⋅t²(1 - t)
//    t³         = 1⋅t³
//
static mpq_t *
make_mpq_matrix_bern_to_sbern (size_t degree)
{
  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  for (size_t i = 0; i <= degree; i++)
    for (size_t j = 0; j <= degree; j++)
      {
        mpq_init (T[i][j]);
        if (i == j)
          mpq_bincoef_ui (T[i][j], degree, i);
        else
          mpq_set_ui (T[i][j], 0, 1);
      }

  return (mpq_t *) T;
}

// Multiply a row vector by this matrix to convert coefficients from
// scaled Bernstein basis to Bernstein basis.
//
// The matrix is diagonal and positive-definite.
//
// The matrix’s rows are the Bernstein coefficients of the scaled
// Bernstein basis polynomials. For example:
//
//    (1 − t)³  = 1⋅(1 − t)³
//    t(1 − t)² = ⅓⋅t(1 − t)²
//    t²(1 - t) = ⅓⋅t²(1 - t)
//    t³        = 1⋅t³
//
static mpq_t *
make_mpq_matrix_sbern_to_bern (size_t degree)
{
  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  for (size_t i = 0; i <= degree; i++)
    for (size_t j = 0; j <= degree; j++)
      {
        mpq_init (T[i][j]);
        if (i == j)
          {
            // The following produces canonical form, making a call to
            // mpq_canonicalize() unnecessary.
            mpz_set_ui (mpq_numref (T[i][j]), 1);
            mpz_bincoef_ui (mpq_denref (T[i][j]), degree, i);
          }
        else
          mpq_set_ui (T[i][j], 0, 1);
      }

  return (mpq_t *) T;
}

static void
fill_spower_middle_row (size_t n, mpq_t T[n + 1][n + 1])
{
  const size_t q = n / 2 + n % 2;

  // A middle row for the extra term in polynomials of even degree.
  if (n % 2 == 0)
    {
      for (size_t j = 0; j <= q; j++)
        {
          mpq_init (T[q][j]);
          mpq_set_ui (T[q][j], 0, 1);
        }

      mpq_init (T[q][q]);
      mpq_set_ui (T[q][q], 1, 1);

      for (size_t j = q + 1; j <= n; j++)
        {
          mpq_init (T[q][j]);
          mpq_set_ui (T[q][j], 0, 1);
        }
    }
}

// Multiply a row vector by this matrix to convert coefficients from
// scaled Bernstein basis to Sánchez-Reyes s-power basis.
//
// The matrix’s rows are the Sánchez-Reyes coefficients of the scaled
// Bernstein basis polynomials.
static mpq_t *
make_mpq_matrix_sbern_to_spower (size_t degree)
{
  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  const size_t n = degree;
  const size_t q = n / 2 + n % 2;

  initialize_matrix_with_zeros (degree + 1, degree + 1, T);

  for (size_t i = 0; i < q; i++)
    {
      // Fill in a top-half row.
      for (size_t j = i; j < q; j++)
        {
          mpq_bincoef_ui (T[i][j], n - j - i, j - i);
          if ((j - i) % 2 == 1)
            mpq_neg (T[i][j], T[i][j]);
        }
      if (n % 2 == 0)
        {
          if ((q - i) % 2 == 1)
            mpq_set_si (T[i][q], -1, 1);
          else
            mpq_set_ui (T[i][q], 1, 1);
        }
      for (size_t j = i + 1; j < q; j++)
        {
          mpq_bincoef_ui (T[i][n - j], n - j - i - 1, j - i - 1);
          if ((j - i) % 2 == 1)
            mpq_neg (T[i][n - j], T[i][n - j]);
        }

      // The corresponding bottom-half row is the reverse.
      for (size_t j = 0; j <= n; j++)
        {
          mpq_init (T[n - i][n - j]);
          mpq_set (T[n - i][n - j], T[i][j]);
        }
    }

  // For special handling of even degrees, put an extra ‘1’ in the
  // middle of the matrix.
  fill_spower_middle_row (n, T);

  return (mpq_t *) T;
}

// Multiply a row vector by this matrix to convert coefficients from
// Sánchez-Reyes s-power basis to scaled Bernstein basis.
//
// The matrix’s rows are the scaled Bernstein coefficients of the
// Sánchez-Reyes basis polynomials.
static mpq_t *
make_mpq_matrix_spower_to_sbern (size_t degree)
{
  mpq_t (*T)[degree + 1] =
    xmalloc ((degree + 1) * (degree + 1) * sizeof (mpq_t));

  const size_t n = degree;
  const size_t q = n / 2 + n % 2;

  for (size_t i = 0; i < q; i++)
    {
      // Fill in a top-half row.
      for (size_t j = 0; j <= i; j++)
        {
          mpq_init (T[i][j]);
          mpq_set_ui (T[i][j], 0, 1);
        }
      const size_t d = n - (2 * i) - 1;
      for (size_t j = i; j <= i + d; j++)
        {
          mpq_init (T[i][j]);
          mpq_bincoef_ui (T[i][j], d, j - i);
        }
      for (size_t j = i + d + 1; j <= n; j++)
        {
          mpq_init (T[i][j]);
          mpq_set_ui (T[i][j], 0, 1);
        }

      // The corresponding bottom-half row is the reverse.
      for (size_t j = 0; j <= n; j++)
        {
          mpq_init (T[n - i][n - j]);
          mpq_set (T[n - i][n - j], T[i][j]);
        }
    }

  // For special handling of even degrees, put an extra ‘1’ in the
  // middle of the matrix.
  fill_spower_middle_row (n, T);

  return (mpq_t *) T;
}

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_mono_to_bern, make_mpq_matrix_mono_to_bern);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_bern_to_mono, make_mpq_matrix_bern_to_mono);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_mono_to_sbern, make_mpq_matrix_mono_to_sbern);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_sbern_to_mono, make_mpq_matrix_sbern_to_mono);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_bern_to_sbern, make_mpq_matrix_bern_to_sbern);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_sbern_to_bern, make_mpq_matrix_sbern_to_bern);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_sbern_to_spower,
                       make_mpq_matrix_sbern_to_spower);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_spower_to_sbern,
                       make_mpq_matrix_spower_to_sbern);

static mpq_t *
make_mpq_matrix_bern_to_spower (size_t degree)
{
  return product_of_square_matrices (degree + 1,
                                     mpq_matrix_bern_to_sbern (degree),
                                     mpq_matrix_sbern_to_spower (degree));
}

static mpq_t *
make_mpq_matrix_spower_to_bern (size_t degree)
{
  return product_of_square_matrices (degree + 1,
                                     mpq_matrix_spower_to_sbern (degree),
                                     mpq_matrix_sbern_to_bern (degree));
}

static mpq_t *
make_mpq_matrix_mono_to_spower (size_t degree)
{
  return product_of_square_matrices (degree + 1,
                                     mpq_matrix_mono_to_sbern (degree),
                                     mpq_matrix_sbern_to_spower (degree));
}

static mpq_t *
make_mpq_matrix_spower_to_mono (size_t degree)
{
  return product_of_square_matrices (degree + 1,
                                     mpq_matrix_spower_to_sbern (degree),
                                     mpq_matrix_sbern_to_mono (degree));
}

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_bern_to_spower,
                       make_mpq_matrix_bern_to_spower);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_spower_to_bern,
                       make_mpq_matrix_spower_to_bern);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_mono_to_spower,
                       make_mpq_matrix_mono_to_spower);

STM_INDEXED_CONSTANTS (STM_ATTRIBUTE_PURE static, mpq_t *,
                       mpq_matrix_spower_to_mono,
                       make_mpq_matrix_spower_to_mono);

static void
mpq_mul_row_by_diagonal (size_t degree, ssize_t stride_a, mpq_t *a,
                         mpq_t T[degree + 1][degree + 1],
                         ssize_t stride_b, mpq_t *b)
{
  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  for (size_t i = 0; i <= degree; i++)
    mpq_mul (buffer[i], a[i], T[i][i]);

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set (*b, buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

static void
double_mul_row_by_diagonal (size_t degree, ssize_t stride_a, const double *a,
                            mpq_t T[degree + 1][degree + 1],
                            ssize_t stride_b, double *b)
{
  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set_d (tmp, a[i]);
      mpq_mul (buffer[i], tmp, T[i][i]);
    }

  for (size_t i = 0; i <= degree; i++)
    {
      *b = mpq_get_d (buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

static void
mpq_mul_row_by_general_square (size_t degree, ssize_t stride_a, mpq_t *a,
                               mpq_t T[degree + 1][degree + 1],
                               ssize_t stride_b, mpq_t *b)
{
  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_mul (buffer[i], a[0], T[0][i]);
      mpq_t *p = a + stride_a;
      for (size_t j = 1; j <= degree; j++)
        {
          if (mpq_sgn (T[j][i]) != 0)
            {
              mpq_mul (tmp, *p, T[j][i]);
              mpq_add (buffer[i], buffer[i], tmp);
            }
          p += stride_a;
        }
    }

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set (*b, buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

static void
double_mul_row_by_general_square (size_t degree, ssize_t stride_a,
                                  const double *a,
                                  mpq_t T[degree + 1][degree + 1],
                                  ssize_t stride_b, double *b)
{
  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set_d (tmp, a[0]);
      mpq_mul (buffer[i], tmp, T[0][i]);
      const double *p = a + stride_a;
      for (size_t j = 1; j <= degree; j++)
        {
          if (mpq_sgn (T[j][i]) != 0)
            {
              mpq_set_d (tmp, *p);
              mpq_mul (tmp, tmp, T[j][i]);
              mpq_add (buffer[i], buffer[i], tmp);
            }
          p += stride_a;
        }
    }

  for (size_t i = 0; i <= degree; i++)
    {
      *b = mpq_get_d (buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

static void
mpq_mul_row_by_upper_triangular (size_t degree, ssize_t stride_a, mpq_t *a,
                                 mpq_t T[degree + 1][degree + 1],
                                 ssize_t stride_b, mpq_t *b)
{
  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_mul (buffer[i], a[0], T[0][i]);
      mpq_t *p = a + stride_a;
      for (size_t j = 1; j <= i; j++)
        {
          mpq_mul (tmp, *p, T[j][i]);
          mpq_add (buffer[i], buffer[i], tmp);
          p += stride_a;
        }
    }

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set (*b, buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

static void
double_mul_row_by_upper_triangular (size_t degree, ssize_t stride_a,
                                    const double *a,
                                    mpq_t T[degree + 1][degree + 1],
                                    ssize_t stride_b, double *b)
{
  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set_d (tmp, a[0]);
      mpq_mul (buffer[i], tmp, T[0][i]);
      const double *p = a + stride_a;
      for (size_t j = 1; j <= i; j++)
        {
          mpq_set_d (tmp, *p);
          mpq_mul (tmp, tmp, T[j][i]);
          mpq_add (buffer[i], buffer[i], tmp);
          p += stride_a;
        }
    }

  for (size_t i = 0; i <= degree; i++)
    {
      *b = mpq_get_d (buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

static void
mpq_mul_row_by_spower_butterfly (size_t degree, ssize_t stride_a, mpq_t *a,
                                 mpq_t T[degree + 1][degree + 1],
                                 ssize_t stride_b, mpq_t *b)
{
  // Multiplication by the ‘butterfly-shaped’ matrices associated with
  // transformation between scaled Bernstein and symmetric power
  // (Sánchez-Reyes) bases.

  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  const size_t n = degree;
  const size_t q = n / 2 + n % 2;

  // Fill the buffer with zeros.
  for (size_t i = 0; i <= degree; i++)
    mpq_set_ui (buffer[i], 0, 1);

  // Scale the rows of T by the entries of a, and add them up.
  for (size_t i = 0; i < q; i++)
    for (size_t j = i; j < n - i; j++)
      {
        // The ith row, 0 <= i <= q.
        mpq_mul (tmp, a[(ssize_t) i * stride_a], T[i][j]);
        mpq_add (buffer[j], buffer[j], tmp);

        // The (degree - i)th row, 0 <= i <= q.
        mpq_mul (tmp, a[(ssize_t) (n - i) * stride_a], T[n - i][j + 1]);
        mpq_add (buffer[j + 1], buffer[j + 1], tmp);
      }

  if (n % 2 == 0)
    {
      // The extra middle row for even degrees.
      mpq_mul (tmp, a[q], T[q][q]);
      mpq_add (buffer[q], buffer[q], tmp);
    }

  for (size_t i = 0; i <= degree; i++)
    {
      mpq_set (*b, buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

static void
double_mul_row_by_spower_butterfly (size_t degree, ssize_t stride_a,
                                    const double *a,
                                    mpq_t T[degree + 1][degree + 1],
                                    ssize_t stride_b, double *b)
{
  // Multiplication by the ‘butterfly-shaped’ matrices associated with
  // transformation between scaled Bernstein and symmetric power
  // (Sánchez-Reyes) bases.

  mpq_t *buffer = xmalloc ((degree + 1) * sizeof (mpq_t));
  mpq_init_poly (degree, buffer);

  mpq_t tmp;
  mpq_init (tmp);

  const size_t n = degree;
  const size_t q = n / 2 + n % 2;

  // Fill the buffer with zeros.
  for (size_t i = 0; i <= degree; i++)
    mpq_set_ui (buffer[i], 0, 1);

  // Scale the rows of T by the entries of a, and add them up.
  for (size_t i = 0; i < q; i++)
    for (size_t j = i; j < n - i; j++)
      {
        // The ith row, 0 <= i <= q.
        mpq_set_d (tmp, a[(ssize_t) i * stride_a]);
        mpq_mul (tmp, tmp, T[i][j]);
        mpq_add (buffer[j], buffer[j], tmp);

        // The (degree - i)th row, 0 <= i <= q.
        mpq_set_d (tmp, a[(ssize_t) (n - i) * stride_a]);
        mpq_mul (tmp, tmp, T[n - i][j + 1]);
        mpq_add (buffer[j + 1], buffer[j + 1], tmp);
      }

  if (n % 2 == 0)
    {
      // The extra middle row for even degrees.
      mpq_set_d (tmp, a[q]);
      mpq_mul (tmp, tmp, T[q][q]);
      mpq_add (buffer[q], buffer[q], tmp);
    }

  for (size_t i = 0; i <= degree; i++)
    {
      *b = mpq_get_d (buffer[i]);
      b += stride_b;
    }

  mpq_clear (tmp);
  mpq_clear_poly (degree, buffer);
  free (buffer);
}

//----------------------------------------------------------------------

VISIBLE void
mpq_poly_mono_to_bern (size_t degree, ssize_t stride_a, mpq_t *a,
                       ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_upper_triangular (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_mono_to_bern (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_bern_to_mono (size_t degree, ssize_t stride_a, mpq_t *a,
                       ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_upper_triangular (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_bern_to_mono (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_mono_to_sbern (size_t degree, ssize_t stride_a, mpq_t *a,
                        ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_upper_triangular (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_mono_to_sbern (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_sbern_to_mono (size_t degree, ssize_t stride_a, mpq_t *a,
                        ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_upper_triangular (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_sbern_to_mono (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_bern_to_sbern (size_t degree, ssize_t stride_a, mpq_t *a,
                        ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_diagonal (degree, stride_a, a,
                           ((mpq_t (*)[degree + 1])
                            mpq_matrix_bern_to_sbern (degree)), stride_b, b);
}

VISIBLE void
mpq_poly_sbern_to_bern (size_t degree, ssize_t stride_a, mpq_t *a,
                        ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_diagonal (degree, stride_a, a,
                           ((mpq_t (*)[degree + 1])
                            mpq_matrix_sbern_to_bern (degree)), stride_b, b);
}

VISIBLE void
mpq_poly_sbern_to_spower (size_t degree, ssize_t stride_a, mpq_t *a,
                          ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_spower_butterfly (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_sbern_to_spower (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_spower_to_sbern (size_t degree, ssize_t stride_a, mpq_t *a,
                          ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_spower_butterfly (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_spower_to_sbern (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_bern_to_spower (size_t degree, ssize_t stride_a, mpq_t *a,
                         ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_spower_butterfly (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_bern_to_spower (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_spower_to_bern (size_t degree, ssize_t stride_a, mpq_t *a,
                         ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_spower_butterfly (degree, stride_a, a,
                                   ((mpq_t (*)[degree + 1])
                                    mpq_matrix_spower_to_bern (degree)),
                                   stride_b, b);
}

VISIBLE void
mpq_poly_mono_to_spower (size_t degree, ssize_t stride_a, mpq_t *a,
                         ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_general_square (degree, stride_a, a,
                                 ((mpq_t (*)[degree + 1])
                                  mpq_matrix_mono_to_spower (degree)),
                                 stride_b, b);
}

VISIBLE void
mpq_poly_spower_to_mono (size_t degree, ssize_t stride_a, mpq_t *a,
                         ssize_t stride_b, mpq_t *b)
{
  mpq_mul_row_by_general_square (degree, stride_a, a,
                                 ((mpq_t (*)[degree + 1])
                                  mpq_matrix_spower_to_mono (degree)),
                                 stride_b, b);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_mono_to_bern (size_t degree, ssize_t stride_a, const double *a,
                    ssize_t stride_b, double *b)
{
  double_mul_row_by_upper_triangular (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_mono_to_bern (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_bern_to_mono (size_t degree, ssize_t stride_a, const double *a,
                    ssize_t stride_b, double *b)
{
  double_mul_row_by_upper_triangular (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_bern_to_mono (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_mono_to_sbern (size_t degree, ssize_t stride_a, const double *a,
                     ssize_t stride_b, double *b)
{
  double_mul_row_by_upper_triangular (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_mono_to_sbern (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_sbern_to_mono (size_t degree, ssize_t stride_a, const double *a,
                     ssize_t stride_b, double *b)
{
  double_mul_row_by_upper_triangular (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_sbern_to_mono (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_bern_to_sbern (size_t degree, ssize_t stride_a, const double *a,
                     ssize_t stride_b, double *b)
{
  double_mul_row_by_diagonal (degree, stride_a, a,
                              ((mpq_t (*)[degree + 1])
                               mpq_matrix_bern_to_sbern (degree)), stride_b, b);
}

VISIBLE void
dpoly_sbern_to_bern (size_t degree, ssize_t stride_a, const double *a,
                     ssize_t stride_b, double *b)
{
  double_mul_row_by_diagonal (degree, stride_a, a,
                              ((mpq_t (*)[degree + 1])
                               mpq_matrix_sbern_to_bern (degree)), stride_b, b);
}

VISIBLE void
dpoly_sbern_to_spower (size_t degree, ssize_t stride_a, const double *a,
                       ssize_t stride_b, double *b)
{
  double_mul_row_by_spower_butterfly (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_sbern_to_spower (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_spower_to_sbern (size_t degree, ssize_t stride_a, const double *a,
                       ssize_t stride_b, double *b)
{
  double_mul_row_by_spower_butterfly (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_spower_to_sbern (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_bern_to_spower (size_t degree, ssize_t stride_a, const double *a,
                      ssize_t stride_b, double *b)
{
  double_mul_row_by_spower_butterfly (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_bern_to_spower (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_spower_to_bern (size_t degree, ssize_t stride_a, const double *a,
                      ssize_t stride_b, double *b)
{
  double_mul_row_by_spower_butterfly (degree, stride_a, a,
                                      ((mpq_t (*)[degree + 1])
                                       mpq_matrix_spower_to_bern (degree)),
                                      stride_b, b);
}

VISIBLE void
dpoly_mono_to_spower (size_t degree, ssize_t stride_a, const double *a,
                      ssize_t stride_b, double *b)
{
  double_mul_row_by_general_square (degree, stride_a, a,
                                    ((mpq_t (*)[degree + 1])
                                     mpq_matrix_mono_to_spower (degree)),
                                    stride_b, b);
}

VISIBLE void
dpoly_spower_to_mono (size_t degree, ssize_t stride_a, const double *a,
                      ssize_t stride_b, double *b)
{
  double_mul_row_by_general_square (degree, stride_a, a,
                                    ((mpq_t (*)[degree + 1])
                                     mpq_matrix_spower_to_mono (degree)),
                                    stride_b, b);
}

//----------------------------------------------------------------------

VISIBLE void
dpoly_unsplit_spower (size_t degree,
                      ssize_t stride0, const double *a0,
                      ssize_t stride1, const double *a1,
                      ssize_t result_stride, double *result)
{
  // Construct a set of symmetric power basis coefficients from two
  // smaller sets of monomial basis coefficients.

  if (degree % 2 == 1)
    {
      copy_f64_with_strides (result_stride, result, stride0, a0,
                             degree / 2 + 1);
      copy_f64_with_strides (-result_stride,
                             &result[result_stride * (ssize_t) degree],
                             stride1, a1, degree / 2 + 1);
    }
  else
    {
      copy_f64_with_strides (result_stride, result, stride0, a0, degree / 2);
      const double m0 = a0[stride0 * (ssize_t) (degree / 2)];
      const double m1 = a1[stride1 * (ssize_t) (degree / 2)];
      result[result_stride * (ssize_t) (degree / 2)] =
        (m0 == m1) ? m0 : 0.5 * (m0 + m1);
      copy_f64_with_strides (-result_stride,
                             &result[result_stride * (ssize_t) degree],
                             stride1, a1, degree / 2);
    }
}

//----------------------------------------------------------------------
