.Dd August 13, 2013
.\"
.\" Copyright (C) 2013 Khaled Hosny and Barry Schwartz
.\"
.\" This file is part of Sorts Mill Core Library.
.\" 
.\" Sorts Mill Core Library is free software: you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation, either version 3 of
.\" the License, or (at your option) any later version.
.\" 
.\" Sorts Mill Core Library is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
.\" General Public License for more details.
.\" 
.\" You should have received a copy of the GNU General Public
.\" License along with Sorts Mill Core Library.  If not, see
.\" <http://www.gnu.org/licenses/>.
.\"
.\" --------------------------------------------------------------------------------
.\" Copyright (C) 2002-2013 Jason Evans <jasone@canonware.com>.
.\" All rights reserved.
.\" Copyright (C) 2007-2012 Mozilla Foundation.  All rights reserved.
.\" Copyright (C) 2009-2013 Facebook, Inc.  All rights reserved.
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions are met:
.\" 1. Redistributions of source code must retain the above copyright notice(s),
.\"    this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright notice(s),
.\"    this list of conditions and the following disclaimer in the documentation
.\"    and/or other materials provided with the distribution.
.\" 
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER(S) ``AS IS'' AND ANY EXPRESS
.\" OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
.\" MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
.\" EVENT SHALL THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
.\" LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
.\" PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
.\" LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
.\" OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
.\" ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\" --------------------------------------------------------------------------------
.\"
.\" This manpage is written in mdoc. See groff_mdoc(7).
.\"
.Dt RB.H 3
.Os
.Sh NAME
.Nm rb.h ,
.Nm rb_gen
.Nd C preprocessor macro implementation of left-leaning 2-3 red-black trees
.\" .Sh LIBRARY
.\" .Lb libsortsmill_core
.Sh SYNOPSIS
.Fd "/* Optionally, disable assertions. See assert(3). */"
.Fd "#define NDEBUG"
.Pp
.Fd "/* Optionally, embed color bits in right-child pointers. */"
.Fd "#define RB_COMPACT"
.Pp
.In sortsmill/core.h
.Sh DESCRIPTION
The
.Fn rb_gen
macro generates a type-specific red-black tree implementation.
.Pp
Arguments:
.Bd -literal
  a_attr    : Function attribute for generated functions (ex: static).
  a_prefix  : Prefix for generated functions (ex: ex_).
  a_rb_type : Type for red-black tree data structure (ex: ex_t).
  a_type    : Type for red-black tree node data structure (ex: ex_node_t).
  a_field   : Name of red-black tree node linkage (ex: ex_link).
  a_cmp     : Node comparison function name, with the following prototype:
                int (a_cmp *)(a_type *a_node, a_type *a_other);
                                      ^^^^^^
                                   or a_key
              Interpretation of comparison function return values:
                -1 : a_node <  a_other
                 0 : a_node == a_other
                 1 : a_node >  a_other
              In all cases, the a_node or a_key macro argument is the first
              argument to the comparison function, which makes it possible
              to write comparison functions that treat the first argument
              specially.
.Ed
.Pp
Assuming the following setup:
.Bd -literal
  typedef struct ex_node_s ex_node_t;
  struct ex_node_s {
      rb_node(ex_node_t) ex_link;
  };
  typedef rb_tree(ex_node_t) ex_t;
  rb_gen(static, ex_, ex_t, ex_node_t, ex_link, ex_cmp)
.Ed
.Pp
The following API is generated:
.Bd -literal
  static void
  ex_new(ex_t *tree);
      Description: Initialize a red-black tree structure.
      Args:
        tree: Pointer to an uninitialized red-black tree object.

  static ex_node_t *
  ex_first(ex_t *tree);
  static ex_node_t *
  ex_last(ex_t *tree);
      Description: Get the first/last node in tree.
      Args:
        tree: Pointer to an initialized red-black tree object.
      Ret: First/last node in tree, or NULL if tree is empty.

  static ex_node_t *
  ex_next(ex_t *tree, ex_node_t *node);
  static ex_node_t *
  ex_prev(ex_t *tree, ex_node_t *node);
      Description: Get node's successor/predecessor.
      Args:
        tree: Pointer to an initialized red-black tree object.
        node: A node in tree.
      Ret: node's successor/predecessor in tree, or NULL if node is
           last/first.

  static ex_node_t *
  ex_search(ex_t *tree, ex_node_t *key);
      Description: Search for node that matches key.
      Args:
        tree: Pointer to an initialized red-black tree object.
        key : Search key.
      Ret: Node in tree that matches key, or NULL if no match.

  static ex_node_t *
  ex_nsearch(ex_t *tree, ex_node_t *key);
  static ex_node_t *
  ex_psearch(ex_t *tree, ex_node_t *key);
      Description: Search for node that matches key.  If no match is found,
                   return what would be key's successor/predecessor, were
                   key in tree.
      Args:
        tree: Pointer to an initialized red-black tree object.
        key : Search key.
      Ret: Node in tree that matches key, or if no match, hypothetical node's
           successor/predecessor (NULL if no successor/predecessor).

  static void
  ex_insert(ex_t *tree, ex_node_t *node);
      Description: Insert node into tree.
      Args:
        tree: Pointer to an initialized red-black tree object.
        node: Node to be inserted into tree.

  static void
  ex_remove(ex_t *tree, ex_node_t *node);
      Description: Remove node from tree.
      Args:
        tree: Pointer to an initialized red-black tree object.
        node: Node in tree to be removed.

  static ex_node_t *
  ex_iter(ex_t *tree, ex_node_t *start, ex_node_t *(*cb)(ex_t *,
    ex_node_t *, void *), void *arg);
  static ex_node_t *
  ex_reverse_iter(ex_t *tree, ex_node_t *start, ex_node *(*cb)(ex_t *,
    ex_node_t *, void *), void *arg);
      Description: Iterate forward/backward over tree, starting at node.  If
                   tree is modified, iteration must be immediately
                   terminated by the callback function that causes the
                   modification.
      Args:
        tree : Pointer to an initialized red-black tree object.
        start: Node at which to start iteration, or NULL to start at
               first/last node.
        cb   : Callback function, which is called for each node during
               iteration.  Under normal circumstances the callback function
               should return NULL, which causes iteration to continue.  If a
               callback function returns non-NULL, iteration is immediately
               terminated and the non-NULL return value is returned by the
               iterator.  This is useful for re-starting iteration after
               modifying tree.
        arg  : Opaque pointer passed to cb().
      Ret: NULL if iteration completed, or the non-NULL callback return value
           that caused termination of the iteration.
.Ed
.\" .Sh IMPLEMENTATION NOTES
.\" .Sh RETURN VALUES
.\" .Sh FILES
.\" .Sh EXAMPLES
.\" .Sh ERRORS
.\" .Sh SEE ALSO
.\" FIXME
.\" .Sh STANDARDS
.Sh HISTORY
The version of
.In rb.h
in Sorts Mill Core Library was adapted from the one used in the
implementation of jemalloc version 3.3.1.
.Sh AUTHORS
.In rb.h
was written by Jason Evans.
.Sh BUGS
This manual page consists mostly of comments copied verbatim from the
source code. Something better should be done.
