.Dd August 16, 2013
.\"
.\" Copyright (C) 2013 Khaled Hosny and Barry Schwartz
.\"
.\" This file is part of Sorts Mill Core Library.
.\" 
.\" Sorts Mill Core Library is free software: you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation, either version 3 of
.\" the License, or (at your option) any later version.
.\" 
.\" Sorts Mill Core Library is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
.\" General Public License for more details.
.\" 
.\" You should have received a copy of the GNU General Public
.\" License along with Sorts Mill Core Library.  If not, see
.\" <http://www.gnu.org/licenses/>.
.\"
.\" This manpage is written in mdoc. See groff_mdoc(7).
.\"
.Dt STM_ONE_TIME_INITIALIZE 3
.Os
.Sh NAME
.Nm STM_ONE_TIME_INITIALIZE
.Nd one-time initialization of a global variable at runtime
.Sh LIBRARY
.Lb libsortsmill_core
.Sh SYNOPSIS
.In sortsmill/core.h
.Fn STM_ONE_TIME_INITIALIZE type_modifiers type func thread_lock_code thread_unlock_code initialization_code
.Ft "type_modifiers type"
.Fn func void
.Pp
.Va "static type func__Value" ;
.Sh DESCRIPTION
The macro call
.Fn STM_ONE_TIME_INITIALIZE type_modifiers type func thread_lock_code thread_unlock_code initialization_code
defines a C function
.Fn func
that behaves as follows. The first time the
function is called, it initializes the variable
.Va func__Value
(which the macro also
defines), then returns the initialized value. On subsequent calls, the
function skips the initialization stage and simply returns the value
of the variable. Thus one achieves the effect of a
.Sq constant
that is initialized at runtime. The initialization is
.Sq lazy ;
it does not occur until the initialized value actually is needed.
.Pp
The
.Ar type_modifiers
macro argument may be zero or more declarations such as
.Ft static
and
.Dv STM_ATTRIBUTE_PURE .
.Pp
The
.Ar thread_lock_code
and
.Ar thread_unlock_code
arguments represent snippets of C code. They should perform locking
actions such as, respectively, locking and unlocking a
.Xr pthreads 7
mutex.
.Pp
The
.Ar initialization_code
argument represents a snippet of C code that must initialize the variable
.Va func__Value .
This variable should not be accessed directly anywhere but in
.Ar initialization_code .
.Sh IMPLEMENTATION NOTES
.Fn STM_ONE_TIME_INITIALIZE
is implemented in terms of
.Fn stm_dcl_load_indicator 3
and
.Fn stm_dcl_store_indicator 3 ,
and thus is guaranteed thread-safe only if
.Lb libsortsmill_core
is built with atomic_ops. Refer to the manual page for those functions
for more information.
.Pp
.Fn STM_ONE_TIME_INITIALIZE
creates, in addition to
.Fn func
and
.Va func__Value ,
an unsigned integer-valued variable called
.Va __func__is_initialized
that never should be accessed directly.
.\" .Sh RETURN VALUES
.\" .Sh FILES
.Sh EXAMPLES
The following slightly contrived program defines a function
.Fn pi
that returns the value of \*(Pi. The value is computed at runtime
just once, the first time
.Fn pi
is called. Notice that the function is declared
.Dv STM_ATTRIBUTE_PURE
so the compiler can optimize out some calls to the function.
.Bd -literal -offset indent
#include <sortsmill/core.h>
#include <pthread.h>
#include <math.h>
#include <stdio.h>

pthread_mutex_t pi__Mutex = PTHREAD_MUTEX_INITIALIZER;

STM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE, double, pi,
                         pthread_mutex_lock (&pi__Mutex),
                         pthread_mutex_unlock (&pi__Mutex),
                         (pi__Value = 4 * atan (1)));

int
main (int argc, char **argv)
{
  printf ("pi   = %lf\\n", pi ());
  printf ("pi/2 = %lf\\n", pi () / 2);
  printf ("pi/3 = %lf\\n", pi () / 3);
  printf ("pi/4 = %lf\\n", pi () / 4);
}
.Ed
.Pp
Probably, in practice, for the value of \*(Pi one rather would use the
.Dv M_PI
macro from
.In math.h .
.Pp
The following quite contrived example defines a function
.Fn scm_pi
that returns the value of \*(Pi, but this time as a Guile value
.Pq type Vt SCM
rather than
.Vt double .
An interesting difference from the first example is that this time a
dynamic wind is used to unlock the mutex, so the mutex is not left
locked should, for example, a Guile exception occur during attempted
initialization.
.Bd -literal -offset indent
#include <sortsmill/core.h>
#include <pthread.h>
#include <libguile.h>
#include <stdio.h>

void
pthread_mutex_unlocker (void *mutex_ptr)
{
  pthread_mutex_unlock ((pthread_mutex_t *) mutex_ptr);
}

void
scm_dynwind_pthread_mutex_unlock (void *mutex_ptr)
{
  scm_dynwind_unwind_handler (pthread_mutex_unlocker, mutex_ptr,
                              SCM_F_WIND_EXPLICITLY);
}

pthread_mutex_t scm_pi__Mutex = PTHREAD_MUTEX_INITIALIZER;

STM_ONE_TIME_INITIALIZE
  (STM_ATTRIBUTE_PURE, SCM, scm_pi,
   scm_dynwind_begin (0); pthread_mutex_lock (&scm_pi__Mutex);
   scm_dynwind_pthread_mutex_unlock (&scm_pi__Mutex),
   scm_dynwind_end (),
   (scm_pi__Value = scm_angle (scm_make_rectangular
                               (scm_from_int (-1),
                                scm_from_int (0)))));

void *
run_in_guile_mode (void *p)
{
  printf ("pi   = %lf\\n", scm_to_double (scm_pi ()));
  printf ("pi/2 = %lf\\n",
          scm_to_double (scm_divide (scm_pi (),
                                     scm_from_int (2))));
  printf ("pi/3 = %lf\\n",
          scm_to_double (scm_divide (scm_pi (),
                                     scm_from_int (3))));
  printf ("pi/4 = %lf\\n",
          scm_to_double (scm_divide (scm_pi (),
                                     scm_from_int (4))));
}

int
main (int argc, char **argv)
{
  scm_with_guile (run_in_guile_mode, NULL);
}
.Ed
.\" .Sh ERRORS
.Sh SEE ALSO
.Xr math.h 0 ,
.Xr guile 1 ,
.Xr STM_ATTRIBUTE_PURE 3 ,
.Xr stm_dcl_load_indicator 3 ,
.Xr stm_dcl_store_indicator 3 ,
.Xr pthreads 7
.\" .Sh STANDARDS
.\" .Sh HISTORY
.Sh AUTHORS
.An Barry Schwartz
.Sh BUGS
If
.Lb libsortsmill_core
is configured and built without atomic_ops
.Pq Nm configure Fl -without-atomic-ops ,
thread-safety cannot be guaranteed.
