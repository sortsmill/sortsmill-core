// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#ifndef SORTSMILL_PATS_CCOMP_MEMALLOC_USER_H_
#define SORTSMILL_PATS_CCOMP_MEMALLOC_USER_H_

#include <sortsmill/core/xgc.h>

static inline void
atsruntime_minit_user (void)
{
  // Do nothing.
}

static inline void *
atsruntime_malloc_user (size_t n)
{
  return x_gc_malloc (n);
}

static inline void
atsruntime_mfree_user (void *p)
{
  x_gc_free (p);
}

static inline void *
atsruntime_calloc_user (size_t n, size_t size)
{
  return x_gc_malloc (n * size);
}

static inline void *
atsruntime_realloc_user (void *p, size_t n)
{
  return x_gc_realloc (p, n);
}

#endif /* SORTSMILL_PATS_CCOMP_MEMALLOC_USER_H_ */
